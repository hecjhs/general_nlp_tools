30 diseños de tatuajes para hombres y mujeres por los que vale la pena aguantar el dolor

 El cuerpo humano está preparado para soportar el dolor por muchas razones; seas hombre o mujer, tu organismo siempre está intentando dar una respuesta favorable porque: 
 1. Se ha descubierto que es una mentira eso de que las mujeres aguantan más que los hombres. 
 2. Se ha comprobado que esa resistencia es una predisposición genética independientemente de tu género. 
 3. La manera en la que se experimenta el dolor también depende de factores externos; no todo recae en ti.  
 Así que, rompiendo con paradigmas falsos e ideas poco fundamentadas, cualquier persona es libre de sufrir cuanto le sea necesario por el motivo que se le presente. Se puede ser un hombre en extremo sensible o una mujer demasiado ruda, que de todas formas, como individuos de la misma especie, somos capaces de hacer frente a las mismas cosas sin miedo a equivocarnos o a “hacer el ridículo”. Bajo ese mismo discurso, entonces no habría porqué pensar que sólo el típico motociclista vestido de cuero y aretes es apto para hacerse un doloroso tatuaje; siempre y cuando se desee con todas las fuerzas, todos somos capaces de resistir un gran sufrimiento con tal de cumplir con una sesión u obtener determinado diseño. Sobre todo si éste último es de fuerte agrado para nosotros. 
  Como bien sabemos, hay zonas en el cuerpo que son más susceptibles al daño que otras, pero si ese tatuaje en específico nos tiene obsesionados, nada nos va a detener hasta conseguirlo. 
 A continuación, enlistamos algunos ejemplos de ello; excelentes trabajos que valieron en lo absoluto la pena, aunque esto significara algunos gritos ahogados o una pequeña lágrima de vez en cuando. Si estás pensando en realizarte un tatuaje similar o en la misma zona, sólo considera que sí, lo vas a lograr, pero necesitas ingerir suficiente agua, utilizar ropa cómoda y relajarte hasta que todo haya terminado. – 
   
  
 “El dolor es tan contagioso como un bostezo”. Sue Garfton 
   
  
 “El dolor enseñaba que una forma, aunque opaca, puede ser luminosa”. Luis Cernuda 
   
  “No hay más que un dolor, estar solo”. Siro 
   
  
 “Los dolores imaginarios son, con mucho, los más reales, ya que se les necesita constantemente y se inventan porque no es posible prescindir de ellos”. Emil Cioran 
   
  
 “El dolor es una cosa bestial y feroz, trivial y gratuita, natural como el aire”. Cesare Pavese 
   
  
 “Allí donde está el dolor, está también lo que lo salva”. Friedrich Hölderlin 
   
  
 “El dolor siempre cumple lo que promete”. Germaine de Staël 
   
  
 “Quien sabe de dolor, todo lo sabe”. Dante Alighieri 
   
  
 “No existe ningún dolor imposible, lo único infinito es el dolor”. Elias Canetti 
   
  
 “Todo hombre se parece a su dolor”. André Malraux 
   
  
 “Cuando el dolor es insoportable, nos destruye; cuando no nos destruye, es que es soportable”. Marco Aurelio 
   
  
 “Detrás de cada cosa hermosa, hay algún tipo de dolor”. Bob Dylan 
   
  
 “Quien quisiera que el hombre no conociera el dolor, evitaría al mismo tiempo el conocimiento del placer y reduciría al mismo hombre a la nada”. Michel de Montaigne 
   
  
 “El dolor, si es grave, es breve; si largo, es leve”. Cicerón 
   
  “Uno no se hace grande más que midiendo la pequeñez de su dolor”. Ernst Wiechert 
 – Puede que el tatuaje tarde años en realizarse, que se necesite más de una sesión para finalizarlo, que la zona sea muy sensible, que te digan que hay tintas más dolorosas que otras, pero a fin de cuentas, sabes que todo valió la pena porque tienes el diseño correcto sobre tu piel y eso sí va a durar por siempre contigo. 
 *** Te puede interesar: 
 Preguntas que estamos cansados de responder quienes tenemos tatuajes 
 Tatuajes para quienes creen que bailar es soñar con los pies