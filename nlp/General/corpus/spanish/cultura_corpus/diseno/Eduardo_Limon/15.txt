Tatuajes para quienes creen que bailar es soñar con los pies

 
 Yo no creería mas que en un dios que supiese bailar. Friedrich Nietzsche 
 Alguna vez ese Zaratustra con voz de Nietzsche , o si acaso fue un Nietzsche con cuerpo de Zaratustra, observó detalladamente que en la danza se encontraba una metáfora del pensamiento. En ese danzar que es opuesto a la pesadez, que hace del cuerpo algo radiantemente ausente, que le quita toda negatividad  y vergüenza, Nietzsche encontró para sus escritos iniciales y todos los posteriores un recurso estético para hablar sobre el espíritu de la ligereza. De esta forma, queda de manifiesto el valor (reivindicado) del cuerpo; ese arte que se realiza por encima de todas las demás manifestaciones y expresión de la vida dirigiéndose al superhombre. 
  
 La danza, entonces, es esa práctica que puede dirigir al cuerpo humano, al pensamiento del hombre, hacia lo más elevado. Es ese elemento necesario para disfrutar de la libertad sobre las cosas, un movimiento ligero que juega y valora sobre la moral y todo lo realmente pesado en el mundo. El bailarín es un individuo que sabe escuchar y atender a su cuerpo, es un hombre que conoce la embriaguez y el éxtasis, un pensamiento que se eleva a un lugar privilegiado, puesto que se sirve de sus piernas para que la tierra le sea más liviana y así reír, caminar, correr, saltar, trepar y bailar haciéndose uno con el cosmos y dejando atrás (abajo) aquello que le encadena. 
  Para quienes la danza, nietzscheanamente hablando, les significa una manera de renunciar a las ataduras del mundo fatigoso y torpe, dicho arte se debe hacer visible en cuantas formas le sea posible. De esta forma, los tatuajes relativos a bailar, a juguetear en el aire y con la realidad han adquirido una importancia vital como medio de representación total para tal ejercicio. Esa conducta que podría leerse también como un ensueño con los ojos plenamente abiertos, con los pies íntegramente activos. A continuación se muestran algunos diseños que obedecen al mencionado ímpetu por probar eso que guarda veloces similitudes con el vuelo. “Bailar es como soñar con tus pies”. Constanze     “A nadie le importa si no puedes bailar bien. Levántate y baila. Los grandes bailadores son muy buenos por su pasión”. Martha Graham 
   
  “No quiero gente que quiera bailar, quiero gente que tenga que bailar”. George Balanchine 
   
  “Hay un poco de locura en el baile que hace a todo el mundo mucho bien”. Edwin Denby 
  
  “Hay atajos para la felicidad, y el baile es uno de ellos”. Vicki Baum 
   
  “Mientras bailo no puedo juzgar. No puedo odiar, no puedo separarme de la vida. Sólo puedo estar alegre y entero. Es por ello que bailo”. Hans Bos 
   
  “Las expresiones más auténticas de la gente están en su baile y en su música. El cuerpo nunca miente”. Agnes de Mille 
   
  
 “Deberíamos considerar perdidos los días en que no hemos bailado al menos una vez”.Friedrich Nietzsche– “Mi Alfa y mi Omega es que todo lo que es pesado y grave llegue a ser ligero; todo lo que es cuerpo , bailarín; todo lo que es espíritu , pájaro”. Nietzsche 
 *** Te puede interesar: 
 Tatuajes inspirados en obras de arte que salieron mal 
 Tatuajes inspirados en libros