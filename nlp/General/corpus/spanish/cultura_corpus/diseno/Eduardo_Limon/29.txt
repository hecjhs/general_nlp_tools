Así se verían las películas clásicas de Disney ilustradas por Tim Burton

 Se abre la tierra y de ella emerge un tétrico árbol con una que otra hoja seca, apenas sosteniéndose de las oscuras raíces que le sirven de sostén. Calabazas cantan y murciélagos hacen el coro con una luna menguante de fondo. Las criaturas más extrañas han salido a pasar la noche con el resto de nosotros. 
 Desde su macabro ingreso a la cultura popular, Tim Burton no ha sido un director cualquiera; a pesar de su renombrada fama, nunca ha caído en el lugar seguro ni ha sido el ídolo de las masas. Para muchos de nosotros, seguramente, la primera vez que estuvimos frente al universo de Burton no sabíamos qué estaba sucediendo, pero nos encantaba. No podíamos explicar hacia dónde se dirigía ese cúmulo de sobrenaturalidad, pero queríamos seguirlo a donde fuera. Gracias a su trabajo comprendimos lo amistosas que pueden las tinieblas y qué tanto podemos estar fascinados por la ultratumba. La estética y la imaginación de este genio se han ido refinando con el tiempo, pero nunca han perdido esa visión característica de las otras realidades que nos esperan; quizá, por eso, también sea tan amado. 
  Su genio creativo ha traído a nuestras pantallas joyas irremplazables como: “Batman”, “The Nightmare before Christmas”, “El Cadáver de la novia”, “Edward Scissorhands” y “Frankenweenie”. Cada una demuestra una evolución en su planteamiento narrativo y una continuación de los matices que hacen clásicas a sus historias. 
  Tenemos una fiebre ya casi extinta por reimaginar el contexto y la apariencia de caricaturas clásicas de Disney, en especial de sus princesas. Afortunadamente esta euforia ya no se encuentra en su momento más alto y hemos podido prescindir por bastante tiempo de estas caracterizaciones alternativas a dichos personajes. Ya sea con un propósito social, como una campaña contra la violencia de género, o por el simple gusto de poner a Blancanieves en una situación incómoda, llegó el momento en que estos diseños cansaron nuestra vista y aburrieron nuestra mente. Caso contrario a los siguientes diseños que, si bien no fueron ejecutados por el mismo Burton, retoman su imaginario y no se quedan en la mirada corta de trasladar la imagen de Cenicienta, o de Ariel, a otros terrenos. Claro, lo hace, pero es importante poner la suficiente atención en cada ilustración para desenmarañar sus posibilidades psicológicas o de conducta, las cuales adquieren una sombra particular en la mente de cada uno. Lo único que basta es dedicarles el tiempo necesario y dejar que éstas hablen por sí solas para que una nueva película se formule en tu cabeza. 
   
 “La locura de una persona no es su realidad”. T.B. 
   
 “ Nunca estuve interesado en lo que los demás pensaban. Siempre me sentí algo triste”. T.B. 
   
 “Como artista, es bueno recordar siempre ver las cosas de una manera y extraña forma”. T.B. 
   
 “Las películas son para mí como una forma cara de terapia”. T.B. 
   
 “Merece la pena luchar por una visión. ¿Por qué malgastar tu vida haciendo los sueños de otro?” T.B. 
   
 “Todos sabemos que el romance entre especies es raro”. T.B. 
   
 “ Si alguna vez has tenido esa sensación de soledad, de ser un extraño, nunca te deja. Puedes ser feliz o exitoso, pero eso nunca te deja”. T.B. 
   
 “Cualquier persona con ambiciones artísticas intenta siempre reconectar con la forma en que veía las cosas cuando era niño”. T.B. 
   
 “ Creo que muchos niños se sienten solos y aislados en su propio mundo”. T.B.    “ Para algunos de nosotros, Halloween es todos los días”.T.B.***Te puede interesar:Datos que no sabías de Halloween12 elementos que caracterizan una película de Tim Burton