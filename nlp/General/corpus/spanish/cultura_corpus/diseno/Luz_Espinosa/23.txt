El arte desde el vitral

 Composiciones enmarcadas en medallones, nichos u otros compartimentos; colores vivos y saturados, formas delimitadas y precisas son pautas estéticas que se encuentran en los vitrales, composiciones que tuvieron su apogeo en la arquitectura gótica y se generalizaron en las construcciones  del siglo XIII. Los vitrales sustituyeron, durante el estilo gótico,  a la pintura mural que se había desarrollado durante el periodo románico en el que la pintura a gran formato se empleó sólo como un arte complementario. Los vitrales se convirtieron en el arte hegemónico del color y del dibujo, pues retomó las funciones simbólico-docentes de la pintura mural a través de sus elementos iconográficos, los que se presentaron en  los talleres y centros artísticos europeos. 
  Los primeros vitrales mostraban pasajes bíblicos y ya en el siglo XVII, estos  se sustituyeron  por mosaicos geométricos de vidrios de colores, desapareciendo así el verdadero arte que tanto brilló en la arquitectura gótica. Esta es una muestra de los mejores murales que se conservan en algunas catedrales del mundo: Sainte Chapelle, Francia 
  Aunque muchos afirman que en París  no hay catedral más bella que la de Notre Dame, Sainte Chapelle también debe ser visitada por su impresionante techo gótico, comisionado por Luis IX en 1239. 
  En esta catedral se encuentran quince de las mejores vidrieras del mundo, estos presentan detalladas escenas del Antiguo y Nuevo Testamento. El Centro Cultural de Chicago, Estados Unidos 
  
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-chicago-dome-2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-chicago-dome-1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-chicago-dome-4.jpg
  Este Centro Cultural fue diseñado, originalmente, como una biblioteca en 1887, cuenta con una cúpula de 38 metros de alto, conformada, en su totalidad, por cristal de Tiffany. Museo Erawan, Tailandia 
  
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-erawan-4.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-erawan-2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/stained-glass-erawan-1.jpg
  El techo de este museo está construido con vitrales occidentales que simula el mapa del mundo  con el zodiaco. Sostenido por cuatro columnas en las que se aprecian relieves e historias religiosas que muestran los principios morales que hacen que haya paz en el mundo. Representando el corazón espiritual de Tailandia, el Museo Erawan  es una construcción emblemática que cuenta con obras de arte tailandés de incalculable valor. Estos se muestran en los tres pisos del edificio que, se dice: simbolizan los tres niveles del Universo. El vitarl brillante reside en el segundo piso, el que se considera como el nivel humano. En representación de la fusión de las culturas de Oriente y Occidente, la vidriera representa un atlas enmarcado por los signos zodiacales que unen a la humanidad. El Instituto Holandés de Imagen y Sonido 
  
http://culturacolectiva.com/wp-content/uploads/2013/12/instituto-holandes.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/imagen-y-sonido.jpg
  Este instituto es uno de los mayores archivos audiovisuales de Europa. En su interior, un espacio lleno de cubos artificial simboliza los medios de comunicación en la era digital. El exterior, de estética caleidoscópica, cuenta con fotografías de imágenes de televisión de todo el mundo. Catedral de San Vito, República Checa 
  
http://culturacolectiva.com/wp-content/uploads/2013/12/vitrales-en-republica-checa.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/vitrales-republica.jpg
http://culturacolectiva.com/wp-content/uploads/2013/12/diseño-de-vitrales.jpg
  Max Svabinsky diseñó este vitral “tríptico” que se encuentra detrás del altar de la Catedral de San Vito. Es una composición que presenta a la Santísima Trinidad con la Virgen María y los santos y soberanos checos. Las tres ventanas fueron diseñadas por este maravilloso artista checo (la referencia que tengo es que fue en 1946 y 1947, pero no pude confirmarla, pues debería ser una fecha anterior) y fueron realizadas por Josef Kricka. En el centro se muestra a la Santísima Trinidad, con Dios Padre luciendo una corona regia; en sus brazos yace su Hijo Jesús muerto, y el Espíritu Santo en forma de paloma aparece a su derecha. La ventana de la izquierda muestra a la Virgen María con la corona de San Vito, junto a Santa Ludmila y el caballero Spytihnev II, duque de Bohemia (1031-1061). La ventana de la derecha representa a San Wenceslao, rey de Bohemia (1363-1419), rogando por su país natal, y a Carlos IV (1316-1378), su padre, ofreciendo una réplica de la Catedral en miniatura.