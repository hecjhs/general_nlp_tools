El efecto del papel en las ilustraciones de Eiko Ojala

 La escultura en papel es una técnica de ilustración en la que, hoja a hoja, se da volumen y profundidad a la imagen final. Bordes blancos dan vida a paisajes a través de cortes precisos y definidos. Gracias a esta técnica, las ilustraciones con cortes de papel adquieren profundidad. Este trabajo requiere de sombras y contrastes en los colores para que los efectos cobren vida. 
   Eiko Ojala es un ilustrador, diseñador gráfico y director de arte estonio quien cambió la técnica de cortar papel por el trabajo digital. Sin ayuda de un software 3D, Eiko logra dar profundidad y apariencia de papel a sus ilustraciones, simulando la técnica de escultura en papel, en soporte digital. 
   Las ilustraciones digitales de Eiko son creadas semejando cortes y montajes en cartulina. La edición de su trabajo logra igualar la apariencia en textura y color del material en el que se inspira: el papel. 
  Eiko asegura no utilizar software especializado para lograr el efecto deseado en sus ilustraciones, a veces hace uso de sombras fotográficas que después incorpora a su trabajo, mismo que, también, se compone de series de desnudos, naturaleza, moda y paisajes llenos de color. 
 
http://culturacolectiva.com/wp-content/uploads/2013/03/nue.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/nue1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/mue2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/ns.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/ndj.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/hndhd.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/bdfhg.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/dnfj.jpg
http://culturacolectiva.com/wp-content/uploads/2013/03/ndhfjd.jpg