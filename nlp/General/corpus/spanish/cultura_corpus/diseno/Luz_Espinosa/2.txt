El Principito: la visión de un mundo

 Pionero de la aviación, autor de best sellers e ídolo romántico, Antoine de Saint-Exupéry gozó en vida de una fama sólo superada por su celebridad póstuma. Su misteriosa desaparición aquel 31 de julio de 1944, durante un vuelo de reconocimiento sobre la Francia ocupada, y el posterior éxito mundial de El principito , hicieron de él un mito que desde entonces ha subyugado a los franceses, quienes lo consideran un “santo”.    Es su obra, El Principito , la que ha logrado que niños y adultos se sientan identificados con la historia que en cada una de sus páginas recuerda que todo adulto fue alguna vez un niño, que las rosas y las estrellas llegan a nuestra vida para transformarla, y que lo esencial es invisible a los ojos. Uno de los libros más leídos y vendidos alrededor del mundo es hoy de dominio público, lo que demostró que cientos de personas se han inspirado en este mítico personaje para crear nuevas historias. A partir del 1 de enero de 2015, cuando se cumplieron 70 años de la muerte de Antoine de Saint- Exupéry, los derechos de El Principito se liberaron y su propiedad intelectual pasó a ser Patrimonio de la Humanidad, así que algunos ilustradores demostraron que todos tenemos al Principito en la cabeza y decidieron hacer su propia versión.   Esta es una galería con “los otros Principitos”, ilustraciones que dan una nueva imagen al personaje original con toques de color y elementos añadidos: 
 El Principito de Einauz 
  El Principito de  Jamsan 
  El Principito de Israel Barrón 
  El Principito de Blue 
  El Principito de  Annie Carbo 
  
 El Principito de  Speth Maclean 
  El Principito de Pokita  El Principito de Kim Minji 
  El Principito de  Nuri Kely 
  El Principito de Nika Goltz 
  El Principito de Lynn Paske 
  El Principito de Hsiao-Chi Chang 
  El Principito de Salvador Núñez