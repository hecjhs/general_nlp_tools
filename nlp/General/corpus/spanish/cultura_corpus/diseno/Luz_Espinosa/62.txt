Michiya Tuskano: la poesía de la arquitectura

 La arquitectura orgánica promueve la armonía entre el entorno humano y el mundo natural. Mediante el diseño, busca comprender e integrarse al sitio, los edificios, los mobiliarios y los alrededores para que la nueva construcción se convierta en parte de una composición unificada. De esta manera, un arquitecto, necesariamente, afirmaba Frank Lloyd Wright, “debe ser un gran poeta”; uno que haga rimas con las puertas, estrofas con los cuartos y un poema del conjunto. 
  Cambiando las letras por el hormigón y el yeso, el arquitecto Michiya Tuskano encontró en esta profesión la expresión artística de la belleza. Casa T es una construcción situada en Miyazaki, al sur de Japón, ubicada entre edificios residenciales y sumergida en el intenso ruido de la ciudad. 
 Adaptándose a su contexto, Casa T resulta una “construcción Haiku”, pues surge, al igual que este estilo literario, “del asombro primitivo del japonés por lo que ocurre en la naturaleza”, en este caso, el entorno urbano en el que se encuentra la Casa T. 
  El arquitecto Michiya Tuskano señala que su decisión de crear una forma blanca y abstracta en medio de la ciudad, que se asemeja a un monolito blanco y marca un fuerte contraste con su entorno, fue causada por el ruido de la calle, así como por el de los edificios residenciales circundantes a la construcción de exterior de hormigón blanco y yeso e interior de madera. 
  La casa T, a diferencia de lo que se pensaría, es un espacio muy iluminado, pues cuenta con aberturas que permiten el paso de la luz natural a través de un patio que ofrece un contrapunto a los interiores de madera. 
   En un esfuerzo por darle prioridad a la privacidad, el arquitecto optó por “armonizar las circunstancias con un pedazo de placa para envolver el espacio de la casa entera”, de esta manera se aíslan los sonidos exteriores para proporcionar tranquilidad a los habitantes de la Casa T. 
  De esta forma, Michiya Tuskano muestra que el diseño arquitectónico es, como en la poesía, un juego de palabras que debe rimar, asombrar y “humanizar la naturaleza mecánica de los materiales”. 
 http://culturacolectiva.com/wp-content/uploads/2013/04/mn4.jpg
http://culturacolectiva.com/wp-content/uploads/2013/04/mn5.jpg
http://culturacolectiva.com/wp-content/uploads/2013/04/mn.jpg
http://culturacolectiva.com/wp-content/uploads/2013/04/mn1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/04/ma3.jpg