Sky House, la blancura del infinito

 La carga simbólica de la que se dota a los colores en sociedades de todo el mundo resulta increíble si se piensa en estos como algo que no existe más allá de la percepción visual. Sin el fenómeno de reflexión, los colores simplemente no existirían, pues siendo totalmente estrictos: sin la intervención ocular que capta las radiaciones electromagnéticas de un cierto rango, que luego son transmitidas al cerebro, el color no existiría. 
  El color, a lo largo de la historia no sólo moderna, ha sido referente de estudios y análisis sobre las reacciones que seres vivos tienen al reconocerlos y relacionarlos. Las definiciones no sólo han sido sociológicas y psicológicas, sino científicas, físicas, filosóficas y artísticas. Cada una de las ciencias y disciplinas que están en estrecho contacto con el fenómeno del color, llegaron a diversas conclusiones, coincidentes en algunos aspectos, que resultaron enriquecedoras para posteriores estudios. 
  La arquitectura moderna y el diseño de interiores hacen uso de la psicología del color para crear espacios amables que envuelvan a sus habitantes en atmósferas de confort. Por esta razón, y por que no existe un mejor lugar que el cielo, el arquitecto estadounidense David Hotson creó: Sky House , un espacio pensado en blanco para producir, en quien la habita, una impresión luminosa de infinito. 
  El edificio en el que Sky House fue rediseñada, es uno de los primeros y más antiguos rascacielos enmarcados sobrevivientes de acero en Nueva York. Este edificio se compone de 22 pisos completos, más sótano y un ático de cuatro pisos. Fue justo allí donde el arquitecto decidió desarrollar Sky House, y transformar una cáscara de hierro oxidado del siglo XIX en una residencia del siglo XXI.  Rodeada del paisaje urbano de Manhattan, Sky House fue diseñada, en su totalidad, en blanco. Para lograr la sensación de infinito, se colocaron espejos y vidrios por todo el interior, creando un reflejo interminable del espacio.  La casa está diseñada en los últimos cuatro pisos del viejo edificio; en los cuales, para ascender y descender, David Hotson modificó las vigas del inmueble para que éstas funcionaran como paredes para escalar. Además, para realizar un descenso divertido, el arquitecto pulió un tubo de acero inoxidable que funcionara como rampa. 
 http://culturacolectiva.com/wp-content/uploads/2013/06/Skyhouse_09.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/tubo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/tubo2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/rampa.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/rampa1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/rampa2.jpg Cada rincón esconde una sorpresa que “potencializa la sensación de vivir en un penthouse divertido, así como todo mundo piensa que sería”. 
 Las potencias psíquicas del color blanco siempre son positivas, y eso es lo que buscó David en el diseño de Sky House , un lugar de paz y tranquilidad, sin falta de diversión. http://culturacolectiva.com/wp-content/uploads/2013/06/bxj.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/mnd.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/xmx.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/hgjg.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/nhbk.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/nh.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/njd.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/jnhjk.jpg