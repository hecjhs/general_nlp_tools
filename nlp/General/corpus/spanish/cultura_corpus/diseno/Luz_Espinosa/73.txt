Carreteras de recuerdos

 Con una maleta apenas con lo necesario, un auto viejo y un tocacintas apenas funcional, decidí iniciar la mejor etapa de mi vida: los viajes que me llevarán al infinito. Mapas enredados por carreteras marcaron momentos importantes de mi vida, personas que me enseñaron, paisajes que evocan fotografías mentales y recuerdos que vienen y van como olas o transiciones lunares. Situaciones marcadas por un mapa y sus pequeños puntos me hacen saber que fui, soy y seré una línea del tiempo en el que cada tramo de carretera forma una parte del camino que recorro.          Matthew Cusick decidió cambiar la línea, el movimiento y color atrapado en el pincel por los trazos y fragmentos de mapas que capturan en un pequeño trozo de papel grandes extensiones territoriales, con el uso de este material, el artista busca hacer propio lo externo, captura en cada trozo de mapa un tiempo y espacio específico de vida.          El artista con residencia en Nueva York puede tardar hasta seis meses en terminar un collage, en ellos combina trozos de diferentes mapas para conseguir los tonos necesarios y así dar una apariencia uniforme en su trabajo que, visto a cierta distancia, parece una pintura.          El artista decidió utilizar mapas en collages para rescatar un poco de nuestros recuerdos; abandonó la paleta de colores por trozos de papel simbólicos que utiliza como mecanismo para entender que la vida es como esa carretera que une ciudades, y que en las carreteras de la vida, los caminos son obras que llevan a panoramas asombrosos, líneas que transforman lo ordinario en algo extraordinario.      
http://culturacolectiva.com/wp-content/uploads/2012/10/1-41.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1-51.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1-61.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1571.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1941.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/asdq1.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_1951.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_1971.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_1991.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_2041.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_2051.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_2091.jpg
http://culturacolectiva.com/wp-content/uploads/2012/10/Matthew-Cusick_2191.jpg