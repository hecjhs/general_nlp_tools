Supermagritte: surrealismo en el mundo de los hongos

 René Magritte aseguró que la creación de objetos nuevos, la transformación de objetos conocidos o el cambio de materia para ciertos objetos como un cielo de madera, por ejemplo, o el empleo de palabras asociadas a las imágenes, la falsa denominación de una imagen, la realización de ideas dadas por los amigos y la representación de ciertas visiones del sueño poco profundo, son, en general, los medios para obligar a los objetos a convertirse, por fin, en sensacionales. Y es que en su obra llena de símbolos invita al espectador a la reflexión a partir de los objetos más cotidianos. 
   Supermagritte tomó la figura de Mario Bros. y su mundo de Amanita muscaria y así creó el diálogo necesario para que el personaje y los escenarios del famoso videojuego se convirtieran en imágenes de reflexión, tal y como en las obras de Magritte, las que giran con frecuencia alrededor de la relación entre el lenguaje y sus objetos; de esta manera encontró en aquellos personajes los elementos necesarios para rendir un homenaje al artista surrealista René Magritte. 
   El artista digital, de quien sólo se sabe que vive en algún lugar de ese sueño lúcido entre el reino champiñón y subcom, sumergido en el surrealismo de las tuberías, plantas, dinosaurios, hongos y flores que arrojan fuego, siguió los consejos del artista belga, quien decía que “para la construcción de lo fantástico no hacen falta grandes alardes imaginativos”, y él encontró ese alarde de semejanza en Mario Bros. 
   Magritte expresó que el lenguaje y la cosa que designa a un objeto pone en cuestión la equivalencia entre la palabra y la imagen; entre estos se encuentra inmersa la breve emoción del descubrimiento; emoción que Supermagritte homenajeó en el mundo de los hongos. 
  
http://culturacolectiva.com/wp-content/uploads/2013/09/puesta-magritte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/obras-de-magrite.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/lluvia-de-magritte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/homenaje-a-magritte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/la-nube-de-magritte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/maguitte-la-nube.jpg
  Puedes ver más adaptaciones de algunas obras de René Magritte a través del  tumblr de Supermagritte.