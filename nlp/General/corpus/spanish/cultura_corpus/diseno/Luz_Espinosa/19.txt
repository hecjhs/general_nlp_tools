Casa Unimog, la memoria es una forma de arquitectura

 La escultora y artista francesa: Louose Bourgeois,  dijo que la arquitectura representa  un objeto de nuestra memoria. Cuando evocamos, cuando conjuramos la memoria para hacerla más clara, apilamos asociaciones de la misma manera que apilamos ladrillos para construir un edificio. De ser esto cierto: La memoria es una forma de arquitectura. 
  Una de las pocas leyendas vivas del mundo del automovilismo en el Unimog; este vehículo, producido por Mercedes-Benz en 1942, debe su nombre al acrónimo que proviene del alemán UNIversal MOtor Gerät (dispositivo motorizado de aplicaciones universales) y es el objeto de la memoria que inspiró al arquitecto alemán Fabian Evers, junto al despacho  Wezel Architektur para diseñar la casa Unimog. 
  Construida sobre una base traslúcida, la casa Unimog cuenta con un revestimiento corrugado opaco que cubre las paredes del primer piso y el techo, mientras que el nivel inferior está rodeado de policarbonato translúcido que permite que la luz del día penetre al taller en el que se encuentra el Unimog, vehículo que dio nombre a la pequeña construcción. Diseñado como un loft, el piso cuenta con una cocina, una recámara, un baño y una terraza que se conecta al interior a través de una pared de cristal que permite apreciar el paisaje exterior. 
  Al igual que el Unimog, que gracias a su tracción permite que pueda ser conducido tanto en carreteras como en campos agrícolas, los materiales con los que compone la casa que lleva el mismo nombre  la colocan en la tipología de una casa de campo racional o de un taller de una casa residencial clásica, pues su estructura permite que se construya tanto en campo como en ciudad y sea doblemente funcional. 
  
http://culturacolectiva.com/wp-content/uploads/2014/01/arquitectura.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/loft.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/alemania.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/construcciones-de-hormigon.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/House-Unimog-by-Fabian-Evers-Architecture.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/banos-de-hormigon.jpg