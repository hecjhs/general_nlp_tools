Rodrigo Curiel Jirafa

 
“El artista radica en la forma en la que vive él la realidad”
Para entender mí visión, éste diálogo y éste trabajo creo que primero hay que entender que mí profesión, la de psicólogo, ha sido mi guía y vehículo, como la forma de ver las cosas y en lo que hago. También nace de ahí Jirafa, mi firma, la cual nació después de un trabajo analítico de una serie de sueños. La introspección, el trabajo interno y el auto-conocimiento es lo que promuevo con mí trabajo.
 
 
 
 
  
 
 
Mi trabajo plástico va desde fotografía, pintura, también mi exploración va a la música y video.
 
  
 
En la fotografía, más que la creación de material fotográfico, veo este trabajo como una creación de imágenes. Y por medio del desarrollo propio de un método en el cual puedo obtener imágenes con características de pintura, sin uso de post edición digital. Es una invitación a explorar mundos abstractos, texturas, colores, y formas en las que busco que el observador encuentre estimulación por las características propias de la obra y en el haga un puente al propio mundo interno en base a al espejeo de la proyección, aparte de ser piezas visualmente atractivas.
En pintura y mural plasmo dimensiones y planos abstractos con influencia en la arquitectura y geometría. Creando también escenas y personajes cuya identidad se basa en lo animal, en máscaras, en las cuales el diálogo es una mezcla del contenido energético y emocional de las posturas humanas vs. la identidad pura y natural del mundo animal. Una forma de hacer consciencia en cuanto qué máscaras usamos y que forman parte de nosotros en la interacción tanto personal como social. Este uso de máscaras es a veces positivo y a veces negativo, nunca dejando de ser humano.
Actualmente produzco proyectos en los cuales este diálogo hable en todo lo que hago.
 
 
 
  
Yo no se si soy el indicado para decir que soy artista o no, lo único que sé es que mantengo una visión del mundo en la a veces me sorprende, me aterra, me hacer estar activo mentalmente, y en la que me fuerza a participar para crear.
 
 
 
  
Para mi el artista va más allá de qué tipo de creación estética, para mí es una forma de vida, ya que el artista tiene una llave que nadie más tiene. Es una visión, del mundo y la vida, él usa estos como una herramienta, más que ser solo un solo evento que pasa desapercibido. Es la inconformidad misma y la aceptación de la sátira y obra escénica, que es la vida.
 
 
 
 
  
WEB:  http://www.wix.com/rodrigojirafa/jirafa 
@lejirafa