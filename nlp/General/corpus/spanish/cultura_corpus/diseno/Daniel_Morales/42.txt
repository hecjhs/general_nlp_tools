Los 10 mejores tatuadores en la Ciudad de México

 El siglo XXI, con los nuevos “hipsters”, con la apertura cultural que se vive en los trabajos donde cada vez hay menos discriminación en ciertos sectores, ha permitido que los tatuajes pasen a ser parte de la vida cotidiana. Ahora hay una revalorización de la estética corporal por parte de las nuevas generaciones, quienes han permitido que los tatuajes no sean mal vistos como en el pasado.   Gracias a esto ha surgido una nueva ola de artistas. Los tatuadores ya no se limitan en sus creaciones; los gustos han cambiado y hay una aceptación en nuevas líneas, trazos y estilos. Un tatuador debe forjar su propia identidad y entregar el 100% de su trabajo al cliente, quien tendrá su firma el resto de su vida.   Considerar a los tatuadores como artistas es lo más atinado, pues es su trabajo es totalmente fruto de la creatividad, la técnica y el trabajo constante. Pasar algo de la mente al papel y después llevarlo al cuerpo es quizás uno de los trabajos artísticos más importantes, pues la obra penetra en la piel y se queda para siempre. Esta no es una metáfora artística, realmente la sangre cae para llevar a cabo obras de arte como estas. En la Ciudad de México existen muchos tatuadores que están revolucionando la ciudad con su trabajo, estos son algunos cuyo estilo llama la atención por lo únicos que pueden llegar a ser.   Christopher Peste Una foto publicada por Christopher Peste (@chrispeste) el 4 de Ago de 2015 a la(s) 2:24 PDT   
 Sus tatuajes tienen un gran trabajo en el detalle; desde las texturas a los colores, Peste logra un trabajo impecable cuando de tatuajes se trata. Lo puedes encontrar como parte del estudio Infierno Tatuajes. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Tatuadores-df-peste-payaso.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Tatuadores-df-christopher-peste.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Tatuadores-df-christopher-peste1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Tatuadores-df-peste.jpg
      Roy Lodela #anchor #sailor #tattoo #old #mobtattoostudioUna foto publicada por Roy Lodela (@roy_lodela) el 11 de May de 2015 a la(s) 3:51 PDT 
Parte de Club Jendrick, Roy Lodela es un profesional con un estilo ecléctico pero experto en el realismo. Los tatuajes son para el parte importante de las personas por lo que espera que quien se tatúe sea exigente al momento del diseño, pues es algo que tendrán por siempre.

http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-roy-lodela4.png
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-df-roy-lodela.png
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-roy-lodela-2.png
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-roy-lodela11.png

   Xian of the death Esta cabeza de toro fue para Ceci, una de mis consentidas �✨Una foto publicada por Christian Castañeda (@xianofthedeath) el 5 de Sep de 2015 a la(s) 6:07 PDT    Christian Castañeda es una artista visual y tatuadora cuya obra es realmente única. Las líneas pueden parecer simples pero son hechas con un profesionalismo que cobran un nuevo significado. Una especie de nuevo simbolismo surge de sus tatuajes en blanco y negro que al verlos querrás solicitar una cita lo más pronto posible. 

http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-xian1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/xian-tatuajes-df.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/xian-tatuajes-df31.jpg

 Te puede interesar: Los tatuadores más caros del mundo Mr. Cráneo  Uno de los mejores tatuadores de la ciudad. Su estilo es bastante tradicional, parte de la “old school” pero con un sentido fresco. Sus tatuajes suelen ser bastante grandes pero no invasivos.

http://culturacolectiva.com/wp-content/uploads/2015/09/mr-craneo-tatuajes1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/mr-craneo-tatuajes2.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/mr-craneo-tatuajes4.jpg

   Xochitl Herrera Geometría #dotwork #blackflagcrew @blackflagcrewUna foto publicada por Xochitl Herrera (@xochtattoo) el 6 de Sep de 2015 a la(s) 3:09 PDT    Xochtattoo es el nombre de su cuenta en Instagram, en la cual puedes encontrar gran parte de su trabajo que va desde lo geométrico y matemático, a lo más orgánico y natural. Un equilibrio natural entre las dos cosas podrían ser los mándalas que realiza con gran destreza.

http://culturacolectiva.com/wp-content/uploads/2015/09/2.png
http://culturacolectiva.com/wp-content/uploads/2015/09/3.png
http://culturacolectiva.com/wp-content/uploads/2015/09/1.png

   Javi Wolf   No son muchos los tatuadores que juegan con la técnica de acuarela en la ciudad, pero uno de los máximos exponentes es Javi Wolf. Los retratos animales son la opción perfecta para rendir tributo a nuestras mascotas favoritas o para tener algún animal lleno de color en nuestra piel.

http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-javi-wolf-4.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-javi-wolf-2.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-javi-wolf-3.jpg

   Fraktal #TATTOO #ink #ganeshstudio @ganeshstudioUna foto publicada por Fraktal (@fraktalstudio) el 30 de Ago de 2015 a la(s) 6:16 PDT    Parte de Ganesh studio, Fraktal representa perfectamente la nueva ola de tatuadores y los nuevos estilos que están llegando. Líneas frágiles y sutiles que apenas forman una figura o diseños detallados a la perfección que demuestran que Fraktal puede moverse en varios terrenos.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-fraktal2.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-fraktal1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-fraktal.jpg
    Babyface Killah   En Inkinc se dedican a jugar con diversos estilos, y el de Babyface Killah es uno de los que representan al estudio con tatuajes que él denomina estilo “gangsta” pero también logra que se adapten al gusto de todos. 

http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-babyface-2.png
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-babyface-1.png
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-babyface-.png

   Nass   Nass es uno de los tatuadores de Estudio 184, uno de los mejores estudios de la Ciudad de México. Experto en tatuajes de gran realismo, Nass es capaz de dejar mándalas, animales antropomorfos y diseños orientales en tu piel. 

http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-nass-.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-nass-1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-nass-2.jpg

     Irving Garcia #tattoo#bear#beartattoo#ink#inked#gallonegrocrew#flowers#artist#gallonegro#beardesing#ilustrationUna foto publicada por Tattoo Artist. (@irving_tattoo) el 25 de Ago de 2015 a la(s) 8:49 PDT    Tatuador de uno de los nuevos estudios de la ciudad. Irving reside en Gallo negro, donde sus tatuajes juegan con el blanco y negro, la acuarela o el color tradicional. Sus obras son verdaderas obras de arte que van desde lo tradicional a lo que está marcando tendencia.  http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-irving-2.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-irving-1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuajes-df-irving-.jpg