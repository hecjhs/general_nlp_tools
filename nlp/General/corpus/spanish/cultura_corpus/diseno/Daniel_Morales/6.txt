Tatuajes que nunca te arrepentirás de haberte hecho

 El Kintsugi es el arte que nos permite admirar la belleza que hay en las cicatrices. Una tradición japonesa que repara los objetos rotos con oro, haciendo de la reparación algo mucho más hermoso que lo original. Una metáfora de las cicatrices que ganamos a lo largo de la vida y que muestran que después de éstas seguimos siendo fuertes e incluso más que antes. Los tatuajes son cicatrices de las que siempre nos deberíamos sentir orgullosos. Heridas abiertas provocadas por un gusto estético, pero muchas veces, por algo mucho más importante. Hay tatuajes que nacen del corazón, que evocan a personas que ya no están, momentos específicos en nuestra vida e importantes recuerdos que no se olvidarán, pero cuya trascendencia es tal que es imposible no marcarlos en nuestra piel. 
 Muchos dicen que el primer tatuaje es algo de lo que siempre te arrepientes, pero eso no debe ser así, pues basta con encontrar algo que sabemos que siempre será nuestro. Podría ser uno de los primeros libros que leímos y transformaron nuestra vida. 
 Tal vez la película que tanto veías con tu hermano y los Stormtroopers a los que siempre se enfrentaban en sus batallas imaginarias. 
 Hay momentos que nunca olvidarás. Un encuentro, un suceso que cambió tu vida o algo completamente personal que sólo tú conoces. Bien podrías llevar la fecha y el lugar exacto para siempre estar en él. 
 Dicen que bailar es como soñar con los pies. Tal vez no es tu profesión, pero nadie se arrepiente de tatuarse su verdadera pasión. 
 Pero también puede que tu pasión sea tu profesión y puedes presumirla con orgullo. 
 Hay ciertas frases que para muchos son bonitas, poéticas y espectaculares. Hay personas cuyas vidas pueden cambiar gracias a una sola de ellas. 
 Muchas veces la mejor inspiración viene de la mejor literatura. 
 Todos sabemos que los viajes nos cambian, por eso algo representativo del lugar al que vamos puede dejar su esencia en nosotros. 
 Incluso pueden ser lugares representativos que sabes que siempre amarás por las cosas que viviste en ese viaje.   Dicen que nunca te tatúes con tu novia o pareja pues no sabes qué pueda pasar, pero la fecha en la que su hijo nació es algo que nunca se arrepentirán de compartir. 
 Hay quienes pueden pensar que es extraño, pero un gran dibujo de tus hijos puede convertirse en tu tatuaje favorito. 
 Incluso llevar a tu familia entera. 
 Los tatuajes pueden ser muchos, en gustos se rompen géneros , pero la verdad es que son pocos los que pueden permanecer con una esencia única y original sin importar cuántas veces sean replicados. Un tatuaje puede ser igual al de alguien más, pero ese significado que le damos, esa intención, es lo que hará que nunca se vea mal. 
 *** Te puede interesar: 
  Tatuajes minimalistas que todas las parejas querrán tener  
 Tatuajes absurdos que deseas en secreto