Ilustraciones que muestran lo erótico y macabro de tus cuentos infantiles favoritos

 
 ¿Dónde está la magia infantil? Todo se transforma y es difícil pensar que los cuentos que escuchábamos en nuestra infancia hayan logrado ese “felices para siempre”. El tiempo nos enseña que ese momento que llamamos “felicidad absoluta” es momentáneo, pues después siempre llega una nueva adversidad; la calma simplemente es encontrarnos en el ojo del huracán a la espera de una nueva catástrofe. Saber que finalmente Caperucita venció al lobo feroz, que Cenicienta se casó y vivió feliz por el resto de su vida es esperanzador, pero muchos saben que esa es una parte de la historia y que seguramente si la narrativa continuara, ellas vivirían un destino menos afortunado. 
   
   
   
 Chelsea Greene Lewta opina que esos personajes seguramente tuvieron fines más trágicos. La artista estadounidense mezcla lo terrorífico con lo erótico para darle vida a sus personajes, creando una perversión sexual poco común. Creció en Estados Unidos, pero su ascendencia asiática influyó fuertemente en su arte. Una mujer que al ser pequeña vivió más tiempo en sus pensamientos que en la realidad y desarrolló una estética impresionante capaz de causar distintas impresiones al mismo tiempo. 
     
  
 Víscera, huesos, cajas torácicas que muestran el interior humano siempre delicado, pero al mismo tiempo también expone las zonas erógenas que incitan al espectador. Sangre y sexo se complementan en bellos y desolados paisajes introspectivos. La psique del artista se refleja en caras sin expresión que despiden sexualidad. 
    
   
 Colores oscuros que ensombrecen al azul y verde que predomina en sus pinturas para dar un aspecto orgánico y melancólico a las escenas en las que podemos ver conejos habitando el cuerpo de lobos, mujeres semidesnudas que parecen esperar a Godot en compañía de gatos y por su puesto, cuerpos de humanos y animales acribillados por flechas mortales. 
  
  
  
 Muerte, sangre, sexo , agua, dolor y excitación, las figuras creadas a partir de la digitalización de dibujos parecen una especie de tributo a Hayao Miyazaki lleno de todo lo que una película para niños no debería tener. La relación del hombre con la naturaleza se pone a prueba, pues a pesar de que los animales parecen estar heridos, permanecen vivos sin atacar los cuerpos desnudos de las personas que ahí aparecen. 
   
   
  Conejos fornicando dentro del estómago de una mujer que parece estar muerta, ángeles sangrantes que resguardan a un hombre con un espejo que muestra a una flor (podría ser Narciso y su exagerada vanidad) y una mujer que invita a que te acerques con lujuria y pasión si eres capaz de ignorar los insectos que recorren su cuerpo. 
     Otras obras se mueven en distintos caminos, siempre incluyendo algo que invite a la reflexión. Una niña con falda y suéter que tiene un agujero en el cuerpo del que se desprenden sus vísceras y al mismo tiempo caen donas completas. Un mundo de realidad y fantasía, pero en lugar de ser un hermoso sueño, parece una gran pesadilla. 
  Una disparidad, encontrar excitante la pasividad de la muerte , la melancolía provocada por las caras tristes de los personajes puede ser perturbador, pero tal vez ese es el propósito de la artista, llevarnos por caminos contrarios para hacer chocar emociones que nos obligan a despertar ante su obra, analizar y decidir: ¿somos testigos de un brutal y macabro experimento o estamos despertando ante otra forma de estimularnos? A veces es mejor no profundizar en la respuesta. 
 *** Te puede interesar: 
 Los 15 artistas transgénero más importantes 
 Fetiches, perversión y sudor en ilustraciones eróticas 
 * Fuente: 
 Chelse Greene Lewyta