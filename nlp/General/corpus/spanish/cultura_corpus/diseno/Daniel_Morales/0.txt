Pequeños tatuajes que sólo las mujeres rebeldes se atreverán a tener

 
 En la vida existen momentos en los que las decisiones que tomamos comienzan a dejar secuelas. Nuestra forma de ser y actuar ante los demás puede complacer, pero también incomodar, perturbar y molestar. Normalmente, el segundo tipo de personas mencionadas son las que brillan en el mundo actual. Dejar atrás lo convencional para formar un criterio propio se aprecia, y aunque muchas personas caen en una trampa al querer llamar la atención siendo de esa manera, hay quienes demuestran que esa es su verdadera y única personalidad.    
   
   
  
 Hoy los jóvenes no tienen un estigma con los tatuajes. Se estima que cerca de un tercio de los considerados “millennials” posee tinta de algún tipo en su cuerpo, por lo que pensar que un tatuaje es rebelde o transgresor es como considerar al rock del siglo XXI un género revolucionario. Sin embargo, hay quienes hacen de la tinta una forma de provocación, una demostración física de la psique única, libre y sobre todo, rebelde. Y hay quienes dejan en las manos de Curt Montgomery ese trabajo. 
 “La sensualidad femenina es admirada, pero también censurada. Los tatuajes demuestran que es algo natural que no debe ser reprimido y exalta a la mujer a apropiarse de su cuerpo e identidad”. 
  
   
   
   
 El artista canadiense hace uso de las líneas sutiles para crear cuerpos eróticos que demuestren el placer sexual de la mujer. Sin tomarse demasiado en serio, dibuja esas escenas pornográficas que inundan su cabeza y poco a poco ha logrado ganar popularidad entre el circulo de tatuadores canadienses. Él no se toma demasiado en serio, pero sí lo hace con su trabajo. Para él, las líneas delgadas y el minimalismo son elementos esenciales que nunca cambiará en su obra, la cual siempre será erótica o, por lo menos, subversiva. 
 *20 tatuajes que todas las mujeres inteligentes querrán hacerse   
   
   
  
 Su método es aún más especial después de conocer su historia. Siempre con aspiraciones artísticas, pero sin encontrar el tiempo para practicar debido a la caótica vida urbana, el tatuador se enclaustró en un pueblo canadiense en el que la nieve domina la región y es casi imposible tener una vida social activa. Durante un año no hizo casi nada más que ilustrar y trabajar en bares durante unas cuantas horas. Practicaba su técnica cerca de ocho horas al día, aprendiendo poco a poco hasta que se sintió listo para tatuar. 
   
   
   
  
 Hoy promueve todo su trabajo en su página de Instagram, de la cual saca a todos sus clientes; ellos pueden opinar acerca de lo que quieren, pero al final es Curt quien decide la estética del dibujo. La sexualidad es el principal elemento y muchas personas portan orgullosas a una mujer dando sexo oral, eyaculación femenina o unas piernas abiertas que invitan al espectador a no apartar la mirada. 
   
   
   
 La sensualidad femenina es admirada, pero también censurada. Los tatuajes demuestran que es algo natural que no debe ser reprimido y exalta a la mujer a apropiarse de su cuerpo e identidad. Montgomery describe su trabajo como algo divertido, pero el trasfondo transgresor lo convierte en un artista con una voz, con un mensaje y con un público que demuestra que incluso la línea más simple puede ser una protesta contra todo lo malo que hay en la sociedad. 
 – Si deseas más ideas para tatuajes que demuestren un lado rebelde y erótico, te recomendamos el trabajo de Petites Luxures , aunque si lo que deseas son tatuajes pequeños con grandes significados, puedes revisarlos aquí .