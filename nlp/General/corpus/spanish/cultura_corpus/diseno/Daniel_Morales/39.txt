Tatuadores que debes seguir en Instagram

 Es innegable que el arte y la tecnología van de la mano. La creación artística, creada de forma análoga, digital o metafísica, se clava en la mente del creador y es perfeccionada hasta que está completamente terminada. Sin embargo, muchas veces hay arte inexistente a los ojos del mundo porque no obtiene la difusión necesaria. Es ahí donde la tecnología, traducida en redes sociales, hace que estas obras viajen espacialmente hasta los rincones más recónditos de la tierra y cualquiera tenga la oportunidad de acceder a ellas. 
  Instagram ha transformado el mundo, no solo la comida y las selfies han entrado en el mundo de la fotografía, esta herramienta ha permitido a artistas de todo el mundo relacionarse, expandir su arte y darse a conocer local y globalmente, y el arte del tatuaje no está exenta de esta actividad. Existen muchos tatuadores que muestran orgullosamente sus trabajos, y de todos aquellos que utilizan la red social, te compartimos una selección de artistas activos que logran darle un toque único a su trabajo y cuyos perfiles deberían estar en tu timeline. Dr_woo_ssc ���Una foto publicada por dr_woo_ssc (@dr_woo_ssc) el 13 de Sep de 2015 a la(s) 2:47 PDT 
Residente de Los Ángeles, su trabajo está enfocado en la idea de que menos es más. Las sutiles líneas y las texturas con las que trabaja muestran el virtuosismo de uno de los mejores tatuadores de California.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-woo-dinosaurio.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-woo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-dr-woo.jpg
   –Roxx Thanks Tanya, safe trip back to London… #2spirittattoo #leica #tattoosbyroxxUna foto publicada por ROXX (@roxx_____) el 10 de Sep de 2015 a la(s) 5:53 PDT     A diferencia de Woo, los tatuajes de Roxx son bastante invasivos. Las líneas van de lo más fino a capas que cubren gran parte del cuerpo. Sus tatuajes realmente son únicos y reconocibles en muchos lugares gracias a su experiencia de tres décadas en las que ha trabajado en más de cinco países.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-roxx.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-roxx-.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-roxx-tattoo.jpg
   –Dotstolines #DotsToLines #LineArt #HealedUna foto publicada por Chaim Machlev (@dotstolines) el 6 de May de 2015 a la(s) 11:39 PDT     Desde Alemania llega Dots to lines, quien es conocido por sus grandes tatuajes en los que las líneas y las curvas expresan en la piel la personalidad de aquel que decide tatuarse. Las lineas comienzan desde puntos inesperados en los brazos o las piernas y recorren todo el cuerpo, haciendo del tatuaje un diseño que juega con la sensualidad y el erotismo.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-dotstolines.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-dotstolines-.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-dots-to-lines.jpg
   –Sasha Unisex Sherlock Cat ��#SherlockHolmesTattoo #sashaunisex #watercolortattoo #vbiproteam #cattattoo #ELEMENTARYUna foto publicada por Sasha Unisex © (@sashaunisex) el 6 de Ago de 2015 a la(s) 8:18 PDT     Quizá una de las artistas más reconocidas en el mundo del tatuaje. Con más de medio millón de seguidores en Instagram es difícil que quienes sigan la cultura del tatuaje en Internet no se hayan encontrado con su trabajo. Esta bella mujer rusa explora la naturaleza -elemento que ama sobre todas las cosas-, los elementos cósmicos. Con un personal uso de la técnica de acuarela crea tatuajes que superan el concepto de ‘belleza’.  
http://culturacolectiva.com/wp-content/uploads/2014/05/sasha13.jpg
http://culturacolectiva.com/wp-content/uploads/2014/05/sasha10.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-sasha.jpg
   –Rachelhauer Healed and fresh Miyazaki tattoos Una foto publicada por Rachel (@rachelhauer) el 29 de Ago de 2015 a la(s) 4:27 PDT     Fanática del arte de tejer, de las sillas y una trekkie a más no poder; su trabajo en el arte de la tinta sobre la piel comenzó en 2009, por lo que su estilo aún no está definido totalmente pero el blanco y negro es una constante. Grandes animales con un sombreado impresionante hacen que cualquiera quiera viajar a Nueva York para solicitar una cita con ella.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-rachelhauer.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-rachel-hauer.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-rachelhauer-.jpg
   –Lisaorth Anatomical heart for Jennifer from Seattle. Artwork and photo © 2015 Lisa Orth.Una foto publicada por Lisa Orth (@lisaorth) el 25 de Jul de 2015 a la(s) 2:38 PDT     Parece que el blanco y negro están de moda, pero no para Lisa Orth. Esta artista, que ahora reside en Los Ángeles, tiene años perfeccionando su técnica para poder ser considerada una de las mejores. Desde sus diseños originales hasta sus tributos a artistas como William Blake, las cortas líneas de Orth son realmente una obra de arte.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-lisaorth.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-lisaorth-blake.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-lisa-orth.jpg
   –Benhakmeen shiny new stag on stephanie. she’s tough, gets cool tats and even pretended my jokes were funny. what more could one ask for in a client? thanks!Una foto publicada por ben merrell (@benhakmeen) el 15 de Abr de 2015 a la(s) 12:01 PDT     Uno de los artistas que continúan por una senda tradicional pero saben innovar en la forma y figura. Diseños clásicos como los insectos, las calaveras y las flores son parte de su trabajo que puede considerarse de gran formato.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-benhakmeen.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-benhakmeen-.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-benhakmeen-1.jpg
   –Gpecarlota Rock lobster �� – #guadalupecarlota #gpecarlota #blacktattoos #black #mexicocity #mexico #df #mexicodf #cdmx #linework #blackistheonlycolor #blacktattooart #tatuadoresmexicanos #linework #etch #etchingtattoo #etching #line #linework #lobster #lobstertattooUna foto publicada por Guadalupe Carlota (@gpecarlota) el 19 de Ago de 2015 a la(s) 11:50 PDT     Según el propio artista, su relación con la ilustración estructurada a partir del protagonismo de la línea y sus diferentes valores se encaminó al arte del tatuaje. GpeCarlota está interesado en el diseño de la vieja escuela y en la iconografía religiosa.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-gp-carlota.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-guadalupe-carlota.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-gpcarlota.jpg
   –Xianogthedeath Irwin, muchas gracias por pedir un monstruito mágico marino ��✨Una foto publicada por Christian Castañeda (@xianofthedeath) el 13 de Sep de 2015 a la(s) 8:23 PDT     Christian Castañeda es una artista visual y tatuadora cuya obra es realmente única. Las líneas pueden parecer simples pero son hechas con un profesionalismo que cobran un nuevo significado. Una especie de nuevo simbolismo surge de sus tatuajes en blanco y negro que al verlos querrás solicitar una cita lo más pronto posible.   
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-xian.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-xianofthedeath.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/xian-tatuajes-df.jpg
   –Bangbangnyc @turan.art @turan.art @turan.artUna foto publicada por Bang Bang (@bangbangnyc) el 12 de Sep de 2015 a la(s) 9:46 PDT     Sus tatuajes son famosos gracias a su impresionante técnica, pero también debido a las persona que han pasado por su silla: Cara Delevingne, Justin Bieber, Rihanna y más tienen su arte en la piel. Por nombres como los anteriores, Bangbangncy se ha convertido en uno de los tatuadores más cotizados de Nueva York.  
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-cara.jpeg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-bangbang.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/tatuadores-instagram-bangbangnyc.jpg 
 *** Te puede interesar: Los símbolos más utilizados en los tatuajes – 
 Referencias: GQ , Huffington Post