Los peores tatuajes en el mundo de la música

 Por naturaleza, cada ser humano tiene la necesidad de dejar una huella en el mundo y en eso, sea lo que sea, para lo que tiene talento. Los escritores nos han dejado libros, artistas cuadros inolvidables, científicos grandes avances que han favorecido a la humanidad y músicos canciones que van dejando su marca de generación en generación. Claro que a estos últimos también los recordamos por otros tantos detalles como su personalidad e imagen, tal parece que los músicos y los tatuajes van de la mano, todo el mundo lo sabe. Por desgracia, ellos tampoco están exentos de ataques de locura que resultan en lamentables creaciones de tinta sobre su piel. Por ejemplo, todo ellos: Tyson Ritter tiene una toma de corriente eléctrica en el brazo y ¿por qué dejarlo allí? tiene también un rodapie por debajo del brazo.  
 
 
 “No te tiene que gustar T-Pain ” dice el tatuaje del cantante de auto-tune.  
 
 Rick Ross posteó en su cuenta de Instagram un nuevo tatuaje en a barbilla que reza “Rinch Forever”, aunque tal vez él sea una forma de mostrar su amor hacia las películas de Richard Curtis.  
 
 Riff Raff tiene la marca que representa a toda la generación de los 90: el logotipo de MTV tatuado en el cuello y Bart Simpson impreso en el pecho.  
 
 La siempre provocadora rapera Missy Elliott , tiene a Bugs Bunny tatuado en su pierna izquierda. Y eso es todo amigos… 
 
  
 
 Jon Bon Jovi tiene el símbolo de Superman tatuado en el hombro izquierdo; tal vez porque porque “Living On A Prayer” es su kryptonita.  
 
 
 Como el legendario Andre 300 cantó una vez: “what’s cooler than being cold? Ice cold”, fue lo que recordamos cuando vimos el cono de helado en la cara de Gucci Mane .  
 
 Eva Ruff Ryders tiene las huellas de un tigre en el pecho sin razón alguna. No por lo menos una que conozcamos.  
 
 Entre los tatuajes de Lily Allen los más extraños son una smiley face, un ácido, la cara de Homero Simpson y un perro, todos ellos en la muñeca.  
 
 El rapero D12 tiene rostro de Nicki Minaj tatuado en el brazo. Eso fue lo que hizo para alguna vez romper el hielo con la reina de Queens.