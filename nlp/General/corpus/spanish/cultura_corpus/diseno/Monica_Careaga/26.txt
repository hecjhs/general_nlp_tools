Manta Underwater Room, una habitación submarina

 En medio de una isla, un complejo se erige rodeado del Océano Índico; es una habitación a cuatro metros bajo el agua en la que el respiro en medio de la soledad marina cuesta mil 500 dólares la noche. 
  En la isla de Pemba, en Zanzíbar, al Este del continente africano, se ubica El Manta Resort, un hotel con 17 elegantes habitaciones, además de villas y salas de grandes jardines, rodeado de arena blanca con vistas alucinantes hacia las aguas del Índico. Pero es la habitación flotante, fabricada en madera, la sensación de los visitantes. Son tres niveles que embisten el océano y las aguas chocan de arriba a abajo con la estructura. 
  Esta popular habitación, Manta Underwater Room, es un diseño de Mikael Genberg, un creativo sueco quien ya antes había proyectado esta idea arquitectónica en un lago de Estocolmo en el año 2000. El dormitorio es una isla-estancia en la que los visitantes pueden ver pasar la vida marina mientras gozan de las comodidades de un resort. 
  
http://culturacolectiva.com/wp-content/uploads/2013/11/The-Manta-Resort-isla.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/The-Manta-Resort.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/The-Manta-Resort-hotel.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/mantaunderwaterroom-hotel.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/mantaunderwaterroom4.jpg
  Los huéspedes pueden elegir entre tomar el sol en la cubierta, permanecer en el espacio de aterrizaje a nivel del mar, con una sala de estar y un baño, a observar la panorámica del océano, o descender a la habitación con la vista de los arrecifes de coral. Este nivel submarino se ilumina con focos subacuáticos en cada ventana, lo que atrae un sinfín de criaturas. 
 http://culturacolectiva.com/wp-content/uploads/2013/11/mantaunderwaterroom10.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/mantaunderwaterroom5.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/mantaunderwaterroom11.jpg