Andrew Miller, “el espíritu de las marcas”

 ¿Cuántas marcas reconoces sólo por sus colores, el tamaño del envase o por su logotipo? La identidad de una marca supera la inclusión de su nombre por escrito, se podría decir, es el punto máximo del entendimiento entre una entidad y su consumidor. Es el espíritu de la marca. 
  A lo largo de 100 días el artista Andrew Miller regresa a los objetos a su forma primitiva: elimina la cara de la marca que los identifica, los distrae de la mente del consumidor al desasociarlos de su contexto, les arrebata su identidad y los deja en blanco para poner a prueba la arraigada relación que guardamos con lo que consumimos. 
   Brand Spirit es el proyecto de Miller en el que día a día, durante más de tres meses, pintó un objeto de color blanco para despojarlo del branding que lo hace “ser”. El artista no emplea ningún tipo de software para la edición de las imágenes; utiliza pintura blanca para crear objetos  brandless y designless . 
   La idea nació de evidenciar el engagement que el consumidor guarda con ciertas marcas. Aunque Miller no lo señala así, puede ser, también, una crítica a la sociedad consumista que deposita su confianza en el valor de la marca más allá de la calidad en el producto. 
   El proyecto se presentó en Swiss Miss, PSFK, Gizmodo, Adweek, The Next Web, Trendland, Arte Huffington Post, Huffington Post Comedia, Hypebeast, Business Insider, Fast Design Company y la Storyboard Tumblr. Visita Brand Spirit aquí: brandspirit.tumblr.com http://culturacolectiva.com/wp-content/uploads/2013/10/mcdonalds.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/casio.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/kindle.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/marlboro.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/mr-potato-head.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/panditas.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/underwood.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/view-master.jpg