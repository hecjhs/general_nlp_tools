Joseba Elorza, el collage es perverso

 El collage es de naturaleza perversa. El que lo hace desbarata las realidades y las mezcla según sus deseos más negros hasta el punto de excitarse con la sola contemplación. El collage no exige la prudencia de la mano creadora, tampoco la exaltación de la imaginación. El hombre se limita a confrontar el tiempo, a las personas, los escenarios; altera la materia y predice, en una suerte de vidente, lo que sucederá. Al final, todo es posible en el collage: un jugueteo visual de la realidad, la propia y la del mundo. 
  Bajo esta idea de las posibilidades infinitas del collage, Joseba Elorza hace su propuesta con la imagen digital. Para el español, la técnica es fuente de un conocimiento más ácido y visionario: descontextualiza “la verdad” de la imagen y crea una nueva a partir de mezclar lo que una y otra exponen. Para Elorza el collage es la oportunidad de recorrer el tiempo y la historia que éste genera. 
  Joseba Elorza es un técnico de sonido quien un día conoció las bondades del Photoshop, desde entonces cayó en cuenta que sabía hilar con humor la tecnología, la ciencia ficción y los episodios históricos en una imagen. 
  Eligió el collage como su voz y su personalidad ermitaña. Este camino no exige una habilidad con el dibujo y permite trabajar solo, por tanto, la persona se ve obligada a confrontar el papel en blanco y crear una propia identidad sin un referente directo. Joseba sabe que necesariamente el entorno configura el estilo, pero prefiere desarrollar las ideas en solitario, sin más vicios que los necesarios. 
  Hacer collage es decidir quién va a estar con quién, “y ellos no pueden hacer nada. Es muy perverso todo”. 
 http://culturacolectiva.com/wp-content/uploads/2013/11/collage-digital.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/fotos-Joseba-Elorza.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/jesebal.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza-arte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza-collage-digital.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza-collage.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza-español.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza-foto.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/Joseba-Elorza.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/MiraRuido_flyalone.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/MiraRuido_observatory.jpg