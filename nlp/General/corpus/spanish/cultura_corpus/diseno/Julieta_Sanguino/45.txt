Tatuajes de “La naranja mecánica” para recordar a Kubrick

 Un melómano, sociópata, que ama la leche y practica la ultraviolencia con sus amigos llamado Alex DeLarge; se divierte matando gente y haciendo crímenes a diestra y siniestra. Sus amigos, llamados drugos,  término que significa colega en ruso, siguen su paso, no les importa lo que pase pues simplemente buscan divertirse y saciar su ira cotidiana. Cuando llega al punto máximo de la violencia, la policía lo captura y busca reformarlo a través de la psicología conductista, obligándolo a mirar videos de guerra para cambiar sus actitudes sociópatas. 
 Esta película dirigida por Stanley Kubrick se convirtió en una crítica social a las “curas” psiquiátricas y psicológicas para al pandillerismo juvenil y la adaptación de las teorías conductistas al comportamiento humano. Ludwig van Beethoven es el músico principal de la película, pues el protagonista se inspira en su música para saciar su sed de violencia, un elemento desmedido y cotidiano, complementado con el sexo como el mayor placer. Esta cinta se ha convertido en una referencia cultural muy importante, el protagonista se ha hecho un símbolo de desorden juvenil y una bandera para la adolescencia rebelde y aquellos trajes que portaban cada día para hacer sus fechorías un traje recurrente en festejos donde el disfraz es obligatorio. La irreverencia de la película es ahora un estandarte para muchos y se la tatúan por la gran marca interior que dejó en ellos. Si eres uno de esos a los que la película les hizo reflexionar profundamente, te presentamos algunos diseños que seguro te encantarán. La música y la clásica escena de Singing in the rain, se presentan en este hermoso tatuaje que dice más que los que tienen muchos detalles.  Un tatuaje bastante clásico enmarcado en un triángulo invertido y con los rasgos característicos de Alex. 
   Otra versión del rostro del protagonista de la cinta.   Su bastón, el sombrero inglés y la botella de leche son las características de la banda de chicos ultraviolentos que causan pavor en las calles inglesas. 
   El rostro del protagonista puede verse bien en el lugar indicado y con la técnica adecuada. 
 Un tatuaje de manga para aquellos que aman la cinta 
 
 La esencia de la película en un brazo. 
 Sutil y simple, este tatuaje puede quedar bien en cualquier ocasión. 
 La silueta de la pandilla puede adornar tu cuello.   Un simple sombrero y el adorno de pestaña harán saber a todos de inmediato de quien se trata. 
 Si te gustan los colores puedes probar con esta opción. 
 Una buena frase de la película puede acompañar a tu tatuaje. 
   O simplemente ponerla en tu pie. 
 El prisionero 665321, Alex DeLarge  Un tatuaje con pocos detalles puede decir más que cualquier otro. 
 A las niñas también les gusta la película… y los gatos 
 Recrear la cinta con pájaros también puede traer admiradores. 
 Un detalle caricaturesco para representar al protagonista. 
 Dos ojos que demuestren el antes y el después del protagonista.  Un protagonista animado similar a los dibujos manga. *** 
 Quizá te pueda interesar: Diseños de tatuajes inspirados en historietas japonesas