Cosas que haces y provocan que se me revuelvan los pezones

 Es por los vestidos escotados por donde se evapora, poco a poco, el pudor de las mujeres. Alexandre Dumas 
 Si de cosas antinaturales se trata, hablar del tabú y la rasquiña que provoca siquiera mencionar la palabra “pezón”, es una de las más grandes. Nuestro cuerpo nos da miedo, nos intimida, no sabemos cómo mostrar al público esa riqueza corporal sin que la gente se asombre y nos vea con el tan esperado morbo que desata esta parte por demás natural. 
  En la playa hay que cubrirlos. Modelos y celebridades se disponen a ir a conferencias y cenas de gala con los vestidos más escotados, es válido mostrar todo el seno menos una parte singular, la punta chispeante que se hace rígida al excitarnos y que provee la supervivencia de nuestra especie. 
  
 En las redes sociales están prohibidos y nosotras también colaboramos por sacarlos de la faz del conocimiento ajeno. En películas subidas de tono, eróticas y pornográficas hacen énfasis en ellos. No hay nada de malo cuando los hombres los muestran con destreza en la playa o al salir de bañarse, pero nosotras debemos cuidarlos con pudor para que nadie nos critique por dejarlos al aire libre, ese aire que provocará su excitación y dureza, el mismo aire capaz de provocarnos un cosquilleo nervioso. 
  
 Cada uno tiene un color diferente, una textura distinta y el tamaño ideal para darle identidad al sujeto que lo posee. Son naturales e importantes. Todos los tenemos y aún así nos avergüenzan. 
  
 ¿Qué pasaría si gritáramos pezones igual que Summer grita “pene” a la menor provocación? No es como decir fuerte “te amo”, dos palabras que provocan las miradas más tiernas, ni “pastiche”, por la que seguramente todos nos tomarían de a locos. Pezón, palabra de miradas incómodas, de recelos, de anhelos secretos, la palabra que le da nombre al Instagram “Pezones revueltos”, que nos muestra la realidad femenina y el oscuro mundo que nadie ve. 
  
 No todas las ilustraciones hacen ese guiño evidente a la desnudez del torso femenino, pero sí a ser mujer y lo que eso implica. Una mujer que demuestra que no tienen que existir los brasieres, que podemos ser peligrosas y que las curvas no matan. Esa que nos representa a todas: llena de honestidad, libre de tapujos y restricciones, a la que no le importan las opiniones ajenas y a quien le molesta más no aceptar los estereotipos que cumplirlos a la perfección… esa mujer que todas soñamos con ser pero que nos da miedo intentar sacarla a la luz. 
  
  El sarcasmo del poder femenino, la brutalidad de esa honestidad que demostramos y que a nadie le parece adecuada, nunca conformarnos, nunca ser el patrón de niña hollywoodense a la que sólo le importa el color rosa, qué tan bien luzca su cabello y conocer al príncipe azul, sin demostrarnos la autenticidad de un amor al estudio, a los conciertos, al arte o cualquier otra cosa que deje de ser superficial y nos apasione verdaderamente. 
  
 Porque también nos importa un amor loco que comparta nuestro sentir y una pareja que sea peligrosa, igual que nosotras. María Bueno tiene 20 años y a esa corta edad sabe que los pezones al aire libre son lo mejor que puede existir en la vida. Sus ilustraciones cuentan principalmente historias de amor y desamor, situaciones cotidianas que vivimos a diario e historias de aquellas que tienen una personalidad fuerte y aguerrida. 
  
 Empezó ilustrando para ella porque con estudios de Diseño y un gusto por dibujar en las noches, su vida se convirtió en arte. Tiene una capacidad innata para ignorar. A su corta edad ya tuvo su primera exposición en España y una aparición en la revista “INFASHION”. Nació en España y estudia Diseño Gráfico. 
      
     
                     
        ¿Cuantas veces hemos sido catalogadas como las provocadoras de todo? Eva, que se comió la manzana, Salomé, quien nunca perdonó el desprecio de Juan el Bautista, Dalilah y el cabello de Sansón, y otras más que lo único que provocan es desatar guerras y conflictos . Te presentamos algunas ilustraciones que demuestran la fatalidad de las curvas femeninas. *** Te puede interesar: 
 El erotismo, las fantasías y la libertad de la sensualidad femenina en ilustracionesLa sutil perversión de la sexualidad femenina en ilustraciones  *  Conoce más de su trabajo aquí