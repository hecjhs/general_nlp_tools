Objetos de “El principito” para sacar a nuestro niño interno

 El Principito es uno de los libros más populares entre niños y adultos; quienes lo leen, descubren un nuevo mundo de inocencia y amor que ya habían olvidado. Porque como lo dice el pequeño príncipe de un asteroide lejano, “l as personas mayores nunca pueden comprender algo por sí solas y es muy aburrido para los niños tener que darles una y otra vez explicaciones”. Un pequeño príncipe conoce a un piloto que se encuentra perdido a la mitad del desierto del Sahara, al platicar, los dos comienzan a aprender cosas del otro, y las críticas al mundo adulto nos hacen reflexionar sobre la poca imaginación y vivacidad que tenemos.Siempre preocupados por la razón, por lo que los otros dirán y en explicarlo todo, pensamos que esas cosas son las que realmente importan en la vida aunque vivamos en una mentira por siempre. De este modo, El Principito no es sólo un libro infantil como se le cataloga, sino una reflexión profunda de la vida y la naturaleza humana. “Lo esencial es invisible a los ojos”, como le dice el zorro al principito y libro de Antoine de Saint-Exupéry debe leerse con calma y profundidad para entender realmente su significado y crítica a la sociedad. La mayoría que hemos leído El Principito e intentamos seguir sus consejos de vida como si fueran las reglas básicas que nos permitan hacer más simple y feliz nuestra existencia. Lo amamos con fervor, intentamos guiarnos por el escritor Antoine de Saint-Exupéry para dejar a un lado la vida adulta y vivir un poco más como niños, sin prejuicios ni la rigidez típica de los adultos. Es por eso que El Principito se ha colocado como uno de los libros más populares hasta nuestros días. Miles de artículos se hacen para rendirle tributo, para darle a las personas un poco de la esencia del gran libro. Que ellos puedan sentirse parte de éste y así armarse de valor para hacer que su vida sea mejor. 
 Te presentamos algunos de esos objetos que seguramente mejorarán tu ánimo. 
 Un hermoso reloj para colocarlo junto a tus libros y no perder el tiempo en cosas que no valen la pena y para que te esfuerces más en alcanzar tus sueños. 
  
 Una cajita grabada con tu personaje favorito para que puedas guardar tus joyas o lo que desees. 
  
 Si necesitas ir a un compromiso formal, no olvides a tu niño interior con estas mancuernas que adornarán a la perfección tus camisas.   
 Para esos días casuales, puedes utilizar una playera con las enseñanzas de Antoine de Saint-Exupéry.     
 Despega tus pies del suelo con unos tenis decorados con esta emblemática historia.    Adorna tu muñeca con esta pulsera con un pequeño asteroide y la figura de El Principito.    
 Haz que tu hogar nunca pierda su magia con esta elegante silla inspirada en el libro. 
   
 Guarda tu información más importante en esta funda para tu laptop. 
   
 Luce al máximo tus dedos con este anillo cortado con láser e inspirado en el personaje principal del relato.   
 O con este otro mucho más discreto y elegante. 
  
 Adorna tus pies con estos peculiares zapatos de tacón que se ven mágicos. 
   
 Coloca tu joyería en este accesorio de piedra que no es un sombrero, sino una serpiente que acaba de comer a un elefante. 
   
 El asteroide B-612 puede permanecer en tu cuello y lo único que debes hacer por él, es cuidarlo como si de ti dependiera su existencia.   
 También el pequeño zorro puede estar contigo por el resto de tu vida y así, siempre tendrás un amigo cerca. 
   
 Para esos días más formales, vístete con una camisa del principito y sorprende a todos los invitados de tu reunión. 
   
 Si quieres adornar tu pared con un reloj original, hazlo con una frase de tu historia favorita. 
   O este otro con el argumento central de la historia. 
  
 Tal vez el regalo más original de todos es este de semillas de baobab, en el que ese árbol enorme que crece en el planeta del principito, puede crecer a tu lado. “Los baobabs antes de crecer, comienzan siendo pequeños”. 
   
 Este prendedor además de discreto, dará a tus atuendos un toque de originalidad que casi todos entenderán y amarán. 
    Regala una taza para desearle buenas noches a la persona que más amas. 
   
 O un case minimalista que tenga la esencia de una de las historias más conmovedoras.   Puedes decorar tu hogar con este mueble inspirado en la serpiente de El Principito.   Apunta todos tus recuerdos en una libreta que te permita soñar y descubrir otros planetas.  
 Un collar un poco más elegante que encantará a toda mujer. 
  
 Un cojín para desearle mágicos sueños. 
  
 O una taza que te deje un gran sabor de boca. 
   *** 
 Te puede interesar: Los mejores tatuajes inspirados en El Principito