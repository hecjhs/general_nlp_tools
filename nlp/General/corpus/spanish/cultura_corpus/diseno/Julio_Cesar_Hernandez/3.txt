La vida cotidiana apesta: 15 ilustraciones de la vida adulta

 La vida cotidiana es eso que a penas notamos que sucede. En el vivir, existe una creencia que sólo los momentos trascendentes en los que sentimos encontrar un ápice de felicidad merecen ser recordados. La vida también es eso que pasa en la cotidianidad, que ocurre en el aburrimiento y el fastidio para así  sopesar aquello que nos congratula. La vida sin espacios, donde la felicidad impera, sería sólo un tren en el cual no nos damos cuenta de lo que ocurre afuera y como si viajásemos a toda velocidad, ésta no nos permitiera apreciar, odiar y reírnos de las cosas elementales, cosas tan odiosas que podríamos denominar como problemas modernos. 
 El tardar varios minutos en desenrollar el cable de tus audífonos, el esperar con ansia el viernes y remitirnos a esa niña de 16 años llamada Brenda Spencer, que por odiar los lunes disparó a una escuela con un calibre .22 argumentando “No me gustan los lunes” – odiar los lunes parece ser muy sano, siempre y cuando no dispares a una escuela -, o recordar lo bien que se siente aún siendo adulto el ir sentado en el primer asiento de un autobús de dos pisos, viendo a la gente pasar; son pequeñas cosas que cuando se hacen conscientes comienzan a existir. 
 A veces la cotidianidad abruma, pero para que exista lo relevante, debe existir una capa por la cual sobresalir. 
  
 El escritor, Chaz Hutton, sacó hace unos días una serie de dibujos realizados en pequeños trozos de papel adhesivos, donde de manera irónica y sencilla nos muestra representaciones gráficas de problemas que surgen cuando una persona llegar a la edad adulta, cuando ciertas repeticiones de patrones nos unen por clichés tan absurdos como decir “Estoy muy cansado para ir con mis amigos”. Lo que el autor nos recuerda es que estas nimiedades pueden de vez en cuando alegrarnos un poco, las buenas y las malas. 
 1.- Tener todo arrumbado en tu cuarto y sólo tener la voluntad de hacerlo a un lado. 
  
 2.- Lo molesto que es tratar de conectar un cable detrás del escritorio. 
  
 3.- Muchas personas aman más a su mascota que los demás. 
  
 4.- La vida es eso que pasa entre el domingo y el viernes. 
  
 5.- Esto siempre da emoción. 
  
 6.- Cuando te dan instrucciones de cómo llegar a tu primer entrevista de trabajo, te pierdes y crees que vas a morir. 
  
 7.- Darle de comer a tu perro, siempre te lo agradecerá a pesar de que se te olvide. 
  
 8.- ¿Procrastinación? ¿Qué es eso? Luego lo busco… 
  
 9.- ¿Dinero y tiempo para viajar? 
  
 10.- Cuando te vas a vivir solo y debes elegir entre muebles o una mesa de cantina. 
  
 11.- Las prioridades cambian y la alacena no se llena sola. 
  
 12.- El error de comparar tu vida con la de los demás. 
  
 13.- Perdido en un festival de música. 
  
 14.- El eterno retorno de los fines de semana, “No lo vuelvo a hacer”. 
  
 15.- Debiste haber aprendido de tu padre cómo ensamblar un mueble. 
  
 Algunas de estas cosas son tan comunes que debería de darnos risa lo necios que podemos llegar a ser al llegar a la edad en la que ser adulto es una necesidad. El golpearte el dedo pequeño del pie contra la pata de la cama es algo con lo que tendrás que vivir toda tu vida. 
 Hay que desconfiar de lo perfecto… 
 Referencia: 15 brutally honest illustrations perfectly sum up adulthood. *** 
 Te puede interesar: Cosas que sólo aprendes cuando eres adulto.