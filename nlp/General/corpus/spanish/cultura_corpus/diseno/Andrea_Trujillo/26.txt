20 diseños para hacer las mejores cartas de amor

 
 A veces las palabras no dan para expresar lo que sentimos por una persona. Se atoran en la garganta, aferrándose a las paredes y negándose a salir, y cuando estamos a punto de decirlas, nos embiste sin anuncio el vacío y sentimos ese cosquilleo maldito en el estómago,  las mejillas se enrojecen y el cuerpo se entorpece . Le miramos alejarse y es entonces cuando los ojos se diluyen y nos derretimos como hielo al sol  ¿Qué fue lo que pasó? Ya teníamos todo un discurso preparado y habíamos imaginado hasta el peor de los escenarios donde salíamos victoriosos en caso de ser rechazados. Sólo esa persona es capaz de cortar de tajo nuestra capacidad de habla y de poner en pausa nuestros pensamientos para escabullirse en ellos a cada momento. 
  Después de varios intentos fallidos, el tesón permanece y seguimos buscando la forma de hacerle saber a esa persona lo que estamos sintiendo porque de otra manera el corazón seguirá aumentando su tamaño hasta estallar en nuestro pecho. Decidimos recurrir al método infalible: la escritura. En el papel dejamos un pedacito de nuestro ser, esperando que cuando la carta sea leída, nosotros en forma de letras quedemos impresos en la memoria de nuestro enamorado. Si tú también estás pensando en escribir una carta, tenemos para ti algunos diseños en los que podrás inspirarte, las palabras corren por tu cuenta. 
 
 Una carta miniatura hecha con una caja de cerillos   
 El papel texturizado es un toque sencillo pero elegante 
   
 Dos diseños hechos con hilo, botones, listones y otras aplicaciones 
     
 Envíale todo tu amor hecho con corazones de tela o papel texturizado como en estas dos tarjetas 
    
 En cada sobre puedes meter notitas miniatura. Mira cómo hacer el sobre aquí 
   
 O puedes usar solamente un sobre que contenga una carta de amor 
  Y si lo prefieres, usa los sobres sólo como motivo decorativo 
   
 Un diseño lindo y sencillo que puedes hacer de dos maneras diferentes 
    Unos botones para ropa le darán personalidad a tu tarjeta 
   Para los más sencillos, un diseño abstracto es la mejor opción   
 Haz tarjetas tridimensionales de San Valentín con hojas de papel 
     
 Añade un calendario a tu tarjeta para conmemorar la fecha 
  También puedes utilizar las hojas de tus viejos cuadernos para hacer una linda tarjeta 
   
 Demuéstrale qué tan rápido late tu corazón con un poco de hilo o estambre 
   
 Haz un diseño original sobre yute en lugar de papel 
   
 ¿Recuerdas el sabor de esos caramelos de azúcar? Ahora puedes hacerlos tú misma con papel 
  
 Dale personalidad a tu tarjeta con algunos sellos postales 
    
 La tarjeta perfecta para las relaciones a distancia 
  
 Dale uso al periódico que tienes arrumbado 
  
 Enciende su vida con una tarjeta hecha con velas 
   
 *** 
 Te puede interesar: 
 La carta de amor más hermosa del mundo que jamás se volverá a escribir 
 Las mejores ideas de separadores de libros que puedes hacer tú mismo