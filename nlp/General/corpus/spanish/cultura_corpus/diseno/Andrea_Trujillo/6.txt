Tatuajes que los amantes de Amy Winehouse morirán por tener

 
 Amy Winehouse no sólo trascendió por su increíble e indudable talento en la música. Traspasó las fronteras de los géneros que solamente eran entonados por voces que pertenecían a mujeres voluptuosas de azúcar morena; ella podía cantar, vaya que podía, y su voz le dio una nueva chispa al soul, jazz y R&B . Sin embargo, una vez que atrajo los reflectores y la atención de los medios, era natural que tuviera que construir una imagen igual de revolucionaria que su música. Amy rompió los paradigmas de la moda que en esos momentos reinaba; se deshizo de la imagen pulcra y femenina que otras artistas adoptaron y retomó elementos del jazz neoyorquino. Su maquillaje sucio y osado, cabello alborotado y voluminoso, y un look de pin-up vintage  fueron los primeros pasos, pero faltaba algo: los tatuajes. Todos estos elementos convirtieron a Winehouse en un ícono —para nada tradicional— de la moda, y sus tatuajes se hicieron un sello  característico de su persona. En total, Amy Winehouse tenía catorce tatuajes, por lo menos a la vista, y la mayoría de ellos estaban hechos con un estilo old school . 
 Si eres un verdadero fanático, probablemente ya los conozcas pero, ¿sabes qué significa cada uno? Hicimos una compilación de tatuajes similares a los que tenía Amy en diferentes partes de su cuerpo y te contaremos un poco acerca de ellos. 
  
 Águilas 
    En la parte más alta de su espalda, Amy llevaba un tatuaje que mezclaba un águila y el jeroglífico egipcio ankh. 
 
 Anclas 
        En el abdomen, Amy llevaba un ancla con una leyenda que decía “Hello sailor”. 
 Betty Boop 
       Fue el primer tatuaje que se hizo Winehouse fue la caricatura de Betty Boop en la espalda baja. 
 Daddy’s Girl 
     En uno de sus hombros llevaba la leyenda “Daddy’s Girl”, un tatuaje para honrar a su padre. 
 
 Plumas nativoamericanas  
     En el antebrazo izquierdo, Amy llevaba una pluma nativoamericana, que también fue de sus primeros tatuajes. 
 
 Herraduras 
    Amy se tatuó una herradura de colores en su brazo izquierdo, justo en medio del que se lee “Daddy’s girl” 
 
 Relámpago 
   Winehouse tenía un tatuaje en forma de relámpago en la parte interior de su muñeca derecha 
 Pin-ups vintage   Pin-ups por todos lados. Amy las llevaba en sus dos brazos; dos en el derecho y una en el izquierdo. 
 Pequeños corazones 
     En su hombro derecho y en el dedo anular donde llevaba su anillo de casada, Amy también portaba pequeños tatuajes en forma de corazón. 
 Además de estos, también tenía otros tatuajes. En su brazo derecho llevaba el nombre de su abuela “Cynthia”; en el pecho, un bolsillo de camisa y el nombre de su entonces esposo Blake; también tenía un pájaro con notas musicales en el que se leía “Never clip my wings” ¿Conoces algún otro tatuaje de Amy Winehouse que no hayamos mencionado? 
 *** Te puede interesar: 
 La triste historia detrás de las canciones de Amy Winehouse Fotos de Amy Winehouse que sólo los verdaderos fans conocen