10 ideas para crear la cocina de tus sueños

 No cabe duda que la cocina es el corazón de un hogar. Es un lugar clave de reunión donde los mejores eventos suceden, ya que en algún momento todos terminamos refugiándonos ahí. También, es donde experimentamos, aprendemos, u otros que ya dominan el arte culinario , crean manjares únicos; pero también es un sitio en el que no ponemos demasiada atención a la hora de diseñarlo o decorarlo, pensamos que, como es un lugar de uso “rudo” de todas formas terminará ensuciándose. 
 La cocina es una de las partes esenciales de cualquier casa o departamento, por lo que es  cierto que el ambiente influye bastante en nuestro apetito y ganas de cocinar, y ya que comer en casa es más saludable, deberías reconsiderar ese espacio. Todos hemos soñado con la cocina ideal; llena de luz, espaciosa y organizada… por eso aquí te damos 10 ideas para que el corazón de tu hogar se convierta en lo que siempre soñaste. 
 Utiliza tonos claros 
  
 Los muebles y las paredes de colores claros visualmente harán que tu cocina luzca más amplia. Agrega más luz 
  
 Entre más iluminada esté tu cocina, más grande se verá. Agrega lámparas y sobre todo, ilumina bien la zona de trabajo. Usa estantes abiertos en lugar de cerrados 
  
 Los muebles cerrados visualmente hacen que la cocina luzca más pequeña. Los estantes abiertos, al contrario, añaden al interior más ligereza. 
 Utiliza líneas para engañar la percepción 
  
  Las líneas verticales visualmente alargan la cocina, mientras que las líneas horizontales le agregan profundidad convirtiendo los rincones estrechos más amplios. Agrega encimeras desprendibles 
  
 Para liberar algo de espacio, puedes utilizarlas como tabla para cortar o como soportes integrados para utensilios de cocina, cuchillos, especias y pan.Usa un armario con muchos estantesEste tipo de armarios no sólo es cómodo y funcional, también ocupa muy poco espacio y se integra en casi cualquier diseño. Instala una islaSiempre que la cocina cuente con el suficiente espacio, resulta de gran ayuda incluir una isla o mesa central que te ayude a manipular los alimentos. Además, pueden servir como una zona de comidas informales. Aquí puedes encontrar cómo hacer una con tus propias manos.Recicla objetos viejos para crear originales diseñosUsando ollas viejas, rodillos o  ralladores puedes crear lámparas originales y percheros que no encontrarás en ningún otro lado.Decora las paredes con utensilios de cocinaPuedes utilizar las ollas o bandejas como decoración al colgarlas en la pared. Dale vida y agrega plantas Con plantas cualquier habitación lucirá más fresca y agradable. Lo más importante es escoger las plantas que no requieren condiciones especiales para su cultivo. También puedes optar por hierbas y especias tales como cilantro, perejil o albahaca.  ***  Te puede interesar: Ideas para tener un jardín en el interior de tu casa