Ideas para hacer lámparas originales con materiales reciclados

 Tener una buena iluminación en el hogar es esencial para nuestro bienestar. La luz es vida y nos ayuda a estar despiertos, llenos de energía y de buen humor. A la hora de decorar nuestros espacios, resulta complicado elegir una buena iluminación, ya que tenemos que concentrarnos en algunos factores: en el tipo de luz, el tipo de lámparas que vayan con la decoración y los sitios específicos donde irán. Por lo regular, tienes que centrarte en iluminar los pasillos, ya que en su mayoría, carecen de luz natural. También en sitios de trabajo como la cocina , los estudios y los dormitorios. Debes recordar que es mejor utilizar focos ahorradores, en pro de tu economía y del medio ambiente. 
 Al iluminar nuestro hogar pensamos en lámparas que realcen el estilo de las habitaciones, con un gran diseño; pero al salir a buscarlas, nos damos cuenta que en su mayoría son muy costosas debido a los materiales que utilizan o a los diseños. La realidad es que no necesitas comprarlas porque tú puedes hacerlas y gastar lo mínimo en ellas. Con materiales que utilizamos todos los días y que estamos acostumbrados a desechar, podemos crear originales diseños; desde vasos desechables, cartones de huevo, cestos de basura o botellas, todos ellos se convertirán en la lámpara que siempre soñaste. Aquí te mostramos 15 ideas de lámparas que puedes hacer tú mismo con todo tipo de materiales reciclados. 
 Lámpara con botes de pintura 
  Recicla los botes de pintura que utilizaste, lávalos, y fórralos con el diseño que más te guste. Aquí las instrucciones. 
 Lámpara con vasos de plástico  Los vasos desechables en realidad pueden tener más usos. Utilízalos para crear una lámpara, Aquí cómo hacerlo. 
 Lámpara con cartones de huevo 
  Recicla los cartones de huevo, pégalos de la manera que quieras y forma una lámpara sin gastar. Aquí las instrucciones. 
 Candelabro con papel de cera 
  Utiliza papel de cera para crear este elegante candelabro que además no te costará una fortuna. Aquí la idea. 
 Lámpara de ralladores de queso 
  Utiliza ralladores de queso y verduras y móntalos sobre una superficie de madera para crear esta original lámpara. 
 Lámpara de ganchos de madera 
  Utiliza la madera de tus ganchos y crea este tipo de lámparas que se verán increíbles en cualquier habitación. 
 Lámpara con sombreros viejos 
  Trae a la vida aquellos sombreros que ya no usas y conviértelos en originales lámparas. 
 Lámpara de papel “orbe” 
  
  Estos papeles son los que se utilizan para colocar cupcakes, en realidad son muy baratos y podrías convertirlos en una original lámpara. Aquí  encontrarás como hacerla. 
 Marco de madera para brindarle estilo a tus focos 
  Un estilo rústico para tus focos. Aquí  encontrarás cómo hacerlo. 
 Lámpara a partir de un cesto metálico 
  Recicla un cesto de basura que ya no uses y conviértelo en una lámpara con mucho estilo. Aquí encontrarás cómo hacerlo. 
 Lámpara con anillas de lata de refresco 
   Siempre tiramos las anillas de las latas de refresco, en realidad son una gran herramienta para hacer un sin fin de cosas. Aquí encontrarás las instrucciones para hacer esta lámpara. 
 Lámparas con botellas y cables coloridos 
   Necesitarás botellas de vidrio, ya sea de vino o de cualquier otra cosa. Lo único que tendrás que hacer es hacerles un orificio en la parte inferior para introducir el cable. Tendrás que encontrar un foco a la medida de la boquilla o un adaptador. 
 Lámpara con tarros de vidrio 
   Este tipo de tarros ya tienen un orificio en la tapa, por aquí podrás introducir el cable con el foco. 
 Lámpara minimalista con madera y cable 
  En esta lámpara la madera y el color del cable son los protagonistas. Ideal para una habitación minimalista. Aquí encontrarás cómo hacerlo. 
 Lámpara con lazo 
  Para realizar esta lámpara sólo necesitarás una pelota grande como molde, lazo y pegamento. Aquí  encontrarás las instrucciones y materiales. 
 Te puede interesar: 20 ideas para hacer un closet sin gastar