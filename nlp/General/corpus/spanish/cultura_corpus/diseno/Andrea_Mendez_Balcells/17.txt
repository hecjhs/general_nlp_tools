Ideas para tener un jardín en el interior de tu casa

  Un estudio realizado en Holanda en el 2011, determinó que con solo media hora dedicada a cultivar un jardín, puedes reducir significativamente tus niveles de cortisol (la hormona que produce el estrés). Por otro lado, se ha asociado esta práctica con un modelo de meditación activa. La jardinería es una práctica muy antigua que aporta un sin fin de beneficios a nuestra salud. Sin embargo, sobre todo en las ciudades, los departamentos son tan pequeños que la existencia de un jardín en tu hogar es prácticamente imposible, ya que los relacionamos con grandes terrenos verdes. Pero existen otras alternativas que te permitirán tener el jardín de tus sueños.  Es común pensar que si nuestro hogar no tiene jardín o balcones será imposible tener plantas. Esto es completamente falso. Es posible mantener plantas dentro de tu casa, ya que existen muchos tipos que pueden sobrevivir sin necesidad de tener contacto directo con el sol. Por ejemplo: los cactus, los bambúes, bonsái, helechos… son plantas que pueden vivir sin problema dentro de tu casa. Al contrario, es altamente recomendable tener plantas al interior de tu hogar, ya que purifican el aire y brindan salud, tanto física como mental. Estas son 10 ideas originales y sencillas para crear jardines en el interior de tu casa. 
 Usa botellas de plástico para hacer macetas 
  
 Recicla todas las botellas de plástico que utilices, corta el fondo y píntalas del color que más te guste. Es importante que conserves la tapa para que la tierra y el agua no se salgan. Si quieres interconectarlas como las que se muestran en la foto, necesitarás tubos pequeños flexibles o popotes y hacer un orificio en la tapas para que así el agua fluya. 
  
 Haz tus propias macetas con guacales 
  Los guacales son objetos que multiusos que también pueden servir como maceteros, decóralos o déjalos en su estado natural.   
 Monta un jardín en la pared de tu sala o cocina 
  Para este tipo de jardines puedes ver aquí más ideas y cómo hacerlos. 
  Haz macetas con corchos e imanes ¿Por qué no reciclar corchos y convertirlos en pequeñas macetas? puedes colocarles imanes y adherirlos en la pared. Aquí cómo hacerlo Recicla botes de pintura o cubetas metálicas y conviértelas en macetas   Es una manera muy sencilla de realizar tus propias macetas. Puedes pintarlas de tonos claros, y sujetarlas de trozos de madera como se muestra en la imagen de arriba.   
 Usa frascos para montar un huerto en la cocina 
  Recicla los frascos de mermelada, llénalos de tierra y agrega las semillas que desees. Tendrás en huerto perfecto en la comodidad de tu cocina. Aquí  más ideas. 
   
 Arma biomacetas en focosConvierte los focos inservibles en originales macetas. Aquí cómo hacerlo.  
 Usa un colador antiguo como maceta colgante 
  Con ayuda de lazos o cadenas puedes convertir tu viejo colador en macetas colgantes. 
  
 Arma un pequeño huerto con cáscaras de huevo 
  Macetas orgánicas con cáscaras de huevo, puedes colocarlas en el el mismo cartón. Sólo rellénalas de tierra y agrega semillas. 
  
 Utiliza un tronco como maceta 
  Puedes convertir un tronco hueco en un huerto, que además se verá increíble en algún librero o estante. 
  
 *** Te puede interesar: Ideas para aprovechar al máximo el espacio de tu hogar