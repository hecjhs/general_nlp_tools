15 lámparas originales que no pueden faltar en tu casa

 Todo era bello por las mañanas. Mañanas tranquilas en las que todo despertaba. El sonido de los pájaros traían consigo notas melodiosas que pronto eran acompañadas de los primeros rayos solares. Sin pasar mucho tiempo, ya se hacía evidente el ruido de otros animales que habían abandonado sus escondites. Nosotros, los seres humanos, dejábamos el nuestro también para calentarnos. Nos alejábamos de la oscuridad y con ello del frío. Así, el sol salía para consentirnos unas cuantas horas. Eran horas que debían aprovecharse para hacer todas las tareas, pues más tarde esta grandiosa estrella dejaría de brillar para irse a dormir también. Entonces todo quedaría en silencio. La respiración reinaría y le haría compañía a las pequeñas estrellas que se asomaban desde lo lejos.  Un buen día, esto cambió. Luego de muchos intentos e investigaciones el hombre aprendió a “atrapar la luz”. Y como hoy ya no tenemos que preocuparnos por la oscuridad durante las noches, acá te dejamos algunos originales diseños de lámparas para que le des luz a tu habitación durante las noches.  Para los fanáticos de las aves.Una buena opción para decorar tu cocina. Dale luz al cuarto de los niños. Diseñado por platinlux.com 
  Lámpara en forma de medusa diseñada por Roy Towry-Russel. Lámparas divertidas que te darán ganas de jugar. Por Toby house. 
 
  Una buena idea si lo tuyo es el arte. 
  Lámpara de piso diseñada por Frank Gehry. 
  Lámpara LED en forma de hongos. Diseñada por Yukio Takano.     Una lámpara ideal de Lumio que tiene la forma de una pequeña libreta. Además puedes usarla para cargar tu teléfono celular o tableta por medio de un cable USB. 
  Una lámpara diseñada por Pani Jurek hecha a base de tubos de ensayo e inspirada en Marie Currie. 
  Una lámpara con un diseñada para Aqua Gallery 
  Unas lámparas con diseño de origami. 
  Un diseño un poco suicida. 
   Lámpara que funciona con energía solar. Además que puedes utilizar como maceta para tus plantas. Diseñada por Yanko Design. 
  Lámpara diseñada por h220430  
 *** 
 Referencia