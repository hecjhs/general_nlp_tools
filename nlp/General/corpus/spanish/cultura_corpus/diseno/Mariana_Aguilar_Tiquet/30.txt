Campañas impactantes que dan voz a los ignorados

 Amnistía Internacional también conocida simplemente como Amnistía o AI es un movimiento que lucha por los derechos humanos. Con presencia en más de 150 países, cuenta con más de 3 millones de miembros y simpatizantes en todas partes del mundo.  Fue fundada en Londres el 1 de octubre de 1962, por Peter Benenson, tras haber leído una crónica periodística que hablaba de la detención de dos estudiantes portugueses por haber hecho un brindis por la libertad bajo la dictadura salazarista, motivo por el cual fueron encarcelados. Benenson junto con Louis Blom-Cooper y Erik Baker, decidieron formar una organización para hacer conciencia sobre las problemáticas respecto a los derechos humanos y así buscar soluciones. La mejor manera que encontraron para esto fue la publicidad. A través de ésta podían evidenciar las injusticias, siempre haciendo uso de la creatividad para buscar un bien común. Es financiada en su mayor parte por medio de donaciones de los socios, lo que le ha permitido mantenerse independiente de cualquier gobierno, interés económico, religioso y sobre todo libre de toda ideología política. Bajo el lema “Nadie será sometido a tortura o tratamiento o castigo cruel, inhumano o degradante”, del artículo 5 de la Declaración Universal de los Derechos Humanos de la ONU, Amnistía realiza su trabajo desde hace más de 50 años para hacer cumplir las normas internacionales. En 1977 Al recibió el Premio Nobel de la Paz por la campaña contra la tortura, y un año después fue galardonado con el Premio de Derechos Humanos de las Naciones Unidas.  Hoy es una organización respetada, que lucha por causas como la liberación de presos políticos, la abolición de tortura, protección a refugiados, además de que busca la denuncia de desapariciones forzadas y proteger la libertad de expresión, manifestación y asociación.   A continuación te presentamos 14 campañas de Amnistía Internacional, que por su gran contenido y búsqueda de justicia merecen la pena conocer. 
  “Para muchos escritores, su trabajo significa una muerte certera”. Campaña bastante clara que ejemplifica el peligro que representa en diversos países del mundo ejercer una de las profesiones más peligrosas; el periodismo. Así como la expresión y libertad de ideas, que frecuentemente se ven coartadas por el abuso de las autoridades.    
 Campaña presentada tiempo antes de los Juegos Olímpicos de Beijing, para denunciar a China como el país con mayor número de ejecuciones. 
   
 “Tu palabra para detener asesinatos”. Ejemplo del poder de la palabra, que aunque muchas veces te puede llevar a la muerte, es también ésta la que te puede salvar y hacer la diferencia. No hay que olvidar que la libertad de expresión es un derecho, así como también lo es la manifestación. 
   
 “Aviva la llama”. El logo de la organización se ha empleado en esta campaña inteligente, para poner fin a la lapidación. 
 
   
 “En lo único que sueñan 300,000 niños soldado es en ser niños”. Uno de los objetivos de AI es el de buscar el cumplimiento de la Convención sobre los Derechos del Niño, la cual establece la igualdad de sus derechos con las de los adultos. 
   
 “50 años contigo cortando la voz de la opresión”. La organización celebró 50 años de existencia y lo representó con la imagen superior. 
 
   
 “Esta imagen no está disponible en tu país”. Muestra de la intolerancia presente aún en muchos países del mundo, en el que para algunos parece más fácil ignorar una realidad y hacer como si ésta no existiera. Imágenes que muchos han decidido vetar y tachar de negativas para la sociedad. 
   “Tu firma tiene el poder de detener ejecuciones”. Un diseño simple pero fuerte, que no necesita gran explicación. 
   
 El anuncio fue colocado en la calles de Amsterdam con motivo de la visita del presidente ruso, Vladimir Putin. 
   
 “Él no ha hecho nada. Sólo está mostrando el numero de Amnistía. Discriminar no es humano. Denúncialo”. La campaña se enfocada en acabar con los prejuicios, pone en evidencia el gran problema universal respecto a ideas que se tienen sobre una persona y de cómo son juzgadas únicamente por su apariencia. 
  Una manera muy creativa de ilustrar la importancia de luchar por una causa; cada opinión cuenta y puede llegar a formar algo grande. En la imagen se ilustra la pena de muerte y de cómo se busca acabar con ella, que es uno de los objetivos principales de AI. 
 
   
 “Nadie nos impedirá verlo”. Mensaje que busca lo mismo que la imagen anterior. 
   
 La última cena de los reclusos antes de ser ejecutados. 
  La campaña, muestra aparentemente una tradicional muñeca rusa, pero en realidad es una bala. La finalidad era la de denunciar las atrocidades cometidas y que pocas veces se conocen a fondo.  *** 
 Con información de Play Ground Noticias