32 artículos de Star Wars que todo fanático debe tener

 En 2015, a casi 40 años de que se estrenara la primera cinta del guionista, director y productor George Lucas, llega Star Wars: Episode VII – The Force Awakens.   Los comentarios sobre el trailer no se hicieron de esperar, pues a lo largo de casi cuatro décadas, las cintas que dieron origen en la mente de un hombre han pasado a formar parte de la cultura popular. Son miles de personas de todo el mundo que han hecho de las espadas de láser, las naves y las galaxias parte de su vida. Convirtiendo a Star Wars en la saga más importante que se ha realizado sobre el espacio, además de ser una de las más taquilleras de la historia.  Fue en 1977 que George Lucas llevó a la pantalla Star Wars: Episode IV – A New Hope, el primer filme en el que hicieran aparición Harrison Ford, Carrie Fisher, Mark Hamill, entre otros. Treinta y ocho años después llega Star Wars: Episode VII -The Force Awakens, que será estrenada a finales de 2015. Y como sabemos que ya son muchos los fanáticos que esperan con ansias que llegue el invierno, en Cultura Colectiva te presentamos algunos productos para que sobrevivas en lo que llega el estreno.   Unas lámparas con reloj que no podrán faltar en tu buró. Diviértete con este increíble ajedrez. Desayuna con este divertido tostador. Haz un pastel con estas tazas medidoras que si las unes forman a R2D2. Disfruta de tu bebida favorita en esta taza. ¡Qué mejor para el invierno que esta chamarra! Disfruta del sol en una silla de R2D2. Agrégale sabor a tus platillos con esta salsera. Cargador para tu celular. Un reloj que todo fanático deberá tener.Disfruta de la comida oriental con estos palillos.  La mejor comodidad la tendrás con estas pantuflas. Utiliza estos bonitos pastilleros. Toma una siesta en esta nave y viaja a las estrellas. Otro reloj para que estés al pendiente de la hora.Si quieres encontrar más productos de Star Wars, da click en la Tienda en Línea de Cultura Colectiva. Lleva a Yoda a todas partes con esta mochila. Una memoria para que guardes toda tu información.Con esta maleta ya puedes viajar con R2D2.  Un humidificador en forma de R2D2. ¡Haz los mejores hielos! Un recipiente para guardar la comida que más te gusta. Un destapador en forma de nave. Protege tus libros o películas de la saga con este diseño. 
   Galletas totalmente pensadas en un fan de Star Wars. 
 
   Un basurero con forma de R2D2. 
  Envases de agua para Evian, diseñados en 2012 por Mandy Brencys. 
  ¡Haz estas grandiosas paletas en forma de espadas! 
 
  Para disfrutar lo mejor del café. 
  Escucha la mejor música con estos audífonos. 
  Hasta los pañuelos podrán lucir bellos aquí. 
  Hielos o chocolates en forma de Darth Vader. 
  Porque también hay mujeres que aman Star Wars. 
 *** 
 Referencia