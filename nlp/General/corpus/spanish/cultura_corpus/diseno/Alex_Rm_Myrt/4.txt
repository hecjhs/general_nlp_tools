18 problemas a los que se enfrentan las mujeres todos los días y que sólo ellas entienden

 
 Se le atribuye a la mujer ser el el ser vivo más bello y cautivador de toda la faz de la tierra, la delicadeza de su actuar y la fortaleza que le caracterizan requiere de procesos que generalmente pasan desapercibidos; sin embargo, son la prueba viviente de que la frase ‘La belleza cuesta’ no necesariamente denota  una situación monetaria, sino una cuestión de actitud ante la vida, cosas tan simples como mantener una postura, disfrazar un malestar con una sonrisa  o mantener siempre un aroma agradable son detalles que siempre serán prioridad ante cualquier situación.   
 Se cree que el cuidado personal para el sexo femenino es indispensable dentro de su desenvolvimiento en sociedad en parte por la influencia del género masculino, y al vivir esto, Agustina Guerrero, diseñadora e ilustradora, decidió retratar, en su “Diario de una Volátil”, lo complejo de ser mujer haciendo gráficas las situaciones en las que suelen actuar como verdaderas magas, desde las más incómodas hasta las más inexplicables e instintivas, estas situaciones ilustran de manera divertida todo algunas de las situaciones más complicadas e inexplicables quizá. 
 Su ingenio y simpatía hace que hasta las situaciones que más difíciles de evitar parezcan agradables, en estas 18 imágenes Agustina intenta describir problemas que quizá todas las mujeres afronten de manera empática sin caer en la negación o la negligencia. 
 1. El estado cambiante de ánimo durante el ciclo Menstrual 
  2. Confianza y cambios durante la relación.  3. Decir ‘Todo está bien’ cuando existe un desorden emocional interno.  4. Causas de la demora en atender el móvil.  5. Inseguridad ante algunas situaciones.  6. Las mil ideas que atraviesan su mente durante una pelea  
 7. Autosatisfacción (sin censura)  8. Grados de confianza en una relación.  9. Dilema del closset  10. Catarsis emocional. 
  11. Aceptación y motivación personal.  12. Comodidad en el hogar  13. Control de emociones. 
  
 14. Rivalidad intersexual  15. Lo complejo de lucir bien ante todo lo impredecible de la vida  16. Control de ideas y emociones  17. Propósitos  18. Los misterios más grandes del mundo. 
  
 Para conocer más los días complicados de Agustina Guerreo, visita su blogg oficial , en el que encontrarás más enredos, diversión y creatividad. Es como entrar a la cabeza de la mujer que está a tu lado. 
 *** 
 Te puede interesar: 
 15 cosas que no hacen las mujeres emocionalmente fuertes 
 Hábitos que sólo tienen las mujeres con alta autoestima