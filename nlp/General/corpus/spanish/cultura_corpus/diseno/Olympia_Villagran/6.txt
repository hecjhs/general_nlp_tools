Prendas oversize que sólo podrás usar antes de cumplir 30

 
 Estaba sentada sombre la alfombra de la sala hojeando el álbum familiar de mi mamá, me salté todas las fotos amarillentas de bebés que ni siquiera reconocía y de versiones de mis abuelos y sus hermanos con bigotes largos y peinados relamidos. Entonces llegué a la mitad de las páginas donde comenzaban las fotografías de mi mamá en los 80, reconocí la época por el tipo de looks que predominaban en ella y algunas de sus primas, pero lo que me impactó fue el parecido de muchas prendas que todas mis amigas y yo usamos ahora; varias son las que llenan las tiendas de moda y aparadores importantes, de hecho podría decir que una de las tendencias más vanguardistas se trata de una copia fiel de la moda ochentera con algunas modificaciones sutiles. 
 Hombreras en los sacos, pantalones de tiro alto, chamarras de satín abombadas, camisas enormes, chaquetas de mezclilla “XL” y muchas otras prendas que hoy abarrotan las calles de las ciudades más irónicas de la moda, son las piezas que hace 20 años o más se utilizaban a diario. Lo que nos hace comprobar que la moda es una gran esfera dentro de la que todo se conecta, las tendencias no desaparecen, sólo se transforman y el estilo no cambia, únicamente mejora. 
 Por ejemplo, el estilo de llevar prendas grandes caracterizó a toda una generación hace décadas, misma de la que hoy retomamos muchos elementos para crear looks “oversize”, pero ¿cómo usar la tan recurrida tendencia “oversize” que tanto nos encanta y nos causa conflicto al mismo tiempo? 
  *Las 10 preguntas que debes contestar para saber por qué nunca tienes nada qué ponerte  – 
 Sacos oversize 
  
 ¿Cómo usarlos? 
 La mejor combinación para esta prenda son los jeans entubados o alguna mini que deje ver parte de tus piernas, ya que las hombreras y solapas anchas de este tipo de sacos hará que la parte superior de tu cuerpo gane volumen, por lo que debes equilibrarlo con tu parte inferior. 
 ¿Por qué usarlos? 
 Son perfectos para cubrirte del frío cuando no quieres arruinar tu outfit si traes vestido o minifalda, además, puedes ocuparlos con un par de pantalones oscuros para darle formalidad a tu look de manera original. 
 – 
 Mom jeans 
  
 ¿Cómo  usarlos ? Dobla la parte inferior de tus jeans para dejar ver un poco de piel; no te preocupes por el calzado, porque estos pantalones van con todo tipo de zapatos y en la parte de arriba no olvides usar blusas que dejen ver la pretina de estos pantalones, como un crop top, o también puedes meter el sobrante de tu blusa por debajo de la cintura de tus mom jeans. 
 ¿Por qué usarlos? Además de cómodos, estos vaqueros transforman cualquier look en un estilo vintage , el cual es uno de los preferidos de toda una generación que se basa en el confort y la elegancia. 
 – 
 Baseball jackets 
  *Detalles que las mujeres exitosas siempre incluyen en su look 
 ¿Cómo  usarlas ? Ya sea sobre tus hombros o con el primer botón abrochado, puedes usarla con cualquier outfit , ya que la combinación de sus colores negro, gris, blanco y azul o borgoña se adaptan a cualquiera que sea tu look . 
 ¿Por qué usarlas? 
 Es una pieza muy versátil y de las pocas prendas sport que puedes incluir en un outfit de día o noche, ya sea con tacones o tenis, pero que siempre estará a la vanguardia. 
 – 
 Boyfriend t-shirts 
  
 ¿Cómo  usarlas ? Literalmente se trata de la playera que le pediste “prestada” a tu novio, pues el corte, tamaño y soltura de esta prenda te hace ver relajada y a la vez vuelve arriesgado tu look . 
 ¿Por qué usarlas? No tendrás que preocuparte de absolutamente nada, puedes ponerte cualquier bra y hacer cualquier actividad con este tipo de playeras, pero al mismo tiempo estarás utilizando una de las tendencias preferidas de las celebridades. 
 – 
 Chamarras de mezclilla XL 
  
 ¿Cómo  usarlas ? Para darle un toque especial, dobla las mangas y trata de combinarlas con colores que contrasten con el azul mezclilla, siempre recuerda llevar debajo algo un poco más estrecho para equilibrar la silueta robusta de tu chamarra con la de tu cuerpo. 
 ¿Por qué usarlas?Es la chamarra más fresca del mundo, pues haga o no un poco de frío, puedes elegirla como la tercera pieza que volverá más que atractivo tu look. – 
 Vestidos tipo suéter 
  ¿Cómo  usarlos ? Puedes optar por ponerte un tank top debajo o solamente dejarlo caer encima de tu cuerpo, la mayoría de estos vestidos son de telas cálidas, así que deja de lado los abrigos o chamarras; si te pones tacones convertirás una de las prendas oversize más interesantes en un look de pasarela. 
 ¿Por qué usarlos?Es una forma de facilitarte la vida, pues el volumen y estilo de esta prenda te hará ver superarreglada aunque no traigas más que un collar y un par de botines. – 
 Trench coat 
  
 ¿Cómo  usarlos ? Es preferible ponerte este tipo de abrigos o gabardinas con algunas plataformas o tacones que compensen lo largo de la prenda con el largo de tus piernas, no olvides usar pantalones o dejar ver algo de piel para que el resto de tu outfit no se pierda entre tanta tela. 
 ¿Por qué usarlos?En estaciones como el invierno es complicado vernos a la moda si lo único que queremos es cubrirnos del frío, pero prendas como ésta pueden transformar cualquier look en algo vanguardista por sí solas.–Ahora que ya comprobaste que todo regresa y que lo que un día fue obsoleto hoy se convirtió en la prenda más utilizada por it girls, fashion bloggers y expertas de moda, puedes atreverte a hurgar en el armario de tu mamá, pues con suerte encontrarás algo que ella pensaba que estaba pasado de moda y que en realidad se trata de un básico del street style. Siempre recuerda equilibrar tu outfit, pues aunque en pasarelas y fashion shows un estilo repleto de prendas oversize se vea increíble, no es lo mismo combinarlas para usarlas a diario, realmente la única manera de descubrir el balance perfecto es probándote todo lo que se te ocurra hasta encontrar la fórmula para aplicar esta tendencia con éxito, lo que te podemos asegurar es que no hay otra forma de vestir más cómoda que usar tallas “XL”.Inténtalo y guíate al mismo tiempo de algunos trucos de moda para perfeccionar tu look que pueden sacarte de algunas dudas a la hora de querer usar algún blazer con hombreras o unos jeans del estilo de los años 90 y para los complementos, es decir el cabello y maquillaje, saca algunas ideas de la guía que te dice  cómo hacerte un chongo diferente cada día del mes .