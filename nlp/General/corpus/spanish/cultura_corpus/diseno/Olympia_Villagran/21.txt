10 errores que debes evitar cuando decoras tu primer departamento de soltera

 
 
 Todo comenzó con una casa de muñecas, pasaron los años y la ilusión de transformar espacios ordinarios en lugares extraordinarios se convirtió, poco a poco, en la pasión de Mariangel Coghlan. Esta magnifica diseñadora mexicana desarrolló un concepto único, al que llamó FUSIÓN+México, resultado de la reflexión sobre la interdependencia de las tendencias del diseño internacional, en combinación con las maravillosas formas, colores y recursos naturales que, muy particularmente, ofrece México. 
 El extraordinario trabajo de la interiorista ha colocado muchos de sus talentosos proyectos en las publicaciones de numerosos libros, revistas especializadas y blogs, además de darle la oportunidad de ganar premios y reconocimientos a su trayectoria. 
 * Air Max: el diseño innovador de Hiroshi Fujiwara 
 El diseño de interiores, como lo describe Coghlan, es una competencia entre las tendencias que resultan del diseño mundial, pero existen referentes del interiorismo que además de adquirir un significado, pueden funcionar como bases de lo que queremos o no para decorar nuestro nuevo espacio. 
 *Ideas de decoración que los viajeros amarán 
  
 10. Evita colgar tus cuadros y/o fotografías demasiado alto 
 Considera un rango de entre 1.45 a 1.55 metros del piso al centro de tu cuadro para colgarlos a la altura ideal. 
    
 9. Nunca dejes tus lámparas muy abajo 
 Para no volver más estrecho el espacio de tus habitaciones, evita colgar o colocar lámparas que queden muy cerca del piso o de cualquier otra superficie. Considera dos metros de piso a techo para guiarte, así aprovecharás toda la luz de los focos y eliminarás esos molestos puntos que se sobrexponen a la luz focalizada de tu lámpara. 
    
 8. Jamás se te ocurra comenzar a pintar sin haber planeado antes el diseño, color y textura que tendrá el producto final 
 Antes de cubrir muebles, suelos y todo lo que no quieras que se manche, asegúrate de saber exactamente qué es lo que quieres a la hora de ponerle color a tus paredes. Todo debe mantener una sintonía, el color de la pintura de tus paredes, de tu sala y cojines, de tus muebles y repisas, no querrás que tu casa parezca estar hecha de pedazos de otros apartamentos. 
   
 7. No compres tapetes pequeños 
 Si no quieres darle un efecto de tapete de baño a la alfombrita de centro de tu sala, elige un tapete que además de ser de un material sofisticado y con increíble diseño, tenga el tamaño adecuado para colocar los muebles sobre él. Recuerda que no deben de quedar patas de sillas, mesas o sillones fuera del borde de tu tapete. 
      
 6. Ni se te ocurra comprar sin antes medir tus espacios 
 De nada servirá que compres la sala más hermosa y cómoda si no cabe en tu departamento. Toma las medidas exactas del largo, ancho y profundo de cada habitación para la que planees comprar muebles, sin olvidar que debes tomar en cuenta el tamaño de los pasillos, elevadores o escaleras, pues a menos que tengas un ventanal gigantesco que dé a la calle, tu sala y todos los demás muebles con los que rellenes cada habitación deberán pasar por esos espacios y te sentirás muy decepcionada si tienes que regresarlos porque no pudieron hacer que entraran en tu hogar. 
   5. No cometas el error de comprar cortinas muy cortas 
 Si cuelgas cortinas que lleguen hasta el piso, tu techo parecerá más alto y tu casa más amplia, no parezcas un principiante al dejar la altura de tu cortina justo en el borde donde termina la ventana, otro tip para darle estilo a tus ventanas es optar por cortinas de telas claras o con transparencias. 
  
 4. No más muebles iguales 
 Antes podía usarse, ahora es el error más repetido y horrible del diseño. No pienses que para que tu casa tenga una decoración armoniosa o interesante es necesario combinar la sala de tres piezas con la mesa de centro que también trae taburetes y las repisas forzosamente deben ser del mismo tono que las patas de tus muebles. Esta bien comprar un par de sillas o burós iguales, pero no hagas eso con todos los elementos de cada habitación, la variedad es sinónimo de profundidad y sofisticación, además le darás versatilidad a tu espacio. 
    
 3. Evita usar todos los accesorios de decoración a la vez 
 Aunque tengas la colección de libros, cuadros o esculturas más increíble de todas, arruinarás su cometido si usas todas las piezas al mismo tiempo. Cuando amontonas todo o intentas llenar cada hueco libre con accesorios de diseño, tu hogar parecerá desordenado y más reducido de lo que en verdad es. 
    
 2. Nunca decores como si todas las habitaciones fueran independientes de la otra 
 Es común pensar que si la sala es para descansar o sentarse a leer un libro y el comedor para comer o pasar a que tus amigos tomen algo, estos cuartos o habitaciones no se conectan de ninguna manera. Pues todo lo contrario, a pesar de que algunos departamentos tienen diseños abiertos y esta unión es más obvia, hay otros que separan perfectamente un espacio de otro. Pero tú tienes la elección de darle continuidad a esos espacios, únelos usando las mismas tonalidades, poniendo una silla idéntica en varios cuartos o iluminando de la misma manera las habitaciones. 
  1. Jamás olvides agregar piezas de arte 
 Puede parecer que acabas de mudarte si no cuelgas un solo cuadro o pintura, o si no tienes sólo un objeto decorativo que destaque en tu mesa de centro o repisa. Utilizar el arte a tu favor no se trata de comprar la pieza más cara, pero todas estas reproducciones o fotografías enmarcadas harán la diferencia entre un espacio personal y una casa cero acogedora. 
    Además de la altura de los objetos que incluyas en cada habitación, el tamaño de tus muebles y la cantidad o estilo de tus accesorios, el color también funge un papel importantísimo en los aciertos y errores que puedes tener a la hora de llenar de vida tu nuevo departamento. 
 El color es precisamente esa magia especial con la que cautivas tus propios ojos y los de todos los que te visiten, a través de un tono creas sorpresa y emoción, generas vibraciones positivas y/o negativas, mueves recuerdos y revives experiencias, así que pon mucha atención a la hora de elegir tu color base o principal y todos los que complementarán la paleta de tu nuevo hogar. 
 También puedes decorar cualquier espacio que consideres que necesita de una renovación a partir de todo lo que amas, como tu escritor favorito, tu género musical predilecto o tu película consentida, lee las 10 ideas de decoración para todo amante de la música o si quieres armonizar tu espacio, revisa los Detalles espirituales para decorar tu hogar, ¡muéstranos cómo aplicas estos tips en tu casa!   * Referencia: 
 Mariangela Coghlan, Firma