20 cosas que debes atreverte a hacer con tu look al menos una vez en tu vida

 
 Hace 45 años el mundo continuaba descontrolándose ante la simple idea de que una mujer portara con elegancia y sofisticación un smoking de color negro, pues el diseñador de alta costura Yves Saint Laurent aún no había girado la Tierra para demostrar que un saco y un pantalón de vestir también lucían perfectos sobre la figura femenina. Tal cual lo comprobaron Claudia Schiffer, Angelina Jolie y Anne Hathaway, siendo las mujeres que le sumaron valor a este valiente paso, que como muchos otros diseñadores, YSL logró trazar sobre la evolución de la moda para demostrarnos que atreverse es sinónimo de éxito. 
 Y no olvidemos que antes de que Christian Dior abriera de par en par la puerta del dramatismo y lo teatral al mundo de la pasarelas, éstas no habían mostrado antes transparencias que sugerían de manera sutil y estética la desnudez del cuerpo, así como piezas de sus colecciones más importantes que retoman las bases del teatro para revivir una escena dramática de los burdeles de París o de la realeza de España. 
  
 Así es como los diseñadores han cambiado al mundo: con reglas que rompen para que jamás vuelvan, con elementos retomados de épocas pasadas, con ideas que reconstruyen la línea sobre la que el estilo vaga y las vanguardias emergen; ellos se atrevieron a ser diferentes, a innovar y a transformar lo intocable. 
 De eso se trata la moda, de reinventarse para crear nuevos looks y estilos que por lo menos una vez en nuestra vida todas deberíamos intentar. 
 – 
 <!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}
-->--> 20. Teñirte el cabello del color que siempre has querido sin temor a equivocarte, si no te gusta el resultado, inténtalo nuevamente con otro. * 7 formas de llevar una cola de caballo sin importar el momento del día  19. Perforarte o tatuarte cuando hayas elegido el diseño o lugar correcto, pues son cosas que con el tiempo cada vez te serán más difíciles poder hacer. 18. Romper reglas de estilo o moda sin importarte la opinión de los demás, pues la mayoría de las personas critican todo lo que no se atreven a ponerse.17. Usar ropa ajustada o prendas escotadas mientras nos veamos bien en ellas, el cuerpo cambia y no será lo mismo usar un crop top hoy que después de casarnos, tener hijos o lo que se te ocurra que puedas estar haciendo en un futuro. 16. Imitar un look de alguna época o personaje pasado, desde un vestido renacentista hasta el cabello indomable y revoltoso de Janis Joplin, pero todas tenemos algún momento o a alguien a quien admiramos y nos gustaría vernos como ellas de vez en cuando. 
    
 15. Usar accesorios o prendas que dejen mostrar tu personalidad camaleónica, deja de pensar que no puedes ponerte pelucas, antifaces o velos, ¡claro que puedes!, y son parte del estilo con el que puedes sellar tu look . 
    
 14. Dejar de lado el maquillaje y enfocarte en un outfit perfecto y vanguardista; muchas it  girls no se preocupan en absoluto por su maquillaje, de hecho, es parte de lo que vuelve a su estilo algo especial. 
   
 13. Vestir con prendas masculinas para experimentar el rol que un saco, un pantalón de vestir y una corbata juegan; de hecho, esta tendencia existe y tiene muchas variantes como el estilo tomboy,  las prendas boyfriend,  las tallas  oversize  y los  smokings. 12. Atreverte a sorprender a todos con accesorios "inadecaudos" para el momento, así que olvídate de no ponerte lentes en lugares obscuros o sombreros durante tardes nubladas. 
    
 11. Combinar pares de zapatos distintos; así como Adidas lo hizo armando pares de dos colores, tú puedes probar con estampados, colores, formas y tipos de zapatos diferentes para darle originalidad a tu outfit . 
    
 10. Las coronas de flores y tocados no son exclusivos de los festivales de música y arte, son el accesorio perfecto si quieres llevar un look  vintage . 
    
 9. Vestir exactamente igual que tu mejor amiga para ir a una fiesta, además de divertido es algo que no puedes olvidar hacer antes de que por falta de tiempo ni siquiera se vean con tanta frecuencia. 
    
 8. Usar transparencias, además de sexy, es algo que todas deberíamos probar al menos una vez, ahora puedes encontrar cientos de prendas que están hechas de materiales totalmente traslúcidos para mostrar tu bra , por ejemplo. 
  
  
 7. Usar algún elemento, estampado o accesorio que te recuerde a tu infancia, esa increíble etapa donde nada te preocupaba, y convertirla en la pieza clave de tu look . 
   
 6. Agregarle algo personal a tu look, algún bordado, accesorio o cualquier cosa que sabes que no se repetirá en ninguna otra parte del mundo, pues de eso se trata la moda. 
    
 5. Córtate el cabello como siempre soñaste, deshazte de ese deseo frustrado y siempre recuerda que te volverá a crecer, y mientras, puedes experimentar con un cambio tan positivo como el de poder peinarte en un minuto. 
    
 4. Cambia de estilo tantas veces como quieras, nadie va a venir a preguntarte por qué ya no vistes hipster o qué sucedió con tus vestidos hippies. 
  
 * Detalles que las mujeres exitosas siempre incluyen en su look  3. Atreverte a hacerte fleco es un cambio que sólo puede volverse positivo en tu look , pues no importa qué forma tenga tu rostro, un flequillo te hará ver superdiferente porque suavizará todas tus facciones. 
    
 2. Invierte en un bolso, accesorio o prenda de diseñador, el hecho de que compres algo de colección o de alguna firma no te convierte en una frívola sin corazón ni cerebro, ahorra y prémiate con algo que siempre imaginaste tener o que realmente valga la pena. 
   
  
 1. Las extensiones, las uñas postizas y las pestañas falsas son algo a lo que muchas nos negamos continuamente, pero todo lo que vemos en tableros de Pinterest o fotos de red carpets está lleno de estos trucos, así que deja de lado el remordimiento y si tienes ganas de usar alguno de estos aditamentos, póntelo sin preguntarle a nadie. 
   La juventud sólo es una etapa y no tan larga como a muchas nos gustaría, pues no nos alcanzarían los días para probar todo lo que la moda tiene para ofrecernos, pero sobre todo, es el momento ideal para hacer cada una de las cosas que hemos querido probar en nuestro look y no nos hemos atrevido, ¡aprovecha! No existe mejor momento para hacerlo que ahora, no te preocupes por nada, no te avergüences de tu estilo ni le pidas su aceptación a nadie más que a ti, recuerda que si tú estás satisfecha con lo que llevas puesto, pero sobre todo con cómo te ves, es más que suficiente.  La moda también puede tratarse de una zona de confort a la que estás acostumbrada y de la que cada vez te resultará más difícil salir, así que al igual que las tendencias que surgen cada nuevo año y que nos dan la oportunidad de probar cosas nuevas o de jugar con nuestro estilo, intenta salirte del molde la próxima vez que tomes algo de tu clóset o elijas algo de la tienda.  Experimenta y prueba con estas 20 cosas que debes probar por lo menos una vez al elegir tu outfit , después conoce las 10 preguntas que debes contestar para saber por qué nunca tienes nada qué ponerte y no permitas que esto te vuelva a pasar después de leer el artículo que te da varias ideas para crear looks que puedes intentar cuando no sabes qué ponerte.