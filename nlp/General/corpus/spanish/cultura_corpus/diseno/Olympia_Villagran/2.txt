Tatuajes que sólo podrán llevar las mujeres que quieren ser diferentes

 
 ¿Qué es ser diferente? 
 Ser apasionada y no temer a lo que tu misma pasión te lleve a enfrentar, crear y no limitarte aun si tu creación te parece poco convencional, creer en tus propias historias y transformarlas en leyendas de las que todos quieran saber más; ser diferente implica moverte de tu manera favorita para tal vez equivocarte, caerte, decepcionarte, levantarte y poder volver a entregarte. 
 Igual que lo hizo  Johanna Olk , la diseñadora e ilustradora que ama dibujar para que los demás se pregunten más sobre su trabajo. Olk llevó su arte a los tatuajes negros, de manera distintiva, con ilustraciones de líneas finas y trazos estilizados en color negro, convirtiendo sus dibujos en historias que se cuentan a través de la piel sobre la que incrusta los diseños de mujeres diferentes, hermosas, glamorosas, pensativas y algo más. 
 Muchas nos identificamos con este tipo de pensamiento o sentimiento que nos hace sentirnos como de otro planeta, en el cual llorar está permitido, amar como loca es más que normal, morir en el intento es parte del éxito y tener ideas distintas es lo que le da sentido a la vida. Para todas esas mujeres de piel transparente, ojos cristalinos y corazón salvaje fueron creados los tatuajes negros que sólo una mujer diferente amaría tener. 
  
 Si quieres ver más sobre el trabajo y la creatividad de esta artista, puedes visitar su sitio oficial, Olk’s Studio , para inspirarte aún más en la ilustraciones con las que Johanna Olk logró enmarcar el significado de una mujer diferente y por supuesto, única. 
 – 
 Ser diferente es formar parte de las mujeres que deciden dejar de preocuparse para mejor ocuparse del rumbo de su historia, bajo sus propias reglas, aceptando sus vicios, amando sus cambios de humor y dejando que su embrollosas ideas sigan desordenando su cabello. 
   – 
 Una mujer diferente defiende su identidad, conoce su cuerpo, identifica su propio olor y presume sus imperfecciones, olvidando las reglas, desechando tabúes e intentado ser ella misma. 
 *  Tatuajes perfectos y sexys en la columna que amarás – 
 Los diseños de la ilustradora de esta serie de tatuajes, con los que muchas mujeres se identifican, son una mezcla de ternura y terquedad que demuestran la firmeza del sexo femenino, el cual muchas veces no es lo que parece, pues lejos de ser princesas esperando ser rescatadas, somos guerreras vanidosas que aman y sienten como nadie más puede.     
 – 
 Ser diferente implica tener miedo y aún así intentarlo, se trata de ver todo con otra mirada, es sentirnos parte de algo para reencontrarnos en donde alguna vez nos perdimos; es darnos nuestro tiempo para olvidar, entender, perdonar y amar. 
  *Diseños de tatuajes de manga que desearás  – 
 Entregarse totalmente, dejarse llevar y permitir que nuestro corazón se quiebre es parte de ser única, llorar sin contenernos, gritar como si no existiera nadie más para volver a respirar sin dolor es lo que una mujer inteligente y sobre todo fuerte hace cuando el amor la decepciona. 
  
 La fuerza de un tatuaje puede ser tan grande como nosotras queramos, pues dependerá de cada una hacerle justicia al trazo en tinta negra que llevamos sobre la piel. Todas estas ideas son para aquellas que no han encontrado la forma de combinar sus sentimientos volátiles con sus pensamientos frenéticos en un solo elemento que puedan tatuarse para recordarse a sí mismas que están aquí para hacer la diferencia. 
 – Al igual que este tipo de tatuajes confirman tu singular personalidad, hay otros que reafirman la solidez de tu alma, por ejemplo los tatuajes que sólo las mujeres fuertes se harán , mismos que puedes complementar con algún símbolo espiritual que te proteja siempre, como los tatuajes de mantras que harán que la divinidad siempre te acompañe .