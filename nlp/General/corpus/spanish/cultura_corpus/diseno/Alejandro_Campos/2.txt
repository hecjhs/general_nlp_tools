Los 12 diseños de tatuajes más populares de 2015

 Conforme pasan los años, los estigmas alrededor de la cultura del tatuaje se rompen, se superan los mitos y existe mucha más aceptación para aquellos que portan con orgullo alguna marca de tinta en su piel. Desde los primerizos que gustan de un pequeño detalle hasta los que ya deben encontrar espacios para el siguiente diseño, los estudios de tatuaje reciben miles de visitantes que ya también reconocen el trabajo de ciertos artistas. Y sí, emplear ese concepto para un tatuador es válido cuando éste hace de la tinta en la piel, una oda a los cánones estéticos, regalando una verdadera obra de arte. 
  
 Hace unos días, la página especializada en tatuajes Tattoodo , reveló el resultado de su encuesta a cientos de tatuadores alrededor del mundo, sobre cuáles fueron los diseños más populares del año. En contraste con otros años en que los búhos o el símbolo de infinito dominó los encargos diarios, múltiples diseños minimalistas acapararon las mentes de cientos de miles de personas que acudieron a los estudios. Además, como cada año, algunos diseños siguen siendo populares por los símbolos que representan, como el compás o el sistema solar. A continuación, te compartimos los 12 diseños más populares de este año. 
 1.- Flechas 
 Fue uno de los diseños preferidos por los jóvenes que se acercaron a los estudios a realizarse su primer tatuaje, y muchos utilizaron referencias de Instagram y Tumblr. 
      
 – 2.- Tatuajes debajo de los senos 
 Algunos tatuadores aseguran que la moda por los tatuajes debajo de los senos se debe al de Rihanna. Con miras a imitar a un símbolo sexual de la actualidad, cientos de mujeres han elegido entre múltiples diseños. 
      
 – 3.- Punto y coma 
 Este diseño tiene su origen en un proyecto que buscó que aquellos que sufrían depresión utilizarán el signo de puntuación como un símbolo de su resistencia ante la enfermedad. Teniendo en mente que no es un punto definitivo, la historia de cada uno de ellos continúa. Gracias a la campaña, se generó mucha conciencia respecto a la enfermedad.    
 – 4.- Montañas 
 Aquellos amantes de la naturaleza, y sobretodo del senderismo, encontrarán que un diseño de montaña les permitirá reafirmar sus gustos y pasiones. Un hombre o una mujer jamás se probarán así mismos hasta que deban enfrentarse ante la inmensidad de una montaña 
      
 – 5.- Tatuajes de acuarela 
 Uno de los diseños que ponen a prueba a los mejores tatuadores, pues requiere del uso correcto de los colores y tonalidades, por lo que no cualquiera puede hacerlo. Si quieres hacerte uno de estos tatuajes, lo mejor es que consultes si tu tatuador ya los ha hecho antes y que conozcas cuál ha sido el resultado. 
      
 – 6.- Mandala 
 Los tatuajes no han estado exentos del interés cultural occidental por culturas del otro lado del mundo, por lo que aquellos que han encontrado en el hinduismo y el budismo pilares para su vida también han retomado múltiples símbolos . Los mandalas han sido uno de los diseños más solicitados del año. 
      
 – 7.- Barras 
 La tendencia minimalista trajo consigo que las barras de color negro fueran uno de los diseños más populares de 2015. 
      
 – 8.- Puntos 
 Así como las barras, los puntos son diseños tatuados meramente para fines decorativos. 
    
 9.- Galaxia 
 El interés por el universo, que ha trascendido siglos del desarrollo humano, sigue en el top de los diseños más buscados por aquellos que quieren decorar su piel. Como en el caso de los tatuajes de acuarela, no cualquier tatuador puede hacer este diseño. 
      
 – 10.- Sistemas solares 
 Como la galaxia, el sistema solar ha decorado cientos de antebrazos en este 2015. 
      
 – 11.- Compás 
 Aquellos que nunca quieran perder el rumbo encontrarán en el diseño de un compás para la navegación, el recordatorio eterno de que nunca se perderán en el océano. 
      
 – 12.- Líneas 
 Como en el caso de los puntos, las líneas son un pequeño detalle en la piel que, quizá sin significado, puede realzar alguna parte del cuerpo. 
      
 Te puede interesar: 13 tipos de tatuajes que vas a querer 
 *** 
 Referencia: Tattoodo