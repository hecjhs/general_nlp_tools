Markthal Rotterdam; el primer mercado cubierto de Holanda

 El Markthal Rotterdam, un mercado cubierto con complejo habitacional, ha abierto de manera oficial después de 5 años de construcción. Diseñado por los arquitectos holandeses MVRDV, abarca una superficie de 312,000 metros cuadrados. 
  Una de sus características más impresionantes es su estructura curva y su colorido mural titulado Cornucopia, hecho por los artistas Arno Conen e Iris Roskam. El mural cubre 36,000 pies cúbicos divididos en múltiples cuadrados de aluminio que poseen una imagen impresa de distintos motivos, desde productos del mercado, flores e insectos que hacen referencia al estilo de los maestros holandeses del siglo XVII. Los dos pisos inferiores poseen espacio para 96 stands de comida y 20 más para locales comerciales. Además, el complejo cuenta con 228 departamentos que cuentan con ventanas al interior para disfrutar de lo que pasa dentro del complejo. Cultura Colectiva te comparte imágenes del nuevo Mercado Rotterdam. 
 http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam12.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam10.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam11.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam2.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam3.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam5.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam7.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam9.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/MarkthalRotterdam8.jpg