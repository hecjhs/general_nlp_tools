10 tipografías para hacerte el tatuaje perfecto

 Tatuar es para diversas culturas  un acto ritual y simbólico que en nuestro mundo moderno se ha convertido en una práctica cada vez más frecuente, aunque significativa para quien lleva en la piel un símbolo , dibujo o lengua escrita. Decidir tatuarse un mensaje escrito conlleva la elección no sólo del mensaje, sino de la tipografía más adecuada en forma, grosor, tamaño, color, diseño, así como el lugar del cuerpo donde lo llevarás. Pensar en una tipografía involucra considerar estos elementos, la importancia de un diseño tipográfico con el cual el trazo de tu mensaje se acople, implica cumplir el objetivo más importante: que te sientas cómodo con él. Considerar la tipografía como un elemento clave en el diseño involucra un proceso que puedes realizar en un trabajo colaborativo con tu tatuador. Apuesta al juego de los trazos con las líneas de tu propio cuerpo. Aquí te presentamos diez estilos tipográficos para que te des una idea. 
 Orientales 
 Abarca tipologías variadas de las letras de la familia de lenguas del mandarín, a las cuales englobamos como chinas, pueden implicar una caligrafía de estilo pequeño y trazos finos, o bien gruesos. 
   Góticos 
 Es quizás una de las tipografías más popularizadas; sin embargo si buscas un mensaje con fuerza, este estilo puede ser el adecuado.    Medio Oriente  
 Existen tipologías variadas que pueden adecuarse a esta categoría, para aquellos que optan por diseños con caligrafía en árabe, sánscrito, persa, con su carga histórica y mítica existen múltiples opciones. 
    Letras con diseño 
 Esta opción es especialmente enfocada a quienes buscan jugar con las letras, perfectas para quienes eligen grabar en la piel una inicial con diseños más rebuscados o simbólicos.   
  Estilo graffiti Para quien gusta de un estilo de arte callejero y en relación a la cultura del steet art, que juega con una mezcla variada de diseños y colores. 
   
 Caligráficos 
 Los estilos caligráficos juegan con la belleza y la elegancia de la letra manuscrita para quien busca un estilo sutil. 
     Geométricos 
 Un ejemplo de este estilo es la tipografía Quartz 974, inspirada en líneas geométricas basadas en triángulos. 
  Minimalistas 
 Las tipologías cursivas o manuscritas son claves en este estilo, que generalmente busca diseños discretos para una palabra o pequeñas frases. 
  
  
 Formas con las letras 
 El juego que puede realizarse con frases puede vincularse a símbolos con los cuales complementar tu mensaje. 
  
   
 Números 
 Los números pueden ser representativos de fechas importantes o conmemorativas, significativas para cada persona. También hay variedad de tipologías a elegir, entre números ordinarios y romanos. 
  
  
 Es buena opción realizar algunas pruebas con diferentes letras y estilos con anterioridad, para ello existen algunas páginas que permiten la conversión de letras en diferentes estilos tipográficos. Algunas opciones son dafot.com o conversordeletras.com. Otra fuente bastante nutrida la puedes encontrar en Pinterest en su Guión de fuentes para tatuajes, en el cual puedes encontrar variedad de ejemplos para que puedas visualizar partes del cuerpo, ideas para tu mensaje, tipografías, colores, estilos minimalistas, diseños y dibujos. 
 Además puedes encontrar tipografías realizadas por diseñadores o bien realizar tu propio diseño. De los múltiples caracteres y opciones que se te pueden presentar, lo más importante es que tomes en cuenta el impacto que quieres darle al mensaje, su visibilidad y claridad. 
  
  
 * 
 Te puede interesar: Los mejores tatuajes en la muñeca 18 originales diseños para tu primer tatuaje *** 
 Referencias: 
 Behance Tatu