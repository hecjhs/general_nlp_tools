5 hábitos que las personas exitosas hacen antes de las 8 am

 
 Se ha llegado a la conclusión de que las personas exitosas se levantan muy temprano y duermen muy poco. Por ejemplo, se sabe que la dama de hierro, Margaret Thatcher, se levantaba todos los días a las 5 de la mañana y dormía sólo 4 horas: podría trabajar hasta las 2 o 3 de la mañana y luego levantarse a las 5 para escuchar algún programa de radio y comenzar el día. 
 Cuando le preguntaron a Napoleón Bonaparte cuántas horas de sueño eran necesarias, se dice que respondió: “Seis para un hombre, siete para una mujer, ocho para un tonto”. El arquitecto estadounidense Frank Lloyd Wright, se despertaba a las 4 de la mañana todos los días. Robert Iger, el CEO de Disney inicia su día a las 4:30 am. Y así podríamos mencionar el horario de sueño de muchas otras personas que se consideran exitosas y llegaríamos a la conclusión de que existe un patrón bastante marcado. 
  
 Más que en otro momento del día , el tiempo que transcurre antes de las 8 de la mañana, es determinante para potenciar nuestro día. Podemos restar estrés, darle ligereza y fluidez a nuestro trabajo, incluso nuestro humor mejora significativamente si comenzamos bien el día. Aprovechar esas horas de la mañana, aunque nos cueste trabajo levantarnos más temprano, es la clave para lograr una vida exitosa y saludable. A continuación te mostramos 5 hábitos que algunas personas exitosas hacían o hacen en sus rutinas matutinas. 
 Hacen ejercicio  
  Podríamos pensar que hacer ejercicio por la mañana nos generará más cansancio durante el día, pero resulta ser completamente lo contrario. Las personas que hacen yoga, salen a correr, o van al gimnasio por la mañana, realizan con más efectividad su trabajo, ya que el cuerpo se libera de estrés y se oxigena. Si consideras muy complicado levantarte a las 5 am, hazlo 15 o 20 minutos antes de lo normal, realiza una rutina corta de ejercicio y sentirás la diferencia desde el primer día. Para que no pongas pretextos, aquí te mostramos una rutina de 6 minutos para hacer antes del baño. 
  
 Planean su día 
  
 Tener un mapa claro de los horarios y actividades que desempeñarás durante el día, te dará una visón más clara de tus prioridades y objetivos. Toma 10 minutos en la mañana para planearlo, también incluye un lapso de descanso en algún momento del día para despejar tu mente. 
 Desayunan alimentos saludables 
  
 El desayuno es la comida más importante del día, e incluir alimentos saludables, además de ser benéfico para tu salud, te mantendrá de buen humor durante el resto del día. Puedes desayunar fuera o en tu casa, pero siempre hazlo abundante y saludablemente. 
 Despejan su mente 
  
 No sólo debemos preocuparnos por nuestra salud física, la mental es elemental para llevar una vida plena. Toma unos minutos en la mañana para meditar, leer un poco, respirar hondo, pensar positivo. Esto ayudará a mejorar tu estado de ánimo y a suavizar la carga de trabajo durante el día. 
 Enlistan sus pendientes 
  
 Haz un top 10 de pendientes, comenzando por los más difíciles, dejando los más sencillos al final, esto para que tu día se torne menos pesado. Te darás cuenta que tu trabajo fluirá mejor y el estrés diminuirá significativamente.  Así que ¡a levantarse más temprano! 
 *** Te puede interesar:  Hábitos que la gente exitosa hace y tú no tenías idea