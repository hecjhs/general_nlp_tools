17 sencillos pero brillantes consejos que harán tu vida más fácil

 ¿Qué estarían haciendo en este momento Carlos Slim, Karl Lagerfeld, Mark Zuckerberg, Bill Gates, Richard Branson? La verdad, no lo sabemos, lo único que es seguro es que son personas tan ocupadas que no tienen tiempo qué perder en los detalles. Todo en su vida es ajetreo constante y el estrés es algo normal. Tampoco hay tiempo de los compromisos sociales o de hacer amigos, pero, ¿qué pasa si te decimos que hay momentos para todo lo anterior, pues también saben administrar sus horas con maestría? 
 Mark Zuckerberg, por ejemplo, siempre usa una playera gris para ahorrar en la toma de una decisión con tan poca importancia. Los mandatarios más importantes tampoco se preocupan por pensar en su vestuario, alguien más se encarga de eso. Y otros, simplemente tienen un estricto régimen que los hace ser de tal forma. 
  Las leyendas urbanas aseguran que Karl Lagerfeld tiene un estricto régimen de vida, tanto que pudo adelgazar 40 kilos para poder lucir un pantalón entubado y siempre, siempre, luce un atuendo impecable para que a la hora de su muerte, luzca como el más grande genio de la industria textil. 
  
 Aquí te damos 17 consejos para que tu vida sea más sencilla y puedas lograr lo que te propones. 
 1. Decide las cosas importantes, porque en 5 años, el 80% de lo que haces hoy será nada. 
 2. Dormir bien, hacer ejercicio y una comida saludable podrían ayudarte a triplicar tus energías y resultados, porque  el enfoque y la motivación aumentan. 3. La regla de los dos minutos: si puedes hacer algo en este instante, como contestar un mail o una tarea en casa, hazla ya. Planear para después, recordar que debías hacerlo y planearlo en el futuro, te tomará 5 minutos o más. 
  4. La regla de los 5 minutos: El mayor remedio contra la procrastinación es no pensar en las enormes tareas que debemos realizar, sino en sólo 5 minutos del mismo trabajo. Considera que sólo al final notarás que la mayoría de las veces, trabajarás más de cinco minutos y ni siquiera te darás cuenta, lo único que importa es tomar el ritmo adecuado. 
 5. La cadena de productividad de Seinfeld: si quieres ser bueno en algo, hazlo todos los días, incluyendo sábados, domingos, Navidad o el día de la bandera. Sin excepciones. 
  
 6. Durante 30 minutos, dedícate sólo a una tarea y nada más: no teléfonos, no mails, no hables con la gente, no veas Facebook. 
 7. Siempre lleva tus audífonos. No debes escuchar música pero ayudará a que la gente se desmotive y no se aproxime. 
  
 8. Programa tu mail para que llegue a una lista de pendientes. No leas tu correo como la primera tarea de tu día ni en la noche. Hazlo, si lo necesitas demasiado, a las 11 de la mañana, a las 14 horas y a las 17. Borra o que no sea necesario y programa lo demás, te ayudará a tener mayor control. 
 9. No estés siempre disponible cuando las personas te llamen. Mantén tu teléfono en silencio y manda las llamadas a buzón cuando no sean necesarias. 
 10. Desecha las tareas pequeñas que no te ayudan, como consultar Facebook, por ejemplo. 
  11. Las tres tareas más importantes: empieza con las más necesaria por la mañana y así sucesivamente. 
 12. La fuerza de voluntad es limitante: no pienses que te ayudará cuando estés en problemas. Toma decisiones importantes en la mañana y automáticamente, verás que más cosas son posibles. 
 13. Siempre pregúntate qué es lo más poderoso que podrías hacer en ese preciso momento. 
    
 14. No perfecciones las cosas demasiado. En el transcurso podrás trabajar en ellas. 
 15. Programa tus momentos de procrastinación . Tu cerebro necesita descanso y a veces un nuevo episodio de “ Oranges Is The New Black ” es lo que necesitas para hacer cosas brillantes. 
  
 16. La presión puede hacer maravillas, utiliza las recompensas y los compromisos sociales para hacerte sentir presionado. 
 17. No hagas compromisos. Muchos te odiarán por esto pero a veces lo mejor viene con lo inesperado y si no quieres asistir, lo mejor es decir no desde un inicio, si tienes tiempo, puedes llegar de sorpresa y te lo agradecerán. Tendrás más tiempo para cuestiones más relevantes y en 20 años no importará demasiado. 
  
 Tal vez a estos sencillos consejos puedas añadir un toque con las rutinas diarias de las  personas más exitosas  y así, lograr todo lo que te propones. 
 *** Te puede interesar: Consejos de un monje budista para aprender a amar Los peores hábitos del mexicano