Señales que te envía para saber que le gustas

 
 Tú, seguramente alucinas que le gustas, cientos de ideas se cruzan por tu cabeza y consideras posible cualquier señal para aseverar que algo más que sólo amistad ocurre entre los dos. Todos hemos pasado por eso. Por una extraña intuición que nos vuelve locos y que a veces, en la más incorrecta de las maneras, nos desengaña de la ilusión. 
 Es muy normal y seguramente a todos nos ha pasado, porque el poder que nuestra mente tiene para convencernos de que algo es cierto, es mucho más grande que la realidad. Agotamos esas noches en vela en las que no dejamos de pensar en que algo puede suceder. Nos gusta creer que el golpe en la espalda, el roce casual o la sonrisa que mantiene mientras pasamos sólo pueden ser señal de que le gustamos, porque en ese momento nos convertimos en las personas más ingenuas e inexperimentadas del mundo entero. 
 Cuando el amor se declara, pierde algo de místico, ese sentimiento de incertidumbre que nos hacía presas de sus redes desaparece y así, el ritual de conquista pasa a un nuevo nivel. Pero mientras tanto, vivimos en suspenso, con la duda de no saber si somos o no parte de sus pensamientos, de su corazón y si la conexión que imaginamos es sólo ficción o algo que se respira en el aire. 
 Si te da al menos cinco de estas señales, puedes tener el camino un poco más seguro, si no, debes esforzarte más o tal vez simplemente dejar que las cosas se enfríen para apuntar tus objetivos hacia otra dirección puede ser lo correcto. 
 – Mensajes en las mañanas y en las noches 
 Tal vez no haya tenido tiempo en todo el día para escribir algo, pero si a punto de ir a la cama te recuerda y escribe el más simple “buenas noches”, de igual manera por las mañanas, ésta es tal vez una de las señales más claras de que le gustas.    
 – Te pregunta qué vas a hacer y él o ella siempre está disponible 
 Busca cualquier excusa para verte y sólo espera el momento en el que tengas tiempo para por fin salir un día y demostrarte lo bien que pueden pasarla juntos y a solas. Otra razón podría ser que en realidad no tiene nada qué hacer y sólo quiere compañía, pero lo más seguro es que busque estar contigo 
  
 – Le interesa toda tu vida… menos cuando hablas de tus amigas o amigos más íntimos 
 No de una pareja, sino de esos amigos que pueden ser competencia clara. En realidad, prefiere evitar el tema y continuar considerando posible que no exista nadie más en tu corazón. 
  
 – Indaga en tu vida personal 
 A pesar del punto anterior, sí sabe cómo preguntar si estás interesado en alguien, enamorado o tienes una relación… ya que saberlo es sumamente importante para poder continuar con la estrategia o redireccionarla. 
  
 – Te molesta todo el tiempo 
 La broma más tonta, burlarse de la cara que haces cuando sonríes, embarrarte sustancias extrañas o simplemente imitar tu voz que resuena con particularidad, todos son actos bastante infantiles que nos remiten al amor de primaria, pero que sin saberlo continuamos haciéndolos todo el tiempo. 
  
 – Se ríe de casi todo lo que dices 
 No todo, evidentemente, porque no se trata de un bufón que esté dispuesto a reír de cualquier cosa, pero las cosas en las que te esfuerzas por que parecen auténticas, tú y esa persona disfrutan de esos momentos en los que deciden simplemente reír para pasarla bien. 
  
 – Te dice que te ves bien aunque tengas una cara destruida 
 Porque para esa persona en realidad te ves bien y las ojeras del día anterior pasan completamente desapercibidas. Eso no sólo implica atracción, sino que estás a punto de enamorarla con profundidad. 
  
 – Te da likes a lo que posteas en tus redes sociales 
 En esa foto que ocurrió hace más de 1 año, en el estado que todos los demás ignoraron, en el más simple emoticón. Está dispuesto a demostrarte que le gustas de la manera más literal posible y seguramente si hizo esto, ya lo notaste. 
  
 – En una fiesta la pasa contigo 
 Te ofrece un trago e intenta que la pases lo mejor posible. Baila contigo aunque no sepa cómo hacerlo y te une a las pláticas si tú eres la persona que no conoce a nadie. 
  
 – Mucho contacto corporal 
 Cuando están juntos, puedes destacar los roces, las caricias o a veces simplemente un apretón de brazos. Si recibes esta señal y también te gusta, es posible que no haya más que simplemente decir que se atraen. 
  
 – Te invita a salir 
 La señal definitiva. Si le gustas, seguramente el paso siguiente es invitarte a salir. No significa formalizar una relación ni mucho menos, pero sí interés por conocerte más. 
   Y es que a veces es muy difícil darte cuenta si alguien está sinceramente interesado en ti. Tal vez con estas señales de que alguien no quiere contigo puedas notar que sólo te está ilusionando. *** Te puede interesar: 
 Cómo darte cuenta de que no le gustas 10 looks que puedes intentar en la primera cita y que no se note que te gusta tanto