10 cualidades que hacen a un hombre más atractivo sin importar su físico

 
 ¿Cuántas veces ocurre que una mujer bellísima sale con un hombre que parece poco agraciado físicamente? Nos parece extravagante y absurdo pero después de unos instantes de convivir con él, entendemos a la perfección qué es lo que la volvió loca. 
 Evidentemente no se trata del físico sino de un tipo de magia que envuelve y seduce a aquellas mujeres que han dejado esa mentalidad retrógrada en la que no importaba cómo te tratara un hombre sino el atractivo que pudiera convertirse en un tipo de trofeo, sí, porque por muchos años, igual que cosificaron a las mujeres, lo hicieron con los hombres. 
 Y entonces, entendimos que el hombre menos agraciado físicamente, también puede ser el más atractivo. No se trata de un cliché sino de una reivindicación a ese Cyrano de Bergerac que tiene una enorme nariz pero las lineas poéticas más hermosas, una inteligencia digna de celebrar con galanura y el toque ideal para expresar su amor y su pasión. 
  
 Nadie debería fijarse en la apariencia del otro sino en la conexión que logramos con esa persona, con el encantador carisma que deshace los prejuicios y las risas que estamos dispuestos a resonar en un cuarto después de platicar por horas a su lado, porque al final el físico se acaba, la seducción que nos puede provocar su rostro tal vez deje de encender nuestras emociones pero la magia que nos liga a alguien , esa nunca termina. 
 Aquí algunas cualidades que no tienen nada que ver con el físico sino con la personalidad que logra enloquecer a todas las mujeres. 
 No conformarse 
 Un hombre que lucha por lo que quiere, que nos enseña que todo es posible y que la luna no está tan lejos como parece, es simplemente irresistible. Es un ejemplo de orgullo y perseverancia con el que nos gustaría tener la charla más intensa. 
  – 
 Confianza 
 Puedes ser el más guapo pero si no existe autoestima, no hay nada. Voz baja, actitud encorvada y dejar que los otros hablen por ti, pierde cualquier esperanza en esa persona, ¿qué pasará cuando debas defender tus ideas? seguramente dejarás que el resto decida por ti. A las mujeres nos gusta un hombre en el que podamos confiar y sentirnos seguras. 
  
 – 
 Buen sentido del humor 
 Tal vez es el único ingrediente que lleva la receta de sensualidad, porque un hombre con sentido del humor puede lograr todo sin mucho esfuerzo.   – 
 Ritmo 
 Al bailar y en la cama, el ritmo es indispensable. Un hombre que se mueve con talento puede ser para nada guapo y aún así provocar suspiros en aquellas que lo miran con devoción. Aplaudir sus pasos, intentar nuevas cosas juntos y hacer conexión en el baile es uno de los ingredientes base para una buena relación. 
  
 – 
 Destreza sexual 
 Que no involucra un gran tamaño sino más bien, escuchar, prestar atención y así, dominar las artes amatorias. Existen muchos hombres que van directo a lo que van sin considerar zonas erógenas, caricias y besos. Ellos nunca triunfarán. 
  – 
 Decisión 
 Con la capacidad suficiente para tener una opinión y no necesitar de nadie para comprar ese helado del que tiene ganas o dejar todo para viajar por el mundo. 
  – 
 Inteligencia 
 La inteligencia no se trata de aprender todo lo que lee sino de poner en práctica todos sus conocimientos para obtener lo que desea. Un hombre inteligente sabrá sobrevivir con lo menos posible o adaptarse al cambio sin explotar de pronto… es difícil pero un rasgo que nos fascina.     – 
 Ser detallista 
 Un detalle, aunque sea el más mínimo, sencillo y barato, puede borrar al resto de los competidores y convertirlos en el candidato ideal para establecer una relación. 
  – 
 Una conversación interesante 
 En ocasiones los hombres quieren impactar a su conquista hablando de temas diversos, pero pronto parecen dueños de la conversación sin dejar que ella emita palabra alguna. La receta ideal es preguntar y establecer una conversación en la que ambos se sientan involucrados. – 
 Encontrar eso que te hace diferente 
 Ese toque de originalidad que tanto se requieren en tiempos modernos donde todo parece ser una copia del resto del mundo. Cada quien tiene ese punto especial que lo vuelve único, pueden ser algunos de los enlistados aquí o el más extravagante que haga que marques con un aura única el lugar al que vas.      
 Porque, quién necesita otra cosa cuando tienes “lo tuyo”. No necesitas una traje cuando tienes los movimientos, ni los movimientos cuando tienes la chispa. No necesitas un buen look cuando tienes los libros, la actitud, el toque… La autenticidad es encontrar tu magia, trabajar en ella y dejarla salir en todo momento con la nueva línea de Axe . Find your magic.