Frases que debes dejar de decir después de los 30

 
 Llega un punto en nuestra vida en el que sentimos que no hay rumbo ni dirección, queremos volver a nuestros años dorados, a ese momento en el que la época nos pertenecía, nosotros éramos quienes rompíamos los estereotipos, los patrones y las reglas para intentar transformar el mundo y hacer que lo que estuviera a nuestro alrededor fluyera a nuestro ritmo. Los años dorados, como dirían los abuelos. Esa época de rebeldía que nos llenaba las venas, nos iluminaba por dentro y sólo queríamos comernos el mundo. 
 Pero de pronto, nuevas cosas y tecnologías salen para darnos un golpe en la cara. Ya no somos quienes estamos más actualizados que nadie, ya no dominamos el entorno ni queremos cambiar el mundo. Ahora somos nosotros los adultos que antes veíamos con extrañeza, los que tenían dinero pero nada de tiempo, a quienes se les había acabado la energía para recorrer paisajes inhóspitos y mantener nuevas experiencias con el resto del mundo. 
 Cobran sentido muchas cosas y otras tantas nos parecen un horrible cliché que el cine retrata con poca astucia. ¿Acaso nos convertiremos en el hombre de “Belleza americana”, que ve en la amiga de su hija su deseo sexual más incontrolable y busca con anhelo y desesperación gustarle? Seguramente te desilusiona un poco llegar a esa edad y no tener todo el dinero de Mark Zuckerberg ni los excesos geniales del lobo de Wall Street. 
 Pero, sin hacer alarde de madurez y mucho menos juzgar a aquellos que parecen entrar a esta época de sus vidas, existen palabras que ya no cuadran o encajan con la persona estable y exitosa que deberíamos aparentar ser. Esas que en lugar de hacernos lucir serios, profesionales y maduros connotan todo lo contrario y además, nos hacen víctimas del adjetivo “patético” que suena a nuestras espaldas. Palabras típicas que, nos imaginamos, nos hacen lucir “a la moda”, listos para juntarnos con personas de menos edad y que la diferencia ni siquiera sea visible. El mejor consejo es evitarlas a toda costa y encerrarlas en lo más profundo de nuestra conciencia porque tal vez así, por fin la gente nos tome en serio. 
 – “Estar en onda”. 
 Porque tal vez cuando la palabra estaba de moda, en los años 60, la palabra onda podía aplicar para cualquier cosa, pero ahora, como tradición, la usamos para referirnos a diferentes cosas… Quién no ha dicho alguna vez: “¿Qué onda?” para saludar o preguntar cómo va una situación, pero la peor de todas es, “estar en onda”, sólo recuerda esto: estar en onda es pertenecer a los 60, ¿realmente deseas estar en onda?. 
  –“Como en los viejos tiempos”.Porque claro, los viejos tiempos siempre son mejores, pensamos. Sin embargo, deberías cambiar de actitud y mirar las cosas maravillosas que puedes hacer hoy. – “Gluglunes, martinis, miercolitros, juevebes, beviernes, sabadrinks…” 
 Calificar los días de la semana con apelativos que hacen referencia a bebidas alcohólicas sólo te hará lucir como alguien que no logró superar su etapa de adolescente, pero que ahora lo único que le queda de ese momento de su vida son los recuerdos. 
  
 – “Tengo un reventón esta noche”. 
 Lo mismo que ir al “huateque” o “una pari”, la fiesta es la fiesta y a ninguna edad debes limitarte a bailar y beber toda la noche; sin embargo, seguramente si dices una de estas frases pronto dejarán de invitarte. No eres sapo para que te revientes. 
  
 – “Estos chavos de ahora”. 
 Porque claro, nosotros, los más maduros, nunca haríamos eso. Esas locuras de los jóvenes. 
  
 – “Qué suave” 
 Que se acompaña de otras frases como chévere o chido. Uno debe entender que los 30 son la época de progreso, éxito y ser la persona más profesional posible, que estas frases nunca acompañen tus expresiones. 
  
 – “Es viernes y hoy toca”. 
 El viernes, el mejor día del año, aquél en el que puedes hacer todo lo que soñaste entre semana, hasta tener sexo desmedido, como la frase lo dice. 
  
 – “La palomilla, la bandera o los cuates”. 
 Porque esas cosas que se nos quedaron tan el inconsciente gracias a nuestros padres, ahora las repetimos sin cesar como si se tratara de una broma de mal gusto. 
  
 – “Vamos a cotorrear”. 
 O echar el coto. En ningún momento es válido. 
  
 – “¿Pues cuántos años me echas?” 
 La peor frase de un adulto al querer conquistar, porque además de que puede salir contraproducente, ¿no estás harto de poner pretextos para no decir tu edad? 
  
 – “Me hace lo que el viento a Juárez, da el gatazo” o cualquier otro refrán 
 Los refranes son válidos para hacer referencia a la gente que los dice y reír un poco de ellos, en otras circunstancias es un poco vergonzoso que alguien de 30 los aplique en su vida diaria. 
  –“Te ves bien modeurrrno”Decir “modeurrrno” puede quitarle todo lo moderno a la situación. Nada más incómodo que alguien lo diga y no saber cómo responder, porque este tipo de frases son tan empáticas a las mamás que cada vez que las dices podrías transformarte en una como por arte de magia. Decir mucho con poco puede sonar complicado. La regla básica de la simplicidad puede traernos muchos beneficios, mismos que puedes encontrar en los prepagos accesibles de Unefon. A nadie le gusta que le cuenten los minutos. ¡Llama, mensajea, utiliza tus redes y hazte escuchar! ¡Exprésate con una llamada, mensaje o un like! Con Unefon puedes decir lo que quieras, cuando quieras. #HazteEscuchar.