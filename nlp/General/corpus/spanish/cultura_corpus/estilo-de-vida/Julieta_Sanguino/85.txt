20 cosas que debes hacer antes de iniciar una relación

 Volver a comenzar, planear un anhelado retorno a la felicidad, reconstruir tu vida y levantarte para vivir nuevamente el amor. Cuando terminas una relación es como si una persona muriera: no lo volverás a ver ni a saber de él, las cosas que hacías no volverán jamás y verlo a diario termina porque simplemente ya no es posible. Vives el duelo, la negación y después intentas superar esa ruptura como los grandes, comienzas tu vida de cero, sin nadie a tu lado, viviendo para ti, siendo libre, independiente y fuerte.  Los demás siguen su curso normal pero algo dentro de ti ha cambiado, ya no eres la pareja de nadie, tu historia de felicidad por siempre ha acabado, no hay nadie que pase por ti, que te lleve flores, te haga sentir mejor, te diga un “te amo” o cocine a tu lado por el simple hecho de cocinar. De pronto estás sola contra el mundo que parece imposible de sortear hasta que te das cuenta que eres feliz e independiente de ese modo.  Pronto retomas tu vida, te das cuenta que no es tan malo estar sola, conoces más personas, eres dueño o dueña de tu tiempo, nadie te cancela porque no haces un compromiso formal y engorroso con alguien, aprendes más sobre ti, aprendes más sobre el mundo y te das cuenta de lo que realmente quieres. Sabes que la vida sigue y que, tal vez, esa relación tenía que terminar en ese momento. Ya no hay palabras incómodas, falsos ánimos, tristezas disfrazadas o eventos familiares ajenos a los tuyos.  Ahora sólo te preocupas por ti y debes aprender a conocerte. La vida se hace más sencilla pero aún hay algo que anhelas, una alegría de pareja que esperas vivir otra vez. Sin embargo, antes de hacerlo, no olvides realizar estas 20 cosas. Y así, disfrutarte y disfrutar a tu nueva pareja al máximo.    Aprende a viajar solo
 A ir al cine solo, a vivir solo, a hacer tu vida sin una sombra constante que siga todos tus pasos. Nadie necesita a otra persona para cumplir sus sueños, porque lo más importante es que sepas y hagas tu vida por ti. 
     Vive

 Comienza con hacer cosas que nunca has hecho. Disfruta los pequeños detalles de cada día y ve al cine de vez en cuando si tu película favorita se proyecta. 
  Ten sexo con muchas personas

¿Y si tu próxima pareja es la definitiva? Disfruta tu soltería y cada vez que quieras, ten sexo. No te limites por el qué dirán, porque, recuerda, lo único que importa es tu placer, sólo recuerda, todo con protección y precaución.   Disfruta tus amigos Tal vez mientras estuviste en una relación no tuviste tiempo para ver a las personas que realmente están ahí siempre que las necesites. Ahora que no tienes pareja, disfrútalos, porque así ellos se darán cuenta lo importante que son para ti. Y recuerda, cuando tengas una nueva relación, no los olvides.    Deja de preocuparte por tu siguiente relación
A veces pensamos demasiado en el futuro y dejamos de ver el presente. Idealizamos una pareja, no podemos esperar a conocer el indicado, queremos pensar en los planes que haremos a su lado y lo hermoso que será la vida con la pareja que creemos especial, pero en este momento de tu vida, lo importante es lo que está pasando en ese instante, el duelo, las experiencias únicas y las metas que puedes lograr.  Supera el pasado

Muchas veces, ni siquiera hemos superado una relación cuando queremos iniciar otra. Comenzamos y nos damos cuenta que aún extrañamos a nuestra pareja anterior. Comparamos nuestras relaciones, el amor que sentimos, lo bueno y lo malo que hacen y muchas veces gana el viejo amor con el que ya vivimos lo mejor y peor de su personalidad.
    Define qué quieres en el futuro

Tal vez esta etapa no sea la idónea para tener una relación, así que si prefieres tener aventuras, viajes y emociones extremas, hazlo. Si quieres viajar a otro país, hazlo y si en tu futuro no está definido tener una relación, no lo hagas, sé dueña de tu futuro.  

 
Perdona

No sólo si tu relación pasada hizo cosas muy malas. Perdónate también. Reconsidera cómo te sientes y todo el dolor que tienes dentro, para después, intentar superarlo, si no, nunca lo harás.

     Quiérete
 Recuerda que lo más importante eres tú. No desperdicies tu tiempo en personas innecesarias, mejor preocúpate por ti, ve las cosas positivas, el ideal de tu cuerpo, tus anhelos, aquellas cualidades que debes resaltar, los errores que debes corregir, la persona que realmente eres y disfrútate como nunca lo has hecho. 
  Imagina a tu pareja ideal y destrúyela
 A veces nos imaginamos que esa persona de las historias de princesas y cuentos de hadas un día realmente llegará: “inteligente, con dinero, una persona divertida, responsable, detallista…” y la lista podría continuar al infinito, pero considera que eso que anhelas tal vez no exista o ni siquiera te guste cuando lo encuentres. Después de hacer la lista, rómpela y atrévete a descubrir personas que realmente existan y sean únicas.    
Conócete 
 Estuviste en una relación, saliste de ella y ahora es tiempo de saber qué te gusta, qué esperas de la vida, tus placeres culposos, tu música favorita y todo lo que sabes. Tal vez nunca lo has analizado, pero seguramente eres una persona con muchos secretos que ni tú misma te has contado. 
    
Reconsidera tu vida

 ¿Cuáles han sido tus logros? ¿Te gusta lo que haces con tu vida? ¿Te emocionas por las cosas que ocurren? ¿Sonríes? Deberías pensar en esto antes de iniciar una relación, replantearte lo que estás haciendo con tu vida, el rumbo que ha tomado y los sueños que debes realizar. Apunta tus logros y nunca los olvides, porque en los momentos que tengas crisis existenciales podrás voltear a verlos y recordar lo bien que lo has hecho.      Realiza algunos de tus más íntimos anhelos

 Este tiempo es para ti y puedes disfrutarlo alcanzando algunos de tus sueños. Si quieres subir a la azotea y ver las estrellas toda la noche, nadie te lo impide; si quieres viajar y conocer el mundo, hazlo y si buscas una aventura que cambie tu vida para siempre, persíguela. 
  Confía
 Cuando nos rompen el corazón, solemos olvidar que existen buenas personas que lo repararán. Debes aprender a distinguirlos y dejarte llevar, porque es cierto que tener el corazón roto es una de las peores cosas, pero si lo mantienes guardado para que nada le pase, no disfrutarás la vida. 
 
Date tiempo para ti
 Cuando descubras esas cosas que te gustan y que disfrutas, hazlas. Ejercítate, medita, lee decenas de libros, ve cientos de películas y no hagas nada que no quieras, porque no debes complacer a los demás, sino a ti. 
  Consiéntete
 Un día de spa, un día de compras desmedidas, los lentes de sol que has visto en ese aparador desde hace semanas, los tenis o la mochila que quieres… Si tienes dinero, cómpralos, gasta en ti, disfruta tu cuerpo, el sabor de las cosas. Emociónate con tu vida. 
     Aprende a ser autosuficiente
 Es el momento para que aprendas todo lo que puedes, que sepas ser independiente. Porque muchas veces esperamos que los demás nos ayuden, ahora es tiempo de que tú lo hagas: aprende a poner un clavo, carpintería, a cambiar una llanta y hasta encender el boiler, no sabes cuándo necesitarás de estas cosas. 
  
 Analiza los errores de tu relación pasada

 Tal vez esa relación acabó por tu culpa o por el de la otra persona. Tal vez tu pareja se cansó de tu posesividad o no quiso continuar obedeciendo ciegamente todo lo que le decías; probablemente no eras esa persona ideal que a veces piensas. Seguramente tú fallaste y, para no continuar con el patrón, es mejor que te detengas y hagas una lista de las cosas que inevitablemente debes cambiar.      Ten una cita con alguien que normalmente no saldrías
 Imagina cómo sería salir con esa persona con la que crees no tener nada en común… probablemente consideras que sería un poco extraño e incómodo, pero esto te ayudará a ampliar tu mente y horizontes. No quedarte en tu zona de confort y experimentar es lo que hace que puedas crecer.       
Emborráchate con él

 Por último, cuando conozcas a tu prospecto ideal, emborráchate a su lado, conócelo lo más libre que puedas. Sin restricciones. En ese momento aprenderás mucho de la otra persona, cómo es, sus anhelos, sueños y reirán como nunca; ideal para conocerlo verdaderamente.          ***

 Te puede interesar:   45 aventuras que sólo podrías vivir con el amor de tu vida

 Consejos para superar una ruptura amorosa