8 consejos científicos para mejorar tu memoria y aprendizaje en una semana

 
 Siempre pasa que a una semana de un importante examen o cualquier otra prueba de conocimientos, los estudiantes reúnen una enorme pila de libros, sacan la desgastada libreta y comienzan a elaborar interminables notas que olvidarán  al siguiente día. Si lo que describimos aún es tu método de estudio, lo mejor es que te vayas preparando para no aprobar, porque una investigación reveló que con este modelo de aprendizaje sólo estás perdiendo el tiempo. 
  
 Así lo comprobó Peter Brown, un destacado psicólogo de la Universidad de Washington, quien tras haber pasado 80 años de su vida investigando sobre el aprendizaje y la memoria, entendió cuál es el mejor proceso para asimilar nuevos conocimientos. 
 Antes de pasar a sus reveladoras ideas, será bueno remarcar que la anticuada estrategia de leer y releer libros de textos debe ser cambiada por la elaboración de diagramas, el uso de “flashcards”, que es una simple relación del concepto con una imagen en un pedazo de papel, y el constante autocuestionamiento de las cosas. 
  
 Si empleas estos básicos principios y le sumas los siguientes consejos, no habrá examen que pueda contra ti y por fin obtendrás la más alta calificación que siempre soñaste y tu memoria mejorará considerablemente. 
 – No releas tus notas 
  
 Las investigaciones de  Peter arrojaron que repetir una y otra vez la misma información no hará que se grabe en tu memoria, al contrario, hará que te confundas más. Cuando lees por primera vez el capitulo de un libro, haces el primer movimiento de abstracción y, bien o mal, aprendes algo. Pero al hacer la segunda lectura, tu cerebro cree que ya conoce dichas ideas y se pone en la postura de “esto ya lo sé”, lo que evita que hagas un proceso más profundo de comprensión. Nunca tendrás una lúcida memoria si no la dejas trabajar y la llenas de repeticiones. 
 – Hazte muchas preguntas 
  
 Después de hacer una única lectura, el paso siguiente es la autoevaluación . Puedes apoyarte por los cuestionarios que vienen al final del libro o hacerte tus propias preguntas. Cuando haces este acto, tu cerebro trabaja en “recuperar” la información que acaba de obtener y la pone al filo de tu memoria. Al preguntarte te obligas a dar una respuesta por ti mismo, la cual será el resultado de una profunda y original reflexión. 
 – Conecta lo que estás aprendiendo con algo que ya sepas 
  
 Relacionar objetos es una habilidad natural del ser humano; si ocupas esta estrategia para nuevos conocimientos, todo se hará más fácil. Este punto es muy sencillo: por ejemplo, si estás estudiando sobre historia, específicamente sobre la Segunda Guerra Mundial, puedes relacionar sus inicios con la pintura del “Guernica” de Pablo Picasso. La fecha en que fue pintada la obra y el inicio de la Guerra es la misma. 
 – Haz de tu información un dibujo 
  
 Es claro que no todos tienen el don del dibujo, pero no es necesario que hagas una obra abstracta por cada concepto que trabajes, basta con un simple diagrama, mapa conceptual o cualquier otro modelo que ordene la información de manera visual. Tu memoria recordará más fácil una imagen o símbolo que una gris y aburrida hoja llena de texto. 
 – Usa flashcards 
   
 Por si no las conocías, las llamadas flashcards son pequeñas fichas que siguen el modelo pregunta-respuesta. En un lado de la tarjeta debes escribir la interrogante del tema y al reverso desarrollar la información necesaria. Todo funciona mejor si le agregas una pequeña imagen que se relacione con el tema, recuerda que el sentido más importante del humano es la vista y entre más color tenga el objeto, será más atractivo. 
 – Evita saturarte de información y date un tiempo de descanso 
  
 Por más angustia que te provoque algún examen, no servirá de nada que pases ocho horas seguidas trabajando con la misma información. Repetir ésta, como ya se dijo,  sólo provoca que te confundas más y si logras memorizar algo es seguro que lo olvidarás en una semana. En cambio, prueba espaciar los momentos de estudios para no caer en la repetición. Varía en las estrategias de aprendizaje y sobre todo, relájate, no eres una máquina. 
 – Repasa durante varios días 
  
 Es muy común que los maestros traten de abarcar un tema por día, pero lo único que logran es el desaprendizaje. Puedes contrarrestar este problema dejando a la mano tus flashcards o diagramas cerca de tu lugar de estudio. No es necesario que leas su contenido, con sólo verlo tu memoria trabajará constantemente y traerá a ti las ideas que viste la semana pasada. Es como un rito de invocación. 
 –  No hay personas “matemáticas”  
 En las matemáticas sólo hay un resultado perfecto y no hay espacio para otras posibilidades, y al igual que este modelo numérico, hay estudiantes que sólo se enfocan en una materia o área de conocimiento porque saben que se les facilita por encima de las demás. Paulatinamente esto puede crear un problema, porque cuando se llegué al dominio de ese conocimiento, la persona se estancará. Caso contrario si se tiene una “mentalidad de crecimiento”, la cual resiste a los cambios, errores y nuevas ideas. 
 – 
 Si haces caso de estos sencillos consejos, tu memoria y aprendizaje serán muy eficientes. Ahora para ser más perfecto sólo necesitas trabajar con tu  creatividad. 
 *** Te puede interesar: 
 Trucos para mejorar tu memoria 
 Cómo tener memoria fotográfica en un mes