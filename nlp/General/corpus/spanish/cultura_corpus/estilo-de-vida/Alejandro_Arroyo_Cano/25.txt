30 diseños de tatuajes que todo fan de Joy Division querrá tener

 
 En algún momento nos hemos internado en un mundo de melancolía inagotable por alguna razón. En esos difíciles momentos, saber que no somos las únicas almas errantes que caminan sin rumbo en la oscuridad puede ayudar a aliviar nuestro dolor. 
 En la década de los 70, de un gris y triste pueblo de Mánchester, surgió una banda que plasmó todos esos sentimientos oprimidos en una nuevo modelo rítmico que se basaba en crudas baterias, un bajo rítmico y una guitarra que sutilmente le daba color al conjunto. Pero por encima de la música, se encontraba la mítica e intrigante figura de Ian Curtis, quien cantó con toda su alma los problemas que padeció a causa de sus  ataques epilépticos. 
  
 Si llegaste a este artículo es porque conoces bien la corta historia de Joy Division, pero si aún tienes dudas o quieres saber más sobre la banda, lo mejor que puedes hacer es ver la película “Control”, de Anton Corbijn, donde se recrea el nacimiento y caída de la más grande banda de post-punk. 
 Sin más rodeos, te mostramos los mejores tatuajes que cualquier fan querrá tener grabado en su cuerpo para recordar siempre que “el amor nos destruirá de nuevo”. 
 – 
  
   
  
   
 “A legacy so far removed, one day will be improved. Eternal rights we left behind, we were the better kind. Two the same, set free too I always looked to you, I always looked to you, I always looked to you”.  A Means To An End  
    
  
    
 La canción más popular de Joy Division es “Love Will Tear Us Apart”, que retrata la vida conflictiva que tenía Ian con su esposa debido a que un sorpresivo embarazo hizo que la pareja se casara con tan sólo 18 años de edad. Los problemas epilépticos de Curtis y su inmadurez en las relaciones fueron la mayor inspiración para el legendario tema. 
   
   
  
  
 “Asylums with doors open wide, where people had paid to see inside, for entertainment they watch his body twist behind his eyes he says, ‘I still exist.’ This is the way, step inside”. Atrocity Exhibition 
   
   
   
 La portada del disco “Unknown Pleasures”, que es actualmente el máximo símbolo de Joy Division, fue creada por el diseñador Peter Saville, quien tomó la ilustración de la Enciclopedia de Astronomía de Cambridge de 1977. La imagen es el registro de unas ondas espaciales que detectó un radiotelescopio en los 70. Tal vez los placeres desconocidos aún sigan esperándonos allá arriba, en el universo. 
 
   
   
   
   
 “Here, here, everything is by design, everything is by design. Here, here, everything is kept inside. So take a chance and step outside, your hopes, your dreams, your paradise. Heroes, idols, cracked like ice”. Autosuggestion 
  
   
  
   
   
 Tras el suicidio de Ian Curtis, el resto de los integrantes de Joy Division tomaron la difícil decisión de terminar el proyecto de post-punk por respeto a su ex líder. De este modo, Bernarnd Sumner, Peter Hook y Stephen Morris crearon New Order, otra banda que en un par de décadas se volvió pilar del movimiento del new wave y que hasta le fecha sigue vigente. 
   
   
   
  
  
 “This is the room, the start of it all, no portrait so fine, only sheets on the wall, I’ve seen the nights, filled with bloodsport and pain, and the bodies obtained, the bodies obtained”.  Day of the Lords  
 Seguramente conoces todo sobre Joy Division, pero ¿sabes toda la historia musical que está detrás de ellos? 
 *** Te puede interesar: 
 Frases de canciones más usadas para tatuajes 
 Los últimos días de Ian Curtis.