Razones para hacerte un tatuaje

 
 Siempre he ido en contra de las corrientes. Si hoy todos visten de negro, regreso a casa y busco mis prendas más chillantes, las más llamativas. Si todos andan en bicicleta, desempolvo mis patines y pago el precio de traer semanalmente las rodillas raspadas y los pantalones rotos. ¿Por qué debo de parecerme a los demás? 
 No importa cuál sea la tendencia del mundo, mi espíritu libre me impide atarme a los constructos sociales que la gente llama moda, además mi asimetría corpórea me impide relucir de manera correcta todo lo que la gente suele ponerse sobre sus cuerpos. Esto incluye también a los tatuajes. 
  
 Una vez soñé que me encontraba en el último día de mi existencia y la imagen concluyente que mis ojos apreciaron fue la de un pedazo de piel garabateado, flojo y deforme. Ni siquiera pude distinguir cuál era el grafo que decidí plasmar muchos años atrás por las arrugas tan marcadas que me dejó el tiempo de recuerdo. Desde ese entonces huía de cualquier persona que quisiera tentar contra mi perfecta y simple piel. 
 Pero la vida nos pone enfrente lo que necesitamos para derribar todos los prejuicios moralistas. Durante un viaje a la bella costa de Oaxaca, conocí una mujer que venía de tierras lejanas. En una noche me contó toda su vida y me mostró su más preciado tesoro: el antiguo símbolo nórdico del “Valknut” que estaba tatuado en su nuca. 
  
 Me contó que aquel dibujo representa el poder que el dios Odín tenía para establecer vínculos con la mente humana y los nueve mundos que prenden del Yggsdrasil en la cosmogonía nórdica. Pero sobre todo, mantiene unido el espíritu de un guerrero caído a nuestra realidad, en su caso, a su padre fallecido. 
 La noche pasó tan rápido y entre charlas interminables acompañadas de un único beso, el amanecer nos envolvió y el llegó momento de partir. Con un pequeño momento entendí que los tatuajes pueden simbolizar algo más que una simple tendencia, tan profundo,  personal y metafórico como tú decidas. Ahora estoy en espera de tatuarme mi “Valknut”, como el guerrero vencido por el amor, que desde el otro extremo del mundo trata de sentirse más cerca de aquella persona. 
  
 Si lo ves bajo esta lente, estás serían las razones reales para hacerte un tatuaje. 
 – 
 Te recordará por siempre alguna etapa de tu vida 
  
 No quiero olvidar nunca lo que pasó esa noche y seguramente tú también tendrás un momento especial que quieres que perdure en tu recuerdo. Si encuentras la imagen que simbolice ese día perfecto, ¿por qué no tatuarte? 
  
 – Creará un puente invisible con otra persona 
  
 Tal vez logré que mi mensaje llegara hasta ella y si no es así, creo fielmente que existen puentes invisibles por todo el mundo que nos unen sin importar la distancia. Tatuarte algo que te vincule con alguien es la mejor manera  para llegar a él o, al menos, para dar el primer paso del camino en un mundo desconocido, pero posible. 
  
 – El tiempo no borrará el sentimiento que un día sentiste 
  
 Cuando hayan pasado muchos años y nuestra cabeza empiece a fallar por el inevitable factor de la edad, siempre existirá esa imagen que nos traerá de vuelta esa perfecta sensación que experimentamos en nuestra juventud. 
  
 – Te hará sentir especial 
  
 En un mundo lleno de apariencias, portar un tatuaje que es una metáfora del amor te eleva sobre todas las personas. Si tú estás consciente de ello, qué importa lo que los demás digan, porta tu tatuaje como si fuera el mayor premio que te dio la vida.   – 
 – Es más duradero que el amor 
  
 Puede que hayas conocido el amor, pero también es posible que en algún momento tenga que acabar. En vez de aferrarte al pasado que más que placer, provoca dolor, porqué no mejor recordar lo perfecto que fue en el momento que por él o ella decidiste tatuarte. Ni siquiera el tiempo podrá borrar su recuerdo de tu piel. 
  
 –   – 
 Ya se ha dicho que el amor mueve montañas, ¿necesitas un poco de inspiración para tu próximo  tatuaje de amor ? 
 *** Te puede interesar: 
 Tatuajes minimalistas con grandes significados 
 20 tatuajes que te recordarán lo que es verdaderamente importante