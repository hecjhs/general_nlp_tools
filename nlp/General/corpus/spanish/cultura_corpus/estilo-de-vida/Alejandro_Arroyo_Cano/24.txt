25 cosas que las mujeres solteras están cansadas de escuchar

 
 Se dice que una mujer se completa cuando se junta con un hombre. Es aquí, en este acto de fusión, cuando la condición femenina se reafirma por el correcto camino de lo que debe ser. Así lo demuestra la historia, las artes y la poesía, pero lo cierto es que todo es una mentira. 
 Al día de hoy se pone en duda si se realizó de manera correcta la búsqueda del equilibrio, de la equidad o  si será que el mundo aún obliga a las mujeres a ocupar esos lugares que ya están asignados de antemano y ellas sólo se acomodan en ellos pensando que fueron las protagonistas de esa decisión. 
  
 Se puede reflexionar sobre dicha cuestión en las ideas que dan cuerda al mundo, por ejemplo, si es correcto que una mujer viva sujeta a otra persona o disfrute de la libertad a pesar de que carezca de la figura omnipotente del hombre. También se puede sospechar de lo contrario, aquella idea de que el hombre necesite de la mujer para reafirmar su papel en el mundo y en la naturaleza, y que sin ellas, su camino fuera hacia el ocaso. 
 Este cúmulo de pensamientos las han trabajado mujeres imponentes, tan grandes como el mundo mismo. Una de ellas fue Simone de Beauvoir con su libro “El segundo sexo”, donde profundiza sobre la condición femenina en un decrépito y tendencioso mundo. Sea cual sea su postura o la tuya, lo mejor es vivir bajo las ideologías que se eligen responsablemente y ser libre.   
 Si por elección propia o azares del destino te encuentras en un momento de soltería, aquí se encuentran las frases que cada vez que escuchas te causan comezón por todo el cuerpo, porque tal vez los demás sigan sin entender que por momentos es mejor estar así. 
 – Tengo un amigo que seguro te va a encantar 
  
 El momento en el que la gente que nos rodea es tan caritativa que nos quiere presentar personas con las que, por supuesto, tampoco queremos salir. Si es tan bueno el amigo, ¿por qué también está soltero? 
 – ¿Otra vez vienes sola? 
 De pronto todo el mundo tiene pareja y eres la única que asiste a las reuniones con la etiqueta de soltera. Al parecer, tus amigas ya olvidaron que se tardaron el doble de tiempo en encontrar a su novio. 
 – Él sería una buena pareja 
 Generalmente lo dicen las madres, pero es obvio que ven el mundo con un lente de hace 30 años. Alguien que las desempolve, por favor. 
 – Ya será el otro año 
  
 Llega el fin de año y con ello los buenos deseos. Dale gracias al mundo por recordarte que fue un año más sin éxito. 
 – Si no sales con nadie es porque eres… 
 La frase concluye con: aburrida, extraña, perezosa, impaciente, caprichosa y todos esos adjetivos que son exactamente lo contrario a lo que en realidad pasa. Si no sales con nadie es porque no quieres. Así de sencillo. 
 – Tal vez si fueras más amigable 
 No es tuyo el problema, es del mundo que no sabe descifrar tu enigmático, pero profundo carácter. 
 – Yo creo que te convendría un cambio de look 
  
 Como si cambiar las prendas hiciera que el hombre ideal apareciera mágicamente. Ayuda a retener miradas, pero ¿qué pasa con lo que hay debajo de la piel? 
 – Hay muchos peces en el mar 
 Entonces se supone que tú debes de cazar a la presa, ¿no tenían que conquistarte y encantarte a ti? 
 – No creo que sea para ti 
 Primero quieren que salgas con quien se te ponga enfrente, después lo haces y llegan los reproches. El mundo está de cabeza. 
 – ¿Y no extrañas tener novio? 
  
 Es obvia la respuesta, pero si estás así es por una cosa, que al parecer todo el mundo olvida. 
 – ¿Qué no quieres tener hijos? 
 Si quieres o no tener hijos es tu decisión. ¿Qué tiene que ver el tema ahora? 
 – Es que nadie te merece 
 Tampoco es bueno que te quieran ver como la mujer perfecta. Tú sabes quién eres y lo que necesitas. 
 – Deberías salir más de noche 
  
 A los lugares que se sale de noche es sólo para pasar el rato. Sería imposible encontrar a tu príncipe azul en las sombras. 
 – Si no te apuras, vas a ser la señora de los gatos 
 En primer lugar, no tiene nada de malo vivir en una casa llena de gatos, y en segundo, nunca serás de color amarillo, sin dientes ni amargada. Échenle más imaginación. 
 – Todas tus primas ya se casaron 
 Sí, pero hay que recordarles a quienes te lo dijeron que ellas lo hicieron a las 17 años y ahora están divorciadas y cuidando hijos. 
 – ¿En serio eres soltera? 
  
 “Sí, ¿algún problema?” 
 – Tranquila, va a llegar cuando menos te lo esperes 
 Ya no eres una niña para que te recuerden los cuentos rosas de tu infancia, eso ya lo sabes. 
 – Era tan bueno, ¿por qué lo cortaste? 
 Es obvio que las demás personas no conocían la realidad de las cosas, pero sobre todo, ¿para qué recordar algo que ya pasó? 
 – Hija, ¿y el novio para cuándo? 
  
 Hace unos años te encerraban bajo llave en tu cuarto y ahora ya hasta te quieren meter a una rifa. ¿Quién los entiende? 
 – Aún no entiendo porqué no tienes novio si estás muy bonita 
 Es posible que tú tampoco lo entiendas, pero así es la vida y hay que aceptarla. 
 – Tienes que pensar en tu futuro, ¿o quieres estar sola siempre? 
 Dile a tus amigas que dejen de ser tan trágicas, estar soltera no es sinónimo de estar sola. Siempre habrá algún amigo que sin ser tu pareja, estará en las buenas y en las malas. 
 – No es tu culpa, es de ellos 
  
 Que dejen de echarle la culpa a los demás, otra vez, si no estás saliendo con alguien es porque seguramente así tú lo quieres. Querer es poder. 
 – No deberías ser tan exigente 
 Si al decir exigente se refieren a que buscas a la persona que te ame sin pedir nada o casi nada, sí, están en lo correcto. ¿Acaso no se trata de eso? 
 – ¿Ya probaste Tinder? 
 “¿Acaso tengo una marcada cara de urgencia?”  La peor frase de todas. 
 – ¿Y nunca han pensado en andar con él?  Cuando te ven mucho tiempo acompañado de un buen amigo y empiezan a decir que se ven “bien” juntos. Alto, un hombre y una mujer sí pueden ser muy cercanos sin tener que ser pareja. 
 – Después de recordar las irritantes frases que toda mujer odia, lo que necesitas es recobrar los buenos ánimos de una manera natural. ¿Gustas? 
 *** Te puede interesar: 
 Razones para dejar de buscar a tu alma gemela 
 Frases que te ayudarán a despertar en los días más difíciles de tu vida