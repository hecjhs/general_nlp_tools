50 cosas que estamos cansados de escuchar en nuestros veintes

 
 Como si nunca se hubieran hubieran emborrachado un fin de semana completo. Como si no hubieran sentido unas ganas enormes de coger y tener que recurrir a actos desvergonzados de seducción para pasar la noche con alguien. Déjenme dormir más tiempo que apenas son las 10. Para qué se molestan en decirme lo que tengo que hacer, un minuto después olvidé lo que me dijeron. 
 Estoy bien con mis vicios, con mis malos hábitos que me conducen a la muerte –así como dicen–. Tal vez no sea razonable, pero me la paso muy bien, increíblemente bien. ¿El futuro? Ese siempre termina por acomodarse y las cosas se arreglan solas. 
 Llevo seis años haciendo lo mismo y aún sigo vivo. Tengo más amigos y más historias que contar. No veo cuál es el problema. Además, no hago nada malo. Si quieren enojarse por algo que sea por algo importante. Aquí todo está bajo control. 
 Me sé de memoria sus frases, al derecho y al revés. ¿Que cuáles son? Son: – 
  
 No te emborraches 
 Para cuándo acabas la tesis 
 Usa condón 
 –  
 Tápate bien al salir 
 Tú no sabes qué es el amor 
 No te juntes con él o ella 
 –  
 Levántate temprano. 
 Tenemos que hablar Compórtate –Lávate los dientes.Estás saliendo mucho.No puedes salir–Pórtate bien 
 Se acabó el alcohol 
 Llegas tarde otra vez –  
 No hay fiesta. 
 Llega temprano a la casa 
 ¿Todavía no acabas la escuela? 
 –  
 No tires basura en la calle. 
 ¿20? Te ves más pequeño, pequeña. 
 No lo vas a lograr 
 –  
 No estés de caliente. 
 Mañana es lunes. 
 No porque mañana hay escuela 
 –  
 Rasúrate 
 Pensé que iban a estar juntos siempre 
 Ya ponte a hacer algo de tu vida 
 – 
  
 No fumes. 
 Eso te hace daño 
 Él o ella no es para ti 
 – 
   Deja de beber. 
 Algún día vas a ser papá y vas a ver 
 Tienes que ayudar en la casa 
 –  
 Escombra tu cuarto 
 Bájale a la música 
 Bájale a tu desmadre 
 –  
 Busca un trabajo. 
 ¿Cuándo te casas? 
 Ya madura  
 – –  
 No desperdicies la comida 
 Tienes que pensar en tu futuro.Deberías ser más sociable 
 [Conoce las películas para una juventud rebelde, desnuda e imperfecta dando click aquí .] 
 –   ¿Vas a salir así? 
 No lo vas a lograrLo entenderás cuando seas grande 
 – 
   Ponte ropa. 
 Mañana hay examen 
 Ya no te drogues 
 Deberíamos darnos un tiempo. 
 Después te vas a arrepentir 
 – ¿Ven? Mejor hay que ahorrarnos la plática para cuando pase algo serio de verdad. 
 – 
 En el siguiente enlace puedes ver las 25 primeras veces que debes tener antes de los 20. También hay que reconocer que no siempre son cosas buenas, en el siguiente link cuáles son los errores que se comenten a los 20 y terminan destruyendo los 30. Aquí hay canciones para acompañar tu fiesta de los 20.