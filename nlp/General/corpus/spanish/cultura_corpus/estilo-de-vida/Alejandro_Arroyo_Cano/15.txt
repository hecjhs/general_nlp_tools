Cosas sobre los hombres y su sexualidad que siempre hemos entendido mal

 
 Todos los hombres agradecemos, de antemano, que te animaras a abrir este texto, porque al igual que tú, me refiero a todas las mujeres, la figura masculina está envuelta en ilusiones sobre lo que nos gusta y deseamos en el mundo. No lo negamos, amamos el sexo, pero no debe ser un gran dilema porque sabemos que ustedes también lo aman. El posible problema es que somos señalados como unas máquinas sexuales , y tal vez sí lo seamos, pero también somos seres afectivos y con buenos sentimientos. Los siguientes puntos te revelarán un poco de la esencia de ese macho salvaje que tanto amas y odias. 
 Para acompañar tu travesía sexual y espiritual, sería una buena idea que ambientaras tu lectura la siguiente canción y así entrar en calor. Él es Marvin Gaye y fue una de las personas más románticas de la historia musical. 
  
 Ahora sí, agárrate. 
 – Siempre queremos sexo 
  Se dice que cada cinco segundo un hombre en el mundo está pensando en el sexo. Con este dato se afirma, supuestamente, que nosotros somos unos seres maniacos y hambrientos que no pueden dejar de pensar en grandes pechos y unas nalgas bien formadas. Pues aunque no lo crean, un estudio realizado en 2011 descubrió que un número considerable de hombres no quieren tener sexo en absoluto. Además comprobaron que los pensamientos de los hombres se centran en la comida y en lo que necesitan hacer para lograr dormir. Sí, acúsennos de que amamos comer y dormir, pero nunca que somos unas bestias sexuales . 
 – Preferimos el sexo casual que las relaciones formales 
  
 Una rotunda mentira. Este humilde articulista que está inmerso en charlas desvergonzadas con otros hombres puede afirmar que todo gira en torno a cómo conquistar a una mujer. Y si lo hacemos, no es sólo porque queremos tener sexo, todo lo contrario, queremos una relación formal que involucren los sentimientos más puros… Si no me crees, en 2010 un estudio científico reveló que 2 de cada 3 hombres podrían ser felices en una relación formal que no incluya el sexo. 
 – Cuando tenemos una erección es porque queremos sexo 
  
 En estas pláticas de hombres siempre existe una anécdota en la cual un pene se paró en el momento menos esperado y como están de moda los pantalones un poco ajustados, no falta la mujer que se siente insultada por ver un bulto entre las piernas. La historia siempre viene acompañada de  “fue un accidente, pero cómo se lo explicas”. Mujeres, los hombres tienen varias erecciones al día en situaciones no sexuales. La excitación sexual masculina se manifiesta en una variedad de formas físicas y psicológicas, incluyendo aumento del ritmo cardíaco y la respiración pesada. El pene erecto no es la única forma. 
 – La eyaculación no es sinónimo de un orgasmo. 
  
 Este es un punto que debería agradarles, porque es claro que después de eyacular, el pene regresa a su estado de reposo, pero si no lo hace, puede seguir de pie mucho tiempo más. Lo más importante a remarcar es que se puede llegar al punto máximo de placer sin arrojar lo que se tiene dentro.  Una sexóloga comentó que “para muchos hombres el orgasmo sin eyaculación les permite ser multiorgásmicos. Una vez que los hombres aprenden a tener orgasmo sin eyaculación, rara vez querrán volver al camino tradicional”. 
 – Entre más dure el sexo, mejor 
  
 Este punto va para hombres y mujeres.  Un estudio en el Journal of Sexual Medicine encontró que ambos sexos prefieren encuentros más cortos  (de siete a 13 minutos) a episodios de larga duración. “Un rapidín puede ser un momento sexual intenso y repentino sin pedir la motivación o justificación”, dice el terapeuta sexual Pepper Schwartz. 
 – Nos gusta tener el control sobre la cama 
  
 Esta frase no siempre se cumple. Al igual que las mujeres, nos gustaría ser perseguidos por toda la habitación por una mujer con cara de ángel travieso, que en el momento que nos alcance nos arrastre hasta la cama, nos despoje de las ropas y tome el control. Esa idea de que el hombre tiene que iniciar el acto sexual ya se empolvó desde hace mucho tiempo. 
 – Siempre somos más experimentados y tenemos una larga historia sexual 
  
 Conozco a muchos hombres que en toda su vida sólo han tenido un par de parejas sexuales, con mucha suerte. Es claro que existen galanes que enfocan su vida a conquistar y saborear las dulces mieles femeninas, pero son casos contados. Durante toda mi formación académica sólo conocí a un hombre con estas características y todos los demás andaban oscilando en una sola relación formal con sexo. No somos tan malos como se piensa. En realidad cuesta mucho trabajo seducir a una mujer, porque hay que ser sinceros, la mayoría estamos bien feos. 
 – Si quieres consentir a tu pareja, tal vez necesites conocer los errores que tienen las mujeres durante el sexo. 
 *** 
 Te puede interesar: 
 Posiciones sexuales que debes hacer por lo menos una vez en la vida 
 Fantasías sexuales que todas las mujeres queremos cumplir