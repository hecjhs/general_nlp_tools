Razones para dejar de buscar a tu alma gemela

 
 Se piensa que en este vasto mundo, perdida entre la multitud, se encuentra la persona justa para nuestro corazón. Aquella que viste con su esplendor nuestra alma desnuda, herida por la falta de caricias y sonrisas. Eso dicen, pero en realidad nunca la he encontrado. 
 Llevo un cuarto de siglo viviendo con los ojos abiertos, volteando en cada esquina, buscando debajo de las piedras, en los rincones oscuros e iluminados. Incluso paso noches en vela recorriendo las calles solitarias mientras espero su llegada, porque al igual que yo, la otra persona tiene que sentir una misteriosa necesidad de salir a lo desconocido para encontrarse conmigo. 
  
 Si es mi alma gemela, el complemento perfecto para mí, se supone que tiene que sentir lo mismo que yo, debe tener mis mismos pensamientos. Entonces entendería que es mejor encontrarnos después de las seis, cuando el sol empieza a ocultarse y el día se vuelve más interesante, en lugar de a mediodía, cuando las calles están saturadas y todo es asfixiante. 
 Por largos años creí que no necesitaba hacer nada extraordinario porque el destino de eso se trata, ¿cierto? No importa lo que hagas, si una persona está destinada para ti, caerá del cielo. Entonces me dediqué a hacer mis cosas, sin preocuparme mucho sobre el amor, viviendo con la ferviente idea de que, algún día, ella llegaría. 
  
 Hasta la fecha ella aún no ha llegado y empiezo a creer que nunca lo hará . Aún así seguiré caminando por las noches, porque sin esperarlo, recorrer la geografía nocturna de las calles me ha hecho pensar en muchas cosas, entre ellas, algunas razones para dejar de buscar a mi alma gemela. 
 – Si hay alguien para ti, es diferente a ti  
 No dudo que algún día podremos encontrar a alguien con quien compartir la vida , pero ahora se entiende que será diferente a nosotros. Si existieran las almas gemelas, seguramente ya la habríamos encontrado desde hace mucho tiempo. Lo mejor es renunciar a esa idea y ser más flexibles. Al igual que yo, si te inundó una fatiga de tanto esperar, lo mejor es hacer nuevas cosas, porque tal vez no haya alguien idéntico a ti, pero sí alguien que contraste armoniosamente contigo y que está esperando. 
 – Sería aburrido vivir con alguien idéntico a ti 
 Una vez que se acepta que no existen las almas gemelas, se puede reflexionar, desde un punto más objetivo, las desventajas que tendría encontrarse a alguien así. Se dice que lo mejor de la vida es aprender nuevas cosas y así salir del infinito círculo de lo cotidiano. Si encontráramos alguien idéntico a nosotros, pronto pensaríamos que simplemente nos reflejamos en el espejo y las posibilidades de conocer nuevas cosas serían igual a cero. ¿Acaso no es eso aburrido? 
 – Si existe “el amor de tu vida”, no significa que debas encontrarlo  
 Tal vez, en esta inmensidad, sí exista una persona perfecta para nosotros, pero aún así, eso no significa que debamos estar con ella. Vivimos más de la mitad de nuestra vida siendo solteros y, siendo honestos, funcionamos mejor. Si llegamos hasta este punto de nuestras vidas con tantos logros, fue por nuestro propio mérito. 
 – No necesitamos complementos Siguiendo con la idea pasada, ¿quién necesita que alguien más te complete? Estamos perfectamente bien así. Nuestro ser ha crecido demasiado como para esperar a alguien que no tiene mucha razón de ser. También somos perfectos solos. 
 – Te pueden leer la mente  
 Imagina que encuentras un ser idéntico a ti: seguramente tendrías miedo de que fuera tan igual que pensara lo mismo que tú. Eso sería como vivir en una prisión mental, no podrías hacer algo sin sentir cómo penetran en tus pensamientos para conocer la razón de tus actos. 
 – Lo mejor está por venir y no va a ser un amor 
 De tanto esperar, ya hasta se te pasó el tren, metafóricamente hablando. Seguramente has visto pasar frente a ti muchas oportunidades, pero por estar en un estado de quietud las has desaprovechado, creyendo ilusamente que puede aparecer milagrosamente en el momento que des un paso. 
 Aceptar que no existen las almas gemelas te quita un peso de encima y te deja vivir libremente. En un futuro, cuando no busques a la pareja ideal, tal vez encuentres algo mejor . 
 *** Te puede interesar: 
 Pláticas que debes tener con la persona de la que crees estar enamorada 
 Cómo volver a confiar en el amor después de una gran desilusión