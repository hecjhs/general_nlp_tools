10 actitudes que tienen todas las personas que se niegan a crecer

 
 Crecer no implica dejar de divertirse, pero sin duda exige darle prioridad a otras cosas que a un hedonismo extremo que ni siquiera Diógenes recomendaría. Todos continúan emborrachándose, mostrando celos y cierto egoísmo, pero a un nivel mucho menor que cuando la fiesta, el sexo y el dinero son lo único en su mente. Hay una época en la juventud en el que comenzamos a ver los cambios en gente cercana a nosotros, esos amigos que parecen más serios y calmados, que ya no salen cada viernes y cuya percepción de la vida parece contradecir el actuar que tuvieron por años. También están los que sabemos que se encuentran lejos de ser llamados “personas responsables”. 
 “Con el tiempo, intentar algo nuevo es más difícil y en muchos casos el miedo al fracaso paraliza, pero al comprender que fracasar no es debilidad, sino experiencia, es mucho más fácil levantarse e intentarlo de nuevo”.  La sociedad actual aplaude la inmadurez, pero algunos lo hacen bajo una lupa específica. Las celebridades que han llegado a la fama por mostrar excesos y ser aplaudidos nunca serán tomados en serio realmente (exceptuando a Donald Trump), pues aunque el espíritu de la juventud siempre sea la llama más viva entre las personas, llega un momento en el que la mentalidad nos obliga a dejar ciertas cosas atrás. Puede que uno nunca se sienta plenamente como un adulto, pero si los siguientes puntos ya no te identifican totalmente, vas por buen camino. 
 – Toman mucho 
  
 Durante la preparatoria muchos buscan tomar siempre que sea posible, en la universidad pueden descontrolarse y durante los primeros años laborales los grupos de amigos intentan reunirse por lo menos un día a la semana para una noche de plática con algunos tragos. El problema es cuando todos se mueven al mismo ritmo, cada vez tomando menos, y uno se niega a cambiar, no se controla e incluso incomoda a los que fueron sus mejores amigos. No dejes que la inmadurez te haga perder a quienes conoces desde hace tantos años, y peor aún, que afecte tu futuro en la escuela o el trabajo. 
 Aprende cómo se ven las bebidas alcohólicas bajo el microscopio . 
 – Se la pasan de fiesta 
  
 Hay épocas en las que el cuerpo aguanta tres días de fiesta a la semana sin problema, pero también existe un límite. Incluso si de alguna forma mágica tu organismo no resiente los días de fiesta, forzar los planes y buscar en todos lados algo que hacer, puede indicar que no estás feliz con la vida que estás llevando. No llegues al punto en el que te encuentras rodeado de personas mucho menores que tú, mirándote incómodamente porque ya no encajas ahí. 
 – No quieren independizarse 
  
 Muchas personas no pueden mudarse por cuestiones económicas y sabemos que la situación actual es peor que nunca, pero hay quienes optan por quedarse en casa simplemente porque tienen todo lo que quieren y no dan nada a cambio. No cocinan, no lavan su ropa, no arreglan la casa ni aportan algo productivo a su hogar cuando tienen todos los recursos para hacerlo. Salir de la zona de confort es difícil, pero al hacerlo te obligas a madurar en aspectos que antes ni siquiera imaginabas. 
 – Son rencorosos 
  
 Aprender a dejar ir es una de las cosas más difíciles pero más necesarias en la vida adulta. Llegar a un trabajo y tomarte las cosas personales es lo peor que puedes hacer, y continuar enojándote con tus familiares, amigos y conocidos por cualquier cosa es simplemente infantil. No todo gira alrededor de ti y no todo el mundo busca hacerte daño de alguna forma. Si necesitas aprender a dejar el rencor atrás, tal vez estas 6 formas de cerrar ciclos según la psicología te pueden ayudar. 
 – Idealizan muchos escenarios 
  
 Las cosas no suceden como nos dijeron cuando éramos niños, y creer que esos escenarios de ensueño sucederán puede afectar gravemente nuestra vida personal. Idealizar una pareja perfecta puede llevar a una relación tóxica, un trabajo perfecto sin responsabilidades no existe y en general, el que la vida te dé todo sin pedir algo a cambio es lo que sucede en películas, pero no en la vida real. No se trata de dejar de soñar, pero sí hay que afrontar las cosas como realmente son. 
 – Consiguen amigos más pequeños 
  
 Evitar madurar puede llevar a algunas personas a buscar amistades que estén pasando por lo que uno pasó años atrás. Querer regresar a una edad idílica es tentador, pero nada recomendable, todo termina siendo un espejismo de lo que ya no será. La edad muchas veces no importa, pero si el propósito de buscar amistades más jóvenes es sentir que tienes su edad, sólo te frustrarás al llegar a la conclusión de que hay cosas que se viven una vez y nada más. 
 – Tienen trabajos sin responsabilidades 
  
 Hay que buscar el trabajo de nuestros sueños, incluso se dice que si haces lo que amas nunca trabajarás un día en tu vida. Pero esto es algo distinto. Tomar ciertos trabajos para evadir responsabilidades es una señal de inmadurez muy grande. Esto va más allá de los trabajos de oficina, quienes son dueños de su propio tiempo y hacen trabajos freelance saben que no se trata de hacer lo que quieren, sino de saber administrar las distintas responsabilidades que tienen. No se trata de sentirse muertos por dentro en un empleo que no te satisface, sino de equilibrar las responsabilidades con lo que eres capaz de hacer. 
 – Tienen miedo de hacer las cosas 
  
 Quienes se niegan a crecer ponen pretextos para intentar las cosas. Salir de casa, conseguir otro trabajo, viajar y más. Los que desean las cosas saben que se deben hacer, no basta con imaginar. Con el tiempo intentar algo nuevo es más difícil y en muchos casos el miedo al fracaso paraliza, pero al comprender que fracasar no es debilidad, sino experiencia, es mucho más fácil levantarse e intentarlo de nuevo. 
 – Descuidan sus relaciones 
  
 Está bien no querer tener una relación, en eso no hay nivel de madurez, pero quienes aceptan entrar en una relación y no se comprometen, no expiden confianza para nada. Al comprometerse con alguien más se deben establecer puntos en común y no dejar cosas al aire para después aprovecharse de la situación. Sabemos que la gente no es pertenencia de alguien más, pero creer que no se debe dar ninguna explicación a una pareja es exactamente lo contrario de una relación. 
 – No ahorran 
  
 Un gran error es vivir el presente y nada más, creer que el futuro es completamente incierto es una gran falacia. No importa la situación, siempre hay que mantener la vista hacia el frente y ser precavidos. Somos una generación que ha perdido muchas prestaciones y que no está pensando en lo que sucederá a largo plazo. Existen expertos que culpan a nuestros padres y a nuestra época, pero queda en nosotros cambiar eso, crecer, e invertir en lo que ni siquiera tenemos claro. 
 – 
 Si deseas cambiar eso, podrías ver las señales que indican que debes salir de casa de tus padres y después comenzar a inspirarte con las cosas que todos aprenden cuando viven solos.