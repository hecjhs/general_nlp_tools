El código de conducta de los papás en Facebook

 Cuando nuestros padres, madres y tíos tenían entre 15 y 30 años de edad, las tecnologías de comunicación e información no habían variado mucho en algunas décadas y vivían sus vidas con una conexión de teléfono, cartas, fotos impresas y teléfonos de moneda para reportarse dónde andaban de fiesta o para avisar que no iban a llegar a dormir. Internet , los mensajes de texto, tomar fotos de tu cereal para informarle al mundo el dato inútil de qué marca de cereal consumes, y avisar en tu muro de Facebook el número de tacos que te comiste ayer, eran cosas que ni en las películas de ciencia ficción hubieran visto tus padres hace 30 años. 
 Los teléfonos celulares comenzaron a comercializarse a mediados de la década de los 80, pero no eran muy populares debido a su gran tamaño y su elevado precio; quienes podían comprarlos tenían que ir cargando por toda la ciudad un teléfono enorme, como los que salen en las películas de la guerra de Vietnam, tomarlo con ambas manos para usarlo y esperar que la pila no se agotara en menos de una hora. Es por eso que era casi imposible que tu padre tuviera un teléfono móvil durante su vida, y si lo hubiese tenido, ten por seguro que sólo servía para hacer llamadas. 
  En la actualidad, tenemos nuevas formas de comunicarnos, y con éstas, se han venido creando algunos códigos de conducta no escritos de cómo es que se usan y las mejores formas para convivir, lo anterior es porque para muchos la Red es una extensión de su mundo, donde interactúan y comunican con personas, conocidas o desconocidas. De igual forma la posibilidad de colgar imágenes ha generado códigos de comportamiento que poco a poco todos hemos ido construyendo, como en cualquier cultura ocurre. 
 Lo anterior no quiere decir que haya un Manual  de Carreño o de buenas costumbres, nada más falso que eso; pero hay algunas situaciones que los papás o personas que no vinieron al mundo cuando ya estaba conectado, hacen que pueden ser un poco irritantes y de vez en cuando chuscas. 
 El código de conducta que quizá tus padres tengan, puede ser que te avergüencen o te irriten. Estas son algunas de las cosas que hacen tus papás en Facebook y que quizá no te gusten. Te puedes arrepentir de publicar tus tonterías 
  Esto ocurre con frecuencia, se te hace fácil publicar tus planes o tus aventuras nocturnas, y si es que tienes a alguno de tus papás en Facebook, al verlo se sienten con la confianza y libertad ya sea de reprimirte o jugarte una broma, lo cual será muy divertido para tus amigos que vean la publicación. 
  Que te demuestren su amor en tu muro 
  
 Esto también ocurre con regularidad, dejándote mensajes tiernos o melosos sobre lo especial que eres para ellos. Es el equivalente  digital de cuando frente a tus amigos te comenzaba a besar la mejilla y a hablarte como bebé. Se conmueven con imágenes que contienen frases motivacionales o reflexiones 
  
 Hay un flujo inmenso de imágenes con buenos deseos y diseños sencillos por toda la red. Son el equivalente a las imágenes en las que cada año un amigo incómodo te etiqueta deseando feliz Navidad junto con 100 personas más que comentan. A veces, tus padres pueden publicarlas en tu muro y si no les prestas atención, mínimo con un like , puede haber reproches de “¿No te gustó la imagen que te publiqué?” 
  Te dan like en todas tus publicaciones y las comentan sólo por comentar 
  
 Tienen el imaginario de que todo lo que ocurre en su timeline , es de tal relevancia que deben interactuar, ya que creenm que las demás personas se pueden molestar si no les demuestran su aprobación por medio de algún mecanismo como el comentario o el like. 
 Se molestan si tu primo comparte contenido que puede ofender 
  
 Cuántas veces les han preguntado sus padres, ¿por qué fulanito puso eso en Facebook? ¿No puedo hacer que ya no me salgan sus fotos que no entiendo? Esto ocurre por la misma situación anterior. Creen que todo lo que aparece en su página está dirigido hacia ellos y eso puede llegar a ofuscarlos. 
 Subir fotos incómodas y etiquetarte 
  
 Cuando aprenden a etiquetar, les parece que es necesario que te enteres de muchas de sus publicaciones, más si se tratan de fotos tuyas de bebé o vestido de venado listo para el festival de la escuela de la primavera de 1994. 
 Preguntan por ti en muros ajenos 
  
 Quizá te fuiste de fiesta y no has regresado porque según tú no tienes pila. No dudes que en algún momento te “vocearán” en el muro de alguien más, justo como cuando te perdías en el supermercado y por el sistema de audio te llamaban para que te reportaras a la zona de servicio a clientes. 
 Piden que aceptes a todos tus familiares 
  
 No está mal tener a tus familiares en Facebook, lo que pasa es que algunas veces, en su literalidad, se toman en serio las interacciones que allí se dan y tu perfil puede ser un nido de malos entendidos. Si algún amigo publica una broma en tu muro, puede ser motivo de escándalo para quienes no conozcan la forma en que tú y tus amigos bromean. 
 Puede que pase más tiempo que tú en Facebook 
  ¿No lo viste en Face? Puede ser una pregunta común, ya que por alguna extraña razón siguen cantidades enormes de páginas que publican sobre diversos temas, desde páginas de frases motivacionales o noticiosas. En el momento en que comienzan a ver las oportunidades de interacción, comienzan a rastrear a sus amigos de la juventud para dejarles un comentario en su muro algo así como “Querido amigo de juventud, te quiero decir que me da mucho gusto encontrarte por este medio. Recuerdo cuando a los 6 años nos robamos…” y demás cosas. Esto lo hacen directamente en los muros porque al principio creen que sólo el destinatario, puede ver lo que ellos le están publicando. 
 Pueden dejarte en ridículo si aprenden a bromear como tú lo haces. 
  
 Ellos te conocen mejor que nadie, así que quizás al principio te dé ternura que te pregunten qué es un meme. Pero cuando llegan a conocer los códigos de conducta que tú tienes, pueden en verdad arruinar tus estados y fotos. Quizá no suceda mucho, pero puede haber casos como el siguiente. 
 Las prácticas de nuestros padres pueden parecer un poco extrañas para nosotros, pero para comprenderlos, debemos recordar que su mundo jamás estuvo tan conectado y tan comunicado. Ahora que tienen la oportunidad de expandir su presencia por medio de redes sociales, lo hacen de una manera muy literal y se toman las cosas más en serio. Asimismo, les ofrece una oportunidad de conocer tu vida, pero con eso hay que tener cuidado, ya que a veces hay aspectos de nuestra vida que posiblemente no le agraden mucho a tus padres. 
 Quizás en algunas ocasiones te enfaden algunos de sus comportamientos en Facebook , pero tiene que ver con la brecha generacional, la cual divide al mundo entre quienes nacieron en un mundo conectado, y los que definitivamente ya tuvieron que aprender a manejar la tecnología en edades adultas. 
 *** 
 Te puede interesar: Esto pasa si le das like a todo lo que ves en Facebook