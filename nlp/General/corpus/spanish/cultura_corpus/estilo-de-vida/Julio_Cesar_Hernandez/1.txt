5 acertijos matemáticos que te pondrán a pensar

 En la vida ciertas cosas pueden ser analizadas desde la postura de las matemáticas. Hay quienes las elevan por encima de toda forma de conocimiento, y si bien no es la única, es una forma muy certera de conocer la realidad y prevenir ciertos comportamientos. 
 Asimismo, es interesante conocer ciertas cosas tan sencillas de la vida como cuál es la probabilidad de que alguien dentro de mi oficina cumpla años el mismo día que yo, o si soy popular dependiendo de las personas con las que me junto. Estas preguntas tan burdas tienen respuestas complejas que implican un razonamiento lógico-matemático muy fuerte. 
 Para conocer un poco de algunos datos que las matemáticas y la estadística aseguran, presentamos cinco razonamientos sobre problemas elementales como la decisión o las relaciones interpersonales. 
 El problema de Monty Hall 
  
 Estás en un concurso en la televisión, donde el presentador te dice que de tres puertas debes elegir una. Dos de esas tres puertas tienen detrás una cabra cada una, y sólo una puerta tiene detrás un auto último modelo. Eliges abrir la puerta 1 y aparece una cabra. El presentador te da la opción de elegir la otra puerta o de quedarte con la puerta que elegiste. Por ejemplo si elegiste al inicio la puerta 2, puedes cambiar a la puerta 3. ¿Cuántas probabilidades de ganarte el coche tienes, si decides no cambiar a la puerta 3? 
 La mejor estrategia en el juego es ir cambiando, ya que si cambias de puerta sólo habrás perdido si el carro aparece en la puerta que ya tenías seleccionada. Las probabilidades de ganar cuando cambias de puerta son 2 a 3, en cambio si te quedas con la puerta que elegiste al inicio, la probabilidad sigue siendo 1 a 3. En el caso de cambiar aumentan las posibilidades ya que la puerta conocida se elimina y ahora las probabilidades son de 2 a 3. No quiere decir que vayas a ganar, pero si cambias de puerta aumentan las posibilidades de no perder. 
 La caja de Bertrand 
  
 Supongamos que tienes en frente tres cajas, y cada una tiene dos compartimentos. Una tiene dos barras de oro, la segunda tiene dos barras de plata y la tercera tiene una barra de oro y otra de plata. Puedes elegir abrir una caja y abrir aleatoriamente un compartimento. Si la barra que descubres es de oro, ¿qué probabilidad tienes de que la otra también sea de oro? Tu primer pensamiento te puede decir que tienes una oportunidad de dos. Parece sencillo pero no lo es tanto. 
 La probabilidad de que te toque otra barra de oro se reduce a 2/3, debido a que no sabes si es la que tiene las dos barras o es la que tiene una. De igual forma la caja con dos barras puede abrirse de los dos lados obteniendo el primer resultado. 
 Tus amigos son probablemente más populares que tú 
  
 El sociólogo Scott L. Feld, descubrió en 1991 que el 74 por ciento de las personas tiene menos amigos que sus amigos. 
 En una oficina de 10 personas, cada persona en promedio tiene 2.85 amigos, pero el promedio de amigos que tus amigos tienen es de 3.39. Lo cual matemáticamente dice que tus amigos son más populares que tú. 
 Lo anterior es sólo un ejemplo, pero puede verse en las redes sociales. Normalmente sigues a personas que tienen más seguidores que tú. Tiendes a tener amigos con más conexiones sociales que tú. 
 El problema del plomero 
  
 Si la semana pasada se te descompuso la tubería del baño y debiste llamar a un plomero, fue y lo arregló todo y posteriormente le pagaste, ¿esta persona es un contador o un plomero contador? 
 Al inicio se menciona que un plomero visita tu casa, así que se puede asumir como un plomero, sin embargo alguien es un plomero contador es por antonomasia un contador y la cantidad de contadores es más alta que la de los plomeros-contadores. La probabilidad de que el sujeto que arregló tu tubería sea un contador-plomero, ya está implícita en que es un contador. También está la probabilidad de que quien arregla la tubería sólo sea un contador. Si se suman las dos probabilidades es probable que sea un contador quien haya sido, ya que existen menos posibilidades de que sea un plomero-contador. 
 El problema del cumpleaños 
  
 ¿Te has preguntado, quién dentro de tu oficina cumple años el mismo día que tú? Digamos que trabajas en una oficina con 23 personas. La respuesta marca que hay un 50 por ciento de probabilidad de que alguien comparta cumpleaños. 
 Si en una oficina tuviéramos a 366 empleados, estadísticamente estaría garantizado que dos personas compartirían fecha de cumpleaños. Regresando al ejemplo de una oficina de 23 empleados, primero se sacan las probabilidades de que dos personas tengan el mismo cumpleaños, lo cual es de 99.72 por cierto de probabilidades de no coincidir, la misa cuestión, para tres personas da un resultado de 99.17 por cierto de probabilidades de no compartir cumpleaños; con cuatro personas en el problema, el porcentaje baja con 98.36 por ciento. Si se siguiera hasta contemplar 23 personas en la operación, da un resultado de 49.27 por ciento de probabilidades de que alguien comparta cumpleaños. Luego la probabilidad de que en un grupo de 23 personas, dos cumplan el mismo día es de 50.7 por ciento. 
 Esto son aproximaciones matemáticas que pueden ser muy certeras, pero como sabemos, la estadística no lo es todo en la vida, es sólo un modelo de aproximación que puede ayudarnos a prevenir ciertos comportamientos o situaciones de la naturaleza, los cuales nunca cambian. 
 Te puede interesar: La ciencia te dice por qué bailar con tus amigos es bueno de muchas maneras. 
 *** 
 Referencia: Business Insider