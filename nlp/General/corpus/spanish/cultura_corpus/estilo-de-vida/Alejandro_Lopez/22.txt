8 cosas que puedes hacer mientras eres joven en lugar de enamorarte

 Cuando se está enamorado empieza uno por desilusionarse a sí mismo, y acaba por desilusionar a la otra parte interesada O. Wilde 
 ¿Por qué no enamorarse? 
 Si bien el amor es un sentimiento fantástico que lleva hacia un estado de felicidad, acompañado de una sensación de bienestar y éxtasis, nada es eterno y la mayoría de las veces todo termina en un gris lamento que sólo el paso del tiempo puede borrar. La vida es demasiado efímera como para pasarla sufriendo por desamor, por el cariño no correspondido o aquel amor imposible. 
 ¿Por qué entregar la vida y el cariño a una sola persona cuando se puede demostrar día a día de un sinfín de formas, sin necesidad de códigos de conducta, del control que exige respetar una relación y entrar a una dinámica que cada vez se gastará más al punto de terminar rompiéndose y terminar ambos quebrados? ¿Acaso no sería mejor disfrutar de la grandeza que tiene la vida para dar el amor que puede repartirse entre todos los iguales y los seres vivos sin la necesidad de sentir celos, añoranza o tristeza? 
 Si piensas de esta forma y el amor para ti es algo secundario, trata de sacar el mayor provecho a cada uno de tus días e intenta cumplir con estas ocho cosas mientras eres joven, situaciones que difícilmente se pueden una vez que caes en las redes del amor: 
 – Viajar por el mundo 
  
 Sin necesidad de extrañar terriblemente a alguien ni sentirte incompleto o solo. Embarcarte en una aventura que te lleve al sitio que siempre has deseado o bien, echarte al hombro una mochila y partir sin un rumbo establecido abierto a lo que el destino y sus sorpresas te deparen es una de las cosas que tienes que hacer cuando eres joven para disfrutarla como en ninguna otra época de tu vida. Viajar a donde puedas aprender cada vez más de la cultura, tradiciones, comida y miles de formas distintas de pensar y actuar en todo el mundo. 
 – Aprender idiomas 
  
 Puedes ocupar todo ese tiempo libre en una actividad verdaderamente productiva que no sólo te ayudará a tener más valía profesional, sino que aumentará tu cultura e incluso reforzará tu actividad cerebral y conexiones neuronales. Aprender un nuevo idioma puede ser difícil y al principio imposible, sin embargo, con un poco de dedicación diaria tu cerebro comenzará a asimilar cada concepto y asociará cada vez más rápidamente palabras y frases de un idioma extranjero, distinto al tuyo. 
 – Tomar cursos de lo que siempre has querido 
  
 Olvídate de la academia. No importa tu profesión ni tu edad: nunca es tarde para aprender algo nuevo. ¿Te apasiona la danza contemporánea, la cocina, la astronomía o la meditación budista? Inscríbete a un curso para principiantes de aquello que siempre has deseado hacer, que te causa curiosidad y que por uno u otro motivo no has intentado. Si al término del curso la actividad te convenció y tienes ganas de más, inscríbete indeterminadamente en lecciones o prácticas, enriquecerás tu vida y tendrás una mente y cuerpo más sanos. 
 – Unirte a un voluntariado 
  
 ¿Qué tal si todo ese tiempo que gastaste en tu relación pasada, entre disputas y malentendidos, lo hubieras aprovechado de forma distinta, como por ejemplo, ayudando a quienes más lo necesitan? Conviértete en un médico internacional, brinda ayuda a asociaciones de personas con capacidades distintas o en zonas de conflicto. Viaja y apoya a la conservación del ambiente, reforesta o cultiva plantas, apoya campañas de animales salvajes en peligro de extinción o reservas ecológicas. 
 [ Aquí podrás encontrar algunas opciones para iniciarte como voluntario.] 
 – Dedicarte enteramente a tus pasiones 
  
 ¿Alguna vez has sentido inspiración por una persona que sabe mucho más que el resto de algo determinado, como un crítico de cine, músico, científico o artista? Tú también puedes ser un experto. Por más complicado que parezca, sólo la práctica y la constancia pueden convertir a un apasionado en un experto de cualquier campo del saber. Tú puedes ser una autoridad en el tema que te apasione, el punto es involucrarse y practicar activamente, mantenerte siempre enterado de la actualidad para trascender en ese campo. 
 – Conocer un sinfín de personas distintas 
  
 No es necesario viajar al otro lado del mundo para conocer a muchas personas, cada una con una forma distinta de pensar, actividades, planes, ideologías y personalidades completamente variadas. Descubrirás que cada cerebro es un mundo y que establecer una relación formal con alguien es más difícil de lo que creías. Conoce todas esas formas de pensamiento y de acción en todos los sentidos (incluso el sexual) y adquiere una visión mucho más profunda y realista del mundo en que te desarrollas. 
 – Conocerte a ti mismo 
  
 Suena fácil, pero llegar a una introspección real y descubrir más sobre ti es una actividad tan necesaria como olvidada en la sociedad actual. Antes de tener cualquier relación sentimental con otra persona, debes conocer a quien estará a tu lado durante toda tu vida sin importar nada más: tú. Saber sobre tus gustos, pasiones, intereses y también todas las cosas negativas que influyen en tu personalidad no sólo te ayudará a escoger mejor a las personas con quienes deseas compartir, además aportará a que tomes mejores decisiones y a la larga, seas más feliz. 
 – Disfrutar de tus amistades 
  
 Cuando una persona encuentra a una pareja formal, es muy común que las amistades pasan a segundo término y aquellos momentos de diversión, experimentación y aventura entre las personas que elegiste para acompañarte el resto de tu vida son cada vez menos frecuentes o simplemente terminan en la memoria como buenos recuerdos de una época pasada. Disfruta de tus amigos, del valor de la amistad y todos esos momentos en que puedes divertirte y ser feliz sin necesidad de una pareja. 
 ¿Sigues sin convencerte de que estar soltera es la mejor forma de disfrutar de la vida? Mira estas señales que indican que la soltería es lo mejor que te puede pasar y piénsalo de nuevo. Todo en la vida debe tomarse con humor, como muestra de ello puedes ver estas divertidas ilustraciones que reflejan cómo se siente la soltería , seguro te identificarás con algunas de ellas.