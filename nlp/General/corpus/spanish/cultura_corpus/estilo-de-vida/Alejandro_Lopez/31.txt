10 señales que indican que eres un intenso

 
 ¿Qué sería de la vida si no la tomamos en serio, si no amamos en serio, reímos en serio, bromeamos en serio? 
 La sociedad está inmersa en una época en la que todo lo que hacemos tiene una carga irrelevante, un espíritu que al mismo tiempo que puede significar muchas cosas, no significa nada . Un beso, un mensaje o una obra de arte pueden ser una expresión profunda de nuestro sentir; sin embargo, la mayoría del tiempo pasan por un proceso que les resta significancia, provocado por la sociedad misma y el profundo temor a comprometerse o definirse bajo cualquier causa o estandarte. 
 ¿Alguna vez te han dicho tus amigos que eres demasiado intenso por enojarte en una discusión, defender un punto de vista o tomar muy seriamente tu relación de pareja? Estas diez señales te ayudarán a identificar si realmente lo eres: 
 – Nunca ocultas tus sentimientos 
  
 Cuando conoces a alguien, lo que menos te importa es lo que piense sobre ti, tu ideología o creencias. No disimulas ni un instante tus sentimientos, sin importar si esto resulta contraproducente. Siempre esperas que los demás estén enterados de lo que sientes para evitar malentendidos o conflictos. Si tienes un problema con alguien, eres directo y vas al grano en vez de especular o provocar dudas. 
 – Te interesas realmente por lo que te apasiona 
  
 La vida está llena de tantas personas como puntos de vista, actividades e ideologías distintas. Por supuesto, hay algunas que no te interesan, pero aquellas cuestiones que consideras importantes, las personas que amas o las cosas que te apasionan, generan en ti un interés genuino que se ve reflejado en tu vida diaria. 
 – Te desespera el tiempo muerto 
  
 Si vas en camino a un lugar que queda a dos horas, te desesperas pensando en que podrías ocupar ese tiempo haciendo otras actividades. Mientras viajas en el transporte público, haces cuentas de los minutos que recorrerás hasta llegar a tu destino, te molestas sobremanera si por alguna razón esta estimación se incrementa, pues aunque en realidad no tengas algo urgente que hacer, sabes que ese tiempo te pertenece y lo estás perdiendo haciendo nada. – Defiendes a muerte tus puntos de vista 
  
 Te enfrascas fácilmente en discusiones profundas en las que defiendes tus puntos de vista sin importar quién sea tu contrincante. No te tomas comentarios de esta naturaleza como un chiste y siempre estás abierto al diálogo y la discusión de las ideas. 
 – Hablas claramente y con convicción 
  
 No eres de las personas que sienten vergüenza o pánico escénico al hablar frente a una multitud. Parece que expresar tus ideas es tu ambiente natural e imprimes la firmeza en la voz y los argumentos necesarios para cada ocasión. La gente suele identificarte como un gran orador, algunos creen que te enojas, pero en realidad, cuando tienes la oportunidad de hablar, lo haces abiertamente y con pasión. 
 – Eres sincero 
  
 Una persona intensa no tolera las mentiras. Entiende que mentir es una actividad de sobra en un mundo que está plagado de farsas y apariencias por doquier. A pesar de que puedas lastimar a alguien, sabes que es mucho mejor ser sincero que ocultar una verdad que a la larga puede acarrear mayores problemas y complicaciones. 
 – Odias las conversaciones banales 
  
 Si llegas a un sitio y la conversación versa sobre la moda, el estado del tiempo, el smartphone de novedad o algo que simplemente consideras como intrascendente, la charla pierde todo tu interés. Tus temas suelen ser profundos e incluso tratas sobre cuestiones filosóficas como el significado de la vida, el arte, la política actual o la naturaleza del género humano. 
 – Te molesta darle vueltas a los asuntos 
  
 Cuando debes tomar una decisión importante, analizas con el tiempo que consideras prudente antes de elegir; sin embargo, casi todas tus primeras elecciones son definitivas debido a que actúas de acuerdo a tu filosofía de vida. En las decisiones de menor trascendencia, desesperas al ver como las demás personas se toman mucho tiempo para elegir algo que de todas formas no tendrá una gran repercusión en un futuro. 
 – Prefieres las acciones a las palabras 
  
 Nunca dices “ojalá nos viéramos”, enfilas hacia su casa y le escribes en el camino. Lo mismo con todas tus acciones: no te gustan las amenazas, simplemente consideras que cuando algo está fuera de su lugar, la mejor forma de corregirlo es actuando al instante. 
 – Te molestan las injusticias 
  
 Tienes un instinto natural que te hace explotar en cuanto detectas una injusticia. Tratas de informarte sobre los contextos políticos y sociales que ocurren en tu país y lo que para otros es un tema de burla, para ti es una cuestión importante y te molesta que los demás no lo comprendan de la misma forma. 
 – Después de todo, ¿qué sería de la vida si sólo hubiera pusilánimes, gente que cambia sus convicciones ante la crítica o se esfuerza por mantener una farsa y ser parte del  status quo ? Las personas decididas son aquellas que escapan del marasmo de la pasividad y se esfuerzan por vivir intensamente durante el diminuto instante en la inmensidad del tiempo y la vastedad del espacio que les corresponde, al cual llamamos vida. 
 *** Te puede interesar: 
 10 razones por las que el sexo te acerca a la persona que amas 
 12 cosas que evitarán tengas una relación tóxica con tu pareja