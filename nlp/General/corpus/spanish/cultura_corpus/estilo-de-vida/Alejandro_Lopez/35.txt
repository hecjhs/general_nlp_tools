15 lecciones de Buda para crecer espiritualmente y vivir mejor

 El budismo es la única religión auténticamente positivista que la historia nos muestra, (…) no cree en la lucha contra el pecado sino, dando totalmente razón a la realidad, lucha contra el sufrimiento. F. Nietzsche El budismo mucho más que una religión, es una filosofía de vida que muchos millones de personas alrededor del mundo pueden practicar mientras simultáneamente profesan cualquier fe. Nietzsche, quien dedicó buena parte de su obra a mostrar la decadencia de las religiones ascéticas (especialmente el cristianismo), especificó que el budismo era auténtico e “higiénico” por cuanto se enfrentaba con el mundo real. El poder de la voluntad y la filosofía contra la “moral de esclavo”, en palabras del filósofo, es uno de los nexos que indudablemente comparten ambos sistemas de pensamiento. 
 ¿Qué es lo que Nietzsche vio en la filosofía budista para que lo convenciera de que no representaba una moral decadente como todas las demás?  El camino de la iluminación , la búsqueda de la felicidad, el equilibrio físico y mental, así como la forma de afrontar las dificultades en la realidad práctica, son los puntos que hacen la diferencia. Los siguientes consejos están basados en los sutras  o los discursos y enseñanzas de distintos Dalai Lama con el fin de crecer espiritualmente y vivir mejor: 
 – Ámate para amar a los demás 
  
 Por más cliché que parezca, resulta imposible amar plenamente a alguien más sin amarte primero. Conocerte y dar lo mejor de ti a los demás es el principio para alcanzar la plenitud y un estado elevado de paz mental.  
 – No confundas amor con apego 
  
 Contrario al cristianismo, el amor en el budismo no es sufrir ni soportar en la espera de una redención, eso es más parecido al apego, al deseo visceral que sólo produce aferramiento. Una relación sana, según está filosofía, está en la libre manifestación de amor por ambas partes, creando en vez de destruir, olvidando los celos y el egoísmo. 
 – Guía tus deseos 
  
 Todos los problemas del hombre se deben a la angustia causada por la insatisfacción de los deseos no cumplidos. Dejar de lado los deseos que son egoístas, materiales y de apego a otra persona traerá armonía. 
 – Acepta los cambios con sabiduría 
  
 El estado de todas las cosas en el Universo es el constante cambio. Nada se mantiene estático, las personas, lugares y situaciones pueden transformarse para bien o para mal. Elegir hacia qué lado se dirige tu vida y tomar todos los cambios como un proceso natural es una decisión sólo tuya. 
 – El sufrimiento es opcional 
  
 Cuando algo te aqueja, crea un malestar que con el paso del tiempo puede convertirse en sufrimiento. El malestar es siempre la manifestación de la angustia, reflejada en el estado de ánimo. El sufrimiento, en cambio, es un estado decadente en el que siempre es posible caer e incluso debe aceptarse como parte de un proceso de conocimiento y amor por la vida. Escapar de él siempre es opcional, todo está en la fuerza de voluntad. 
 – Conoce las reglas y rómpelas cuando sea necesario 
  
 Esto aplica para todos los aspectos de la vida. Si una ley es injusta, rómpela. Si tu bienestar está en juego por una convención moral, olvídala. Si la obediencia es mezquina, desobedece. 
 – Perdona siempre 
  
 No se trata de “poner la otra mejilla” ni de expiación. “Perdona al menos por egoísmo”, dijo el decimocuarto Dalai Lama. Cuando no tengas una razón real para perdonar, hazlo por ti mismo, para sacudirte ese malestar y evitar que se convierta en sufrimiento. 
 – Todas las derrotas son un aprendizaje 
  
 Cada pérdida, cada fracaso y derrota, sin importar su naturaleza, es una gran oportunidad para tomar una lección. Nunca lo pases por alto. Así, la próxima vez que tropieces, antes de lamentarte detente a pensar cuál es el aprendizaje que conlleva el duro momento. 
 – Enamórate de las raíces, no de las flores 
  
 Aprende a diferenciar entre la esencia y la apariencia de las cosas. Desde la naturaleza hasta las personas. Lo realmente importante siempre está oculto y es la vía a la plenitud. En palabras del Principito: “Lo esencial es invisible para los ojos”. 
 – Amar no es necesitar 
  
 La mejor relación de amor es aquella en la que el amor de ambos es más grande que la necesidad por el otro. Sin sufrimiento ni pasiones desbordadas, simplemente encontrar el equilibrio. 
 – Sigue la ley del desapego 
  
 La misma lección del amor se aplica a todos los aspectos de la vida: Lo que te hace feliz no está en realidad en las cosas a las que te apegas y sin las que crees que no puedes sentirte pleno, sino en los pensamientos y la paz que hay en tu interior. 
 – Apasiónate por lo que haces 
   
 Busca incesantemente hacer todos los días aquello que disfrutas, lo que te apasiona, que te motiva y empuja a querer saber más, ser mejor o desarrollar tus capacidades hasta el límite. Cada determinado tiempo pregúntate “¿en realidad hago lo que más deseo?”, si tu respuesta es negativa, esfuérzate por cambiarla. 
 – Cambia tu forma de pensar y cambiará tu vida 
  
 Todo lo que eres es reflejo de tu mente. Los deseos, las pasiones y las expectativas desbordadas son motivo de angustia y preocupación que impactan negativamente en tu vida. Si hoy mismo te propones cambiar tu forma de pensar, verás que los efectos positivos en tu vida serán inmediatos. 
 – El primer paso a la plenitud es un cuerpo sano 
  
 Según la máxima budista, “tu cuerpo es tu templo”. Contrario a la filosofía ascética de muchas religiones que se centran en lo ideal, mantener la salud física es primordial pues el cuerpo es el vehículo del alma. Hacer ejercicio, alimentarse sanamente y mantener un equilibrio entre salud física y mental son el primer paso para alcanzar la plenitud en todos los aspectos. 
 – Duda de lo establecido 
  
 No porque algo haya sido dicho antes y aceptado por muchas personas quiere decir que sea cierto o está bien. Poner en duda todas las creencias, tradiciones y conocimientos es una gran manera de cambiar tu realidad y principios por otros que te guíen a la felicidad. 
 – Poner en práctica los preceptos anteriores es el primer paso para cambiar radicalmente tu filosofía de vida, y buscar una más sana, dispuesta a perdonar, amar y alcanzar la plenitud aceptándote tal cual eres. La  comprensión budista del Universo se enfoca en el momento actual, en este instante de la vida como el prioritario. Todos los demás errores quedaron atrás y el futuro está construyéndose a cada segundo, la realidad exige que los cambios en tu forma de pensar sean aquí y ahora. *** Te puede interesar:6 películas budistas que te enseñarán las estaciones de la vidaComo superar la pérdida según el budismo