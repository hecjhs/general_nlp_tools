10 ideas para ganar dinero extra en tus tiempos libres

 
 ¿Has pensado todo lo que podrías hacer si tu ingreso fuera un poco más elevado? 
 Según datos de la OCDE, México es el país con los horarios de trabajo más amplios y la menor productividad en el mundo. En promedio, cada trabajador labora 2 mil 246 horas al año, el máximo de todos los países de la lista, cuya media es de mil 766 horas. Esta cifra corresponde a 5 horas por día, incluyendo fines de semana y días inhábiles. La situación es alarmante por donde se vea: el poder adquisitivo sigue en picada desde la década de los setenta, mientras las horas de trabajo se multiplican y la mano de obra es la más barata del mundo. ¿Por qué los mexicanos no perciben sueldos más altos a pesar de trabajar más que ninguna otra nación? 
 Nunca están de más un poco de ingresos disponibles en tu cuenta de banco o bien, en tu bolsillo. La mayoría de las personas pasan tiempo pensando en todas aquellas cosas que cambiarían en su vida con un sueldo un poco más alto , ya sea pagar deudas pendientes, comprar algo que hace tiempo deseaban o ahorrar para un viaje. Existen opciones viables para hacer dinero extra en ese tiempo libre que a menudo no utilizas en alguna actividad productiva. Si requieres de un ingreso extra que te ayude a mejorar tu calidad de vida, toma en cuenta estas diez ideas para aprovechar esos espacios libres que tienes entre todas tus obligaciones y pasatiempos: 
 – 1. Cocina 
  
 Todo el mundo tiene un secreto culinario en su familia que se transmite de generación en generación y cuya receta se conserva en el anonimato sobre todas las cosas. Pide a tu mamá, tía o abuela la fórmula mágica para preparar esa comida que te parece la mejor que has probado. Prepara porciones individuales para vender y acostumbra a tus clientes a que así será un día de la semana; si eres constante tendrás éxito y podrás seguir con tu pequeño negocio. 
 – 2. Vende tus fotografías 
  
 Si te gusta la fotografía, lo único que necesitas es poner manos a la obra y disparar a cuantos objetivos te parezcan interesantes: un atardecer, un congestionamiento vial o los árboles en medio de la ciudad. Bancos de imágenes como 123rf.com o Shutterstock venden fotografías u ofrecen una buena cantidad dependiendo de la calidad de tus imágenes para venderlas a terceros. 
 – 3. Gestiona tus redes sociales 
  
 Las cuentas más grandes de las principales redes sociales pueden participar en los programas publicitarios como escaparates para las marcas que ahí se anuncian. Si tienes una cuenta donde compartes vivencias, pasiones o algún hobbie en particular, consulta en la sección de ayuda la forma en que puedes monetizar tus publicaciones. Dependiendo del impacto de tu perfil, puedes aumentar tus ingresos vía Internet y seguir gestionando la red según tus gustos. 
 – 4. Pasea perros 
  
 Este empleo está convirtiéndose en una gran fuente de ingresos para las personas que tienen un par de horas libres por la tarde y además, aman trabajar con animales. Ponte de acuerdo con la veterinaria más cercana y ofrece tus servicios por medio de Internet o carteles. Una vez listo, diseña el recorrido y si los clientes tienen buenas referencias de tu trabajo, cada vez tendrás más oportunidades de obtener ese ingreso extra. 
 – 5. Renta una habitación 
  
 Muchos sitios se especializan en acercar a las personas que rentan alguna clase de hospedaje en un sitio determinado con quienes estarán de visita o vacaciones en esos lugares. Si te sobra una habitación, tienes un departamento libre u ofreces tu sala con un colchón para una estancia de turistas, date de alta como anfitrión y toma las mejores fotos a tu espacio. 
 – 6. Presta algún servicio privado 
  
 Si tienes horas libres por la mañana y notas que la demanda en transporte escolar entre tus vecinos es alta, ofrécete para llevar a un grupo reducido cada que tengas oportunidad. Es cuestión de poner atención a tu alrededor y encontrarás aquellos trabajos momentáneos que puedes hacer y cuestan tiempo o trabajo a la gente. Posiblemente preferirán pagar una cuota baja. 
 – 7. Llena encuestas en línea 
  
 Utiliza esta opción si pasas tiempo perdido frente a una pantalla, ya sea en tu trabajo como guardia, supervisor de vigilancia u otra actividad que conlleve tu atención en una computadora. Muchas empresas pagan a estudios de mercado para saber la preferencia de sus clientes, busca quiénes están solicitando opiniones y colabora llenando un puñado de encuestas. 
 – 8. Hazte agente de ventas 
  
 Si tienes muchos compañeros de trabajo, amigos o conocidos que creas puedan estar interesados en las ventas por catálogo de productos de salud, para la belleza, nutricionales o ropa y calzado, conviértete en un agente independiente y crea tu propia red de clientes. Si bien necesitas de un gran margen para empezar a percibir un ingreso sensible, si incentivas a los demás a unirse al negocio, serás recompensado por la empresa pagadora. 
 – 9. Da un curso 
  
 Aprovecha esa habilidad especial en tu tiempo libre y organiza cursos de aprendizaje de aquello que dominas: una clase de inglés, danza, matemáticas, pintura o ayuda para la tarea, promociona entre tus conocidos la actividad y sondea el mejor horario para concentrar al grueso de las personas interesadas. Emprende un curso básico y si resulta exitoso, intenta con otro más avanzado o una disciplina afín. 
 – 10.  Vende artículos online 
   
 El mundo del mercado online es una oportunidad para iniciar una carrera como vendedor virtual. Todo lo que debes hacer es especializarte en la compra-venta de algo en especial: cámaras de fotografía, piezas de máquinas, discos antiguos o estampillas postales; el secreto está en adquirir en sitios alternativos esos objetos de valor que puedes vender por el doble de su precio en Internet a personas interesadas. 
 – Si buscas más opciones en línea, toma nota de estas  10 páginas en las que podrás ganar  dinero extra. ¿Te es imposible ahorrar y llevas años posponiendo esa gran aventura para el próximo verano? Si tu respuesta es afirmativa, pon en marcha algunas de las 13 cosas que puedes hacer para ganar dinero y hacer el viaje de tus sueños .