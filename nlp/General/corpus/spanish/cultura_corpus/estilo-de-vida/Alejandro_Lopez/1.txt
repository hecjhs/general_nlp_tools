10 hábitos que parecen normales pero están dañando tu salud mental

 
 Todo mundo tiene una idea de lo que debería hacer para mejorar su salud mental: leer libros, tomar clases de música o aprender a tocar algún instrumento, llevar una alimentación balanceada y exigirle más al cerebro a través de la resolución de crucigramas y juegos que le permitan crear  conexiones neuronales al tiempo que mejora su funcionamiento. 
 De la misma forma, la gran mayoría de las personas cree en la idea de que para el cerebro lo mejor es pensar; mientras para los músculos y la condición física, lo idóneo es salir y practicar algún deporte o, bien, machacarse en el gimnasio: hacer ejercicio, alimentarse sanamente y no fumar, todos son hábitos que la gente intenta cumplir para evitar hacer daño a su cuerpo, ¿cierto? 
 Todos estos son relacionados constantemente con la salud física, conllevan una mejor figura y un aumento en la condición física; sin embargo, el riesgo de no hacerlo incluye daños más importantes para el organismo que el aspecto físico: todos ellos estropean la salud mental. Descubre diez hábitos que parecen normales pero están dañando seriamente a tu mente, provocando estrés, falta de memoria, baja concentración, ansiedad y una disminución de la capacidad para resolver problemas cognitivos: 
 – Fotografiar todo 
  
 Tener una cámara de una calidad media en el bolsillo para cada ocasión en que algo especial o insólito se cruce en tu camino es una gran ventaja; sin embargo, fotografiar cada experiencia, salida, festejo, persona u objeto es dañino para tu salud mental. Según un estudio publicado por Psychological Science, pasar el tiempo fotografiando absolutamente todo produce una pérdida de memoria a corto plazo y los recuerdos se fijan de peor manera. En vez de eso, toma un par de imágenes, guarda tu smartphone y disfruta del momento. – No hacer ejercicio 
  
 Olvida los problemas que atañen a las reacciones fisiológicas de tu organismo como la obesidad, la pérdida de grasa muscular y la falta de apetito: llevar una vida sedentaria exenta de una actividad física continua contribuye a un descenso en las conexiones neuronales, además de la pérdida de autoconfianza y un aumento de síntomas relacionados con la ansiedad y la depresión. 
 – Estar dentro de una relación tóxica 
  
 El problema es de tal magnitud que en ocasiones las personas envueltas en relaciones de pareja enfermizas suelen creer que se trata de los altibajos comunes en toda relación, cuando en realidad están inmersas en un espiral decadente que sólo trae problemas de ansiedad, estrés traumático y depresión generalizada, además de la pérdida de identidad y una creciente inseguridad en la toma de decisiones. 
 – Consumir grasas saturadas 
  El consumo excesivo de alimentos ricos en grasas saturadas, especialmente aquellos de origen animal, tienen un efecto adverso en la producción de dopamina, además de disminuir el tiempo de reacción y afectar negativamente en la flexibilidad cognitiva, asociada con la incapacidad de recordar claramente sucesos significativos y pérdida de memoria. 
 – Tomarte la vida muy en serio 
  
 Tratar de ejercer un control férreo sobre cada una de las actividades que realizas en tu día a día es una de las mejores formas para crear un estado depresivo, de inseguridad e impotencia. No trates de controlar todas aquellas actividades que puedes improvisar y disfrutarás más que planificando todo el tiempo. 
 – Fumar 
  
 Haciendo a un lado los problemas de salud más conocidos en personas fumadoras, el cigarro daña a todos los órganos y el cerebro no es la excepción. El boletín científico de la Biological Psychiatry demostró que la corteza orbitofrontal de los individuos fumadores se reduce drásticamente conforme aumenta su adicción al tabaco; este adelgazamiento está relacionado con la pérdida de inteligencia y conexiones neuronales. 
 – Dormir poco 
  
 Descansar menos de 6 horas por noche es un hecho esencial para que aparezcan trastornos asociados con el estrés crónico, mal humor y ansiedad. Además, perderás la capacidad de concentración y tu cerebro responderá más lento a los estímulos del medio. 
 – Vivir atado al teléfono móvil 
  
 Disfrutar de la vida a través de los pixeles puede ser una conducta que genere trastornos mentales con graves consecuencias para tu salud, como sufrir de una sobreestimulación debido a las notificaciones constantes que envía el teléfono. La conectividad permanente se traduce en estrés y fatiga crónica que termina por consolidar un círculo vicioso alrededor de la comunicación vía Internet. 
 – Trabajar demasiado 
  
 Al margen de la multitud de problemas en la vida personal que trae consigo trabajar durante jornadas largas y extenuantes, el cerebro es el órgano más afectado: las personas que trabajan más de diez horas diarias tienen un 50 % más de posibilidades de caer en una depresión severa que quienes trabajan por jornadas de siete u ocho horas. 
 – Buscar todo en Internet 
  
 Utilizar constantemente el buscador más popular del mundo o sacar inmediatamente el smartphone para recordar un número de celular, datos, hechos históricos o una fecha importante, causa graves problemas para la salud mental. El neurolingüista del MIT, Earl Miller, afirma que el cerebro humano es cada vez menos utilizado en sus funciones de memoria gracias al desarrollo de Internet, que le permite corroborar en todo momento la información sin pasar por un análisis riguroso de lo que realmente recuerda. 
 – Con la facilidad actual de suplir el esfuerzo mental por un simple movimiento de dedos, el cerebro ha perdido terreno ante la inmensidad de la red. Refuerza tus conexiones neuronales y vuelve a utilizar la materia gris para desarrollar y resolver problemas lógicos, matemáticos o emocionales. Sigue estos 10 hábitos para tener un cerebro más sano y trata de recordar antes de acudir al buscador que contiene cada uno de lo saberes en la historia de la humanidad. Si pretendes aumentar tu capacidad para resolver problemas complejos, pensar fuera de la caja y abrir tu óptica del mundo, prueba los 5 pasos para incrementar tu inteligencia y haz de ellos un hábito, pronto notarás cómo mejora tu capacidad cognitiva de retención, comprensión y memoria.