30 cosas que debes descubrir antes de los treinta

 Somos jóvenes, estamos en camino a ser la generación que haga girar el mundo, vivimos rápido, amamos lento, nos destruimos en piezas como un rompecabezas con cada nuevo amor y nos armamos dolorosamente dentro de nuestra soledad. Evolucionamos en nuevos seres con cada giro al Sol y no nos damos cuenta, somos autodestructivos, somos el escozor de una sociedad gris de dinosaurios que caminan en línea recta, somos sabios, ingenuos e irreverentes, todo a la vez. Rompemos las barreras de lo establecido, inventamos sueños y materializamos creencias. Tenemos la audacia de robarle una sonrisa a un mundo que parece caerse a pedazos. Somos jóvenes y nunca volveremos a serlo, en unos años cruzaremos la frontera y nos perderemos, entraremos a la tercera década y el mundo nos maldecirá con una seriedad y sobriedad que nadie debería tener. Mientras, vivimos esperando que nunca llegue esa fecha, descubriendo nuevas palabras en el diccionario infinito que es la vida, redescubriendo nuestro camino antes de los treinta, un punto por año, un paso a la vez, como los siguientes. 
 El verdadero amor. Lo que más te gusta del sexo. Que no es amor, sólo es deseo. La música que te apasiona. Tus errores y cómo mejorarlos. 
  Que olvidar el pasado es necesario. Que la felicidad no se encuentra con alguien más. Cómo valerte por ti mismo. Que nada llega solo, debes luchar por ello. Quiénes son tus verdaderos amigos . 
  Quién eres en realidad y hacía dónde vas. Que la autocompasión nos sirve para nada.  




 Que tus decisiones pueden ser cruciales. La experiencia de ir a una playa nudista. 
 Mudarte de ciudad y volver al mismo lugar. Tus virtudes y tus defectos. Descubrir sabores, comer y beber de todo. El placentero dolor de un tatuaje. Lo que piensan los demás de ti no importa para nada. Que no le debes explicaciones a nadie. 
  Que nada es perfecto en la vida, mucho menos tú. Las consecuencias al día siguiente de una noche de excesos. Que los demás no tienen la culpa de tus problemas. Tu libro favorito, tu película favorita o tu ciudad favorita. Cómo preocuparte menos. 
  Tu personalidad. La mayor cantidad de lugares posibles. Que vivir solo no es tan fácil como lo imaginaste. No gastar dinero sólo por gastar. Nuevas y más responsabilidades, siempre verlas con positivismo. 
  
 Así como tienes que experimentar estas 30 cosas, atrévete a descubrir los sabores de la nueva  Ciel Exprim . Sabe a mandarina, tu mente descubre algo más.