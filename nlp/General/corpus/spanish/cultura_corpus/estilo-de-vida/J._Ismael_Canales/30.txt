Porqué el gin es la nueva bebida favorita de todos

 Es un hecho, el cocktail está de moda, o mejor dicho, nunca ha dejado de estarlo. Su presencia en la literatura, música y cinematografía, data de muchos años atrás, ya sea en clásicos como Casablanca (1942) con Ingrid Bergman y Humphrey Bogart, hasta la película indie protagonizada por Bill Murray y Scarlet Johansson, Lost In Translation; el cocktail ha tenido momentos memorables. 
 Octubre será el momento en que el cocktail cree recuerdos inolvidables en la ciudad de México, con la llegada de Cocktail Week . Del 15 al 25 de octubre, más de 100 bares importantes del corredor Roma-Condesa y Polanco, tendrán actividades y promociones para que te familiarices con la cultura del cocktail, o bien, para que sigas deleitándote con tus mezclas favoritas. 
  
 El ginebra más laureado del mundo, Tanqueray N° Ten, reclutará a prestigiosos bartenders de México para que ofrezcan sus cocteles secretos y las mejores creaciones con N° Ten, el cual por su sabor aterciopelado, debido a su fabricación a base de cítricos finamente seleccionados, se convierte en un gran protagonista en el mundo de la coctelería. 
 En el año 2000 la ginebra Tanqueray N° Ten salió al mercado con tan grande aceptación que forma casi inmediata comenzó a acumular los premios más importantes del mundo. Tan solo en ocho meses de su llegada al mercado estadounidense, ganó 7 premios en las mayores competiciones de Estados Unidos. 
 El secreto detrás de la calidad de Tanqueray N° Ten es que se produce en pequeños lotes. Se utiliza un alambique único y muy pequeño conocido como “10th Still”, esta particularidad le otroga un sabor mucho más intenso y profundo que la convierte en la ginebra ideal para elaborar el Martini perfecto. 
  
 Cocktail Week, la plataforma de coctelería nacida en Londres y con presencia en hermosas ciudades del mundo como Madrid, Barcelona, Singapur, París y San Francisco, se hará presente en los mejores establecimientos de México. 
 Para conocer los bares y restaurantes participantes dentro de la ruta del Cocktail Week by Tanqueray N° Ten visita la página www.cocktailweek.com.mx , mientras tanto te compartimos la receta para el martini perfecto. 
 El Martini Perfecto 
 2 shots de Tanqueray N° Ten Gin 1 shot de dry vermouth 2 gotas de naranja 1 twist de cáscara de limón 
 Enfriar Vaso. Mezclar todos los ingredientes en el shaker. Servir en copa martinera y finalizar con el twist de cáscara de limón.