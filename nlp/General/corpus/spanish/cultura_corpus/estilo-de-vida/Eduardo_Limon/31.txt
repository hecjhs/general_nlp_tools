10 cosas que las personas reprimidas sexualmente experimentan

 Absolutamente nadie muestra interés en quitarte la ropa. Ha pasado bastante tiempo desde que alguien te tocó en esas partes que te hacen enloquecer para llegar al punto más álgido del clímax y poco a poco empiezas a creer que esos días de caricias y orgasmos  se han ido definitivamente. Puede que no hayas encontrado a un ser humano con quién hacerlo, quizá no te estés esforzando lo suficiente por atraer a otros con tu apariencia o una ruptura amorosa (reciente o distante) haya comenzado una terrible racha de suerte sexual; de cualquier manera, has caído en las garras de la represión. 
  Y cuando nos referimos a represión sexual, cabe señalar que no estamos haciendo mención de esos estados psicológicos en los que alguien contiene su expresión sexual ni del seguimiento de una práctica de abstención, sino a los impedimentos externos para la realización de la sexualidad. Es decir, cuando por más que se intente tener sexo con otra persona, ese momento tan anhelado nunca llega a tu cama. Cuando todo análisis foucaultiano alrededor de los actos sexuales parece no funcionar: nadie ejerce poder sobre tu cuerpo ni te sientes presa u objeto de la libertad contemporánea, por más falsa que sea ésta, según el filósofo. 
  Si así fuera, no te importaría ser el gobierno de otra persona; sin embargo, tu soltería y la tranquilidad de tu ropa interior parecen no depender de ti, no ha sido tu elección quedarte así, con las ganas. Es más, olvidemos por un momento tu estado anímico en cuanto a relaciones sentimentales, ahora lo que más importa es aquel incurable hormigueo entre tus piernas. 
  Lo único que podemos anunciar respecto a este estado (lamentable) es que se necesita paciencia y dedicación, finalmente el sexo y sus encuentros son algo que llega tarde o temprano siempre y cuando no se espere con el estoicismo de un oso panda. Hasta ellos logran emparejarse de vez en cuando. 
 Eso sí, seguramente no puedes dejar de pensarlo como un gran problema, y lo es, pero no significa otra cosa más que una desesperación que posiblemente no te deje avanzar en tus intentos. Entonces nubla cualquier idea que a continuación se presente y sigue buscándolo. No es tan complicado como parece. – Todo te excita 
  Literalmente todo. Cualquier cosa te recuerda tu soledad y encuentras momentos de conmoción en cualquier actividad. Incluso hacer ejercicio o ver una película son pretexto para pensar en sexo. 
 – Tienes irritabilidad 
  Todo te hace enojar. Aunque sea la cosa más burda, sientes que no puedes con eso; las pláticas con los demás, los retos en el trabajo, tu mala conexión de internet en casa, el sabor de la comida, etcétera. 
 – Cancelas planes 
  Ya sean amigos, familiares o compañeros de la oficina, si hay potencial en una cita para terminar en la cama, lo cancelas todo. Te encuentras una incesante búsqueda por el placer y nada ni nadie te va a frenar de tus cometidos. Pero cuidado, tampoco exageres. 
 – Consideras a cualquiera 
  Y por cualquiera nos referimos incluso a una de tus exparejas. La selección se ha escapado de tus dedos haciéndote pensar que es ahora o nunca el momento de estar en la cama con alguien y eso te hace pensar que cualquiera es un buen candidato. 
 – No soportas las experiencias ajenas 
  Cada que alguien menciona su vida en pareja o una anécdota de sexo, no quieres más que arrojarle un martillo sobre la cabeza. No soportas la idea de que una amistad tuya experimente “mejores” cosas que tú y eso te puede hacer perder los estribos; no exageres, deja que los demás vivan lo que tienen que vivir. 
 – O sus conquistas 
  Cuando ves que uno de tus amigos se fue a casa con alguien más, deseas con todas tus fuerzas que algo salga mal. Y, obviamente, no puedes evitar sentirte mal por tener tan malignos pensamientos, pero hay una ira dentro de ti que no puedes controlar. 
 – Te refugias en la comida 
  Porque una gran rebanada de pizza o una hamburguesa con queso entienden todos tus problemas a la perfección. De vez en cuando disfrutas la compañía de seres humanos, por supuesto, sólo que la comida de un tiempo para acá te está significando más que de costumbre. 
 – O tomas demasiado en serio a Netflix 
  Incluso el servicio cree imposible que sigas ahí enfrente y ahora más que nunca te pregunta si en verdad quieres continuar viendo esa serie. Sal, toma aire, asiste a fiestas; tu vida no está perdida ni significa una catástrofe mundial. Además, quedándote en casa no solucionas nada. 
 – No te importa tu apariencia 
  Entonces no ves problema alguno en seguir usando tus sudaderas viejas y esos calcetines agujerados. Toma una ducha, queda con tus amigos para salir de fiesta, luce tus mejores prendas y demuéstrate que sigues teniendo lo tuyo. 
 – Dejan de importante los otros 
  Cuando conoces a alguien sólo ves un órgano sexual en potencia para servir a tus cometidos y te conviertes en un monstruo utilitarista. Eso tampoco te va a llevar a ningún lugar; al contrario, ¿de qué sirve tener una larga lista de encuentros sexuales si lo que necesitas, además, es un poco de compañía íntima? ¿En serio quieres seguir de esta manera? Es decir, a nadie debería importarle y de hecho a nadie le interesa más que a ti, pero no es justo. Ni siquiera es sano que continúes pensando en tus catástrofes de vida sexual ; considera que seguramente esa enajenación que ahora padeces en cuanto al tema es una pieza más de toda la insatisfacción  que experimentas. Relájate, aclara tu mente y trabaja seriamente, no con ideas insulsas, para encamarte con alguien más. 
 *** Te puede interesar: 
 Tu permanente soltería explicada por tu signo zodiacal 
 10 películas que toda Bridget Jones en el mundo debe ver 
 * Referencia: 
 Pucker Mob