17 estrategias infalibles para superar la ansiedad de todos los días

 
 Sufrir de  ansiedad es algo serio y no debe tomarse como una enfermedad de moda o algo que sólo sirve para evadir las responsabilidades. Dadas nuestras últimas maneras de vivir tanto personal, social y emocionalmente, el estrés se ha apoderado de las masas con mayor fuerza que antes y ha orillado a resultados catastróficos que quizá nunca imaginamos. Y es justamente esa presión en el pecho la que no nos deja vivir; esa resistencia a los eventos que nos rodean es lo que lleva a nuestros dolores temporales a convertirse en una constante que quita el sueño. 
  Como personas ansiosas, sabemos que escapar de este sentimiento no es tan fácil; no basta con un simple “relájate” ni con renunciar al trabajo, los estudios o cualquier cosa que esté causando mella. Por eso es un poco cansado que los demás sólo puedan dar consejos que involucren estoicismo o buenos ánimos. Sin embargo, sí podemos admitir dos cosas: a) solemos rehusarnos a la realidad presente y b) nos preocupamos por todo y cualquier cosa siempre. No dejamos que las cosas transcurran. Así que, si queremos disminuir el estrés y conquistar la ansiedad ante los días, lo primero es abrazar lo vivido tal cual se aparece. Hay que dejar ir todas esas suposiciones falsas y expectativas fuera de lo alcanzable para no estar en una oscura cueva de malos pensamientos. 
   
 -Todos deberíamos tener la oportunidad de ver un atardecer sin importar la ajetreada ciudad en que vivamos- Si superamos estos dos puntos iniciales, entonces podemos tomar otros consejos — los que aquí mencionamos — para seguir en prácticas que no, no son sencillas, pero si de verdad lo intentamos, lo queremos con todas las ganas, harán una gran diferencia con tus tardes. A continuación se encuentran 17 puntos infalibles para alguien que, aceptando su existencia con todo lo bueno y lo malo que le conforme, también esté dispuesto a ver con mejor cara las cosas y cambiar el oxígeno que circunda al planeta. 
  – Fuera máscaras: Deja que los demás te vean como de verdad eres. Todas tus imperfecciones y bondades son suficientes para que las personas indicadas se queden a tu lado. – Confía en tu intuición: No necesitas justificarte ante nadie. Todos deben aceptar eso; empezando por ti. 
  
 – Abandona lo que no te está funcionando: Empieza por esos pensamientos que sólo generan una telaraña en tu mente. – Toma decisiones firmes: Sobre todo cuando estés dudando. El estrés y la dificultad de las cosas están en la manera cómo reaccionas, no en cómo se da la vida.  
 – Respira: No importa el tamaño de los problemas; primero debes estar en paz. Si no, todo sale mal. – Organízate: A veces no se puede decir que sí a todo. Por más buenas que sean algunas oportunidades, no podemos abarcar más de la cuenta. 
  
 – Practica la gratitud: No busques razones para estar de malas. Mejor recuerda siempre la fortuna que te rodea en ciertas situaciones. – No todo es personal: Lo que los demás hacen es por ellos, no por ti. Así que, bueno o malo, no pienses que todo debe repercutir en tu persona. 
  
 – Reconoce: Tienes dos cosas que aceptar en tu mente, probablemente tienes más de lo que necesitas y debes empezar a codiciar menos de lo posible. – Experimenta antes que acumular: Las vivencias son mil veces más importantes y significativas que cualquier adquisición material. 
  
 – Haz lo mejor que puedas: Y disfrútalo. Si sale mal, ya te ocuparás de ello después. – Enfócate en lo positivo: No necesitas estar en un grupo de optimistas para saber que mientras mejor disposición tengas ante la vida, mejores cosas verás. 
  
 – Deja ir: Si algo ya no está. Pues no está y ya. No te aferres. – Sé humilde: De lo contrario, no tendrás la mente abierta nunca y grandes experiencias se escaparán de tus manos. 
  
 – Ve despacio: Si sigues pensando que a mayor rapidez, mejores resultados, te saltará tanto un ojo que se escapará de tu rostro. – Disciplínate: Los hábitos suelen generar felicidad. Además de aportar aptitudes y habilidades exitosas. 
 – Descansa: Duerme; un cerebro y un cuerpo cansados son más propensos a destruir las buenas cosas en la vida. Y así, cuando las tengas enfrente, sabrás reconocerlas. 
  Siguiendo estos consejos verás cómo poco a poco el drama ya no encontrará cabida en tus días y las buenas acciones, personas y relaciones llegarán por sí solas. Suena a que intentamos hallar el hilo negro de la creación o formular una receta secreta para la supervivencia de una ansiedad a la que no le vemos fin. 
 *** Te puede interesar: 
 11 cosas que sólo las personas con ansiedad piensan durante el sexo 
 10 hábitos para olvidarte de la ansiedad del futuro  
 * Referencia: 
 Marc & Angel