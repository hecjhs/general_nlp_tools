13 tipos de hombres de los que debes evitar enamorarte

 Desde que somos muy jóvenes comenzamos con la larga y exhaustiva búsqueda de nuestro compañero de vida. Si lo pensaron, no me refiero a la necesidad de tener a un hombre a nuestro lado, me refiero a ese sentimiento de plenitud y bienestar que llega con esa persona. Prueba de ensayo y error, tropezón tras tropezón, reparar el corazón una y otra vez, y tenerlo listo para la siguiente aventura. 
 La verdad es que mientras llega el amor de nuestras vidas podemos hacer el paseo más divertido y menos doloroso. ¿Cómo? Hemos compilado algunos tipos de hombres de los que deberías evitar enamorarte ; créenos, te vas a ahorrar mucho tiempo y lágrimas. 
  
 El huevón 
 No estudia, no trabaja, no hace nada de su vida y vive en casa de sus padres, que no tiene nada de malo, pero no planea ni remotamente salir de ahí. Encima de eso, espera que hagas cosas por él porque a eso lo acostumbraron sus “papis”, pero eso sí, no moverá un dedo por complacerte. 
 El patán 
 No te llama ni te busca. Si te hace el favor de concederte una cita, te trata mal y se le van los ojos a la menor provocación: con la mesera, tu mejor amiga y lo que se mueva. 
 El Todas mías 
 Quiere salir contigo; te manda mensajes, te invita a salir y te roba uno que otro beso. El problema es que hace exactamente lo mismo con otras cinco mujeres. Es el típico galán carismático que quiere caerle bien a todo el mundo, especialmente a las mujeres. 
  
 El Estoico  
 Ni siquiera sabes si le gustas o no porque es 100% inexpresivo. Cuando por fin sales con él, es como hablarle a la pared porque no se ríe, apenas habla y parece que tiene una parálisis facial. Si te molestas por algo, aunque intentes generar alguna reacción en él, no puedes lograrlo y parece que no le interesa en lo más mínimo. 
 El Pseudointelectual 
 Siempre encuentra la manera de citar a algún filósofo en sus conversaciones, aunque estén hablando del clima. Te presume su última adquisición literaria y utiliza un vocabulario rimbombante para impresionarte. Se queja del cine comercial porque “él sólo ve cine de arte” y subestima tu inteligencia. 
 El Vicioso 
 Se le hace muy normal salir a tomar en martes y probablemente fuma dos cajetillas al día. Sin falla, sale todos los fines de semana y de vez en cuando inhala algo más fuerte. 
  
 El Tacaño 
 Si en la primera cita te sugirió que pagaran en partes iguales, probablemente estés saliendo con un tacaño. Nunca va por ti hasta tu casa y si lo hace, probablemente te pida que cooperes para la gasolina. Siempre tiene un discurso preparado para librarse de pagar las cuentas y con el tiempo, puede que tú termines pagándole a él. 
 El Mano Larga 
 Apenas lo conoces e insiste a tener contacto físico a la menor provocación. Te toma de la mano o de la cintura y al despedirse ya quiere besarte en la boca. 
 El Hipersensible 
 Parece que tiene síndorme premenstrual todo el tiempo. No habías conocido a un hombre que pudiera llorar ni emocionarse más que tú en toda tu vida. 
  
 El Psicópata 
 “¿Dónde estás? ¿Con quién vas? ¿A qué hora regresas?”, son preguntas típicas del psicópata. Sus celos son enfermizos y trata de controlar tu tiempo al grado de no permitirte salir con tus amigas e incluso, ponerte una falda. Revisa tu celular y tiene las contraseñas para todas tus redes, y si encuentra algo que le parezca sospechoso, ¡aguas! 
 El Chistosito 
 Inicialmente es su sentido del humor lo que llama tu atención, pero con el tiempo te das cuenta de que intenta hacer mofa de todo y se inventa los peores chistes. A eso súmale que habla sin filtro y carece de todo tacto para decirte las cosas. 
 El Intenso 
 Puede ser que tenga muchas emociones y no sepa cómo manejarlas. Te escribe todo el tiempo y te llama diariamente para darte los buenos días y las buenas noches. Al mes de relación ya está diciendo te amo y al siguiente está planeando cómo se llamarán sus hijos. 
  
 El Hippie Poser 
 No tiene dinero para gastar contigo pero sí para comprar marihuana y mantener su estilo de vida bohemio . Se queja del gobierno todo el tiempo pero no hace el mínimo esfuerzo para contribuir con un cambio. Algunos se quejan del sistema y por no querer pertenecer a él se niegan a tener un trabajo capitalista , que no está mal si están dispuestos a ganarse la vida de la manera que mejor les parezca; sin embargo, el hippie poser se niega a hacer una u otra porque casi siempre depende de los ingresos de sus padres. 
 *** 
 Te puede interesar: 
 Razones por las que deberías enamorarte de un metalero Todo lo que las mujeres siempre se preguntaron de los hombres