44 maneras en las que estás arruinando tu vida sin darte cuenta

 La vida no viene con un manual, una guía de supervivencia ni claves para superar los niveles difíciles. Eres arrojado a un mundo que dista mucho del paraíso del vientre de tu madre, y día a día, golpe a golpe, la vida te pone a prueba. Unos dicen que sólo naces para sufrir, mientras otros que vienes a aprender y a morir. Sin importar el sentido que cada uno guarde para su mortal existencia, uno no puede negar las oportunidades que tiene al experimentar la vida desde los 5 sentidos. ¿Cuál ha sido tu mejor experiencia hasta ahora? 
 Fotografía por Laura Makabresku 
 El día a día, los problemas existenciales y los pequeños detalles que nos sacan de quicio provocan que nos dejemos llevar por el flujo de la rutina, sin detenernos a observar lo que pasa a nuestro alrededor. E incluso, en algunos momentos, dejamos de vernos a nosotros mismos reflejados en el espejo. Aunque la vida nos pervierta a caer en un espiral en el que parece que el tiempo transcurre lentamente, muy tarde nos percatamos de que en realidad el tiempo pasó sin que nos diéramos cuenta. 
 Para evitar que llegue el momento en que todo a nuestro alrededor deje de tener sentido, bien vale la pena reflexionar sobre pequeños detalles y errores que estamos cometiendo y no nos llevarán a ninguna parte. No porque nuestra vida sea un error y nuestras conductas estén equivocadas, sino porque siempre podemos mejorar y acercarnos un poco más a aquello que los filósofos llamaron felicidad. 
 1.- Perder el tiempo arrepintiéndote por el pasado 
 2.- Quejarte de tus problemas sin hacer nada 
 3.- Dejar de perseguir tus sueños por lo difícil del camino 
 4.- Ignorar tu verdadera pasión e ir sólo por el dinero 
  
 5.- Crear un estilo de vida falso sólo para impresionar 
 6.- Gastar más de lo que tienes 
 7.- Obsesionarte por controlar todo a tu alrededor 
 8.- Creer que nadie puede ayudarte o enseñarte algo 
 Fotografía por Laura Makabresku 
 9.- Decir cosas que sabes que pueden herir a los demás 
 10.- Caer en la ansiedad de controlar el futuro 
 11.- No enfrentar pequeños problemas que a la larga pueden ser peores 
 12.- Envidiar a alguien sólo por su estilo de vida 
  
 13.- Esperar algo de los demás 
 14.- Buscar que todo lo que haces impresione a alguien más 
 15.- Mentirte a ti mismo respecto a lo que quieres en la vida 
 16.- Trabajar o hacer algo que no te gusta y dejarte llevar por el paso de los días 
  
 17.- Vivir a la sombra de alguien más 
 18.- Nunca terminar lo que empiezas 
 19.- Tenerle miedo a viajar y conocer nuevos lugares 
 20.- Evitar estar solo y hablar con tus demonios 
  
 21.- No aprender cosas nuevas 
 22.- Creer que la educación termina en la universidad 
 23.- No hacerte responsable de tus actos, palabras y silencios 
 24.- No confiar en nadie 
 Fotografía por Laura Makabresku 
 25.- Hacer múltiples cosas a la vez; cada una necesita su propio tiempo y momento 
 26.- Olvidarte del tiempo y detalles para consentirte 
 27.- No ayudar a los demás por iniciativa propia o sin recibir beneficio propio 
 28.- Olvidarte del ejercicio 
 Fotografía por Laura Makabresku 
 29.- No decir lo que piensas y no pensar lo que dices 
 30.- Cerrarle tu vida a los demás 
 31.- Escuchar los problemas de los demás 
 32.- Olvidarte del mundo real por las drogas y el alcohol 
 33.- Perderte en la televisión 
 34.- Dejar morir a tu niño interior 
 35.- Tomarte las cosas de manera personal 
 36.- Sabes perfectamente lo que te daña, pero no dejas de hacerlo 
  
 37.- Menospreciar las opiniones y gustos de los demás 
 38.- Resistirte al cambio 
 39.- Intentar alcanzar la perfección 
 40.- No enamorarte 
  
 41.- No abrir tu mente a través de libros y películas 
 42.- Nunca planear las cosas 
 43.- Perder a tus amigos por una relación enfermiza 
 44.- Tener miedo a probar cosas nuevas 
 – Referencia: 50 ways to waste a life 
 – Te puede interesar: 
 85 preguntas para cambiar tu vida en 2016 
 10 hábitos que evitarán que te distancies de tu pareja 
 12 consejos de Ray Bradbury para quienes quieren ser escritores