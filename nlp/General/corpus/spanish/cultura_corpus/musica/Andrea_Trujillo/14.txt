Diseños de tatuajes que sólo los fans de Arctic Monkeys amarán

 
 ¿Por qué hacerte un tatuaje inspirado en tu banda favorita? La pregunta correcta sería: ¿y por qué no? La música, y más aún, los artistas que la interpretan pueden tener una fuerte influencia sobre nosotros. En muchas ocasiones sucede que el nuevo sencillo de nuestro grupo predilecto nos cae como anillo al dedo por las circunstancias a las que nos enfrentamos en ese momento, puede ser que los músicos resulten un ejemplo a seguir por su historia de vida o que su trayectoria nos inspire en nuestros planes personales. 
 No es extraño que personas de alrededor del mundo hayan tomado como inspiración a los Arctic Monkeys , agrupación que desde sus inicios cambiarían el rumbo de la música sin saberlo gracias a una plataforma como (MySpace) y los fans que en 2003 subieron su primera maqueta a las redes. NME se refirió a ellos como la banda más importante de nuestra generación, y no se equivocaron. 
 Sus fanáticos fueron testigos de la evolución de la banda, tanto musical como físicamente, crecieron con ellos, se inspiraron en ellos y no dudaron en dejar una marca permanente en su cuerpo que lo reafirmara. 
 Estos son algunos diseños de tatuajes que los fans Arctic Monkeys amarán. 
  Inspirados en sus álbumes 
    
 Inspirados en Alex Turner 
      
 Inspirados en sus canciones                
 O puedes inspirarte en los que lleva Alex Turner 
   
 *** 
 Te puede interesar: 
 20 cosas que no sabías de Alex Turner Tatuajes inspirados en obras y artistas famosos