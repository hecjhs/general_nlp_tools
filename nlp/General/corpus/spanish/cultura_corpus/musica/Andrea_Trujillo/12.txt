10 grandes canciones para dedicar a lo más hermoso de este mundo: la mujer

 
 No se trata de un discurso feminista ni mucho menos, pero es necesario darle crédito al género bajo el que me tocó nacer. Soy mujer y estoy orgullosa de ello, reconozco y respeto a todas y cada una de mis hermanas alrededor del mundo; agradezco pertenecer al sexo débil que le ha tomado cientos de años luchar por un lugar en la sociedad, litros de sudor y sangre para ganarse el respeto de los demás y la tenacidad con que ha enfrentado toda adversidad. Somos maravillosos seres que día con día nos enfrentamos con discriminación de género , miradas lascivas, desigualdad e incontables injusticias en una sociedad patriarcal. Es una batalla constante con la ideología obsoleta que aún corrompe la mentalidad de nuestra sociedad actual y como fieras no bajamos la guardia. En nosotras vive el equilibrio nato que nos permite ser la figura femenina delicada, esposa y madre dispuesta a darlo todo por los suyos y al mismo tiempo una guerrera que ha hecho temblar al mundo con sus pasos. Estos atributos son la dualidad perfecta que nos caracteriza y con que nos hemos abierto camino en un mundo empedrado. Igualmente me quito el sombrero ante los hombres que han sabido reconocer el verdadero significado de ser mujer y quienes han honrado a la suya, llámese madre, pareja o hermana. Ante aquellos que como símbolo de su amor y respeto han creado maravillosas obras de arte dedicadas a ella y escrito los más bellos poemas y canciones como tributo. Es a través de la música como casi se ha logrado definir a la mujer y es por eso que hemos compilado diez canciones escritas especialmente para nosotras. 
 Woman – John Lennon En una entrevista para Rolling Stone, Lennon reveló: “En una tarde soleada en Bermuda, de pronto me di cuenta de lo que las mujeres hacen por nosotros. No sólo lo que mi Yoko hace por mí”. 
 You’re a Big Girl Now – Bob Dylan “Oh, you are on dry land  You made it there somehow  You’re a big girl now”  
 Layla – Eric Clapton 
  
 Esta canción fue inspirada por el clásico de la literatura persa “ The Story of Layla and Majnun”, de  Nizami Ganjavi. Cuenta la historia de un hombre que se enamora de una mujer y que se vuelve loco porque no puede casarse con ella, por mandato del padre de la doncella. 
 No Women No Cry – Bob Marley 
  
 En la canción original, se lee “No, Woman, Nuh cry”; “Nuh” significa “don’t” en jamaiquino. La versión que incluye el disco “Natty Dread” es muy diferente a como la mayoría la conoce, es un poco más rápida y larga. 
 Calico Skies – Paul McCartney 
  
  McCartney, escribió esta canción después de las afecciones del Huracán Bob en agosto de 1991. Él se encontraba en Long Island en aquel entonces, tomó su guitarra y escribió lo que él llamó una canción de amor que se convierta en una canción de protesta de los años 60. 
 The Greatest – Cat Power 
  
 “Once I wanted to be the greatest Two fists of solid rock With brains that could explain Any feeling” 
 Mama, I’m Coming Home – Ozzy Osbourne 
  
 Ozzy escribió el track para Sharon y expresó: “ Mama, I’m Coming Home  es algo que siempre le he dicho a mi esposa al teléfono cuando la gira está por terminar”. Osbourne dejó las drogas y el alcohol antes de grabar este tema; dice que fue gracias a Sharon que cambió su estilo de vida y admite que ya estaría muerto de no haberlo hecho. 
 Four Women – Nina Simone 
  
 Con respecto a este tema, Simone explicó: “Me di cuenta de que habíamos sido inducidas a sentirnos mal acerca de nosotras mismas por culpa de otros hombres de color y también blancos”, refiriéndose a las injusticias cometidas hacia la raza negra, particularmente hacia las mujeres a quienes hicieron sentir mal por su cuerpo y complexión. 
 Thank God for Girls – Weezer 
 En esta canción Rivers Cuomo expresa muy claramente su gratitud hacia el sexo femenino.  “She’s so big She’s so strong She’s so energetic in her sweaty overalls Thank God for girls” 
 Black Magic Woman – Carlos Santana 
  
 “Yes, you got your spell on me baby Turning my heart into stone I need you so bad, magic woman I can’t leave you alone” 
 *** 
 Te puede interesar: 
 Canciones que te gustaría que te dedicaran una vez en la vidaYo no soy mujer de nadie, mi sensualidad me pertenece