Janis Joplin y otros grandes músicos que encontraron la inspiración en la pintura

 
 ¿Qué es lo que hace artista a un artista? Tal vez en este video encontremos la respuesta… 
  
 Dicen que quien nace para artista lleva en las venas el talento, y aunque pueden tener mayores aptitudes en alguna disciplina en específico, casi siempre hay otras habilidades que rebasan su área de especialidad. Esto puede deberse a diferentes razones; una, por ejemplo, puede ser la sensibilidad y capacidad para la percepción y reflexión que caracteriza a las mentes creativas; otra, es el haber nacido en un nicho de artistas, y por último, el cómo la educación no se centra únicamente en una materia, sino que explora otras disciplinas con la finalidad de construir a un artista integral y enriquecer su trabajo a partir de otras perspectivas. ¿Qué sería de un bailarín si durante su formación no aprendiera los conceptos básicos del ritmo y musicalidad? 
 Actualmente, la creación artística no se encuentra arraigada a una disciplina o técnica específica, aislada de las otras como solía ocurrir anteriormente. La interrelación de las artes ha propiciado el enriquecimiento del proceso creativo, la ampliación de los horizontes del creador, y permite la coexistencia y cruzamiento de los distintos procesos de creación. En el caso de la música, artistas como David Bowie , Marilyn Manson, Kurt Cobain, John Lennon y Paul McCartney experimentaron con las artes plásticas y visuales, alternando su labor como cantantes e instrumentistas con la de pintores e ilustradores. Sin embargo, existen otros nombres poco mencionados de grandes músicos que han dedicado una buena parte de sus vidas al arte pictórico. 
 
 Bob Dylan 
  En 1994, Dylan publicó un libro con una serie de bocetos que realizó entre 1989 y 1992, titulado “ Drawn Blank”. Posteriormente, en 2006, Ingrid Mössinger, directora del museo alemán, Kunstsammlungen Chemnitz, le sugirió agrandar las obras, utilizando acuarela y goucache. Trabajó durante ocho meses, produciendo alrededor de 170 pinturas y una parte de la obra fue expuesta por primera vez en Alemania a finales de 2007.     
 Miles Davis 
  Pocos años antes de su muerte, Miles Davis expresó que para él, pintar era terapéutico, y que mantenía su mente ocupada cuando no estaba haciendo música. Trabajó principalmente con lápiz y pintura, y su estilo oscilaba entre el arte figurativo y el abstracto. Las figuras andróginas y el arte tribal africano eran lo temas más recurrentes en su pintura. En 2013, su obra completa fue publicada en el libro “Miles Davis: The Collected Artwork”. 
     
 Patti Smith 
   Patti Smith comenzó a explorar las artes visuales en los 60. Después de ser la musa del fotógrafo Robert Mapplethorpe, Smith heredó la Polaroid Land que él utilizaba para retratarla. En 2008, Smith expuso la obra que creó entre 1967 y 2007 en el Fondation Cartier pour l’Art Contemporain de París, que exaltó la relación que había entre su vida artística y espiritual a través de sus dibujos, fotografías y filmes. 
   
 Ronnie Wood 
  Ronnie Wood se formó en el Ealing College of Art, además de haber nacido en una familia en donde todos se dedicaron a la música y a las artes. Aunque eligió a la música como su carrera principal, Wood siempre llevó consigo a la pintura a lo largo de su vida. Muchas de sus obras incluyen al resto de los integrantes de The Rolling Stones, otros músicos y celebridades; sin embargo, hay otros en los que se muestra un lado más íntimo, donde retratando a su familia y vida en el hogar.  
    
 Janis Joplin Janis Joplin fue una figura representativa en la escena musical de los años 60; sin embargo, poco se conoce de su de afición por el arte pictórico que construyó desde muy temprana edad. Antes de incursionar en la música, su deseo era convertirse en diseñadora de modas; en su obra, Janis logra capturar su pasión por el arte y la moda. La mayor parte de sus pinturas las realizó entre los 12 y 20 años.    
 Devendra Banhart 
  Banhart estudió artes en el San Francisco Art Institute antes de inclinarse por la música; sin embargo, su carrera como artista visual no ha parado y está constantemente creando y ocasionalmente exhibe su obra, que se basa en ilustraciones, pinturas y fotografía. Recientemente, su obra fue publicada en el libro “I Left My Noodle on Ramen Street”, una recopilación que muestra su trayectoria por las artes visuales. Devendra combina ambas de sus carreras, creando el arte de las portadas de sus discos, incluso interviniendo las ilustraciones con las letras de sus canciones. 
    
 No sólo fueron ellos quienes encontraron otra fuente de inspiración en la pintura, además de la música. Otros artistas que trabajaron con pincel y lienzo fueron Kim Gordon (Sonic Youth), Ryan Adams, Tony Bennett y Joni Mitchell, por mencionar algunos ¿A qué otros músicos conoces que hayan incursionado en las artes plásticas o visuales? 
  
 *** Te puede interesar: 
 Rockeros y pintores, cuando la pintura se vuelve una extensiónTranspose, el arte de convertir la literatura en música