Canciones que no entenderás hasta que sepas lo que significa el amor

 
 La primera vez que experimentaste una atracción por otra persona no tenías idea de lo que estaba en curso, no entendías porqué siempre querías estar con ese ser, lo único que sabías es que siempre pensabas en él y toda parecía perfecto cuando estabas a su lado. En la primera relación parece que lo que te está pasando es lo más intenso que experimentarás en tu vida; conforme el tiempo pasa e interactúas con otras personas, comienzas a darte cuenta de que esto no es así. Creíste haber experimentado la mejor relación porque no habías encontrado a otra persona que correspondiera tu amor con la misma intensidad. En pocas palabras, sólo puedes tomar conciencia de que sientes algo ideal después de experimentar múltiples escenarios. 
  
 Es por todo esto que nunca sabrás cuándo comienzas a amar a alguien hasta que llega una persona te enseña como hacerlo. Te das cuenta de que lo que antes llamabas amor, en realidad era una simple atracción física que pasaba tras unos meses juntos o era una ligera conexión que provocaba que apreciaras a otra persona. Puedes pensar que tus sentimientos iniciales tienen grandes paralelismos con las siguientes canciones, pero únicamente las comprenderás por completo si has estado o estás en una relación te haya enseñado todo lo que implica amar a otra persona. 
 – 
 Los sentimientos que experimentas por el otro 
 El amor va más allá de estar físicamente atraído por otra persona. Implica preocupación, apoyo, tolerancia y por supuesto, afecto. Nada es mejor que tener la certeza de que tienes a una persona que está dispuesta a todo para ayudarte y que tú también estás convencido de hacer lo mismo. 
 – ‘Forever’ – Dropkick Murphys “At times we may fall, like we all tend to do, But I’ll reach out and find that I’ve run into you, Your strength is the power that carried me through Forever”.   – ‘Make You Feel My Love’ – Adele 
  
 “When the rain is blowing in your face, And the whole world is on your case, I could offer you a warm embrace To make you feel my love”. 
 – ‘I’m Yours’ – Jason Mraz 
  
 “Well, open up your mind and see like me, Open up your plans and damn you’re free. Look into your heart and you’ll find love”. 
 – 
 La comprensión 
 Amar a alguien significa aceptarlo con todo lo que implica. Hay cosas que seguramente te molestarán porque es un ser humano y nadie es perfecto, a pesar de esto jamás intentarás cambiarlas porque parte de una relación es amar al otro tal y como es. 
 – ‘How To Save a Life’ –  The Fray 
  
 “Let him know that you know best, ‘Cause after all you do know best”. 
 – ‘All The Same’ – Sick Puppies 
  “I dont mind where you come from  As long as you come to me, But I don’t like illusions I can’t see Them clearly, I don’t care, no I wouldn’t dare  To fix the twist in you”. 
 – ‘Sometimes You Can’t Make It On Your Own’ – U2 
  
 “We fight all the time You and I…that’s alright. We’re the same soul I don’t need…I don’t need to hear you say That if we weren’t so alike You’d like me a whole lot more”. 
 – 
 Los problemas 
 El amor implica aceptar al otro, en todas las relaciones humanas inevitablemente surgirán problemas o discusiones que en algún momento pueden acabar en una separación. Cuando existe algo más, una ruptura no está contemplada, así que intentas encontrar una solución y colocarte en el lugar de la otra persona 
 – ‘Black’ – Pearl Jam 
  
 “I know someday you’ll have a beautiful life, I know you’ll be a star in somebody else’s sky, But why, why, why can’t it be, can’t it be mine?”. 
 – “Say” – John Mayer 
  
 “Take all of your wasted honor Every little past frustration Take all of your so-called problems, Better put ‘em in quotations”. 
 – ‘It’s Not Over’ – Secondhand Serenade 
  
 “It’s not over. It’s not over, it’s never over, unless you let it take you, it’s not over, It’s not over, it’s not over, unless you let it break you. It’s not over”.– Cuando has encontrado un amor de verdad, vale la pena tomar todas las medidas posibles con tal de salvar la relación . Una unión de este tipo te hace ver al mundo de una manera completamente diferente y te hace crecer como persona. 
 *** Te puede interesar: 
 16 señales poco románticas que prueban que estás con la persona indicada 12 canciones que te enseñarán todo lo que debes saber sobre desamor