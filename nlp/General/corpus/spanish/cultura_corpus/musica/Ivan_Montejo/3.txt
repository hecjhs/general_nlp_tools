15 conciertos impactantes que dejaron en shock a la humanidad

 
 La memoria es selectiva y únicamente recuerda los datos que constantemente utilizamos o las experiencias que son sumamente importantes: el primer beso, la muerte de un familiar querido, el primer día de clases o un viaje especial. A estos momentos se le agregan acontecimientos históricos que marcan a toda una generación: la crisis de los misiles, la caída del muro de Berlín o el ataque a las Torres Gemelas. 
  
 En primera instancia, ambos recuerdos parecen similares; recordamos lo que estábamos haciendo cuando nos enteramos, volvemos a experimentar el dolor o placer que sentimos e incluso podemos rememorar los días anteriores al evento. Sin embargo, hay una gran diferencia entre ambos, unos evocan a experiencias personales y los otros son eventos que sucedieron en lugares muy lejanos. 
 Los conciertos son ideados para crear recuerdos personales y únicos, nada se compara con observar a tu artista favorito interpretar esa melodía que has escuchado cientos de veces. No obstante, hay momentos en los que las experiencias personales trascienden los recitales e impactan a la sociedad. Cárceles, quema de instrumentos, asesinatos, violaciones y reuniones extrañas, todos fueron escenarios y eventos que mostraron la gloria y el terror que puede provocar la música. 
 – Bob Dylan – Newport Folk Festival, Rhode Island (1965) 
  
 En las dos ediciones anteriores, Bob Dylan había conquistado a los asistentes del Festival de Folk de Newport. Después de un altercado con la organización del evento, Dylan decidió utilizar una guitarra eléctrica en su presentación para atentar en contra de la naturaleza del evento. Durante su presentación el público quedó consternado, muchos lo abuchearon, otros lo ovacionaron y la mayoría no supo qué hacer. Sus últimas dos canciones, ‘Mr. Tambourine Man’ y ‘It’s All Over Now, Baby Blue’, fueron interpretadas con instrumentos acústicos, aspecto que fue recibido con una ovación. Dylan regresaría al festival 37 años después. 
 – Jimi Hendrix – Monterey Pop Festival, California (1967) 
  
 Woodstock es visto como el inicio de la contracultura de la década de los setenta, sin embargo, Monterey Pop fue su precursor directo. Este escenario vio el debut de Janis Joplin, pero el público fue testigo de un hecho histórico cuando presenciaron la presentación de un guitarrista afroamericano. Jimi Hendrix le dio a los asistentes un show sin precedentes: tocó acordes con los dientes, con la guitarra en su espalda y al final de su actuación le prendió fuego a su instrumento en una especie de ritual. 
 – Johnny Cash – At Folsom Prison, California (1968) 
  
 Johnny Cash, desde la publicación de su canción ‘Folsom Prison Blues’, demostró que tenía un interés por tocar en dicha prisión. Cumplió su sueño el 13 de enero de 1968, cuando se presentó en dos ocasiones. Una noche antes de la presentación, el sacerdote de la cárcel le dio a Cash una canción llamada “Greystone Chapel”, que fue escrita por un prisionero; durante la presentación cumplió el deseo del religioso y la melodía fue inmortalizada en un álbum publicado un año después. 
 – The Doors – Dinner Key Auditorium, Miami (1969) 
  
 En el recinto no había aire acondicionado y Jim Morrison esta sumamente borracho. A la mitad del concierto, el vocalista de The Doors comenzó a instigar al publico a llevar el concierto a las calles para pintar y hacer ruido fuera del recinto. La situación subió de tono cuando una persona subió al escenario y lo cubrió con champagne, ante este hecho, se quitó la camisa e incitó al publico a desnudarse. Lo que sucedió a continuación nunca ha quedado muy claro, pero muchas personas aseguraron que comenzó a masturbarse. El evento quedó inconcluso, pero tiempo después Morrison fue a juicio por exposición indecente. 
 – The Beatles – Concierto en la azotea de las oficinas de Apple Corps, Londres (1969) 
  
 La última presentación pública de los Beatles: era la tarde del 30 de enero de 1969, cuando el tecladista Billy Preston sorprendió a los empleados de un edificio de oficinas con un concierto improvisado. Durante 42 minutos tocaron nueve canciones hasta que la Policía Metropolitana de Londres les pidió reducir el volumen del evento. 
 – The Rolling Stones – Altamont Speedway Free Festival, California (1969) 
  
 Al conocer el éxito de Woodstock, los Rolling Stones decidieron replicar este evento en la Costa Oeste. El concierto integró a músicos de la talla de Santana, Jefferson Airplane, Crosby, Stills y Nash and Young. Al ser un concierto alternativo, los Hells Angels se encargaron de la seguridad, en varias ocasiones tuvieron altercados con el público debido a su incapacidad para controlar a la multitud. Los problemas tuvieron consecuencias letales cuando Meredith Hunter fue asesinada a puñaladas durante la presentación de los Rolling Stones, presumiblemente le había mostrado un revólver a los motociclistas y estos respondieron con las armas blancas. La banda inglesa decidió suspender definitivamente el evento después de varias interrupciones. 
 – Sex Pistols – Silver Jubilee of Elizabeth II (1977) 
  
 El año 1977 marcó el 25 aniversario del ascenso al trono de la reina Isabel de Inglaterra. En el marco de la celebración, Sex Pistols publicó ‘God Save the Queen’, himno de crítica hacia la monarquía; para llevar este ataque un paso más allá, rentaron un bote privado para ofrecer un concierto gratuito en el río Támesis. Ante esta protesta, la policía rodeó a la embarcación y los obligó a anclar, una vez en tierra prosiguió con el arresto del manager de la banda y de varios miembros de la tripulación. 
 – Pink Floyd – The Wall Tour (1980-1981) 
  
 Fueron en total 31 conciertos que revolucionaron al mundo del rock. Videos, marionetas gigantes y un monumental muro sorprendieron a miles de espectadores. Esta gran puesta en escena no fue gratuita, se estimó que el costo del tour alcanzó 1.5 millones de dólares, razón que provocó un gran déficit en la economía de la banda. La importancia de esta gira cobra un mayor sentido cuando se toma en cuenta que fue la última en la que participó Roger Waters 
 – AC/DC, Metallica y Pantera – Monsters of Rock, Moscú (1991) 
  
 El 19 de agosto de 1991, los altos cargos de la Unión Soviética intentaron finalizar los cambios que comandaba Mijaíl Gorbachov a través de un golpe de estado. Sus intentos fracasaron y menos de dos meses después se llevó a cabo el primer concierto de rock en la historia de Rusia. En un principio parecía que la cara soviética de la Primavera de Praga volvería a aparecer en el lugar; durante la actuación de The Black Crowes hubo altercados con la autoridad que obligaron a parar el concierto. Sin embargo, la organización del evento pudo controlar la situación y se convirtió en una señal del cambio que desintegraría a la Unión Soviética tres meses después. 
 – Varios artistas – The Freddie Mercury Tribute Concert, Wembley Stadium, Londres (1992) 
  
 El mundo de la música no fue el mismo después de la muerte de Freddie Mercury el 24 de noviembre de 1991. La noticia llegó tras varios meses en los que se rumoraba que Mercury tenía VIH, este hecho motivó a los miembros restantes de Queen a organizar un concierto tributo que recordara la memoria de su compañero y concienciara al público acerca de la existencia del sida. El evento reunió a los mas grandes artistas de rock de la época, entre los que estuvieron Elton John, Tony Iommi, David Bowie, James Hetfield, George Michael, Robert Plant, Axl Rose y Slash. El evento fue visto por 500 millones de televidentes y recaudó 20 millones de libras. 
 – Nine Inch Nails – Woodstock (1994) 
  
 Festival organizado para conmemorar el 25 aniversario del festival original de 1969. La historia se repitió en este evento y los asistentes superaron con creces las expectativas iniciales que habían estimado a unas 350 mil personas, por si fuera poco, la constante lluvia convirtió al pasto de la granja en lodo que cubrió a todos los asistentes. Estos factores se manifestaron en la presentación de Nine Inch Nails: este concierto tuvo la mayor asistencia del evento y los integrantes de la banda participaron en una guerra de lodo antes de salir al escenario, por lo que tocaron todo su repertorio en estas condiciones. 
 – Limp Bizkit – Woodstock (1999) 
  
 Lo que iba a ser el aniversario número treinta del festival de la paz y el amor, se convirtió en lo que muchos llamaron “el día en que la música murió”. El calor provocó numerosos desmayos e incluso una muerte, el agua escaseaba, la presentación de Red Hot Chili Peppers dio origen a numerosas fogatas clandestinas y Rage Against the Machine quemó una bandera de Estados Unidos en el escenario. Hubo varios reportes de violaciones y asaltos sexuales, según los testimonios estos crímenes se propagaron cuando Fred Durst incitó al público durante la canción ‘Break Stuff’. 
 – Rage Against the Machine – Democratic National Convention, Los Ángeles (2000) 
  
 En el 2o00, Rage Against the Machine organizó un concierto a las afueras del recinto donde se estaba llevando acabo la Convención Nacional Demócrata. Más que un evento musical, se convirtió en una protesta gracias a la crítica de Zack de la Rocha hacia la democracia estadounidense y el ataque hacia el sistema bipartidista. A la mitad del concierto, la policía de Los Ángeles proclamó que debía suspenderse debido a medidas de seguridad, este anuncio provocó una serie de altercados con la autoridad que dejaron una gran cantidad de heridos y arrestados. La crítica social y la protesta cobraron una relevancia mucho mayor cuando George W. Bush ganó la presidencia en unas dudosas elecciones. 
 – Audioslave – Live in Cuba (2005) 
  
 El 6 de mayo de 2005, Audioslave se convirtió en la primera banda estadounidense de rock que actuó en la Habana, Cuba. Por si este hecho no fuera suficiente, tocaron el concierto más largo en la historia de la agrupación e incluso interpretaron temas de sus antiguas bandas: Sundgarden y Rage Aganist the Machine. 
 – Daft Punk – Alive 2006/2007 
  
 Su primer tour desde 1997; esta gira en numerosas ocasiones ha sido nombrada como un espectáculo memorable y deslumbrante, los visuales del evento fueron memorables ya que mezclaron tomas en vivo con efectos. Desde Coachella y Lollapalooza hasta la ciudad de México y el Lago Ness, la gran importancia de esta gira fue recopilada en el disco “Alive 20o7”, que ganó el Grammy al Mejor disco de música electrónica en el 2009. 
 – Algunos se caracterizaron por la violencia, pero la mayoría de estos eventos fueron sucesos que demostraron la capacidad que tiene la música para cambiar el entorno y mentalidades; sin duda alguna, son conciertos que quisiéramos que volvieran a suceder. 
 *** Te puede interesar: 
 Conciertos que esperamos para 2016 
 Los mejores conciertos en la historia del rock