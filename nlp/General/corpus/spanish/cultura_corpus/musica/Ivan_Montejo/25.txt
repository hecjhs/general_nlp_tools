10 canciones de The Strokes que nos recuerdan lo agridulce de ser jóvenes

 
 La palabra adolescente es muy cercana al verbo adolecer. Cuando estamos en esta edad parece que no pertenecemos a ningún lugar: la infancia parece estar muy lejana y las figuras de autoridad son un recuerdo constante de que todavía no se es un adulto. En esta edad somos una bolsa de hormonas que sólo puede ver la vida a través de extremos. Nuestros padres se convierten en los peores seres de este mundo cuando nos prohíben o nos ordenan algo; nuestros amigos son los únicos aliados en los que podemos confiar y creemos que el primer amor es lo mejor que podemos experimentar. 
 Fue precisamente en esta etapa de la vida cuando Julian Casablancas conoció a los futuros miembros de la banda que se llamaría The Strokes. Uno de los momentos más duros de su infancia fue el divorcio de sus padres, hecho crucial que cambiaría para siempre su vida ya que Sam Adoquei, su padrastro, le abrió los ojos al mundo del rock clásico que había estado oculto. 
 La joven agrupación aprovechó los beneficios de la épica y publicó su primera canción en el sitio de NME. Inmediatamente el mundo fue testigo de su talento y las disqueras saturaron sus líneas telefónicas con la intención de firmarlos.  En poco tiempo, los jóvenes que integraban a The Strokes se convirtieron en la voz de una juventud necesitada de expresar su interior. 
 – ‘Last  Nite’ 
  
 ‘Last Nite’ fue la canción llevó a The Strokes al estrellato. Esta canción abunda en la depresión y los extraños caminos que recorre, te recordará todos esos momentos en los que, sin razón alguna, sentías ser la persona más infeliz del mundo. 
 – ‘Barely Legal’ 
  
 Cuando alcanzamos la mayoría de edad creemos ser los reyes del mundo. Queremos probar todas las sustancias que estén a nuestra mano y experimentar las vivencias que podamos. Después de todo ya somos adultos y parece ser que no le tenemos que rendir cuentas a nadie. 
 – ‘You Only Live Once’ 
  
 Uno de los mayores éxitos de esta banda estadounidense, ‘You Only Live Once’ es un himno para esa etapa en la que puedes darte el lujo de tomar todo a la ligera y sólo enfocarte en los placeres de la vida. Cuando crecemos, desgraciadamente, ya no podemos seguir haciendo esto; debemos de conseguir un trabajo, asumir responsabilidades  y cumplir deberes. 
 – ‘Modern Age’ 
  
 Cuando somos jóvenes el mundo es sumamente extraño: apenas comenzamos a aprender lo que significa sentir, creemos que somos incomprendidos y no entendemos por qué nuestros padres sólo quieren arruinar nuestras vidas. ‘Modern Age’ retoma estos sentimientos y nos muestra  la extrañeza de la vida moderna a la que nos enfrentamos. 
 – ‘Soma’ 
  
 Inspirada en la “droga perfecta” imaginada por Aldous Huxley en la novela “Un mundo feliz”. ‘Soma’ evoca a las innumerables dudas que llegaron a nuestra cabeza cuando se nos ofreció por primera vez consumir una sustancia prohibida que prometía abrir nuestra percepción. 
 – ‘I Can’t Win’ 
  
 El tiempo nos enseña que el fracaso siempre forma parte de la vida y solamente crecemos cuando aprendemos a vivir con él. Sin embargo, cuando somos jóvenes creemos que cuando termina una relación o cometemos un error no hay forma de dar vuelta atrás y estamos próximos al fin del mundo. 
 – ‘Automatic Stop’ 
  
 Jamás podremos olvidar a nuestro primer amor. ‘Automatic Stop’ evoca esa etapa de la vida en la que creíamos que lo único que se necesitaba para amar a otra persona era que ésta  sintiera lo mismo. 
 – ‘Juicebox’ 
  
 En la mayoría de las ocasiones, el primer amor de tu vida inevitablemente se convertirá en tu primer corazón roto. ‘Juicebox’ abunda en el mundo de sensaciones que produce el fin de una relación y el sentimiento de soledad que este hecho significa. 
 – ‘Under Cover of Darkness’ Una de las cosas más difíciles de hacer cuando eres joven, es aceptar el fin de una relación. ‘Under Cover of Darkness’ evoca al momento en el que estuviste dispuesto a cambiar y a hacer todo lo posible para evitar que esa persona se fuera de tu vida. 
 – ‘Trying Your Luck’ Cuando estamos aprendiendo a amar, no estamos seguros de nada y lo único que nos queda es arriesgar todo por la otra persona. ‘Trying Your Luck’ te llevará a los momentos en los estabas comenzando a aprender lo que significa estar con otra persona. 
 Cuando somos jóvenes , en pocas ocasiones tenemos la oportunidad de reflexionar sobre nuestras acciones. Siempre es bueno volver a ese tiempo en el que vida era más sencilla y cuando nuestras únicas preocupaciones tenían que ver con las labores escolares. 
 *** Te puede interesar: 
 A 10 años del último gran disco de The Strokes 
 Los mejores momentos en la carrera de The Strokes