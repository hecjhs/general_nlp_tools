11 canciones de Molotov para comprender la involución del mexicano

 
 A principios del 2007, Televisa contactó a Molotov con la intención de promocionar el Torneo de Clausura de Futbol del mismo año. Inmediatamente, seguidores de la banda criticaron a sus integrantes por aliarse con la empresa que habían atacado unos años antes; Molotov defendió sus acciones argumentando que la televisora de Emilio Azcárraga estaba en un proceso de cambio y que hicieron el video por su amor a este deporte. 
  El video y la canción fueron producidas sin mayor contratiempo, pero de un momento a otro, el proyecto fue eliminado. Micky y Tito aseguraron que  los miembros de Televisa cambiaron de opinión días después, cuando entendieron el doble sentido que tenía el “Queremos ver golazos”. Este hecho, que en apariencia tiene muy poca relevancia, es una muestra del impacto que ha causado Molotov en la sociedad mexicana: esta banda es capaz de utilizar el albur para ridiculizar la hegemonía que nos domina. 
 Según Antonio Gramsci, la hegemonía es el proceso de dirección por el cual un sector social actúa sobre el otro;  en este sistema, la dominación se crea a partir de violencia y consenso. Este aspecto no significa que todos los individuos están conformes con el poder, es por ello que existe una contrahegemonía que combate lo establecido. 
 En nuestro país, esta resistencia se ha dado desde diversos frentes y acciones: motines originados por el alza de los precios del maíz, guerras de castas, alzamientos guerrilleros, huelgas, marchas, destrucción de estatuas, revoluciones y, por supuesto, por medio de manifestaciones artísticas. La lucha en contra de lo establecido es la principal bandera que enarbola Molotov, pero ésta no es la única, ya que nos muestra un abanico musical de lo que significa ser mexicano y los problemas que esto indica. 
 – ‘Voto latino’  
  
 “Voto latino de entre las masas, voto latino para la igualdad de razas”. 
 Si tuviéramos que definir nuestra relación con Estados Unidos en una palabra, ésta sería bipolar. Por un lado, aunque no lo queramos aceptar, amamos prácticamente todos sus productos culturales: el cine, la televisión, la música y los deportes, sin embargo, al mismo tiempo seguimos teniendo el trauma de la pérdida de la mitad del territorio. 
 En la actualidad, Molotov le dio un giro a esta relación con su canción “Voto latino”: el nuevo grito de combate ahora es una exigencia histórica por el reconocimiento de la raza mexicana.  
 –  ‘El carnal de las estrellas’   
 “Te ofrece la fama como a Luis Miguel, pero antes tendrás que acostarte con él” 
 Aquí también se puede citar la canción ‘Que no te haga bobo Jacobo’. En la historia del México contemporáneo ha habido numerosas crisis que han puesto en jaque al sistema político; en teoría, los medios de comunicación masivos son una opción crítica que sirve como contrapeso al poder. ‘El carnal de las estrellas’ nos demuestra que en México las cosas no son así y que ya estamos hartos del contenido que produce la televisión. 
 –  ‘Puto’  
  
 “¡Puto! El güey que quedó conforme, ¡Puto! El que creyó lo del informe”. 
 La televisión mexicana es la clara muestra de la doble moral que existe en nuestro país. Las escenas violentas y sexuales que se muestran en las telenovelas no tienen ningún problema, pero se comete un pecado si se osa pronunciar una palabra “altisonante”. Pensada para motivar a los apáticos en los conciertos, ‘Puto’ abrió una ligera brecha en la restricción de nuestro lenguaje. 
 –  ‘Amateur’  
   
 “Nos encanta que nos digan cómo hacer nuestras canciones, viniendo de pasmeros a aceptar reclamaciones”. 
 Esta canción podría ser bautizada como “el carnal de las estrellas” de la industria musical. Día con día escuchamos un sinnúmero de nuevos artistas en la radio con un ingenio cuestionable, al mismo tiempo que en las calles hay bandas con grandes talentos que buscan por donde quiera lugares para exhibir su arte. 
 –  ‘Santo Niño de Atocha’    
 “No le pida a San Ignacio que le cuide de su hogar, mejor pida al arquitecto no construya en su lugar”. 
 Después de una larga vida de sufrimiento y martirio que la convirtió en santa, en 1688 murió Catarina de San Juan, mejor conocida como la “China poblana”; durante su funeral su cuerpo tuvo que ser resguardado para evitar que el público lo mutilara para hacerse de una reliquia. Molotov nos recuerda en esta canción que esta clase de fanatismo religioso todavía sigue vigente. Diversas personas continúan esperando una intervención divina en lugar de tomar acción. 
 –  ‘Chinga tu madre’  
  
 Pocas palabras demuestran el folclor mexicano como “la chingada”. Este simple concepto puede ser modificado un sinnúmero de veces para calificar acciones, cosas y personas (tan sólo basta imaginar la cantidad de significados que engloba el verbo “chingar”). 
 Por si fuera poco, Octavio Paz en el “Laberinto de la Soledad”, analizó este concepto y nos señala que “la chingada” es la madre que ha sufrido metafórica o realmente. En pocas palabras, todos somos hijos de la chingada. 
 –  ‘Here We Kum’    
 “Somos los que en la playa chingan los planes chingones, somos los superchilangos entre tú y tus vacaciones, ¡güey!” 
 El himno de Molotov, ‘Here we kum’ no sólo es esencial para conocer lo que hace esta banda; sino que también describe todos los elementos que están detrás de “ser mexicanos”: el humor negro,  la crítica, el albur y un largo etcétera. 
 –  ‘Lagunas metales’    
 “Me lleva la Bersuit Vergarbat. Ya no viví la Maldita Vecindad. Amigos invisibles, yo nunca los vi. Los héroes del silencio, jamás los oí”. 
 Si ‘Here We Kum’ es el himno de Molotov, ‘Lagunas metales’ hace lo mismo con la música en español. Gracias a la campaña “ Rock en tu idioma “, producida por la disquera BMG Ariola, cientos de bandas pudieron darse a conocer en México y en el mundo. Agrupaciones como Caifanes, Maldita Vecindad, Neón y Fobia rápidamente se convirtieron en nuevos medios para acercarnos a nuestra realidad. 
  –  ‘Hit me’   
 “Te dimos trabajo pagado y honrado, te dimos un arma para cuidarnos y el arma que usas la usas para robarnos”. 
 Para algunos la corrupción es una condición natural del ser humano, mientras que para otros es una cuestión natural. En México la corrupción es el pan de cada día aunque no nos guste aceptarlo y lo intentemos ocultar a como dé lugar. 
 El 19 de febrero de 1913, en su puesto como ministro de relaciones exteriores, Pedro Lascuráin tomó la presidencia de México ante la renuncia de Madero y Pino Suárez. Su primer acto fue nombrar a Victoriano Huerta como Secretario de Gobernación y 45 minutos después, renunció. Todas estas molestias fueron para darle un marco legal a un golpe militar, canciones como “Hit me” son armas necesarias para acabar con este tipo de encubrimientos. 
 –  ‘La raza es la pura raza’  
 “Damas y caballeros, sean todos bienvenidos. Vamos a divertirnos morenos y güeritos mestizos y negritos, también los amarillitos”. 
 El racismo es un problema que creemos que sólo tienen los vecinos del norte, los mexicanos en teoría aceptamos a cualquier persona sin importar el color. Sin embargo, nuestro pasado muestra que en muchas ocasiones se ha intentado regenerar a la población con la intención de “blanquearla”. “La raza es la pura raza” es una oportunidad ideal para reflexionar sobre los problemas que creemos que no existen, pero que en realidad sólo están ocultos. 
 – ‘E. Charles White’  
 “Elma Canon Moreno. ¡Presente!” 
 El deporte nacional por excelencia: el albur  es la esencia de la picardía mexicana. ‘E. Charles White’ es una obligación para cualquier persona de mente inocente que constantemente caiga en el doble sentido. 
 
  ¿Qué nos hace mexicanos?  Es una pregunta a partir de la cual se han escrito miles de libros. El malinchismo, la resignación, el trabajo, la corrupción, la historia, las costumbres, y la religión; todos son elementos que alguna vez nos han descrito como grupos, pero nunca estamos seguros si tienen siempre la razón. 
 
 *** Te puede interesar: 
 Molotov regresa con Agua Maldita  
 18 canciones que todo amante del rock mexicano debería conocer