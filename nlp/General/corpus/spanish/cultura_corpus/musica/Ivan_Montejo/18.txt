20 grandes canciones de grupos que desaparecieron en los últimos 20 años

 
 Todo comienza con una plática entre amigos, inspirados por sus ídolos musicales, deciden crear una banda. En un principio ensayan en la casa de un conocido y ofrecen conciertos a los que asisten menos de una decena de espectadores. Con el tiempo se comienza a correr la voz del gran talento de una nueva agrupación, en un instante reciben el primer contrato con una disquera y las cosas cambian para siempre: el cielo es el límite. 
 Con el éxito, las amistades se comienzan a romper: lo que en principio era un vínculo humano se convierte en una transacción económica, nuevos intereses y personas comienzan a dividir las metas y cuando el ego crece, se pierden los sueños iniciales. Cientos de bandas han pasado por estas etapas antes de llegar a separarse. Se asegura que la influencia de Yoko Ono acabó con los Beatles, el ego de Axl Rose hizo lo suyo con Guns N’ Roses y la adicción de Sid Vicious hizo lo propio con Sex Pistols. 
  
 Las rupturas no significan el fin del mundo, una separación puede liberar un talento que de lo contrario se hubiera visto limitado. John Lennon, Paul McCartney, Ozzy Osbourne, Sting y Morrissey son muestra de la increíble capacidad musical que ha surgido de separaciones. Lo único que nos queda hacer es escuchar las siguientes canciones y esperar que los integrantes de estas bandas puedan seguir el camino de los más grandes. 
 – 1. ‘Like a Stone’ – Audioslave  
  
 Cuando Rage Against the Machine se separó, la mayoría de sus miembros se reunieron con Chris Cornell para formar esta agrupación. Audioslave, en sus seis años de existencia, publicó tres discos y se convirtió en la primera banda estadounidense en tocar al aire libre en Cuba. 
 – 2. ‘No Sleep Till Brooklyn’ – The Beastie Boys  
  
 The Beastie Boys fueron una de las bandas que demostró que el rock y el hip hop no están peleados. ‘No Sleep Till Brooklyn’ es la mayor prueba de esta cuestión ya que colaboraron con Kerry King, guitarrista de Slayer. El 2009 parecía un año prometedor debido a que publicarían un álbum que le iba dar un giro radical a la organización, pero en ese mismo año, Adam Yauch perdió la vida y terminó la carrera de esta increíble banda. 
 – 3. ‘Love Rhymes With Hideous Car Wrecks’ – The Blood Brothers  
  
 The Blood Brothers se destaca por tener duelos vocales entre Johny Whitney y Jordan Billie, mientras que la guitarra de Votolato ha ido desde un sonido pesado hasta líneas minimalistas. Su último álbum fue publicado en el 2006, poco tiempo después se separaron por diferencias artísticas. 
 – 4. ‘You Are My Sunshine’ – The Civil Wars  
  
 The Civil Wars fue uno de los dúos más reconocidos de nuestro tiempo. Joe Willians aseguró que el particular nombre de la banda se generó mientras miraba los cientos de monumentos de la Guerra Civil estadounidense. En agosto de 2014 regalaron la canción ‘You Are My Sunshine ‘ en línea y anunciaron su separación definitiva. 
 – 5. ‘Nearer Than Heaven’ – Delays 
  
 Oficialmente siguen activos, pero los proyectos artísticos del vocalista Greg Gilbert nos hacen pensar que esta banda es parte del pasado. Delays fue conocido por el falsete de Gilbert y un sonido propio identificado por los agudos del sintetizador. 
 – 6. ‘Holy Diver’ – Dio  
  Ronnie James Dio saltó a la fama al suplir a Ozzie Osbourne en Black Sabbath, cuando decidió abandonar esta histórica banda, fundó Dio. Esta agrupación, a través de sus diez discos de estudio, se convirtió en uno de los pilares del metal. Dio llegó a su fin en el 2010 cuando su vocalista y fundador murió de cáncer de estómago. 
 – 7. ‘Entre dos tierras’ – Héroes del Silencio  
  
 En los doce años en los que estuvieron juntos, los Héroes del Silencio vendieron más de tres millones de discos en más de treinta y siete países, convirtiéndose en una de las bandas españolas más exitosas. La agrupación anunció su separación en 1996, aunque tuvieron un breve regreso en el 2007 cuando conmemoraron su vigésimo aniversario. 
 – 8. ‘Oxygen’ – JJ72 
  
 Según Mark Greaney, vocalista de la banda, el nombre de la agrupación proviene del año en que se casaron sus padres (1972) y su profunda admiración por el escritor James Joyce. JJ72, desde su debut, se convirtió en una de los grupos más importantes de la música indie hasta que en el 2006 anunciaron su separación tras diez años juntos. 
 – 9. ‘The Widow’ – The Mars Volta 
  
 Una de las mejores banda del rock progresivo, The Mars Volta fue fundada por el guitarrista Omar Rodríguez-López y el vocalista Cedric Bixler-Zavala e incorporó una increíble variedad de géneros entre los que estuvieron el jazz, fusión, hard rock y la música latinoamericana. En 2013, Cedric anunció su salida, después de hacer todo lo posible para mantener a la banda unida. 
 – 10. ‘The Ghost Of You’ – My Chemical Romance 
  
 My Chemical Romance se convirtió a principios del milenio en en la banda favorita de millones de adolescentes. Su estilo fue criticado por cientos de personas, pero esto no fue el caso del vocalista de la icónica banda de punk Black Flag, Keith Morris, que aseguró que MCR era de su agrado. En marzo de 2013 anunciaron su ruptura definitiva. 
 – 11. ‘Wonderwall’ – Oasis  
  
 Esta canción es la tercera del álbum “(What’s the Story) Morning Glory?”, pero su riff inicial se puede escuchar desde la pista número uno. En el 2009, tras una fuerte discusión entre los hermanos Gallagher, Noel decidió abandonar el grupo, en teoría las cosas seguirían sin él, pero después de algunas presentaciones, la banda cambió de nombre. 
 – 12. ‘Roxanne’ – The Police  
  
 La fusión de rock, punk, reggae y jazz hizo que The Police se convirtiera en una de las bandas inglesas más exitosas de la historia. Oficialmente se separaron a mediados de la década de los ochenta, pero la reunión de 2007 nos demostró que deben de regresar a los escenarios lo antes posible. 
 – 13. ‘Killing In the Name’ – Rage Against the Machine 
  
 En 1992, Rage Against the Machine alteró a la sociedad estadounidense con su álbum homónimo. Su agresivo sonido y el activismo que demostraban en sus canciones incomodaron a los conservadores y motivaron a los revolucionarios. ‘Killing in the Name’ volvió a impactar el mundo en el 2009 cuando una iniciativa de Facebook la colocó en el número uno de ventas en el Reino Unido. 
 – 14. ‘All Too Human’ – The Rakes 
  
 The Rakes con ‘All Too Human’ alcanzaron su mejor posición en las listas de popularidad. Durante toda su carrera fueron sumamente exigentes para ofrecer la mejor calidad posible en sus presentaciones y sus grabaciones, en el 2009 aseguraron que ya no podían llegar a estos estándares y decidieron separarse. 
 – 15. ‘The One I Love’ – R.E.M. 
  
 R.E.M. es uno de los pilares del rock alternativo. A finales de 1997, Bill Berry abandonó la agrupación dejando a Buck Mills y Stripe como los únicos miembros oficiales. Fue hasta septiembre de 2011 cuando se anunció la disolución definitiva de este influyente grupo. ‘The One I Love’ en 1987 se convirtió en su primer éxito comercial. 
 –16. ‘Cuando pase el temblor’ – Soda StereoPocas bandas latinoamericanas se pueden comparar a esta agrupación, Soda Stereo fue una banda de rock argentina que experimentó con diversos géneros musicales,  entre los que estuvieron el rock progresivo, shoegaze, neopsicodelia, britpop y la música electrónica. En septiembre de 1997 ofrecieron en el estadio de River Plate “El último concierto”. Diez años después se volvieron a reunir para una breve gira. –17. ‘Teenage Riot’  – Sonic Youth  
  
 ‘Teenage Riot’ está inspirada el el líder la la banda Dinosaur Jr., que según los miembros de la banda es una de las figuras más influyentes en sus carreras. Sonic Youth tomó sus primeras influencias del movimiento artístico No Wave y del hardcore punk y en poco tiempo se convirtieron en pioneros de la escena Noise Rock, género decisivo para el surgimiento del rock alternativo de la década los noventa. En octubre de 2011, su disquera anunció el fin definitivo de la banda. 
 – 18. ‘Don’t You Worry’ – Swedish House Mafia 
  
 Swedish House Mafia fue un supergrupo de música electrónica conformado por Axwell, Steve Angello y Sebastian Ingrosso. En junio de 2012 anunciaron que el grupo se enmarcaría en su último tour, que eventualmente finalizaría en Miami un año después. 
 – 19. ´Bitter Sweet Symphony’ – The Verve 
  
 Fue uno de los grupos británicos más importantes de los años 90, The Verve revivió el sonido psicodélico y su disco “Urban Hymns”  es uno de los más vendidos en la historia del Reino Unido. Debido a problemas creativos entre el vocalista Ashcroft y el guitarrista McCabe, la banda se separó en 1999. Ocho años después tuvieron un breve regreso en el que grabaron su álbum Forth. 
 – 20. ‘Seven Nation Army’ – The White Stripes 
  
 Por años engañaron al mundo al decir que eran hermanos. The White Stripes, a través del albúm “Elephant”, tomaron al mundo por sorpresa; desgraciadamente para la industria musical, en febrero de 2011 anunciaron su separación oficial. ‘Seven Nation Army’ se ha convertido en un himno del rock que es coreado en múltiples recintos deportivos. 
 Cuando nos apasionamos por una banda, hacemos todo lo posible por conocer todos sus secretos . Desgraciadamente, las agrupaciones tienen fecha de caducidad e inevitablemente llegará un momento en el que se separarán. Lo único que nos queda es honrar su memoria a través de su arte. 
 *** Te puede interesar: 
 10 canciones para conocer 10 grandes bandas 
 Las mejores bandas femeninas del rock