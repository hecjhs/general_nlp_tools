11 músicos latinoamericanos que encuentran inspiración en la marihuana

 
 Hay un total de 100 mil fumadores de marihuana en Estados Unidos, y la mayoría son negros, latinos, filipinos y artistas. Su música satánica, jazz y swing es el resultado del uso de la marihuana Harry Anslinger A principios del siglo XX, cientos de ciudadanos estadounidenses vieron con horror a sus vecinos del sur. Los mexicanos demostraban ser un pueblo salvaje que estaba protagonizando una guerra civil, sus sospechas se reafirmaron cuando un bandido llamado Pancho Villa comandó un ataque a la localidad de Columbus, Nuevo México. 
 Las razones detrás de esta avanzada nunca han sido claras pero la explicación más convincente apunta a que fue una acción para demostrar el rechazo hacia el reconocimiento que Estados Unidos le  dio al gobierno Carrancista. La interpretación que le dio la sociedad estadounidense de la época resalta por su extrañeza: diversas voces aseguraron que la marihuana alteraba a los mexicanos y los motivaba a cometer terribles actos de violencia. 
 A estos comentarios se les unieron nuevos argumentos que señalaron que esta droga alteraba los afroamericanos de los estados del este. En este lugar, la marihuana jugaba un importante papel dentro de los círculos que producían jazz, según testimonios prohibicionistas esta planta hacía que los negros se atrevieran a “ver a las personas blancas a los ojos, pisar la sombra de los hombres blancos y mirar a una mujer blanca dos veces”. 
  Han pasado casi cien años desde estos hechos y la situación ha sufrido modificaciones, pero en esencia sigue igual. Numerosas voces han rechazado los argumentos racistas con los que se prohibió la marihuana, para cambiarlos con discursos basados en la defensa de la juventud y algunos estudios científicos. En la otra trinchera se encuentran numerosos músicos mexicanos y latinoamericanos, que han alzado su voz para demostrar que hemos convivido con esta planta por cientos de años y que en todo ese tiempo ha sido un catalizador de la creatividad e inspiración. 
 – 1. Adrianigual 
  “[Hacer música] es más divertido con marihuana porque ríes más y pierdes contacto con la realidad, que es una enfermedad de la que todos sufrimos. Puedes entrar en contacto con tu mente y concentrarte mejor con tus ideas, que es lo que todos estamos buscando. Algunas veces no puedes fumar porque tienes que manejar en la ciudad, ir en bicicleta o tomar un exámen de matemáticas. Pero el resto del tiempo te encuentras en esta burbuja individualista de solipsismo frenético, todo gracias a una pequeña planta que puedes cosechar en tu propia casa. No estoy tratando de decir que la hierba te convertirá en un monje zen, pero te ríes, te vencen en el poker y ¿a quién le interesa cuando cuando te encuentras volando? Casi me hace llorar. ¡Viva la marihuana! ¡Vivan todas las drogas! ¡Abajo con las leyes y fronteras, la hermandad, los asesinatos en masa y el fin del mundo!” 
  
 – 2. Andy Chango 
  ” Desde los 13 años fumo porros, creo que es divertido, es relajante para el estrés y para conciliar el sueño. También creo que es interesante para cualquier persona destruir la realidad, al menos un par de veces en la vida”. 
  
 – 3. Dengue Dengue Dengue 
  “La maría nos ayuda a alcanzar el canal creativo, olvidar lo que está pasando alrededor de nosotros y sólo fluir. Ayuda a guiarnos más por nuestros instintos que por nuestra razón y definitivamente dirige hacia mejor música”. 
  
 – 4. Pepper Kilo de Füete Billete 
  “Personalmente, la hierba no influye en mi proceso creativo con Füete. Fumo marihuana porque me ayuda a navegar en la sociedad más facilmente. Soy una persona muy ansiosa y muchas veces cuando voy fuera permito que cosas tontas me afecten. Pero si fumo, no me enojo y puedo dejar pasar todo; además se siente increíble, río mucho, la comida sabe mejor y la música suena mejor”. 
  
 – 5. Joan Manuel Serrat 
  “En Barcelona lo que había era grifa. Desde pequeño recuerdo que lo fumaban marineros y legionarios. En la Plaza Real se distribuía y consumía. Luego probé el chocolate. Y donde descubrí la maría fue en América, había potentísimas variedades, verdadera bombas atómicas para el cerebro”. 
  
 – 6. Tony Gallardo, María y José 
  “De esta manera me ayuda fumar marihuana. Cada día trabajo en beats , armonías y melodías. Una vez que tengo una idea con letras en la cabeza, usualmente comienzo la canción utilizando el material que poseo. Fumar me ayuda a limpiar todas las cosas tontas de mi cabeza, para enfocarme completamente en la canción. Esto me permite deshacerme de cualquier prejuicio o duda acerca de lo que otra persona pueda pensar para únicamente concentrarme en hacer la canción que salga de mí, que sea parte de mí”. 
  
 – 7. Mamacita 
  “La hierba me ayuda a encontrar la esencia de lo que quiero comunicar con otros a través del sonido. Es lo que uso para saber cómo mostrar mis sentimientos con otros. Para componer canciones me gusta ir a la casa de mi mamá en Chillán, ir a algún lugar en donde me pueda sentir sola y libre, fumar un porro y sólo relajarme conmigo misma, ese es mi ideal. También escribo cientos de poemas sin pensarlos mucho. En ocasiones, una de las frases de los poemas se convierten en canciones. Igualmente me gusta trabajar con un amigo productor con el que me llevo muy bien y fumamos juntos, probando sonidos y efectos. La marihuana en verdad ayuda a sentir una conexión espiritual y musical con otra persona… Es como hacer el amor con tu cerebro y después dar a luz a un niño”.   
 – 8. Teen flirt 
  “Uso la marihuana como punto de referencia. Usualmente trabajo sobrio, reedito mientras estoy en un viaje y finalmente escucho por última vez la canción para estructurar mientras estoy así. Pero sí, [la marihuana] es muy importante. También me ayuda a comunicarme con mis colaboradores. Nos inspiramos y la marihuana nos ayuda a entendernos y colocarnos en la misma página”. 
  
 – 9. Jesse Baez 
  “Por lo general soy una persona ansiosa y todo me estresa. Cuando fumo y escribo música me relaja y me permite enfocarme en una cosa en lugar de miles de cosas que usualmente se encuentran en mi cabeza. No sé si alguna vez escribiré sin la influencia de la marihuana y no quiero saber si lo haré”. 
  
 – 10. Niño Árbol 
  “[la marihuana] Algunas veces es un arma de dos filos. Puede ayudarme mucho cuando comienzo una nueva pista; tocaré con ritmos o sonidos y podré construir la base de una canción con mucha velocidad (no estoy diciendo que no puedo hacer eso sin marihuana, pero con ésta tiende a fluir rápidamente). Por otro lado, cuanto tengo que mezclar la pista en un viaje puedo sentirme abrumado y aburrirme con facilidad; es como si mis oídos se cansaran más rápido que cuando estoy sobrio. En pocas palabras, cuando estoy drogado mis fuerzas se improvisan y hacen ruido, especialmente cuando hay una sesión de improvisada o un tipo de colaboración”. 
  
 – 11. Felipe Pérez de 424 
  “[cuando escribimos música] Siempre nos ha gustado alejarnos, escapar de la ciudad y combinar la marihuana con entornos naturales que nos hagan sentir en paz, esto abre la puerta y deja tu creatividad fluir. También he notado que tenemos una predilección por ciertos tempos en nuestra música y siempre le menciono a los miembros de la banda que no fuman que tiene que tocar como si estuvieran drogados. Para muchas personas, yo incluido, la marihuana puede ser una herramienta que fortalece el proceso creativo”. 
  
 Todas estas muestras de inspiración creativa son fundamentales en un tiempo en el cual se está cuestionando la pertinencia de la prohibición de la marihuana . 
 *** Te puede interesar:  
 Canciones que hablan sobre marihuana 
 Efectos positivos de las drogas  
 * Referencia: 
 Remezcla