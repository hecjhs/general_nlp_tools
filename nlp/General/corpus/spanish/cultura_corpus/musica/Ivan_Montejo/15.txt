Canciones que demuestran porqué Freddie Mercury es el mejor cantante de toda la historia

 
 Su primer escenario fue una pequeña escuela localizada en el Sultanato de Zanzíbar, su inspiración era la estrella de Bollywood Lata Mangeshkar, su primera banda se llamaba The Hectics y sus primeras canciones provenían del repertorio de Cliff Richard y Little Richard. Se llamaba Farrokh Bulsara, era parsi y seguía al zoroastrismo. 
  Pocas personas hubieran imaginado que una rebelión política en esta colonia sería fundamental para cambiar para siempre la historia del rock. En 1964, en la Revolución de Zanzíbar, rebeldes locales derrotaron al sultanato que tenía una mayoría árabe; este hecho provocó un persecución hacia todas las etnias que no tenían descendencia africana, entre los que se encontraban los parsis. Ante esta situación, los padres de Farrokh decidieron mudarse a Feltham, Inglaterra. 
 Cuatro años después se unió al guitarrista Brian May y al baterista Roger Taylor para revivir la experiencia que vivió con The Hectics. La nueva agrupación se llamaría Queen y Farrokh, el nuevo cantante, rebautizaría con el nombre “Freddie Mercury”. 
  Su talento  inmediatamente dejó una huella en la industria musical inglesa, escribió y compuso cientos de canciones e inclusive sirvió como productor de otros artistas. Sus melodías se caracterizaron por su complejidad material que incluía una estructura con docenas de acordes. 
 Desde joven demostró un gran interés por el piano y mientras vivió en Londres aprendió a tocar la guitarra. En un principio era sumamente autocrítico acerca de sus habilidades, pero con el tiempo comenzó a emplear su talento instrumental en Queen y en su carrera de solista. 
 – ‘Radio Ga Ga’ – “The Works” (1984) 
  
 Sus particulares actuaciones en el escenario crearon cientos de rumores acerca de su orientación sexual. Para muchos el nombre de la banda era una clara referencia hacia la homosexualidad, pero otros refutaban esta afirmación al señalar su relación sentimental con Mary Austin. Independientemente de la verdad, Freddie se convirtió en un símbolo de la aceptación de la diversidad. 
 – ‘I Want to Break Free’ – “The Works” (1984) 
  
 Toda esta serie de talentos, que para muchos hubieran sido suficientes para crear una carrera legendaria, quedan opacados si se les compara con su voz. Actualmente es considerado uno de los cantantes más influyentes en la historia del rock y ahora la ciencia acaba de reafirmar lo que nuestros oídos ya sabían. 
 – ‘Bohemian Rhapsody’ – “A Night at the Opera” (1975) 
  
 Un grupo de investigadores checos, austriacos y suecos acaban de publicar un estudio que analizó seis entrevistas y el repertorio musical de Queen. Uno de los primeros hallazgos que encontraron fue que a pesar de ser reconocido ampliamente por su voz de tenor, es muy probable que en realidad era un barítono. 
 – ‘Flash’ – “Flash Gordon” (1980) 
  
 Los científicos aseguraron que es sumamente difícil llegar a una conclusión clara sin tener presente al sujeto de la investigación, por ello el cantante Daniel Zangger-Borch ayudó en el estudio imitando la voz de Freddy. 
 – ‘Somebody to Love’ – “A Day at the Races” (1976) 
  
 Lo que descubrieron es que Freddie utilizó subarmónicos, un estilo de cantar en donde los pliegues ventriculares vibran junto con los pliegues vocales. La mayoría de las personas nunca son capaces de utilizar estos pliegues, a menos que practiquen cantos truvanos. 
 – ‘Love of My Life’ – “A Night at the Opera” (1975) 
  
 Aunado a esto, las cuerdas vocales de Mercury se movían más rápido que las de otras personas. La vibración típica fluctúa entre 5.4 Hz y 6.9 Hz, mientras que la de Freddie era 7.04 Hz. Este particular vibrato le dio capacidades físicas que ni siquiera Luciano Pavarotti puede igualar. 
 – ‘Under Pressure’ – “Hot Space” (1981) 
  Desgraciadamente para el mundo de la música, a finales de la década de los ochenta comenzó a circular la noticia de que Freddie tenía VIH. En numerosas ocasiones negó este rumor , pero cada vez se ausentaba más de los conciertos y los ensayos. El 22 noviembre de 1991 su manager rompió el silencio al anunciar que lo de la enfermedad era verdad, veinticuatro horas después el comunicado, Freddie Mercury perdió la vida por una neumonía causada por el virus. 
 – ‘Don’t Stop Me Now’ – “Jazz” (1979) 
  
 Desde la primera ocasión que escuchamos la voz de Freddie Mercury supimos que su capacidad musical no tuvo comparación. Ahora la ciencia comprueba lo que habíamos pensado y nos de muestra que no es coincidencia que Queen creara la canción más feliz de la historia . 
 *** Te puede interesar:  
 10 cosas que no sabías de Freddie Mercury  
 Canciones inéditas de Freddie Mercury y Michael Jackson