14 canciones inspiradas en asesinos seriales, homicidas y caníbales

 
 Fueron 16 puñaladas las que horrorizaron al mundo el 9 de agosto de 1969. A “La Familia” no le interesó que su víctima tuviera ocho meses y medio de embarazo, las órdenes de Manson eran claras: “Destruir totalmente a todo el mundo de la manera más horripilante que puedas”. Los horrores cometidos en el hogar de Roman Polanski indignaron a la sociedad estadounidense, incluyendo al mismo Manson, por lo que decidió comandar un asesinato para demostrarles la forma correcta de hacerlo.El sitio elegido fue el número 3301 de la Calle Waverly Drive, hogar de Leno LaBianca y su esposa Rosemary. Sus víctimas murieron por múltiples puñaladas causadas por una bayoneta; una de las imágenes que más aterrorizaron a las autoridades fue la frase “Helter Skelter” escrita con sangre en la puerta de la nevera. 
 Al ser arrestado, Charles Manson aseguró que tomó la frase ‘Helter Skelter’ de la canción del mismo nombre de los Beatles, aseguró que hacía referencia a una futura guerra entre la raza blanca y la negra. Los asesinatos en teoría ayudarían a que el conflicto armado llegara mucho más rápido. 
  
 Poco tiempo después los papeles se invirtieron, el hombre que había encontrado su inspiración en una canción de una banda inglesa ahora lo era para muchos artistas que escribían canciones inspiradas en los ínfimos hechos cometidos por “La Familia”. Guns N’ Roses, Ozzy Osbourne, White Zombie, Marilyn Manson y Neil Young son sólo algunos ejemplos de esta cuestión; este aspecto no es un hecho aislado, ya que existen una gran cantidad de melodías que buscan adentrarse en los terribles rincones de las mentes más dañadas de la historia 
 – Elizabeth Báthory (1560 – 1614) 
  Fuentes de la época aseguran que esta aristócrata húngara fue responsable de la muerte de 650 mujeres. Los motivos detrás de estas acciones eran claros, Elizabeth estaba obsesionada con sus belleza y creía que la sangre de vírgenes podría mantener su juventud, razón por la cual tomaba baños con este líquido vital. Estos baños son cuestionados por muchas voces que aseguran que fueron exageraciones creadas por sus enemigos directos, pero lo que sí es un hecho es que existen más de 300 testigos que aseguran que la condesa torturó y asesinó a varias mujeres. 
 – ‘Countess Bathory’ – Venom 
  
 “Welcoming the virgins fair, to live a noble life In the castle known to all, the Counts infernal wife She invites the peasants with endless lavish foods But when evening spreads it wings, she rapes them of their blood”.  
 – ‘Elizabeth’ – Ghost 
  
 “ Underneath the moonlight of old hungerian skies buried in the blood-drenched earth These barren lands of ice She was an evil woman with an evil old soul Piercing eyes emotionless a heart so black and cold”. 
 – Jack el Destripador (finales del siglo XIX) 
  En 1888, una serie de asesinatos azotaron el distrito Whitechapel de Londres. Una carca aseguró que el responsable era “Jack el Destripador”, un personaje astuto y burlón que asesinaba principalmente a prostitutas de la zona. Recibió su apodo después de extraer los órganos de al menos tres víctimas. Muchas fuentes cuestionan su existencia, pero lo que es un hecho es que ha causado un impacto único en nuestra cultura. 
 – ‘The Ripper’ – Judas Priest 
  
 “ In London town streets When there’s darkness and fog When you least expect me And you turn your back, I’ll attack”. 
 – ‘Jack The Ripper’ – Morrissey 
  
 “ And I know a place Where no one is likely to pass Oh, you don’t care if it’s late And you don’t care if you’re lost’. 
 – Ed Gein (1906 – 1984) 
  Técnicamente no es un asesino serial, pero su retorcida mente ha inspirado innumerables películas y canciones. Durante su vida reconoció el asesinato de dos mujeres, pero impactó a la sociedad estadounidense cuando las autoridades descubrieron lo que había hecho con los cuerpos. Los periódicos de la época publicaron que acostumbraba exhumar cadáveres de mujeres que habían muerto recientemente.  Con estos restos realizó numerosos objetos, entre los que se encontraron una máscara de piel humana, platos hechos de cráneos, muebles tapizados con piel humana y un cinturón hecho de pezones humanos. 
 – ‘Dead Skin Mask’ – Slayer 
  
 “ Graze the skin with my  fingertips The brush of dead warm flesh pacifies the means Incised members ornaments on my being Adulating the skin before me”. 
 – ‘Deadache’ – Lordi 
  
 “ The mad butcher goes bump in the night And loneliness It makes nights endless”. 
 – John Wayne Gacy (1942–1994) 
  Muchos pueden decir que el miedo a los payasos es injustificado, pero la terrible historia de este hombre prueba que este temor no está tan alejado de la realidad. Entre 1972 y 1978 comenzaron a desaparecer una gran cantidad de niños y jóvenes en el condado Cook, de Illinois. Después de una serie de investigaciones encontraron que John Wayne Garcy fue el culpable de por lo menos 33 asesinatos. Garcy generalmente conocía a sus víctimas cuando actuaba en fiestas infantiles como el payaso “Pogo”, después las llevaba a su hogar para violarlas y asesinarlas. Fue sentenciado a muerte en 1980 y 14 años después fue ejecutado. 
 – ’33 Something’ – Bathory 
 “The smell of love the smell of human blood and excrement. Once you’ve played with Mr. Gacy, there’s no way out. No release”. 
 – Ted Bundy (1946 – 1989) 
  Los terribles actos de Ted Bundy permanecen presentes en la mayoría de las mentes que han conocido su historia. En la década de los setenta atacó a una gran cantidad de mujeres en diversos lugares de Estados Unidos. Sus actos no se limitaron al asesinato y la violación, ya que ocasionalmente regresaba a a las escenas del crimen para tener relaciones con los cuerpos en descomposición. Fue encerrado en 1975 y confesó ser partícipe de 30 homicidios, aunque muchos aseguran que este número se queda corto. 
 – ‘Ted, Just Admit It…’ – Jane’s Addiction 
  
 “I am the killer of people You look like a meatball I’ll throw away your toothpick And ask for your giveness”. – ‘Stay Wide Awake’ – Eminem 
  
 “So dark and so cold my friends don’t know this other side of me There’s a monster inside of me it’s quite ugly and it frightens me”. 
 – Issei Sagawa (1949) 
  En 1977, Issei Sagawa se mudó a París para conseguir su doctorado en literatura. Tres años después de este hecho invitó a su compañera Renée Hartevelt con el pretexto de transcribir poesía. De repente sintió un impulso por absorber su energía y le dio un disparo en el cuello. Por dos días Sagawa comió del cuerpo de su víctima, fue arrestado por las autoridades cuando intentó deshacerse del cuerpo en un lago. A pesar de haber confesado sus crímenes nunca fue ingresado a prisión, ya que su defensa argumentó que estaba demente. Fue ingresado a un hospital psiquiátrico y diez años después fue liberado. 
 – ‘La Folie’ – The Stranglers 
  
 “ Combien de crimes ont ete commis Contre les mensonges et soi Disant les lois du coeur Combien sont la a cause de la folie Parce qu’il ont la folie”. 
 – ‘Too Much Blood’ – The Rolling Stones 
  
 “A friend of mine was this Japanese. He had a girlfriend in Paris. He tried to date her in six months and eventually she said yes. You know he took her to his apartment, cut off her head”. 
 – El Asesino del Zodiaco 
  Uno de los casos más misteriosos que ha azotado a la sociedad estadounidense. Entre 1968 y 1969 fueron asesinadas siete personas en Benicia, Vallejo, Lake Berryessa y San Francisco, tan sólo dos personas lograron sobrevivir al ataque y afirmaron que un misterioso hombre que tenía entre 20 y 30 años las atacó cuando estaban en su auto. El asunto alcanzó fama internacional cuando el responsable de las muertes contactó a los medios de comunicación para anunciarles que había asesinado a 37 personas. Por si fuera poco, mandó extrañas cartas escritas en un lenguaje desconocido. Hasta la fecha el caso sigue abierto y nadie sabe quién fue el responsable de los actos. 
 – ‘Zodiac’ – Macabre 
   “ This is the Zodiac speaking I like killing people because it is so much fun It is more fun than killing wild game in the  forest ‘Cause man is the most dangerous game of all to kill”. 
 – ‘The Zodiac’ – Kamelot 
  
 “You will never really know my name without reflection A careful devil’s irony in pure perfection In pure deceit”. 
 – Las historias de estas personas despiertan en nosotros extrañas sensaciones: por un lado no podemos evitar sentir terror ante los terribles hechos que cometieron, pero al mismo tiempo nos provocan una extraña curiosidad. Este interés puede ser alimentado con estas canciones o con documentales sobre asesinos seriales.  
 *** Te puede interesar:  
 Terribles asesinos seriales que nunca fueron atrapados  
 Las mejores películas de asesinos seriales de todos los tiempos