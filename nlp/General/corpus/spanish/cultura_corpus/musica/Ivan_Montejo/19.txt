El legendario festival que todo amante del rock psicodélico debe visitar

 
 En los famosos viajes de Lemuel Gulliver, el capitán encuentra una inmensidad de mundos fantásticos en donde es un gigante entre enanos, un enano entre gigantes y un hombre humillado en una tierra habitada por caballos. 
 Este universo imaginado por Jonathan Swift inspiró a una enorme cantidad de personas que querían experimentar las irreales aventuras protagonizadas por Gulliver. Uno de estos hombres fue Owsley Stanley, un químico autodidacta que cocinó millones de dosis de LSD , no para convertirse en un traficante de drogas, sino para mejorar la producción artística. Para promocionar la nueva sustancia, frecuentemente asistía a conciertos con la intención de guiar a los artistas en un viaje de ácido. 
  
 Fue así como comenzó a distribuir ácido a los Merry Pranksters. Este grupo que giraba alrededor del novelista Ken Kensey y organizaba eventos centrados en el  consumo del LSD, que eran acompañados por espectáculos de luz, proyección de películas y música discordante. 
 Estas reuniones y la influencia de Owsley Stanley influyeron a cientos de músicos que intentaron replicar los efectos de las drogas psicodélicas en su arte. Utilizaron nuevas técnicas y efectos de grabación basados en culturas no occidentales, instrumentos como las ragas y las pedales de la música india. Las letras se impregnaron de temas surrealistas y excéntricos, inspirados en el esoterismo o en la literatura. 
  
 De la noche a la mañana surgieron bandas y cantantes que hipnotizaron a las masas con órganos eléctricos e increíbles solos de guitarra. Fue un verano de ensueño que duró seis años, pero terminó abruptamente en 1966, cuando el LSD  fue declarado ilegal por el gobierno de Estados Unidos. A esta criminalización se le sumó el caso del asesinato de Sharon Tate, en el cual los miembros de la banda de Charles Manson  fueron vinculados con este tipo de drogas. El último hecho que terminó por aniquilar al rock psicodélico fueron las repentinas muertes de los protagonistas del género, entre las que estuvieron Brian Wilson de The Beach Boys, Brian Jones de The Rolling Stones, Peter Green de Fleetwood Mac y Syd Barrett de Pink Floyd. 
  
 El viaje terminó abruptamente, pero en el inicio del milenio los integrantes de la banda Black Angels tuvieron una plática que traería al género de los sesenta al presente. Durante una gira imaginaron la posibilidad de tocar con sus más grandes influencias, entre las que estaban Brian Jonestown Massacre, the 13th Floor Elevators, Portishead, Silver Apples, Raveonettes, Black Rebel Motorcycle Club y Clinic. 
   
 De esta reunión fue creada la Reverberation Appreciation Society, colectivo que organizó en el 2008 el Austin Psych Festival en el Red Barn, al norte de Austin, Texas. El año pasado alcanzó la cúspide cuando 13th Floor Elevators celebró su cincuenta aniversario y elevó a los asistentes a dimensiones desconocidas.  
  
 “I don’t need these wings to guide me They are hardly ever there It’s the thing building up inside me Makes me feel light as air I’ve got levitation”. 
  
 No es coincidencia que a partir de este festival el Austin Psych Festival fuera rebautizado con el nombre Levitation y por el que ya han pasado talentos como el de Melody’s Echo Chamber, Black Mass, Destruction Unit, The Blind Suns y The Black Angels, entre otros. 
    
 Levitation en la edición de este año se llevará a cabo en el rancho Carson Creek, sitio que estará a un mundo de distancia de Austin a pesar de localizarse a unos escasos diez minutos de la capital texana. El festival ahora se desarrolla a lo largo de tres días e incluye espacios para acampar, mercados, galerías de arte, espectáculos de luces y exhibición de posters. 
   
 A lo largo de la historia, las drogas psicotrópicas han inspirado en diversas ocasiones a las mentes más brillantes de nuestra era, desde las históricas bandas que crearon al género hasta las pequeñas agrupaciones que están comenzando a influir al mundo. Levitation es una oportunidad inigualable que materializa los sueños más surrealistas a través de la música. 
 *** Te puede interesar: 
 Top ten: festivales de música en el mundo 
 Grandes discos para adentrarse en el rock psicodélico