Los 50 mejores discos de punk de todos los tiempos

 
 Llegaron a bordo del Granma a finales de 1956 y parecía que incluso Dios estaba del lado de Batista, la tormenta los hizo encallar a una gran distancia de la orilla, solamente doce sobrevivieron al accidente. El objetivo era derrumbar una dictadura que tenía a su mando a 80 mil hombres y la única ventaja era que los habían dado por muertos debido al altercado. Se sumergieron en las profundidades de Sierra Maestra y comenzaron a atacar esporádicamente bases del gobierno, la guerrilla se fue popularizando entre los habitatantes de las montañas y las noticias llegaron a oídos internacionales. 
  
 Al refugio de los revolucionarios llegó un periodista que se hacía llamar Herbert Matthews, entrevistó al líder de la revolución y publicó su material en el New York Times. La noticia circuló por todo Cuba y el mundo, grupos se alzaron en ciudades y regiones remotas para apoyar a los barbudos de Sierra Maestra. Tan sólo tres años después del accidente del Granma, la Revolución  entró triunfante a la Habana. 
 Fidel Castro y el resto de los combatientes de la Revolución cubana desde un inicio estaban convencidos de que su objetivo era acabar con la dictadura de Fulgencio Batista. Sin embargo, la forma en la que llevarían a cabo su meta en pocas ocasiones estuvo clara debido a los numerosos contratiempos y que al poder que se enfrentaban. Casi veinte años después de la entrada a la Habana, una serie de jóvenes decidieron seguir los pasos del movimiento 26 de julio, pero esta vez en la industria de la música. 
 El punk surgió como un grito que buscaba un cambio en el rock, por definición la música juvenil era un acto de rebeldía hacia la generación anterior, sin embargo, muchos jóvenes sentían que el espíritu se había desvirtuado: las letras únicamente se centraban en el consumo de sustancias –legales e ilegales–, el dinero y el sexo. Ante esta situación, el punk se rebeló en contra de toda la realidad: la industria musical era corrupta, el rock era banal y la afinación era algo de lo que se podía prescindir. 
  
 Surgieron bandas como The Clash, Sex Pistols y The Jam, que marcaron la tendencia de la rebelión. El gigante en esta ocasión no tenía a 80 mil soldados, pero tenía millones de dólares, por lo que lo único que se podía hacer era tomar fuerza de las clases que habían sido hechas a un lado: los trabajadores . Adoptaron su vestimenta y les dedicaron sus himnos, la revolución estaba en marcha y el destino era la Habana 
 1. Dead Kennedys – “Fresh Fruit of Rotting Vegetables”  Ruidosos y rápidos, pero con un sentido de dinámica y del individualismo musical, los miembros de Dead Kennedys demostraron desde su debut que no eran una agrupación convencional. Su inspiración en el surf y en el rockabilly nos entregaron una de las mejores introducciones que un bajo puede hacer en ‘Holiday in Cambodia’, mientras que en ‘California Über Alles’ es prueba de un sonido fuerte y fresco.  
 
 2. The Clash – “London Calling”  The Clash, en su primer álbum, fue una de las bandas pioneras en experimentar con reggae, en “London Calling” va un paso mucho más allá al integrar sonidos del rocabilly, ska, R&B, pop, jazz y rock. Una obra combativa por donde se le vea al integrar canciones como ‘London Calling’, ‘Spanish Bombs’ y por supuesto, ‘The Guns of Brixton’, que se convirtió en una melodía esencial para cualquier amante del punk. 
 
 3.  Minor Threat – “Out of Step” Una de las bandas más influyentes de nuestros tiempos; su canción ‘Straight Edge’ eventualmente se convertiría en la base de la subcultura del punk que defiende la abstinencia al alcohol, tabaco y drogas. Minor Threat con “Out of Step” inició una revolución dentro del punk: las adicciones se convirtieron en el enemigo a atacar en un mundo en donde envenenar el cuerpo era la moda.  
 4. Streetlight Manifiesto – “Everything Goes Numb”  Una pieza fundamental de lo que es ahora conocida como Ska punk. Ésta fue el debut del vocalista y líder de la banda de ska Catch 222. Increíble batería, rápidas secciones de guitarras y trompetas que hacen de la tercera ola de ska una prueba de que el punk nunca ha muerto. 
 5. The Clash – “The Clash” The Clash, desde sus inicios, demostró su talento musical al incorporar géneros que iban desde el reggae hasta el jazz y ska. Su álbum debut es una muestra de furia y odio mezclada con pasión por el rock y la revolución. ‘Police & Thieves’ presenta una de las primeras interacciones con el reggae, mientras que ‘White Riot’, ‘I’m So Bored with the U.S.A.’, ‘Career Opportunities’ y London’s Burning’ son muestras la crítica social y el poder de cambio que puede tener el punk. 
 6. Propagandhi – “Today’s Empires, Tomorrow’s Ashes”  Una prueba de que se pueden alcanzar sonidos fuertes y más pesados sin la necesidad de sacrificar los riffs y las melodías. Las letras del álbum mostraron una mayor madurez de la banda y la capacidad vocal que que podía alcanzar Todd. Una dura crítica hacia la adopción de la ideología del punk por la industria discográfica corporativa. 
 7. Cock Sparrer – “Shock Troops”  
 Cock Sparrer es una de las bandas street punk más importantes de toda la historia y este disco nos muestra su cumbre. “Shock Troops” aborda cuestiones como el patriotismo, el fracaso del punk y el orgullo de la clase obrera. Probablemente el mejor disco de Oi! jamás publicado. 
 8. Leftöver Crack – “Fuck World Trade” Debido al arte de su portada, fue prohibida su venta en diversas tiendas departamentales de estados unidos. “Fuck World Trade” por un lado continúa la lucha de la banda en contra del racismo, sexismo y capitalismo, mientras que continúa su espíritu anarcocomunista. Para muchos críticos es basura sin sentido, pero para otros un título central en la revolución del rock. 
 9. Sex Pistols – “Never Mind the Bollocks” 
  
 Según Noel Gallagher, exintegrante de Oasis, todo ha sido irrelevante después de  “Never Mind the Bollocks”. El primer y único álbum de Sex Pistols no sólo revolucionó al mundo de la música y del punk, también alteró a la sociedad conservadora británica y se prohibió tocarlo en la mayor parte de Inglaterra. Era claro que un grito de anarquía en el Reino Unido no iba a ser aceptado con tanta facilidad.  
 10. Stiff Little Fingers – “Inflammable Material” 
  
 El primer álbum independiente en llegar a los veinte más escuchados del Reino Unido. “Inflammable Material” abunda en la realidad del Norte de Irlanda a través de canciones relacionadas con la violencia sectaria, el aburrimiento, el abuso de la industria discográfica y la opresión policial. Un gran disco que le dio a toda una generación esperanza de una alternativa de vida. 
 11. Toy Dolls – “Absurd-Ditties” 
 12. Rancid – “Let’s Go” 
 13. Black Flag – “Damaged”  
 14. The Specials – “Specials” 
 15. Refused – “The Shape of Punk to Come” 
 16. Bad Brains – “Bad Brains” 
 17. Against Me! – “Against Me! Is Reinventing Axil Rose” 
 18.Choking Victim – “No Gods / No managers” 
 19. Ramones – “Ramones” 
 20. Dead Kennedys – “Plastic Surgery Disasters” 
 21. Gang of Four – “Entertainment” 
 22. Rancid – “… And Out Come the Wolves” 
 23. Minutemen – “Double Nickels on the Dime” 
 24. Circle Jerks – “Group Sex” 
 25. The Stooges – ‘Funhouse’ 
 26. Catch 22 – “Keasbey Nights” 
 27. Leftöver Crack – “Mediocre Generica” 
 28. The Exploited – “Punks Not Dead” 
 29. The Misfits – “Static Age” 
 30. Bad Religion – “Suffer” 
 31. Propagandhi – “How to Clean Everything” 
 32. NOFX – “Ribbed” 
 33. AFI – “Black Sails in the Sunset”   
 34.Charged G. B. H – “City Baby Attacked bt Rats” 
 35. Conflict – “The Ungovernable Force” 
 36. Bad Religion – “Against the Grain”  
 37. Bad Religion – “No control”  
 38. Ramones – “Rocket to Russia”  
 39. Dropkick Murphys – “The Gang’s All Here” 
 40. Fugazi – “13 Songs”  
 41. Anti-Flag – “Die for the Government” 
 42. Gorilla Biscuits – “Start Today” 
 43. Pennywise – “Pennywise” 
 44. The Vandals – “Hitler Bad, Vandals Good”  
 45. Iggy and The Stooges – “Raw Power” 
 46. Rise Against – “Revolutions Per Minute”  
 47. X – “Los Angeles” 
 48. Wire – “Pink Flag” 
 49. Misfits – “Walk Among Us” 
 50. Kortatu –”Kortatu” 
 – El punk, más que un género musical, es un movimiento que intentó modificar el mundo que le rodeaba desde sus inicios. La lucha no sólo se dio en Inglaterra o en Estados Unidos, sino que también existió este levantamiento en México . 
 *** Te puede interesar: 
 Fotografías que sólo un verdadero punk conoce  
 Los mejores discos de punk de todos los tiempos  
 * Referencia: 
 Rate Your Music