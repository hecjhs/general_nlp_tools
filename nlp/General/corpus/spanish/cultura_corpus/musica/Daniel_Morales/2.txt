6 mujeres que no tienen que usar escotes para tener una banda de rock

 
 Durante los conciertos de Yeah Yeah Yeahs, Karen O grita demostrando que puede dar el alma por su público, canta con la fuerza de quien ve en el rock más que una válvula de escape, para ella es un estilo de vida, una salvación y la muerte misma. Durante sus presentaciones llegan puntos en los que grita de tal manera y abre la boca de tal manera que fácilmente inserta el micrófono en su boca. ¿Éxtasis musical o rebelión feminista en contra del objeto fálico? 
 No hay duda que hay quienes han sugerido más de una vez que lo que hace Karen O es una de las cosas más sensuales en un escenario, demostrando que la rockera siempre será distinta que su contraparte masculina. 
  
 Las mujeres han sido un objeto en todos lados: televisión, literatura, política, etcétera. El arte ha tenido grandes hombres a los que se les aplaude su actitud con las prostitutas; el genio y la puta, podría decirse. En el rock la mujer siempre ha luchado por un lugar bajo los reflectores y a pesar de que estamos en 2016, el sexismo vive y muchos quieren ver a sus estrellas con poca ropa. Tal vez es porque la tradición del pop continúa con ese estigma y muchas superestrellas se valen de su cuerpo y no de su voz, pero las siguientes mujeres rockeras continúan la lucha de demostrar que son más que el físico. Sí, algunas han abrazado su lado sexual, pues no se busca que lo repriman, pero tampoco significa que se validen gracias a ello. 
 – Beth Gibbons 
  
 La vocalista de Portishead es una de las mujeres rockeras más admiradas en el mundo de la música. Siguiendo la tradición de Patty Smith o P. J. Harvey, ella demuestra que lo que publica requiere de tiempo y madurez, por eso no tiene una larga discografía, pero sí una gran carrera. El trip-hop lleva su sensual voz, pero no es amada por ser una cara bonita o tener un cuerpo escultural, sino porque en cada palabra evocada entrega todo con la pasión de una cantante y compositora, pues además de ser melódica y armoniosa, sus letras son poesía pura. 
  
 “Las mujeres tienen un camino mucho más difícil para llegar a la cima, pero tal vez esa es la razón por la que hoy las voces femeninas son las mejores en el género”. – Alison Mosshart 
  
 Pocos amantes del rock no están enamorados de la vocalista de The Kills, pero si le preguntan a alguno de los que sí, seguramente la primera excusa que darán es la actitud de Mosshart. Así como Kendrick Lamar es el sinónimo del rap en la actualidad, Alison Mosshart lo es del rock (lo siento Dave Grohl, pero el rock lo lideran las mujeres). Fotógrafa, guitarrista, pintora, modelo, actriz y líder tanto de The Kills como de The Dead Weather, la cantante de 37 años puede estar orgullosa de publicar discos a los que realmente se les puede clasificar en el género rock. 
  
 – 
 Emma Richardson 
  
 Compartiendo protagonismo con Russell Marsden, juntos dan voz a Band of Skulls, una de las mejores bandas que combina el rock y el blues en la actualidad (muy alejados de lo que fue Led Zeppelin hace mucho). Con la responsabilidad de guiar a la banda, Emma hace uso del bajo de forma espectacular y en cuatro discos, ha logrado convertir a su banda en una de las más importantes del rock a nivel mundial. En lugar de ser un objeto sexual, ella se ha incrustado en el mundo de la pintura con sus obras de gran formato que muestran una especie de test de Rorschach con colores armónicos muy fáciles de contemplar, y claro, interpretar subjetivamente. 
  
  
 – 
 Beth Ditto 
  
 La exvocalista de Gossip llevó a la banda a la grandeza gracias a su imponente voz. Con sintetizadores y guitarras a todo volumen, ella sobresalía y demostraba que el rock y el dance también podían llevarse sin caer en el cliché de música digerible y pop. Tal vez el escote sí lo usa, pero para ella no es el empoderamiento femenino de una mujer como Salma Hayek o Kate Upton, sino el de alguien que luchó toda su vida con la imagen corporal. Eso ha llevado a esta mujer a disolver su banda, buscar el estrellato como solista y también a publicar un libro, tener una columna sobre imagen corporal e incluso modelar, demostrando que todos los cuerpos pueden ser considerados dentro del mundo de la moda y siendo un ejemplo a seguir para millones de mujeres. 
  
 – Erika Wennerstrom 
  
 Con una voz grave, la vocalista de Heartless Bastards demuestra esa influencia de cantantes como Tom Waits y Tom Petty, el último, responsable de que su banda tenga ese nombre. Con orígenes en Ohio, Erika creció rodeada de una fuerte pero competitiva escena musical, por eso resulta curioso que no haya sucumbido al fuerte ambiente sexual que existe en la industria del rock y que sin tener que mostrar su cuerpo para ganar atención, se convirtiera en la líder de la banda más importante de su escena. 
  
 – Brittany Howard 
  
 El golpeteo de una batería, una simple guitarra y la desgarradora voz de Brittany Howard. Así conocimos a Alabama Shakes a principios del 2012, meses después, siendo un fenómeno mundial, ya se presentaban en el Corona Capital con un lleno total. Pocas bandas pasan de desconocidos a superestrellas en cuestión de meses, y no podemos negar que gran parte del éxito de Alabama Shakes es por su principal guitarrista y vocalista: Brittany Howard. Una de las voces más “blueseras” del momento, que continúa creciendo en potencial y talento con cada disco que publican. 
  
 – 
 Garbage, Blondie, The Pretenders y más bandas tienen a mujeres rockeras a su cargo desde hace muchos años, demostrando que el rock puede ser sexual, pero no esencial. Las mujeres tienen un camino mucho más difícil para llegar a la cima, pero tal vez esa es la razón por la que hoy las voces femeninas son las mejores en el género. Para entender más de su música, te recomendamos las mejores bandas femeninas del rock e incluso también las rockeras a las que deberías copiarles el estilo.