Frases de Frank Zappa para dejar de vivir en un mundo de idiotas

 “Todo el mundo es idiota hasta que demuestre lo contrario”. -Frank Zappa 
 Los lamentos de la sociedad muchas veces son los que el artista plasma en la obra. El hartazgo, la inconformidad; cosas con la que muchos cargan, pero que poco hacen para cambiarlo. Es el artista quien a manera de enfrentamiento a esas personas crea obras que hablan de eso, pero también las hace sabiendo que ayuda a la gente a liberarse, aunque sea momentáneamente. Frank Zappa fue uno de esos artistas, un forajido que estaba obligado a vivir en sociedad, pero que siempre despotricó contra las más rudimentarias formas de poder: la Iglesia y el Estado. Zappa despertó en los jóvenes la chispa de la lucha, de pelear por lo que creían de una forma inteligente. Él no era los que llamaba a la rebeldía por el simple hecho de llevar la contra. Él sabía quienes eran los principales culpables de que la estupidez, el egoísmo y el conservadurismo (bloques elementales que frenan el progreso) gobernaran su nación y ende, el mundo . 
  Fue un trabajo artístico el que la mayoría recuerda, y que está fuertemente entrelazado a su pensamiento individual, pero Zappa fue un personaje importante fuera de los escenarios. Siempre abogando por la educación independiente, valorando mucho más el pensamiento crítico que alguien puede aprender en la calle y las bibliotecas que en las escuelas que sólo esperan robar tu dinero. Dejó la escuela después de ver que la universidad no iba a cumplir sus expectativas y finalmente se dio por vencido con las instituciones cuando sus hijos tenían 15 años y los sacó de sus escuelas para que ellos se educaran independientemente. Acciones tan drásticas pocos las toman, y por eso te dejamos con algunas frases del músico que te harán ver la vida de otra forma y darte cuenta de que a veces nos ponemos en el papel del estúpido sin darnos cuenta. 
  “Algunos científicos argumentan que, debido a su abundancia, el hidrógeno es el bloque básico con el que el universo está construido. Estoy en desacuerdo. Pienso que hay mucha más estupidez que hidrógeno, y que por lo tanto la estupidez es el bloque básico con el que el universo está construido”. 
 “Abandona la escuela antes de que se pudra tu mente por exponerla a nuestro mediocre sistema educativo. ¡Olvídate del título y ve a una biblioteca y edúcate a ti mismo si tienes las pelotas bien puestas! Algunos de ustedes parecen robots plásticos a quienes le dicen que leer”. 
 “La religión es excelente para mantener callada a la gente común”. 
 “Cuanto más aburrido es un niño, es cuando más sus padres, al mostrar a su hijo, reciben adulaciones por ser buenos padres. Porque tienen un chico domesticado como un animalito en sus casas”. 
 “El periodismo musical consiste en gente que no sabe escribir entrevistando a gente que no sabe hablar para gente que no sabe leer”. 
  
 “Si tienes una vida aburrida y mediocre es por haber escuchado a tu mami, a tu papi, a tus profesores, a los curas o a algún tipo en la televisión diciéndote cómo hacer las cosas ¡Así que te lo mereces!” 
 “Adán y Eva estaban en el paraíso, y el cuento de hadas dice que la serpiente “mala” convenció al hombre de comer la manzana del Árbol del Conocimiento. Y el castigo fue que Eva debería sangrar y él tendría que trabajar ¿Pero qué tenía de malo querer tener conocimiento? Por ello, el cristianismo es una religión anti-intelectual. No quieren que seas inteligente. Así que sé un estúpido imbécil y tienes el cielo ganado. Eso es el cristianismo”. 
 “La sociedad paga para tener un sistema educativo de mierda, porque mientras más idiotas salgan, es más fácil venderles algo, hacerlos dóciles consumidores, o empleaduchos. Graduados con sus títulos y nada en sus cabezas, creen saben algo pero no saben nada. ¿Qué música escuchan? Mis discos seguro que no”. 
 “Sin desviarse de la norma, el progreso es imposible”. 
 “Mi consejo como padre es que, si quieres criar a un niño feliz y saludable mentalmente, aléjalo de la iglesia lo más que puedas.” 
  
 “Los niños son ingenuos, creen cualquier cosa. La escuela ya es suficiente, pero si encima lo acercas a una iglesia, lo estás metiendo en problemas”. 
 “El comunismo no funciona porque a la gente le encanta poseer porquerías”. 
 “Tengo un importante mensaje que dar a la gente “linda” de todo el mundo. Si crees que eres lindo, tal vez creas que eres hermoso. Sólo quiero decirles una cosa: hay más gente como nosotros, unos feos hijos de puta, que ustedes, así que… ¡tengan cuidado!” 
 “La ilusión de libertad continuará mientras sea rentable dicha ilusión. Cuando sea muy cara de mantener, bajarán el escenario, cerraran las cortinas y sacarán las sillas, y podrás ver el agujero en la pared del teatro”. 
 “La estupidez tiene un cierto encanto del que la ignorancia carece”. 
  
 “Nunca me ha importado que treinta millones de personas puedan pensar que estoy equivocado. El número de personas que pensaban que Hitler tenía razón no prueba que estuviese en lo cierto ¿Tengo acaso que estar necesariamente equivocado sólo porque unos pocos millones de personas piensen que no tengo la razón?” 
 “Cienciología… pones unas monedas y un tipo te resuelve unas cuantas de tus preguntas, y si pagas suficiente dinero te convierte en un maestro ¿Qué diferencia tiene con las otras religiones?” 
 “Es tan absurdo que la gente se tome las cosas tan en serio… seré honesto, a mis dieciocho años trabajé en mi actitud para no tener seriedades, ¿y qué?, estar vivo ya es demasiado extraño…” 
 “Muchas cosas erróneas de la sociedad deben ser atribuidas a que la gente que hizo las leyes tenía un mal ajuste sexual”. 
 “La política es el departamento de “espectáculos” de la industria”. 
  
 “El maquillaje no cuenta, más vale mejorar tu mente”. 
 “Yo no quiero que la gente me recuerde. Tipos como Bush quieren ser recordados, y para eso emprenden guerras. Yo no quiero ser recordado, realmente no es importante.” 
 “Todo el universo es una gran broma”. 
 “Recuerda que la información no es conocimiento. El conocimiento no es sabiduría. La sabiduría no es verdad. La verdad no es la belleza. La belleza no es el amor. El amor no es la música. La música … la música es lo mejor.” 
 Te puede interesar: 10 autodidactas, la escuela no lo es todo 
 Picasso, James Dean, Zappa y otros grandes artistas capturados en el baño