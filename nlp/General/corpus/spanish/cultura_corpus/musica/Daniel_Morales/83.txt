Frases de Manu Chao para amar clandestinamente

 Manu Chao es de esos músicos que realmente puede cambiar el rumbo de cualquier vida. Muchos pueden crecer y alejarse de los ritmos eclécticos del cantante franco español, pero al escuchar sus letras, el sentimiento de unidad con las personas menos afortunada crece, el querer cambiar al mundo parece posible y la lucha ideológica anticapitalista es sembrada exitosamente. En algunos perdura y en otros pasa desapercibida, pero siempre queda algo de ese sentimiento antisistema que él representa.  Entre reggae, ska, ritmos latinos, españoles y más, Manu Chao habla de política, de refugiados y de todo lo que hoy es tan importante como cuando el publicaba esas canciones hace 10 o 20 años. Sus temas son universales, en todos lados hay desplazados, pobres y refugiados, pero también hay amor y desamor, temas que también son parte esencial del trabajo de Manu Chao. Tal vez en México no se vea tan activo como en Europa y eso se debe a turbulentas conspiraciones pertinentes a su ultima visita a nuestro país durante el gobierno del ex presidente Felipe Calderón, pero el músico continúa dando conciertos y luchando por las causas sociales. La siguientes son algunas de las letras de sus canciones que, quien las conozca sabe que pueden hablar de distintos temas, pero que alejadas de un contexto político pueden transformarse en hermosas letras de amor y desamor. Su música es esencial en la escena global, desde la Patagonia hasta Tijuana, desde Lisboa a París o bien; desde cualquier lugar en el que su música se escucha hasta la próxima estación, esperanza . 
 “Si me das a elegir entre tú y ese cielo donde libre es el vuelo pa’ llegar al olvido… si me das a elegir me quedo contigo…” 
  “Me gusta la noche, me gustas tú”. 
 “Por tus tierras adentro yo me iré, por el camino fresco de tu amor”. 
 “Te espero siempre mi amor. Cada hora, cada día. Te espero siempre mi amor. Cada minuto que yo viva”. 
 “Tú no tienes la culpa mi amor, que el mundo sea tan feo”. 
  “A veces me gustaría morir, queda tan poca esperanza. A veces me gustaría morir para no volver a verte nunca más. A veces me gustaría morir para no saber nada más”. “Y sólo deseo estar a tu lado, soñar con tus ojos, besarte los labios, sentirme en tus brazos que soy muy feliz”. “Me llaman calle bala perdida, así me disparó la vida”.  “El nada en el mar. Ella nada en el mar. Todo nada en el mar. Como una raya. Infinita tristeza late en mi corazón”. 
 “Necesito la luna para caminar con ella por la noche. Necesito el sol para calentar mi vida. Necesito el mar para mirar muy lejos. Te necesito muy cerca de mí”. 
 “Me llaman guapa siempre a deshora”. 
 “Me llaman el desaparecido , que cuando llega ya se ha ido”. 
  “Caí en la trampa, de ser tu amigo, caí en la trampa de en ti confiar”. 
 “Cuando me buscan nunca estoy, cuando me encuentran, yo no soy”. 
 “Sé que nunca fuiste mía. Ni lo has sido ni lo eres. Pero de mi corazón, un pedacito tú tienes”. 
 Mientras esperamos que Manu Chao regrese a México, te dejamos con su concierto en el Zócalo de la Ciudad de México: 
  *** Te puede interesar: Frases de Amélie para sobrevivir al amor