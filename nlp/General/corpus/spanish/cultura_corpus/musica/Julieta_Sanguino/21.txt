Canciones que pueden sacar a cualquiera de la friend zone

 ¿Eres amigo de esa persona porque te cae bien o en realidad te gusta? A veces, es inevitable introducirnos en las oscuras aguas de la friend zone y sin querer, nos convertimos en el mejor confidente de esa persona que en realidad nos mueve todo como ninguna otra, con la que queremos iniciar una relación, amarla y darle todo. No vemos la hora para que ella o él se dé cuenta que no nos interesa su amistad ni que nos cuente de sus otras conquistas, que no queremos hacer favores sin recibir algo a cambio, que nos morimos por tocar su cuerpo y besar esos labios que nos hacen suspirar. 
 Queremos hacer todo por esa persona pero también queremos que ella lo haga por nosotros. Que sienta un poco de esa pasión que se desborda cada vez que se nos acerca. Los labios se hacen más rojos, las palpitaciones de nuestro corazón aumentan, nuestras pupilas se dilatan y estamos listos para atacar. Sin embargo, el inevitable término “amigo” se aparece en la zona de combate, nos noquea, el orgullo se derrumba y nuestros pies, sólo nos sostienen para no sufrir una humillación mayor. 
  Buscamos huir, escapar, volver a tener una oportunidad y muy valientes, damos un abrazo casual para conocer la oportunidad que tenemos y sin esperanzas, nuestro orgullo se derrumba cuando nos cuenta de su cita romántica de la que acaba de regresar. No lo soportamos pero seguimos ahí sin saber qué hacer destruidos por dentro pero con la firmeza de que algún día lo logremos. 
 Te damos una playlist que seguro te rescatará de la oscura zona y por fin le digas, de una vez por todas, que no quieres ser su amigo sino su amante. 
 “Courage” – The whitest boy alive 
 “Having said what I needed to, Having shown what I feel for you, What my intentions have been today Now it’s time for you to do the same”. 
 Dile por fin que se decida y enfrente que no estarás para siempre ahí, simplemente, pídele una respuesta. Si te quiere, que lo muestre, como dice la canción. 
  
 – 
 “Chasing pavements” – Adele “Should I give up, Or should I just keep chasing pavements? Even if it leads nowhere, Or would it be a waste? Even If I knew my place should I leave it there?”  Una canción para cuando ya no sabes qué hacer… ¿te quedas o te vas? una reflexión para tus adentros, tal vez valga la pena seguir detrás de esa persona pero ¿y si no?… 
  
 – 
 Don’t Think Twice – Bob Dylan “But I wish there was somethin’ you would do or say To try and make me change my mind and stay But we never did too much talking anyway But don’t think twice, it’s all right” . Seguramente siempre estás entre la decisión de quedarte o irte. Empezar a buscar un nuevo amor o continuar ahí, listo para recibir amor. Dile que no lo piense dos veces y por fin se decida a estar a tu lado con esta canción de Bob Dylan.  – 
 “Every Other Freckle”- Alt-J  “You’re the first and last of your kind (Pull me like an animal out of a hole!) I wanna be every lever you pull And all showers that shower you Gonna paw paw at you Like a cat paws at my woolen jumper Be your Minpin And borrower of handsome trivia” Declárale tu amor diciéndole todo lo que quieres ser para él o ella. No te conformes, dalo todo. 
  
 – 
 “Do I Wanna Know?” – Arctic Monkeys  
 “Crawling back to you Ever thought of calling when you’ve had a few? Cause I always do Maybe I’m too busy being yours to fall for somebody new Now I’ve thought it through Crawling back to you baby i’m yours breakbot”. 
 Una canción que lo dice absolutamente todo. Cuántas veces nos hemos puesto borrachos y querido llamar a esa persona que pensamos es especial y preguntarle si su corazón está abierto para ti, decirle que no puede pensar en nadie más por pensar ella aunque no sienta lo mismo. 
  
 – 
 “House of cards” – Radiohead 
 “D on’t want to be your friend I just want to be your lover No matter how it ends No matter how it starts Forget about your house of cards And I’ll do mine” 
 Es una de las canciones más románticas de Radiohead, para que no finjas ser su amigo para llegar a su corazón, dile que quieres ser su amante. 
  
 – 
 “Lover” – Devendra Banhart “You’ll never have to ask I’ll give you my sweet grass I’m gonna mesmerize your ass Just give me my first chance, it’s gonna be the last I’m gonna make you wanna stay” 
 Decirle cuánto te gusta, lo que harías por ella (él) si te da una oportunidad, podría hacer la diferencia entre la friend zone y una relación íntima.   
 – 
 “Pictures Of You” – The Cure  “ If only i’d thought of the right words  i could have held on to your heart  if only i’d thought of the right words i wouldn’t be breaking apart  all my pictures of you “. 
 ¿Esa persona especial es real o simplemente un espejismo de algo que pensamos, existe? Si tan sólo supiéramos expresarnos como lo hacemos en nuestra cabeza, tal vez todos seríamos más plenos. 
  
 – “Closer” – Nine Inch Nails “You let me violate you, you let me desecrate you You let me penetrate you, you let me complicate you Help me I broke apart my insides, help me I’ve got no soul to sell Help me the only thing that works for me, help me get away from myself” Por último, la canción más sincera de todas. Si lo que quieres es tener absolutamente todo de la otra persona, qué mejor que esta canción para decirlo sin tapujos. 
  
 – “Vapour Trail” – Ride  “You seem to come and goI never seem to know And all my timeIs yours as much as mineWe never have enoughTime to show our love”Un amor en el que todo parece maravilloso, al menos en tu cabeza, sólo puede ser dicho con esta canción, en la que, en la última estrofa, le pide mostrar su amor.***Te puede interesar:Canciones para saber que estás en la friend zone15 canciones que si te han dedicado saben que te han amado