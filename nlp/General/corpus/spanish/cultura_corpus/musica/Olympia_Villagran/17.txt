Canciones de Miles Davis que te harán sentir que la vida y el jazz son mejores cuando improvisas

 
 Sobre las calles parisinas se escuchan los cálidos susurros que entre amantes se declaman al oído; acurrucados debajo de la Torre Eiffel, se dejan llevar por las luces que estrellan una noche rojiza y aterciopelada en París. El halo de magia que rodea a la Ciudad de la Luz convierte a los enamorados en poetas, a los mensajeros en escritores y a los distraídos en artistas, quienes se dejan llevar por el encanto de una ciudad rosa, en donde es imposible esquivar las gotas de amor que caen sobre los improvisadores románticos de París. 
 Esa debe ser la razón por la que Miles Davis experimentó una encantadora necesidad de dejarse llevar durante la composición que grabó en el estudio Paris Le Post Parisien para la película “Ascenseur pour l’échafaud”. Este thriller fue la propuesta que Davis estaba esperando para dejar que las notas del jazz abordaran sorpresivamente la cinta de esta producción y a él mismo. La banda sonora que musicalizó el filme de Louis Malle trabajó a partir de escasas indicaciones por parte de Davis, quien después de explicarle a sus músicos la trama armónica de esta grabación, improvisó sin ningún tema compuesto previamente. 
 Los días que se grabó esta obra maestra, resultado de un instinto musical nato por parte del músico, no fueron los únicos en lo que Miles Davis conquistó el mundo del jazz, pues junto a Ornette Coleman, John Lewis, Gerry Mulligan y Gil Evans, entre otros músicos, rompió paradigmas que esquematizaban al jazz de manera tradicional, convirtiéndolo en un espacio mucho más abierto a las nuevas propuestas musicales del género. 
 – ‘The Maids of Cadiz’ Álbum – “Miles Ahead” 
  
 Una de las características más representativas de la música de este trompetista es la de utilizar instrumentos inusuales como la tuba y el cuerno francés en sus composiciones, además de la instrumentación típica que se utiliza desde que se sentaron las bases del jazz. 
 – ‘Solea’ Álbum – “Sketches of Spain” 
  
 A partir de la habilidad de Miles Davis para fusionar sonoridades que parecían opuestas, estableció las bases del jazz rock, corriente que mezclaba técnicas de rock y “trucos” electrónicos con el jazz, consagrándose como uno de los músicos experimentales más exitosos de la época. 
 – ‘My Man’s Gone Now’ Álbum – “Porgy and Bess” 
  
 El primer contacto que MD tuvo con su ídolo, el trompetista Dizzy Gillepse, convirtió su admiración en un deseo frenético por convertirse en un músico completo, compositor y trompetista de un jazz nuevo, que permitiera normas distintas con las que Miles podría jugar. 
 – ‘Freddie Freeloader’ Álbum – “Kind of Blue” 
  
 La influencia que llevó a que todas estas canciones proyectaran una mayor libertad, se debe a los distintos maestros que Miles tuvo durante su formación. Gil Evans, por ejemplo, le enseñó a crear música a partir de una sola escala de notas a seguir, jamás le dio un acorde compuesto, por lo que al joven aprendiz le resultaba posible modificar el desarrollo de la melodía durante su interpretación, costumbre que posteriormente se volvería parte de todo el repertorio de este ícono del jazz. 
 – ‘Black Comedy’ Álbum – “Miles in the Sky” 
  
 Los cambios en la música libre que este jazzista componía se reflejan en “Miles in the sky” y “Filles de Kilimanjaro”, álbumes del 68 que el mismo compositor describía como “coloridos”. 
 – ‘Nefertiti’ Álbum – “Nefertiti” 
  
 Uno de los temas más emblemáticos de esta estructura musical, particular de Davis, es ‘Nefertiti’, de 1968, ya que la composición rítmica que sonoriza este álbum divaga e improvisa cada nota. 
 – ‘Petits Machins’ Álbum – “Filles de Kilimanjaro” 
  
 A partir de estas interesantes fusiones, el trompetista logró ampliar el reducido público que en ese entonces admiraba el jazz. Convirtiendo e invitando a toda una generación en ser los oídos de su maravilloso experimento. 
 – ‘Maysha’ Álbum – “Agharta” 
  
 ‘Bitches Brew’ fue otra de las metamórficas canciones del jazzista que escaló las listas de éxitos de manera abrupta y la misma en abrirle las puertas a otro mundo que Davis ya conocía por encima, el rock. 
 – ‘Bitches Brew’ Álbum – “Bitches Brew”   
 Miles Davis fue la encarnación de cómo el ser humano debería vivir en la cotidianidad, evolucionando, adaptándose al cambio, aprendiendo y transformando su mundo. Pues al pasar de los años, la música y sus géneros evidentemente cambiaron, ampliando el espectro y las posibilidades, pero para Miles esto nunca fue algún obstáculo, al contrario, distante de unirse al grupo del olvido, colaboró directamente con artistas pop y de otros géneros que fueron ganando popularidad. 
 Entre 1987 y 1988, Davis aprovechó sus últimos años de vida para continuar dejándose llevar por lo que fluyó en sus venas desde el primer momento en que se acercó a la música, una impaciente libertad, un ferviente deseo y una constante necesidad por improvisar con su talento y la pasión que sentía por el jazz. 
 *** Te puede interesar:  
 El telegrama que Hendrix y Miles Davis enviaron a McCartney Canciones que combinan el alma del jazz y la fuerza del rock