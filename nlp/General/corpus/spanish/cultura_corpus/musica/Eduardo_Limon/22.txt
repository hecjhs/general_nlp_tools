Estrellas de la música que perdieron su carrera gracias al alcohol

 Sentirse totalmente apabullados por el mundo exterior es algo que muchos hemos padecido en nuestros días; percatarse que la falta de sincronía entre lo que hacen los demás y nosotros puede perfilarse hacia la destrucción es también notar que no hay quién a nuestro alrededor para salir de los pesares. Si esto es algo que a cualquiera le sucede y le nubla con su sombra , es incluso más característico de esas estrellas –artistas y personajes del espectáculo– que tienen que lidiar con la incomprensión o la exigencia de miles de personas en todo el globo. 
  En ocasiones, el éxito está marcado no sólo por el exceso de talento y los altos grados de satisfacción y experiencias que conlleva el ganarse al mundo en las pruebas de talento que implica la vida, en la carrera por ver quién es el que se aferra por más tiempo a la gloria, sino también por el abuso de todos los placeres y vicios que éste ofrece. Así es como muchas personalidades de la música, tras haberse encontrado de frente con la fama, comienzan un camino de abusos y desordenes que se perfila indiscutiblemente, insalvablemente, a la ruina. Han existido casos en que dichas exageraciones se han vuelto el sello distintivo de una personalidad en la industria musical, se han tomado como su condición permanente, pero eso no impide que, al verle entregada en tal desgracia, no cauce pena su final anunciado. 
  En el listado a continuación hacemos un pequeño recuento de esos ídolos que destruyeron su carrera y su estancia en el planeta Tierra, aunque no su legado. No la manera de transformar la realidad. – Billie Holiday 
  Con la mezcla de sustancias como el alcohol y su grave dependencia a las relaciones destructivas, la dama del jazz encaró a la muerte debido a sus problemas cardíacos y la cirrosis hepática que le produjeron sus abusos. 
 – Bon Scott 
  Quien fue el segundo vocalista de la legendaria banda AC/DC se halló muerto durante una madrugada en 1980 en el auto de uno de sus amigos por intoxicación etílica. Su largo historial de fiestas y excesos desembocó en un fatídico final que no sorprendió demasiado, pero sí causó malestar. 
 – John Bonham 
  Este mítico baterista fue hallado muerto en la casa de Jimmy Page, compañero de la banda a la que pertenecía –Led Zeppelin– en los años 70, después de haber ingerido 40 vasos de vodka en menos de 12 horas y haberse asfixiado con su propio vómito. 
 – Kurt Cobain 
  El fundador del grunge y Nirvana, tras haberse dado a conocer por sus problemas relacionados a las drogas, legales o no, se le ubicó como una persona en extremo depresiva. Conjugación mortal para un joven hombre que constantemente no veía la salida. 
 – Amy Winehouse 
  La diva contemporánea del jazz y el soul fue causante de una de las ausencias más dolorosas en nuestros tiempos; su fallecimiento tampoco fue un asalto de asombro, pero el ver cómo una mujer en extremo talentosa destruía su nombre al salir bajo el influjo del alcohol a escena, era más de lo que podíamos soportar. 
 – Elvis Presley 
  Y nos referimos obviamente al Elvis que vestía de blanco para sus shows en Las Vegas; quizá el uso de otras drogas o el consumo de cierta alimentación fue lo que realmente le llevó a la tumba, pero su constante ingesta etílica demolió en sus presentaciones a quien fue consagrado como Rey en sus tiempos de gloria. 
 – Keith Moon 
  Probablemente uno de los mejores bateristas en la historia del rock, fue Keith, exintegrante de The Who; reconocido por su abuso del alcohol y otras drogas, finalmente vio su propio desenlace al consumir demasiadas pastillas con las que supuestamente combatía sus problemas de bebida. 
 – Janis Joplin 
  Durante una época en la que la experimentación con sustancias adictivas era cosa de diario, esta cantante se caracterizaba por estar bebiendo constantemente, a toda hora. Y fue en un temprano 1970 que una fuerte ingesta de alcohol y heroína le hicieron caer para no volver jamás. 
 Así como estas historias hay demasiadas en la industria de la música, el cine y el espectáculo en general; hay incluso cantantes o miembros de alguna banda que atraviesan dicho problema en la actualidad y resulta imposible que abandonen sus hábitos ¿Cuántos han muerto ya o se perfilan a ello por los excesos que su fama implica? 
 *** Te puede interesar: 
 La indescifrable muerte de la niña que no sólo quería triunfar, sino simplemente ser amada 
 La triste historia detrás de las canciones de Amy Winehouse