Cosas que debes saber si eres chica y estás en una banda

 Ser mujer y liderar una banda o ser cantante solista implica más retos de los que se creen. No queremos volver a los viejos discursos ya gastados del feminismo en los años 70 o a argumentos contrapatriarcales que dirigen la mirada sobre esos terrenos que la mujer “debe” ganar, lo hemos dicho miles de veces. Sin embargo, mucho de ello sigue siendo válido y vigente a pesar de apariencias ya muy bien asimiladas en nuestra sociedad; no por contar con una Madonna , una Adele o cualquier otro nombre femenino en los charts internacionales podemos decir que la música está equilibrada en cuanto a género. Basta con dar un vistazo a las estadísticas para notar que las mujeres prácticamente no figuran en un mundo que, contra toda opinión generalizada, no está tan abierto al cambio como parece. 
  En ese intento por dar batalla y hacerse notar puede que existan diversas referencias femeninas, pero pocas veces se emiten, por su parte, comentarios o consejos con la fuerza que se requiere. Sobre todo porque, de una u otra forma, también se espera de su parte una disertación en los términos comunes o establecidos: el de hacer todo conforme a las delimitaciones de su género, representando el lado sensible, delicado y sutil de la música, en especial si estamos observando la escena rock de nuestra era. ¿De qué sirven entonces la voz de la mujer si va a sucumbir a un papel tradicionalista e incluso complaciente? Cierto, tampoco podemos satanizar a las cantantes o instrumentistas que han decidido andar por ese sendero; no obstante, sus actitudes enmarcan convenientemente las opiniones de esas “otras” mujeres que han explorado otras alternativas y también tienen mucho por decir. Es así como su voces se vuelven declaraciones de alcance contestatario y tintes revolucionarios. Quizá no impresionen tanto en su formulación ni sean extraordinarias en cuanto a su ejecución, pero son algo y ese “algo” es mejor que nada. Peor sería el silencio o la inactividad. 
  
 Chrissie Hynde, por ejemplo, a la cabeza de su banda The Pretenders, ha dictado una suerte de decálogo para todas aquellas mujeres que sueñan con una vida de rock en un arte acaparado por el sexo masculino o en una industria dominada por el hombre. Sus palabras, para nada desdeñables, exhortan a esas chicas a demostrar su talento, su espíritu creativo y la trascendencia de sus inspiraciones. He aquí los consejos de Hynde, los cuales fueron presentados por vez primera en la contraportada de su single ‘The Night in my Veins’: 
  – No te quejes de ser una chica, hagas referencia al feminismo o te quejes del sexismo. Todas hemos sido arrojadas y jodidas, pero nadie quiere escuchar a una fémina quejosa. En vez de ello, escribe libremente una letra al respecto y arrasa. 
  
 Nunca pretendas saber más de lo que en realidad es. Si no sabes los acordes, regresa a las notas. No vayas a un tablero a menos que planees convertirte en ingeniera de audio. 
  
 Haz que los demás miembros de la banda se vean y escuchen bien. Saca lo mejor de ellos; ése es tu trabajo. Y asegúrate de sonar también. 
  
 No insistas en trabajar con otras mujeres; eso es una estupidez. Adquiere a los mejores para el trabajo. Si acaso es una mujer, bien, tendrás a alguien para ir de compras contigo en vez de pedirle a alguien del crew que vaya contigo durante la gira. 
  
 No pienses que mostrar tus senos e intentar verte provocativa ayudará. Recuerda que estás en una banda de rock. No es un “Jódeme”, es un “¡Jódete!”. 
  
 Trata de no tener sexo con alguien de la banda. Eso siempre termina en lágrimas. 
  
 No trates de competir con los chicos, eso no impresiona a nadie. Recuerda, una de las razones por las que les gustas es porque no alimentas la competencia de egos masculinos. 
  
 Si cantas, no aúlles ni chilles. Nadie quiere escuchar esa porquería; suena histérico. 
  
 Rasura tus piernas, ¡por Dios! 
  
 No tomes consejos de alguien como yo. Haz lo tuyo siempre.  
 Chrissie Hayden puede parecer lo más antifeminista en el mundo de las mujeres que defienden su lugar en este mundo ; quizás lo sea y muy probablemente no le importe. Al menos su música está ahí para demostrar que esta fórmula le ha funcionado y su carrera se ha hecho relevante. A fin de cuentas, como ella lo misma lo dice, puedes tomar sus consejos o no. Esto se trata de hacer las cosas como uno mejor las quiera. 
 *** Te puede interesar: 
 Un ABCdario para entender a las mujeres en el rock El mundo necesita mujeres críticas de rock