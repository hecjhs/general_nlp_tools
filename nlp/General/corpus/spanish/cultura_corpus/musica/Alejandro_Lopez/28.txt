Canciones que te harán llorar para liberar el alma

 
 Hay momentos en los que se hace necesario detener el vértigo con el que avanza nuestra vida en la sociedad contemporánea y tomarse un rato para reflexionar no sólo sobre nuestras situaciones sentimentales y el peso con el que cargamos y del que tenemos que deshacernos a un nivel individual, también sobre todos aquellos hechos que generan sufrimiento y dolor  alrededor del mundo, de las enormes injusticias y desigualdades que llegamos a percibir como normales en nuestra obtusa conciencia, fundada en el absurdo racionalista de que mientras no lo veamos, significa que no ocurre, o nuestra falta de sensibilidad e individualismo que nos dictan que no tenemos porqué ser solidarios con alguna causa si no la padecemos. 
 Estas canciones funcionan como un bálsamo que ayudará a liberar el alma, a motivarnos para darnos cuenta de todas las situaciones mencionadas y a tomar acciones por hacer de nuestro entorno personal y de éste un mundo mejor. 
 – ‘Gloomy Sunday’ – Portishead  
 Esta canción levantó una gran polémica en Estados Unidos cuando se popularizó en la voz de Billy Holiday, debido a que diversos mitos urbanos la asociaban a un estado de depresión que llevaba al suicidio; sin embargo, la verdadera esencia de esta melancólica pieza está en la época en que fue compuesta, durante la Gran Depresión que azotó a nivel mundial durante el periodo de entreguerras, en la que las bases de la sociedad a nivel global se pusieron en jaque. 
 “Gloomy is sunday with shadows I spend it all My heart and I have decided to end it all”. 
 – “Polvorado” – Nacho Vegas 
  
 Lanzada en 2014 en el álbum “Resituación”, es una crítica directa al sistema capitalista que propició la crisis que repercutió con fuerza en España desde 2008, la cual hizo notoria la enorme desigualdad en la distribución del ingreso y la riqueza, el triste ambiente, los desahucios y la pérdida de patrimonio de la gente inspiraron esta obra. 
 “¿Dónde está todo aquel amor del que nos hablaron siendo niños? ¿Era otra forma de extorsión o era tan sólo un espejismo?” – ‘Bookends’ – Simon & Garfunkel  
  
 A pesar de tener sólo dos estrofas, esta canción es sencilla pero dramática; cumple con su cometido de ayudarnos a dejar esa relación atrás junto con todos los recuerdos tormentosos y empezar a pensar en el futuro. 
 “ Long ago it must be  I have a photograph  Preserve your memories  They’re all that’s left you”. 
 – ‘Love Will Tear Us Apart’ – Joy Division 
  
 El icónico tema de la banda británica liderada por Ian Curtis es casi un himno que nos recuerda nuestra fragilidad ante el amor, para recuperar la fuerza y volver a nuestra vida normal y entender que todo es un ciclo donde algunas veces estaremos en la cima y otras en el fondo. “You cry out in your sleep, All my failings exposed. And there’s a taste in my mouth, As desperation takes hold. Just that something so good just can’t function no more”. 
 – ‘Earth Song’ – Michael Jackson 
  
 Esta canción exhorta a generar consciencia en la continua devastación que las grandes empresas y la sociedad en general hemos creado en el único sitio que tenemos para vivir: el planeta Tierra. 
 “Did you ever stop to notice All the blood we’ve shed before Did you ever stop to notice The crying Earth the weeping shores?” 
 – ‘When You Gonna Learn?’ – Jamiroquai 
  
 La canción trata de reflexionar sobre la sociedad moderna y todos sus excesos y víctimas, así como de la solidaridad a nivel global que necesitamos tener para sacar adelante las situaciones difíciles. 
 “So my friend to stop the end on each other we depend Mountain high and river deep, stop it going on We gotta wake this world up from its sleep People, stop it going on”. – ‘Everybody Hurts’ – R.E.M. 
  
 Esta melodía nostálgica se hace presente en los momentos en los que todo sale mal, motivándonos a seguir adelante y no dejarnos vencer por las adversidades que se presenten en el difícil y largo camino que es la vida. 
 “Everybody hurts Take comfort in your friends Everybody hurts Don’t throw your hand, oh no”.–‘No existes’ – Soda StereoCon un sonido crudo y agresivo acompañado de una letra directa, esta pieza escrita por Cerati para el álbum “Signos” deja salir ese sentimiento de dolor y rencor que negamos al término de una relación tortuosa y necesitamos expresar para seguir adelante. “Quizás deba tomarme una revancha,aún tenemos cuentas que saldar,deslizaré mi puño por tu espalda…No existes,no existes”.–La música, al igual que otras expresiones artísticas, es una fuerza liberadora capaz de influir en nosotros de una manera positiva que ni siquiera imaginamos. Puede ser un medio que comunique mensajes de libertad, esperanza y rebeldía a aquellos que más lo necesitan, un verso capaz de generar un cambio de actitud en nosotros, una visión distinta de nuestro entorno y un respiro para el alma y el corazón.***Te puede interesar: 15 canciones que si te han dedicado sabes que te han amado 
 20 canciones en español perfectas para escuchar de noche y pensando en alguien