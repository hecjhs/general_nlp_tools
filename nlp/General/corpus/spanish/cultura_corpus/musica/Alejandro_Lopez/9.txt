10 canciones inéditas de Soda Stereo sólo para conocedores

  La banda que se formó en el mismo verano de la Guerra de las Malvinas en Punta del Este, entre músicos inquietos y la poderosa influencia de The Police que visitó la Argentina dos años antes, terminó por forjar la historia del rock latinoamericano. Cerati conoció a Zeta en la universidad, donde cada uno tenía su propia banda. Después de descubrir la similitud en sus influencias, ambos músicos buscaron a los complementos perfectos para formar su propia agrupación, incluso Andrés Calamaro participó en los teclados y voz en un prototipo de banda que practicaba en el garage de Zeta, llamada Proyecto Erekto, que poco después desapareció. Para ese entonces, Charly asediaba a la hermana de Cerati y en una de esas llamadas telefónicas, Gustavo contestó el teléfono. En la conversación diplomática, Alberti le hizo saber que tocaba la batería. Diez años después, Soda llegaba al punto más alto, donde ninguna banda en español había estado antes, copando la Avenida 9 de Julio ante más de 250,000 personas. 
 Desde sus inicios en el icónico Cabaret Marabú, hasta colaboraciones históricas con Virus o Andy Summers, el legado de Soda Stereo como el de toda banda que pasó a la historia, está repleto de presentaciones, covers y versiones inéditas que sólo son conocidas por sus seguidores más enterados. Para un genio creativo de la talla de Gustavo Cerati, fue imposible quedarse quieto y dejar de experimentar con ritmos, estilos y músicos de distintas latitudes y   realidades. La inquietud artística del ex-Soda llevó a la banda a experimentar con distintas bandas de la escena latinoamericana de entonces, además de homenajear a las grandes bandas que forjaron su inclinación musical, como The Police, The Cure y Queen. 
  Estas son diez piezas inéditas de la banda más importante de rock en América Latina: 
 – ‘Juegos de seducción (Inglés)’ – Soda Stereo 
  
 Esta extraña versión idéntica a la original pero con la letra en inglés, fue grabada por la banda como un posible intento por incursionar en el rock angloparlante. Nunca se tocó en vivo, ni siquiera se lanzó en algún álbum de estudio. 
 – ‘Imágenes paganas’ – Soda Stereo y Virus 
  
 Después de la muerte de Federico Moura en 1988, Soda se unió musicalmente a Virus durante algunos recitales  como homenaje. La agrupación resultante recibió el nombre de “Vida”.  En este extracto de 1995, Cerati interpreta ‘Imágenes paganas’ con el poder en la guitarra de Marcelo Moura. 
 – ‘Tráeme la noche (Bring On The Night)’ – Gustavo Cerati y Andy Summers 
  
 En 1998, Cerati grabó junto con el exguitarrista del trío inglés una versión, como homenaje a The Police, de la canción “Bring on the Night”, con letra traducida al español. Summers y Cerati se encerraron durante más de diez horas en un estudio de Los Ángeles y bajo una intensa lluvia, crearon esta rareza con arreglos de Gustavo. Andy elogió cada uno de los toques personales que Cerati le dio al tema. 
 – ‘Vuelta por el universo’ – Soda Stereo 
  
 Una de las canciones del disco psicodélico y experimental de Cerati con Daniel Melero fue interpretada por Soda por única vez en 1996 con arreglos más rockeros que los originales de estudio. 
 – ‘Algún Día (Someday One Day)’ – Soda Stereo 
  
 La canción escrita por Brian May fue adaptada en una versión con la letra compuesta por Cerati para el disco de homenaje “Tributo a Queen: los grandes del rock en español”, en octubre de 1997. Ésta fue la última pieza de estudio grabada por Soda antes de su separación, razón por la que fue acogida con cariño por los fanáticos. Además, fue la canción que sonó de cortina antes de que la banda saliera al escenario durante la gira “Me Verás Volver”. 
 – ‘I Want You So Bad (She’s so Heavy)’ – Soda Stereo 
  
 Una versión muy libre del tema de The Beatles fue interpretada por Soda Stereo en el histórico recital en la Avenida 9 de Julio. El tema, originalmente compuesto por la dupla Lennon/McCartney, fue interpretado por Soda con todo su sonido en una sorpresa para los más de 250 mil asistentes que se dieron cita en el concierto más multitudinario en la historia de la Argentina. 
 – ‘Cuando pase el temblor (Inglés)’ – Soda Stereo 
  
 Al igual que la versión en inglés de ‘Juegos de seducción’, este tema se grabó por única ocasión durante la producción de “Nada Personal” y nunca más salió a la luz. 
 – ‘Message In a Bottle’ – Soda Stereo & Caifanes 
  
 En un recital histórico que fue parte de la “Gira Animal”, Soda se presentó en el Palacio de los Deportes de la Ciudad de México en 1991, teniendo como invitado a Caifanes. Al momento de introducir a Saúl y compañía, ambas agrupaciones interpretaron ‘Message In A Bottle’, de The Police, del álbum “Reggatta de Blanc”. 
 – ‘Yo la vi parada ahí (I Saw Her Standing There)’ – Soda Stereo 
  
 Soda inició su carrera tocando en el mítico Cabaret Marabú, donde distintas bandas icónicas de la época se presentaron durante sus inicios. En esta ocasión, Cerati, Charly y Zeta interpretaron un tema de los Beatles en español, el audio no es el mejor, pero sin duda es un extracto que vale oro. 
 – ‘En remolinos (Versión inédita)’ – Gustavo Cerati 
  
 Una versión bestial de la original de Soda Stereo interpretada por Cerati en solitario en el Teatro Gran Rex en 2003. Es el único registro distinto de este tema, que se mezcla con la letra de ‘Fantasma’ mientras se acerca a su final. 
 – La banda que compuso himnos de toda una generación aún sigue vigente entre los más jóvenes, que ven en Gustavo Cerati la inspiración para iniciar su carrera artística en el mundo del rock. Si quieres saber más sobre el trabajo del músico, lee las canciones más enigmáticas de Cerati explicadas por él mismo . Si prefieres enterarte sobre el rock nacional, agrega a tu playlist cada una de las canciones que todo amante del rock mexicano debe conocer.