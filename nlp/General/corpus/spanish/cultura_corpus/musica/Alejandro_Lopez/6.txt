50 discos de los 80 que según NME deberíamos volver a escuchar

 
 Los maravillosos ochenta… una década llena de vértigo que marcó definitivamente el ritmo que seguiría el mundo del que ahora somos herederos: la  guerra de Vietnam llegó años antes a su fin, mientras que otra guerra, más silenciosa pero igualmente preocupante, invadía las conciencias. Uno de los principales impulsores de la paz, John Lennon, es asesinado en Nueva York. “Thriller”, de Michael Jackson, se convierte en el álbum más vendido en la historia y en la Ciudad de México, tres años después de la llegada de Miguel de la Madrid al poder, un terrible terremoto es el protagonista de uno de los días más tristes del país. 
 La primera versión de Windows sale al mercado y un año más tarde, el NES se convierte en la primera consola global. Tatcher y Reagan se convierten en los líderes de los países más poderosos del mundo y junto con ellos, el neoliberalismo se convierte en política económica. En el fabril y gris Mánchester, la banda más importante del indie  tocaba por primera vez en el famoso The Haçienda y al mismo tiempo inicia la Guerra de las Malvinas, conflicto que impactó poderosamente en la música de ambos lados del mundo. ¿Cuáles fueron los mejores trabajos de estudio de la década? Estos son 50 álbumes que suenan tan vigentes como hace más de treinta años y es obligación de cualquier melómano escucharlos para reavivar su legado en la cultura global: 
 – 50.”Warehouse: Songs And Stories” – Husker Du (1987)   – 
 – 49.”Yo! Bum Rush The Show” – Public Enemy (1987) 
 – 48. “New York” – Lou Reed (1989) 
 – 47. Meat Is Murder” – The Smiths (1985) 
 – 46.”Infected” – The The (1986) 
 – 45. “Beautiful Vision” – Van Morrison (1982) 
  
 – 44. “Like A Prayer” – Madonna (1989) 
 – 43. “Midnight Love” – Marvin Gaye (1982) 
 – 42. “You Can’t Hide Your Love Forever” – Orange Juice (1982) 
 – 41. Imperial Bedroom – Elvis Costello (1982) 
 – 40. “Green” – R.E.M. (1988) 
  
 – 39. “Heaven Up Here” – Echo And The Bunnymen (1981) 
 – 38. “Straight Out Of The Jungle” – The Jungle Brothers (1988) 
 – 37. “Sister” – Sonic Youth (1987) 
 – 36. “My Life In The Bush Of Ghosts” – David Byrne & Brian Eno (1981) 
 – 35. “Atomiser” – Big Black (1987) 
  
 – 34. “George Best” – The Wedding Present (1987) 
 – 33. “Rattlesnakes” – Lloyd Cole & The Commotions (1984) 
 – 32. “Miss America” – Mary Margaret O’Hara (1988) 
 – 31. “Talking With The Taxman About Poetry” – Billy Bragg (1986) 
 – 30. “The Nightfly” – Donald Fagen (1982) 
  
 – 29. “Nebraska” – Bruce Springsteen (1982) 
 – 28. “Crocodiles” – Echo And The Bunnymen (1980) 
 – 27. “The Eight Legged Groove Machine” – The Wonder Stuff (1988) 
 – 26. “Don’t Stand Me Down” – Dexy’s Midnight Runners (1985) 
 – 25. “Blood & Chocolate” – Elvis Costello (1986) 
  
 – 24. “The Smiths” – The Smiths (1984) 
 – 23. “Rum, Sodomy And The Lash” – The Pogues (1985) 
 – 22. “This Nation’s Saving Grace” – The Fall (1985) 
 – 21. “Rain Dogs” – Tom Waits (1985) 
 – 20. “16 Lovers Lane” – The Go-Betweens (1988) 
  
 – 19. “Parade” – Prince (1986) 
 – 18. “Dare” – The Human League (1981) 
 – 17. “Kilimanjaro” – The Teardrop Explodes (1980) 
 – 16. “Swordfishtrombones” – Tom Waits (1983) 
 – 15. “The Lexicon Of Love” – ABC (1982) 
 –  
 – 14. “Surfer Rosa” – Pixies (1988) 
 – 13. “Bummed” – Happy Mondays (1989) 
 – 12. “Searching For The Young Soul Rebels” – Dexy’s Midnight Runners (1980) 
 – 11. “Remain In Light” – Talking Heads (1980) 
 – 10. “Low-Life” – New Order (1985) 
  
 Este álbum significó la transición definitiva de Summers, Gilbert y Morris del post-punk a un ritmo más dance , marcando distancia con lo que fue Joy Division y encontrando su estilo propio. Las líricas explícitamente sexuales, la incursión de sintetizadores y los ritmos bailables, sin dejar del todo el rock, lo hicieron un éxito inmediato con calificaciones positivas de la crítica. 
 – 9. “Sound Affects” – The Jam (1980) 
  
 La curiosidad de un joven Weller lo llevó a formar The Jam, una banda donde exploró sus inquietudes musicales a profundidad. Su quinto álbum incluye sonidos basados en su ídolo, John Lennon, y una fusión de funk  con una letra aparentemente crítica, hecho que el mismo Weller desmiente, afirmando que “Sound Affects” es el mejor disco de toda su producción artística. 
 – 8. “Closer” – Joy Division (1980) 
  
 Después de que tocaran su punto más alto con la salida de “Unkown Pleasures” (1979), la banda supo reinventarse y el sonido sombrío y melancólico evolucionó musicalmente manteniendo la esencia y densidad de sus inicios. Significó el último material de Joy Division antes de la trágica muerte de Ian Curtis. 
 – 7. “Hatful Of Hollow” – The Smiths (1984) 
  
 El último trabajo de estudio de la banda indie más importante de los 80 es un recopilatorio de singles que vio la luz apenas nueves meses después de su primer lanzamiento, “The Smiths” (1984). El álbum es alegre, una narrativa al más puro estilo de la poesía de Morrissey y la energía de Marr sobre amor, pero sobre todo, desamor y relaciones fallidas. 
 – 6. “Psychocandy” – The Jesus And Mary Chain (1985) 
  
 Es difícil que una banda encuentre su sonido característico en su primera producción; sin embargo, la liderada por los hermanos Reid pasó súbitamente de los pequeños escenarios a la notoriedad internacional con “Psychocandy”, mostrando que eran mucho más que un híbrido de The Beach Boys con The Velvet Underground, como comúnmente se describió su música. 
 – 5. “It Takes A Nation Of Millions To Hold Us Back” – Public Enemy (1988) 
  
 El segundo álbum de estudio de Public Enemy es el punto de partida forzoso de cualquier intento por hacer  hip-hop en la actualidad. La placa más emblemática del género, que se cuela entre los grandes del rock en esta y otras listas por la potencia de su lírica, todas escritas por Chuck D, que retratan la situación racial y de opresión hacia los afroamericanos, además de la pobreza y brutalidad policial que aún hoy sigue vigente en los Estados Unidos. 
 – 4. “Sign O’ The Times” – Prince (1987) 
  
 El disco más aventurado de Prince es posiblemente el más valioso de su enorme genio musical. The Purple One se encargó de cada uno de los detalles de este álbum y decidió arriesgarse mezclando sonidos funk, soul, pop, incursiones electrónicas y canciones minimalistas. El resultado fue excepcional, inmediatamente aclamado por la crítica mundial y silenció a todos aquellos que creían que después de su gloriosa carrera, el de Minneapolis no tenía mucho más que contar. 
 – 3. “3 Feet High And Rising” – De La Soul (1989) 
  Una revelación total en el hip hop. Si Public Enemy mostró el mensaje que debía seguir este género, De La Soul volvió con toda la fuerza al hip hop bailable y lleno de energía, sin hacer de lado la realidad y las vivencias que marcan su naturaleza. 
 – 2. “The Queen Is Dead” – The Smiths (1985) 
  
 No tiene discusión partirse en elogios ante uno de los discos más pensados de la historia. Todo el espíritu de The Smiths: la música artística contra la música basura, la poesía, la introspección personal, el egoísmo y al mismo tiempo, la ingravidez de no ser más que una mota de polvo en el tiempo y el espacio. Es el emblema de toda una generación, la filosofía de vida que se refleja en la literatura hecha música. 
 – 1.”The Stone Roses” – The Stone Roses (1989) 
  
 Detrás del halo que dejó Morrissey en toda una década de hacer música, The Stone Roses emergieron en la escena britpop como un indicativo de que los ochenta habían llegado a su fin. A pesar de que estuvieron desde el lanzamiento de su primer sencillo ‘So Young/Tell Me’ en 1985 como incógnitos, la banda no dejó de lanzar singles que influyeron poderosamente en artistas venideros de la talla de los Gallagher, que en distintas ocasiones se han referido a ellos como una de sus bandas favoritas. 
 ¿Qué es el indie ? Diversos críticos han tratado de explicarlo y no es tan sencillo definirlo en una opinión consensuada. Lee la guía básica para todo el que intente conocer qué es la música indie si quieres informarte del tema. La mitad del año está a la vuelta de la esquina, es momento de escuchar  los mejores discos del primer semestre del 2016 , toma nota de ellos y júzgalos por ti mismo.