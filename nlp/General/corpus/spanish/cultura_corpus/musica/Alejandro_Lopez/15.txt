17 canciones para tirarte en la cama y perder la mirada en el techo de tu cuarto

 Para Kurt Cobain, la música dañaba a su cuerpo de dos formas: le causaba una profunda gastritis que él creía psicosomática por toda la rabia y el coraje que imprimía en cada presentación. El vocalista de Nirvana también sufrió escoliosis, una desviación de la columna que se acrecentaba con el peso de su guitarra. Cobain afirmaba estar siempre adolorido, tal vez en ambos sentidos, tanto física como mentalmente; sin embargo, siempre creyó que ese dolor era el motor de sus letras y del estilo oscuro de la banda de Seattle. De forma opuesta, Bob Marley afirmó que “una cosa positiva de la música es que cuando te pega, no sientes dolor”. Para él, la música era una forma de liberación que asumió hasta sus últimos días. A pesar de las formas tan distintas de concebir y materializar su esencia en canciones, tanto Kurt Cobain como Bob Marley experimentaron el poder de la música en sus vidas. Así como ellos, cualquiera puede dejarse llevar por el ritmo y dejar que la mente viaje hasta donde los riffs lo permitan. Estas son 17 canciones para relajarte, tirarte en la cama y dejar que tu mirada se pierda en el techo de tu cuarto, para relajarte y olvidar, al menos por un instante, todo lo demás: 
 “See The Sun” – The Kooks 
  
 “You can have love and lots of other things To make you think But when it all comes down to you girl Yeah no matter what you do…”. 
 – “Flightless Bird, American Mouth” – Iron & Wine 
  
 “Have I found you?Flightless bird, grounded, bleedingOr lost you?American mouthBig pill stuck going down”. 
 – “You Could Be Happy” – Snow Patrol 
  
 “Most of what I remember makes me sure I should have stopped you from walking out the door You could be happy, I hope you are You made me happier than I’d been by far”. – “Detlef Schrempf” – Band Of Horses 
  
 “So take it as a song or a lesson to learn And sometime soon be better than you were If you say you’re gonna go, then be careful And watch how you treat every living soul”. – “I Will Follow You Into The Dark” – Death Cab For Cutie 
  
 “Love of mine, some day you will die But I’ll be close behind I’ll follow you into the dark No blinding light or tunnels to gates of white Just our hands clasped so tight Waiting for the hint of a spark”. – “Love Affair” – Copeland 
  
 “Was your kiss too weak? Were your eyes too tired? And much too young to be in love Much too young to be in love “. 
 – “Casimir Pulaski Day” – Sufjan Stevens 
  
 “In the morning, through the window shade When the light pressed up against your shoulder blade I could see what you were reading”. 
 – “No One’s Gonna Love You”  –   Band of Horses “And anything to make you smile It is my better side of you to admire But they should never take so long Just to be over then back to another one”. – “Hot Gates” – Mumford & Sons 
  
 “But even in the dark I saw you were the only one alone At these hot gates you spit your vitriol Though you swore you wouldn’t do this anymore And I can’t be for you all of the things you want me to…”. – “Minnesota, WI” – Bon Iver 
  
 “Armour let it through, borne the arboretic truth you kept posing Sat down in the suit, fixed on up it wasn’t you by finished closing “. 
 – “Mad Sounds” – Arctic Monkeys 
  
 “Mad sounds in your ears make you get up and dance Make you get up All night long reappear Make you get up and dance”. – “Peng Pong” – Erlend Øye 
  
 “I think about you day and night girl I think about you night and day I think about you all the time girl”. – “Going to California” – Led Zeppelin 
  
 “Seems that the wrath of the Gods  Got a punch on the nose and it started to flow;  I think I might be sinking.  Throw me a line if I reach it in time  I’ll meet you up there where the path  Runs straight and high”. 
 – “Chained” – The XX 
  
 “Separate or combine I ask you one last time Did I hold you too tight? Did I not let enough light in?”. 
 – <!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
<!--
td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}
-->--> "I'm Yours" - Jason Mraz 
  
 "Well, open up your mind and see like me, Open up your plans and damn you're free. Look into your heart and you'll find love, love, love, love". - "Homeward Bound" - Simon & Garfunkel 
  
 "Homeward bound I wish I was homeward bound Home where my thought's escaping Home where my music's playing Home where my love lies waiting silently for me". - "The Other Side of Mt. Heart Attack" - Liars 
  
 "I can always be found I will stay by your side And I want you to find me So, I'll stay by your side". - La música, más que transmitir sentimientos, es un estado mental, sólo hace falta dejarse llevar. Si necesitas una playlist para levantarte de la mejor manera, toma nota de las canciones que  las personas exitosas escuchan por la mañana para tener un buen día . Además, el equilibro entre cuerpo y mente es fundamental para sentirte pleno y al máximo, aquí las 15 lecciones de Buda para crecer espiritualmente y vivir mejor.