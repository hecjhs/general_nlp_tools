Canciones de grandes bandas que sólo un verdadero fan conoce

 
 Cuando empezaron a hacerse las primeras grabaciones de música, había un limitante por el tiempo que podían contener los discos de vinilo. Todo dependía del tamaño del disco, que podía ser de 7 o 10 pulgadas. Para ahorrarse material y dinero, los álbumes eran grabados por ambos lados, así se tenía un lado A y un lado B. La cara principal contenía la canción principal, el tema que el manager elegía para transmitirse en la radio y que con el tiempo se convertiría en un éxito. 
  
 Conoce  los 10 B-sides de Depeche Mode. 
 Generalmente el lado B albergaba la canción secundaria del sencillo. Con el tiempo, el B-side se fue perfilando como el material inédito que reforzaba la venta del disco. En la actualidad los formatos de música han evolucionado y se ocupa muy poco el disco de vinilo, pero se quedó la designación del lado B para los temas sorpresas, raros o inéditos que no vienen dentro del álbum. 
 Las siguientes canciones son B-sides de discos o rarezas que sólo un verdadero fan conoce. También hay un par de canciones sueltas que no pertenecen a ningún material discográfico pues sólo fue subidas a la red como un demo o maqueta. Ponte a prueba y descubre si eres un fiel seguidor de tu grupo favorito conociendo sus temas inéditos. 
 – 
 ‘The Serpentine’ – Tame Impala 
 Decir una fecha en la que comenzó  Tame Impala sería impreciso. Su primer EP salió en 2008 aunque Kevin Parker ya llevaba tiempo componiendo. De esa etapa perdida en el tiempo se desprende el tema ‘The Serpentine’, una potente canción que recuerda bastante a Iron Butterfly por su suciedad y la baja calidad de sonido intencional. Hoy la canción se puede considerar como rareza. 
  
 – 
 ‘The Amazing Sounds of Orgy’ – Radiohead 
 Radiohead es uno de los grupos que no paran de componer. Las canciones que presentan en sus discos son sólo algunas piezas del gran número de melodías que surgen dentro de su estudio. Por eso la banda cuenta con Box Sets especiales y singles con diversos B-Sides. Del sencillo ‘Pyramid Song’ se desprende ‘The Amazing Sounds of Orgye’, un tema oscuro con una fuerte carga ideológica. 
  
 – 
 ‘Brazil’ – Arcade Fire 
 Del álbum “Funeral” se desprenden diversos lados-B que son recogidos en el disco “B-Sides & Rarities”. La canción ‘Brazil’ está suelta por las redes y es popular entre los fans que llevan escuchando toda la vida Arcade Fire. Algunos dicen que es un cover del compositor brasileño Francisco Alves, escrita en 1930, y están descontentos por la ausencia de créditos por parte de la banda canadiense. 
  
 – 
 ‘Peter Panic’  – Blur 
 Dentro del sencillo ‘Girls & Boys’ se encuentra la canción ‘Peter Panic’, que se dice es un tema inspirado por David Bowie y sus viajes por el espacio. La primera línea dice “Hello Peter Panic, you’ve landed on our planet”, ¿será cierta la inspiración? 
  
 – 
 ‘Street Lites’ – Pulp 
 En 1994 se estrenó el disco “His N Hers”, el primer gran salto del Pulp. Este material contiene la canción ‘Do You Remember The First Time?’ que cuando se estrenó en single venía acompañado por el B-Side ‘Street Lites’. Esta canción sólo era conocida por los fans que compraron el sencillo. Hoy está disponible en la versión especial del “His N Hers”. 
  
 – 
 ‘Back To The Old House’ – The Smiths 
 Otra banda que tiene un montón de rarezas perdidas en el tiempo es The Smiths. Ellos recopilaron B-sides de sus canciones durante muchas presentaciones en vivo. Del tema ‘What Difference Does It Make?’ surge el lado B: ‘Back To The Old House’. 
  
 – 
 ‘Dangerous’ – Depeche Mode 
 En 1990, Depeche Mode lanzó el disco más importante de su trayectoria, según los críticos de música. En ese material estás las míticas canciones ‘Enjoy The Silence’ y ‘Personal Jesus’. Los verdaderos fans saben que ‘Dangerous’ es un B-side que compite de frente con aquellas increíbles canciones. 
  
 – 
 ‘I Pray, Olé’ – David Bowie 
 De sus largos paseos por Alemania, Bowie se inspiró para componer diversos discos, entre ellos “Lodger” (1979). De ese industrial material surge el tema ‘I Pray, Olé’, una grabación frío con Brian Eno en los sintetizadores. Es una canción experimental con un toque muy marcado de Roxy Music. 
  
 – 
 ‘Plastic Passion’ – The Cure 
 The Cure tomó fuerza en la competitiva escena de la música en Inglaterra con la canción ‘Boys Don’t Cry’, de ahí se desprende el B-side ‘Plastic Passion’. Cuando en Estados Unidos fue relanzado el disco “Three Imaginary Boys” se incluyó dicha canción en el lado A del disco. Hoy sólo los verdaderos fans saben la diferencia y las ubican. 
  
 – ¿Conociste el B -side de tu artista favorito? 
 Actualmente, la digitalización del disco está reuniendo los B-sides y para incluirlos dentro de los álbumes. Este gesto es aplaudido por muchos, pero los verdaderos fans siguen comprando los sencillos de las canciones, venerándolos como una joya. Ya que estás en tu fase de melómano, conoce las mejores canciones de toda la historia y los 100 mejores discos de todos los tiempos según Consequence of Sound.