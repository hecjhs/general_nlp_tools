10 canciones que demuestran que el jazz no sólo es cosa de hombres

 
 El jazz es juguetón. Tratar de definirlo sería como atrapar a un pájaro que vuela de manera errática por los cielos. Desde sus inicios, el género se caracterizó por su ritmo sincopado, desordenado o volátil. Su principal característica estaba en la improvisación, donde se dejaban en libertad las emociones que aterrizaban en una pieza musical. Sus propiedades, aunque pocos lo reflexionen, encuadran perfectamente con el espíritu indomable de la mujer. Un alma que pasa de la tranquilidad al ardor de la vida en un sólo segundo. 
  
 Antes del jazz era el blues, y como todo en la vida, era acaparado por la figura masculina. Sin embargo, durante los años 20, con el gran invento del disco, las mujeres empezaron a grabar en el estudio como coristas de grandes músicos, especialmente del género  azul.  En la década siguiente, los hombres dejaron que su opuesto empezara a desarrollarse en el piano para tener presentaciones en clubes nocturno. Con el estallido de la Segunda Guerra Mundial, los varones dejaron sus ciudades y se fueron a los campos de batalla. Fue en ese entonces cuando el espíritu femenino se liberó en los escenarios. Por desgracia duró poco su libre revoloteó, porque una vez que terminó la guerra, las bandas de jazz regresaron a las manos de ellos, dejándolas como pianistas o coristas. 
 Poco a poco las mujeres fueron ganándose su lugar, pero era inevitable que el control siguiera bajo la figura masculina. Fue en 1959 cuando se estrenó la película “Some Like It Hot”, donde se mostró a una banda de jazz compuesta totalmente por mujeres, liderada por una joven y bella Marilyn Monroe. Esto provocó revuelo entre las mujeres y un poco de recelo en los hombres, pero sirvió para que se desatara una ola de intérpretes femeninas que por fin estaban al mando de su propio conjunto. 
   
 Fue un camino difícil el que continuó, pero valió cada esfuerzo para lograr una equidad en el mundo del jazz y otros géneros posteriores. Por eso es importantes recordar a las mujeres del jazz que demostraron que la música no sólo es de los hombres. 
 – ‘Feeling Good’ – Nina Simone 
 Nina Simone fue una mujer que estuvo al filo de la demencia por el sufrimiento que le provocaba la vida. Por el color de su piel y la época en la que vivió, se enfrentó a un mundo que la quería destruir. Su final fue trágico, pero mientras tuvo la fuerza para estar de pie, cantó con toda el alma. 
  
 – ‘Mi amor’ – Stacey Kent 
 La intérprete de jazz Stacey Kent nació en Estados Unidos, pero nunca se conformó con conocer sólo una pequeña parte del mundo. Su gusto por la música la llevó a recorrer Francia, Londres, España y muchos otros países donde el jazz está en constante cambio. De todos esos lugares aprendió algo y lo comparte en su música elegante y reconfortante. 
  
 – ‘The Beat Goes On’ – Buddy and Cathy Rich 
 Buddy Rich fue uno de los bateristas más importantes en el mundo del jazz. Todos lo querían para que tocara en su banda, pero él sólo tenía ojos para su hija. Cuando Cathy creció, empapada de los gustos de su padre, colaboró con él y creó piezas increíbles. De esos momentos familiares se desprende el tema “The Beat Goes On”, que sin duda te hará bailar en medio de tu oficina o escuela. 
  
 – ‘Just Because You Can’ – Catherine Russell 
 Catherine es otra muestra de la herencia familiar. Su padre, Luis Russell, fue mucho tiempo el director musical del maestro Louis Armstrong y era obvio que no iba a dejar que su hija no conociera el placer que provoca el jazz. La carrera de Catherine abarca muchos éxitos y anécdotas. Quizá el mundo la identifica porque colaboró con David Bowie tocando la guitarra y haciendo coros para la gira del disco “Reality”. 
  
 – ‘They Can’t Take Away From Me’ – Ella Fitzgerald & Louis Armstrong 
 El jazz no hubiera sido el mismo sin la figura de Ella Fitzgerald. Junto con Bille Holiday y Sara Vaughan, está considerada como la cantante más importantes del género. Ganó 13 premios Grammy y fue galardonada con la Medalla Nacional de las Artes y la Medalla Presidencial de la Libertad de Estados Unidos. Así de importante fue. Sin más, deléitate con este increíble tema que interpreta en compañía de Louis Armstrong. 
  
 – ‘Let’s Fall In Love’ – Diana Krall 
 Quizá Diana Krall sea una de las figuras más populares del jazz actualmente. Mucho de su trabajo consiste en interpretar piezas clásicas del género, imprimiéndole un toque personal de sensualidad. Su fama mundial muchas veces la hace acreedora a comentarios negativos sustentados en la falta de originalidad. A pesar de todo, tiene canciones que son dignas de elogiar, dejando fuera todo mal pensamiento. 
  
 – ‘Fine and Mellow’ –  Billie Holiday 
 Billie Holiday era una mujer delicada y pequeña, pero arriba de los escenarios se transformaba en una imponente figura. La crítica musical la calificó como “inigualable y posiblemente la mejor cantante del siglo”. Frank Sinatra, por su parte, la consideraba “su mayor influencia”. Así de trascendente es Billie para la música. Para comprobarlo no hay nada mejor que escuchar su música. 
  
 – ‘Kat’s Song’ – Laura Ellis Sacada de los clubes nocturnos de Los Angeles, la carrera musical de Laura Ellis es muy corta, pero de mucha calidad. La cantante no es muy conocida, pero su sensual voz hace que valga la pena escucharla. Si le das una oportunidad, no te defraudará. 
  
 – ‘Fever’ – Peggy Lee 
 Ella es otra figura que alcanzó el éxito en la década de los 40. Su música ha influido a artistas como Paul McCartney, Mette Midler, Madonna, Elvis Costello o Dusty Springfield. El mismo Louis Armstrong dijo que era una de sus cantantes favoritas. Escucha uno de sus clásicos con el que demostró que el jazz también es para mujeres. 
  
 – ‘Summertime (Studio Rio Version)’ – Sarah Vaughan 
 Para terminar la tríada sagrada del jazz femenino, Sara Vaughan llegó con una voz grave y un vibrato incomparable para tomar el trono. Sus capacidades más destacadas son su inventiva armónica y su alto sentido de la  improvisación. Si quieres conocer la historia del jazz, tienes que escuchar a Sara Vaughan. 
  
 – Si te gustaron las canciones pero aún sientes que te falta conocer más del género, existen una películas que puedes ver y que revelan los secretos del jazz. 
  
 *** Te puede interesar: 
 10 piezas de jazz para empezar a convertirte en un experto 15 canciones de jazz para las almas melancólicas que viven de noche