Secretos que revela tu banda favorita sobre tu vida sexual

 
 Dime qué escuchas y te diré quién eres, o lo que es mejor, qué secretos guardas en la cama. Si aún no lo habías notado, la música es la mejor forma de conocer a las personas porque es el reflejo directo de lo que guardan por dentro. Por ejemplo, cuando vas por la calle y alguien está cantando una canción de The Doors , lo primero que piensas es: “seguro tiene un estilo clásico y salvaje”. En cambio, ¿qué pasaría si te encuentras con alguien que va coreando algún tema de Maná? Mejor ni explicarlo. 
  
 Por eso, cuando conozcas a una nueva persona las preguntas obligadas son: ¿Cuántos años tienes? ¿A qué te dedicas?, y sobre todo, ¿cuál es tu banda favorita? Con estas tres sencillas interrogantes te ahorrarás horas de pláticas y momentos incómodos que se pueden presentar en un futuro. Porque hay que aceptarlo, la gente no siempre dice ser quien en realidad es. 
 Si logras encontrar a alguien que tenga gustos similares o interesantes, el siguiente paso es descifrar lo que dice entre líneas en sus canciones favoritas. Por supuesto, la mejor manera de conocer a los demás es conociéndote a ti mismo primero, por eso, ahora conocerás la relación entre tus bandas favoritas y tus hábitos sexuales, que seguramente no te habías puesto a pensar. 
 – Florence + the Machine 
  
 Debajo de ese atuendo de colores, se esconde una personalidad fuerte y con mucho carácter, el problema es que después del “Shake It Out” todo se vuelve muy tranquilo y sin oportunidad de remontar 
 – The Beatles 
  
 Te adaptas a cualquier ritmo en la cama. Comienzas de manera muy romántica, pero a la primera oportunidad sacas la bestia que llevas dentro. Te dicen el mil brazos, por eso del “Octopus’s Garden”, así que cuidado. 
 – Radiohead 
  
 Como tú no hay dos. Seduces, excitas y haces gritar o llorar de la fuerte emoción que provocas. Últimamente has estado experimentado nuevos juegos y algunas personas te reclaman tus clásicos pero efectivos movimientos. 
 – Kraftwerk 
  
 Eres eficiente. Sabes tocar los botones indicados para que empiece la fiesta, pero cuidado, también puedes caer en un ritmo automatizado que en el futuro dejará de sorprender. 
 – Metallica 
  
 Qué se puede decir. Todo es muy fuerte, pero muy rápido. En la cama se necesita más que sólo fuerza. 
 – M.I.A.  Te gusta hacer travesuras en la cama. Tus movimientos excitan hasta a la persona más incrédula, que una vez entrada en el acto, se mueve junto a ti con un ritmo que daría un infarto. 
 – The Libertines 
  
 Para ti, sexo es igual a exceso. En el momento de actuar desbordas toda la pasión que tienes y entregas tu alma en un sólo acto. Ten cuidado, si sigues así, te acabarás muy pronto. 
 – Grimes 
  
 Tienes muchos fetiches ocultos. Un día te gusta el sexo tranquilo, muy normal, y al siguiente sacas tu lado salvaje con una ligereza cualquiera podría pensar que hacer el amor contigo es como flotar. 
 – Sigur Ros 
  
 Eres un misterio que enciende el alma. Nadie entiende lo que haces en la cama, tu lenguaje es como de otro planeta, pero quien te prueba no puede soltarte. 
 – Morrissey 
  
 Pasaste de moda. Tus años gloriosos ya ocurrieron y ahora sólo quedan tus recuerdos. De vez en cuando tienes momentos de esplendor, pero son opacados por el cansancio de tantos años de goce desbordado. 
 – The Ramones 
  
 Repites todo una y otra vez. Tienes un ritmo básico todo el tiempo. Algunas veces enciendes y pones todo el ambiente, pero con el tiempo se torna aburrido. Claro, habrá muchas personas que no se podrán desenganchar de ti. 
 – Pink Floyd 
  
 Haces explotar la cabeza de los demás. Eres toda una experiencia incomparable y puedes extenderte tanto como tu cuerpo lo pida, transportando a la otra persona a lugares desconocidos. 
 – David Bowie 
  
 Conoces todos los secretos sexuales. Adoptas cualquier forma sobre la cama. Eres una animal, un rebelde y un futurista. Cada acto es una experiencia nueva y siempre estás buscando nuevas formas de expresarte en la oscuridad. 
 – Gustavo Cerati 
  
 Inventas nuevas posturas con el tiempo. Empezaste bien y los años terminaron de perfeccionar tus artes amatorias. Exploraste todos los géneros sobre la cama y los refinaste, pero como todo, llegaste a tu final. Ahora eres un recuerdo de lo que fue. 
 – Sonic Youth 
  
 El sexo es como un juego para ti. Mantienes la energía de un adolescente. No tienes límites. Haces lo que quieres, de la manera que quieres. Eso sí, haces un escándalo cuando entras en acción. 
 – Massive Attack 
  
 Eres pasivo-agresivo en la cama. Puedes pasar de un plano totalmente romántico a un espacio experimental y desconocido a los ojos de los demás. Nadie te supera, eres una leyenda y seguirás siéndolo. 
 – Tame Impala 
  
 Te propusiste revivir los fetiches de los viejos tiempos y lo conseguiste, pero no sólo eso, ahora les imprimes una furia que nadie puede alcanzar. Quien te prueba no te suelta, sólo ten cuidado de no perder el camino y caer en lo que todos hacen. 
 – The XX 
  
 Tu estilo en la cama es muy tranquilo, pero tienes algo inexplicable que hace llorar de placer a las personas más duras. 
 – Depeche Mode 
  
 Eres un espectáculo sexual. El movimiento de tus caderas lleva un ritmo imparable que desarma a cualquiera. Robas suspiros sin quitarte la ropa y cuando lo haces, dejas sin palabras a las personas. Eres un sueño del pasado que aún tiene mucho tiempo por existir. 
 – The Strokes 
  
 Eres como un infante a la hora de la acción. Aún te falta mucho camino por recorrer, pero tu habilidad en estos días deja satisfecha a cualquier persona. Digamos que en un principio quien te probó pensó que llegarías muy lejos, ahora estás en una etapa estancada, pero cuando encuentres el punto exacto para brindar un placer desbordante, todos estarán a tus pies. 
 – La relación entre la música y la persona es muy misteriosa, descubre la canción que influyó en tu nacimiento . 
 – 
 *** Te puede interesar: 
 Canciones para dedicar que dicen justo lo que sientes 
 9 canciones para entender la complejidad, intensidad y caos de la mujer