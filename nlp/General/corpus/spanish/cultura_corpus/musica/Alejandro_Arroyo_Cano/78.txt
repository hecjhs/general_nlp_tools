10 canciones que amarás si te gusta la nueva música electrónica

 
 Si llegaste hasta aquí es porque te encantan todos los sonidos que surgen de la combinación de la tecnología y la creatividad humana. También es probable que cuentes con mucha información acerca de la música electrónica que se escucha en tiempos recientes, pero para comprender por completo el presente, hay que conocer un poco del pasado. 
 Contar más de  100 años de historia sería tedioso ahora, pero si escuchas uno de los discos más representativos de este género, todo será más sencillo y tu oído identificará las principales características sonoras que actualmente se siguen repitiendo. El disco que se propone a estudiar es el de “Authobahn”, del grupo alemán Kraftwerk. 
  
 El disco parece un simple montaje de sonidos que carecen de ritmo. Pero debes tener en cuenta que en 1974, fecha en que se lanzó el álbum, la concepción de música electrónica era muy diferente a la de hoy. Antes la función de este tipo de música era la de experimentar, mientras que actualmente el género se relaciona con el movimiento. 
 Antes de “Authobahn” no existía otro disco que fuera enfocado exclusivamente a un género electrónico, sólo había pequeñas maquetas o grabaciones de ruidos aislados. Unir el rompecabezas fue como haber descubierto otro mundo y por sencillo que se escuche, fue un hecho que cambió el curso del mundo. 
  
 La genialidad de Kraftwerk fue que construyó una base sonora con lo que salía de su computadora, es decir, una pequeña grabación de cinco segundos era desfragmentada y reordenada para darle un sentido lógico. Podía grabar cualquier sonido del entorno y después era transportada a un mundo digital, donde iniciaba la magia. 
 Por eso a la  música electrónica no sólo le basta las teoría clásica de armonía, porque se necesita saber sobre secuencias y estructuras para darle ritmo a cualquier sonido. Así llegamos al día de hoy, donde la música deja de basarse en instrumentos para construir de la nada una pista entera de manera digital. 
 – ‘Families’ – John Talabot 
  
 Este DJ y productor de Barcelona, presenta una suave, pero rítmica, estructura que es acompañada por la suave voz de Glasser. Su truco está en jugar con las escalas sonoras, que combinando con un marcado bombo, hará que flotes con su sonido envolvente. 
 – ‘Losing My Patience’ – Shit Robot 
  
 Marcus Lambkin, quien da vida a Shit Robot, trabajó muy de cerca con LCD Soundsystem, adoptando su estilo elegante y sutil. Este tema es bastante relajado, pero funciona muy bien al inicio de una larga noche de baile. 
 – ‘Viol’ – Gesaffelstein 
 Mike Lévy, mejor conocido como Gesaffelstein, tiene un estilo muy peculiar que se basa en sonidos muy robustos y oscuros. Este tema da prueba de ello. Se ha presentado un par de ocasiones en el país y tiene cada vez más seguidores. 
 – ‘Closing Shot’ – Lindstrom 
  
 Este productor noruego ha experimentado hasta el cansancio con los sonidos digitales. Es muy popular en Europa con remixes de Doves, Roxy Music y Allez Allez, también su producción propia es tan extensa como necesaria de escuchar. 
 – ‘Dumb Disco Ideas’ – Holy Ghost! 
  
 Desde Nueva York, esta banda se basa en sintetizadores y secuenciadores para construir prácticamente toda su discografía. Su música tiene mucho movimiento y es ideal para cuando tienes ganas de mover el cuerpo. 
 – ‘Flott Flyt’ – Diskjokke 
  
 Forjado en el mundo clásico de los DJs nocturnos, Joachim Dyrdahl es un productor noruego que sólo necesita una secuencia sólida que se ensamble perfectamente con un sintetizador para hacer que el ritmo inicie. 
 – ‘Eurodans’ – Todd Terje 
  
 Este DJ noruego alcanzó una popularidad mundial con su último disco, “It’s Album Time”, aunque todo su material pasado es igual de bueno. A Todd le gusta usar sintetizadores con sonido vintage que le brindan una armonía suave a la mayoría de sus temas. 
 – ‘Take a Chance’ – Moullinex (Satin Jackets Remix) 
  
 Este proyecto de origen portugués es encabezado por Luis Clara Gomes, quien también se presenta como DJ. La banda se ha presentado en repetidas ocasiones en el país, cada una de ellas ha sido una buena experiencia gracias a su excelente show en vivo. 
 – “We Share Our Mother’s Health’ (Trentemoller Remix) – The Knife 
  
 El proyecto estaba conformado por un dúo de músicos suecos, aunque para las presentaciones en vivo, recurrían a más gente para aumentar la calidad de los shows. Alcanzaron el éxito con el tema ‘Hearbeats’ y lograron consolidarse como una de las más atractivas bandas de la escena independiente en Europa. 
 – ‘Military Fashion Show’ – And One 
  
 Este músico y productor alemán reúne los sonidos clásicos del synthpop y del Electronic Body Music para crear un paisaje industrial y mecánico que hace vibrar a cualquier lugar en el que suene. Su música refleja lo que se vive durante las noches en  Alemania. 
 – 
 Si estas canciones te levantaron el ánimo, quizá necesites complementar la lista con más beats . 
 *** 
 Te puede interesar: 
 10 genios de la música eléctronica 
 Bandas de rock en español que debes escuchar para salir de la rutina