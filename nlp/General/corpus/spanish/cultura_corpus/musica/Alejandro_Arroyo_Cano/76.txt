13 canciones de rock chileno para escuchar de madrugada

 
 Llega la noche y consigo la tranquilidad. Porque el día está dominado por un reloj que nunca se detiene entre obligaciones diarias y no saber qué hacer con la libertad de la juventud. Aquella misteriosa etapa en la que todo parece perfecto y las risas son perpetuas y los amigos inseparables. Entonces, mientras haya luz, lo mejor es salir a la calle y golpear duro al mundo que tan poco sabe de la vida. Por eso estás tú, con los inseparables audífonos y una larga lista de canciones, para enseñarle a los demás cómo se disfruta de una atrevida existencia. 
 Por la mañana, las canciones deben de incitar al baile, al movimiento, porque nadie quiere empezar la rutina con bostezos, o lo que es peor, con tonalidades menores que provoquen el llanto. Si no son melodías con mucha energía, entonces hay que elegir aquellas que evoquen la felicidad con recuerdos de la última cita o el primer beso. Ahora sí, estás listo para enfrentar cualquier obstáculo que se presente. 
  
 Pero la noche tiene reglas distintas. Ya no necesitas obtener fuerzas de la música, el silencio nocturno soporta más sentimientos que sólo risas. Ahora sí tienes la oportunidad de divagar en la parte incomprendida de la vida. Es hora de reflexionar sobre el amor, la escuela o el trabajo, la amistad y el futuro, un tema que a cierta edad desinteresa pero acecha de manera aún incomprendida. Mas no te espantes, porque aún no es tiempo de tomar decisiones difíciles,  con saber qué pasará en tu tierna relación o la próxima ronda de exámenes es suficiente. 
 Por razones obvias, te tomas unos minutos para decidir cuál es el siguiente paso en las obligaciones, casi todo está dicho. Y lo que queda es el misterio del amor en todas sus definiciones. Comienzas a pensar en este tema,  y como un parpadeo, te internas en la recta final de la oscuridad, la madrugada de un día que está por comenzar. 
  
 Para no perder el tiempo que resta aquí hay un listado de canciones que te ayudarán en ese momento de profunda reflexión sobre el devenir, o al menos, sobre el amor que ocupa tanto espacio en tu cabeza. 
 – ‘Afortunada’ – Francisca Valenzuela 
  
 Cuando se necesita hablar del corazón de una mujer, no hay mejor voz que la de Francisca Valenzuela. Su experimentada vida le otorga acertados conocimientos de todas las dudas que se tiene en el amor. 
  
 – ‘Milagro’ – Lucybell 
  
 Cuando el amor se detona, la noche se ilumina de diferentes maneras. No se necesita decir mucho, sólo agradecer a la vida por poner un milagro frente a ti. 
  
 – ‘Las seis’ – Joe Vasconcellos 
  
 Con una cadencia lenta, este compositor chileno recuerda el frío de la mañana que provoca la distancia y la soledad del corazón. Termina la noche y lo que queda son unas enormes ganas de llegar a su cama. 
  
 – ‘Simetría’ – Ases Falsos 
  
 Esta canción es un poema de la perfección. Cuando todo está alterado y desordenado, Ases Falsos reclaman que se tiene que buscar la simetría de las cosas, esto incluye al amor, por supuesto. 
  
 – ‘Noche (Gotas de miel)’ – NicoleBajo un velo de metáforas, Nicole suaviza la noche con una canción que agradece al otro por estar ahí en la oscuridad para brindar apoyo. 
 – ‘Tírate’ – Los Tres 
 También el dolor encuadra a estas horas y el grupo de Los Tres acepta que en ocasiones un amor nunca regresa. El sufrimiento inunda el pensamiento y hace decir palabras tan complicadas como: ‘Tírate”. 
  
 – ‘Canción para mañana’ – Los Bunkers  El corazón se agota de vivir en el dolor y la tristeza que provoca un abandono, pero siempre existirá el mañana, con nuevas promesas y alegrías. 
  
 – ‘Quédate’ – De Saloon 
 El dolor es un círculo sin fin. La noche lo intensifica y lo mejor que se puede hacer es decirle: “Quédate” a esa persona que un día regaló un ramo de flores y un mapa de caricias. 
  
 – ‘Cuando respiro en tu boca’ – Lucybell 
 Cuando se tiene la fortuna de vivir un amor sincero, respirar en su boca es un alimento para el alma. No se necesita más, todo está envuelto en sentimientos de tranquilidad que se pueden gozar en el silencio nocturno. 
  
 – ‘De la claridad a nuestro sol’ – Chancho en Piedra 
 Esta canción mezcla los sonidos del funk y el jazz de un modo suave para darle voz a un discurso de melancolía provocada por el desamor. 
  
 – ‘Tren al sur’ – Los Prisioneros 
 Este es uno de los grupos más importantes para la historia del rock chileno. ‘Tren al sur’ es el canto de todas las personas que se sienten extranjeras en este mundo y que necesitan viajar a lo más profundo de su alma para encontrarse. 
  
 – ‘Cerrar y abrir’ – Los Tres 
 Así como la madrugada es el límite entre el día y la noche, esta canción sitúa al amor entre la oscuridad y la luz, ahí donde se puede destruir o sanar. 
  
 – ‘Pasajero’ – Pedropiedra 
  
 La vida es un largo viaje de la que eres pasajero. Durante el camino conocerás el amor y la amistad, mientras observas diversos paisajes de colores y sombras, pero al final, de eso consiste vivir. 
  
 – Para completar esta lista, necesitas escuchar algunas canciones en español para estar solo y  pensando en alguien. 
 *** Te puede interesar: 
 15 canciones tristes en español para decirle adiós a esa persona que te hizo tanto daño 
 10 canciones que amarás si te gusta la nueva música clásica