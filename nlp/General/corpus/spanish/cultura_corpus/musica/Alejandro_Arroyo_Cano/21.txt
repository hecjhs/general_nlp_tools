Las 8 mejores canciones del genio del jazz para noches de amor interminables

 La historia del jazz puede resumirse en cuatro palabras: Louis Armstrong, Charlie Parker.Mils Davis Cuando se trata de la cita perfecta hay que preparar hasta el más pequeño detalle. El atuendo, la comida y el lugar pueden arreglarse con un poco de dinero, pero la delicadeza que debe sentirse en el ambiente no puede obtenerse de manera artificial. Si se tiene una cita de amor, la única forma de crear una atmósfera elegante y seductora es con la música correcta. Si quieres tener una noche de pasión interminable que conduzca el alma hasta el cielo y la regrese colmada de sensaciones placenteras, es necesario escuchar a Louis Armstrong. 
 Louis Armstrong se trata de una de las figuras más carismáticas e innovadoras de la historia del jazz y probablemente el músico más popular de los años 20. Gracias a sus habilidades musicales y a su brillante personalidad transformó el jazz de una música de baile con raíces folclóricas a una nueva forma de arte popular. Lo más importante es que fue un eterno enamorado de la música y la pasión desbordante que tiene ante la vida se escucha en cada una de sus composiciones. 
  
 Louis aprendió música escuchando al trompetista King Oliver, tenía un oído muy refinado e intuitivo. Tardó un par de años en dominar el instrumento y formar su propia orquesta. En 1928 grabó “Western Blues”, uno de los discos más famosos de la historia de la música. Cuando ya era reconocido en el país, quiso honrar su raíces a toda costa, así que comenzó un extenso proceso de reinterpretación, es decir, tomó temas folclóricos que habían quedado guardados en su memoria y los adaptó para tocarlos con su banda. 
 Su éxito continuó al punto de convertirse en el músico afroamericano más importante de Estados Unidos en la década de los 50. Unos años antes de morir compuso la canción “Hello, Dolly”, la cual obtuvo el puesto número uno en las listas norteamericanas, superando incluso al grupo The Beatles. Así de importante y preciso fue Louis Armstrong. Por eso nunca defraudará a su público, incluso en los momentos de pasión, sólo hay que escoger las canciones indicadas para que la noche fluya. 
 – ‘La Vie en Rose’ 
 La canción le pertenece a Édith Piaf, quien la inmortalizó en los corazones de todos los amantes con una notable interpretación. Años más tarde, Louis Armstrong la retomaría mágicamente con su trompeta. Escuchar la canción provoca un suspiro instantáneo que marca el inicio de la noche. 
  
 – ‘They Can’t Take That Away From Me’ 
 Esta pieza maestra es cantada a dueto con Ella Fitzgerald, una de las mujeres más importantes en el jazz. La ternura brota en una de las letras de amor más bellas en la historia, pues enaltece el enamoramiento al grado que es un sueño romántico ver cómo la otra persona prepara el té. 
  
 Conoce las 10 canciones que demuestran que el jazz no es sólo de hombres . 
 – ‘Mood Indigo’ 
 El tema fue compuesto por Duke Ellington con una suavidad casi inmaculada. Al tener un tema tan especial en sus manos, Duke pidió que Louis Armstrong le pusiera la cereza del pastel con su voz y sus dotes en la trompeta. Si una noche de amor está próxima hay que aprovechar la melodía de estos dos grandes del jazz y darle el ritmo indicado a las caricias entre los amantes. 
  
 – ‘A Kiss To Build A Dream On’ 
 Esta canción fue compuesta por Bert Kalmar en 1935, pero fue hasta 1951 cuando Louis Armstrong la grabó con una orquesta de jazz. La letra es un gran poema romántico: “Dame un beso para construir un sueño y mis pensamientos se quedarán pegados a él. Cariño, no te pido nada excepto esto, un beso para construir un sueño”. 
  
 – ‘If I Could Be With You (One Hour Tonight)’ 
 Cuando hay que pedirle a alguien que se quede un momento más, no hay mejor manera que hacerlo que con una canción de Louis Armstrong. El tema fue compuesto por James P. Johnson y desde entonces los grandes intérpretes del jazz han interpretado este poema. Incluso apareció en la película “Casablanca”. 
  
 – “Moon River” 
 Originalmente el tema fue compuesto para que Audrey Hepburn la cantara en la película “Breakfast at Tiffany’s”. La belleza de la canción es tan grande que era imposible que se quedara sólo en el cine. Louis Armstrong fue uno de los grandes maestros del jazz que la interpretó de nuevo de una manera muy tierna. 
  
 – ‘I Can’t Give You Anything But Love’ 
 De una manera elegante y al más puro estilo de Armstrong, el más grande intérprete de jazz retoma la composición de Dorothy Fields y la transforma en un rítmico swing que es perfecto para animar la velada después de una larga jornada de besos y caricias. 
  
 – ‘I Only Have Eyes For You’ 
 Cuando llega la hora de separarse tras una espléndida noche de amor, es válido poner una canción más para hacer el último baile de la noche. Louis Armstrong se despide del encuentro con una canción que captura certeramente el único amor hacia una persona, uno que no se tiene dos veces en la vida, uno que te obliga a sólo tener ojos para él o ella. 
  
 – La pasión de Louis Armstrong era tan grande que pasó componiendo música hasta los últimos días de su vida. El toque romántico está en todas las pasadas canciones, pero si quieres aumentar la intensidad a tu velada, escucha las 20 canciones de jazz que te harán ir de la cena a la cama. Si reconoces que el jazz es sumamente seductor pero va más allá de una cena romántica es probable que conozcas las 10 piezas de jazz para empezar a convertirte en un experto.