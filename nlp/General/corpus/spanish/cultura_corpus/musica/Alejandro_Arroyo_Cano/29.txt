Bandas que debes que conocer si ya llegaste a los 20

 Terminó la época de descontrol, bueno, llegó a su fin la primera etapa del viaje. Porque en el rock nunca se acaba la fiesta. Lo que sí hay que tener en cuentas es que el momento de darle a tus oídos algo más interesante que sólo acordes planos ensuciados de una gran distorsión. Grupos como Nirvana, Joy Division y todas esaa bandas de preparatoriano sirvieron para contextualizarte, pero ahora se necesita más armonía, más contrapunto y cadencia. Como se dijo, en el rock o en cualquier espacio de la música nunca se acaba de conocer. Por eso sería absurdo quedarse con una biblioteca de cien artistas. Ese número está bien, pero sólo en los primeros años. Ahora es tiempo de dar el siguiente paso, es decir, de madurar. ¿Te suenan bandas como Foxygen, Crocodiles o Bart Davenport? Si dijiste que sí, muy bien, se nota que te tomas muy en serio lo que significa el rock. De lo contrario, hoy es el momento de salir de la cuna. 
 Puede que algunas de estas bandas ya las conozcas, entonces quizá te atraiga más conocer lo más fresco en la música, lo que acaba de salir del horno, en ese caso, da click aquí. Sólo falta aclarar que este pequeño playlist no está pensado en la novedad, sino en el pasado, en lo que está un poco empolvado pero que vale la pena retomarlo. 
 – Fleetwood Mac – ‘Dreams’ 
  
 Cuando el oído empieza a educarse y exige algo más tranquilo, pero de mayor calidad, Fleetwood Mac es la mejor opción por su elegancia melódica y su ritmo que viene del blues y lo transforman en un soft rock. La banda tiene un listado enorme de éxitos y ‘Dreams’ es un buen comienzo. Si te gustó puedes pasar a ‘The Chain’ o ‘Little Lies’, otras canciones agradables para los sentidos. 
  
 – Nick Cave & The Bad Seeds – ‘ Red Right Hand’ 
  
 Desde sus primeros pasos, Nick Cave se decidió por llevar la música a un nivel superior de experimentación. Inició en el punk y fue madurando conforme a su edad. Su proyecto más exitoso lo tiene con de The Bad Seeds. Esta canción muestra un estilo sobrio, profundo y muy reflexivo. 
  
 – Nick Drake – ‘Pink Moon’ 
  
 El virtuosismo con la guitarra se ve en sus tres discos de larga duración que lanzó antes de su muerte a los 26 años. Los críticos lo califican como uno de los mejores guitarristas y compositores que dejaron inconclusa su obra. La calidad de sus composiciones es magnífica, como él no hay dos. Su música es poco divulgada en Latinoamérica, pero es una obligación conocerlos si se quiere aprender de las joyas de la música. Déjate llevar por el susurro de ‘Pink Moon’ 
  
 – Brian Eno – ‘Deep Blue Day’ 
  
 Escuchar ‘Deep Blue Day’ es como dar un paseo por el espacio, mientras el vacío se llena de unas ondas cristalinas que conducen al espectador a los confines del universo. Justo esa sensación es la que quería realizar Brian Eno y, por supuesto, lo logró con su gran experiencia en construcciones sonoras mediante la tecnología. Sin duda, Brian Eno es un músico que debe acompañarte desde este momento de tu vida. 
  
 – 
 Jeff Buckley – ‘What Will You Say’ 
  
 Otro de los grandes músicos que murió dejando el éxito por delante. Su habilidad con la guitarra se fusionaba de manera conmovedora y seductora con su voz de ángel.  Su mayor canción fue ‘Hallelujah’, que tiene un toque místico, una blanca luz que rodeaba su alma hasta el día que murió a los 30 años. Si quieres terminar de madurar tus sentidos y tu gusto musical, debes conocer a fondo la obra de Buckley. Un buen inicio es este tema. 
  
 – Thievery Corporation – ‘Lebanese  Blonde’ 
  
 Es momento de obligar a tu oído a comprender composiciones más complejas.  Thievery Corporation es un proyecto norteamericano que experimenta con el trip hop y el lounge. Su música es para relajar la mente con el fluir de sus notas que crean atmósferas de tierras sagradas. El camino hacia lo místico inicia con ‘Lebanese Blonde’. 
  
 – Crocodiles –  ‘I Wanna Kill’ 
  
 Llegando a artistas más nuevos, esta banda de garage, hard rock y post-punk es la muestra de la vitalidad durante la juventud. A pesar de que su música supera cualquier tema que suena en la radio, Crocodiles no alcanza un grupo mayor de seguidores. ‘I Wanna Kill’ es su tema más “conocido” y seguramente te enganchará si amas el buen rock. 
  
 – Bart Davenport – ‘Girl Gotta Way (Peaking Lights Remix)’ 
  
 Si en solitario Bart Davenport ya era muy bueno, el remix que hace Peaking Lights a ‘Girl Gotta Way’ termina de pulir el diamante. Desde el momento que pongas play sentirás cómo Davenport entra por tu cuerpo como una espesa niebla que te hará sentir un increíble placer. Llegar a los 20 es comenzar a madurar y ésta es una buena manera para empezar a hacerlo. 
  
 – Foxygen – ‘Make It Known’ 
  
 Desde California, Foxygen presenta una muestra bastante poderosa y atractiva de psychedelic pop, garage y rock. En realidad a ellos poco le importa la cuadratura de los géneros, lo más importante es expresar lo que se lleva dentro. ¿Qué es? Una jovial locura que no terminará hasta que el cuerpo desfallezca. Muchos medios especializados han calificado a este dúo como una “banda revelación”, así que no hay más pretextos para no escuchar a Foxygen. 
  
 – The War On Drugs – ‘Under the Pressure’ 
  
 De los nuevos grupos que se encuentran dentro de esta lista, The War On Drugs posiblemente sean los más famosos. Su popularidad se disparó con la llegada de su disco “Lost in the Dream”. Para no confundirse, porque su música puede llegar a tener muchas texturas, la banda toca indie rock. Si aún no los conocías, esta es la edad fundamental para empezar a hacerlo. No hay mejor carta de presentación que la siguiente canción. 
  
 – El mundo de la música es tan grande que también estaría mal encasillarse con el rock, por eso, en el siguiente enlace hay algunas nuevas propuestas de música electrónica que seguro te van a encantar. Si definitivamente lo tuyo es el rock, si das click aquí conocerás a algunas bandas que te harán salir de la rutina de lo que estás escuchando. Ya que estamos hablando de los buenos tiempos, para completar tu día aquí hay 13 películas de la juventud rebelde, imperfecta y desnuda.