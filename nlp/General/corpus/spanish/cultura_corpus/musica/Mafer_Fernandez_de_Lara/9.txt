Las 25 mejores portadas de la revista NME

 Después de vivir el esplendor de los años 60, ser espectadora de las creaciones de grandes bandas, convertir a sus portadas en uno de los rostros más importantes de la industria musical y caracterizarse como una de las revistas musicales más reconocidas de Inglaterra y el mundo; hoy, 63 años después de su lanzamiento, la revista New Musical Express (NME) abandona los quioscos de revistas para darle un nuevo giro a la emblemática publicación. 
 La revista, que a lo largo de su historia,  ha destacado por acompañar a las bandas en su crecimiento musical y profesional, pero también ha tenido momentos difíciles. 
  Con el posicionamiento del rock y las bandas,  la competencia de la publicación semanal aumentó, llevándola casi al cierre. Fue hasta que Alan Smith, en 1963, tomó el puesto de editor,  cuando la revista retomó su camino presentándose como un semanario creativo y original. 
 En 1976, con el nacimiento del género Punk,  la revista obtuvo un nuevo logro, era la primera en apoyar y difundir este estilo. New Musical Express continuaba sosteniéndose como la publicación musical más importante del Reino Unido, incluso The Sex Pistols escribieron una canción sobre la editorial: “Anarchy In The UK”. Una revista que ha estado presente en millones de los más grandes acontecimientos de la historia musical, desde la muerte de Elvis Presley y Kurt Cobain hasta el surgimiento del dubstep y la música indie.  Actualmente, con un record de 200 mil ejemplares vendidos, NME, el símbolo de la prensa musical londinense, va a seguir los pasos de otras publicaciones que han comenzado a distribuirse como revistas gratuitas. 
  Aunque su fuerte estará basado en su versión digital, la que desde 1996 ha representado un punto importante para su negocio, a partir del 18 de septiembre se imprimirán 300 mil copias de una nueva NME gratuita. Además de este gran cambio, el enfoque de sus publicaciones ya no sólo abarcará temas de música, también le darán importancia a la moda, la televisión, la política, etc. De acuerdo con Mike Williams, director de NME, buscan tener la misma influencia pero más grande y más fuerte. En víspera de este gran cambio y cumpliendo los 63 años de recopilar una gran historia, la revista, a través de página web, pidió a sus lectores que eligieran, de una larga lista de portadas, sus favoritas. Estas fueron las más votadas, convirtiéndolas en las 25 mejores portadas de la historia de New Musical Express . 
  1. Manic Street Preachers – Octubre 3, 19922. Manic Street Preachers – Mayo 11, 19913. Amy Winehouse – Julio 30, 20114. Nirvana – Abril 16, 19945. Ian Curtis – Junio 14, 19806. Pulp, Marzo 2, 19967. The Ramones – Mayo 21, 19778. The Sex Pistols – Agosto 6, 19779. Blur vs Oasis – Agosto 12, 199510. The Libertines – Junio 8, 200211. Echo & the Bunnymen – Febrero 20, 1982  
 12. Syd Barret – Abril 13, 197413. The Stone Roses – Noviembre 18, 1989 
  
 14. David Bowie – Mayo 15, 1976 
  
 15. The Birthday Party – Marzo 23, 198316. John Lennon – Diciembre 13, 1980 
  
 17. The Clash – Abril 2, 1977 
  
 18. The Sex Pistols vs Bill Grundy – Diciembre 11, 197619. Jo Brand and Pulp – Abril 9, 199420. The Slits – Octubre 7, 197821. Daft Punk –  Mayo 14, 2013 
  
 22. Björk – Agosto 14, 1993 
  
 23. Morrissey – Febrero 13, 1988 
  
 24. Blur – Junio 25, 1994 
  
 25. Debbie Harry y Andy Warhol – Junio 11,1986