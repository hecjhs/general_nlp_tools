Las mejores frases de Zoé que siempre nos harán sentir en la Vía Láctea

  Eran los tempranos 2000 y en los salones de secundarias y preparatorias se comenzaban a escuchar las canciones de una banda mexicana que sin esperarlo, marcaría un nuevo comienzo para la música en español en nuestro país, esa banda fue Zoé.  La agrupación liderada por León Larregui iba poco a poco haciéndose en espacio entre los jóvenes a los que pocos proyectos mexicanos les hacían ruido; ya no pertenecían a la generación que idolatraban a Café Tacvba y la música en inglés, como The Strokes, no los llenaban por completo, por lo tanto, en su corazón musical había un espacio que llenar y con discos como su homónimo y Rocanlover comenzaron a ser vistos hasta que llegó su pieza maestra: Memo Rex Commander y el Corazón Atómico de la Vía Láctea, trabajo que desde el título es memorable.   Dejaron de ser una banda que tocaba donde pudieran con dos que tres pasados de mezcales aventándoles vasos para llenar arenas y tener el poder de llenar fechas con shows a lo largo del país y fuera de éste. Zoé se convirtió rápidamente en la sensación demostrando que ni importa qué tan independiente sea una banda y el sello discográfico al que pertenezca, pues lo que necesitamos como público con canciones que nos hagan reflexionar, que nos cuenten historias distintas y que nos hagan querer vivir esas que parecen lejanas de tan hermosas que se escuchan.   Zoé le ha cantado al amor sin tener que decir te amo, se han colado en corazones que se resistían primero a bandas emergentes y después a los que no le ven valía al mainstream; sea como sea, hoy, siguen siendo una de las agrupaciones más importantes cuando se música en español se habla en toda Latinoamérica y estas frases con una pequeña prueba de que siempre habrá una canción con a voz de Larregui que te haga volar en un espacio infinito.    “Sé que te perdí cuando resbalé, pero nunca jamás te dejaré de amar”
  “Quiero libertad en un mundo material, sentir el amor sin volverme a enamorar”  “Todas las noches bajo la Vía Láctea parecen eternas si tú no estás”  “Tengo ganas de ser aire y me respires para siempre”  “Perdóname por mi levedad”


  “Llevo tu voz en mi voz, grabada con aerosol”
  “Quiero descubrir por qué le tengo tanto miedo al amor”
  “Recobré la vista al comprender que tus ojos son el infinito”  “Te me escurres por los ojos suavemente”

“Nunca quise hacerte mal pero siempre que me acerco al fuego se me escurre el diablo”



  “Esteriliza mi sangre y purifica mi amor”  “No tengo que ser clarividente para darme cuenta que no estás”  “Brillas en el firmamento y te vas”
  “Te abandonaste en el juego de la falsedad”   “En el regazo de tu piel me dejo llevar al sol”


  “Abraza al mundo con tus alas de fuego”  “Suéltame y déjame volar a través del Universo”  “Quiero un fin se semana en el cielo, quiero volver a verte otra vez”  “Dame sólo un beso que me alcance hasta morir”  “Déjate llevar que tarde o temprano tienes que despertar”



***
Te puede interesar: 
Las peores decepciones de la música en el 2015