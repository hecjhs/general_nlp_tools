10 videos de bandas internacionales que fueron grabados en México

 ¡México! ¡Capital! ¡La populosa ciudad donde orgullosa ostenta Flora su vergel más lindo! ¡No os extrañe si yo por ella brindo, que es mi ciudad natal, donde he nacido, donde la luz del sol he conocido! Es mi ciudad natal un gran museo; por dondequiera veo automóviles, coches, carretelas, casas particulares, mil escuelas do mis colegas, jóvenes y niños, van de la ciencia a recibir cariños. Salvador Novo  Así como poetas se han inspirado en la caótica pero siempre hermosa Ciudad de México para regalarle algunas de sus letras, músicos y artistas también han sido fuertemente inspirados por todo lo mágico que converge en nuestra tierra. Se han hecho innumerables canciones que hablan sobre la vida en México , su situación social y la belleza de sus tesoros naturales. Otras de las obras artísticas que se han creado en suelo mexicano son videos que acompañan sencillos de bandas extranjeras, desde Madonna hasta las bandas más indie del rock en inglés ha tomado los escenarios que les ofrece México para “atraparlos” por siempre en su material y en la memoria de sus fans.  Estos son algunos videos de bandas o solistas extranjeros que se han grabado en México.  Damon Albarn – Heavy Seas of Love El líder de Blur para este track se hizo acompañar de Brian Eno. La idea creativa de esta pieza corrió a cargo de Albarn y Aitor Thorup; aunque en realidad, la mayoría de las tomas fueron producto de los videos que Damon tomó durante sus vacaciones por todo México.   
 

Florence and the Machine – What Kind of man  El video original es un cortometraje dirigido por Vincent Haycock y coreografiado por Ryan Heffington, y se estrenó el mismo día que lo rodaron. 
   
  Real Estate – Hard to hear  El director Richard Ley ha reunió en un video la frustración que se puede llegar a sentir por vivir en una caótica ciudad y estar la mayor parte del tiempo fuera de casa. Los alrededores de varios sitios de la ciudad de México fueron el marco para llevar esa situación a una proyección visual. 
    
  
The Kooks – She moves in her own  El sencillo forma parte del disco Inside In/Inside Out de un ya lejano 2006. El videoclip del track se fue grabado en Tijuana y fue dirigido por Diane Martel con los mejores momentos de la banda en la ciudad. 
   
   Nine Inch Nails – We’re in this together Trent Reznor y un grupo de hombres vestidos de negro corren por unas calles vacías de Guadalajara, suben a un tren y llegan a un campo. Estuvo dirigido por Mark Pellington y lanzado el 27 de agosto de 1999 aunque tiempo después apareció una versión extendida del video donde aparece una corta escena de una mujer joven vestida de rojo en contraste con el video o en blanco y negro. 
    
  Anna Calvi – Sing to me  Está dirigido por la cineasta Emma Nathan quien viajó con Anna a nuestro país para realizar las tomas necesarias para entregar un material visual espectacular e impecable. 
   
   Feist – The bad in the each other  La cantante ha dicho en varias ocasiones que una de sus ciudades favoritas es México, por lo que cada vez que viene a dar un concierto no puede perder la la oportunidad de pasear por su calles, así que para este video decidió aparecer con el resto de su banda por las calles del centro histórico. 
    
  Bombay Bicycle Club – Lights out words  Tal parece que en una de las visitas que la banda tuvo a nuestra ciudad, se toparon con la ciudadela y toda la gente que acude a bailar cada fin de semana. Así que quisieron que esas bonitas costumbres fueran recordadas por siempre acompañadas de su suave y alegre canción. 
    
  The Rolling Stones – I go wild Fue en 1995, días antes de que presentaran en concierto en México como parte de la gira para el  “Voodoo Lounge”, la banda británica decidió grabar en el video del sencillo “I go wild”. El lugar que eligieron fue ex-templo de San Lázaro, cerca del centro histórico de la Ciudad de México. 
    
   Moby – Disco Lies  La historia de un pollito que se escapa de una granja después de ser testigo de la matanza de sus amigos y familia fue dirigido por Evan Bernard. El video no sólo fue grabado en nuestra ciudad, sino que además fue una declaración de Moby en contra de la industria de la carne. 
   
 *** Te puede interesar: Canciones que sólo recordamos por sus videos