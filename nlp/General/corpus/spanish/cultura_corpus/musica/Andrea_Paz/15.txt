Canciones con las que quisiéramos despedirnos de este mundo

  “La muerte es algo tan tremendamente airado, que sólo la desnudez, la elemental desnudez, puede escindirla del ridículo”. Camilo José Cela

 Cuando pensamos en la muerte nos atacan esas preguntas sobre el qué le vamos a dejar al mundo, cómo vamos o cómo queremos ser recordados sin darnos cuenta que lo que hacemos para los demás, inconscientemente lo hacemos sólo para nosotros y para trascender porque tal vez el miedo a ser olvidados es más grande que la simple idea de dejar de existir físicamente en este plano. Dejando de lado razones obvias por las que podamos dejar de existir, vamos escondiendo lo que de verdad nos mata… esos recuerdos que en su momento abrazaban el alma; los que llegaron a salvarnos del vacío, los que nos hicieron creer que no estábamos solos en el mundo aunque claramente, por más acompañados, todos somos islas desiertas. 
 Utópicamente, sostenemos firmemente que la vida es para ser feliz y esa es la misión más difícil que se nos pueda encomendar.  Y como si la existencia se tratara de una película en la que se nos da una última oportunidad de hacer eso que nunca nos atrevimos, que nos hizo perder la cabeza, las llaves y el tiempo, pensamos en personas que jamás se preocuparon por nosotros; en las que amamos más de la cuenta y en las que hasta propina les dejamos; en esas cosas que dijimos cuando debimos callar y viceversa; en las cartas que no escribimos o en las que nunca enviamos; en todas las veces que perdimos la dignidad y la volvimos a encontrar, en todas esas mentiras creídas y en verdades puestas en duda; en los mediocres a quienes nos aferramos y en los recuerdos de los que nos alejamos… y aún así, nos damos el tiempo para pensar en esas  canciones  con las que queremos despedirnos.  “On My Way” en la voz de Sinatra es ya todo un himno de despedida, pero, por fortuna, en ese inmenso mar que nos ha dado la música, tenemos ene cantidad de  canciones  para hacerlas nuestras y hacer una remembranza de todo eso que vivimos, de eso que necesitamos decir y sobre todo, canciones que expresan por nosotros sentimientos que aún no han sido descritos con palabras. 


 A Day in the Life – The Beatles“Woke up, fell out of bed, dragged a comb across my head. Found my way downstairs and drank a cup, and looking up I noticed I was late”


 For Lovers – Pete Doherty
“I forgive you everything. Meet me at the railroad bar about 7 o’clock, we joke while the sun goes down.
Watch the lovers leaving town”
 Rock n’ roll suicide – David Bowie
“No matter what or who you’ve been . No matter when or where you’ve seen , all the knives seem to lacerate your brain . I’ve had my share, I’ll help you with the pain. You’re not alone”

 Like a Rolling Stone – Bob Dylan
“How does it feel to be on your own with no direction home, like a complete unknown, like a rolling stone” El Necio – Silvio Rodríguez
“Yo no se lo que es el destino, caminando fui lo que fui. Allá Dios, que será divino. Yo me muero como viví”  There Is A Light That Never Goes Out”- The Smiths
“Take me out tonight because I want to see people and I want to see life. Driving in your car, oh, please don’t drop me home because it’s not my home, it’s their home, and I’m welcome no more” The Day is Coming – My Morning Jacket
“The day is coming, you know what I mean. The day is coming, the way is clear. The day has come, hey babe” Never Get What You Want – The Rolling Stones
“Honey, you can’t always get what you want. You can’t always get what you want but if you try sometime, yeah, you just might find you get what you need!”
 Dancing in the Dark – Bruce Springsteen
“Message keeps getting clearer, radio’s on and I’m moving around the place. I check my look in the mirror, I want to change my clothes, my hair, my face. Man I ain’t getting nowhere”

 
 Perfect Day – Lou Reed  “It’s such a perfect day,  i’m glad i spent it with you. H h, such a perfect day,  you just keep me hangin’ on,  you just keep me hangin’ on”   
 *** Te puede interesar:  Canciones para enamorar a alguien a quien no le gusta el rock  15 canciones que todos hemos dedicado para recuperar a un amor