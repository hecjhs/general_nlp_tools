Madurar es: elegir tus canciones favoritas

 El fin de semana, me enfrenté a esa pregunta que por mucho tiempo me dio pánico “¿Cuáles son tus cinco discos favoritos de todos los tiempos?”. Durante tres segundos la pregunta retumbó en mi mente y comenzaron a llover imágenes de portadas de discos que me encantan, la respuesta fue sencilla, rápida y irreductible. Quizá a eso se le llame madurar.  Esa pregunta que involucra los discos o canciones preferidas de toda la vida, siempre será el equivalente al playlist para el fin del mundo, a eso que llevarías a una isla desierta para volverte menos loco. Y como cada día estamos más cerca del inevitable “fin del mundo” ya sea que lo vivamos o no, heme aquí dándoles algunos consejos para sobrevivirlo.    Por supuesto que habemos quienes hacemos memoria dependiendo de lo que escuchamos “X” día. -¿Te acuerdas cuando fuimos a comer y te conté que me enojé con Panchito? -Claro! en la cafetería sonaba “Stolen Dance” de Milky Chance… más o menos algo así. Por lo que lo más preocupante es ¿qué incluir en lo que será nuestra obra maestra? Hay que pensar muy bien en los tracks que incluirá el jefe de jefes de todos los playlists que jamás hayan existido.    
1. No agregues canciones demasiado extrañas para hacerte el “cool”, procura que todas hayan cambiado tu vida, pero también que sepas cuáles han cambiado la vida de la otra persona, porque los recuerdos nunca vienen mal.

 
2. Si vas a agregar canciones felices, que también tengan razón de ser, no al azar y no las agregues porque tienes miedo de que ya está por acabarse el mundo y entonces lo evades con música que sólo hará que la melancolía se vuelva una completa tragedia. Ya es trágico; lo mejor que puedes hacer es dejarte ir y disfrutarlo lo más que puedas.

 
3. No tengas miedo a incluir dos o tres canciones que te dan penita. Si hay canciones que escuchabas cuando eras un/una puberto/a, agrégalas sin temor, no habrá nadie quien se burle de ti, pero tampoco exageres.
 
4. Incluye canciones que sabes que te han dedicado. Si no te ha dedicado canciones, pues… inventa algunas que quisieras lo hubieran hecho.
 
5. No seas fan from hell. Si te gusta la discografía entera de algún artista, perfecto, pero sé sincero, no es lo único que quieres escuchar antes de que llegue el fin.  Escoge dos o tres, únicamente las más importantes.
 
6. Cierra los ojos y reza porque que hayas escogido el mejor soundtrack para el gran final.      Cabe mencionar que también se necesita todo el kit: cuadernos de notas, unos audífonos, los diarios infinitos de canciones vitales. Mente en blanco. Apaga tu celular, salte de Facebook, Twitter, Instagram, no hagas check-in por una vez en tu vida; imagina que a nadie le importa en dónde estas y piensa por un instante que esta será la última vez que podrás intentar cambiarle la vida a alguien a través de la música. Necesitarás concentración total.