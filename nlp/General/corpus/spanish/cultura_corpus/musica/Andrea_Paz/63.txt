Guía de supervivencia para el Corona Capital

 
 Ir a un festival no solo es cosa de comprar un boleto, organizarte con tus amigos y ya; se requieren de algunas cuantas mañas o técnicas de supervivencia para que el vivir un festival se convierta en una experiencia casi mágica el estar rodeado de música todo el tiempo y no una que nos haga lamentar el haber estado ahí para recordarlo como el peor día de nuestras vidas. 
 Cada año vemos en nuestras redes sociales y en publicaciones de medios especializados fotografías de bandas que quisiéramos que pisaran nuestro país y de cientos de personas que tal parece han conocido el estado más puro de felicidad y que parecen disfrutar el hecho de acabar casi vomitando acordes y quemaduras de sol. 
 Tal vez en México aún no lleguemos a tener un festival como Coachella o Lollapalooza, pero el Corona Capital cada año no le pida nada a los anteriores, pues se ha convertido en el mejor fin de semana de muchos melómanos que lo esperan con ansias desbordantes. Así que enlistamos algunos algunos puntos esenciales que debes de tomar en cuenta para hacer de este Corona Capital 2015 (y de cualquier otro festival) el mejor de los recuerdos en el año. 
     Conoce a las bandas  Un festival se trata de ir a disfrutar en primer lugar de la música, así que por favor, no vayas sólo a tomarte la selfie y a estar en una especie de “fiesta” local con tus amigos, respeta al de al lado que de verdad quiere disfrutar de quienes están sobre el escenario. Con meses de anticipación o por lo menos algunas semanas, busca playlist, escucha los discos básicos de los headliners o pide a alguien que te recomiende qué escuchar para que llegues preparado. 
 Compra con antelación los boletos  Nunca falta el despistado que muere por ir de último momento y se enfrenta a dos situaciones: que los boletos se han terminado y los tendrá que adquirir en reventa o comprarlo a ese que “ya no pudo ir” a precios más altos. Piensa que en el peor de los casos, la reventa para este tipo de eventos es lo más lamentable que puedes hacer, pues muchas veces los boletos son falsos. Evita la estafa. 
 Fija puntos de reunión 
 Ya sabemos que la señal móvil es casi inexistente y que los mensajes que envías ese día son recibidos hasta que estás en tu casa. Por eso, si tú o tus amigos quieren disfrutar de actos al mismo tiempo en distintos escenarios, usen los totems distribuidos a lo largo del lugar como punto de encuentro. Así, si alguien se pierde, sabe en dónde esperarlos pase lo que pase. 
 Nunca olvides el papel higiénico 
 Especialmente para las chicas es un problema tener que usar un baño portátil que por supuesto, no tiene papel higiénico incluido, así que llevar un rollo en la bolsa, pañuelos desechables o toallitas húmedas te salvarán la vida. *¡Infecciones alert! 
 Protector solar 
 Últimamente tenemos todas las estaciones del año en un solo día, así que no importa si el día amanece nublado, lleva un protector porque tal vez en la tarde el sol sea insoportable. 
 Gafas de sol 
 Punto vital y accesorio imperdible. Además, son un accesorio básico del look festivalero. 
 Zapatos cómodos 
 ¿Qué pasa por la mente de esas niñas que van en tacones? Ni una top model soportaría más de 10 horas de festival en zapatos altos. Muchas creen que un evento de esta naturaleza es su momento estrella y sacan del closet sus mejores modelitos, pero tómenlo con calma que no es una pasarela: tenis y botas siempre serán los ideales para andar de escenario en escenario. 
 Ropa cómoda 
 Retomando un poco el punto anterior, el estar cómodos es básico pero también es cierto que un festival es la oportunidad perfecta de ponerte eso que jamás de atreverías en un día común. Shorts muy cortos, blusas rotas o que tapan sólo lo suficiente, colores neón, pelucas, medias estampadas o hasta disfraces son más que válidos. Y quién sabe, podrías aparecer el lunes en algún blog que publique los mejores outfits, pero toma en cuenta que además podría llover de sorpresa, como el año pasado, así que no olvides una chamarra. 
   
  Comparte coche 
 Si el llegar es caótico, el irse aún más, porque seamos sinceros, salimos como horda zombie. Si alguien se ofrece como conductor designado y está dispuesto a llevar a todos a sus respectivos hogares, será la mejor solución para evitar en la medida de lo posible la problemática del estacionamiento y salida del Autódromo 
 Lleva el presupuesto exacto 
 ¿Cuántas historias no hemos conocido del que revive el lunes con todo empeñado? Si llevas sólo la cantidad de dinero que puedes gastar POR DÍA, el lunes evitarás arrepentirte de cada cerveza que estaba fuera de lo planeado. Además, es casi mitad de quincena, así que tendrás que echar mano de los ahorros 
 Medicinas básicas 
 Es casi 100% acabar con dolor de cabeza, espalda, pies u hombros. Está permitido pasar con medicamentos básicos, como pastillas analgésicas para que llegues vivo hasta el último acto. 
 Backpack mediana 
 Regularmente las mochilas grandes no están permitidas, pero con una mediana, como la que llevas a la escuela u oficina, basta para guardar lo básico como suéter, un cambio de camisetas, el souvenir, el vaso de recuerdo, los lentes de sol y demás cosas que dejes de usar durante el día 
 Come ligero 
 Primero, por presupuesto, porque ya sabemos que una sopa que normalmente cuesta $10 pesos la venden en $50 y segundo para evitar lo más posible usar los baños porque conforme avanza el día la fila para esperar turno crece y crece; por último, entre el refresco, la cerveza, el mezcal, el sol, gente por doquier y largas caminatas… evitar los malestares estomacales es lo mejor que puedes hacer. 
     
Puntos extras : Impermeable (porque nunca se sabe), condones, Pepto Bismol y chicles. 
 *** Te puede interesar: Bandas que a nadie le interesan en el Corona Capital