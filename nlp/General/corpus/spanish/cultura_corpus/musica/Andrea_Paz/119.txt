Canciones para recordar al amor de tu vida

 Se dice que toda persona tiene 3 amores importantes a lo largo de su vida: con en el que se inicia en las artes amatorias conocido como “el primer amor” y le proceden los llamados “el amor mi vida” y con el que te casas o haces una vida, o sea, el amor que se pretende ser “por siempre”.  Claro que para darte cuenta de todo lo anterior tienes que haber pasado por cada una de esas experiencias, pero quizá estés atorado (a) en alguna de ellas sin poder pasar a la siguiente y no sepas cómo identificarlo. Quizá crees que tu primer amor es con el que estarás toda la vida y hasta que la muerte los separe sin tomar en cuenta que así como todo empieza, debe terminar una vez que se hayan cumplido las 6 etapas fundamentales del amor.  1- Enamoramiento Duración: 1 a 18 meses  2. Vinculación Duración: 18 meses a 3 años  3. Convivencia Duración: entre el 2do y el 3er año  4. Autoafirmación:   Duración: entre el 3er y 4to año  5. Colaboración Duración: de 5 hasta los 15 años  6. Adaptación Duración: del 15 al año 25  Una vez que identifiques en qué parte de tu relación te encuentras o hasta cuál has llegado, te dejamos algunas canciones que indudablemente te harán recordar los mejores momentos que has vivido como resultado del enamoramiento, canciones que te harán recordar al amor de tu vida… cualquiera que este sea. Wolfman featuring Peter Doherty – For Lovers 
    
 Bee Gees – How Dee is your Love 
    
 Be My Baby – The Ronettes 
    
 Queen – Love of my Life 
    
 The Kills – The Last Good Bye 
    
 Amy Winehouse – Love is a Losing Game 
    
 Phosphorescent – Song for Zula 
    
 The National – I Need My Girl 
   
  Beck – Blue Moon 
    
 
 Blur – Tender 
   
 También te puede interesar:  Las canciones tristes son para sentirse mejor Canciones de amor que sólo los hipsters dedican