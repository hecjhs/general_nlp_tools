Camisetas de bandas que ya no deberías usar

 Arturo Vega nació en Chihuahua. A los diez años se mudó con su familia a la Ciudad de México y en 1969 viajó a Nueva York, con la idea de estudiar cine. Un día, a finales de 1973, Arturo estaba pintando y escuchando música en su departamento, cuando de pronto un tipo abrió la puerta preguntando por una chica que vivía en el mismo edificio, “Hola, soy Dee Dee” dijo, comenzaron a platicar y desde entonces se hicieron amigos. 
 Luego de un tiempo y con más confianza, Dee Dee llevó a sus amigos Johnny y Tommy al departamento de Arturo, lugar que se convertiría en el preferido para que los Ramones tuvieran míticas reuniones. Ya con cierta relación con el mundo de la música, Arturo terminó siendo el director artístico de la legendaria banda de punk, pintó sus primeros telones y estuvo presente en 2,261 de los 2,263 conciertos que dieron.  Pero también hizo algo que pasaría a la historia: diseñó el logo de los Ramones; un águila sosteniendo un bat y con los nombres Johnny, Joey, Dee Dee y Tommy estampados en blanco sobre negro. Éste tal vez sea el logo más reproducido de cualquier banda en el mundo, el más pirateado y sobre todo, el más usado en una camiseta por amantes (y wannabes) de la música. Así como la de los Ramones, hay otros tantos logos de bandas que han invadido ene cantidad de closets habitando en camisetas. Así como dicen que un vestido negro es un básico en el guardarropa de una mujer, estas camisetas que enlistamos a continuación han sido las más usadas por aquellos fans del rock; son su equivalente al vestido negro: nuca debe faltar y puede encajar en casi cualquier ocasión; tanto que ya no importa el estilo de quien la porte, pues en automático te pone “en onda”, como supuesto rocker aunque muchas veces no tengas ni una referencia de lo que vistes. Guns N’ Roses  
 
 
 
 Rolling Stones  
 
 Led Zeppelin  
 
 The Doors  
 
 Joy Division  
 
 Kiss  
 
 
 Metallica  
 
 Nirvana  
 
 Pink Floyd  
 
 AC DC