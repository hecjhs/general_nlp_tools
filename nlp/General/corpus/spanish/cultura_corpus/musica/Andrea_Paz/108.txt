Canciones que hablan sobre marihuana

 Calvin Cordozar Broadus Jr, Snoop Lion, Snoop Doggy Dogg, Tha Doggfather, The Bigg Boss Dogg, Bigg Snoop Dogg y The D-O Double G son unos de los tantos nombres con los que se le conoce a Snoop Dog, el rostro de la marihuana. En la música, hay otros tantos nombres que están directamente relacionados con la hierba con ene cantidad de beneficios, en esta ocasión, nos concentramos en los que frece para propiciar la creatividad y para invocar a las musas de inspiración. Snoop Dogg ha dado un paso más y no sólo ha hecho de la marihuana una de las principales características en sus canciones, recientemente ha anunciad que el próximo octubre lanzará  una página web dedicada exclusivamente a la cannabis.   La página se llamará merryjane.com y será una enciclopedia para todo aquel que quiera saber más sobre la  planta. El sitio estará apoyado por artistas como Seth Rogen y Miley Cyrus, entre otros.Antes que eso suceda y podamos conocer todo sobre la nueva plataforma, enlistamos algunas canciones que tienen como protagonista a la marihuana, o bien, que le dan un significado más profundo al 4/20. Ray Charles – Let’s Got To Get Stoned  Cuando salió a la luz en 1966, rápidamente alcanzó los primeros lugares de popularidad en las listas de los Estados Unidos, pues coincidía con la salida de rehabilitación del cantante por su adicciones a las drogas durante dieciséis años. 
   
  
  Snoop Dogg- Gin and Juice  Cuando se habla de Snoop Dogg, en automático nos vemos envueltos en una nube de humo proveniente de todos los porros que se fuma al día. “Gin and Juice” es el segundo sencillo del Snoop Doggy Dogg, su álbum debut con el que creó el ya insuperable Doggystyle. La canción además, rinde homenaje a Seagrams y la ginebra Tanqueray. 
    
  Bob Marley – Kaya Es casi requisito que el reggae hable sobre marihuana, y quién mejor que Bob Marley como ejemplo de ello. De hecho, Kaya una de las muchas maneras con las que los rastafaris denominan la hierba y aunque no se trata de una canción sobre la legalización, describe lo importante que es la kaya para los rastas, tanto como el agua o la comida. 
    
  Addicted – Amy Winehouse  Una más de las joyas de “Back to Black” y aunque cuando la escuchamos por primera vez no era más que un relato en tono sensual y provocativo sobre un porro, hoy tiene todo el sentido del mundo. 
    
 
 
Beastie Boys – Hold it Now Hit It  Una más de las grandes canciones de los BB, que entre tantos elementos que se involucran en la historia hay algunas palabras clave como “dress so fly” que revelan el verdadero sentido y tema principal del track. 
    
  Nirvana – Moist Vagina  Fue grabada para el álbum In Utero, y el título original era “Moist Vagina And Then She Blew Him Like He’s Never Been Blown, Brains Stuck All Over The Wall” pero se modificó porque era demasiado controversial. La canción no se trata de mujeres o sexo, sino sobre drogas, específicamente marihuana. Kurt Cobain dijo que esta canción se explica cómo la marihuana es el único que tiene para comer. 
    
 
  The Beatles – Get Back Sabemos que Bob Dylan fue quien inició a los Beatles en el camino de la hierba para que entonces ellos se siguieran de largo. En muchas de sus canciones hay algunas de las pistas en las que sabemos la influencia que la marihuana tenía en ellos, por ejemplo, en este track escuchamos “J o-Jo was a man who thought he was a loner, but he knew it couldn’t last. Jo-Jo left his home in Tucson, Arizona, for some California grass”. 
    
  Afroman – Because I Got High  “Because I Got High” podríamos traducirlo o entenderlo como “Porque estaba drogado”. La canción fue excesivamente popular en EE. UU, el Reino Unido y Australia. La letra va sobre un caso en el que el consumo de marihuana degrada la calidad de vida pero contada de forma “amigable”. 
    
  Sixto Rodríguez – Sugarman Esta es sin duda su canción más célebre, misma con la que abre el documental sobre su vida y carrera, además fue una de las más populares en la radio de Sudáfrica. “Sugarman” podría pensarse que es una canción que habla sobre amor… y en realidad sí, pero el amor hacia la marihuana y cocaína, todo se una forma poética como sólo Sixto sabía hacerlo. 
    
 
  Black Sabbath – Sweet Leaf  Antes de probar otras tantas sustancias, los integrantes de Black Sabbath comenzaron como muchos, con esta hierba mágica en busca de inspiración, no por nada en la canción escuchamos como Ozzy menciona: “tú me presentaste a mi mente” o “mi vida estaba vacía hasta que te conocí”. En el fondo y como todos sabemos, el track es una carta de amor a la marihuana. 
   
 Quizá te interese: Cosas que no sabías sobre Snoop Dogg