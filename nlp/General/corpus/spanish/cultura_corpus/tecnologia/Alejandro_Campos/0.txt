La página que te dice el día en que te vas a morir

 Posiblemente el título te remita a las páginas fraudulentas con sonido sumamente molesto que aseguraban poder predecir tu fecha de muerte a principios de los 2000. La magia de estas páginas no llegaba hasta ahí, pues incluso tenían la capacidad de saber cómo morirías con base en tus gustos, miedos y fobias. Así, los adolescentes estimaban una prematura muerte a causa de la tostadora en la bañera o un terrible accidente de avión, pero el asunto no pasaba de un momento de risa, un profundo miedo o un virus en la computadora Windows 98 de la escuela. Sin embargo, ahora, con las posibilidades tecnológicas y los estudios demográficos, es posible conocer el día en que vamos a morir.  Fotografía de Cathrine Ertmann 
 A diferencia de aquellas páginas del pasado y que evocan la nostalgia de más de uno de nosotros, la página Population.io permite a los usuarios conocer la fecha precisa en que abandonará el mundo que poco a poco estamos matando con mucha más seriedad que antes. La iniciativa nació de la mente de Wolfgang Fengler, un economista alemán del Banco Mundial que se propuso acercar el estudio de la población humana a un público más amplio. Así, a partir de números que intervienen con factores socioeconómicos del mundo actual, quien ingrese a la página puede “explorar una nueva perspectiva de la vida para encontrar su lugar en el mundo del hoy y el mañana”. 
  
 Tras ingresar tu fecha de nacimiento, tu género y el país donde naciste, el algoritmo de la página te mostrará información demográfica respecto a tus datos. A partir de un cruce de información con el Banco Mundial y datos de las Naciones Unidas, Population.io calcula tu esperanza de vida y determina cuántos años te quedan de vida. 
 Además de conocer ese dato, la página te dice en tiempo real cuántas personas hay en el mundo, y entre ellas, qué número ocupas tú. A partir de ese dato puedes saber qué porcentaje del mundo es mayor y menor que tú, y específicamente en tu país de origen. Todo se calcula con base en datos disponibles de la población mundial que las Naciones Unidas publican cada cinco años, la base de datos se remonta hasta 1950 y se utiliza la población media proyectada para la esperanza de vida de 200 países. 
   
 Si tu espíritu curioso persiste, la página también te puede decir cuántas personas comparten tu fecha de nacimiento en el mundo y en tu país, e incluso, cuántas comparten tu hora de nacimiento. También puedes comparar la esperanza de vida que tienes en tu país y la que podrías haber tenido si hubieras nacido en otro país… o en otro momento histórico. ¿Qué esperanza de vida tendrías de haber nacido en 1970? 
 Descubre el día que vas a morir AQUÍ 
 Fotografía de Cathrine Ertmann 
 Descubre el día que vas a morir AQUÍ 
 Fuente: Population.io 
 – Te puede interesar: 
 15 páginas de Internet que te harán más inteligente 
 20 páginas donde puedes aprender algo nuevo cada día 
 Páginas de Internet para aprender y saber de arte