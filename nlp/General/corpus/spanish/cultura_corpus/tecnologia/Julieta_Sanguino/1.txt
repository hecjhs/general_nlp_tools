Maneras efectivas y divertidas para desarrollar tu habilidad mental

 
 Algunos nacen dotados con la sabiduría a flor de piel, no deben esforzarse en lo más mínimo y cautivan al mundo a través de la sofisticación casi innata que tienen, tal como Demian de la novela de Hermann Hesse, un chico que desde pequeño tenía gran talento para descubrir el mundo sin ayuda, no se trataba de aquél que pasaba horas en la biblioteca, ni del que que disfrutara presumir de su inteligencia, sino simplemente un chico sensible que amaba el mundo de una manera intensa. 
 Nosotros, los que no nacimos con ese talento innato, debemos hacer mil y un cosas para lograr alcanzar la máxima sabiduría. Los que tenemos que optar por remedios viejos, nuevos y algunos que nadie imaginó para desarrollar nuestra mente y con ella, el espíritu. Algunos remedios para desarrollar nuestra habilidad mental son bastante sencillos y un tanto lógicos, pero otros no podrás creer que te ayudarán. 
 – Armar un rompecabezas 
 Los rompecabezas ayudan a que tu mente esté más alerta todo el tiempo. Nunca se volverá una rutina porque siempre encontrarás diferentes imágenes que reten tu intelecto. Si ya intentaste todos los modelos, ahora puedes seguir con los rompecabezas en tercera dimensión o alguno más complejo. También puedes optar por crucigramas o acertijos.    
 – Jugar La Fortuna del Saber 
 Un juego gratuito para celular que te permitirá aprender cosas que de ninguna otra manera te atreverías a preguntar. Con seis niveles que te permiten obtener un boleto para participar en alguno de sus concursos y ganar diferentes premios en efectivo, que van de 5 mil hasta 400 mil pesos. Descárgala para iOS o Android . 
  
 – Juegos de mesa 
 Los juegos que requieren de estrategia, pensar y son competitivos, son una manera excelente para incrementar tus habilidades lógicas y tu empatía. 
  
 – Hacer mandalas 
 Crear patrones hermosos puede ser el mejor aliciente para tu mente. No necesitas mucho, los puedes hacer en la arena, con algunos gises e incluso como decoración para tu hogar. Cada trazo significará una idea que baja al pensamiento más básico para crear algo bello y así saber un poco más de cómo funcionan tus instintos artísticos básicos. 
  
 – Colorear libros de creatividad 
 Si coloreas libros, existen cinco beneficios claros: tu cerebro experimentará un estado de meditación que no es fácil de conseguir; el estrés y la ansiedad disminuirán intensamente; los pensamientos negativos se irán y de pronto verás lo positivo de eso que te molestaba tanto; tu mente se concentra en el presente y lograrás plena atención; y por último, es una excelente forma de promover tu creatividad. 
  
 – Adopta hobbies diferentes 
 Emprende nuevas actividades. Esto te mantendrá en un proceso de aprendizaje constante y tu mente siempre se sentirá bien y con mucha intensidad. Ya sea pintura, danza, aprender otros idiomas, correr, aprender a tocar un instrumento, todas mantendrán tu mente al máximo. 
  
 – Lee más libros, ve más televisión, asiste más al cine 
 Haz que tu mente se entretenga con películas, libros o televisión, siempre y cuando lo que veas te mantenga pensando. 
  
 – Rompe las rutinas 
 Conscientemente, intenta romper esos hábitos que tienes arraigados sólo por un momento. Cena algo que nunca habías imaginado, toma una ruta diferente al trabajo. 
  
 – Transmite tu conocimiento 
 Esto se convierte en una manera de reflexionar lo que sabes y así interiorizas mejor todo tu conocimiento, porque en ocasiones ni siquiera nosotros somos capaces de dimensionar todo lo que se oculta en nuestra mente. 
  – 
 La Fortuna del Saber te enseñará todo lo referente a cultura general y además, te hará ganar. Descarga la aplicación aquí  y sigue su página de  Facebook  para saber mucho más. Está disponible para sistema iOS, Android y es totalmente gratis. No lo dudes, aprende, diviértete y gana mucho más que sólo conocimiento.