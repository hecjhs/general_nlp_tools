50 preguntas que te ayudarán a saber cuál es tu tipo de personalidad

 
 Dicen que los primeros años de vida son fundamentales para un ser humano puesto que durante esta etapa las vivencias que tengamos, sean buenas o malas, moldearán el carácter que tendremos en un futuro. Es decir, de pequeños somos como una hoja en blanco en la que poco a poco se escribirán las instrucciones con las que debemos comportarnos cuando lleguemos a la etapa de madurez. Por si los golpes exteriores del entorno no fueran suficientes, la genética también juega un papel muy importante en lo que somos y eso puede complicar las cosas aún más. Si quieres saber otros factores que afectan la personalidad, da click aquí . 
  
 Los factores heredados son los elementos que recibimos de nuestros padres, pueden ser las capacidades o limitaciones psíquicas, los defectos o cualidades físicas, así como algunos gustos, hábitos o pensamientos. Sí, cada uno de nosotros somos individuos independientes, pero tendremos que cargar toda la vida con nuestros orígenes. Por ejemplo, puede que la sociedad determine que los hombres más atractivos sean aquellos que tengan una gran altura, pero la carga genética determine una persona sea de estatura baja. En este caso, si no se trabaja con la seguridad y el autoestima de la persona puede que él mismo sufra un rechazo de sí, ocasionándole problemas emocionales. 
 Lo mejor sería que el mundo nunca nos limitara y que en verdad seamos libres de ser nosotros mismos, pero por desgracia siempre existirá una tendencia o moda que discrimine a las personas. Para combatir de la mejor manera la realidad hay que estar consciente de quiénes somos con estas sencillas preguntas. Claro, deben ser contestadas de manera honesta, de lo contrario, la verdadera personalidad nunca se sabrá. 
 – 
  
 1. ¿Tienes dificultades para presentarte con otras personas? 
 2. ¿Sueles internarte en tus pensamientos al grado que ignoras o te olvidas del exterior? 
 3. ¿Puedes relajarte y concentrarte incluso cuando el ambiente es estresante? 
 4. ¿Rara vez haces algo por simple curiosidad? 
 5. ¿El entorno de tu trabajo y casa es ordenado? 
 6. ¿Te sientes superior a otras personas? 
 7. ¿Ser organizado es más importante que ser adaptable? 
 8. ¿A veces sientes que debes justificarte con las otras personas? 
 9. ¿Te importa ser el centro de atención? 
 10. ¿Te consideras a ti mismo como una persona práctica a una creativa? 
 Quien construyó las bases de la teoría de la personalidad fue el teórico Carl Jung . Él aceptó que nacemos con una herencia psicológica y biológica, la cual determinará nuestro desenvolvimiento en la sociedad. Su gran aportación llegó cuando dijo que había dos tipos de personas: los introvertidos y los extrovertidos. En sencillas palabras, los primeros son tímidos, se retraen de la compañía, mientras que los extrovertidos son altamente sociales. 
 11. ¿Las personas rara vez pueden hacerte enojar? 
 12. ¿Se te dificulta hablar de tus sentimientos con los otros? 
 13. ¿Tu estado de ánimo puede cambiar muy rápido? 
 14. ¿Te preocupas muy pocas veces de cómo tus acciones pueden afectar a los otros? 
 15. ¿Prefieres improvisar a perder el tiempo planeando? 
 16. ¿Te parece más atractivo un buen libro o una película en casa que cualquier evento social? 
 17. ¿Rara vez te dejas llevar por las ideas o fantasías? 
 18. ¿Si alguien no te responde un mensaje rápidamente comienzas a preocuparte? 
 19. ¿Tus sueños tienden a concentrarse en el mundo real? 
 20. ¿Te cuesta mucho tiempo poder integrarte a las actividades sociales de tu trabajo? 
  
 Para puntualizar en la personalidad, agrupó las energías de las personas en los apartados de pensamiento, sentimiento, sensación e intuición. El pensamiento introvertido se caracteriza por estar interesado en las ideas en lugar de los hechos, le importa la realidad interior y pone poca atención en las personas. En el aspecto sentimental, es una persona ligeramente reservada, pero simpática y comprensiva con los amigos. Se nutre de sus sensaciones internas y por lo mismo, tiene tendencias artísticas. Su intuición hace que esté interesado constantemente en el futuro, más que en el presente. En general son soñadores. 
 21. ¿Tu naturaleza es de improvisar, más que de planear? 
 22. ¿Te controlan tus emociones en lugar que tú las controles a ellas? 
 23. ¿Eres una persona relativamente tranquila y callada? 
 24. ¿Sueles pensar en las razones de la existencia humana? 
 25. ¿La lógica es más importante que el corazón cuando se deben tomar decisiones importantes? 
 26. ¿Mantener las opciones abiertas es más importante que tener todo preparado? 
 27. ¿Rara vez te sientes inseguro? 
 28. ¿Ser justo es más importante que ser cooperativo cuando se trabas en equipo? 
 29. ¿Te sientes con más energía después de haber convivido con un grupo de personas? 
 30. ¿Crees que eres una persona sentimentalmente estable? 
  
 En el sentido opuesto, el pensamiento del extrovertido está interesado en los hechos del entorno, aunque reprime sus emociones y sentimientos, lo que ocasiona un descuido en sus relaciones sentimentales. En el lado sentimental está interesado en las relaciones humanas, se adapta muy bien a cualquier ambiente social. Dice Jung que pone énfasis en las percepciones que le provocan las realidades tangibles y por eso está constantemente buscando el placer. Dicen que el extrovertido es un aventurero y vierte todo su interés y energía en un objetivo hasta que lo consigue, después tiene la necesidad de nuevos retos. 
 31. ¿Por lo general estás motivado y lleno de energía? 
 32. ¿Prefieres perder una discusión para evitar que los otros se molesten? 
 33. ¿Tus viajes son muy pensados? 
 34. En una discusión, ¿la verdad está por encima de los sentimientos? 
 35. ¿Sueles sentir envidia de los otros? 
 36. ¿Tiendes a perderte en tu pensamientos cuando caminas por las calles? 
 37. ¿Dejas que los otros influyan en tus acciones? 
 38. ¿Tu principal objetivo al salir de noche es conocer a otras personas? 
 39. ¿Tienes un calendario con tus citas y te atienes a él? 
 40. ¿Piensas que todas las opiniones deben ser respetadas a pesar de que sean correctas o incorrectas? 
  
 Resumiendo, las personas introvertidas prefieren las actividades en solitario a las que son en grupo. Son personas muy sensibles con el mundo exterior y sus sentidos siempre están recibiendo estímulos del entorno. Son muy imaginativos, de mente abierta y curiosos. Prefieren la novedad ante la estabilidad. Se centran en los significados ocultos y en las posibilidades futuras. Las personas extrovertidas tienen los “pies sobre la tierra”,  son muy prácticos y pragmáticos. Tienden a tener hábitos muy inflexibles y se concentran que lo que sucede alrededor. Ponen la lógica por encima de los sentimientos. 
 41. ¿Pierdes cosas con frecuencia? 
 42. ¿Te llamas a ti mismo soñador? 
 43. ¿Se te dificulta relajarte cuando estás acompañado por muchas personas? 
 44. ¿Te preocupa lo que dicen los demás de ti? 
 45. Si la habitación está llena, ¿prefieres mantenerte cerca de las paredes que en el centro? 
 46. ¿Tiendes a procrastinar tus actividades hasta que ya no tienes tiempo y las haces? 
 47. ¿Sientes ansiedad en situaciones estresantes? 
 48. ¿Piensas que es mejor agradarles a los demás que tener poder sobre ellos? 
 49. ¿Te interesan las cosas no convencionales en la literatura, cine o música? 
 50. ¿Con frecuencia tomas la iniciativa en eventos sociales? 
  
 ¿Cuál crees que sea tu personalidad? La página 16 personalities propone que las personas introvertidas y extrovertidas no son la única división de personalidad, pues también tiene una esencia analítica, diplomática, protectora y aventurera. Cada una de ellas tiene una característica en especial. Por ejemplo, en el grupo de los analistas están quienes “dirigen”, que son aquellos que siempre encuentran un camino por seguir o lo crean. Los “lógicos” son inventores e innovadores del conocimiento. Quienes tienen un plan o estrategia para la vida son los “arquitectos” , mientras que los curiosos pensadores que no se resisten a los retos intelectuales son los “polémicos” . 
 En el rubro de los diplomáticos , se encuentran los “abogados”, que son callados pero inspiran la justicia con sus fuertes ideas de equidad. Los “mediadores” son personas altruistas que siempre ayudarán a las buenas causas. Quienes son carismáticos e hipnotizan con las palabras son los  “protagonistas”  y  los “compañeros” son las personas entusiastas y creativas para hacer reír a los demás. 
  
 En el grupo de los protectores están los “ejecutivos” , que siempre están administrando los bienes materiales. Los “serviciales” son extremadamente cuidadosos con el trato de las otras personas y les gusta ayudar. Quienes se dedican a velar por el bienestar de los otros son los “defensores” . Por último, en el lado aventurero se encuentran los “virtuosos”, que son aquellos que tienen una facilidad para usar cualquier herramienta u objeto para sacarle el mayor provecho. El “empresario” es listo, energético y muy perceptivo, mientras que el “artista” es espontáneo, energético y entusiasta. 
 – Después de saber cuál es tu personalidad a partir de la teoría de Jung, quizá quieres saber la relación que tiene la música con lo que eres, llévate una gran sorpresa dando click aquí . Hay otras teorías sorprendentes acerca de tu carácter, si no lo crees, descubre tu personalidad según la ubicación de tus lunares. Entre tantas preguntas también puedes haberte cansado de todo, en ese caso, relájate y conoce los mejores discos de lo que va del año dando click aquí . 
 * Referencia: 
   16 personalities  
 BBC