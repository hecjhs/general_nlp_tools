Juegos en línea que te ayudarán a relajarte y calmar tus nervios

 Salir del trabajo, caminar después de haber estado sentado un promedio de 6 horas, tomar el transporte público sólo para volverte a sentar. Saber que a pesar de que estás a 10 kilómetros de tu casa, el tiempo del trayecto equivaldrá a haber viajado dos horas a 100 kilómetros por hora para haber llegado posiblemente a Querétaro. Sumando, son 4 horas diarias en trasladarte 10 ó 15 kilómetros. Al llegar a casa, sigues teniendo responsabilidades como hijos, tarea, pareja, mascota, etc… 
   
 Después del ajetreo del día, muchas personas eligen relajarse leyendo un libro a media luz, tomando un café o té; otros quizá prefieran acostarse un rato en el sofá de la sala, prender su televisor y olvidarse del estrés por medio un programa televisivo o una película en Netlfix. Puede ser que existan personas que definitivamente no tengan ya energías para realizar una actividad recreativa después del día laboral, o quienes definitivamente todos los días se vitaminen, y salgan a la hora que salgan de la oficina o lugar de trabajo, todavía opten por ir dos horas al gimnasio a trabajar – sí, otra vez a trabajar – su musculatura. 
  
 Si definitivamente no perteneces a ninguno de esos dos grupos de personas, quizá te interese conocer una serie de juegos online que te ayudarán a sentirte más relajado y centrar tu atención en tareas tan sencillas como acumular burbujas para olvidarte de el día tan complicado que tuviste. Esta lista de pequeños juegos te ayudará a sentirte más aliviado. 
 1.- Bells : 
  
 En este juego tomas el papel de un pequeño conejo, el cual tiene la misión de escalar lo más alto posible usando de apoyo las campanas que van apareciendo. Sin más complejidad que esa, lo que hace este juego relajante, es la música y los bellos gráficos. Asimismo el control de juego es intuitivo para que al jugarlo no te rompas la cabeza. 
 – 
 2.- Auditorium : 
  
 Este juego es un poco más complejo. La dinámica consiste en ir controlando una serie de halos de luz de distintos colores para que puedan ir rebotando en una serie de sensores. Para poder jugar completamente este juego, es necesario pagar, pero la versión demo es muy entretenida. 
 – 
 3.- Fishing Girl : 
  
 Ir de pesca es una actividad que nos permite estar en estado contemplativo. Esperar un largo rato para que algo pique nos da la oportunidad de pensar sobre lo que deseemos. Este juego es una emulación muy sencilla e intuitiva en la que el objetivo es pescar, y con sólo un click lanzas la línea al agua esperando pescar algo. 
 – 
 4.- Echo Genesis : 
  
 Más que un juego, es una obra de arte digital en la que a partir de navegar por distintos lugares, puedes interactuar con el entorno. Recuerda a los juegos para niños en los que al presionar una planta, emitía un sonido y se movía. 
 – 
 5.- Drift : 
  
 Al igual que el primer juego de esta lista, tomas el papel de un conejo el cual debe saltar entre algunos globos para ir ganando puntos. Lo relevante de este juego son las gráficas color pastel que pueden hacer que te sientas en un pequeño cuento de hadas. Visualmente es muy bueno. 
 – 
 6.- TanZen : 
  
 Los anteriores juegos están disponibles en línea para jugar. Este juego es descargable y consiste en una serie de rompecabezas sencillos con lindos diseños. 
 – 
 7.- Word Search : 
  
 Quien diga que buscar palabras en un crucigrama no relaja, está totalmente equivocado. Descargando este juego, puedes pasar horas buscando palabras de distintos temas como deportes o películas. 
 – 
 8.- Jet Worm : 
  
 De la franquicia Wroms , este juego consiste en la misma dinámica de todos, arrojar gusanos u objetos. Sigue siendo un clásico que recuerda a algunos juegos de computadora de los años 90. 
 – 
 9.- Flappy Monster : 
  
 Hace unos años salió una aplicación llamada Flappy Bird , que causó sensación por lo adictiva que era. El sistema es sencillo, tocar con el dedo la pantalla para que el pájaro se eleve y debes esquivar los obstáculos. Esta versión tiene nuevos niveles, ya no sólo son las tuberías verdes que recordaban a Mario Bros . 
 – 
 10.- Nintendo Duck Hunt : 
  
 La emulación del clásico juego de la pistola naranja de Nintendo está disponible y gratis para descargarse. Te reencontrarás con el perro burlón. 
 – 
 Esta lista contiene cinco  juegos en línea ; que a pesar de que ya casi no se producen, la sencillez de éstos puede ayudar a perderte un momento. Asimismo, los otros cinco son descargables y varios emulan juegos ya conocidos que por una parte pueden relajarte, y por la otra hacerte perder el tiempo por lo divertidos que pueden ser. Así ya no sólo llegarás a tu casa a ver las últimas actualizaciones de tus amigos en Facebook . 
 *** 
 Te puede interesar: 9 aplicaciones que te ayudarán a dormir mejor.