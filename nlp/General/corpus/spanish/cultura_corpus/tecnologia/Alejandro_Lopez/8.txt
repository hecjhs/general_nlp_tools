15 datos inteligentes que debes conocer antes de los 30 para entender el mundo

 
 A lo largo de la niñez, una curiosidad  casi innata en la humanidad se cuestiona cada fenómeno que percibe en su entorno. Nada más aprende a hablar, el hombre comienza incesantemente una búsqueda de respuestas sobre todo lo que le rodea. Preguntas que pueden parecer completamente burdas para los adultos, son formuladas con total seriedad y extrañeza por los más pequeños, especialmente durante la etapa del “por qué”. 
 Dependiendo de la satisfacción con las respuestas y el compromiso de los adultos por estimular su interés, los niños siguen en mayor o menor medida investigando sobre su entorno. Conforme pasan los años, las interrogantes se transforman sin perder su esencia. Mientras perdure la imaginación, la curiosidad se mantendrá viva y es esa llama la que arde en aquellos que eligen dedicar su vida a descubrir grandes misterios sobre todo lo que somos y lo poco que sabemos. 
 Aprender sobre el cerebro humano, el Universo conocido, la edad de la Tierra, el origen de la vida, el ser humano, es una obligación para entender el estado actual o pasado de todas las cosas, además de adquirir un horizonte de pensamiento mucho más extenso y profundo sobre nuestra existencia. Estos son 15 datos inteligentes que debes conocer antes de los 30 para entender de qué va y cómo está formado ese ínfimo espacio y tiempo al que llamamos vida: 
 1. En promedio, una cadena de ADN con la información genética de una persona podría estirarse un equivalente de la distancia del Sol a Plutón cerca de 17 veces 
  
 
 2. El ser humano adulto está formado de 206 huesos, mientras que los recién nacidos poseen 270 que se van uniendo conforme crecen. 
 3. Si el Sol fuera del tamaño de una pelota de playa, Júpiter tendría el de una pelota de golf y la Tierra el equivalente a una cabeza de alfiler. Los tamaños y las distancias en el Universo son representadas burdamente en imágenes y monografías. 
  
 
 4. En promedio, una persona camina el equivalente a cinco vueltas al planeta Tierra durante toda su vida. 
 5.Con cerca de 2 mil kilómetros de largo, la Gran Barrera de Coral es el organismo vivo más grande en la Tierra. 
  
 
 6. Una célula tarda alrededor de 60 segundos en completar una vuelta completa alrededor del cuerpo. 
 7. El Universo conocido tiene más de 50 mil millones de galaxias. Dentro de cada una existen cerca de mil billones de estrellas como el Sol, alrededor de las cuales orbita un sistema planetario. Estadísticamente, es casi imposible que estemos solos. 
  
 8. Desde el final de la Segunda Guerra Mundial, Estados Unidos ha invadido un país o entrado en guerra en 33 ocasiones. 
 9. La escritura y la lectura no son capacidades innatas del cerebro humano, sino complejos desarrollos cognitivos, de ahí que sea tan complicado para los niños aprender a leer. 
  
 
 10. Los elementos indispensables para la vida son el oxígeno, carbono, hidrógeno, nitrógeno, calcio y fósforo. 
 11. El ADN es el lenguaje de la vida. Todos los organismos vivos comparten un ancestro común en su genoma. 
  
 12. Actualmente, 62 personas poseen la misma riqueza que la mitad de la población mundial. 
 13. Ninguna representación cartográfica es exacta en tamaños ni distancias. Es imposible proyectar una esfera en una superficie de dos dimensiones como un mapa. 
  14. Según el modelo del calendario cósmico, que expresa proporcionalmente el tiempo transcurrido desde el Big Bang hasta el presente a partir de un año entero, el homo sapiens apareció en los últimos diez minutos del 31 de diciembre. 
 15.  La estructura del Universo es asombrosamente parecida a la del cerebro, por lo que podrían encontrarse las claves de uno en el otro y viceversa. 
  
 – ¿Quieres conocer más hechos sorprendentes? Mira estos  73 datos para enamorarte aún más del misterio del espacio . ¿Estás seguro de que todo lo que sabes es totalmente real? Descubre si tienes razón leyendo los  10 mitos del espacio que todos creíamos reales .