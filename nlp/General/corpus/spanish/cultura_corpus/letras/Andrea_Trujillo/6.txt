10 libros de terror para nunca más conciliar el sueño

 
 Sentir cómo se eriza la piel desde la espalda baja hasta la nuca es una sensación que difícilmente puede ser provocada por las letras impresas en un papel. Sin embargo, algunos autores han sido capaces de eso, y de sembrar en nosotros el miedo de apagar las luces, o de hacernos pegar un salto cuando escuchamos un ruido venir desde lejos. Escribir ya supone la habilidad de saber utilizar y acomodar las palabras con cierta gracia, pero lograr que el horror se expanda no sólo en la mente del lector, sino el espacio que éste habita, conlleva un excepcional talento. 
 Algunos autores, como Stephen King o H. P. Lovecraft lograron dominar con gran maestría el género del terror, aterrizando en las páginas de sus libros sus fantásticos y terroríficos escenarios. Hemos compilado diez títulos de libros de terror , que aunque te harán sentir encrispar cada uno de los vellos de tu cuerpo, la intriga será que lo que te impida leer hasta el punto final. Ponte cómodo y prepárate para nunca más conciliar el sueño. 
 – “No tengo boca y debo gritar” Harlan Ellison 
 Publicado por primera vez en 1967, Ellison se inspiró en la ilustración de una muñeca de trapo sin boca, realizada por su amigo William Rotsler, para titular y escribir el relato. La historia nos sitúa en un futuro postapocalíptico después de una catástrofe nuclear. Solamente cinco humanos sobrevivieron, y se verán condenados a vivir una existencia horrible en manos de la computadora militar AM. Puedes leerlo aquí. 
 I Have No Mouth, and I Must Scream. Dennis Smith. Ilustración para IF en 1967, acompañó al relato de Ellison. 
 “ODIO. DÉJENME DECIRLES TODO LO QUE HE LLEGADO A ODIARLOS DESDE QUE COMENCE A VIVIR MI COMPLEJO SE HALLA OCUPADO POR 387.400 MILLONES DE CIRCUITOS IMPRESOS EN FINISIMAS CAPAS. SI LA PALABRA ODIO SE HALLARA GRABADA EN CADA NANOANGSTROM DE ESOS CIENTOS DE MILLONES DE MILLAS NO IGUALARIA A LA BILLONESIMA PARTE DEL ODIO QUE SIENTO POR LOS SERES HUMANOS EN ESTE MICROINSTANTE POR TI. ODIO. ODIO. AM dijo esto con el mismo horror frío de una navaja que se deslizara cortando mi ojo. AM lo dijo con el burbujeo espeso de flema que llenara mis pulmones y me ahogara desde mi propio interior. AM lo dijo con el grito de niñitos que fueran aplastados por una apisonadora calentada al rojo. AM me hirió en toda forma posible, y pensó en nuevas maneras de hacerlo, a gusto, desde el interior de mi mente”. 
 – “Penpal” Dathan Auerbach 
 “Penpal” comenzó como una serie de pequeñas e interconectadas historias que se publicaban en un foro de Internet. Posteriormente, fue adaptado a ilustraciones, grabaciones, cortometrajes y ahora a novela. Puedes leerlo aquí (en inglés) y escucharlo en el video a continuación. 
  
 “I had heard the footsteps but was too far gone to be woken up by them, and when I was awoken it wasn’t from the sound of footsteps or a nightmare, but because I was cold. Really cold. When I opened my eyes I saw stars. I was in the woods. I sat up immediately and tried to figure out what was going on. I thought I was dreaming, but that didn’t seem right, though neither did me being in the woods”. 
 – “El Gran dios Pan” Arthur Machen 
 Fue la primera novela de Arthur Machen, publicada en 1890. “El Gran dios Pan” fue aborrecido, criticado y denunciado como un libro brutal y repugnante debido a su decadente estilo y extraña sexualidad que se desprende a lo largo del a historia. Léelo aquí.  
 “Mientras los dos hombres conversaban, un ruido confuso de gritos había aumentado gradualmente en intesidad. El ruido se elevaba desde la parte este y cobraba fuerzas en Picadilly, acercándose más y más, como un torrente de sonido; agitando las calles usualmente tranquilas, y haciendo de cada ventana el marco para una cara, curiosa o excitada. Los gritos y las voces reverberaban a lo largo de la silenciosa calle donde vivía Villiers, haciéndose más claras a medida que avanzaban, y mientras Villiers hablaba, la respuesta subió desde la acera…”. 
 – Relatos y poemas Edgar Allan Poe 
 A Poe se le ha considerado maestro del género del terror y logra contar con gran detalle en relatos muy cortos las más escalofriantes historias. Lee sus cuentos aquí . 
  
 “¡Es cierto! Siempre he sido nervioso, muy nervioso, terriblemente nervioso. ¿Pero por qué afirman ustedes que estoy loco? La enfermedad había agudizado mis sentidos, en vez de destruirlos o embotarlos. Y mi oído era el más agudo de todos. Oía todo lo que puede oírse en la tierra y en el cielo. Muchas cosas oí en el infierno. ¿Cómo puedo estar loco, entonces? Escuchen… y observen con cuánta cordura, con cuánta tranquilidad les cuento mi historia…”. —”El Corazón Delator“ 
 
 “La chica de al lado” Jack Ketchum 
  En una calle sin salida, en un oscuro y húmedo sótano de la casa Chandler, Meg y Susan, están cautivas a manos de una tía que está cayendo progresivamente en la locura.  Puedes leerlo aquí . 
  ” Pude ver cómo se le tensaban los músculos del cuello y los que rodeaban la columna vertebral mientras trabajaba. Oí ruido de uñas. Me levanté y me acerqué a ella. Estaba escarbando. 
 Estaba escarbando con los dedos en el suelo de cemento, allí donde se encontraba con la pared. Cavando un túnel para salir. Se le escapaban pequeños ruidos de agotamiento. Se le habían roto las uñas y le sangraban, ya había perdido una de ellas, las yemas de sus dedos también sangraban, y su sangre se mezclaba con la tierra que escarbaba del cemento debilitado en una extraña aleación de ambas sustancias. Su última negativa a someterse. Su último acto de desafío. La voluntad que surgía de un cuerpo derrotado, para forzarlo a convertirse en piedra sólida”. 
 – “Horror en Amityville” Jay Anson 
 Lo que hace aún más terrorífico este libro, es que los relatos planteados en él son basados en historias reales.  En la novela se trata el caso de una casa que fue presuntamente poseída por presencias extrañas a mediados de los 70. Puedes descargarlo aquí . 
  “George went back down the stairs and warily approached the storage closets. As he stood before the shelving that hid the small room, the odor became stronger. Holding his nose, George forced open the paneling and shone his flashlight around the red painted walls. The stench of human excrement was heavy in the confined space. It formed a choking fog. Nauseated, George’s stomach began to heave. He had just time enough to pull the panel back into place and shut out the mist before he vomited, fouling his clothes and the floor”. 
 
 “Otra vuelta de tuerca” Henry James 
 Perturbadora, intrigante, y a veces asfixiante es la atmósfera de esta novela de terror psicológico que solo recomendamos leer si no vas a dormir solo.  Léelo aquí . 
  
 “El caso, debo mencionarlo, consistía en una aparición en una casa tan antigua como la que nos acogía en aquellos momentos, una aparición terrorífica a un niño que dormía en el mismo cuarto que su madre, a quien despertó aterrorizado; pero despertarla no disipó su terror ni lo alivió para recuperar el sueño, sino que, antes de haber conseguido tranquilizarlo, también ella se halló ante la misma visión que había atemorizado al niño”. 
 – “It” Stephen King 
 Popularizada por la cinta que lleva el mismo nombre, la novela nos cuenta la historia de un grupo de chicos que son aterrorizados por un malvado monstruo capaz de cambiar de forma. Puedes leerlo aquí . 
  
 “Se llevó la mano a la boca, con ojos dilatados de horror. Por un momento, sólo por un momento, creyó haber visto que algo se movía allá abajo. Tuvo súbita conciencia de que el pelo le caía sobre los hombros en dos gruesos mechones, cerca, muy cerca del desagüe. Algún instinto la obligó a erguir la espalda para apartar de ahí su pelo. Miró alrededor. La puerta del baño estaba firmemente cerrada (…) –¿Quién eres? -preguntó al lavabo, en un susurro…”. 
 – “El bebé de Rosemary” Ira Levin 
 Adaptado al cine por Roman Polanski en 1968, el libro cuenta la historia de una pareja que decide vivir en un apartamento del que se escuchan terribles historias. Descárgalo aquí . 
  
 “Abajo había un enorme salón de baile, en uno de cuyos lados una iglesia ardía violentamente y en el otro un hombre con una barba negra la estaba mirando fijamente. En el centro había una cama. Se recostó en ella, y, de repente, se vio rodeada por diez o doce personas desnudas, hombres y mujeres, Guy entre ellos. Eran personas mayores, las mujeres grotescas y con los pechos lacios. Minnie y su amiga Laura-Louise estaban allí, y Roman con una mitra negra y una túnica negra de seda. Con una fina varita negra le estaba haciendo dibujos en el cuerpo”. 
 – “En las montañas de la locura” H. P. Lovecraft 
 Es un claro homenaje a su más grande influencia, Edgar Allan Poe, donde narra la historia de un geólogo que realiza una expedición al continente antártico y donde se topará con una gran cordillera, llena de obscuridad y maldad. Léelo aquí . 
  
 “La sorpresa auténticamente perturbadora fue la que recibimos cuando, después de pasar por encima de un bulto que nos había inquietado sobremanera y de desenvolverlo de la lona que lo cubría, encontramos algo realmente inquietante. Al parecer, otros, además de Lake, se habían interesado por coleccionar especímenes curiosos, pues allí había dos, helados, rígidos, en perfecto estado de conservación, y envueltos cuidadosamente para que no sufrieran más daño”. 
 *** 
 Te puede interesar: 
 El cortometraje de H.P. Lovecraft que te hará gritar de miedo 
 Poe y Lovecraft, del horror psicológico al pavor cósmico