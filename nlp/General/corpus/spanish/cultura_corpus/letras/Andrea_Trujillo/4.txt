Frases con las que los verdaderos amantes de los libros se identificarán

 “De pronto me encontraba en un mundo desconocido, que al mismo tiempo me resultaba familiar. El piso era terso, y pude percibir manchas negras en él que comenzaban a erigirse alrededor mío. Se convirtieron en sombras, en personas, luego en paisajes y a medida que caminaba cambiaban de forma. Me sentí inmersa en un mundo que no era el mío, pero extrañamente sentí pertenecer como en ningún otro. Aquellas sombras me abrazaron y me acogieron en su planeta descolorido y, sin darme cuenta, ya era yo una silueta que las acompañaba. Por momentos éramos puntos, comas y letras; por otros silencios breves o prolongados, pero habitamos siempre el vacío, llenándolo con figuras e historias; reviviendo viejas memorias y construyendo las nuevas. Caminamos y danzamos sin cesar durante horas, días y semanas, hasta que llegamos a un callejón sin salida donde inmediatamente supimos que ese era el final. Lentamente comencé a salir de aquel paraíso extraordinario, me elevaba y lo veía de lejos, vislumbrando aquellas manchas que en silencio se despedían de mí. Sabía que nos volveríamos a ver, tal vez en un mes, tal vez en un año, pero nos volveríamos a ver”. 
  
 Algo así se siente cuando logramos compenetrarnos con una historia, cuando abandonamos sin voluntad nuestra realidad para adentrarnos en otra y conseguimos alunizar en las páginas de un libro nuestra imaginación . En las letras encontramos consuelo y compañía; a través de ellas conocemos el mundo sin levantar la mirada y expandimos nuestra mente a horizontes inexplorados. Las palabras de otros se convierten en nuestra propia voz, y con ellas descubrimos quienes somos; diría Borges : “Uno no es lo que es por lo que escribe, sino por lo que ha leído”. 
 Estas son algunas frases con las que los verdaderos amantes de los libros se identificarán. 
 
 “Los libros son los mejores amigos: nos dan consejo en la vida y consuelo en la aflicción”. Richard Withelock 
 “No hay espectáculo más hermoso que la mirada de un niño que lee”. Günter Grass 
 “Un libro abierto es un cerebro que habla; cerrado un amigo que espera; olvidado, un alma que perdona; destruido, un corazón que llora”. Proverbio hindú 
 “Siempre imaginé que el Paraíso sería algún tipo de biblioteca”. Jorge Luis Borges 
 “Un libro, como un viaje, comienza con inquietud y se termina con melancolía”. José Vasconcelos 
  
 “Los que escriben con claridad tienen lectores; los que escriben oscuramente tienen comentaristas”. Albert Camus 
 “El mundo está lleno de libros preciosos, que nadie lee”. Umberto Eco 
 “El libro que no soporta dos lecturas no merece ninguna”. José Luis Martín Descalzo 
 “He buscado por todas partes el sosiego y no lo he encontrado sino en un rincón apartado, con un libro en las manos”. Tomás Kempis 
 “Un buen libro es aquel que se abre con expectativas y se cierra con provecho”. Aimos Alcott 
  
 “A menudo en tu vida te encontrarás con que un libro es mejor amigo que un hombre”. Luigi Settembrini 
 “Adquirir el hábito de la lectura y rodearnos de buenos libros es construirnos un refugio moral que nos protege de casi todas las miserias de la vida”. W. Somerset Maugham 
 “Amar la lectura es trocar horas de hastío por horas de inefable y deliciosa compañía”. John Fitzgerald Kennedy 
 “Creo que parte de mi amor a la vida se lo debo a mi amor a los libros”. Adolfo Bioy Casares 
 “El recuerdo que deja un libro es más importante que el libro mismo”. Gustavo Adolfo Bécquer 
  
 “Los libros me enseñaron a pensar, y el pensamiento me hizo libre”. Ricardo León 
 “Para que el hombre sea fuerte debe comer regularmente, y para que sea sabio debe leer siempre”. Jeremy Collier 
 “Sentarse tranquilamente bajo la luz de una lámpara con un libro abierto entre las manos, y conversar íntimamente con los hombres de otras generaciones, es un placer que traspasa los límites de lo imaginable”. Elizabeth Barrett Browning 
 “Todo libro que ha sido echado a la hoguera ilumina al mundo”. Ralph Waldo Emerson 
 “De los diversos instrumentos inventados por el hombre, el más asombroso es el libro; todos los demás son extensiones de su cuerpo… Sólo el libro es una extensión de la imaginación y la memoria”. Jorge Luis Borges. 
 * 
 Seguramente habrá uno o varios libros que hayan marcado tu vida , cuéntanos cuáles han sido en los comentarios y compártenos la frase que más te haya gustado. 
 *** 
 Te puede interesar: 
 25 de las frases más bellas de la literatura 
 Frases que nos enseñan que el amor no es cualquier cosa