Poemas de desamor que nos recuerdan que los amantes provienen de mundos distintos

 Antonieta Rivas Mercado, la gran promotora cultural  y escritora, aseguró que sólo padeciendo de amor se sabe cuánto se ama. Y es que a nadie le resulta ajeno saber que, ante todo, el amor es un compromiso entre dos personas, que no significa que la experiencia sea la misma para los dos involucrados. Está el amante y el amado, pero estos dos proceden de regiones distintas. Muchas veces, la persona amada es sólo un estímulo para todo el amor dormido que se ha ido acumulando desde hace tiempo en el corazón del amante y, de un modo u otro: todos lo sabemos pero nos aferramos al inminente, trágico y doloroso final. El desamor que se sufre logra que en el alma se cree un espacio que hace entender a todo enamorado que el amor es algo solitario, lo hace conocer una nueva y extraña soledad, y este conocimiento lo hace sufrir. Hay quienes, como Antonieta Rivas Mercado, no soportan el sufrimiento y deciden poner fin a su vida, pero hay personas, como los poetas, quienes toman su dolor y lo transforman en letras para gritar al mundo que ese sentimiento debe salir. 
  Si a ti como a los poetas de cultura colectiva te asfixia el dolor del desamor, te invitamos a leer estos poemas: 
 La sangre de los erizos es lila (Jennifer Gómez) Cernuda decía que los hombres un día inventaron el amor para compartir su frío. Los erizos de Schopenhauer supieron hacer lo que muchos de nosotros no hemos podido nunca: encontrar la distancia de seguridad para soportar el dolor y el frío en invierno. Y yo hace años que vivo sumida en un invierno del que no he tenido traslación, a pesar de los temporales y de que lloviera más dentro de mí de lo que jamás ha llovido ahí fuera. Pero, al contrario que los erizos todavía no he logrado encontrar una distancia que me asegure tu calor sin tener que sobrellevar tus púas. Reniego, aun así, de cualquier daño irreversible que no sea provocado por ti. O de toda promesa vana en la que no nos juremos dolor eterno. Que al amor también es eso, dolernos, y a mí nadie me ha dolido como tú lo haces. Glaciares de invierno en ellos vivimos. Impasibles al amor normal. Buscándonos y evitándonos siempre tan cerca y tan lejos, queriéndonos y sufriéndonos, como los erizos.   
 Severos problemas de triangulación (Amaury Galván) 
 Concuerdo contigo si me dices que es mejortu lado de la cama. Pablo Malaurie. 
 dices que tu lado de la cama, cariño, es mucho, mucho mejor de lo que es el mío 
 no sé si concuerdo contigo 
 no estás aquí para sentirlo tengo severos problemas de triangulación y tú lo sabes y no es necesariamente malo aunque me insistas 
 tengo capacidades casi nulas para simbolizar tu presencia tengo mucha menor capacidad para simbolizar el momento en el que desapareces de mi vista y no estoy loco aunque me insistas y no estoy loco, cariño, aún no estoy loco 
 no sé si concuerdo contigo 
 ya me has dicho que es mejor tu lado de la cama y no suena bonito suena perfecto en los demás pero en ti no suena nada bonito 
 no sé si concuerdo, cariño 
 me acuerdo de ese tipo: ¿quién es él? ¿te está coqueteando? no me mientas es el tipo que en el cine te flirteó no me digas su nombre que no me digas su nombre en serio no me digas su nombre su nombre irrumpió por vez primera la fase REM de mi sueño el 22 de marzo de 2010 y lo sigue haciendo con frecuencia, cariño, su apellido es Cassel 
 
 que no me digas su nombre no me digas más que seguro ya conoce tu lado de la cama no trates de negarlo no sé si concuerdo, cariño, si me dices que es mejor un amor que un enemigo y viene la incertidumbre 
 sé que para mí el enemigo es Cassel no sé si para ti el enemigo soy yo o si para Cassel, el enemigo soy yo 
 sé que para mí el amor eres tú no sé si para ti el amor es Cassel o si para Cassel el amor eres tú 
 o si para mí el enemigo eres tú o si para ti el amor sí soy yo o si para Cassel el enemigo eres tú o si para mí el amor es Cassel o si para Cassel el amor no es prioridad o si para ti no existe el amor y yo jodiendo siempre con lo mismo 
 no sé si concuerdo, cariño, si me dices que es mejor un amor que un enemigo 
 Cassel, cariño ahora es una certeza 
 aparición del III profecía autocumplida dicen los terapeutas, cariño Cassel el III 
 la única verdad es habernos conocido tú y yo cariño tú y yo 
 todo era más bonito cuando sólo entrábamos al nido tú y yo cariño tú y yo 
 no sé si concuerdo contigo 
 tal vez sí sea mejor tu lado de la cama 
  
 Todo lo que te dejo antes de irme (Ulises Franco) Te he dejado un alarido en la mesa, dos cupones de camión  y el vacío de tres copas. Poco más de cuatro cuentos de ficción y la mitad de la mitad de veinte poemas.   Sobre la cama te dejo mi ropa y mi boca. Algunas otras cosas.   Y bajo la mesa te dejo las seis noches que nos cambiamos como baraja, la carta de siete de corazón rojo. Te dejo mi oficio y su ventaja    El resto me lo llevo. En la soledad estás a salvo (Nahui Olin Nava García) Porque en el fondo no quieres ser amado, ni bajar la guardia o dejarte besar las comisuras, esas donde comienza tu palabra y termina tu silencio. En realidad no quieres sentirte vulnerable ante unos ojos que vean con amor tus enojos que se encarnan en tu ceño. 
 Eres tú, un ente solitario, enamorado del caos y de los amores imposibles, culpando al destino que se ha encargado de poner sólo errores sobre tus labios. Culpas a aquellos que no te supieron amar como tú querías. Una vez más no te hiciste cargo, no asumiste que en lo profundo de tu ser te encanta vivir contemplando el desamor dormido sobre tus sienes, te place caminar sin el afán de encontrar a alguien esperándote. 
 Eres tú contra el mundo, amando tu soledad, tu desinterés con todo y tus espontáneos fanatismos a cualquier cosa; eres tú siendo el culpable del incendio y el bombero a la vez. Y todas esas veces que, pateando piedras, te preguntaste cuándo será que llegará alguien para ti, lo hiciste sabiendo que andabas a contraflujo, tomaste aire y al final del suspiro soltaste una carcajada, porque siempre lo has sabido, porque las reglas de este juego las has inventado tú. 
  Y tu recuerdo me visitará otra vez  (Hermes Moncada) Esta casa se ha convertido en un caparazón de caracol de mar. No se oye el mar, se escuchan los recuerdos. Cuando todo calla por las madrugadas, en las que normalmente estoy despierto, la luz intrusa de la Luna entra por las ventanas y dibuja tu figura, la que intento abrazar sin poder hacerlo. Lo que más me recuerda a ti es tu ausencia. ¡Mierda! ¿Cuántas copas ahogarán tu recuerdo? Espero que tanto pensarte, recordarte, a estas altas horas, no te despierte. No interrumpa tu sueño. Mira que no me gusta molestarte, aunque sea yo el que te extrañe. Algunas veces suelo abrir una ventana, algunas de esas veces el viento nocturno trae tu aroma. Tienes el aroma de la noche: misterio, seducción, belleza, suavidad, toque de ajeno a esta tierra, toque de Luna, así es tu aroma; debe ser por eso que el insomnio me mantiene a estas horas despierto. A estas horas es cuando tu recuerdo está más vivo, es cuando tu ausencia se hace más presente, es cuando más te extraño, es cuando tú estás dormida en una cama que no es la mía, y es cuando digo tu nombre y el viento lo lleva a tu ventana que nunca se abre. Es cuando me voy a dormir triste, a sabiendas de que mañana habrá otra noche y tu recuerdo me visitará de nuevo. Listado de errores (Herson Barona) La página que buscas no está disponible. 
 «Algo salió mal, técnicamente. Gracias por darte cuenta —vamos a arreglarlo y todo volverá a la normalidad pronto». 
 Es cierto, era un error: estábamos teniendo dificultades técnicas. El error 409 se refiere a la existencia de un conflicto. 
 Revisa tu historial de búsquedas, cierra las ventanas emergentes, tus cuentas alternas, lee los mensajes no enviados en tu correo electrónico 
 (recordatorio: marcarte como spam). 
 Error 404. Es posible que el vínculo que seguiste esté roto. 
 Error 417, fallaron las expectativas, siempre esperas demasiado de los otros. 
 Abre la carpeta de imágenes ordenadas por épocas y scrollea hacia el pasado para provocarte nostalgia 
 antes de borrar tu disco duro y el respaldo de tu disco duro. 
 Error 404. El contenido que buscas no pudo hallarse; si existe ya no está ahí: 
 las cosas han cambiado de sitio. Todo se ha movido de lugar excepto tú. Error 301. Movido permanentemente (la traducción falla: podría decir que se mudó), y este enlace no te redirige a ninguna parte. 
 El error 424 indica una falla de dependencia. El error 303 se llama «ver a otros». 
 Abre un documento en blanco: escribe un final o un nuevo comienzo, 
 escribe la palabra casa una y otra vez hasta el hartazgo, 
 cierra el documento sin guardar los cambios o guárdalo con el nombre «error 404_el pasado es una cosa que no es». Error 408: se acabó el tiempo de espera. 
 Abre y vacía la papelera de reciclaje, piérdelo todo como quien pierde su patrimonio 
 (la palabra casa sin cambios), 
 inicia una sesión en modo incógnito en el explorador, 
 escribe su nombre en la barra de búsqueda: 
 Error 410. El sitio que intentas visitar es un no lugar, 
 el contenido fue borrado, no está disponible y no lo estará de nuevo. 
 Se ha ido. La persona que buscas ya no existe. 
 Los errores de http que comienzan con un 4 son errores del usuario: 
 el 426 indica que se requería mejorar, el 429 es un error provocado por un exceso de peticiones; 
 error 449: reintentar después de llevar a cabo la acción adecuada. 
 Intento decir que toda la culpa es mía, pero tal vez me equivoco. 
 Reintento: la culpa es mía. 
 Error 404: ya no somos los de antes.   Enamorarse y tener hambre no es lo mismo , aunque se sienta como un hueco en  el estómago. Si a ti también se te fue la vida con el gran amor que se marchó, te vendría bien conocer la diferencia que pocos entienden entre amar y enamorarse . 
 *** Las fotografías que acompañan esta compilación de poemas pertenecen a Berta Vicente Salas, para conocer mas de su trabajo, te invitamos a visitar su página oficial . 
 *** 
 ¿Te gustaría que tus poemas y creaciones literarias aparecieran publicadas en nuestra plataforma? Envíanos tus propuestas a [email protected] /* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */