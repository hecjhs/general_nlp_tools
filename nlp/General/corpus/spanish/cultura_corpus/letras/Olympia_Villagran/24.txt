Libros que podrían provocar un infarto a las buenas conciencias

 Sudoración moderada o excesiva, escalofríos, espasmos, pezones endurecidos, erecciones fálicas, cosquilleos, elevación de la temperatura corporal, lubricación, hipersensibilidad, mareos, taquicardia, temblores, tensión muscular, jaqueca, sequedad, náuseas, palpitaciones y dificultad para respirar, son algunas de las reacciones físicas que nuestro cuerpo experimenta, de manera involuntaria, cuando entra en una especie de crisis, placentera o no.Por ejemplo, la excitación sexual, un episodio de ansiedad o una incomodidad exacerbada, lo cual viene acompañado de nervios y angustia que podrían, en algún momento y a cierta escala, provocarnos un infarto. Realmente existen situaciones que nos ponen en aprietos a la hora de enfrentarlas, generando reacciones y produciendo sustancias dentro de nuestro cuerpo que nos podrían llevar al borde de la muerte.También existen otro tipo de “trampas” culpables de hacernos sentir incómodos, desubicados, abochornados y hasta nauseabundos. Las palabras, por ejemplo, poseen un poder implícito que controla, en muchas ocasiones, el tono, volumen, velocidad y forma en que las pronunciamos, por lo que generan, de acuerdo a la concepción de cada uno, ciertas reacciones que nos acomodan en una situación placentera hasta el clímax o en un apuro irremediablemente insufrible.Debajo, presentamos los libros que a lo largo de sus páginas le provocarán a más de uno desde un sutil orgasmo hasta una repulsión que los obligará a cerrar el libro.– “Lolita” – Vladimir Nabokov 
 Precocidad exacerbada, ansiedad sexual. 
 – “Flowers in the Attic” –  V. C. Andrews 
 Venganza súbita, suspenso enfermizo. 
 – “The Catcher in the Rye” – J. D. Salinger 
 Rebeldía destructiva, perversión pornográfica. 
  
 – “Delta of Venus” – Anais Nin Encuentros sexuales, incesto dominante. 
 – “Helter Skelter” – Vincent Bugliosi Muerte sanguinaria, crimen agonizante. 
 – “Pet Sematary” – Stephen King Terror mortífero, tortura letal. 
  
 – “Go Ask Alice” – Beatrice Sparks Consumante adicción, escape desesperado. 
 – “The Claiming of Sleeping Beauty” – A. N. Roquelaure (Anne Rice) Copulación robada, sumisión sexual 
 – “Coffee, tea or me?” – Donald Bain Infidelidad disfrazada, perdición inmoral. 
  
 – “American Psycho” – Bret Easton Ellis Locura abominable, sadismo incómodo. 
 – “The Silence of the Lambs” – Thomas Harris Canibalismo vil, cruel obsesión. 
 – “Surrender” – Lora Leigh Erotismo prohibido, exclavitud desnuda. 
  – “A Clockwork Orange” – Anthony Burgess Desviación lasciva, intensidad demencial. 
 – “Peyton Place” – Grace Metalious Abuso abrasivo, aborto inducido. 
 – “Watership Down” – Richard Adams Ambición mundana, concupiscencia retrógrada. –“Girl Interrupted” – Susanna KaysenInocencia desvalijada, exceso clandestino.–“Little Birds” – Anais NinPornografía pueril, lujuria temprana.–“Exquisite Corpse” – Poppy Z. BriteFilias diabólicas, preferencia impúdica.– Tal vez, las cuatro palabras que leíste debajo de cada título no te removieron ni la conciencia, pero si fuiste de los que se estremeció mientras las leía, se te salió alguna mueca disgustada o te acaloraste conforme tu pantalla iba bajando, entonces eres de esas buenas conciencias a las que estos libros les provocaría un infarto. 
 ***Te puede interesar:Libros eróticos para calentar el veranoLas letras sucias: libros escritos por estrellas porno