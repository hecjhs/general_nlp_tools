50 cosas que debes saber para poder entender la Ilíada

 
 La manifestación de la belleza, el sentimiento estético y la emoción interna es lo que la poesía, ya sea en verso o en prosa, plasma por medio de la palabra. Los precursores de este género, los griegos , entendían como líricas o cantos a las líneas que componían la poesía. Adornada de rasgos teatrales y dramatismo es como estas obras poéticas contaban historias magnificas en cortos o extensos versos. 
 Existen muchos cantos u obras que conceptualizan propiamente al género poético, una de las odas más emblemáticas de esta rama de la literatura es la “Ilíada” de Homero , sin embargo, su belleza no le ahorra dificultad al texto. Entre batallas vengativas, pasiones inmorales, más de 30 personajes, dos ciudades griegas, dioses, reyes y soldados, se relata la historia que hipnotizó a generaciones. 
 Para facilitarte su lectura, nosotros te anotamos en 50 breves puntos lo que sabes saber para comprender esta rapsodia: 
  
 1. La Ilíada es una epopeya griega y el poema más antiguo de la literatura occidental. 
 2. Está conformada por 15 mil 693 versos divididos en 24 cantos. 
 3. El tema principal gira en torno a un lío amoroso que posteriormente desencadena una guerra. 
 4. Esta obra es una de las atribuciones más grandes para la literatura y fue creada por Homero. 
 5. Los protagonistas del conflicto son Helena y Paris, la reina que escapa con el príncipe de Troya para vivir libremente su amor. 
 6. El esposo de Helena, Menelao, se enfurece y comienza una búsqueda exhaustiva para encontrar a su esposa, desatando la guerra. 
 7. El contexto de la obra se ambienta en el siglo IX a.C. dentro de Troya y la ciudad de Esparta. 
 8. Aunque se relaciona en ciertos momentos a los personajes con los dioses, la obra se centra en un plano terrenal. 
 9. Principalmente, en este conflicto se ven involucrados la nobleza y los soldados que defienden a sus reyes, tanto en Troya como en Grecia. 
 10. El intercambio de riquezas y/o personas era una de las formas de pago que se practicaban en la época bajo la que se desarrolla esta historia. 
  
 11. Los géneros que caracterizan a la obra son el épico y la epopeya, típicos del periodo literario de la Ilíada. 
 12. El primer canto aborda “La peste y la cólera”, en el que se describe la peste que se desata sobre el campamento aqueo a causa de los nueve años de guerra que llevan combatiendo los soldados. 
 13. Los aqueos son el grupo de griegos que se enfrentaron a los troyanos, el conjunto de soldados que defendieron la ciudad de Troya. 
 14. “El sueño de Agamenón y la Beocia” es el segundo canto, en el que Agamenón, a manera de visón, tiene un sueño en el cual  Zeus le aconseja armar a sus tropas y atacar Troya cuanto antes. 
 15. En este mismo apartado, la Ilíada menciona el catálogo de naves con las que Agamenón atacaría, son más de 1,186 barcos atacantes con cientos caudillos que llegarían a cada región. 
 16. La rapsodia número tres lleva por nombre “Los juramentos y Helena en la muralla”, justo a partir de ese momento, Paris comienza a desafiar a Menelao en combate, el ganador se quedaría con Helena y sus tesoros. 
 17. Paris, a pesar de ser un excelente combatiente, estuvo a punto de ser asesinado por su oponente, el esposo de Hera, pero para su suerte es salvado por Afrodita. 
 18. “Violación de los juramentos y revista de las tropas”, este es el cuarto título con el que se resume el momento en el que Pándaro rompe la tregua con Menelao, quien incitado por Atenea, le lanza una flecha y lo hiere. 
 19. Este evento trajo la reanudación de una lucha entre troyanos y aqueos. 
 20. Hera y Atenea fueron las divinidades que ayudaron a los aqueos, mientras que Ares y Apolo se encargaron de guiar desde lo alto a los soldados de Troya. 
  
 21. Así fue como Diómedes, un soldado de Grecia estuvo a punto de matar a Eneas y a su vez hiere a Ares, batallas que forman parte de “Principalia de Diómedes”, el canto número cuatro. 
 22. Los adivinos son otro grupo de personajes que aparecían constantemente en la Ilíada, en muchas ocasiones ellos fueron los que llevaron al triunfo o al fracaso varias de las batallas entro ambos combatientes. 
 23. Héleno, hijo de Príamo y adivino, sugiere a Héctor que regrese a Troya para pedir a las mujeres troyanas que construyan ofrendas en el templo de Atenea, por lo que Héctor debe despedirse de su esposa Andrómaca para emprender su viaje, es por eso que este sexto canto lleva el nombre de “Coloquio de Héctor y Andrómaca. 
 24. Posteriormente aparece “Combate singular de Héctor y Áyax”, este duelo tiene lugar en la noche que pone fin a la batalla entre aqueos y troyanos, acordando una tregua. 
 25. La “Batalla interrumpida” es el canto siete donde el Rey del trueno ordena a los demás dioses detener cualquier acto que intervenga, positiva o negativamente, en los enfrentamientos de los dos campamentos. 
 26. Como estrategia bélica, envían a Agamenón a disculparse con Aquiles para que regrese a la lucha en defensa de los aqueos. 
 27. A pesar de la súplica, Aquiles se niega a reaparecer; ésta es la resolución del noveno canto titulado “Embajada a Aquiles”. 
 28. La parte diez de este relato se llama “Gesta a Dolón” y se centra en una misión secreta que Domeñes y Odiseo realizan durante la noche para matar a Dolón. 
 29. El espionaje fue una táctica típica de esta epopeya, así obtenían información del enemigo para emboscarlo. 
 30. La “Gesta de Agamenón” surgió de la ventaja que los aqueos repentinamente perdieron cuando Agamenón es herido por Colón. 
  
 31. A varios de estos cantos se les tituló gestas por tratarse de hechos dignos de ser recordados por su heroicidad o trascendencia. 
 32. “Combate de la muralla” fue cuando Héctor lideró a las tropas troyanas para atravesar el muro de los aqueos. 
 33. Otra de las batallas destacadas fue “Batalla junto a las naves”, en la que Poseidón anima a los aqueos a resistir. 
 34. Zeus es el protagonista del canto XIV, a quien Hera y Afrodita engañan para distraerlo momentáneamente de la batalla. 
 35. Cuando Zeus descubre la trampa en la que ha caído, ordena a Poseidón abandonar a los aqueos mientras desata su furia sobre Ares por apoyarlos; estas dos acciones llevan a los troyanos a combatir desde sus naves al opositor. 
 36. Al capítulo anterior se le llamó “Nueva ofensiva desde las naves”, por tratarse de una lucha sobre el agua. 
 37. La “Gesta de Patroclo” y de “Menelao” son los cantos 16 y 17 de la Ilíada, ambas fueron batallas definitivas para marcar el declive de los troyanos. 
 38. Cuando Menelao despoja a Patroclo de sus armas en “La Gesta de Menelao”, Hefesto comienza fabricar nuevas herramientas para Aquiles. 
 39. Las armas que utilizaban en esa época eran espadas, jabalinas, escudos y armaduras, y justo sobre ellas trata el canto 18: “Fabricación de Armas”. 
 40. En ese tiempo, los juramentos eran parte fundamental de la guerra o su conciliación. 
  
 41. “Aquiles depone la ira” expone justamente cuando él se reconcilia con Agamenón, quien a manera de juramento le promete que jamás tocó a Briseida, el regalo que le devuelve a Aquiles. 
 42. El dios del Olimpo retira la prohibición que había impuesto a otros dioses para ayudar a su favorito en el combate, por eso el canto XX se titula “Combate de los dioses”. 
 43. Casi al final se lleva a cabo una batalla junto al río, en la que Aquiles mata a más de tres soldados junto a la corriente. 
 44. El canto 22 presencia la “Muerte de Héctor”, quien queda fuera del muro cuando las fuerzas troyanas se refugian en la ciudad, lo que lo lleva a pelear con Aquiles, tras lo cual termina perdiendo la vida. 
 45. El penúltimo escrito de la Ilíada pertenece a “Juegos en honor a Patroclo”, estos juegos fúnebres se celebraron para invitar a los participantes a lanzar la jabalina, el tiro con arco, lanzamiento de peso, carrera de carros, entre otras actividades que implicaban fuerza y virilidad. 
 46. Por último, “Rescate de Héctor” cierra como el canto 24, en el que el cadáver de Héctor es regresado a Príamo cuando éste le ruega a Aquiles por el cuerpo. 
 47. La “Ilíada” es un texto ideal para los amantes del lenguaje poético, pues de esa manera fue redactado. 
 48. La narración de esta poesía parte de un orador heterodiegético, es decir, en tercera persona, pues quien nos cuenta la historia no es un personaje que participa en los acontecimientos de la “Ilíada”. 
 49. Las emociones que rigen, en toda medida, las acciones de los actores de la “Ilíada” son principalmente la ira, la cólera y la furia. De hecho, una de esas palabras es la que abre el poema. 
 50. Este relato griego del siglo VIII a.C. es la obra griega más famosa de su época. 
  
  Cincuenta años no parecerían tantos para aprenderte todos y cada uno de los personajes de la Ilíada, sus lazos familiares y su listado de amoríos, batalles y traiciones. Pero para Alianza Editorial sí se trata de algunos años que vale la pena celebrar, pues su batalla cumple 50 años de lograr permanecer como líderes en el mercado literario, con un catálogo de autores y obras de todos los géneros para satisfacer el gusto de cualquier lector.