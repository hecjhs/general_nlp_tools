Entendiendo a Cortázar: instrucciones para leer Rayuela correctamente

 
 Cuando se habla de Julio Córtazar, se encuentran dos tipos de lectores: los que terminaron Rayuela y los que la abandonaron. No hay nada que reprochar si en tu primer intento de entrar en el juego, desistieras por no entender sus reglas. Aquí se expondrán algunos trucos. 
 Lo primero que debes entender es que Rayuela es un libro y a su vez son muchos libros. Justo en esta multiplicidad se encuentra su grandeza y por lo que puede confundir al lector, pero descuida, no es tan difícil como parece. 
   
 El libro contiene 155 capítulos, los cuales están divididos en tres partes: “Del lado de allá”, “Del lado de acá” y “De otros lados”. En primera instancia, no hay que prestarle mucha atención a esto. Lo siguiente a distinguir son las tres formas en las que puedes leer el libro. 
 Si eres un neófito en el mundo cortazariano, lo mejor es empezar con una lectura lineal, es decir, iniciar el juego en el capítulo uno y viajar en línea recta hasta el capítulo 56, donde encontrarás una marca que indica el final de la historia. El lector que tome esta ruta deberá aceptar el final del juego y dispensar del resto de las páginas. Aquí ya tienes la primera historia que engloba el mundo de Rayuela . 
    
 Es obvio que hay algo más, y si tras conocer el enredado mundo de Horacio Oliveira te sientes preparado para dar otro gran paso, adelante. 
  
 La segunda forma de leer Rayuela es dejándote guiar por el tablero de direcciones, el cual se encuentra al principio de la obra. Te darás cuenta que con esta guía puedes saltar del seis al 93, por ejemplo. Éste también es el segundo libro que presenta Cortázar. 
 Es la misma historia y a la vez es otra historia, porque en ella descubrirás pasajes alternos que cambiarán, sin alterar la sucesión de hechos, la concepción de la obra. Aquí descubrirás más personajes y pequeñas acotaciones que son como un álbum de recortes del mismo Cortázar. Puedes encontrar desde un pequeño poema de un verso, hasta una nota de periódico que transcribió Julio. Todo con el fin de enriquecer el juego. 
 La última forma de leer el libro es de manera libre. Disfruta sin límites todo lo que te ofrece el texto, deja de querer seguir las reglas, lo establecido de la literatura y haz de Rayuela lo que tú quieras. Deja que el azar sea tu guía. Abre el libro en cualquier parte y dirígete a cualquier dirección, porque al final, eso es lo que en realidad Julio Cortázar desea para ti: que te conviertas en un gran Cronopio . 
  – Si después de estas instrucciones aún te cuesta trabajo entender Rayuela, relájate y explora las ideas de Julio a través de sus mejores frases de amor. 
 *** Te puede interesar: 
 Las ilustraciones que no conoces de Rayuela de Julio Cortázar 
 Datos que no conocías de Julio Cortázar