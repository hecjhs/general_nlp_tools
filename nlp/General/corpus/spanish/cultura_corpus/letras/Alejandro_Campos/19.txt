El beso según los grandes escritores de la historia

 El beso comienza desde la tensión entre miradas: dos almas que se encuentran en un microsegundo. Dos cuerpos que se acercan, atraídos por un extraño magnetismo que se detona con un click. Una mirada previa que confirma aquello que se desea. Los ojos se cierran, y las manos trazan sus propios caminos. Los l abios encienden sus terminaciones , se cargan de una energía que llevará un mensaje: un todo. 
 Los besos  son la gran pasión de los amantes, de quienes expresan lo inexpresable en el roce húmedo de los labios. Esos pequeños tesoros que anteceden a “la rendición final” y que conllevan todo aquello que necesitamos saber. El beso, como un ente omnipresente alrededor del mundo, ha sido ampliamente descrito, vanagloriado y maldecido por cientos de escritores, filósofos y poetas . Te compartimos algunas de las frases, definiciones y apariciones de este gran personaje en la obra de los escritores más célebres en la historia: 
 A una mujer –  Víctor Hugo 
  
 ¡Niña!, si yo fuera rey daría mi reino, mi trono, mi cetro y mi pueblo arrodillado, mi corona de oro, mis piscinas de pórfido, y mis flotas, para las que no bastaría el mar, por una mirada tuya. 
 Si yo fuera Dios, la tierra y las olas, los ángeles, los demonios sujetos a mi ley. Y el profundo caos de profunda entraña, la eternidad, el espacio, los cielos, los mundos ¡daría por un beso tuyo! 
 John Keats 
 “Ahora, un suave beso – Sí, por ese beso me comprometo a una dicha sin fin” 
 Eduardo Galeano 
 “Todos somos mortales hasta el primer beso y la segunda copa de vino”. 
 Emil Ludwig 
 “La decisión de besar a alguien por primera vez es el punto crucial en cualquier historia de amor. Cambia radicalmente la relación de dos personas, incluso la rendición final: porque el beso lleva consigo la rendición misma. 
 Hemingway 
  
 “No quería darte un beso de despedida -ese era el problema- quería darte un beso de buenas noches, y en ello hay una gran diferencia”.  
 Alfred de Musset 
 “Libéranos con un beso hacia un mundo desconocido”. 
 La Cabellera –  Guy de Maupassant 
 “¡Cuánto he llorado, durante noches enteras, pensando en las pobres mujeres de otro tiempo, tan bellas, tan tiernas, tan dulces, cuyos brazos se abrieron para el beso, y ya muertas! ¡El beso es inmortal! ¡Va de boca en boca, de siglo en siglo, de edad en edad; los hombres lo recogen, lo dan y mueren! 
 Lo que el viento se llevó –  Margaret Mitchell 
 “Debes ser besada, y por alguien que sepa cómo hacerlo”. 
 Shakespeare 
 “Aquí, aquí voy a estacionarme con los gusanos, tus actuales doncellas; sí, aquí voy a establecer mi eternal permanencia, a sacudir del yugo de las estrellas enemigas este cuerpo cansado de vivir. -¡Echad la postre mirada, ojos míos! ¡Brazos, estrechad la vez última! Y vosotros, ¡oh labios!, puertas de la respiración, sellad con un beso legítimo un perdurable pacto con la muerte monopolista! -Ven, amargo conductor ; ven, repugnante guía! ¡Piloto desesperado, lanza ahora de un golpe, contra las pedregosas rompientes, tu averiado, rendido bajel! ¡Por mi amor -(Apura el veneno.) ¡Oh, fiel boticiario! Tus drogas son activas. -Así, besando muero”.  
 Paul Verlaine 
 “Cuando en mis sienes calme la divina tormenta, reclinaré, jugando con tus bucles espesos, sobre tu núbil seno mi frente soñolienta, sonora con el ritmo de tus últimos besos”. 
 Pablo Neruda 
 “En un beso sabrás todo lo que he callado”. 
  Doctor Zhivago –  Borís Pasternak 
 “Tú y yo. Es como si nos hubieran enseñado a besarnos en el cielo para ser enviados juntos a la tierra, sólo para que comprobemos si sabemos lo que nos enseñaron”. 
 F. Scott Fitzgerald 
 “El beso se originó cuando el primer reptil macho lamió a la primera mujer reptil, implicando en una manera muy sutil, que ella era tan suculenta como el pequeño reptil que devoró la noche anterior”.  
 Memorias de mis putas tristes  – García Márquez 
 “La noche del cinco de diciembre la besé por todo el cuerpo hasta quedarme sin aliento: la espina dorsal, vértebra por vértebra, hasta las nalgas lánguidas, el costado del lunar, el de su corazón inagotable. A medida que la besaba, aumenta el calor de su cuerpo y exhalaba una fragancia salvaje”. 
 Hermann Hesse 
 “En el primer beso sentí que algo se derretía dentro de mi, algo que dolía de una manera exquisita. Todos mis anhelos, sueños y angustias, todos los secretos que dormían en lo profundo, despertaron. Todo se transformó y se llenó de encanto. Todo tuvo sentido”. 
 Gustavo Adolfo Becquer 
 “El alma que puede hablar con los ojos, también puede besar con la mirada”. 
 Oscar Wilde 
  “Un beso puede arruinar una vida humana”. 
 Lord Byron 
 “Cuando la edad enfría la sangre y los places son cosa del pasado, el recuerdo más querido sigue siendo el último, y nuestra evocación más dulce, la del primer beso”. 
 Octavio Paz 
 “Un mundo nace cuando dos se besan”. 
 Miguel de Unamuno 
 “Besos que vienen riendo, luego llorando se van, y en ellos se va la vida, que nunca más volverá”.  *** 
 Te puede interesar: El beso según los escritores latinoamericanos