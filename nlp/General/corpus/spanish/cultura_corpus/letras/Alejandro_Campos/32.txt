Los libros favoritos de Gabriel García Márquez

 “–Tu papá está muy triste –dijo. 
 Ahí estaba, pues, el infierno tan temido. Empezaba como siempre, cuando menos se esperaba, y con una voz sedante que no había de alterarse ante nada. Sólo por cumplir con el ritual, pues conocía la respuesta, la pregunté: 
 –¿Y eso por qué? 
 –Porque dejaste los estudios. 
 -No los dejé –le dije–. Sólo cambié de carrera. 
 La idea de una discusión a fondo le levantó el ánimo. 
 –Tu papá dice que es lo mismo –dijo. 
 A sabiendas de que era falso, le dije: 
 –También él dejó de estudiar para tocar el violín. 
 –No fue igual –replicó ella con una gran variedad–. El violín lo tocaba sólo en fiestas y serenatas. Si dejó sus estudios fue porque no tenía ni con qué comer. Pero en menos de un mes aprendió telegrafía, que entonces era un profesión muy buena, sobre todo en Aracataca. 
 –Yo también vivo de escribir en los periódicos –le dije”. 
  
 Uno puede hablar mucho del gran escritor latinoamericano, Gabriel García Márquez, quien con su narrativa cautivó al mundo con historias de su natal Colombia, con las que toda Latinoamérica se presentó ante el mundo. Sin embargo, en su autobiografía, Vivir para contarla , publicada en 2002, el colombiano brinda detalles sobre su vida que atrapan en el lector en un viaje al pasado. En un valioso ejercicio narrativo con los detalles de la infancia y adolescencia que determinaron la vida del Nobel, apreciamos las facetas más desconocidas del escritor, los momentos clave de su vida, las personas más apreciadas por Gabo y también aquellos libros que marcaron su desarrollo literario. 
 Así como García Márquez  le temía a volar ante la posibilidad de que el avión se desplomara, el colombiano gustaba de leer a los grandes de la literatura, de dejarse llevar por las recomendaciones literarias de sus amigos y de aprovechar cada instante de su día para tomar un libro entre sus manos. Fue así como con el paso de los años conoció las obras relevantes dentro del mundo de la literatura, mismas que definieron sus intereses, su estilo narrativo y la genialidad para contar historias. A lo largo de su autobiografía, Gabo menciona algunos de los libros que marcaron su vida, en algunos casos especificando los detalles detrás de sus intereses y en otros, tan sólo mencionando títulos. 
  
 La sinceridad del colombiano y la elegante manera de compartirnos los detalles íntimos de su vida, permiten que sus asiduos lectores encuentren en Gabo la figura de un escritor de talla completa. Te compartimos algunos de los libros que el escritor menciona en su autobiografía y el fragmento que da cuenta de su importancia dentro de la vida del colombiano. 
  Te puede interesar: Cosas que no sabías sobre Gabriel García Márquez 
 Ulysses James Joyce 
  
 “Jorge Álvaro Espinosa, un estudiante de derecho que me había enseñado a navegar en la Biblia y me hizo aprender de memoria los nombres completos de los contertulios de Job, me puso un día sobre la mesa un mamotreto sobrecogedor, y sentenció con su autoridad de obispo: 
 –Esta es la otra Biblia 
 Éra, cómo no, el Ulises de James Joyce, que leía a pedazos y tropezones hasta que la paciencia no me dio para más. Fue una temeridad prematura. Años después, ya de adulto sumiso, me di a la tarea de releerlo en serio, y no sólo fue el descubrimiento de un mundo propio que nunca sospeché dentro de mí, sino además una ayuda técnica invaluable para la libertad del lenguaje, el manejo del tiempo y las estructuras de mis libros”. 
 La metamorfosis Franz Kafka 
  
 “Vega llegó una noche con tres libros que acababa de comprar, y me prestó uno al azar, como lo hacía a menudo para ayudarme a dormir. Pero esa vez logró todo lo contrario: nunca más volví a dormir con la placidez de antes. El libro era La Metamorfosis de Franz Kafka, en la falsa traducción de Borges publicada por la editorial Losada de Buenos Aires, que definió un camino nuevo para mi vida desde la primera línea, y que hoy es una de las divisas grandes de la literatura universal. […] 
 […] Al terminar la lectura me quedaron las ansias irresistibles de vivir en aquel paraíso ajeno. El nuevo día me sorprendió en la máquina viajera que me prestaba el mismo Domingo Manuel Vega, para intentar algo que se pareciera al pobre burócrata de Kafka convertido en un escarabajo enorme”. 
 Las mil y una noches 
 “Hoy, repasando mi vida, recuerdo que mi concepción del cuento era primaria a pesar de los muchos que había leído desde mi primer asombro con Las mil y una noches . Hasta que me atreví a pensar que los prodigios que contaba Scherezada sucedían de veras en la vida cotidiana de su tiempo, y dejaron de suceder por la incredulidad y la cobardía realista de las generaciones siguientes. Por lo mismo, me parecía imposible que alguien de nuestros tiempos volviera a creer que se podía volar sobre ciudades y montañas a bordo de una esfera, o que un esclavo de Cartagena de Indias viviera castigado doscientos años dentro de una botella, a menos que el autor del cuento fuera capaz de hacerlo creer a sus lectores”. 
 La cabaña del tío Tom Harriet Beecher Stowe 
  
 “Fue una lástima no haber leído todavía a los nuevos novelistas norteamericanos, que apenas empezaban a llegarnos, pero tuve la suerte de que el doctor Vélez Martínez empezara con una referencia casual a La cabaña del tío To m, que yo conocía bien desde el bachillerato. La atrapé al vuelo. Los dos maestros debieron sufrir un golpe de nostalgia, pues los sesenta minutos que habíamos reservado para el examen se nos fueron íntegros en un análisis emocional sobre la ignominia del régimen esclavista en el sur de los Estados Unidos”. 
 Moby Dick Herman Melville 
   
 “Gustavo Ibarra, con su visión compasiva del corazón caribe, se divirtió con mi relato de la noche en Barranquilla, mientras me daba cucharadas cada vez más cuerdas de poetas griegos, con la expresa y nunca explicada excepción de Eurípides. Me descubrió a Melville: la proeza literaria de Moby Dick , el grandioso sermón sobre Jonas para los balleneros curtidos en todos los mares del mundo bajo la inmensa bóveda construida con costillares de ballenas”. 
 La casa de los siete tejados Nathaniel Hawthorne 
  
 “[Gustavo Ibarra] me prestó  La casa de los siete tejados , de Nathaniel Hawthorne, que me marcó de por vida. Intentamos juntos una teoría sobre la fatalidad de la nostalgia en la errancia de Ulises Odiseo, en la que nos perdimos sin salida. Medio siglo después la encontré resuelta en un texto magistral de Milán Kundera”. 
 Edipo Rey , Sófocles; Pata de Mono , W.W. Jacob y  Bola de sebo, Maupassant 
  
 “Mi escaso interés en los estudios fue más escaso aún después de la nota de Ulises , sobre todo en la universidad, donde algunos de mis condiscípulos empezaron a darme el título de maestro y me presentaban como escritor. Esto coincidía con mi determinación de aprender a construir una estructura al mismo tiempo verosímil y fantástica, pero sin resquicios. Con modelos perfectos y esquivos, como Edipo rey , de Sófocles, cuyo protagonista investiga el asesinato de su padre y termina por descubrir que él mismo es el asesino; como La pata de mono , de W.W. Jacob, que es el cuento perfecto, donde todo cuando sucede es casual; como Bola de sebo , de Maupassant, y tantos otros pecadores grandes a quienes Dios tenga en su santo reino”. 
 Luz de agosto William Faulkner 
  “Tal como ella temía, la tormenta vapuleó la temeraria embarcación mientras atravesábamos el río Magdalena, que a tan corta distancia de su estuario tiene un temperamento oceánico. Yo había comprado en el puerto una buena provisión de cigarrillos de los más baratos, de tabaco negro y con un papel al que poco le faltaba para ser de estraza, y empecé a fumar a mi manera de entonces, encendiendo uno con la colilla del otro, mientras releía Luz de agosto , de William Faulkner, que era entonces el más fiel de mis demonios tutelares”. 
 La montaña mágica  Thomas Mann 
  “Los buenos tiempos empezaron con Nostradamus y El hombre de la máscara de hierro , que complacieron a todos. Lo que todavía no me explico es el éxito atronador de la montaña mágica, de Thomas Mann, que requirió la intervención del rector para impedir que pasáramos la noche en vela esperando un beso de Hans Castorp y Clawdia Chauchat”. 
 El viejo y el mar Hemingway 
  “Lo malo fue que al final de aquel viaje de nostalgias no habían llegado todavía los libros vendidos, sin los cuales no podía cobrar mis anticipos. Me quedé sin un céntimo y el metrónomo del hotel andaba más deprisa que mis noches de fiesta. […] Lo único que me devolvió el sosiego fueron los amores contrariados de El derecho de nacer, la novela radical de don Félix B. Caignet, cuyo impacto popular revivió mis viejas ilusiones con la literatura de lágrimas. La lectura inesperada de El viejo y el mar , de Hemingway, que llegó de sorpresa en la revista Life en español, acabó de restablecerme de mis quebrantos”. 
 Otros libros referidos en la autobiografía de García Márquez: 
 Las palmeras salvajes y  Luz de Agosto de William Faulker 
 Hijos y amantes de D.H. Lawrence 
 El Aleph y otras historias de Jorge Luis Borges 
 The Collected Stories de Ernest Hemingway 
 Contrapunto de Aldous Huxley 
 De ratones y hombres , y Las uvas de la ira de  John Steinbeck 
 El camino del tabaco de Erskine Caldwell 
 Historias de Katherine Mansfield 
 Manhattan Transfer de John Dos Passos 
 Orlando de Virginia Woolf 
 *** 
 Referencia: Brain Pickings