Cuando las palabras seducen, la vida tiene un propósito

 Existe en Internet una tendencia que recopila aquellas frases que le hablan a lo más profundo del ser humano; oraciones y poemas que traspasan la carcaza rutinaria y urbana para conectar con aquello que bombea la vida a todo el cuerpo. “#Wordporn” es la etiqueta empleada por los amantes del atinado acomodo de palabras para compartir frases que van más allá de los premios y los reconocimientos, pues en realidad son estandartes y anclas de vida. 
 Frases, juegos de palabras, rimas y verdades escritas por grandes de la literatura, personajes históricos, escritores amateurs o bloggers de 4 de la mañana. Con un suave tecleo en la computadora o el frenesí que amenaza con dañar el teclado, los seres humanos detrás de estas “máximas” hablan de aquello que nos hace humanos: los sentimientos. Encontramos en éstas breves pero furibundas historias de amor, la soledad humana como paradoja, la lucha incansable contra los problemas existenciales, rayos de luz entre la tiniebla de la depresión o sutiles sonrisas y regalos de la vida. El delicado paseo de nuestros ojos por cada letra despierta desde una ligera sensación extraña en el cuerpo hasta la conexión con el otro en el lado anverso de la frase, sin importar que éste sea anónimo, pues después de todo, es humano. 
 Cantos humanos que nos invitan a compartirlos, aullidos desesperados que piden ayuda y murmuros que golpean en las paredes sin eco alguno. Te compartimos algunas de las mejores frases de #Wordporn. 
 – ” ¿Por qué estás tan triste? Porque me hablas en palabras y yo te veo con sentimientos”. 
 León Tolstói 
  – “Te destruiré de la forma más hermosa posible. Y cuando me vaya, finalmente entenderás porqué las tormentas llevan nombres de personas”. 
 Caitlyn Siehl 
 – “¡Cuán rápido le tengo celos al viento, cuando él, y no yo, tiene el privilegio de jugar con tu cabello!”. 
 Tyler Knott Gregson 
 – “Y te elegiría a ti; en cien diferentes vidas, en cien diferentes mundos, en cualquier versión de la realidad. Te encontraría a ti, y te elegiría a ti”. 
 Kiersten White 
  
 – “Tal vez nos encontremos de nuevo, cuando seamos ligeramente mayores y nuestras mentes menos frenéticas, y yo seré adecuado para ti y tu serás adecuada para mí. Pero ahora, soy caos para tu mente y tú eres veneno para mi corazón”. 
 – “Nunca dejes tu felicidad en las manos de alguien más. La dejarán caer. Lo harán cada una de las veces que lo hagas”. 
 Christopher Barzak 
 – “Enamórate de tu vida. Cada segundo de ésta”. 
 Jack Kerouac 
 – “Existen incendios, vastos e infinitos, que se queman dentro de mí. Y los llevaré conmigo hasta que estés lista para caminar a través de las flamas por mí”. 
 William C. Hannan 
  – “Ella enterró sus oídos en la cama del palpitar de su corazón, y en cuestión de segundos: se enamoró perdidamente con la manera en que su soledad, repentinamente se quedaba dormida, en su pecho”. 
 Christopher Poindexter 
 – “Las únicas personas despiertas a las tres de la mañana están enamoradas, solas o borrachas. O las tres al mismo tiempo”. 
 – “Aún podría morir por ti, pero estoy intentado descifrar si significas tanto para mí o si yo significo tan poco para mí”. 
  – “No es una falta de amor, sino una falta de amistad la que provoca matrimonios infelices”. 
 Friedrich Nietzsche 
 – “Un hombre que pueda manejar de forma segura mientras besa a una bella chica simplemente no está dándole la atención que el beso merece”. 
 Albert Einstein 
 – “Ese es el problema con poner a los otros primero; les has enseñado que tú vienes después”. 
 – “El primer deber del amor es escuchar” 
 Paul Tillich 
 – “La amaba no por la manera en que bailaba con mis ángeles, sino por la manera en que el sonido de su nombre silenciaba mis demonios”. 
 Christopher Poindexter 
  – “De todas las cosas que mis manos han sujetado, por mucho lo mejor eres tú”. 
 – “Algunas personas me dicen que sea fuerte. Y algunas personas me dan fuerza”. 
 Donte Collins 
 – “Quien ha sido destruido, sabe cómo destruir”. 
 – “Amarte me ha enseñado cómo se siente estar muerto y seguir respirando”. 
  – “Tu determinación por luchar contra tus demonios hará que tus ángeles canten”. 
 August Wilson – “Algunos corazones se entienden a sí mismos, incluso en el silencio”. 
 Yasming Mogahed 
 – “No tomes una decisión basada en el consejo de personas que no tendrán que lidiar con las consecuencias”. 
 – “No me interesa si has estado con los grandes. Me interesa saber si te has sentado con los rotos”. 
  – “Es sencillo encontrar alguien con quien pasar el tiempo. Pero es difícil encontrar alguien con quien construir”. 
 – “Enséñame todas las partes que no amas de ti para saber por dónde empezar”. 
 – “No importa cuánto revises el pasado. No hay nada nuevo que ver”. 
  – “Ella vive la poesía que no puede escribir”. 
 Oscar Wilde 
 – “Habrá decenas de personas que te robarán el aliento, pero quien te recuerde que debes respirar, es con quien debes quedarte”. 
 – Te puede interesar: 25 de las frases más bellas de la literatura