25 frases de Confucio que te harán reflexionar sobre tu propia vida

 Huele a otoño. La brisa del aire retumba en tu cara mientras inhalas un fresco olor a bosque conífero. Abres los ojos y te maravillas al retomar tu día en el lugar en el que has despertado. Frente a ti un majestuoso lago cuyo movimiento, apenas perceptible, es causado por los peces que nadan en él. El ulular de los árboles a causa del viento cubre la tierra con un manto multicolor. Tonalidades amarillas, rojas, naranjas y lo que queda del verde pintan este cuadro. Contemplas. Respiras. 
 ** 
   
 En la historia de la Antigua China existe un hombre que encabezó una doctrina espiritual que revolucionó toda la región: Confucio. Nacido en 551 a.C, Confucio vivió en medio de una época de constantes guerras en la región, por lo que la fe en los dioses de aquella época se había perdido. El ser humano, en constante búsqueda por una aspiración más allá de lo terrenal, anhelaba un guía espiritual y moral que le permitiera seguir adelante y superar los trágicos hechos que le perseguían. A la edad de los 50 años, Confucio empezó a recorrer China compartiendo sus reflexiones en torno a la vida, el amor, la muerte, la guerra, las virtudes y los defectos de las personas. Sus ideas estaban basadas en los principios y preceptos de los sabios de la antigüedad. Pronto, sus enseñanzas comenzaron a llenar el vacío espiritual y moral que la sociedad china padecía e incluso, líderes políticos comenzaron a realizar un reordenamiento de la sociedad china en torno a la palabra de Confucio. Incluso, hoy en día, el régimen maoísta en China basa gran parte de sus estructuras en la instrucción confucionista. Sin embargo, las enseñanzas de Confucio también han permeado en las sociedades de Vietnam, Corea, Japón 
 Confucio era un fiel creyente de que la educación era el pilar más importante para una sociedad pues “constituía el valor general del individuo y de los grupos en los cuales éste se integra de forma activa: la familia, la comunidad y la nación”. Asimismo, su pensamiento filosófico se basa en la buena conducta en la vida, el buen gobierno del Estado (caridad, justicia y respeto a la jerarquía), el cuidado de la tradición, el estudio y la meditación. En cuanto a las virtudes máximas del confucionismo se encuentran: la tolerancia, la bondad, la benevolencia, amor y respeto a la naturaleza, amor y respeto a los padres, respeto a los mayores, al orden político, al orden social, al orden religioso y a la Armonía. Sin embargo, para lograr aquello se necesitan las 3 virtudes fundamentales: la bondad que produce alegría y paz interior; la ciencia que disipa todas las dudas y la valentía que ahuyenta todo temor. 
  
 Asimismo, en cuanto a un aspecto político, Confucio consideraba que el príncipe (jefe de Estado o padre) debía de tener un carácter y una conducta intachable, misma que le permitiera que sus súbditos siguieran su ejemplo. Partiendo de ello, creía que existían 3 relaciones claves para el buen desarrollo de una sociedad: gobernante/súbdito, marido/mujer y padre/hijo. 
 A pesar de que los principios morales del confucianismo podrían asemejar a aquellos de una religión, el objetivo de Confucio no era fundar y encabezar una nueva corriente espiritual. Por el contrario, Confucio realizó una serie de críticas, reformas y enseñanzas a los modelos sociales, económicos y políticos de China, planteando así una doctrina ética. Respecto a dicha doctrina, te presentamos 25 frases emblemáticas de Confucio que te harán reflexionar sobre tu propia vida: 
 
 “Así como el agua toma la forma del recipiente que la contiene, un hombre sabio debe adaptarse a las circunstancias”. 
 “Cuando el objetivo te parezca difícil, no cambies de objetivo; busca un nuevo camino para llegar a él”. 
 “Dale un pescado a un hombre y comerá un día. Enséñale a pescar y comerá toda la vida”. 
 “Todas las cosas buenas son difíciles de conseguir y todas las cosas malas son muy fáciles de conseguir”. 
 “Un hombre que no piensa y planifica su futuro encontrará problemas desde su propia puerta”. 
 “Lo prudente no quita lo valiente”. 
  
 “No hagas a otros lo que no quieres que te hagan a ti, ni te hagas a ti lo que no le harías a los demás”. 
 “Cuando el sabio señala a la luna, el necio mira el dedo”. 
 “No te rindas nunca si quieres volver a casa”. 
 “Ser como el sándalo que perfuma el hacha que lo corta”. 
 “Antes de embarcarte en un viaje de venganza, cava dos tumbas”. 
 “La vida es muy simple, pero insistimos en que sea complicada”. 
  
 “El silencio es un amigo que jamás traiciona”. 
 “Exígete mucho a ti mismo y espera poco de los demás. Así te ahorrarás decepciones”. 
 “Aceptar lo inesperado. Aceptar lo inaceptable”. 
 “El mejor indicio de la sabiduría es la concordancia entre las palabras y las obras”. 
 “No pretendas apagar con fuego un incendio, ni remediar con agua una inundación”. 
 “Lo único que no se recupera nunca en la vida cuando se pierde es el tiempo transcurrido”. 
  
 “A quien tiene todas las respuestas no se le han preguntado todas las preguntas”. 
 “Lo escuché y lo olvidé. Lo vi y lo entendí. Lo hice y lo aprendí”. 
 “El éxito depende de la preparación previa pero sin dicha preparación el fracaso es seguro”. 
 “Podemos volvernos sabios a través de tres formas distintas. Primero, a través de la reflexión que es la más noble. Segundo, por imitación que es la más fácil. Y la tercera por experiencia, que es la más amarga”. 
 “El hombre que mueve montañas empieza moviendo pequeñas piedras”. 
 “El hombre sabio busca lo que desea en su interior; el no sabio lo busca en los demás”. 
 “Elige un trabajo que te guste y no tendrás que trabajar ni un día de tu vida”.