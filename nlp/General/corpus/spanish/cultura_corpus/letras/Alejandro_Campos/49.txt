25 poemas de amor y desamor para dedicar

 En ocasiones nos faltan las palabras para describir aquello que sentimos y nos hace temblar por dentro. Sea el amor, el desamor, la esperanza o las emociones que surgen de ellos, hemos de admitir que encontramos cierta paz y tranquilidad al leer algo con lo cual nos podemos identificar. Y es que hallar en las palabras los pensamientos que nublan nuestra mente y verlos a éstos acomodados en un sentido que nuestra cabeza desconoce, es reconfortante. 
 ¿Cómo podrías describir esa sensación del estómago cuando estás a punto de ver a la mujer que amas? O por qué no evitar decir que tienes un nudo en la garganta cuando la vida te ha sobrepasado. Quizá quieras decirle a esa persona especial que no puede seguir esperando que seguirás ahí, a su lado, cuando ella sólo te da indiferencia. Y claro, siempre resulta motivador leer unas frases que te alienten a seguir, a continuar en esta búsqueda (?) del amor o en la propia. 
 La historia ha consagrado cientos de miles de frases de numerosos escritores que han superado su mortalidad a través del eco de sus palabras. Frases que se estampan en camisetas, con faltas de ortografía en las paredes, en tarjetas de regalo que terminarán en la basura. Frases gastadas al final de cuentas. Sin embargo, dentro de esa misma evolución de las letras, nuevos escritores y poetas luchan porque sus palabras sean leídas, escuchadas, reproducidas y valoradas. 
 Por ello, te compartimos 25 poemas amor y desamor de colaboradores de Cultura Colectiva para este 14 de febrero. 
 Dedícalos, nos los robes. 
 Amor 
  
 Te quiero desde hace mucho de Quetzal Noah 
 El beso matutino de Alfonso Sámano 
 Viajo hacia ti por Tuto 
 La delgada línea de Quetzal Noah   
 Qué importa de Noé Hernández 
 Para que te vayas quedando conmigo de Quetzal Noah Instrucciones para hacer el amor de Quetzal Noah 
 
 Desamor 
  
 Un ratito me basta de Quetzal Noah 
 Cuando no está tu presencia de Estefanía Plasencia 
 Tantas cosas pendientes por Quetzal Noah 
 7 am por Fabrizio Coprez Por ahora no, gracias de Quetzal Noah Murió el amor de Andre Guayasamín Te reservas en mi garganta de Noé Hernández Figueroa Encamados de Jorge Sarquis La errónea idea de Quetzal Noah Dar de Abimael Manzano Me fui enamorando de ti de Noé Hernández Hora de dejarte ir de Quetzal Noah Extrañas ocasiones de Quetzal Noah 
 
 Esperanza 
  
 Te hablo de intentarlo de Quetzal Noah Uno va a declarar su amor de Quetzal Noah Todo estará bien de Noé Hernández Exigir al corazón de Quetzal Noah 
 Estoy sin comprender cómo marcarme en ti de Noé Hernández