25 libros sobre periodismo y comunicación que puedes descargar gratis

 Gabriel García Márquez colocó en un pedestal al periodismo frente a los grandes retos del siglo XX, y en 1996, afirmó que “hace unos cincuenta años no estaban de moda las escuelas de periodismo. Se aprendía en las salas de redacción, en los talleres de imprenta, en el cafetín de enfrente, en las parrandas de los viernes”. Además, el célebre cafetín de las cinco de la tarde brindaba una oportunidad para aprender sobre el oficio, pues “quienes no aprendían en aquellas cátedras ambulatorias y apasionadas de veinticuatro horas diarias, o los que se aburrían de tanto hablar de lo mismo, era porque querían o creían ser periodistas, pero en realidad no lo eran”. ¿Cómo ha cambiado el periodismo descrito por el Premio Nobel casi 20 años después de su discurso en la 52º asamblea de la Sociedad Interamericana de Prensa en Los Angeles? 
  
 El oficio, como lo conocemos, está en una carrera contra su condena y extinción, pues la evolución constante de las tecnologías de la comunicación y el nuevo paradigma del consumo y difusión de información ha sobrepasado los principios del periodismo . Ahora, el mejor trabajo del mundo se aprende a partir de una combinación entre la teoría y la práctica. Las antiguas generaciones de periodistas se sumergen en un mundo de redes sociales, clickbait y algoritmos, en que parece que ya no hay tiempo para la confirmación de la información. Por su parte, las nuevas generaciones que nadan en el mundo de la tecnología parecen no tomar en cuenta los pilares que hicieron del oficio lo que antaño fuera.  La única manera de salvar a la profesión y fortalecer su papel como cuarto poder es aprovechar las herramientas digitales para promover los principios, y en esas herramientas está la posibilidad de aprender, leer y actualizar al oficio en Internet frente a los nuevos paradigmas. Para ello, y para toda las generaciones interesadas, compartimos 25 libros de interés sobre periodismo y comunicación. 
 La compilación pertenece a un listado de 100 libros sobre periodismo y comunicación hecho por Pedro Ylarri, del Blogd elmedio , un espacio con información, herramientas, libros y recursos para aquellos involucrados en periodismo, marketing y publicidad. De aquella selección, te compartimos 25 libros relevantes para aquellos profesionales de la información o para quienes desean internarse más en las esferas del mejor oficio del mundo. 
  
 De la lista total, se eligieron título relevantes que abordan los grandes retos del periodismo en el siglo XXI, manuales de escritura para web, glosarios y un interesante ensayo sobre el copyright en los tiempos de Internet. 
  
 
 1.- Cobertura del Narcotráfico y el Crimen Organizado Centro Knight 
 2.- Cómo escribir para la web Guillermo Franco 
 3.- Comunicación local y nuevos formatos periodísticos en Internet Guillermo López García (ed.) 
 4.- Comunicación multicultural en Iberoamérica José Marques de Melo 
 5.- Contra el copyright Stalmman, Ming, Rendueles y McLeod 
 6.- CryptoPeriodismo. Manual Ilustrado para Periodistas. Fernández y Mancini 
 7.- Cultura Libre.  Cómo los grandes medios están usando la tecnología y las leyes para encerrar la cultura y controlar la creatividad. Lawrence Lessig 
 8.- El futuro del periodismo EVOCA 
 9.- El impacto de las tecnologías digitales en el periodismo y la democracia en América Latina y el Caribe Centro Knight 
 10.- El imperio digital. El nuevo paradigma de la comunicación 2.0 Leandro Zanoni 
 11.- Entre las cenizas: Historias de vida en tiempos de muerte Periodistas de a Pie 
 12.- Glosario básico de Internet Rafael Fernández Calvo 
 13.- Guía de habilidades multimedia para periodistas Mindy Mc Adams 
 14.- Guía de periodismo en la era digital Centro Internacional de Periodistas 
 15.- Herramientas digitales para periodistas Sandra Crucianelli 
 16.- La sociedad de la ignorancia Brey, Innerarity y Mayos 
 17.- Las 10 mejores prácticas para medios sociales james Hohmann 
 18.- Los riesgos del periodismo en tiempos de redes EVOCA 
 19.- Manual de Periodismo de Datos Gray, Bounegru y Chambers 
 20.- Manual de seguridad para periodistas. Cubriendo las noticias en un mundo peligroso y cambiante.  Frank Smyth 
 21.- Manual de periodismo independiente Deborah Potter 
 22.- Periodismo ciudadano. Evolución positiva de la comunicación Espiritusanto y Rodríguez 
 23.- Periodismo en tiempo real Clases de periodismo 
 24.- Valores y criterios de la BBC Asociación de la prensa de Madrid 
 25.- Entrar a la cuarta pantalla. Guía para pensar en móvil Flores, Dorat, Pérez, Raby y Zamorano 
 – Referencia: Blog del Medio 
 – Te puede interesar: Libros que todo periodista debe leer