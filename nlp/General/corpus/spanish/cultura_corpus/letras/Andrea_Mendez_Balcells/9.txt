Descarga gratis las obras completas de los grandes filósofos

   
 “No puedo enseñar nada a nadie. Sólo puedo hacerles pensar”. Sócrates 
 Los seres humanos poseemos una curiosidad inherente que nos hace preguntarnos el qué, cómo, cuándo, dónde y por qué de todo lo que nos rodea. Por naturaleza necesitamos explicaciones y desde el surgimiento de las primeras civilizaciones, ya comenzaban a especular sobre conceptos abstractos como la existencia, la verdad, el conocimiento, la belleza, la mente, la moral… hasta que este conjunto de razonamientos tuvo un nombre: Filosofía. 
 La palabra filosofía, de acuerdo a sus raíces griegas, significa “amor por la sabiduría”; s e dice que Pitágoras (570-497 a. n.e.) fue el primero en utilizar este término cuando se le preguntó qué tipo de sabiduría practicaba, a lo que respondió que él no era un sabio, sino un amante de la sabiduría, es decir, un filósofo. La filosofía e s el conjunto de conocimientos que, de manera racional, intenta determinar las nociones fundamentales que constituyen y rigen la realidad y el fundamento de la acción humana. Siempre hemos necesitado conocernos un poco más para así conocer lo que está a nuestro alrededor.  Debido a que la realidad cambia constantemente y la filosofía se basa en especulaciones o análisis conceptuales, a través de la historia han existido un gran número de pensadores que, de acuerdo a su época, han explicado la vida y la esencia de las cosas. Una buena manera de entender más sobre nuestra existencia, es leer a estos filósofos que aportan distintas visiones sobre la vida, y es por ese amor a la sabiduría que aquí puedes descargar la obra completa de algunos de los filósofos más importantes de todos los tiempos, desde la época antigua, hasta la actualidad.    Época Antigua (Filosofía Greco-Romana)   
 Platón  (427 –  347 a.C.) 
 Aristóteles (384 a.C. – 322 a.C.) 
 Epicuro (341-270 a.C.) 
 Séneca (4 a.C.  – 65 d.C.) 
 Epicteto (55-135) 
 Época Medieval 
 San Agustín 
 Pedro Abelardo 
 Santo Tomás – Summa Theologica en 5 tomos 
 Guillermo de Ockham 
 Época Moderna 
 Descartes 
 Spinoza 
 Locke 
 Leibniz 
 Hume 
 Kant 
 Herder   
 Giambattista Vico 
 Hegel 
 Schopenhauer 
 Jaime Balmes (5 libros) 
 Adam Smith 
 John Stuart Mill 
 Época Contemporánea  
 Kierkegaard 
 Marx y Engels (obras en tres tomos) 
 Ernesto Che Guevara 
 Freud 
 Nietzsche 
 Henri Bergson 
 Wilhelm Dilthey 
 William James 
 Wittgenstein 
 Simone Weil    Sartre y Beauvoir 
 Michel Foucault 
 Walter Benjamin 
 Deleuze 
 Habermas 
 Michel Onfray (13 obras) 
 Fuente 
 Holismo Planetario