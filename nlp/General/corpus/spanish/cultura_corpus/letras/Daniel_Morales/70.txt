72 libros que todo amante de la literatura debe de haber leído antes de los 30

 En la literatura encontramos lo que no somos y nutrimos esa otredad a nuestro beneficio. A partir de conocer otras realidades, otras verdades y mentiras, somos capaces de analizar lo que realmente nos sucede. Esa magia literaria es la que hace que muchos sean adictos a crecer a partir de las experiencias de otras personas. Son muchos los libros trascendentales y no podemos dar una fecha exacta de cuándo deberían ser leídos, pero creemos que los que mencionaremos a continuación deberían ser leídos antes de los 30 años. 
  
 Esa edad para muchos se convierte en un parteaguas en su vida. Se deja atrás el tan preciado segundo piso y se espera llegar con la experiencia y la gracia dignos de alguien que ha vivido, pero que también se ha cultivado. Definitivamente faltan libros aquí, una lista siempre puede ser mejorada, pero por ahora, el equipo de Cultura Colectiva considera que estos libros pueden enseñarte bastante acerca de la vida, el amor, el dolor y mucho más; todo para que continúes creciendo literaria y personalmente. 
 “100 Años de Soledad” (1967) – Gabriel García Márquez 
 “The Sound and the Fury”  (1929) – William Faulkner 
 “El Principito” (1943) – Antoine de Saint-Exupery 
 “The Catcher in the Rye” (1951) – J.D. Salinger 
 “La Virgen de los Sicarios” (1994) – Fernando Vallejo 
 “1984” (1949) – George Orwell 
 “Fahrenheit 451″(1953) – Ray Bradbury  “El Príncipe” (1513) – Nicolás Maquiavelo 
 “Los Detectives Salvajes” (1998)  – Roberto Bolaño 
 “Diablo Guardián” (1964)  – Xavier Velasco 
 “Ficciones” (1944) – Jorge Luis Borges 
 “Así habló Zaratustra” (1883)  – Friedrich Nietzsche 
 “Rayuela” (1963)  – Julio Cortázar  
 “The Doors of Perception” (1954) – Aldous Huxley  “La insoportable levedad del ser” (1984) – Milan Kundera 
 “Amor líquido. Acerca de la fragilidad de los vínculos humanos” (2005)  – Zygmunt Bauman 
 “On the Road” (1957) – Jack Kerouac 
 “In Cold Blood” (1965) – Truman Capote 
 “Pedro Páramo” (1955) – Juan Rulfo 
 “Niebla” (1907) – Miguel de Unamuno 
 “The Road” (2006) – Cormac McCarthy  “Por quien doblan las campanas” (1940) – Ernest Hemingway 
 “El Coronel no tiene quien le escriba” (1961)  – Gabriel García Márquez 
 “El Barón rampante” (1957) – Italo Calvino 
 “Rojo y Negro” (1830) Stendhal 
 “La metamorfosis” (1915)  – Franz Kafka 
 “El extranjero” (1942) – Albert Camus 
 “Lolita” (1955) – Vladimir Nabokov  “Ana Karenina” (1877) – León Tolstói 
 “Azul” (1888)  – Ruben Darío 
 “Ciudades desiertas” (1982) – José Agustín 
 “La Divina Comedia” (1313) – Dante Alighieri 
 “La Iliada y La Odisea” (S. VIII A.c.) – Homero  
 “20 mil leguas de viaje submarino” (1870) – Julio Verne 
 “El tambor de hojalata” (1959) – Günter Grass  “Macbeth” (1611) – William Shakespeare 
 “El Lazarillo de Tormes” (1554) – Anónimo 
 “The Picture of Dorian Grey” (1890) – Oscar Wilde 
 “El lobo estepario” (1927) – Hermann Hesse 
 “Animal Farm” (1945) – George Orwell 
 “Drácula” (1897) – Bram Stoker 
 “The Hitchhiker’s Guide to the Galaxy” (1979) – Douglas Adams  “Ham on Rye” (1982) – Charles Bukowski 
 “Siddhartha” (1922) – Hermann Hesse 
 “Tom Sawyer”  (1876) – Mark Twain 
 “Lord of the Flies” (1954) – William Golding 
 “The Brief Wondrous Life of Oscar Wao” (2007) – Junot Díaz 
 “Ciudad de cristal” (1985) – Paul Auster  
 “Los pilares de la Tierra” (1989) – Ken Follett  “La montaña mágica” (1924) – Thomas Mann 
 “Ensayo sobre la ceguera” (1995) – José Saramago 
 “At the Mountains of Madness” (1936) – H. P. Lovecraft 
 “El testigo” (2004) – Juan Villoro 
 “Espejos” (2008) – Eduardo Galeano 
 “Crimen y castigo” (1866) – Fiodor Dostoyevsky 
 “High Fidelity” (1995) – Nick Hornby  “Naked Luch” (1962) – William Burroughs 
 “La filosofía en el tocador” (1795) – Marqués de Sade 
 “Madame Bovary” (1856) – Gustave Flaubert 
 “El perfume” (1985) – Patrick Süskind 
 “Crónica del pájaro que da vuelta al mundo” (1994) – Haruki Murakami 
 “Mrs. Dalloway” (1925) – Virginia Woolf 
 “El viejo y el mar” (1952) – Ernest Hemingway  “Trilogía de la noche” – Elie Wiesel (1955) 
 “El Nombre de la Rosa” (1980) – Umberto Eco 
 “Oliver Twist” (1837) – Charles Dickens 
 “Alice in Wonderland” (1865) – Lewis Carroll 
 “The Bonfire of the Vanities” (1987) – Tom Wolfe  
 “El Emperador” (2002) – Ryszard Kapuściński 
 “Atonement” (2001) – Ian McEwan 
 “Strange Case of Dr. Jekyll and Mr. Hyde” (1886) – Robert Louis Stevenson 
 “The Lord of the Rings” (1954) – J. R. R. Tolkien ¿Encontraste tu libro favorito o necesitamos hacer crecer esta lista? Tal vez faltan exponentes como Joyce, Proust o Wallace, pero creemos que esos pueden servir en otra etapa de la vida.  Aquí buscamos formación y claro, lineas tan poéticas como poderosas que puedan ayudarte a superar cualquier crisis existencial. 
 *** 
 Te puede interesar: 
  Libros para creer en el amor verdadero 
 10 libros para leer con tu pareja