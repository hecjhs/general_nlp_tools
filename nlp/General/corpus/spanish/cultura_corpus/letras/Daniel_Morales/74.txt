Aprende a hablar Nadsat, el lenguaje de la Naranja Mecánica

 “Dobos drugos. Que felicidad contar con su presencia para la primer clase de nadsat. Una polesño forma de goborar que te servirá en los momentos más difíciles. Esos cuando estás en una bitha contra un bolche y brachno brato que se cree mejor que tú. Dorogos drugos, sé que creen que un malenco de moloco es todo lo que necesitas para ganar una bitha, pero déjenme darles una soviet: las turkas siempre pueden ser la diferencia entre ganar o perder. Por eso te recomiendo que memorices y te aprendas las siguientes slovos, pues la yasicca es la mejor puschca. Si las turkas no funcionan, siempre está el confiable cluvo en la golová para callarlos por siempre”. Su drugo, Alex. 
  Las palabras de Alex, el protagonista del famoso libro “La Naranja Mecánica” son parte de una jerga futurísta. Creadas por el autor Anthony Burgess a partir de distintas lenguas eslavas, pero en su mayoría el ruso. Tal vez el libro no sea tan conocido hoy, pero sin duda la película dirigida por Stanley Kubrick sí lo es y ahí hay un pequeño problema, pues la cinta tuvo que ser adaptada para la comprensión del público, por lo que el trabajo del lingüista y novelista fue reducido para favorecer al espectador. 
  Nadsat es el lenguaje de la juventud. Su definición viene de la palabra “adolescente” en ruso. Esta representa el paralelismo con la denominación “teen” inglesa. En ruso todos los números del 11 al 19 terminan en “nasdat” así como en inglés del 13 al 19 son finalizados con el termino “teen”. Rebelión juvenil y adolescente. Alex se esfuerza por ser un problema para la sociedad y su mentalidad iracunda y agresiva representan lo peor de esos que son considerados “el futuro”. 
  
 Hablar nadsat requiere entender la profundidad que el escritor quiso darle a su juvenil apropiación. Inglés y ruso unidos en un idioma. Recordemos que el libro fue escrito en 1962, momento en el que la Guerra Fría vivía en el imaginario de todos. Esperar que los comunistas y los capitalistas convivieran en el presente no parecía una buena idea, por lo que “La Naranja Mecánica” fue situada en un fututo distópico (el origen de la leche que genera ultraviolencia seguramente podría generar una gran película de ciencia ficción): 
 “-Muy curioso -comentó el doctor Brodsky. – Ese dialecto de la tribu. ¿Sabe usted de dónde viene, Branom? -Fragmentos de una vieja jerga -dijo el doctor Branom, que ya no tenía un aire tan amistoso-. Algunas palabras gitanas. Pero la mayoría de las raíces son eslavas. Propaganda. Penetración subliminal.” 
 Soviéticos y americanos conviviendo juntos, la falacia era evidente. Incluso remata el chiste insertando palabras infantiles en su jerga. La versión en inglés establece que “disculpa” pasa de ser “apology” a “appy polly loggy”. 
  
 Según Burgess la jerga fue creada porque sabía que si usaba el lenguaje de moda en ese momento, la obra tendría una temporalidad (pensemos en “The Catcher in the Rye” , mientras el mensaje es atemporal, su protagonista siempre vivirá en los años cincuenta) y pronto sería anticuada. Burgess logró hacer del libro una obra adecuada siempre en el futuro y gran parte funcionó por la forma de hablar de Alex. El nadsat será siempre un dialecto de los jóvenes, no se adaptará dentro de un moda y morirá. Alguien puede leer “La Naranja Mecánica” en 50 años y sentir que Alex es un adolescente de su época hablando. Por eso te dejamos con las siguientes palabras que el autor del libro ayudó a traducir al español para la primer edición de “La Naranja Mecánica” en español: 
 Apología: Disculpas Bábuchca: Anciana Besuño: Loco Biblio: Biblioteca Bitha: Pelea Bogo: Dios Bolche: Grande Bolnoyo: Enfermo Boloso: Cabello Brachno: Bastardo 
 Brato: Hermano Bredar: Lastimar Britba: Navaja Brosar: Arrojar Bruco: Vientre Bugato: Rico Cala: Excremento Camison: Blusa Cancrillo: Cigarrillo Cantora: Oficina 
  
 Carmano: Bolsillo Cartófilo: Patat lopar: Golpear, Llamar Cluvo: Pico Colcolo: Campanilla Copar: Entender Coschca: Gato Coto: Gato Cascar: Morir Carola: Amor 
 Cacar: Golpear, Sestruir Crarcar: Aullar, Gritar Crastar: Robar Crichar: Gritar Criuba: Sangre Crunchy: Masticar Cuperar: Comprar Chai: Té Chaplino: Sacerdote Chascha: Taza 
  
 Chaso: Guardia Chavalco: Chico Cheloveco: Individuo Chepuca: Tonteria China: Mujer Chilumpear: Atrasar Chisna: Vida Chistar: Lavar Chovelo: Bruto Chudesño: Extraordinario 
 Chumchum: Ruido Chumlar: Murmurar Crobo: Sangre Débochca: Muchacha Dedón: Viejo Dengo: Dinero Dobo: Bueno, Bien Domo: Casa Dorogo: Estimado, Valioso Dratsar: Pelear 
  
 Drencrom: Droga Drugo: Amigo Duco: Asomo, Pizca Dva: Dos Eme: Mamá Evaporar: Morir Estepalo: Pico Filosa: Mujer Fons: Precioso Forella: Mujer 
 Fuegodoro: Bebida Gasetta: Diario Glaso: Ojo Gloria: Cabello Glupo: Estúpido Goborar: Hablar, Conversar Goli: Unidad de moneda Golosa: Voz Golová: Cabeza Gorlo: Garganta 
  
 Grasño: Sucio Gronco: Estrepitoso, Fuerte Grudos: Pechos Guba: Labio Gular: Caminar Gulivera: Cabeza Imya: NombreInteresobar: Interesar Itear: Ir, Caminar, Ocurrir Joroschó: Bueno, Bien Klebo: Pan 
 Kroovy: Sangre Krunchi Krunchi: Comer Lapa: Pata Litso: Cara Liudo: Individuo Lontico: Pedazo, Trozo Lovetar: Atrapar Lubilubar: Hacer el amor Ma: Mamá Málchico: Muchacho 
  
 Malenco: Pequeño, Poco Maluolo: Mal, Malo Maslo: Manteca Mersco: Sucio Meselo: Pensamiento, Fantasía Mesto: Lugar Militso: Policía Minuta: Minuto Molodo: Joven Moloco: Leche 
 Mosco: Cerebro Munchar: Masticar, Comer Metesaca: Fornicar Nachinar: Empezar Nadmeño: Arrogante Nadsat: Adolescente Nago: Desnudo Naso: Loco Naito: Noche Niznos: Calzones 
  
 Nocho: Cuchillo Noga: Pie, Pierna Nopca: Botón Nuquear: Oler Ocno: Ventana Ochicos: Lentes Odinoco: Solo, Solitario Odin: Uno Osuchar: Borrar, Secar Papapa: Papá 
 Pe: Papá Pianitso: Borracho Pischa: Alimento Pitear: Beber Placar: Llorar Platis: Ropas Plecho: Hombro Plenio: Prisionero Plesco: Salpicadura Ploto: Cuerpo 
  
 Poduchca: Almohada Polear: Copular Polesño: Útil Polillave: Llave maestra Ponimar: Entender Prestúpnico: Delincuente Privodar: llevar, conducir Ptitsa: Muchacha Puglio: Miedos Puschca: Arma de fuego 
 Quijotera: Cabeza Quilucho: Llave Quischcas: Tripas Rabotar: Trabajar Radosto: Alegría Rascaso: Cuento, Historia Rasdrás: Enojo, Cólera Rasrecear: Trastornar, Destrozar Rasudoque: Cerebro Rota: Boca 
  
 Ruca: Mano, Brazo Sabogo: Zapato Sacarro: Azúcar Samechato: Notable Samantino: Generoso Sarco: Sarcástico Sasnutar: Dormir Scasar: Decir Scolivola: Escuela Scorro: Rápido 
 Scotina: Vaca Scraicar: Arañar Scvatar: Agarrar Schaica: Pandilla Scharros: Nalgas Schesto: Barrera Schiya: Cuello Schlaga: Garrote Schlapa: Sombrero Schlemo: Casco 
  
 Schuto: Estúpido Silaño: Preocupación Siny: Cine Sladquino: Dulce Slovo: Palabra Sluchar: Ocurrir Slusar: Oír, Escuchar Smecar: Reír Smotar: Mirar Snito: Sueño 
 Snufar: Morir Sobirar: Recoger Sodo: Bastardo Soviet: Consejo, Orden Spatar: Dormir Spachca: Sueño Spugo: Aterrorizado Staja: Cárcel Starrio: Viejo, Antiguo Straco: Horror 
  
 Subos: Dientes Sumca: Mujer vieja Svonoco: Timbre Svuco: Sonido, Ruido Synthemesco: Droga Talla: Cintura Tastuco: Pañuelo Tolchoco: Golpe Talkshockear: Agredir verbalmente Tri: Tres 
 Tuflos: Pantuflas Turka: Palabra Ubivar: Matar Ucadir: Irse Uco: Oreja Uchasño: Terrible Umno: Listo Unodós: Cópula, Copular Usy: Cadena Varitar: Preparar 
  
 Veco: Individuo, Sujeto Velocet: Droga Vesche: Cosa Videar: Ver Vidrio: Ojo Vono: Olor Warble: Canción Yajudo: Judío Yarbloco: Testículo Yama: Agujero 
 Yarmoles: Testículos Yasicca: Lengua Yecar: Manejar un vehículo 
  
 “Así que ya lo saben drugos. La próxima vez que algún sodo los tenga por los yarmoles, intenten hablar, pues el svuco de la humillación puede ser más satisfactorio que el del schlaga rompiendo los subos”. 
 *** 
 Te puede interesar:  
 2o cosas que deberías saber sobre La Naranja Mecánica 
 Tatuajes de la Naranja Mecánica para recordar a Kubrick 
 *** Fuente: soomka