Frases que sólo comprenderán quienes se han decepcionado del amor

 
 ¿Cómo expresar algo que no entendemos? El amor es un sentimiento que desborda la razón. Éste llega a ser tan grande que por eso no es necesario decirlo, la gente puede ver que estamos enamorados a la distancia. Se va de nuestro cuerpo, transforma nuestra vida, visión y entorno; nos convierte en los seres más felices en la Tierra… hasta que deja de hacerlo. 
 “¿Sabes? Un corazón puede estar roto, pero aún así sigue latiendo”. El efecto sigue las leyes físicas y la reacción es igual de potente. El desamor duele más allá del entendimiento humano y no importa que muchos lo hayan vivido, cuando de te dicen “te entiendo” sabes que nadie puede comprender lo que pasa por tu cabeza. Una relación puede terminar, pero el desamor se apega a nosotros por tiempo indefinido y debemos soportar esa bruma que no nos deja ver que la vida sigue, que la felicidad existe y que el tiempo cura las heridas… no, por ahora no hay más que una pasividad imposible de aplacar. 
  
 A veces uno, a pesar de siempre decir que no sucedería, deja de amar a su pareja, a veces es al revés. ¿Qué termina con el amor? La rutina, el tiempo, las infidelidades, la falta de compromiso, los accidentes y las promesas. La lista puede ser infinita y las respuestas imposibles, y tal vez intentar entender no es lo mejor, pero sí algo inevitable. Las siguientes frases muestran esa triste realidad, las grandes plumas de la literatura han capturado con precisión lo que un corazón roto exclama y si tú no sabes cómo decir lo que sientes, tal vez estas palabras te puedan ayudar.  “Había contraído contigo compromisos imprudentes y la vida se encargó de protestar: te pido perdón, lo más humildemente posible, no por dejarte, sino por haberme quedado tanto tiempo”. Marguerite Yourcenar 
 “La peor forma de extrañar a alguien es estar sentado a su lado y saber que nunca lo podrás tener”. Gabriel García Márquez 
 *Si deseas saber más acerca del gran Gabo, puedes leer más frases de él aquí . 
 “Lo contrario del amor no es el odio, sino la indiferencia”. Ellie Wiesel 
 “Cuando el sol se ha puesto, ninguna vela lo puede reemplazar”. George R.R. Martin 
 “El amor es una enfermedad inevitable, dolorosa y fortuita”. Marcel Proust 
  
 “El corazón fue hecho para romperse”. Oscar Wilde 
 “Amar duele. Es como entregarse a ser desollado y saber que en cualquier momento la otra persona podría irse llevándose tu piel”. Susan Sontag 
 “La cuerda cortada puede volver a anudarse, vuelve a aguantar, pero está cortada. Quizá volvamos a tropezar, pero allí donde me abandonaste no volverás a encontrarme”. Bertolt Brecht 
 “No permitas que alguien sea tu prioridad cuando tú sólo eres una opción”. Mark Twain 
 “También te amo, pero mi tiempo contigo ha terminado”. Garth Nix 
  
 “La persona que amas y la persona que te ama nunca son la misma persona”. Chuck Palahniuk 
 “¿Sabes? Un corazón puede estar roto, pero aún así sigue latiendo”. Fannie Flagg 
 “Viviendo con alguien que amas puedes sentir más soledad que viviendo completamente a solas… Si quien amas no te ama”. Tennessee Williams 
 “Para olvidar a alguien hay que volverse extremadamente metódico. El desamor es una especie de enfermedad que solamente puede combatirse con rutina”. Veronica Gerber 
 “Era demasiado joven para saber amarla”. Antoine de Saint-Exupéry 
  
 “El corazón perece de una muerte lenta. Se desprende de cada esperanza como si fueran hojas hasta que un día no queda ninguna. Ninguna esperanza”. Arthur Golden 
 “Si a usted le negaran de una manera tan rotunda el casarse con la mujer que ama y la única salida que le dejaran para estar cerca de ella fuera el casarse con su hermana, ¿usted no tomaría la misma decisión que yo?”. Laura Esquivel 
 “Es al separarse cuando se siente y se comprende la fuerza con que se ama”. Fiódor Dostoyevski 
 “No ser amado es una simple desventura. La verdadera desgracia es no saber amar”. Albert Camus 
   “En el amor todo ha terminado cuando uno de los amantes piensa que sería posible una ruptura”. Paul Bourget 
  
 “El amor hace que el tiempo pase; el tiempo hace que el amor pase”. Eurípides 
 “El amor es un misterio. Todo en él son fenómenos a cual más inexplicable; todo en él es ilógico, todo en él es vaguedad y absurdo”. Gustavo Adolfo Bécquer 
 “¿Por qué si el amor es lo contrario a la guerra es una guerra en sí?”. Benito Pérez Galdós 
 “Ofrecer amistad al que pide amor es como dar pan al que muere de sed”. Ovidio 
 “¿Sabe lo mejor de los corazones rotos? Que sólo pueden romperse de verdad una vez. Lo demás son rasguños”. Carlos Ruiz ZafónSi logras expiar el desamor y estás listo para algo nuevo, estas frases de amor pueden ser la mejor medicina. Aunque si deseas seguir con la tortura, puedes cambiar el arte y liberar tu dolor en compañía de las películas más tristes.*Fuente: Toda las fotos son de Laura Makabresku