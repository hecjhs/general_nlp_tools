Libros que debes leer para entender tus series favoritas

 
 Hace poco terminé de ver la primer temporada de “Stranger Things” y me invadió una sensación de desasosiego. Esto suele suceder cuando se termina un libro, pero no una serie. Existen excepciones y es porque hay personajes con los que nos identificamos de tal forma que es difícil no sentir cariño por ellos y preocuparnos por su destino, por lo que al finalizar la serie no podemos evitar sentir una extraña nostalgia. “Friends”, “Breaking Bad” y la antes mencionada son algunas de las series que nos hacen apegarnos emocionalmente a los personajes, y ahora con la insana manía de ver una serie lo más rápido posible. 
 “Puedes creer que ‘It’ es un libro acerca de un monstruo, pero va mucho más lejos y seguramente el final te dejará llorando para después convertirse en una de tus lecturas favoritas”.   
 Esa sensación de abandono y fugacidad puede ser llenada con un libro que refleje lo que viste en pantalla. También pueden ser devoradas, pero las piezas literarias suelen tardar más tiempo en terminarse, y con una historia que nutra lo que vimos en televisión, sentirás que continúas viendo esos capítulos que tanto te atraparon. Las siguientes obras pueden ser el preámbulo bajo el que se crearon los programas que se mencionan, pero también pueden significar historias que te harán identificar lo que puede o no suceder en ese programa que tanto amas, puede darte pistas de adónde se moverá la trama o un aspecto más profundo de la psicología de los personajes. 
 – Narcos “Matar a Pablo Escobar” (2001) – Mark Bowden 
 Seis capítulos que dividen la era de Escobar, desde los atentados que se presencian en la serie como el avión derribado con una bomba, hasta lo que esperamos ver en futuras temporadas, como el gigantesco operativo para dar con el narco más importante de la historia e incluso la historia de “los Pepes”, esos enemigos clandestinos de Escobar que deseaban verlo muerto, tal vez incluso más que el gobierno. Además, el libro hace un profundo análisis de la vida en esa época y del pensamiento e idiosincracia del colombiano, por lo que podemos entender el horror desde distintos puntos de vista. 
  
 – 
 Orange is the New Black “Orange is the New Black: My Year in a Women’s Prison” (2010) – Piper Kerman 
 La obra de Kerman fue fundamental para la existencia de la serie, pero eso no implica que el final que ella tuvo en una cárcel de mujeres será el mismo para la Piper de ficción. Como bien sabemos, las series basadas en obras literarias suelen alejarse de la trama original para dar paso a la creatividad de los escritores en turno. No te arruinaremos el libro por si llegan a adaptar parte del final para la serie, pero sí te recomendamos que leas la autobiografía de la exreclusa para entender a profundidad a tus personajes favoritos de la serie, pues aunque en ella se pueden ver muchas facetas de alguien, el texto de Kerman habla con una honestidad que la televisión no puede reflejar. 
  
 – 
 Game of Thrones “War of the Roses” (2013) – Conn Iggulden 
 Claro, podríamos haber recomendado la saga de “A Song of Ice and Fire”, pero es demasiado obvio. Por otro lado, las novelas acerca de la guerra en la que George R.R. Martin se inspiró para la serie puede llevarte a un mundo menos fantástico, pero no por eso menos peligroso, caótico y deprimente. La Guerra de las Rosas o Guerra de las dos Rosas fue una guerra civil que enfrentó a la Casa de Lancanster contra la Casa de York en el siglo XV. A partir de esos conflictos sucedieron matrimonios, venganzas, pactos y muchos asesinatos, que hoy podemos ver reflejados en las atrocidades que los Lannister, Stark, Tyrell y otras casas del mundo de Martin que se muestran en televisión. 
  
 – 
 Stranger Things “It” (1986) – Stephen King 
 Muchas novelas y series son homenajeadas en esta serie, desde otros clásicos de King hasta la novela gráfica “Akira”, pero sin duda, “It” es parte de la esencia del show. Un grupo de amigos se enfrenta a una misteriosa criatura que los atormenta y amenaza con destruirlos; sin ayuda de los padres deben intentar salvarse y salvar al mundo. Tal como lo dicen en la cinta “Stuck in Love”, puedes creer que “It” es un libro acerca de un monstruo, pero va mucho más lejos y seguramente el final te dejará llorando para después convertirse en una de tus lecturas favoritas. 
  
 – 
 House of Cards “Macbeth” (1606) y “Ricardo III” (1591) – William Shakespeare 
 Esta serie también surgió de un libro, pero la versión estadounidense se centró mucho más en las tragedias shakespereanas para darle un giro universal a sus personajes. Frank Underwood bien podría ser Ricardo III y Claire reflejarse en el personaje de Lady Macbeth. Si deseas conocer más acerca de la teoría de qué pasará en “House of Cards” por sus orígenes trágicos, puedes hacerlo aquí . 
  
 – 
 Lost “Lord of the Flies” (1954) – William Goulding 
 Un grupo de sobrevivientes en una isla, eso es lo que caracteriza a la serie y al libro, pero mientras el libro de Goulding retrata a unos niños que muestran el lado más oscuro de la condición humana, la serie intenta actuar como un laboratorio de la humanidad y de lo que puede suceder si la gente decide actuar junta en contra de lo desconocido. Aún así, vale la pena leer el libro “Lord of the Flies” para entender esos momentos de Lost en los que vale más la supervivencia que la hermandad. 
  
 – 
 The Sopranos “Honor thy Father” (1973) – Gay Talese 
 Uno de los grandes libros acerca de la mafia fue escrito en la misma época que se estrenó la cinta “The Godfather”, pero este libro fue relevante a nivel mundial hasta finales de los noventa cuando The Sopranos se estrenó. El ascenso y caída de una familia de la mafia en Estados Unidos es el tema central de libro de Talese, y la serie es justamente la vida de una familia de la mafia, pero sin incorporar esos elementos románticos que “The Godfather” muestra. Apegada a la realidad contemporánea, la serie se ha convertido en una de las más importantes referencias televisivas, siendo considerada una de las más importantes de la historia, y el libro “Honor thy Father” te demostrará el realismo que tiene la serie. 
  
 – 
 Mr. Robot “Free Software, Free Society: Selected Essays of Richard M. Stallman” (2002) – Richard Stallman 
 Una serie difícil de entender al 100 %, pero que no por eso es mala. La privacidad y la libertad siempre están en juego en una de las series más inteligentes jamás creadas y los ensayos de Richard Stallman te volverán aún más paranoico después de leerlos. Un enfoque filosófico y social a todo lo relacionado con software y la vida digital; los tratados de Stallman te harán entender la vida tal como la ve Elliot Anderson en la serie. 
  
 – 
 Tal vez al principio no lo notes, pero al revivir tu serie o ver la nueva temporada, encontrarás mucho más fácil adivinar qué hará alguien o cuáles son sus motivos verdaderos. Los libros siempre son parte fundamental del séptimo arte, si quieres saber cómo terminará una película antes de verla, puedes entenderlo a partir de métodos literarios, y si sólo deseas ver una buena película, esta es una recomendación de los estrenos de Netflix que seguramente disfrutarás.