Siete claves para saber cómo van a terminar las películas antes de que las veas

 
 ¿Conoces a esa persona que a la mitad de la película te puede decir quién es el misterioso personaje detrás de todas las desapariciones, o que incluso desde el inicio puede decirte si esos románticos en pantalla encontrarán el amor o se quedarán solos? Muchas veces los patrones se repiten en el cine y si la película es mala, puede ser predecible, pero hay otras que contra todo pronóstico, son resueltas por ciertas personas que parecen tener un talento natural para predecir los finales. Esto es un poco más difícil en los libros, pero se relaciona, pues todo parte de los conocimientos de literatura. 
 “El protagonista está abierto a vivir nuevas experiencias y termina en una tierra extraña. A partir de ese momento tendrá que encontrar el camino a casa que lo hará crecer internamente”. Claro que la teoría del cine aporta mucho, también ver muchas películas y leer novela tras novela, pero finalmente se puede reducir a simples conceptos tan antiguos como nuestra cultura. Se dice que no existen historias nuevas, pues todas son alteraciones de algo existente, la originalidad radica en la forma en que se altera un escenario para hacerlo fresco de nuevo. Si deseas escribir una historia original no debes temer, pues estos lineamientos no implican que así deban ser todas las historias, y claro que los conceptos se pueden mezclar para darnos excelentes historias que realmente son difíciles de predecir. 
  
 En su libro “The Seven Basic Plots”, Christopher Booker muestra las siete tramas básicas de las que surgen todas las historias. En su libro desmenuza todo y parece que en realidad muestra nueve, pero hay quienes incluso dicen que hay 20 o más tramas básicas. Aquí te presentamos las que Booker refiere en su libro, junto con algunos ejemplos para ejemplificar que realmente cualquiera puede pronosticar el final de las historias. 
 – La metatrama 
 No es una trama en sí, sino la estructura que muchas historias tienen. Esta se compone de: anticipación, sueño, frustración, pesadilla y resolución. La anticipación es el inicio en el que el personaje (o héroe) se enfrenta al cambio. Así como en todas las películas te presentan a un personaje junto a su rutina y después ves cómo algo se altera en ella (la muerte de un familiar, perdida de empleo o cambio radical de estilo de vida). 
 El sueño es cuando el personaje logra ciertas victorias y aparentemente todo va bien, pero como el nombre lo indica, resulta ser más una ilusión debido a los problemas a lo que realmente no se ha enfrentado. Es aquí cuando llega la frustración, en la que el enemigo se muestra realmente, empequeñeciendo al personaje, o cuando otro personaje cercano muere. 
  
 Esta etapa suele terminar cuando algo una nueva línea se abre en la historia y el personaje debe tomar una nueva actitud. La pesadilla es el momento en el que todo parece perdido, pues a pesar de la nueva agenda, nada parece funcionar para el héroe y todo va de mal en peor; todo se da por vencido y justamente en el momento en el que el mal está por ganar, el héroe tiene una resolución; ésta es la última etapa de la trama, en la que el héroe, en contra de todo pronóstico, sale triunfante. 
 Esta es la trama esencial de todas las películas comerciales que hay en el cine. Un ejemplo puede ser la película “Mean Girls”. Una chica sale de su zona de confort y llega a un nuevo escenario en el que gana amigos y todo parece ir bien para ella, pero al ser traicionada, decide planificar una venganza, debido a eso pierde a sus amigas y aunque poco a poco se convierte en alguien popular, todo se viene abajo cuando descubren su doble vida, finalmente busca la redención y su vida encuentra un nuevo balance. 
 El resto de las tramas son las siguientes: 
 Venciendo al monstruo 
  
 El héroe se aventura a vencer a una criatura maligna o fuerza exterior que busca destruir su hogar. Suele desarrollarse cuando se descubre la amenaza del monstruo, cuando el protagonista siente la llamada para vencerlo, la preparación para enfrentarlo, la batalla y la consecuente victoria y celebración. 
 En este punto se encuentra la película “Star Wars: A New Hope”, que juega a la perfección con otra teoría: el camino del héroe. Luke es un joven que busca aventuras y, cuando su familia es asesinada y su hogar destruido, comienza un viaje lleno de aventuras en el que el conocimiento y el peligro lo hacen más sabio y poderoso. En la literatura podemos pensar en el antiguo poema “Beowulf” o “Harry Potter”. 
 – 
 De la pobreza a la riqueza 
  
 La favorita de las telenovelas y las antiguas películas de Disney. El héroe comienza con una humilde vida, y gracias a su bondad y a sus actos, termina por enriquecerse, claro que también esto puede llegar a ser metafórico. En ciertas ocasiones el personaje es pobre y prueba la riqueza, pero al final de la historia vuelve a sus humildes orígenes, pero con una gran lección de vida. 
 “Cenicienta”, “Aladdin”, “Great Expectations” o “Jane Eyre” son ejemplo de esta trama. 
 – 
 La búsqueda 
  
 El héroe tiene una vida ordinaria o aburrida hasta que algo sucede y debe cambiar su suerte, por lo que se entera de una búsqueda que lo llevará a lejanos lugares. Después se enfrenta a distintas adversidades que cerca del final lo harán encontrarse con un enemigo, el cual, dependiendo de su destino, hará que el héroe consiga lo que desea, regrese con las manos vacías o peor aún, muera. 
 “El señor de los anillos”, en lugar de encontrar un objeto, busca eliminarlo, pero de igual manera es considerado uno de los más grandes viajes en la historia de la literatura. De igual manera está “La Odisea”, “Harry Potter y las reliquias de la muerte” o “The Talisman” son ejemplos de una trama de búsqueda. 
 – 
 “Viaje y retorno” 
  
 Parecida a la búsqueda, el protagonista está abierto a vivir nuevas experiencias y termina en una tierra extraña. A partir de ese momento tendrá que encontrar el camino a casa que lo hará crecer internamente, para finalizar en su lugar de origen pero con una gran lección de vida. 
 “Almost Famous” tiene a Will como protagonista, quien emprende un viaje con la banda Stillwater por todo Estados Unidos con la misión de entrevistarlos. A pesar de que al final Will regresa a casa y continúa con su vida normal, las experiencias que tuvo a lo largo de la gira le abren las puertas para un futuro prometedor. 
 – 
 Comedia 
 Las comedias románticas tiene mucho de esto, pues se trata de una situación en la que los personajes deben superar una adversidad y enfrentarse a distintos problemas que resultan en un final feliz (bodas, reuniones o más). Más que risas, puede ser también cuando los patrones se vuelven más y más confusos. “A Midsummer Night’s Dream”, de Shakespeare, “El diario de Bridget Jones” o las novelas de Jane Austen siguen esos confusos patrones, aunque no se pueden negar los hilarantes momentos cómicos que tienen. 
 – Tragedia 
  
 A diferencia de la comedia, la tragedia, género establecido desde la antigua Grecia con las celebraciones a Dionisio, resulta en finales tristes, muchas veces debido a que el personaje principal intenta ser o aparentar más de lo que realmente puede. Tradicionalmente culmina con el castigo del personaje, quien muere como condena; claro que actualmente los géneros se invierten llevando al personaje a un destierro, exilio o destinos que muestran que ni con arrepentimiento podrá regresar a ser lo que era. 
 Aquí el protagonista se siente incompleto, busca un objeto que desea para conseguir todo lo que quiere, después lo consigue y poco a poco cae en un agujero de soledad y maldad que empeora cada vez más hasta que es destruido por culpa del objeto. Si bien “El señor de los anillos” no es una tragedia, la vida de Gollum la representa. 
 – 
 Renacimiento 
  
 Cuando el protagonista se ve amenazado por una fuerza externa, las cosas parecen mejorar para él, pero tiempo después ese poder regresa y lo lleva a un estado finito en el que parece ser destruido o asesinado, sin embargo, el personaje logra “renacer” debido a ciertas circunstancias que lo hacen más fuerte y así logra vencer el mal. 
 Ultimamente está de moda hacer que el malo se convierta en un buen personaje. Películas como “Megamind” o “Despicable Me” lo muestran a la perfección y “Beauty and the Beast” mostraron ese camino a la redención hace décadas. 
 Conociendo la metatrama y las tramas es sencillo predecir el final de muchas películas y algunos libros. Aunque recuerda que es de la mezcla y la adaptación que surgen las historias originales. Así es como podemos tener una serie como “Breaking Bad” en la que se ve un camino a la perdición, también un renacimiento con tintes del camino del héroe y más, convirtiéndola en algo mucho más que un simple drama. 
 – Si quieres poner a prueba esto, te recomendamos analizar películas consideradas como las mejores chick flicks , así como películas filosóficas . De esa forma tendrás una percepción amplia de lo que significa ver películas. Puedes hacer lo mismo con los libros, pues  los que son para jóvenes adultos pueden ser mucho más predecibles que los que hablan de temas aún más profundos.