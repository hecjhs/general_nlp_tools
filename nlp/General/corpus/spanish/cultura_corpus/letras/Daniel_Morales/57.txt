Minimalistas portadas de tus libros favoritos

 
 Hay cierta magia en entrar a una librería y desbordar de emoción al ver tantos libros a nuestro alcance. Los ojos ven miles de obras al instante; millones de palabras condensadas bajo un espectro imaginativo que muestra algunas de las historias más importantes que han marcado al mundo. Una librería no es una simple tienda, es donde el conocimiento humano se encuentra entre obras de arte replicadas que son capaces de cambiar nuestra vida después de pasar por nuestras manos y ojos. Hay una contradicción en este sentido, la literatura es arte, de eso no hay duda, pero los empaques en los que vienen muchas veces distan de ser estéticos y eso es comprensible — mientras las palabras del libro, que son la esencia del arte, lleguen a más personas, mejor — , pero también hay algunos libros que podrían mejorar su exterior y convertirse en artículos de colección que atesoraríamos por siempre. 
  
  
 Muchos dicen que no se debe juzgar un libro por su portada, una clara referencia a no suponer lo peor de la gente al basarnos en su apariencia, pero si lo enfocamos realmente a la literatura y a obras que de antemano sabemos son la cumbre del pensamiento humano, podemos hacer una excepción. Hablamos de esos libros universalmente aclamados que están insertos en nuestro imaginario, por lo que una buena portada puede ser la diferencia entre leer o no la obra. 
   
   
 El libro “La marca del editor”, de Roberto Calasso, da el juicio perfecto que demuestra la importancia y necesidad de una buena portada: “ Écfrasis era el término que se usaba en la Grecia antigua para indicar el procedimiento retórico que consiste en traducir en palabras las obras de arte. Hay escritos dedicados por completo a la écfrasis y el maestro inigualado sigue siendo Baudelaire. No sólo en prosa, sino también en verso. Ahora bien, el editor que busca una portada — lo sepa o no — es el último, el más humilde y oscuro descendiente en la estirpe de aquellos que practican el arte de la écfrasis, pero aplicada esta vez a la inversa, es decir, tratando de encontrar el equivalente de un texto en una sola imagen. No basta que la imagen sea adecuada. Deberá además ser percibida como adecuada por múltiples ojos extraños que por lo general aún no saben nada de lo que encontrarán en el libro”. 
   
   
 Dicen que el amor a primera vista no existe, pero su contradicción establece que el amor entra por los ojos. Ésa es la importancia de las portadas, son imágenes pensadas para darle contexto a los lectores, para invitarlo a encontrar una maravilla dentro de su contenido y por eso nunca debería (a menos que un manifiesto artístico lo implicara) estar ausente de un diseño o una imagen. Alguien que entendió eso a la perfección fue el ilustrador y diseñador simplemente llamado Christian, quien opera bajo el nombre de Sinch. A través de sus conocimientos técnicos y su pasión por la literaturdoa ha reimaginado portadas de grandes obras literarias y los resultados parecen ser mejores que lo que hacen muchas editoriales. 
   
   
 El diseño contemporáneo se ha enfocado en hacer que menos sea más y Sinch ha encontrado en esa fórmula la verdadera esencia de las obras que ilustra. Miles de palabras dispuestas para crear un imaginario imposible de recrear que se transforman en sobrios y delgados dibujos que con su simpleza te remiten a los momentos más importantes del libro. “The Catcher in the Rye” es la obra de un nihilista adolescente que no quiere nada en la vida, excepto, tal vez, ser el guardián que cuida que los niños no caigan al barranco en un campo de centeno. Toda la ideología de la modernidad estadounidense la resume el ilustrador en un cigarrillo que deja salir unas cuantas espigas de centeno. Por otra parte, “Heart of Darnkness” permite de forma bastante simple entender el complejo imaginario de Conrad y su viaje por África a partir de dos elementos. 
   
   
 A través del minimalismo , el diseñador ha logrado capturar la esencia de las obras. Tal vez aún no encuentres libros ilustrados de esa forma, pero puedes comprar las impresiones para decorar las paredes de tu casa y demostrar al mundo que la literatura  corre por tus venas. 
   
   
 Se necesita más que  talento gráfico para lograr estas imágenes, se debe condensar algo que a veces es más amplio que un universo, pues en un libro pueden residir miles de voces que si bien no se expresan, obligan al lector a imaginar la vida y mentalidad de cada personaje, generan mundo tras mundo y conocen a más de una persona, y todo eso debe convertirse en un diseño. Sinch lo hace con obras serias y cuentos infantiles (culpen a la literatura que lee con sus hijas, pero son algunos de sus mejores diseños) que seguramente si fueran las portadas oficiales de los libros, nadie dudaría en comprarlos. 
   
   
   
  
 *** Te puede interesar:  
 El secreto detrás de las portadas de Pink Floyd 
 El secreto detrás de las portadas de The Beatles 
 *** Fuente: 
 Sinch