25 frases de Oscar Wilde para entender la ironía de la vida

 “La mejor manera de librarse de la tentación es caer en ella”. -Oscar Wilde Una anécdota poco probable acerca de la muerte del escritor irlandés Oscar Wilde es que mientras esperaba la llegada de la muerte, su tan despreciable enemiga se encontraba en un triste cuarto desordenado con una decoración nada digna de su genio. Entonces dijo: “O se va ese tapiz de la pared, o me voy yo”, y acto seguido el escritor pereció.  La frase puede que nunca haya sido exclamada, pero es creíble que algo así haya sido dicho por uno de los más graciosos, irónicos y eruditos escritores de la literatura universal. Oscar Wilde vivió en una época en la que la represión y la homofobia era mucho más marcada de lo que es hoy, por lo que al ser obligado a vivir en las sombras, el escritor adquirió un humor ácido que a pesar de que han pasado 100 años desde su muerte, no han sido equiparables por nadie. En 46 años entregó una novela, cinco libros de poemas y diversas obras de teatro. En ellas se encuentra el legado intelectual del escritor que junto a James Joyce representan los pilares de la literatura irlandesa. 
 A continuación te presentamos 25 frases de Oscar Wilde para entender la vida, sus ironías e infortunios, que con un poco de humor nos hacen analizar todo lo que somos y hacemos. “Vivir es lo más raro de este mundo, pues la mayor parte de los hombres no hacemos otra cosa que existir”. 
  “Todos estamos en la cloaca, pero algunos estamos mirando a las estrellas”. 
 “A veces podemos pasarnos años sin vivir en absoluto, y de pronto toda nuestra vida se concentra en un solo instante”. 
 “El cinismo consiste en ver las cosas como realmente son, y no como se quiere que sean”. “Sé tú mismo, el resto de los papeles ya están tomados”. 
 “Un hombre puede ser feliz con cualquier mujer mientras no la ame”. 
 “Las mujeres han sido hechas para ser amadas, no para ser comprendidas”. 
  “Si usted quiere saber lo que una mujer dice realmente, mírela, no la escuche”. 
 “No hay nada como el amor de una mujer casada. Es una cosa de la que ningún marido tiene la menor idea”. 
 “Como no fue genial, no tuvo enemigos”. 
 “No existen más que dos reglas para escribir: tener algo que decir y decirlo”. 
 “No soy tan joven para saberlo todo”. 
  “Amarse a sí mismo es el comienzo de una aventura que dura toda la vida”. “Discúlpeme, no le había reconocido: he cambiado mucho”. 
 “Cuando la gente está de acuerdo conmigo siempre siento que debo estar equivocado. 
 “Si nosotros somos tan dados a juzgar a los demás, es debido a que temblamos por nosotros mismos”. “El trabajo es el refugio de los que no tienen nada que hacer”. 
  “Uno debería estar siempre enamorado. Por eso jamás deberíamos casarnos”. “La educación es algo admirable, sin embargo, es bueno recordar, que nada que valga la pena se puede enseñar”. 
 “La ambición es el último refugio del fracaso”. 
 “La experiencia no tiene valor ético alguno, es simplemente el nombre que damos a nuestros errores”. 
  “La sociedad perdona a veces al criminal, pero no perdona nunca al soñador”. 
 “El hombre puede creer en lo imposible, pero no creerá nunca en lo improbable”. 
 “La única diferencia que existe entre un capricho y una pasión eterna es que el capricho es más duradero”. 
 “Que hablen de uno es espantoso. Pero hay algo peor: que no hablen”. 
 
 *** 
 Te puede interesar: Las frases más bellas de la literatura mexicana