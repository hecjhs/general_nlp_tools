Libros de Jaime Sabines para sentir el silencio más insoportable

 Los amorosos callan. El amor es el silencio más fino, el más tembloroso, el más insoportable. Los amorosos buscan, los amorosos son los que abandonan, son los que cambian, los que olvidan. Su corazón les dice que nunca han de encontrar, no encuentran, buscan. Los amorosos andan como locos porque están solos, solos, solos, entregándose, dándose a cada rato, llorando porque no salvan al amor. Les preocupa el amor. Los amorosos viven al día, no pueden hacer más, no saben. Siempre se están yendo, siempre, hacia alguna parte. 
 (Fragmento) 
  El trabajo que Jaime Sabines realizó a lo largo de todos sus años de vida terrenal estuvo plagado de amorosas estrofas y bellos pensamientos del amor humano que toca suavemente lo divino; su obra política y poética le han valido el reconocimiento alrededor del mundo como una de las voces románticas más valiosas en la literatura y en la sociedad. Su producción raya en el extremo sensible no como una conquista en el terreno de lo cursi, sino como una línea sincera, un hilo suave que conecta a la mente del hombre con el corazón del ser amado y la tierra que les acoge, el escenario en donde se da su amor. 
   Esa necesidad por cubrir con palabras al otro corazón que hace latir no sólo al nuestro, sino al planeta Tierra entero con su pulso, se halla a la perfección con la creación de Sabines, siempre preocupado y atendiendo esta urgencia del alma por enaltecer la luz de una mirada, el calor de un abrazo, la batalla por un beso y la tranquilidad de la compañía. Es por eso que, entendiendo la vitalidad y la fuerza de una poesía como la del maestro mexicano, recordamos hoy aquellos libros que comprendieron en sus primeros días los escritos que emanaban de su pluma; hojas y versos de lo inesperado, de lo tierno, de lo cariñosamente infinito.  – “Yuria” 
  Esta antología original de 1967 dio por vez primera testimonio del genio creador que radicaba en Jaime Sabines y el por qué tendríamos que prestarle atención por siempre. En ella destaca el poema grandioso que arriba lee el mismo Sabines en el Palacio de Bellas Artes. 
 – “No es que muera de amor” 
  En 1981 el poeta chiapaneco dio a conocer un compendio de poemas que marcarían la nueva ruta y sentido de sus creaciones; con una madurez adquirida significativamente al momento de escribir, sus poemas se hicieron rápidamente esa palabra de amor que cubría al mundo entero dada su nueva proyección. 
 – “Poemas sueltos” 
  Esta compilación nació justamente de la necesidad por comprender en su totalidad el cuerpo poético de Sabines y se conformó por aquellos versos huérfanos que se gestaron entre 1951 y 1961. 
 – “Tlatelolco” 
  Durante 1968 y con un nuevo rostro para sus escritos, Sabines produjo este título como otra manera de presentar al amor y a la desesperación; una manera que se circunscribía en la revolución juvenil y en los verdaderos cambios que se necesitaban en el mundo. “Tlatelolco” es esa contribución a lo sucedido, a lo perdido y a no anhelado. 
 – “Cartas a Chepita” 
  
 Si bien no es precisamente un libro de poemas, es una declaración de amor; Sabines, perdidamente enamorado de su siempre Chepita , da muestra de su talento y elegancia al dedicar palabras que encuentran la belleza en su sonoridad, estructura y mensajes. Podría pensarse incluso en una guía ejemplificada para el cómo enamorar correctamente a quien nos roba el sueño. 
 *** Te puede interesar: 
 Poemas de Jaime Sabines para enamorar a la mujer indicada 
 El francotirador de la literatura, Jaime Sabines