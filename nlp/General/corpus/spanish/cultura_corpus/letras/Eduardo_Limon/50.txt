10 libros que no podrás abandonar tan fácilmente

 Apropiarte de algo que no te pertenecía es algo muy común en nuestras vidas; constantemente lo hacemos y ni siquiera nos damos cuenta, pero tomemos como ejemplo tu película favorita . Cuando hallas un filme que aporta todo lo que necesitas en tu ser, deja de pertenecerle al director, al productor, al resto del mundo y se convierte en tuya, sólo tuya; y si alguien osa criticar o decir algo en contra de ella, es peor que si hubieran tratado de amputarte una mano o dejarte sin agua por días. Sucede con los amigos, con los discos compactos o viniles (porque sí, habemos unos anticuados que no podemos renunciar al diseño de los álbumes), las películas y muchas otras cosas que bien podríamos pensar de nuestra propiedad, pero si lo pensamos bien, sólo son cosas prestadas, producciones que algún genio creativo o actos que un ser cercano nos dan para que entendamos lo que significa estar presentes en el mundo. 
  Algo similar sucede con los libros, esos trozos de realidad que fueron arrancados de una verdad mayor, siempre cambiante para podernos transportar en infinitas posibilidades de lo existente; hay entonces libros que se hacen de uno pero, sobre todo, se hacen con uno porque nos han brindado la posibilidad de escapar y fascinarnos. Si alguna vez (o varias) se ha experimentado esto, no poder despegar los ojos de algunas páginas por temor a perdernos algo importante, sabemos lo que significa frenar este mundo físico al que estamos atados por seguir leyendo sin cansancio. A continuación te mostramos aquellos títulos que efectivamente han causado esa sensación, esa necesidad de devorarlos lo más rápido posible, cueste lo que cueste. Y cuando tú los leas, si es que no lo has hecho ya, sabrás que hay algo en sus escenarios, en sus personajes, que te privan fácilmente de tu libertad. 
 – “Crónicas marcianas” – Ray Bradbury 
  
 “Los hombres de Marte comprendieron que si querían sobrevivir tenían que dejar de preguntarse de una vez por todas: ¿Para qué vivir? La respuesta era la vida misma”.   – “A sangre fría” – Truman Capote 
  
 “Los vecinos de este pueblo son todos amigos nuestros. No hay nadie más. En cierto modo esa es la peor parte de este crimen. Qué cosa más terrible cuando los vecinos no pueden mirarse los unos a los otros sin preguntarse…” – “Tokio Blues” – Haruki Murakami 
  
 “Ella posó sus manos sobre mis hombros y se quedó mirándome fijamente. En el fondo de sus pupilas, un líquido negrísimo y espeso dibujaba una extraña espiral. Las pupilas permanecieron largo tiempo clavadas en mí. Después se puso de puntillas y acercó su mejilla a la mía. Fue un gesto tan cálido y dulce que mi corazón dejó de latir por un instante”. – “Mujeres” – Charles Bukowski 
  
 “Me alegraba de no estar enamorado, de no ser feliz con el mundo. Me gustaba estar en desacuerdo con todo. La gente enamorada a menudo se ponía cortante. Perdían su sentido de la perspectiva. Perdían su sentido del humor. Se ponían nerviosos, psicóticos, aburridos. Incluso se convertían en asesinos”. – “El monje” – Matthew Gregory Lewis 
  
 “¡Escúcheme, hombre de corazón duro! ¡Escúcheme, orgulloso, severo y cruel! ¡Habría podido salvarme y devolverme la dicha y la virtud, pero no quiso! Usted es el destructor de mi alma, mi asesino. ¡Que caiga sobre usted la maldición de mi muerte y la de mi hijo aún no nacido!” – “This is how you lose her” – Junot Díaz 
  
 “And that’s when I know it’s over. As soon as you start thinking about the beginning, it’s the end”.  
 [“Y ahí supe que terminó. Tan pronto comienzas a pensar sobre el principio, es el fin”.] – “Señorita Vodka” – Susana Iglesias 
  
 “2) Cree en un dios que sepa hacer cocteles, adóralo. 3) Recuerda y no olvides: no hay belleza que pueda compararse con la de las puertas de una cantina que se abren…” – “Crónica de una muerte anunciada” – Gabriel García Márquez 
  
 “ Al verla así, dentro del marco idílico de la ventana, no quise creer que aquella mujer fuera la que yo creía, porque me resistía a admitir que  la vida terminara por parecerse tanto a la mala literatura”. – “A tale of love and darkness” – Amos Oz 
  
 “If you steal from one book you are condemned as a plagiarist, but if you steal from ten books you are considered a scholar, and if you steal from thirty or forty books, a distinguished scholar”.[“Si robas de un libro eres condenado como un plagiario, pero si robas de diez libros eres considerado un erudito, y si robas de treinta o cuarenta libros, un distinguido erudito”.]  
 – “El Imperio” – Ryszard Kapuściński 
  
 “Al mundo lo amenazan tres plagas. La primera es la plaga del nacionalismo. La segunda es la plaga del racismo. Y la tercera es la plaga del fundamentalismo religioso. Las tres tienen un mismo rasgo, un denominador común. La irracionalidad, una irracionalidad agresiva, todopoderosa, total”. Ya se sea un bibliófilo o un bibliópata, las ansias por recurrir a un buen texto son insaciables, esos mundos desconocidos que poco a poco se convierten en propios sobrepasan cualquier interés por el que de hecho nos afecta físicamente y nos orillan a seguir a como dé lugar con dicha adicción. ¿Qué otros títulos nos han hecho perder así la cabeza ? 
 *** Te puede interesar: 
 72 libros que todo amante de la literatura debe de haber leído antes de los 30 
 15 libros que se convertirán en películas este 2016 y no podemos omitir