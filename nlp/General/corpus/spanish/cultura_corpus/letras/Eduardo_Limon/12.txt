Las palabras más bonitas del español según la BBC

 Dejarnos fascinar por el inglés es increíble; esas suaves y hermosas erres, esa eles sutiles, suspirantes tes y haches que enamoran a la pronunciación, son sólo algunos ingredientes de la lengua extranjera en su receta para la adoración. Desfallecer ante el romanticismo del francés aunque sus sonidos sean casi imposibles para el hispanohablante, ¿por qué no? Esa maravillosa nasalidad y esa garganta que se sofoca ante lo amoroso son fantásticas; cómo no rendirse a los pies de ese lenguaje europeo. Caballos galopantes de tierras lejanas que asombran con su hermosura, palabras tan sofisticadas que provienen de la pluma shakespeareiana o la investigación proustiana; de qué manera no dejarse llevar por el idioma de fuera. 
  “Nuestra identidad lingüística lo tiene todo para competir con cualquier otro código de comunicación, el español es uno de los lenguajes más poderosos en el planeta Tierra”. Sí, todo eso es hermoso. Exuberante. Sin embargo, no hay manera de renunciar a nuestra lengua madre; por mucho que a veces la miopía de lo extraño nos impida vislumbrar la magia de lo propio, reconocer que nuestra identidad lingüística lo tiene todo para competir con cualquier otro código de comunicación, el español es uno de los lenguajes más poderosos en el planeta Tierra. Siendo la segunda lengua con más hablantes del mundo, la BBC solicitó a sus lectores que opinaran cuáles palabras se podrían catalogar como espléndidos esfuerzos de la conexión humana. 
  Eso creó, en buena medida, una consciencia sobre la sonoridad y la significación de ciertas palabras; ampliando, a su vez, el vocabulario de quien consultara la lista. El resultado fueron cientos y cientos de palabras compitiendo en un listado por ganar el título de las más bellas. A continuación, se encuentran esas 10 que consiguieron más votos ya fuera por sus letras, por sus sílabas o por el concepto que encierran. 
 
 Sempiterno 
  Que durará siempre; que habiendo tenido principio, no tendrá fin. 
 Mandrágora 
  Planta herbácea de la familia de las Solanáceas, sin tallo y con muchas hojas. 
 Cadejo 
  Animal  legendario  de la región  mesoamericana , siendo muy conocido en las zonas rurales en Mesoamérica. 
 Libertad 
  Facultad natural que tiene el hombre de obrar de una manera o de otra, así como de no obrar, por lo que es responsable de sus actos. 
 Anonadado 
  Estado de gran sorpresa o dejar muy desconcertado a alguien. 
 Dulce 
  Que causa cierta sensación suave y agradable al paladar, como la miel, el azúcar, etcétera. 
 Arrebol 
  Color rojo de las nubes iluminadas por los rayos del sol. 
 Maquiavélico 
  Que actúa con astucia y doblez. 
 Conticinio 
  Hora de la noche en que todo está en silencio. 
 Triquismiquis 
  Persona con escrúpulos o reparos vanos de poquísima importancia. 
 Una de las más importantes facultades en la lengua es que ésta se construye y se modifica con el uso que le damos las personas; su belleza e importancia radica en las transformaciones que cada hablante le da a cada palabra en justo tiempo y a su justa medida. Éstas fueron las que en su momento resultaron importantes; no obstante, ello puede cambiar con el paso de los años y el uso que se requiera de sus formas. Para tener una mayor conciencia de estos fenómenos, visita Carlos Fuentes, un combatiente en las fronteras del lenguaje y Bandas que inventaron sus propios lenguajes .