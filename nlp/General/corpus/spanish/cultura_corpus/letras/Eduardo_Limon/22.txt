Lecciones de amor de Sartre y Beauvoir que deberíamos aprender

 
 Él era filósofo, dramaturgo , novelista y periodista francés. Ella, filósofa, escritora y profesora francesa. Ambos, una unión polémica que ha atravesado los muros del tiempo y ha demostrado que las complicaciones tradicionales en una relación afectiva son cosa de niños junto al amor existencialista. Se conocieron en la universidad cuando tenían 24 y 21 años respectivamente; desde entonces, sus lazos tanto pasionales como intelectuales se vieron tensados en sus vidas y decidieron emprender un camino juntos. 
  Camino para nada sencillo, pues mezclarían de una forma muy original y caótica sus planteamientos filosóficos con sus actos cotidianos (como todo buen pensador debería hacer); leían acompañados, visitaban bares, escuchaban jazz, bebían café, perdían la razón en el alcohol y nunca se casaron. Con el tiempo y el apoyo mutuo a sus carreras, él, Jean Paul Sartre, se consolidó como el padre del Existencialismo, y ella, Simone de Beauvoir, como una librepensadora pionera en la defensa de la libertad sexual y femenina contemporánea. 
  Su manera tan única y revolucionaria de profesar el amor en este mundo, además de sus grandes aportaciones a los sistemas de pensamiento en nuestra sociedad, les ha colocado también en el marco de una pareja en extremo singular. Vinculo que se ha malinterpretado como “relación abierta”, banalizando y deformando un pacto que involucraba respeto, admiración y romance. ¿Qué podríamos rescatar entonces de ese acuerdo considerado y tolerante? 
 1. Que las coincidencias entre dos personas van más allá de lo físico. 
  
 2. La compañía se da no en los términos de la belleza o la unión tradicional, sino de la magia y el entendimiento.  
 3. La importancia de una persona no se une forzosamente a un acuerdo de fidelidad en términos íntimos.  
 4. La calma de la carne no está ligada a cargas morales.  
 5. Que los contratos convencionales son prescindibles.  
 6. La sexualidad es variable y el polideseo es aquello que nos mueve.  
 7. En una relación no se tiene porqué ser autoritario y posesivo.  
 8. El amor se puede resignificar siempre. 
  
 Este par de filósofos increíbles se amaba. Nadie puede dudarlo o siquiera ponerlo en entredicho. Cierto es que al final de sus vidas, Simone se sintió herida por las actitudes conservadoras y hasta cierto punto cuidadoras de otra mujer, pero eso nunca frenó sus sentimientos ni quebró todo eso que construyeron juntos. Esa devoción que sentían el uno por el otro se transparenta, por ejemplo, en una carta que le escribió Jean Paul a su querida mujer libre : 
 Mi querida chiquilla Por mucho tiempo he querido escribirte por la tarde luego de esas salidas con amigos que pronto estaré describiendo en “A Defeat”, del tipo donde el mundo es nuestro. Quise traerte mis alegrías de conquistador y postrarlas a tus pies, como hacían en la Era del Rey Sol. Y luego, agotado por el griterío, siempre me iba simplemente a la cama. Hoy lo hago para sentir el placer que tú aún no conoces, de virar abruptamente de amistad a amor, de fuerza hacia ternura. Esta noche te amo en una manera que aún no conoces en mí: no me encuentro ni agotado por los viajes ni envuelto por el deseo de tu presencia. Estoy dominando por mi amor por ti y llevándolo hacia mi interior como elemento constitutivo de mí mismo. Esto ocurre mucho más a menudo de lo que lo admito frente a ti, pero rara vez cuando te escribo. Trata de entenderme: te amo mientras prestas atención a cosas externas. En Toulouse, simplemente te amaba. Esta noche te amo en una tarde de primavera. Te amo con la ventana abierta. Eres mía y las cosas son mías, y mi amor altera las cosas a mi alrededor y las cosas a mi alrededor alteran mi amor. […] Te amo con todo mi corazón y toda mi alma. 
 *** Te puede interesar: 
 El consejo de Sartre para curar tu vacío existencial 
 El amor índigo de Sartre