6 libros que debes leer según Angelina Jolie, Natalie Portman y otras famosas

 
 A lo largo de la historia han existido libros que han cambiado el transcurso del devenir ¿Qué hubiera sido de la biología sin “El origen de las especies” de Darwin? ¿Qué camino hubiera seguido la economía moderna sin “La riqueza de las naciones” de Adam Smith? ¿Hubieran existido revoluciones del siglo XX de no haber existido el “Manifiesto Comunista” de Karl Marx y Friedrich Engels? 
 Uno de estos libros es “El segundo sexo” de Simone de Beauvoir, en esta obra, su autora argumenta que la mujer es una entidad que fue construida socialmente, ya que se le ha definido a lo largo de la historia respecto a algo más: como madre, esposa, hija, etcétera. Nunca ha podido alcanzar una identidad propia  desde sus propios criterios. 
 Gracias a esta publicación, cientos de ideas cambiaron la forma como se pensaba de la mujeres y lucharon por derrumbar las construcciones sociales. Una de las primeras acciones que se llevó a cabo para llegar a un cambio, fue la lucha por la despenalización del aborto . Esta acción le devolvería a las mujeres el control sobre su cuerpo y derrumbaría la idea de que el sexo femenino únicamente fue creado para dar a luz a un niño. 
  La obra de Simone de Beauvoir es sólo una de las muchas letras que se han convertido en las favoritas de las siguientes mujeres: 
 – Angelina Jolie 
 “Vlad the Impaler, In Search of the Real Dracula” (2003), de M. J. Trow. 
  El libro está inspirado en el hombre que inspiró a Bram Stoker para crear a Drácula. Vlad Tepes fue famoso por los diversos métodos de tortura utilizados para contener los embates otomanos que amenazaban a Europa. Jolie afirma ser una gran admiradora de este personaje, asegura que de niña leyó casi todos los libros que hablan sobre Vlad.  
 – Anne Hathaway 
 “El jardín secreto” (1910), de Frances Hodgson Burnett 
  “Yo siempre quise ser Mary Lennox. Todavía tengo una debilidad por los jardines y siempre voy a ver si puedo encontrar las puertas cerradas que se encuentran dentro de ellos”. 
 Mary Lennox es el personaje principal de esta novela, debido a una epidemia de cólera en la India colonial es enviada al Reino Unido a vivir con su tío. Ahí pasará horas en el jardín privado de la residencia, es en este oasis donde encontrará una puerta que dará acceso a un refugio misterioso. 
 – Gwyneth Paltrow 
 “El cuaderno dorado” (1962), de Doris Lessing 
  “Yo estaba electrificada por la heroína de Lessing, Anna, y su lucha para convertirse en una mujer libre. El trabajo, la amistad, el amor, el sexo, la política, el psicoanálisis, la escritura;  todas las cosas que me preocupaban eran los sujetos de Lessing”. 
 Esta novela narra la historia de Anna Wulf y su intento por finalizar su quinto libro, el dorado. Cada texto ficticio abunda en una temática diferente entre las que están una novela, un diario personal y su experiencia durante la Segunda Guerra Mundial y como miembro del Partido Comunista. 
 – Jennifer Lawrence 
 “Anna Karenina” (18779, de Leo Tolstoy 
  “Realmente no esperaba que me gustara, pensé que no querría terminar un libro de 800 páginas, entonces empecé a reducir la velocidad de lectura y releí  los mismos capítulos una y otra vez. Te enamoran los personajes, creces con ellos”. 
 Una de las obras cumbres del realismo. Esta novela describe la tragedia de Anna Karenina, una dama rusa que ve su vida alterada cuando se le presenta la dicotomía de seguir lo que le indica su corazón o mantenerse con la familia que ya ha creado. 
 – Keira Knightley 
 “Orgullo y prejuicio” (1813), de Jane Austen 
  “He leído el libro mucho. He estado enamorada con el libro desde que tenía aproximadamente siete años. Tenía toda la serie Austen en pasta dura. Estaba obsesionada con la versión de la BBC cuando tenía diez u once. Leí el libro, finalmente, cuando tenía unos catorce años y me obsesioné de nuevo “. 
 Desde el momento de su publicación, “Orgullo y prejuicio” se convirtió en una de las obras favoritas de la literatura inglesa al vender más de 20 millones de copias. La historia sigue a Elizabeth Bennet y su adaptación a los modales, el matrimonio y en general a la nobleza territorial de la Regencia británica. 
 – Natalie Portman 
 “Cloud Atlas”, por David Mitchell 
  “Este fue el regalo que le di a todos los que conocía desde hace tres años. Son seis historias diferentes que se cuentan en diferentes géneros y períodos de tiempo: una de ellas es la ficción histórica, otro es un suspenso de los setenta, el sexto es un cuento postapocalíptico. Es uno de los libros más hermosos, entretenidos y desafiantes; algo que se lleva toda su atención. Creo que las historias son meditaciones sobre la violencia, en particular la necesidad de ella”. 
 En el set de filmación de “V for Vendetta”, Natalie Portman les presentó a las hermanas Wachowski una novela de David Mitchell que estaba leyendo. Este primer contacto dio origen a una de las películas más complicadas e infravaloradas de nuestra época.  
 – La inspiración en las letras abre nuestra curiosidad para conocer qué películas fueron dirigidas por mujeres . En estos casos, sus libros favoritos nos acercan al proceso creativo que está detrás de su trabajo. 
 *** Te puede interesar:  
 10 películas de mujeres que cambiaron al mundo 
 Las mujeres no venden: grandes escritoras que tuvieron que ser hombre