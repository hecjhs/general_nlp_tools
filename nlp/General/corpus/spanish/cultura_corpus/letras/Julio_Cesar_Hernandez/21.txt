Las mejores novelas negras de América Latina

 Las novelas negras, también llamadas novelas policiacas, es un género que inició a partir de que las urbes comenzaron a desarrollarse de manera estrepitosa a principios del siglo XX, en Norteamérica, y con todo lo que esto acarreaba. Se sabe que cuando una población comienza a crecer, también los problemas crecen, la delincuencia se dispara, la densidad poblacional implica problemáticas que pocas veces se prevén, o si se prevén, no son materia que importe a quien desarrolla las ciudades. 
  
 La literatura es escaparate y espejo de la sociedad en la que se desarrolla, es un termómetro social capaz de congelar el momento en tinta para la posteridad; no por nada para conocer un lugar y una época pasada se recurre a lo que alguien en un momento determinado escribe. Asimismo es posible crear reconstrucciones y reinterpretaciones de momentos pasados por medio de la literatura. Los inicios de la novela negra nos dirigen a las ciudades norteamericanas de principios de los XX, génesis de las bandas del crimen organizado,  los detectives encubiertos y la violencia sórdida de los callejones más oscuros y el slang  sonando en las páginas de dichas novelas. 
 Este género también se desarrolla en Latinoamérica de la manera que lo hizo en Estados Unidos y Europa; por supuesto que América Latina tiene sus grandes urbes llenas de historias dignas de un relato de James M. Cain, autor de El cartero siempre llama dos veces.  Estas son algunas de las mejores novelas negras latinoamericanas: 
 – 
 El Complot Mongol  (1969) – Rafael Bernal 
   Considerada la primera novela negra mexicana, El  Complot Mongol es una obra maestra de la narrativa mexicana. Ambientada en el México de principios de los sesenta, cuando la Revolución ya olía a naftalina, ésta narra el papel que tiene un matón a sueldo del gobierno mexicano. Dicho personaje, duro y malhablado se ve inmiscuido en lo que podría convertirse en el atentado más grande después del asesinato de Kennedy en 1963. Mientras trabaja con los rusos y los estadounidenses, el protagonista mantiene un diálogo interno constante donde no para de recordarse a sí mismo “Pinche Mongolia exterior, pinches rusos, pinches todos”. 
 – 
 Paisaje de Otoño  (1998) – Leonardo Padura Fuentes 
   Esta novela fue escrita por el autor Leonardo Padura, de origen cubano. Relato ubicado en la Habana, comienza cuando unos pescadores encuentran en la orilla del mar un cuerpo con una extraña señal marcada. Dicho crimen destapa un pasado de corrupciones donde el occiso estaba involucrado en una huída a Miami. Se comienza a dibujar la historia, a partir de recordar la vida del asesinado, que poco antes de su muerte había regresado a la isla por un secreto que sólo él conocía. 
 – 
 Pasado Negro  (1985) – Rubem Fonseca 
  
 Rubem Fonseca es un autor brasileño y nos ofrece esta gran novela del género. Trata de la vida de un hombre con un pasado oscuro, quien después de esconderse durante diez años con una adolescente, descubre la literatura y el amor, y a partir de eso salta a la fama como novelista. Un día el cuerpo de una mujer asesinada es encontrado por la policía, lo curioso es que dentro del auto encuentran un libro del famoso novelista, Gustavo Flavio, con lo cual comienzan las pesquisas para averiguar el crimen. 
 – 
 Los siete hijos de Simenon  (2000) – Ramón Díaz Eterovic 
   El autor chileno, Ramón Díaz Eterovic, tiene la característica de que en todas sus obras aparece el detective Heredia, quien se va enrollando en crímenes y elucubraciones. En esta novela, Heredia decide investigar el crimen de un abogado, pero al comenzar la investigación se topa con una oscura trama alrededor de la construcción de un gasoducto donde los intereses políticos y económicos ponen en peligro el medio ambiente. 
 – 
 Sombra de la sombra  (1986) – Paco Ignacio Taibo II 
  
 Se considera a este reconocido autor mexicano como el creador de la novela neopoliciaca. Su estilo un tanto caótico de contar las historias recuerdan un folletín en el que trozos de historia se van conjuntando para así al final unir la piezas de toda la obra. En este libro cuatro personajes se desenvuelven en una locura desenfrenada, los personajes remiten un poco al protagonista de El Complot Mongol, ya que al igual que éste, son resabios de la cultura violenta de la Revolución Mexicana.  – 
 Luna caliente  (1983) – Mempo Giardinelli 
  
 Novela de origen argentino, nos transporta a El Chaco, Argentina, donde un joven se ve arrastrado al crimen por el amor de la bella Araceli, quien tiene sólo 13 años de edad. Mantiene el toque del chico corrompido por la sociedad que lo orilla a cometer el crimen, mientras reflexiona un poco sobre Dostoievski y su actuar. Entre el crimen y el amor, esta novela corta es una excelente muestra de novela negra latinoamericana. 
 – 
 La transmigración de los cuerpos  (2013) – Yuri Herrera 
  
 Posiblemente una de las más eclécticas en este listado. Esta obra del autor mexicano, Yuri Herrera, ha sido denominada como un imprescindible de la literatura nacional contemporánea. La historia trata del encuentro de La Tres Veces Rubia y el Alfaqueque, a partir de este encuentro, comienzan a deslizarse por un mundo oscuro de tugurios, sexo y tabernas. Con un tono detectivesco es una obra que vale la pena leer. 
 – 
 Los corruptores (2013) – Jorge Zepeda Patterson 
  Esta novela mexicana, no necesariamente cumple todos los cánones de lo que debería ser una novela negra, pero parece interesante el trabajo que el autor hace al momento de construirla. Desde el primer capítulo, un crimen. Aparece el cuerpo de una famosa modelo en un terreno baldío que colinda con una casa propiedad del Secretario de Gobernación. A partir de este suceso filtrado por un periodista comienza la persecución de éste y sus tres amigos de juventud que se ven absorbidos por el suceso y los obliga a reunirse para esclarecer el crimen, antes de ser los muertos “colaterales” de una lucha de poder en las altas esferas mexicanas, el narco y los medios. Lo novedoso de esta novela es que toma en cuenta las posibilidades que las nuevas tecnologías de la información dan para hablar de espionaje, y una nueva especie de “detective digital”. 
 *** 
 Te puede interesar:  
 Las mejores novelas de lo que va de 2015.