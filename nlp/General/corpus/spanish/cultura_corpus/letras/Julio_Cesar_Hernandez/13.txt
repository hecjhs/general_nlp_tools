10 poemas que revelan el lado oscuro del amor

 El amor como concepto universal parece que ha caído en el simplismo de que sólo trae felicidad y placer. Si como todas las cosas en el mundo tiene su opuesto posible dentro de sí mismo, como el día tiene a la noche, lo bello a lo feo; el amor contiene al dolor y al odio. 
 Tener un opuesto no exime de volar por las ambivalencias. Las líneas que hacen que crucemos la puerta entre el amor y el odio siempre están abiertas, y como vasos comunicantes se alimentan una de otra. El amar es odiar. El amar implica no amar a los demás, a eliminar todo punto que pueda exigir una tensión amorosa, cerrarse a sólo darle al sujeto de amor lo que se cree que necesita. 
  
 ¿Te amo porque te necesito, o te necesito porque te amo ? 
 Es lo que nos pregunta Erich Fromm… ¿Las obsesiones en el amor de qué se alimentan? Se alimentan de nosotros mismos, de lo que creemos, de lo que vemos por medio de unos sentidos que varias veces se han puesto a discusión. Pero lo que es cierto, si los sentidos no lo son del todo, es que el amar significa eliminarte para fundirte en otro universo inestable creado por dos y que en cualquier momento puede expulsarlos reconvirtiéndolos en dos nuevos extraños. Del todo a la nada y de la nada al todo. 
 El amor romántico, como lo concebimos, tiene un génesis muy particular. Se sabe que uno de los primeros escritores que le puso cara de mujer al amor fue Francisco Petrarca a principios del siglo XIV en Italia y Francia. Él es quien elevó a la mujer hasta el pedestal más alto en el parnaso; a esa corriente de estética se le llamó petrarquismo. Si bien, el culto al amor y a la mujer son antiquísimos, en registros como la poesía se pueden ver hasta que Petrarca pone de manifiesto sus pasiones y preocupaciones, así como metáforas entre la muerte y el amor.   Con él comienza esta lista, en la que el amor muestra la otra cara, la cara de la desesperación, celos, etc., la cara que nadie quiere ver; la cual, sin embargo es la más fuerte, es la que contiene los sentimientos más profundos y transformadores de la existencia humana. 
 
 Petrarca – Mi loco afán Mi loco afán está tan extraviado de seguir a la que huye tan resuelta,  y de lazos de Amor ligera y suelta  vuela ante mi correr desalentado, 
 que menos me oye cuanto más airado  busco hacia el buen camino la revuelta:  no me vale espolearlo, o darle vuelta,  que, por su índole, Amor le hace obstinado.  
 Y cuando ya el bocado ha sacudido, yo quedo a su merced y, a mi pesar,  hacia un trance de muerte me transporta:  
 por llegar al laurel donde es cogido  fruto amargo que, dándolo a probar,  la llama ajena aflige y no conforta. 
 – 
 Conde de Lautréamont – Canto IV (Fragmento) 
 Es un hombre o una piedra o un árbol el que va a comenzar el cuarto canto. Cuando el pie resbala sobre una rana, se tiene una sensación de repugnancia, pero cuando se roza apenas el cuerpo humano con la mano, la piel de los dedos se agrieta, como las escamas de un bloque de mica que se rompe a martillazos; y lo mismo que el corazón de un tiburón que ha muerto hace una hora palpita todavía con tenaz vitalidad sobre el puente, lo mismo nuestras entrañas se agitan en su totalidad mucho tiempo después del contacto. ¡Tanto horror le inspira el hombre a su propio semejante! Puede ser que al decir esto me equivoque, pero puede ser también que diga la verdad. Conozco, concibo una enfermedad más terrible que los ojos hinchados por largas meditaciones sobre el extraño carácter del hombre, pero aunque la busco todavía… ¡no he podido encontrarla! No me creo menos inteligente que otros, y sin embargo, ¿quién se atrevería a afirmar que he acertado en mis investigaciones? ¡Que mentira saldría de su boca! 
 – 
 Sor Juana Inés de la Cruz – El ausente, el celoso, se provoca… 
 El ausente, el celoso, se provoca, aquél con sentimiento, éste con ira; presume éste la ofensa que no mira y siente aquél la realidad que toca: Éste templa tal vez su furia loca  cuando el discurso en su favor delira; y sin intermisión aquél suspira, pues nada a su dolor la fuerza apoca. Éste aflige dudoso su paciencia y aquél padece ciertos sus desvelos;  éste al dolor opone resistencia; aquél, sin ella, sufre desconsuelos: y si es pena de daño, al fin, la ausencia, luego es mayor tormento que los celos.  – 
 Oscar Wilde – Desesperación 
 Las estaciones derraman su ruina mientras pasan, Pues en la primavera los narcisos alzan sus rostros Hasta que las rosas florecen en ígneas llamas; Y en el otoño brotan las violetas púrpuras Cuando el frágil azafrán suscita la nieve invernal, Pero los decrépitos y jóvenes árboles renacerán, Y esta tierra gris crecerá verde con el rocío del verano, Y los niños correrán entre un océano de frágiles prímulas. 
 ¿Pero qué vida, cuya amarga voracidad Desgarra nuestros talones, velando la noche sin sol, Alentará la esperanza de aquellos días que ya no retornarán? La ambición, el amor, y todos los sentimientos que queman Mueren demasiado pronto, y sólo encontramos la dicha El los marchitos despojos de algún recuerdo muerto. 
 – 
 Charles Bukowski – Sí sí 
 cuando Dios creó el amor no nos ayudó cuando Dios creó a los perros, no ayudó a los perros cuando Dios creó a las plantas eso fue ordinario cuando Dios creó el odio adquirimos una utilidad estándar cuando Dios me creó él me creó cuando Dios creó al mono estaba amodorrado cuando él creó a la jirafa estaba borracho cuando él creó los narcóticos estaba drogado y cuando él creó el suicidio cayó bajo 
 cuando él te creo a ti recostada en la cama él sabía lo que estaba haciendo estaba borracho y estaba drogado y creó las montañas y el mar y el fuego al mismo tiempo 
 cometió algunos errores pero cuando te creó recostada en la cama se vino sobre todo su bendito universo 
  
 – 
 José María Fonollosa – Kennamore Street 
 Yo quiero que tú sufras lo que sufro:  aprenderé a rezar para lograrlo.  
 Yo quiero que te sientas tan inútil  como un vaso sin whisky entre las manos;  que sientas en el pecho el corazón  como si fuera el de otro y te doliese.  
 Yo quiero que te asomes a cada hora  como un preso aferrado a su ventana  y que sean las piedras de la calle  el único paisaje de tus ojos.  
 Yo deseo tu muerte donde estés.  Aprenderé a rezar para lograrlo. – 
 Leopoldo María Panero – El noi del sucre 
 Tengo un idiota dentro de mí, que llora,que llora y que no sabe,  y mira sólo la luz, la luz que no sabe.Tengo al niño, al niño bobo,  como parado en Dios,  en un dios que no sabesino amar y llorar, llorar por las nochespor los niños, por los niños de falo dulce,  y suave de tocar, como la n oche.  Tengo a un idiota de pie sobre una plazamirando y dejándose mirar, dejándose violar por el alud de las miradas de otros,  y  llorando, llorando frágilmente por la luz. Tengo a un niño solo entre muchos,  as a beaten dog beneath the hail, bajo la lluvia,  bajo el terror de la lluvia que llora, y llora, hoy por todos,  mientras el sol se oculta para dejar matar,  y viene a la noche de todos el niño asesinoa llorar de no se sabe por qué, de no saber hacerlode no saber sino tan sólo ahorapor qué y cómo matar, bajo la lluvia entera,con el rostro perdido y el cabello dementehambrientos, llenos de sed,  de ganas de aire,  de soplar globos como antes era,  fue la vida un día antesde que allí en la alcoba de los padres  perdiéramos la luz. 
 – 
 Julio Cortázar – Bolero 
 Qué vanidad imaginar que puedo darte todo, el amor y la dicha, itinerarios, música, juguetes. Es cierto que es así: todo lo mío te lo doy, es cierto, pero todo lo mío no te basta como a mí no me basta que me des todo lo tuyo. 
 Por eso no seremos nunca la pareja perfecta, la tarjeta postal, si no somos capaces de aceptar que sólo en la aritmética el dos nace del uno más el uno. 
 Por ahí un papelito que solamente dice: 
 Siempre fuiste mi espejo, quiero decir que para verme tenía que mirarte. 
 Y este fragmento: 
 La lenta máquina del desamor los engranajes del reflujo los cuerpos que abandonan las almohadas las sábanas los besos 
 y de pie ante el espejo interrogándose cada uno a sí mismo ya no mirándose entre ellos ya no desnudos para el otro ya no te amo, mi amor. 
  
 Manuel Acuña – Nocturno a Rosario 
 Pues bien, yo necesito decirte que te adoro, decirte que te quiero con todo el corazón; que es mucho lo que sufro, que es mucho lo que lloro, que ya no puedo tanto, y al grito que te imploro te imploro y te hablo en nombre de mi última ilusión. 
 De noche cuando pongo mis sienes en la almohada, y hacia otro mundo quiero mi espíritu volver, camino mucho, mucho y al fin de la jornada las formas de mi madre se pierden en la nada, y tú de nuevo vuelves en mi alma a aparecer. 
 Comprendo que tus besos jamás han de ser míos; comprendo que en tus ojos no me he de ver jamás; y te amo, y en mis locos y ardientes desvaríos bendigo tus desdenes, adoro tus desvíos, y en vez de amarte menos te quiero mucho más. 
 A veces pienso en darte mi eterna despedida, borrarte en mis recuerdos y huir de esta pasión; mas si es en vano todo y mi alma no te olvida, ¡qué quieres tú que yo haga pedazo de mi vida; qué quieres tú que yo haga con este corazón! 
 Y luego que ya estaba? concluido el santuario, la lámpara encendida tu velo en el altar, el sol de la mañana detrás del campanario, chispeando las antorchas, humeando el incensario, y abierta allá a lo lejos la puerta del hogar… 
 Yo quiero que tú sepas que ya hace muchos días estoy enfermo y pálido de tanto no dormir; que ya se han muerto todas las esperanzas mías; que están mis noches negras, tan negras y sombrías que ya no sé ni dónde se alzaba el porvenir. 
 ¡Qué hermoso hubiera sido vivir bajo aquel techo. los dos unidos siempre y amándonos los dos; tú siempre enamorada, yo siempre satisfecho, los dos, un alma sola, los dos, un solo pecho, y en medio de nosotros mi madre como un Dios! 
 ¡Figúrate qué hermosas las horas de la vida! ¡Qué dulce y bello el viaje por una tierra así! Y yo soñaba en eso, mi santa prometida, y al delirar en eso con alma estremecida, pensaba yo en ser bueno por ti, no más por ti. 
 Bien sabe Dios que ese era mi más hermoso sueño, mi afán y mi esperanza, mi dicha y mi placer; ¡bien sabe Dios que en nada cifraba yo mi empeño, sino en amarte mucho en el hogar risueño que me envolvió en sus besos cuando me vio nacer! 
 Esa era mi esperanza… mas ya que a sus fulgores se opone el hondo abismo que existe entre los dos, ¡adiós por la última vez, amor de mis amores; la luz de mis tinieblas, la esencia de mis flores, mi mira de poeta, mi juventud, adiós! 
  
 – 
 Alí Chumacero – Vencidos 
 Igual que roca o rosa, renacemos y somos como aroma o sueño tumultuoso en incesante amor por nuestro duelo; fugitivos sin fin que el rostro guardan, mudos cadáveres precipitados a una impasible tempestad; y morimos en nuestras propias manos, sin saber de agonías, caídos descuidados al abismo, a través de catástrofes en nuestro corazón dormidas, así tan simplemente, que al mirar un espejo hallamos dentro sombras silenciosas o una paloma destrozada. 
 Porque nada delata que existamos en esta soledad del pensamiento, y el olvido desciende hacia la tierra como un equívoco de Dios, dormida imagen donde en sueños se martiriza por saberse bello; porque es inútil la embriaguez que nos cubre de olvidos contra el mundo cuando es la lentitud y el sentirse arrojados sobre el lecho, como el cesar y el impedir, lo que alimenta nuestro amor y el incansable continuar entre los hombres, del dolor de la carne enamorados. Igual que rosa o roca: crueles cadáveres sin agonía.  *** 
 Te puede interesar: La vida maldita de Arthur Rimbaud