David Catá, el dolor de las memorias

 Se conoce como recuerdo la restauración del pasado a partir de las memorias. Personas, experiencias y objetos habitan la historia de cada uno y cualquier gesto del presente puede evocar un pasaje de la propia vida. Para preservar los recuerdos existe la imagen o la palabra; usar el cuerpo como medio de representación de la memoria es un acto para exteriorizar el olvido. 
  David Catá reflexiona sobre el futuro de los recuerdos antes de que estos se guarden en el pasado. Para el español, las relaciones familiares, amistosas, de pareja o profesionales construyen su cuerpo de trabajo que traslada a la palma de su mano. El soporte de su actividad es su propio cuerpo: borda sobre la piel su autobiografía y afianza con el dolor la permanencia del recuerdo. 
  Aunque Catá reconoce su habilidad como propia de un acto creativo, es ésta mas bien la manera en que las cicatrices sobre la piel le impiden desprenderse de su historia personal. No se está frente a una expresión artística porque no pertenece a lo estéticamente bello, sino a la figura reconocible de un rostro en la violencia de la piel. 
  Asegura Catá que existe una relación causa-efecto entre el paso del tiempo y el olvido; sabe que el acto de olvidar secunda al inminente transcurrir de los años, por ello busca la permanencia corporal de sus memorias a través de diseñar una huella cuya ejecución por medio del dolor físico atestigüe lo vivido. El trabajo de David Catá se integra por fotografía, pintura, escultura y video. Constantemente trabaja con las ideas de la memoria, la permanencia y el olvido. 
  
http://culturacolectiva.com/wp-content/uploads/2014/01/tejidos-en-la-piel.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/piel-bordada.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/david-cata.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/retratos-en-la-piel.jpg
  Cada persona que conocemos nos construye en algún sentido. Su imagen sobre nosotros habla del entorno en que nos desarrollamos, y así su vida es parte de la nuestra, y viceversa. En Mi vida a flor de piel, Catá apunta que el dolor no es frontera, es red que sostiene a los involucrados en un acto de afecto. “[…] Pensar que en un momento mi mano ha tocado su mano”. 
 davidcata.com