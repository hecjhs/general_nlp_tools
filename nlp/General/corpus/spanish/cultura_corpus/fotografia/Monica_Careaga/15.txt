Elogio de la estabilidad, una representación visual del “eterno retorno”

 Las diferentes connotaciones del concepto filosófico del eterno retorno apuntan a una repetición del mundo en un ciclo del tiempo incesante. El mundo está destinado a ocurrir una y otra vez bajo las mismas circunstancias y como escenario de los mismos acontecimientos. Esto resulta en un pensamiento aterrador para el hombre que lo lleve a la desesperación por estar condenado a repetir lo vivido incansablemente. Pero decía Nietzsche que es la religión el obstáculo para la superación del hombre, en una metáfora del mito de Sísifo. El esfuerzo del hombre por avanzar se vuelve inútil por todas las limitaciones que la religión le impone, haciendo que regrese cada vez al punto de origen para comenzar de nuevo su camino. Esta visión de Nietzsche define el proceso como “la eterna tragedia de la no superación”. 
  En una representación del eterno retorno, el fotógrafo francés Nicolas Rivales presenta Elogio de la estabilidad , light paintings de aros de fuego que juegan con la idea del ciclo del que habla este concepto filosófico en el que el camino es una curva cerrada que conlleva un esfuerzo inútil. 
  Propiamente el fotógrafo no atribuye la serie a una representación del eterno retorno, pero la propuesta visual hace sentido con el término filosófico y la definición que hace Rivales sobre su trabajo. La luz dorada o el círculo luminoso que aparece en sus fotografías simboliza la esperanza del hombre por volver a comenzar en medio de la profundidad de la noche. La luz inhibe el caos y supone una oportunidad de éxito frente al esfuerzo inútil, aunque éste sea inminente. La luz nunca es más reconfortante que la angustia de la sombra, señala el francés. 
  
http://culturacolectiva.com/wp-content/uploads/2014/01/circulos-luminosos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/aros-de-fuego.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/aros-de-luz.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/circulos-de-luz.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/luz-aro.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/luz.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/puntos-de-luz.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/sol.jpg
  Sobre el nombre, Elogio de la estabilidad , a primera vista hace referencia a la condición de los aros de luz que permanecen sobre la imagen, pero bien puede suponer la creencia del hombre de que ha logrado superarse, sólo hasta que es azotado por su destino para volver a comenzar. 
 nicolasrivals.com