Almas iluminadas

 Fantasmas de luz fluorescente deambulan en Nueva Zelanda, también se han visto en Vietnam. Entes, algunos de rostros ocultos, se delatan por su silueta iluminada. Son como faros itinerantes que transitan en calles durante el ocaso de la noche. Son como extensiones de luz de luna que adoptara formas humanas y aparecieran elevadas sobre el mar, situadas bajo el arco de un edificio, agazapadas como esperando algo, mirando el horizonte, el camino delante de sus pies… o mirándote a ti. 
   El fotógrafo Fabrice Wittner se valió de la técnica de lightpainting, la cual captura la trayectoria de la luz en una fotografía por medio de la abertura prolongada del obturador y una larga exposición, para crear dibujos de personas a partir de luz. Wittner “pintó” hombres, mujeres y niños a partir de unas plantillas que él creó y que le sirvieron de guía para rellenar los espacios con luz blanca.  “Después de todo, el arte está hecho para la calle”.  La serie que lleva por título Enlighted Souls comenzó en mayo de 2011 y está compuesta por dos grupos de fotografías que tienen como constante la presencia de estas almas iluminadas que roban la atención por su brillante aparición en las calles de la ciudad. La primera entrega de esta serie se llevó a cabo en Christchurch, Nueva Zelanda, y comenzó como una contribución artística y moral del artista por el terremoto ocurrido en aquel país en febrero de 2011. Mediante el uso de plantillas, Wittner decidió pintar personajes imaginarios en los sitios visiblemente afectados por la catástrofe como una forma de “recordar las pérdidas humanas y mostrar el espíritu de una ciudad”. Para estas imágenes el fotógrafo decidió recrear personas colaborando en las tareas de rescate, así como niños, hombres y mujeres que transitaban por la calle observando los escombros. 
http://culturacolectiva.com/wp-content/uploads/2012/10/a1.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a2.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a3.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a4.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a5.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a6.jpeg  La segunda entrega de la serie, realizada en septiembre de 2011, se llevó a cabo en Hanoi, Vietnam, como resultado de un viaje hecho por Wittner quien no quiso desperdiciar tiempo y decidió repetir la técnica en las calles del país asiático. En este caso, el artista eligió a algunos niños de aldeas remotas al norte de la capital en quienes se basó para crear nuevamente sus plantillas guía. Actualmente este trabajo está en curso para ser desarrollado en otros lugares y más personas puedan ser parte del proyecto en un futuro cercano. 
http://culturacolectiva.com/wp-content/uploads/2012/10/a7.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a8.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a9.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a10.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a11.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a12.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a13.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a14.jpeg 
http://culturacolectiva.com/wp-content/uploads/2012/10/a15.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a16.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a17.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a19.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a20.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a21.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/a18.jpeg