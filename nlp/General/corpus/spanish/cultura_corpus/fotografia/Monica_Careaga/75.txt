Nigel Tomm y sus mujeres arrugadas

 Es como hojear una revista y confrontar una belleza casi celestial impresa en papel couché. Es sentir envidia y admiración con una misma imagen. Pasas las hojas y las miras a ellas de reojo, con cierto desdén, como si mirándolas feo se deformaran y puedas dejar de sentirte amenazada. Lees un poco el contenido “tips infalibles para adelgazar en una semana”, regresas la página anterior, quizá y ellas también leen ese tipo de artículos, lees un poco más y vuelven a aparecer con sus perfectas formas; te incomodas, arrancas la foto y la arrugas, al menos así no se contempla entera.    Pareciera que el objetivo en el trabajo fotográfico de Nigel Tomm es “destruir” la belleza por medio de la deformación de imágenes de bellas mujeres a las que después de fotografiar, literalmente, arruga como hojas de revistas.    El escritor, pintor, músico, director y fotógrafo experimenta con la fotografía de desnudo y el retrato, por medio de los cuales captura siempre a mujeres, ya sea su rostro o en sugerentes posiciones, donde una fotografía normal permitiría ver de forma explícita el cuerpo femenino, pero que sufriendo éstas una deformación por plegar el papel; Tomm permite una reinterpretación de las imágenes jugando con la sensualidad y el erotismo dejando que la mente del espectador complete la imagen.  “Me llamo Nigel Tomm y soy un artista”  De Tomm no se sabe mucho, sin embargo su trabajo ha circulado en la red logrando el reconocimiento de críticos, conocedores y espectadores. Su éxito ha sido tal, que su trabajo ahora puede ser visto en un libro titulado Famous Photographers Asked Nigel Tomm to Create the Most Famous Photography Book in the World: “Most Famous Photographers Must Have the Most Popular Photography Book of All Time” .    “Mi arte es el estudio de los problemas generales y fundamentales sobre cuestiones como la existencia, el conocimiento, la verdad, la belleza, la ley, la justicia, la validez, la mente y el lenguaje. Si el arte es entonces la suma total de todo lo que sabemos; mi arte es el depósito de todo lo que puedo recordar: el almacén de la memoria. Mi arte es un área importante en relación con las propiedades espaciales que se conservan en las deformaciones continuas de los objetos, por ejemplo, las deformaciones que incluyen estiramientos, pero sin desgarro o encolado”.       
http://culturacolectiva.com/wp-content/uploads/2012/10/1463.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2269.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/3214.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/4174.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/6130.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/7106.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/892.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/986.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1072.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/11129.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/12113.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/13102.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1464.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1538.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1637.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1729.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/1923.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2018.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/21104.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2270.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2320.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2417.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2516.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2615.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2717.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2815.jpeg
http://culturacolectiva.com/wp-content/uploads/2012/10/2915.jpeg