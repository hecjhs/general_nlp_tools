Marinus, la repetición de la naturaleza

 “No lo sabía cuando empecé este blog pero, aparentemente, hago GIF”. Hay momentos en la naturaleza que se desean conservar, prolongar y repetir, repetir, repetir… Escenas curiosas, de miedo y hasta chuscas trascienden por su permanencia en una fotografía, pero esto no es suficiente para Marinus, un artista visual holandés quien escala en esta técnica para repetir instantes de la naturaleza. 
  Gifs de la naturaleza es el término con el que se puede denominar el trabajo de Marinus. El artista retoma fragmentos de la vida silvestre, los desastres naturales, la vida marina y los sucesos astronómicos, y los convierte en originales animaciones que “conservan el presente”. Sus gifs los agrupa en su tumblr “Head Like an Orange” , el cual está activo desde octubre de 2011 y actualiza constantemente. 
  Marinus captura una escena brevísima, pero significativa de la naturaleza, y hace secuencias cortas para producir una animación a partir de una fotografía fija, también conocida como cinemagráfico: una imagen estática en la que se deja una acción repetitiva en alguna de sus partes; el efecto es bastante particular, pues son fotos fijas que aluden movimiento, lo que hace parecer que se presencian situaciones “vivas”. 
   
http://culturacolectiva.com/wp-content/uploads/2013/02/40279394488.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/40371550049.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/40954470589.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/41470313066.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/42220933700.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/42695285706.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/43027504931.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/43256038128.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/43929474327.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/44007689281.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/nature-2.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/nature-4.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/nature-6.gif
http://culturacolectiva.com/wp-content/uploads/2013/02/tumblr_m6l7siPebG1r4zr2vo1_500.gif