Las mujeres núbiles de Daniel González

 El ruso Vladimir Nabokov dejó por escrito en su lectura de néctar llamada Lolita que las nínfulas, criaturas escogidas que revelan su verdadera naturaleza demoniaca entre los nueve y los catorce años, viven en un reino de playas de aguas relucientes como espejos. Una isla encantada con rocas rosadas rodeada de mar vasto y brumoso. En un terreno de tiempo hechizado donde la magia de estas ninfas de fantástico poder se crece entre sus compañeras, y unas con otras juegan en los valles frescos, inconscientes de su belleza que enloquece a los fascinados peregrinos. 
  En la representación de estas escenas exquisitas, el fotógrafo colombiano Daniel González presenta su más reciente serie con la que es posible dar forma a los encuentros de las ninfas. En medio de valles y campos con árboles y hojas verdes, entre las hojas secas y la tierra viva, González capturó a estos seres esencialmente humanos que no son las nínfulas descritas por Nabokov, pero pudieron serlo. Hay que ser artista y loco, pero infinitamente melancólico, con una gota de ardiente veneno en las entrañas , para reconocer a las nínfulas de entre la multitud. Las mujeres que poblan sus fotografías aún conservan rasgos de candor perverso que abruma la conciencia de los hombres más centrados; ahora, las que alguna vez posiblemente fueron unas nínfulas, aparecen en su etapa núbil, deseosas y provocadoras de catástrofes en el mundo terrenal. 
  Las núbiles de Daniel González son felinos reconocibles por sus pómulos, de delicados miembros aterciopelados, con cabellos largos que ocultan de primer momento su belleza madura y provistas de pechos como peras o manzanas. Se reúnen en la espesura del bosque donde juguetean con sus cuerpos desnudos, tomadas de las manos, ocultas entre los árboles, dispuestas a ser inmortales en una postal. En otras imágenes aparecen protagonistas de sus rituales al cuerpo: reunidas en círculos carnales, con los brazos al cielo en ofrenda de su propio ser; otras veces colocadas como piezas del juego del placer. En otros momentos son sólo ellas, las núbiles en medio del entero. 
  
http://culturacolectiva.com/wp-content/uploads/2014/07/sexos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/amor.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/cielo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudez.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudo-mujer.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/fotos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujer-desnuda.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-al-desnudo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-desnudas.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-en-el-campo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-sensuales.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/ritos-mujeres.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/ritos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/rituales.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/sexo.jpg
  El trabajo fotográfico de Daniel González comprende, principalmente, el culto al cuerpo femenino a través de registrar su desnudez, aunque también cuenta con una serie dedicada al collage. Intersection, Several nudes, cinnamon, corallium, wheat, censure y on the mountain son algunas de sus series que recogen a bellas núbiles en convivencia con el medio natural. En algunas postales también es posible apreciar el desnudo masculino a través de la lente del colombiano. 
  
http://culturacolectiva.com/wp-content/uploads/2014/07/daniel-gonzalez.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudo1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/desnudos1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/hombres-desnudos.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-bellas.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/mujeres-desnudas1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/sexo-.jpg
http://culturacolectiva.com/wp-content/uploads/2014/07/sexo1.jpg
  El trabajo de Daniel González, de 19 años y de quien se desconoce más información, es una oda al cuerpo que nos contiene, y del que dice: puede no ser bello, pero en su naturaleza se reivindica como real. 
 Revisa su trabajo completo aquí: cargocollective.com/danielgonzalez 
 www.facebook.com/unfocusedsmile 
 www.flickr.com/photos/unfocusedsmiley/