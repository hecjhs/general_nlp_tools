Heinz Maier: Salpicaduras de color en alta velocidad

  Gotas de agua que refrescan, que mojan nuestro rostro con su brisa. Gotas de agua que bañan y visten las flores. Gotas de agua que sacian la sed pero que también mojan la ciudad. Gotas de agua que salpican para producir arte.      Heinz Maier es un fotógrafo alemán que captura el instante justo en el que una gota de agua se impacta contra una superficie, pero ¿Qué tiene de novedoso su trabajo? La respuesta es la salpicadura de la gota que crea figuras formadas sólo en la imaginación.     

 
Mediante la técnica de la fotografía a alta velocidad, Maier logra plasmar en una imagen las distintas formas que adopta una gota de agua de arriba hacia abajo; como atraída por un resorte que extiende su movimiento a todas direcciones creando formas que asemejan pequeñas esculturas de vidrio.
 

 
Aunque Maier tiene poco tiempo fotografiando salpicaduras de agua, ha logrado perfeccionar su técnica en la fotografía macro, con la que también captura insectos, otra de sus propuestas. La idea de fotografiar las formas de las gotas de agua surgió cuando el artista vio alguna de estas típicas imágenes y decidió darles un estilo más dinámico y colorido.
 

 
“He estado fotografiando desde el final de 2010, por ahora estoy experimentando con la fotografía macro que es una de las mejores maneras para relajarme”
 

 
Agua con colorante, enjuague bucal y la luz del flash teñida con geles de colores, son algunos de los materiales que Maier incorpora para hacer de sus salpicaduras de agua una obra de arte que invita al tacto pero sobretodo, recrea la vista.
 

 
“Lo más interesante de sus fotografías no es simplemente el instante que recoge, esas salpicaduras ya se han visto en otras ocasiones, sino la capacidad para replantear un tema tan explotado y llegar más allá hasta dejar su sello en cada imagen”
 

 
Este es uno de los montajes que utiliza el artista para tomar estas fotografías de salpicaduras multicolor; se trata de dos flashes de reportaje con geles de colores para conseguir imágenes en distintas tonalidades, un pequeño cartón que simula una caja de luz con dos destellos y mucha creatividad que dota a sus fotografías de un hiperrealismo que hace creer que son estructuras de cristal.
 

 
Maier es un fotógrafo que captura la belleza de pequeños instantes. Los colores, la iluminación, las formas que logra con cada gota de agua y su habilidad como reproductor de la técnica, es lo que caracteriza y da valor a su trabajo que parece tener una intervención digital. 
        
http://culturacolectiva.com/wp-content/uploads/2014/10/gota9.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota10.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota11.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota12.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota131.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota14.jpg
http://culturacolectiva.com/wp-content/uploads/2014/10/gota15.jpg