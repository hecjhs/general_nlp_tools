Pep Ventosa, en el carrusel

 En cada vuelta en el carrusel se construyen imágenes sube y baja de emociones encontradas en la diversión. Momentos difuminados suceden en círculo y por los ojos entran colores y formas de las figuras que parecen fragmentarse con el viento. Estos trozos del tiempo vistos desde arriba del carrusel son los que captura Pep Ventosa. 
  El trabajo de este catalán es un juego de niños; Pep Ventosa (1957) construye experiencias visuales a partir de mostrar lo que los ojos capturan cuando se recorre el mundo en un carrusel. Carousels, In the Round es el nombre de esta serie de fotografías que el español hizo arriba de este popular juego mecánico: en cada vuelta, Ventosa disparó varias veces a lo largo del camino y con cada imagen hizo una sola a través de colocar una sobre otra, así, no se mira un solo carrusel sino varias tomas separadas que materializan “una vuelta”. 
  Pep Ventosa hace con cada fotografía una exploración del propio medio a través de la deconstrucción y reconstrucción de las imágenes para generar una memoria visual. 
  
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-34.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-52.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-62.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-72.png
  Las fotografías de Pep Ventosa se han exhibido en EU, Reino Unido, España, Alemania, Francia y Suiza, y su trabajo está en la colección permanente del Museo de Arte Crocker.