¿Y si Cristo hubiera sido mujer?

  Una visión fotográfica de la materialización de Cristo en el cuerpo de una mujer es lo que ofrece el mexicano Ricardo María Garibay con su serie de imágenes en blanco y negro: Corpus Crista . 
  Garibay hace una reinterpretación de la mítica figura del Cristo, de la religión cristiana, y la plantea en una serie fotográfica en la que expone a éste como una mujer emulando la postura de su crucifixión, pero sin utilizar la cruz, sino apostando por la asociación que se tiene de Cristo crucifijado. 
  La serie de Garibay puede tener dos lecturas. Por un lado, su discurso resulta un tanto feminista con el que reivindica a la mujer al elevarla a un rango de divinidad para convertirla en la figura de salvación por excelencia, así, relaciona la identidad de Cristo con el reconocimiento que ha tenido la mujer a lo largo de la historia como la que sufre, llora y soporta los males, y los bienes. Por el otro, puede leerse la parte más sensible, y hasta romántica, que Garibay explora en el hombre a través de la mujer representada como fuente de vida. 
  Mención aparte merece la composición de las imágenes: la iluminación y la técnica del claroscuro que no resultan parte del ejercicio fotográfico sino que acompañan y profundizan el discurso y a la modelo misma, logrando un equilibrio natural que invita a mirar a detalle. 
  El origen y contexto de las imágenes de Garibay surge del día de Corpus Christi, celebración que data desde la Edad Media, en la que Juliana de Cornillón promovió una festividad en honor al cuerpo y la sangre de Cristo; Garibay retoma el concepto y lo traslada a un discurso contemporáneo en el que involucra una triada “divina”: la figura de la mujer, la representación de Cristo crucificado y el erotismo que distingue la obra del mexicano.  Ricardo María Garibay es Antropólogo Social con una Maestría en Desarrollo Rural y un Posgrado en Desarrollo Sustentable, por el Colegio de México. 
 Su labor como fotógrafo la ha desarrollado desde 1975, en la que se especializa en la fotografía etnográfica, la naturaleza viva y el desnudo femenino. Su trabajo se ha presentado en más de 30 exposiciones individuales y 26 muestras colectivas en México y el extranjero. 
   “Corpus Crista es un himno a la vida, a la lucha cotidiana, a la mujer. Muestra a mujeres reales de este tiempo. A mujeres dispuestas a ofrendarse, a luchar, a resucitar, a tomar decisiones y a crear su propio modelo de mujer. Mujeres que día a día libran batallas para hacer cambios y mejorar el mundo. Y dentro de la belleza de su fotografía Ricardo deja una reflexión: ¿Y si Cristo hubiera sido mujer?” 
 http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-33.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-5.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-8.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-9.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-10.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-119.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-121.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-151.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-171.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-181.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-191.png
http://culturacolectiva.com/wp-content/uploads/2013/04/Imagen-20.png