Claudia Cruz, la triada de la fotografía

 Su vida es un triángulo entre La Habana, España y México. Claudia Cruz tiene historia revolucionaria contada por el puño de su familia. Creció entre escritores y combatientes, y con una nostalgia profunda por el mar, una de sus pasiones después de la naturaleza y la escritura. 
  Las triadas de su vida definieron su vocación, le dieron sentido a lo que hace; primero como escritora y después como fotógrafa. Es con ésta que sustituyó el poder de la palabra con la fuerza de la imagen para documentar a la madre naturaleza, a la que reconoce como la procreadora de la vida. 
  El interés de Claudia por la naturaleza lo trasladó a la figura humana: sus formas, espacios, curvas y color. Pero, también, adquiere un sentido sociológico que incluye el papel del hombre en sociedad, en cómo percibe y aborda el mundo, cómo el exterior lo configura y lo ubica en el presente. 
  
http://culturacolectiva.com/wp-content/uploads/2013/11/Claudia-Cruz.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/desnudos-en-imagen.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/desnudos-fotograficos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/desnudos1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/fotos-de-desnudos.jpg
  Claudia se sabe nómada pero en un sentido más enraizada a La Habana. Esta condición viajera ha repercutido en la formación de su sentido de pertenencia a algún lugar, y son sus múltiples visiones del mundo las que alimentan su trabajo artístico abierto a la interpretación. 
  La cubana señala a su trabajo fotográfico indirecto y difuso por la “inestabilidad” que produce el constante movimiento de la información. Contrapone la inmediatez de los tiempos modernos, las formas directas de comunicación y el espacio árido para la imaginación con su propuesta fotográfica para el detenimiento y la reflexión. El arte, para Claudia, no se piensa: te gusta o no. Cualquier explicación sobra, pues la interpretación es una cuestión personal, de la historia de cada uno. 
  
http://culturacolectiva.com/wp-content/uploads/2013/11/barridos-fotos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/fotos-difusas.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/migracion.jpg
  Otro triángulo define su obra fotográfica: la técnica, el sentido de pertenencia y su condición viajera son las tres características que toma en cuenta para producir sus series. Sobre los resultados, casi siempre elabora un collage, “un espejo”, un cuadro, son estos las extensiones y materialización de su proceso creativo oscilante. Al final intenta justificar la forma y el motivo de sus fotografías. 
  
http://culturacolectiva.com/wp-content/uploads/2013/11/collage-colores.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/collages-Claudia-Cruz.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/collages-fotos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/collages1.jpg
  Las fotografías de Claudia son las imágenes de su cabeza en su estado consciente o no. Su visión y su multiculturalidad hacen la mitad de su propuesta, la otra se compone de los otros y en un sentido más profundo de los que considera sus momentos difíciles. Ningún “artista” ES sin su entorno social. El que lo crea que se encierre en una cueva y cuando termine de “crear” y mire el resultado, verá que tiene que ver con todo aquello de lo cual se alejó.  
http://culturacolectiva.com/wp-content/uploads/2013/11/paisaje.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/collages-arte.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/collages-de-color.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/fotos-colores.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/paisajes-collage.jpg
  Claudia Cruz estudió Comunicación en la Universidad Iberoamerica en el D.F., cuenta con una especialidad en cine y una maestría en Fotografía Documental por la EFTI, en Madrid. Su obra está compuesta por una propuesta audiovisual, mayormente dedicada a la fotografía y el video, producción y dirección de proyectos fotográficos y publicaciones escritas como El Fanzine.

http://culturacolectiva.com/wp-content/uploads/2013/11/EDWARD-WESTON-fotos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/EDWARD-WESTON-imagenes.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/EDWARD-WESTON-tributo.jpg

 claudiacruzbarigelli.virb.com