David Szauder, pensamientos fragmentados

 Cuando se trata de evocar un pensamiento es cuando más borroso se vuelve. La mente, se sabe, es el mejor disco duro, pero los recuerdos, aunque algunos siempre intactos, se dañan por el tiempo, el olvido, la nostalgia o el resentimiento. 
  El artista húngaro David Szauder, conocido como Pixel Noizz, indaga los rincones de la memoria donde las ideas y los recuerdos se fragmentan, y traduce estas composiciones en fotografías “dañadas”. La serie de Szauder, que lleva por nombre Failed Memories , explora la “naturaleza imperfecta de la memoria”, la que coarta la imagen clara de los pensamientos que generamos. Cuando por alguna causa física o emocional se pierden trozos de alguno, estas lagunas inundan la imagen y la vuelven borrosa. 
  
http://culturacolectiva.com/wp-content/uploads/2013/10/artista-David-Szauder.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/artista-hungaro-David-Szauder.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-artist.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-foto1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-fotos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-imagenes.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-images.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder-pictures.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/David-Szauder_.jpg
  Para evidenciar estos recuerdos fragmentados, Szauder combina técnicas digitales con fotografías: aplica distintos códigos sobre fotografías, sobre las que se ubica principalmente en la cabeza y el rostro, a través de software especializado para la edición de imágenes. El artista, señala, “no modifica el origen de la imagen, sólo su superficie”. Para finalizar, cada imagen se acompaña de un breve texto que explique, desde la visión del artista, la composición final.