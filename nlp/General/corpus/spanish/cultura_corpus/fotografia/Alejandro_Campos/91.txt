25 fotografías olvidadas por la historia

 Apuntas. Disparas. Repites la secuencia anterior decenas de miles de veces. La memoria se satura. Eliminas las tomas borrosas, movidas o que no te gustan. En cuestión de segundos puedes volver a tomar miles de fotos de nuevo. Sencillo. Pero no siempre fue así. 
 Antes de la invención de la fotografía digital y del desarrollo de memorias portátiles con capacidades insospechadas para los años ochenta, el mundo de la fotografía era distinto. Quizá conozcas los rollos fotográficos, las Polaroid y las cámaras desechables para esos viajes que te encontraban sin cámara. Probablemente recuerdas la incertidumbre de haber tomado una fotografía y desconocer si había “salido” bien. Y decimos “salido” aunque la fotografía sólo salga como tal de las Polaroids. Tal vez la expresión nos remita a los pensamientos ingenuos de los enemigos de las cámaras que afirmaban con tanta vehemencia que las fotografías nos robaban  el alma. Salimos de nuestro cuerpo para aparecer en la foto. 
 Imagina entonces acudir al revelado de fotografías, esperar la hora que la tienda de autoservicio estipula y ver que probablemente tu dedo aparece en la mitad de las fotografías. Otras más están sobreexpuestas, movidas,  algunas que tomaste sin darte cuenta y se te terminó el rollo. Para aquellos fotógrafos que hacían de su vida la proyección de la luz sobre el papel, la técnica era esencial. 
 Primero, conocer cada parte de la cámara, entender las funciones, herramientas y saberlas aprovechar. Y cuando se presentaba la ocasión, hacer magia con los principios de la fotografía para capturar, en esencia, aquello que se deseaba perpetuar. Ahora imagina tal situación ante la inmediatez de la vida misma, de los conflictos bélicos, accidentes, manifestaciones sociales, asesinatos. La fotografía debía ser rápida pero perfecta. Y también imagina esas fotografías donde apareces caminando por primera vez antes de estrellarte contra la alfombra, mientras cantabas arriba de una mesa a los ocho años y por qué no, aquel día que te quedaste dormido en tu propia primera comunión. 
 Cada fotografía cuenta una historia, por ello, Cultura Colectiva te presenta 25 fotografías históricas olvidadas por la historia. 
  El automóvil utilizado por los famosos bandidos Bonnie y Clyde cuando fueron abatidos. Testigos que presenciaron el tiroteo dicen haberse quedado sordos durante unas horas debido a la tremenda metralla. 
  El veterano de la Guerra Civil Estadounidense, Samuel Decker, posa con sus prótesis de brazos fabricados por él mismo. 
  Dos prisioneros de Attica juegan ajedrez a través de los barrotes de sus respectivas celdas. 
  Soldados estadounidenses se resguardan en una trinchera a menos de una milla de la detonación de una bomba nuclear de 43 kilotones en 1953. 
  Mark Twain fumando un cigarrillo en 1905. 
  Oficiales del Departamento de Policía de Los Ángeles posan encubiertos en 1960. El objetivo del operativo era detener a los ladrones de bolsas. 
  Niños en “pulmones de acero”, tratamiento empleado antes del descubrimiento de la vacuna contra la polio en 1937. 
  La librería principal de la ciudad de Cincinnati en Estados Unidos. Fue demolida en 1955. 
  La primera foto conocida de algún equipo deportivo en la historia. Corresponde a “The 1958 Knickerbocker Base Ball Club” de la ciudad de New Jersey. 
  Albert Einstein en su visita al Gran Cañón en 1922. 
  Eisenhower y Patton examinan una serie de artefactos robados por los nazis ocultos en una mina de sal en 1945. 
  La foto corresponde a un improvisado hospital estadounidense en la estructura de una destruida iglesia francesa en 1918. 
  Hombres y mujeres esperan en fila después de la gran inundación de Lousiville de 1937. 
  Un grupo de niños en una excursión escolar visitan a un bisonte en 1899. 
  El tradicional “Pony Express” utilizado para entregar el correo y el futuro de dicho trabajo en un avión postal. 
  La primera ambulancia perteneció al Hospital Central de Bellevue en 1869. 
  Una montaña de cráneos de bisontes listos para convertirse en fertilizante en 1870. 
  Madereros de Michigan en 1890. 
  Veteranos de la bandos rivales de la Guerra Civil Estadounidense se dan la mano en 1913. 
  El Teatro Rivoli presentando “Jaws” (Tiburón) en 1975. 
  Debido a la prohibición, el alcohol de todas las ciudades de Estados Unidos fue tirado al drenaje. La foto es de 1921. 
  Una casa improvisada como escuela en Alabama, 1935. 
  “Toro Sentado” y Buffalo Bill en 1885. 
  Así lucía el camino a Woodstock en 1969. 
  Lewis Powell, uno de los co-conspiradores en el asesinato de Abraham Lincoln. Sería colgado en 1865. 
 Fuente.