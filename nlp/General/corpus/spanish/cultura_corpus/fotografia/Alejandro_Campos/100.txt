62 fotografías históricas que seguramente no has visto

 La historia está llena de aventuras, situaciones y personajes que trascienden las épocas. Desde la invención de la fotografía, se nos ha concedido la oportunidad de atesorar esos momentos y recuerdos para siempre. Sin embargo, algunas fotografías han permanecido perdidas u ocultas a nuestros ojos. 
 Cultura colectiva te presenta 62 fotografías históricas que seguramente no conocías. 
 1.- William Harley y Arthur Davidson en 1914, fundadores de la famosa marca de motocicletas, Harley-Davidson. 
  
 2.-Bill Gates detenido por conducir sin licencia en 1977.  
 3.- The Beatles antes de tomar la legendaria foto en Abbey Road, una perspectiva muy distinta. 
    
 4.-Muhammed Ali y Michael Jordan en 1992. 
  
 5.- The Beatles en 1960, cuando aún eran un quinteto. Aparecen también Stuart Sutcliffe y Pete Best (Ringo Starr aún no era baterista). 
  
 6.- La primera fábrica de autos en Estados Unidos, Ford Inc en 1926. 
  
 7.- Robin Williams uniéndose al equipo de porristas en 1980. 
  
 8.- Disfraces tradicionales de una Noche de Brujas de 1900. 
  
 9.- El Estadio Camp Nou, en Barcelona en 1925. 
  
 10.- Pelea de box en el estadio de los Yankees en 1923. 
  
 11.- La Ciudad de Chicago en 1967. 
  
 12.- El legendario Bruce Lee en una pista de baile. 
  
 13.- Einstein con sus famosos zapatos peludos. 
  
 14.- Osama Bin Laden con sus compañeros de judo en Arabia Saudita a principios de los 80. 
  
 15.- Un niño con sus piernas artificiales en 1898. 
  
 16.- El rostro de la Estatua de la Libertad antes de ser enviada a Estados Unidos como regalo del gobierno de Francia. 
  
 17.- Fotografía de la grabación de Batman en 1966, destacan los efectos especiales utilizados. 
  
 18.- Un joven Kobe Bryant a los 18 años de edad en 1996. 
  
 19.-La última foto conocida del Titanic antes de que se hundiera en 1912. La fotografía fue tomada desde el ferry Queenstown el 11 de abril después de que éste le transbordara pasaje. 
  
 20.- Hachiko antes de su funeral. 
  
 21.- Jimi Hendrix y Mick Jagger en 1969. 
  
 22.- The Beatles y Muhammed Ali en 1964. 
  
 23.-Martin Luther King Jr. y Marlon Brando (Protagonista de ‘El Padrino’) 
  
 24.-Charlie Chaplin y Albert Einstein 
  
 25.- Chuck Norris y Bruce Lee 
  
 26.- Steve Jobs y Bill Gates. 
    
 27.- Ian Fleming y Sean Connery. 
  
 28.- Charlie Chaplin y Mahatma Gandhi. 
  
 29.- Marilyn Monroe y Sammy Davis, Jr. 
  
 30.-El primer Mickey Mouse dibujado por Walt Disney. 
  
 31.- La primera computadora en Inglaterra, 1950. 
  
 32.- Participantes del Maratón en los primeros Juegos Olímpicos modernos en Grecia en 1896. 
  
 33.- Puente Golden Gate de San Francisco en construcción en 1937. 
  
 34.- Fidel Castro y Malcom X en 1960. 
  
 35.- Una mujer sale de su refugio tras la explosión de la bomba atómica en Nagasaki en 1945. 
  
 36.- Kurt Cobain y Krist Novoselic con el ‘número de la bestia’. 
  
 37.-Vladimir Putin (primero de izquierda a derecha) durante su adolescencia. 
  
 38.- Johnny Depp y Oasis. 
  
 39.- Muhammed Ali intentando convencer a un hombre de no suicidarse. 
  
 40.- Robert Downey Jr. y Slash. 
  
 41.- El último concierto de The Beatles. 
  
 42.- Bob Marley jugando con una pelota de fútbol. 
  
 43.- Una publicidad de una computadora en la década de los 80. 
  
 44.- El astronauta Buzz Aldrin tomándose una selfie en 1966. 
  
 45.- El Presidente Bush recibe las noticias del 11 de septiembre de 2001. 
  
 46.- Jack, primer mono ‘estadounidense’ en regresar vivo del espacio sostiene un periódico con la noticia de su regreso. 
  
 47.- The Rolling Stones en 1963. 
  
 48.- Che Guevara y Fidel Castro en 1965. 
   
 49.- El Monte Rushmore bajo construcción en 1939. 
  
 50.- Jugando golf en un rascacielos en 1932. 
  
 51.- Llegada de los primeros plátanos a Noruega en 1905. 
  
 52.- Un japonés se rinde ante soldados estadounidenses. 
  
 53.- La Torre Eiffel en construcción, década 1880. 
  
 54.- Un innovador sistema de estacionamiento en Nueva York en 1930. 
  
 55.- Vehículo de una rueda, propuesto por M Goventosa de Udine, Italia, 1931. 
  
 56.- Barack Obama durante sus años de adolescencia en el equipo de basketball. 
  
 57.- El último concierto de Jimi Hendrix en el Festival de Amor y Paz en Alemania, 1970. 
  
 58.- Mahatma Gandhi en Londres. 
  
 59.- Queenie, el primer elefante en esquiar en la década de los 50. 
  
 60.- Kurt Cobain después de su arresto.  
 61.- Steven Spielberg divirtiéndose detrás de cámaras de la película ‘Tiburón’. 
  
 62.- Un músico que toca el acordeón, llora en el funeral de Franklin Delano Roosevelt.