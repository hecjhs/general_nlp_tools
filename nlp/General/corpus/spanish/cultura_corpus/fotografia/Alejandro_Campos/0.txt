25 fotografías que prueban que la humanidad está condenada a morir

 Escoria de mundo. 
 A veces quisiera no pertenecer a esta especie que lo ha destruido todo, y que no conforme con cazar y encerrar a cada especie que su ojo ha visto alguna vez, también tiene un desagradable gusto por matarse entre sí. Quizás aprendimos demasiado pronto a dominar al resto de los animales, y sin que el mundo nos forzara a unirnos para mantenernos con vida, encontramos diferencias que nos den razones para matar al otro. ¿Han muerto nuestros instintos? Durante miles de años perfeccionamos el arte de la guerra, y con ello, las maldiciones que tal fantasma trae consigo: hambre, miseria, tragedia, muerte, destrucción. Soledad. Tal fantasma sigue volando entre nuestros bosques y desiertos, al pendiente de una naturaleza humana que vive y respira odio, y que tiene un cerebro demasiado pequeño que reacciona antes de pensar. 
 – Me avergüenzo. 
 De los errores humanos que provocan cientos de miles de muerte, y de los cadáveres que no se lloran. 
 La tragedia de Bhopal causó más de 15 mil muertos en 1984. La fotografía, de Pablo Bartholomew capturó el momento en que un hombre enterraba a un niño. 
 Del horror humano que provoca que un niño, aterrorizado, confunda una cámara con un arma. 
 En diciembre de 2014, Osman Sagirli tomaba fotografías en un campo de refugiados sirio cuando una niña confundió su lente con un arma, levantó sus brazos, se mordió los labios y se rindió ante el fotógrafo. 
 De las huellas de sangre y dolor en un cuarto que escuchó los alaridos y la desesperación de miles antes de morir. 
 El genocidio de Ruanda provocó la muerte de más de 800 mil personas tutsis. La fotografía muestra las huellas de los niños que intentaban escapar del cuarto donde estaban encerrados. 
 Por los trágicos sucesos del pasado que jamás fueron esclarecidos pero miles de almas fueron humilladas. 
 Aunque los años pasen, la matanza estudiantil del 2 de octubre en México permanece como una herida abierta. 
 De las lecciones no aprendidas. 
 La persecución y asesinato de miles por las causas e ideales de un pequeño grupo apoyados en la ignorancia y miedo de todo un continente. 
 Que un rasgo de humanidad sea un privilegio y no un derecho. 
 Fotografía de 2003, en que un prisionero iraquí consuela a su hijo, quien tenía miedo de verlo encapuchado y con esposas, por lo que el padre pidió consideración para que le quitaran las esposas y pudiera abrazar a su hijo. 
 De los paraísos corrompidos. 
 Mar de Java en Indonesia. 
 – Me entristece. 
 Que lejos de mí aún la gente muera por ignorancia. 
 Perteneciente a la fundación Albert Kahn, esta foto muestra una costumbre mongola en que una mujer es encerrada en una caja de madera y se le deja morir de hambre. 
 Y por fanatismo. 
 En 1976 el fotógrafo Neal Ulevich retrató como simpatizantes del dictador Thanom Kittikachorn reprimieron una manifestación de estudiantes de la Universidad de Thammasat. 
 Que la pobreza provoque cientos de muertos apilados en las calles del mundo. 
 Fotografía de Don McCullin que retrató la miseria y el hambre en Nigeria. 
 Pensar en números, en cuentas bancarias y en presupuestos para cerrar puertas, fronteras y esperanzas. 
 Un grupo de migrantes kurdos es detenido en la frontera con Turquía en su intento por dejar atrás la guerra en Siria. 
 No ver las huellas del pasado. 
 Fotografía de 1945 que muestra la sombra de una víctima de la explosión nuclear de Hiroshima en unos escalones. 
 Que de una fotografía tan triste, la muerte la haga tan bella. 
 En Somalia, una mujer carga el cuerpo muerto de su hijo. 1992. 
 Los niños que lloran cuando sus padres son acorralados y sienten la violencia de la que tanto han huido. 
 Un par de niños sirios lloran cuando el ejército griego empieza a reprimir a sus padres para cortarles el paso. 
 La mirada incierta de una niña que ha perdido la infancia. 
 Esta fotografía de Abd Doumany retrató a una niña siria en un hospital de Damasco después de que su casa fue bombardeada en un ataque aéreo de 2105. 
 De la muerte de miles de animales por nuestra insensatez. ¿Cuántas especies han desaparecido y desaparecerán por nuestra avaricia y consumismo? 
 – Me enfurece. 
 Que el hambre aún sea un tema mientras miles de toneladas de comida son desperdiciadas. 
  
 El camino de un padre con sus hijos en una ciudad destruida, y las preguntas de los niños que entienden la guerra muy pronto. 
 Un padre camina con sus hijos por una calle destruida de Aleppo, Siria. 
 Que la pobreza se combine con el desastre para resultar en una tragedia. 
 Fotografía de Patrick Farrell que muestra el dolor y la muerte producto del terremoto de 2008 en Haití. 
 La tranquilidad con que algún día se entendió el odio, la intolerancia y la discriminación. 
 Una nana africana peina a una niña europea desde una banca en la que se le permite sentarse. 
 El silencio del mundo ante un pueblo que se defiende del mayor ultraje en la historia. 
 Un niño palestino arroja una piedra a un tanque israelí durante la Intifada. 
 Nuestro salvajismo indiscriminado. 
 Fotografías de mediados de 1870 en que se muestra a una larga pila de cráneos de bisonte americano a punto de ser convertidos en fertilizante. 
 Que hoy, todo lo que viva o muera dependa de nosotros. 
 Cadáver de un albatros que muestra que antes de morir, su estómago era un pequeño basurero. Fotografía de Chris Jordan. 
 – Me complace, me asusta y me alivia. 
 Que nuestro futuro no esté garantizado. 
 Fotografías que prueban que la tierra está muriendo