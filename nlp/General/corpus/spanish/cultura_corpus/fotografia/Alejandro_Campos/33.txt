Fotógrafos del fetichismo: el retrato del placer oculto

 “Jamás olvidaré la primera vez que me pidió que la asfixiara. Las luces de la ciudad entraban por los resquicios de las cortinas, y una pequeña franja de luz verde neón cruzaba sus pechos. Sus pezones estaban erectos, hinchados y heridos por los constantes mordiscos; mi espalda, en consecuencia, rasgada por el despertar de su excitación. Su voz pausada y cortada me susurró al oído, mientras su mano recorría mis muslos hasta llegar a la entrepierna, sujetando mi miembro con fuerza cuando dijo ‘asfíxiame’. La miré a los ojos y noté ese brillo de quien está camino a su lugar favorito. 
 Tierna, delicada, expectante. Sentí que todas las noches en ese viejo hotel de la colonia Centro me llevaron a ese preciso momento. Antes de responder, recordé la mirada de esa tímida chica que conocí en la calle, y a quien sin saber por qué, seguí hasta su casa. Recordé el par de ojos marrones viéndome a través de la cortina y el zapateo de vuelta a la puerta. El encuentro, la invitación y un mecánico intercambio de fluidos. Sentir las marcas de las cuerdas que sujetaban sus muñecas a la cama, notar el rimmel corrido a causa de las lágrimas de placer y esa mirada torcida al azotarme. Me sentí parte de algo mucho más grande y más placentero. Y me dejé llevar. 
 Ella esperaba mi respuesta, y sin dudarlo más, coloque mi mano izquierda alrededor de su fino cuello, mientras mi otra mano recorría caminos al sur de su ombligo”. 
  
 Cuerdas, látex, mallas, látigos. El fetiche termina donde la creatividad humana y la necesidad de saciar los deseos sexuales hacen lo propio. Lo principal es encontrar un elemento en que la fantasía sexual detone la satisfacción plena, la entrega de los sentidos y la inconsciencia de la mente. Erróneamente etiquetados como enfermedades, pues sólo se pueden considerar como tales cuando la práctica es recurrente, los fetiches también son un gran tabú en la sociedad. Sólo ‘los otros’ los practican, aquellos degenerados incapaces de sentir placer bajo las conductas normales. 
 Sin embargo, ante la lente de múltiples fotógrafos, el fetichismo se entiende como parte de la condición humana; un deseo de detonar el placer oculto en objetos o partes del cuerpo. Aprovechando su talento fotográfico, los artistas de la lente han centrado su toma en documentar las prácticas del otro mundo sexual, uno alejado de las prácticas socialmente aceptadas pero que representan un gran caso de estudio. Te compartimos el trabajo de seis fotógrafos del fetiche alrededor del mundo. 
 Jacques Biederer 
 De origen checo, Jacques fue uno de los pioneros en emplear la fotografía para retratar la sexualidad humana. Sus inicios se remontan a 1913, en París, cuando comenzó a fotografiar a mujeres en ropa interior, para dar paso al desnudo individual y en pareja con su debida producción: locaciones naturales o sets interiores. Posteriormente se centró en la fotografía del fetiche, retratando a mujeres y hombres jugando con látigos y corsets en escenas de dominación o de pony play.  Con una propuesta controvertida para la época, Biederer gustó gracias a la perspectiva artística de su lente. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Jacques-Biederer-fetichismo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Jacques-Biederer-fotografias.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Jacques-Biederer-fotos-fetiche.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Jacques-Biederer-fetiche-fotografias.jpg
  – 
 Robert Mapplethorpe 
 Considerado una de las figuras célebres de la fotografía en blanco y negro, Mapplethorpe estudió en el Pratt Institute Brooklyn, donde mostró una inclinación hacia el homoerotismo en el arte. El estadounidense alguna vez afirmó que no le gustaba la fotografía, sólo disfrutaba tenerlas en sus manos, sin embargo, los años lograron su encuentro profesional con la fotografía. Además de sus grandes retratos de celebridades en Nueva York de la década de los 70, también retrató la escena BDSM de la gran ciudad, mostrando los placeres ‘más bajos’ de una sociedad que en el día juzgaba y criticaba y por la noche disfrutaba de lo prohibido. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Robert-Mapplethorpe-fetichismo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Mapplethorpe-BDSM.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Robert-Mapplethorpe-blanco-negro.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Robert-Mapplethorpe-nueva-york-fetiche.jpg
  – 
 Elmer Batters 
 El gran maestro del fetichismo hacia las piernas, los pies y los dedos. Con múltiples fotografías en blanco y negro que destacan la belleza de las pantorrillas, la curvatura del pie y el uso sugerente de medias, Batters exploró uno de los fetiches más populares en el mundo. A pesar de sus múltiples imágenes, Batters pasó desapercibido durante gran parte de su carrera hasta que fue redescubierto por un editor alemán que le produjo tres libros. Los amantes de las extremidades bajas encontrarán en la propuesta de Batters una manera de desempolvar la fantasía sexual. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Fetiche-de-pies-Batters.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Elmer-Batters-fetiche-pies-jpg.png
http://culturacolectiva.com/wp-content/uploads/2015/09/Batters-fetiches-sexuales-pies.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Fetiches-sexuales-pies-Batters.jpg
  – 
 Nobuyoshi Araki 
 Este fotógrafo y artista de origen japonés tiene más de 40 años retratando distintas etapas de la desnudez femenina. A partir de una serie fotográfica que tomó durante la luna de miel con su esposa, Araki ha retratado a la mujer en sus diversas etapas de desenvolvimiento sexual. Ampliamente criticado por grupos feministas por promover la humillación física y psicológica de la mujer a través de su lente, Araki mantiene su interés por explorar el erotismo femenino. Sus escenas incluyen la dominación, al sadomasoquismo y algunos otros fetiches que logran la detonación del placer en los protagonistas de sus historias. El japonés acompaña sus fotos de textos escritos en forma de diario íntimo, reafirmando el sentido artístico de las imágenes. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Nobuyoshi-Araki-BDSM.png
http://culturacolectiva.com/wp-content/uploads/2015/09/Araki-fetichismo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/BDSM-Nobuyoshi-Araki.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Nobuyoshi-Araki-fetiche.jpg
  – 
 Ikko Kagari 
 La principal referencia del trabajo de Kagari es un libro publicado en 1994, llamado Pervert Rush , en el que el fotógrafo publicó su trabajo producto de múltiples viajes en el metro de Tokio durante las horas pico. En el mundo subterráneo, Kagari retrató aquello que sucedía entre el tumulto de gente, el roce de los cuerpos y la inexistencia del espacio personal. Gracias a una cámara infrarroja, el nipón captó los momentos en que parejas, desconocidos o personas solitarias aprovechaban el momento para satisfacer sus deseos y necesidades sexuales. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/Ikko-kagari-tokio.png
http://culturacolectiva.com/wp-content/uploads/2015/09/ikko-kagari-fetiches.png
http://culturacolectiva.com/wp-content/uploads/2015/09/Ikko-Kagari-metro-japon.png
http://culturacolectiva.com/wp-content/uploads/2015/09/fetiches-japoneses.png
   – 
 Toni Chaptom 
 Con colaboraciones para múltiples publicaciones especializadas en la fotografía fetichista, este fotógrafo español se centra en “el lado oscuro de la Dama” que decide posar para su lente, revelando a “esa niña mala” que vive dentro de cada mujer. Como parte de su composición Chaptom halla contrastes entre las modelos y las localizaciones. El BDSM y el fetiche hacia el látex son temas recurrentes en sus fotografías. http://culturacolectiva.com/wp-content/uploads/2015/09/Toni-chaptom-latex.png
http://culturacolectiva.com/wp-content/uploads/2015/09/Toni-Chapman-fotos-de-fetiche.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Chapton-latex-fetiche.png
http://culturacolectiva.com/wp-content/uploads/2015/09/Toni-chapton-fetiches.jpg