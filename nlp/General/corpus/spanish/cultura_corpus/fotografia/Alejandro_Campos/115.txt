Richard Silver: Fotografía de transición

 El escritor francés Marcel Proust, quien hablara en su libro “En busca del tiempo perdido” sobre los efectos del tiempo en la mente de las personas dijo alguna vez que “Los días pueden ser iguales para un reloj, pero no para un hombre”. Y así como los días no son iguales para nosotros, tampoco lo son las horas para edificios y monumentos expuestos a la transformación producto del paso del tiempo. 
  El artista neoyorquino, Richard Silver, ha capturado a través de una serie de imágenes, cómo distintos monumentos y edificios cambian de apariencia del día a la noche. A pesar de que ello se podría presentar a través de diversas imágenes donde se pueda hacer la comparación, el fotógrafo y artista decidió presentar la transición del tiempo en una misma fotografía. La serie titulada “Time Slice” muestra la transformación de los edificios a través de 36 fotografías tomadas en intervalos de varias horas y posteriormente combinadas en una sola composición. Si bien se trata del mismo edificio, en cada composición se puede apreciar cómo los efectos naturales del día (o la noche) pueden transformarlo a éste y a todo su entorno. 
 http://culturacolectiva.com/wp-content/uploads/2014/09/shanghaichina.png
http://culturacolectiva.com/wp-content/uploads/2014/09/suleymaniyemosqueistanbulturkey.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/tongarikieasterislandsunset.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/veniceitaly.png
http://culturacolectiva.com/wp-content/uploads/2014/09/birdsnestbeijingchina.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/colosseumromeitaly.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/hagiasophiaistanbulturkey.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/londonengland.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/milanitaly.png