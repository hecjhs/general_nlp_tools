38 primeras veces de la humanidad en fotografías

 La fotografía, inventada en 1826, nos ha permitido tener acceso a momentos, situaciones y personas de otras épocas históricas. El invento cuyos orígenes técnicos se remontan hasta la antigua Grecia, ha fungido como un fiel testigo de la historia y ha sido empleada con distintos fines. Es responsabilidad del hombre aquello que decida hacer con lo que su inteligencia, a través de la ciencia y el conocimiento, le permiten crear. 
 Cultura Colectiva te presenta 38 primeros momentos que fueron fotografiados y pasarían a la historia de la humanidad. 
 La primera foto de The Beatles con Ringo Starr como baterista. Agosto 22 de 1962 
   
 La primera foto de Abraham Lincoln (1840) 
  
 El primer restaurante McDonald’s en San Bernardino (1948) 
  
 El primer servicio religioso judío en Alemania desde la llegada de Hitler al poder. La ceremonia fue oficiada por tropas estadounidenses durante la batalla de Aachen (1944)  Jimi Hendrix quema su guitarra por primera vez (1967) 
  
 Jimi Henson creando su primer títere de “La Rana René” (Década de los 50) 
  
 El primer vuelo de los hermanos Wright. La foto fue tomada como evidencia del logro. (1903) 
  
 Gleen Burges y Dusty Baker, de los Dodgers de Los Ángeles, realizan lo que se cree fue el primer “high-five” (2 de octubre de 1977) 
  
 Henry Ford posa en el primer vehículo que construyó, el Cuatriciclo Ford (1896) 
  
 Hannah Stilley, nacida en 1746 y fotografiada en 1840. Es la mujer cuya fecha de nacimiento es la más antigua, en ser fotografiada. 
  
 Leola N. King, la primera oficial de tránsito en Estados Unidos. (1918) 
  
 La primer foto submarina (1893) 
  
 Sally Halterman, la primera mujer en recibir licencia para conducir una motocicleta en Washington D.C. (1937) 
  
 La primer foto tomada desde el espacio fue lograda por una cámara desde un cohete V2 (1946) 
  
 Howard Carter y sus asistentes al abrir la tumba de Tutankamón. (3 de enero de 1924) 
  
 La primer foto (conocida) de una investidura presidencial en Estados unidos, durante el juramente de James Buchanan (1857) 
  
 Louis Washkansky, beneficiario del primer transplante de corazón en el mundo. La foto fue tomada tres días después de la cirugía en el hospital Groote Schuur en Ciudad del Cabo, Sudáfrica. (6 de diciembre de 1967) 
  
 Mujeres de la ciudad de Minneapolis formadas para votar por primera vez en una elección presidencial (1920) 
  
 La primer y única foto tomada desde la superficie de Venus (1982) 
  
 La primer foto de un equipo de baseball (1858) 
  
 Madam C.J. Walker, la primer mujer en los Estados Unidos en volverse millonaria por sus propios esfuerzos a través de la venta de productos de belleza para mujeres afroamericanas. 
  
 La primer foto en la historia. Muestra la vista desde una ventana en Saint-Loup-de-Varennes en Francia (1826)   
 Fotografía de los primeros Juegos Olímpicos modernos (1896) 
  
 El primer bikini en ser mostrado públicamente (1946) 
  
 Otto Lilienthal se convirtió en el primer ser humano en lograr un vuelo en planeador (1894) 
  
 Annie Edson Tayler fue la primera persona que sobrevivió a la caída de las cataratas del Niágara en su cumpleaños 63. (1901) 
  
 Construcción de la primera rueda de la fortuna en el mundo construida para la Feria Mundial de Chicago de 1893. 
  
 ENIAC, la primera computadora en el mundo. (1940) 
  
 Instalación del primer letrero neón en las Vegas (1941) 
  
 El primer satélite en órbita, Sputnik 1 fue lanzado por la Unión Soviética. (1957) 
  
 Los primeros pasajeros del subterráneo de Nueva York (1904) 
  
 Bertha Benz, con la ayuda de sus dos hijos, se convirtió en la primera persona en conducir una distancia larga en un vehículo. Recorrió 106.21 kms el 5 de agosto de 1888. 
  
 La Reina Elizabeth II da su primer mensaje de Navidad a través de la televisión. (1957) 
  
 Ruby Bridges se convirtió en 1960 en la primer afroamericana en asistir a una escuela primaria “para blancos” en el sur de los Estados Unidos. Fue resguardada por policías debido a las amenazas contra su vida. 
  
 El primer árbol de Navidad del Centro Rockefeller (1931) 
  
 Los primeros plátanos llegan a Noruega (1905) 
  El primer “wheelie” en ser fotografiado (1936) 
  
 Fuente.