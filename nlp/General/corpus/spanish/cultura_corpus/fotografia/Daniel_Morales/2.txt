30 fotografías que capturan el erotismo y la “frialdad” de las mujeres rusas

 
 La sensualidad no conoce fronteras, el erotismo mucho menos. A través del lente de una cámara se puede capturar el deseo de la humanidad reflejado en el cuerpo de una persona. En su libro “Más allá del bien y del mal”, Nietzsche abre con las palabras “Suponiendo que la verdad sea mujer…”, uno de los tratados filosóficos más conocidos en la historia comienza en el supuesto de que la figura femenina es la única verdad. Tal vez eso es lo que piensa Krill Chernyavsky al hacer sus fotografías. 
   
   
   
  
 Llenas de erotismo, tal vez lo que pasó por la mente de Chernyavsky fue “Suponiendo que el erotismo es mujer”, pues sus imágenes no tienen una limitación al exaltar las cualidades femeninas de las mujeres rusas. Estas mujeres, pertenecientes a una tierra considerada fría e inhóspita que las pinta ante la comunidad mundial como personas solitarias, insensibles y preocupadas por cosas fuera de lo común para todos, son las que posan utilizando sus cuerpos como forma de protesta, rompiendo el estereotipo de la fría tierra postcomunista. 
   
      
 La sexualidad es su arma y ellas, seguras de su cuerpo, dejan que la cámara dispare contra ellas sabiendo que el verdadero daño es el que ellas causan ante los miles de espectadores que día con día crecen en las páginas de su camarada ruso, el fotógrafo Frill Chernyavsky. 
   
   
   
  
 La idea de una tierra fría e inaccesible que no permitió el paso a Napoleón ni a Hitler se desmorona ante la belleza natural del país más grande del mundo. Una relación de forma y fondo entre las mujeres y el paisaje hacen de las fotos una revelación ante los escépticos que creen que las nevadas y ventiscas dominan todas las ciudades rusas. 
   
   
   
  Mostrando otra cara de su país, el artista no deja pasar la oportunidad para dar una declaración acerca de algunas ideas políticas que son estereotipo de la antes Unión Sovietica. A pesar de la total sexualidad de sus obras, también hace guiños al humor. Una comida en McDonald’s no es algo que resalte en una imagen, pero la carga sexual de la modelo, junto a la idea de que la fotografía proviene de un país conocido por su pasado, hacen de la experiencia una gran sátira. 
   
   
   
  
 Sin una estética marcada, las imágenes pueden ser similares al trabajo que Santiago PGM hace en México. Tal como Santiago, el trabajo de Krill muestra a una mujer segura de sí, llena de vitalidad y sexualidad que no le da miedo explotar, pues sabe que las apariencias son una cosa, pero la realidad está en el interior. 
   
   
   
    
 No es el sexo por el sexo, no es la degradación de la mujer ni un intento de convertirla en un objeto. Recientemente, la actriz y modelo Emily Ratajkowski publicó un ensayo en el que decía lo siguiente: 
 “Para mí, “sexy” es un tipo de belleza, una forma de autoexpresión, una que debe ser celebrada, que es maravillosamente femenina”. 
   
   
   
   
  
 Ya sea en Rusia, Francia o México , la fotografía erótica es un tipo de arte que difícilmente pasa desapercibido. Es empoderamiento, voyerismo, perversión, arte y ese es sólo el comienzo. ¿Para ti qué es la fotografía erótica? 
 *** Te puede interesar: 
 La sutil perversión de la sexualidad femenina en ilustraciones 
 Ilustraciones sexuales, difícil verlas sin sentirlas 
 * Fuente: 
 VK