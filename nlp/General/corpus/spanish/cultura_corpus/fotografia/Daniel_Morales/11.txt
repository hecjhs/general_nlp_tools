Fotografías de la intimidad, el amor y los silencios de una pareja redescubriendo qué significa estar juntos

  Fue la repentina ola que me golpeó con tu llegada. No sabía si eso era amor, pero definitivamente era deseo. A diferencia de lo que suelo hacer, esta vez me acerqué. Llámalo confianza o embriaguez, pero me sentía bien y sabía que debía tenerte. Te convertiste en la causa de mi deseo y creía que querer siempre más era algo bueno. Te llevé a la cama esa misma noche y no puedo contar las horas, pero sí las veces. Salvaje, agotador y sumamente intenso; tú me hacías sentir deseada y altamente sexual. Nuestra química se disolvió cuatro veces y al disiparse la niebla nos encontramos en una soledad compartida que decidimos no romper. 
  
  
  
 No lo negaré, tuvimos momentos hermosos que se acercaron a lo que he conocido como felicidad. Tu presencia me alteró de tal forma que me atreví a hacer cosas que sin ti no habrían sido posibles. Tuvimos aventuras en las que me llamaste “amor” y así como creí ser feliz, creí amar. Rompimos tabúes y destrozamos estereotipos; nos encapsulamos en nuestro universo y no dejamos que nadie se acercara. Esa compañía mutua nos alejó de todos y no nos importó, gozábamos en soledad. Siempre juntos, siempre riendo, siempre gritando, entregados a sentir y no a escuchar. Sí hubo palabras, pero eran ecos vacíos de todo lo que callábamos, pues en realidad nunca estuvimos juntos.     
   
  
  
 Reflejo de nuestras circunstancias, la realidad llegó de golpe. Un día, sin más, me di cuenta de que estaba sola, que tu presencia en mi cama era un intento de llenar algo que tú simplemente no eras, además también lo notaba en ti. No eras feliz, no sé si en ese momento lo descubriste como yo o simplemente querías recrear esos primeros encuentros llenos de erotismo. No fue el tedio, la costumbre o los celos, como le sucede a tantas relaciones; nosotros simplemente no éramos. Fingimos intentando mantener esa farsa que llamábamos amor, pero fue inevitable tocar fondo y darnos cuenta de que si nos encontramos en soledad, el futuro no prometía algo mejor. 
  
  
   
 La desintoxicación no es difícil. Acostarme con otros, usar la compañía de extraños como sedante ante el dolor, convertirme en esa mujer que no se reconoce y finalmente, regresar a una vida que no conocía, buscar emocionalmente el cobijo y la ayuda de las personas que abandoné por ti. Algo bueno pasó, me di cuenta de que la soledad estaría siempre en mí, no luché por alejarla, permití que me abrazara y en ella encontré mi esencia. No necesitaba estar con alguien para validar mi persona. Me encontré, construí sola lo que soy y me juré nunca dejar que alguien más me lo quitara. Soledad no es estar sin alguien a tu lado, soledad es dejar atrás tu personalidad por la necesidad de querer sentirte cercano a alguien. 
  
   
  Reencontrarnos fue una desagradable sorpresa que se convirtió en una maravillosa coincidencia. El recuerdo y el dolor del pasado no fueron fáciles de olvidar, pero las experiencias por venir se pronosticaban distintas gracias a que éramos otros. La sexualidad agresiva dio paso a una intimidad sutil que hoy ambos apreciamos, ese primer encuentro es un alegre recuerdo, pero hoy sabemos que podemos hacer el amor una vez y después hablar todas las noches. Hoy nos conocemos por primera vez, nos encontramos compartiendo momentos y viviendo experiencias aparte. Hoy las palabras no cesan, hoy me conoces y lo más importante de todo, hoy me conozco. 
   
 Irina Munteanu es una fotógrafa polaca de 22 años que se convirtió en una sensación de Internet al publicar fotografías con una intensa carga emocional cuando tenía 19 años y aún no ingresaba a la escuela de fotografía. Hoy sus fotografías han aparecido en revistas como  Love Issue, Soft Skin, Glacier Magazine, Télégramme Magazine y más. El texto de ficción es obra del autor.  
 *** Te puede interesar:  
 Canciones que te recuerdan a esa persona y te hacen saber que estás enamorada  
 Fotógrafas eróticas que están rompiendo las reglas 
 *** Fuente: 121 Clicks , La Mono , Cultura Inquieta