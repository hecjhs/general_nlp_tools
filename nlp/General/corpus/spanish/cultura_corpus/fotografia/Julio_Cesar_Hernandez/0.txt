A través de los ojos del cóndor; la fotografía aérea antes de los drones

 “Este lugar es verdaderamente una tierra donde la naturaleza domina… no el hombre. Es tierra de nadie”. Robert B. Haas Los cielos son ese lugar en el mundo que no nos pertenece, que sólo tiene sus habitaciones abiertas para las criaturas que han nacido con alas, que pueden elevar su vuelo por la nubes como el cóndor, que imponente surca los aires de la magnífica América Latina. Así, con las alas desplegadas, todos podemos verlos y recordar la magnificencia de una tierra que parece no tener fin. 
 Si el cóndor vuela con las alas, el ser humano lo hace con sus deseos; bien dicen que el desear es la primera sala antes de cruzar el umbral de la realidad. 
 Volar había sido un placer vetado para los mortales. Ícaro, el padre de los soñadores, lo intentó en un tiempo que ya no existe, y que no sabemos si existió. Aquél que voló alto y por querer volar más alto, sus alas se atrofiaron, dejándolo a la suerte de su alas de humano, para caer en el Egeo y morir. 
  
 Otro de los deseos humanos más profundos es poder congelar el tiempo, hacerlo perpetuo y muerto, sin movimiento, ver incontable número de veces lo que hemos visto pasar frente a nuestros ojos, para después verlo desaparecer entre las cataratas del tiempo. 
 La vida es tiempo y el tiempo es vida. Lo que ocurre en la naturaleza es algo que no se puede apreciar si no se ha estado allí, o por lo pronto, si alguien no ha estado allí para disparar el obturador de su cámara, y cómo semidiós poseer en una caja negra el tiempo y la luz. 
  
 El fotógrafo Robert B. Haas es autor de esta serie fotográfica llamada A través de los ojos del cóndor que fue publicada en 2007 por National Geographic. Desde 2002, Haas ha centrado su trabajo en la fotografía aérea, logrando captar situaciones jamás antes vistas en la naturaleza, como la fotografía arriba, que muestra una parvada de flamingos, que en conjunto y casi como poniéndose de acuerdo, forman la figura de un enorme flamingo. 
 Por fin el sueño, ya conquistado de volar, se complementa con el otro deseo de congelar la luz y el tiempo, generando una síntesis hermosa donde al volar, captura al mundo para así, como Prometeo, trayéndonos a los demás lo hermoso del planeta Tierra. Eso es lo que Robert B. Haas ha hecho, incluso antes de que se pensase siquiera en utilizar un dron para retratar la naturaleza. 
    Los dos más grandes deseos del hombre conjurados, volar y el congelar la vida, Haas los une para entregarnos imágenes irrepetibles. 
  
  
 Para el recorrido que el fotógrafo hizo por Latinoamérica , contempló países como Argentina, Chile, México, Perú, etc. donde capturó las escenas más impactantes del continente. No sólo se limitó a tomar fotografías de los ambientes y contextos naturales, sino también se dio tiempo de capturar las ciudades con su lente, todo, arriba de una avioneta o helicóptero como desde los ojos del cóndor. 
  
  Curanilahue, Chile. Posas de tratamiento de agua junto a una fábrica maderera. 
 Ese cóndor que se queda para sí las visiones de su América, jamás podrá decirnos cómo es que nos mira desde el cielo. Haas, en un afán de Ícaro-Prometeo, toma el lugar del cóndor y nos baja de los cielos azules, lo que celoso el ave guarda. 
  
  
  
  
 La intención de Haas es mostrarnos ese lado salvaje de una América Latina que decimos que es nuestra, pero que a la vez no lo es. La América Latina donde el cóndor vuela es de sí misma, y nosotros sólo la admiramos y transformamos para nosotros, sin ser nuestra. Y a pesar de eso, la vivimos. Es nuestra y de sí misma, eso es América Latina. 
  
  
  
 ¿Ya ven lo que el cóndor ve? 
 *** 
 Te puede interesar: 50 fotografías que demuestran la belleza del mundo