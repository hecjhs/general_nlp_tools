Fotografías icónicas de los últimos 100 años (Parte dos)

 El fotógrafo José Ramón Bas considera a la fotografía como una interesante herramienta para captar energías, no sabe qué es lo esconde el papel fotográfico, pues es algo difícil de explicar… Sucede y ya está. La fotografía es testigo infalible de la realidad que cuestiona el mundo y el hombre que lo habita. En ella se encuentra el sentido de todo y es a través de éstas que se muestra lo absurdo y complicado de la vida. Hay algunas imágenes que hablan por sí solas, y aquí presentamos algunas de las más conmovedoras. La segunda parte de una colección de fotografías icónicas de los últimos 100 años que demuestran la angustia de la pérdida, el poder de la lealtad y el triunfo del espíritu humano. 
  Niño que muere de hambre toma de la mano a rescatista. 
   Cirujano descansa tras una operación de trasplante de corazón de 23 hora de duración, que resultó exitosa. 
  Diego Frazão Torquato, niño brasileño de 12 años, toca el violín en el funeral de su maestro, quien le enseñó a escapar de la pobreza y la violencia a través de la música. 
  Un soldado ruso “toca”  un piano abandonado en Chechenia, 1994 
  Joven descubre que su hermano fue asesinado. 
  Hombre cae desde el World Trade Center el 9/11. 
  Las tumbas de una mujer católica y de su marido protestante, Holanda, 1888. 
  Demostración del uso del condón en un mercado de Jayapura, capital de Papúa, 2009. 
  Los soldados rusos se preparan para la batalla de Kursk, julio 1943. 
  Hombre afgano ofrece té a soldados estadounidenses. 
  El poder de las flores. 
  Pareja abrazada en los escombros de una fábrica derrumbada. 
  En India, hombres sin hogar esperan alimentos distribuidos gratuitamente  fuera de una mezquita. 
  Puesta de Sol en Marte. 
  Niño gitano en víspera de año nuevo. En la comunidad gitana es muy común que niños comiencen a fumar a muy temprana edad. 
  Durante las inundaciones de la ciudad de Cuttack, India, en 2011, un aldeano salvó a numerosos gatos callejeros con una cesta que mantenía en equilibrio sobre la cabeza.