Ebrahim Noroozi y sus contratos fotográficos

 Simon Norfolk, fotógrafo documental nigeriano, afirma que cuando se fotografía el horror, se firma un contrato moral con las, en ese momento, victimas. Dicho contrato compromete al fotógrafo a convertir el sufrimiento en un testimonio para que las injusticias no vuelvan a pasar. Por eso, el contrato de un fotoperiodista es importante, pues debe tener claro que las imágenes que mostrará deben cumplir con éste… si no lo hace, sólo sería un explotador más de la desgracia ajena. 
  Irán es el segundo país, por detrás de China, con más penas de muerte al año. Según la ONU, en 2011 se llevaron a cabo 600 ejecuciones. En Irán, cientos de personas se conglomeran para observar la ejecución de alguien que violó las reglas morales y las Leyes del país. Esto con el fin de demostrarle a la población iraní que los actos injustos se pagan. 
 En 2012, Ebrahim Noroozi, fotógrafo iraní, ganó el premio World Press Photo por su serie Hanging in Iran (Ahorcamiento en Irán). Ésta muestra la ejecución pública de cuatro criminales que entraron a una casa haciéndose pasar por basijis (guardias de la moral islámica). Una vez dentro del hogar, retuvieron a los hombres y violaron a las mujeres que habitaban el lugar. 
  La fotografías de Ebrahim no buscaban centrarse en los ejecutados sino en las situaciones que se presentaban al rededor del hecho que llevó a los cuatro hombres a la horca; desde el momento en el que la gente se comenzó a reunir para ver la consumación, hasta cuando los cuerpos de los ajusticiados yacían colgados desde el “paredón”. Hanging in Iran,  muestra, en imágenes en blanco y negro, el momento anterior a los segundos más vulnerables de la vida, el momento justo en el que todo ser ya no es dueño de sí mismo y se entrega a la angustia y desesperación: el momento anterior a la muerte. 
  De esta manera, Ebrahim propone a la fotografía no como una respuesta ni una pregunta, sino como una exclamación; una que muestra la incongruencia de ambas partes. Por un lado, la barbarie de violentar a la comunidad y por la otra, la de ejercer justicia “a través del peso de la Ley”. 
  
http://culturacolectiva.com/wp-content/uploads/2013/06/nmkl.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/mskz.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/ns.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/b..jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/bkm.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/ncncdld.jpg