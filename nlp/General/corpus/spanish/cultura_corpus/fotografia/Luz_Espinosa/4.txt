Mujeres fuertes durante la Historia

 Koni Annan, Secretario General de las Naciones Unidas de 1997 a 2006, reconoció durante un discurso en conmemoración al Día de la Mujer, que en sociedades destrozadas por la guerra, frecuentemente son las mujeres las que mantienen todo en marcha, pues son, usualmente, las principales defensoras de la paz. Y es que mientras el mundo parece ser tomado por hombres, son ellas, el 51 por ciento de la población mundial, quienes logran una unión social a través de la inclusión, esa a la que ellas y sus acciones han llegado. 
  Margaret Bourke-White en el edificio Chrysler para tomar una fotografía en 1934. Campesinas, estudiantes, profesionistas, amas de casa, artesanas e intelectuales han roto las barreras que la sociedad machista había impuesto durante los siglos pasados. María Teresa Fernández de la Vega dijo, durante un discurso ante el gobierno español, que “cada vez que una mujer da un paso, todas avanzamos”, y la historia de la humanidad está llena de mujeres que se han atrevido a dar el primer paso para que sus iguales sigan avanzado y rompan los esquemas impuestos generación tras generación. 
  Primer equipo de baloncesto femenil. A lo largo de la Historia han aparecido mujeres que cambiaron de manera trascendental la existencia de la generaciones venideras: Eva Peron, Simone de Beauvoir, Marie Curie, Anais Nin, Frida Kahlo y Rosa Luxemburgo son algunas de las que con sus descubrimientos, investigaciones y arte demostraron que las mujeres son más que labores del hogar. 
  Mujer samurai, 1800. A continuación presentamos una serie de fotografías en las que aparecen mujeres que cambiaron e hicieron increíble la Historia: 
  En 1967, Katherine Switzer se convirtió en la primera mujer en correr el maratón de Boston. 
  En 1907, Maud Wagner fue reconocida como la primera tatuadora. 
  En 1976, Ellen O’Neal se convirtió en uno de los primeros skaters profesionales. 
  Leola N. King, primer agente de tráfico femenina de Estados Unidos en 1918.  Enfermeras estadounidenses en Normandía, 1944. 
  Miembros femeninos de Ángeles del Infierno en 1973. 
  Las primeras mujeres en participar en  roller derby en 1950. 
  Primeras mujeres afganas en estudiar medicina, 1962. 
  Mujeres boxeando en una azotea de Los Angeles, 1933. 
  Coalición de Liberación de las Mujeres marchando por la igualdad salarial en 1970. 
  Mujer musulmana protege a una mujer judía cubriendo su estrella de oro en Sarajevo durante la Segunda Guerra Mundial. 
  Jeanne Manford marchó, en 1972, en la ciudad de Nueva York durante el desfile del orgullo gay.  Gertrude Ederle después de convertirse en la primera mujer en nadar el Canal Inglés, 1926.