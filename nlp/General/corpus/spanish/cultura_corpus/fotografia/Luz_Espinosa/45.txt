Hendrix Kerstens: un diálogo entre la pintura y la fotografía

 Si se planteara un diálogo entre el arte del siglo XVII y el contemporáneo, seguramente la mejor discusión se centraría entre la pintura y la fotografía, pues muchos fotógrafos contemporáneos se nutren de las tradiciones clásicas para presentar su trabajo. Composición escénica, iluminación y encuadres son elementos innegables en la relación existente entre una y otra técnica. 
  Vermeer, uno de los mejores pintores de los Países Bajos, reconocido por su maestría en el uso y tratamiento de la luz, ha sido la inspiración del fotógrafo holandés Hendrik Kerstens, quien ha retratado durante 15 años a su hija, su única modelo, bajo la estética de los retratos del siglo XVII. 
  En 1995 Kersten tomó como modelo a su hija Paula, y bajo una iluminación utilizada por pintores del siglo XVII comenzó su carrera fotográfica. 
 El trabajo de Hendrix juega con el lenguaje de la pintura a través de la fotografía. Sus composiciones rompen con la solemnidad de los retratos más representativos de los pintores holandeses a través de la inclusión de bolsas, servilletas, lámparas o retazos de tela para generar un diálogo lleno de humor entre pasado, presente y futuro. 
 Las fotografías de Kersten abren una puerta al pasado, dejan conocer el presente de su modelo y permiten echar un vistazo al futuro, desmitificando las estéticas temporales. 
  Kersten lleva más de 15 años fotografiando a su hija con un atuendo diferente en cada imagen y siempre bajo la misma estética: el oscuro fondo contrasta con la clara piel de la chica quien dirige una misteriosa mirada “que se niega a hablar”. 
  
http://culturacolectiva.com/wp-content/uploads/2013/07/curtain.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/tumblr_m2e9kfQys11r353bno1_r3_500.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/hendrik-kerstens-1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/large.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/pimp-up-towel-Hendrick-Kerstens.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/712000.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/tumblr_luv94roew11r2an97o1_500.png
http://culturacolectiva.com/wp-content/uploads/2013/07/HENDRIK-KERSTENS.jpg
http://culturacolectiva.com/wp-content/uploads/2013/07/red2brabbit2biv.jpg