La curiosidad en la fotografía

 En la fotografía lo único que cuenta es la curiosidad. Cuando ésta se tiene, difícilmente se querrá regresar a ver sólo lo cotidiano. Ser curioso es encontrar en cualquier rincón algo que asombre, despierte o genere una sensación: coraje, tristeza, alegría o emoción. Ser curioso hizo que Anna Belova, quien ahora firma su trabajo como Anka Zhuravleva, experimentara con el dibujo, la arquitectura, el óleo y la fotografía, haciendo de su trabajo una sutil y curiosa mezcla entre lo onírico y lo sensual. 
   Las fotografías de Anna son de una fuerte carga emotiva; en éstas, mezcla realidad, fantasía y sentimientos para transmitir lo que con palabras le sería imposible. Su curiosidad artística la ha llevado a experimentar con el dibujo, la pintura, la fotografía y las artes visuales. 
  Anna Belova ingresó al Instituto de Arquitectura de Moscú para estudiar la misma profesión que su madre, quien le inculcó el dibujo desde pequeña, pero después de la muerte de ésta, decidió experimentar entre el diseño de tatuaje, una banda de rock y la fotografía para las revistas Playboy y XXL .   En 2002, el pintor Gavriil Lubnin le enseñó la técnica al óleo, con la que experimentó en los años siguientes. Durante ese periodo realizó algunas obras de gran carga emocional, ésta era tan fuerte que al término de su serie parecía que las piezas habían sido creadas por diferentes personas. 
  En el 2006 comenzó a dedicarse a la fotografía, en la que maneja con mayor facilidad la dualidad del blanco-negro, y el color, y se sumerge tanto en la melancólica expresión de sus retratos más frágiles como en las secuencias de sus personajes voladores. Estas imágenes le han dado reconocimiento internacional y han mostrado el verdadero estilo de la artista. 
  http://culturacolectiva.com/wp-content/uploads/2013/02/an2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/an3.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/an4.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/an1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/jsjdh.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/an6.jpg
http://culturacolectiva.com/wp-content/uploads/2013/02/an7.jpg