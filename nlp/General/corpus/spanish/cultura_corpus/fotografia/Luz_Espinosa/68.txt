Fumar es un placer genial, sensual

  Fumaba por el largo y estilizado pitillo, inhalaba y exhalaba el sensual humo que salía por mis labios e invadía el lugar. Amo el acto de fumar, desde encender el cigarrillo hasta escuchar el leve susurro que lanzo al aire en señal de aprobación con el tabaco. Por que estoy de acuerdo con la canción: Fumar es un placer genial, sensual.    A inicios de siglo, la acción de fumar dotaba de estética y estatus a quien la hacía, encender tabaco en pipa, pitillo o un puro fue una imagen explotada en distintos productos, producciones y marcas, al menos en Occidente. En oriente, fumar no es un acto de consumo que denote estatus o genere una visión positiva o negativa de quien lo haga a pesar de las enfermedades y males que esto provoque.     Frieke Janssens es una fotógrafa belga que después de estudiar fotografía, se interesó por la psicología y en especial el concepto que se tiene alrededor del acto de fumar en el Oriente y Occidente del mundo. Para mostrar el concepto de fumar, la fotógrafa decidió usar a niños como modelos. Las fotografías muestran a menores vestidos con moda de inicios de siglo y mediante estas imágenes, Frienke intenta separar la norma social del “adulto fumador” centrando la atención en el hecho mismo de fumar. “Sentí que ver a niños fumando tendría un impacto real sobre el espectador y lo obligaría a observar el acto de fumar, sobre simplemente hacer supuestos de la persona que lo hace”.    A Frienke la inspiró el video del niño indonés que fumaba dos cajetillas de cigarros al día. Para la creación de sus fotografías se utilizaron palitos de pan y queso para simular los cigarros y baritas de incienso para el humo. 
http://culturacolectiva.com/wp-content/uploads/2012/11/fu1.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu2.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu3.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu4.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu7.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu9.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu10.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu11.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu12.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu13.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/fu14.jpg