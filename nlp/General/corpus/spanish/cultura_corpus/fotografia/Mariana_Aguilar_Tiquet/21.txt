Los mejores fotógrafos de guerra

 Nunca he logrado comprender la guerra. Es difícil ver cómo los hombres se hacen tanto daño los unos a los otros. Han vendido su corazón a cambio de un signo que dice valer mucho, uno que les garantiza, según ellos, tener más de lo mismo, de ese pedazo de papel que mueve al mundo. Pero no es sólo eso lo que buscan. Tener el control significa todo. El poder guía sus vidas. Es quien camina al frente seguido de un mar de hombres que como olas golpean y arrasan con lo que se encuentran. Inundan y se apoderan de lugares que no les pertenecen. Las lágrimas de los afectados no dejan de caer. Ojos de cristal se pueden ver por doquier con llantos que acompañan la furia del mar, uno que cada vez se torna más salado.  Sé que entre tantas lágrimas algunos ahora pueden ver con más claridad, que al limpiar los ojos la venda cae de éstos; pero son los que ya no lloran los que más daño hacen. Esos que paulatinamente van perdiendo la vista. 
 Están también aquellos hombres que sirven de ojos para el resto del mundo. Para todos los que están lejos del mar salado, y para los que la tempestad parece lejana y distante. Son esos hombres los que se acercan a las olas y se sumergen en el Océano para mostrar lo que sólo los que llegan a las profundidades conocen bien. Algunos les llaman fotógrafos de guerra y otros fotoperiodistas. Para mí son hombres que descienden a las profundidades, ahí donde la oscuridad reina y el oxígeno falta. Hombres que cargan con sólo una cámara.  Robert Capa Nacido con el nombre de Endre Ernö Friedmann, el húngaro fue un corresponsal y fotoperiodista que documentó conflictos bélicos como la Guerra Civil Española, La Segunda Guerra Mundial y la primera Guerra de Indochina, entre muchos otros. Irónicamente, el joven que pasó a ser mundialmente conocido como el gran fotógrafo de guerra pudo ver su sueño hecho realidad cuando se convirtió en “fotógrafo de guerra en paro” debido a la ausencia de nuevos conflictos. Hoy lo recordamos como uno de los grandes, fundador de la agencia Magnum y testigo del sufrimiento humano. Sufrimiento que quedó grabado en los negativos que ni el agua salada podrá borrar.

http://culturacolectiva.com/wp-content/uploads/2015/06/robert-capa4.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/robert-capa11.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/robert-capa1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/robert-capa3.jpg

 Tony Vaccaro Muy joven para disparar el obturador, pero no para disparar un arma. Soldado y fotógrafo, Vaccaro recuerda lo que el ejército opinó de su trabajo como fotógrafo durante la Segunda Guerra Mundial.  A los 22 años de edad, Vaccaro encontró la manera de realizar fotografías con su Argus C3 al mismo tiempo que combatió al lado de la División de Infantería No. 83 de Estados Unidos. Las fotografías durante el desembarco en Omaha, Normandía, así como las imágenes capturadas en Alemania forman parte importante del archivo fotográfico del Memorial de Caen. Una vez concluida la guerra, Vaccaro permaneció en Alemania donde obtuvo los primeros trabajos como fotógrafo para diversos medios. 
  
http://culturacolectiva.com/wp-content/uploads/2015/06/tony-vaccaro3.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/tony-vaccaro.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/tony-vaccaro1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/tony-vaccaro2.jpg
  Te puede interesar: El fotógrafo que retrató los horrores de la Segunda Guerra MundialJames Nachtwey ¿Cuántas veces hemos visto la misma ola venir y golpear con gran fuerza? Una tras otra llega y golpea, haciendo más difícil la labor del fotógrafo para congelar un instante. Nachtwey hace uso de la cámara para inmortalizar hechos dramáticos, y así evidenciar las atrocidades que ocurren en el mundo para que no sean olvidadas, pero sobre todo para evitar que se repitan. Una imagen es todo lo que se necesita para congelar las olas que la guerra trae consigo y mostrarlas al resto de la humanidad.

http://culturacolectiva.com/wp-content/uploads/2015/06/james.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/natch1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/natch.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/james4.jpg

 Larry Burrows Burrows fue un fotoperiodista inglés que tuvo un acercamiento a la imagen cuando trabajó para la revista Life, donde imprimió fotografías de Robert Capa. Testigo del dolor causado por uno de los peores enfrentamientos del siglo XX, documentó la Guerra de Vietnam entre 1962 y 1971. Su trabajo se vio impedido cuando el helicóptero en el que viajaba, junto con los fotógrafos Henri Huet, Keisaburo Shimamoto y Kent Potter, fue derribado en Laos en 1971. La Operación Lan Som 719, invasión de las fuerzas sudvietnamitas contra el ejército Popular de Vietnam del Norte y el Pathet Lao, estaba siendo cubierta por los fotógrafos cuando un mar de olas lo llevó de nuevo al fondo del Océano.

http://culturacolectiva.com/wp-content/uploads/2015/06/burrows.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/larry-borrows.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/larry2.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/larry.jpg

  Eddie Adams La Guerra de Corea y la Guerra de Vietnam, fueron sucesos bélicos que serán recordados en la historia como trágicos. Las imágenes de Adams son prueba de ello; del dolor que el hombre puede causar, uno que perdura y que las fotografías nos recuerdan, aun cuando las balas se han ido. La ejecución de un guerrillero del Viet Cong por un policía de Saigón, es una imagen contundente que muestra la frialdad con la que un hombre mata a otro de los suyos, a uno de su misma raza. Presenciar tanto sufrimiento lo llevó a dejar la fotografía de guerra para dedicarse a fotografiar celebridades, pues éstas “no te arrebatan nada tuyo”.

http://culturacolectiva.com/wp-content/uploads/2015/06/eddie-adams.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/ddie-adams.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/eddie.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/eddie-adams2.jpg

 Horst Faas Fotografías fuertes y crudas. Así pueden ser descritas las imágenes de un hombre que siempre luchó por mostrar la realidad tal cual es. Su trabajo comenzó en Argelia y el Congo, aunque después se centró en Vietnam. Fue galardonado con el Pulitzer en 1965 por esta labor, y más tarde con otro por el trabajo de la Guerra de Liberación de Bangladesh en 1972. En 1967, luego de ser herido por una granada, nunca volvió a caminar aunque su trabajo y apoyo fotográfico no terminó en ese momento. En 2003, mostró su apoyo a los fotógrafos de guerra con la publicación “Lost Over Laos”, un homenaje a los fotoperiodistas asesinados en 1971 en Laos.

http://culturacolectiva.com/wp-content/uploads/2015/06/host-fass.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/war-is-hell.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/horst.jpg
http://culturacolectiva.com/wp-content/uploads/2015/06/fass.jpg

 H.S. Wong El fotoperiodista chino es el autor de “Sábado Sangriento”, una fotografía tomada durante la Segunda Guerra sino-japonesa. Sus imágenes muestran con crudeza lo duro que es la guerra, los destrozos que deja y las secuelas que trae consigo una vez que la tormenta parece haber pasado.  Wong capturó imágenes de diversos ataques japoneses en China, por lo que la Asian American Journalists Association lo nombró periodista asiático pionero en 2010. Arriesgó su vida en múltiples ocasiones para obtener la fotografía deseada, por lo que el gobierno japonés le puso precio a su cabeza. Las amenazas de muerte lo siguieron hasta China, aun con el apoyo británico por lo que tuvo que mudarse a Hong Kong. 
  Un enfrentamiento bélico es terrible y el dolor que trae consigo puede verse a través de las fotografías, pero no puede compararse con las miradas de aquellos que lo vivieron y fueron testigos del golpeteo de una furia que arrasa con lo que se encuentra. Hombres que se empaparon de dolor y que con sólo una cámara dejaron un legado al resto del mundo.