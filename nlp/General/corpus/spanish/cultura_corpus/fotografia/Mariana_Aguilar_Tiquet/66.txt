Fetichismo y bondage en las fotos de Gilles Berquet

 Gilles Berquet se graduó en pintura de la escuela de Bellas Artes de Montpellier, Francia, pero más tarde encontró su pasión en la fotografía. Sus imágenes están influenciadas por la técnica de pintura que posee.     “Yo adopté la fotografía como un artista de pintura, con la voluntad de producir imágenes del espíritu y no como reproducciones de la realidad. Además, yo no tengo la impresión de tomar fotos, más bien de fabricarlas”.     Su obra está influenciada por artistas como Irving Klaw, uno de los primeros fotógrafos fetichistas, y en John Willie, fotógrafo e ilustrador del bondage, práctica erótica que consiste en amarrar a una persona para provocar placer. Es aplicada frecuentemente en el sadomasoquismo.   Las fotografías de Berquet tienen un gran contenido sexual, en ellas se puede ver a la mujer en un escenario casi teatral; convierte la figura femenina en un fetiche. 
    Su trabajo no invita al vouyerismo porque la finalidad de su obra es mucho más compleja. En sus fotografías se puede observar un toque de erotismo obscuro; pasa por el hedonismo de la cultura pagana, y pone de manifiesto la barbarie y el salvajismo que hay en el mundo. 
  
http://culturacolectiva.com/wp-content/uploads/2014/09/Gilles-Berquet.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/poupee-gonflable-1995.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/Gillesmano.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/le-muscle-du-sommeil-1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/GillesB.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/Berquet_.jpg
  En 1994 creó la revista Maniac , en la cual plasma el arte del bondage con cierto humor.   Sus imágenes ambiguas causan en el espectador sensaciones encontradas. Las fotografías son a la vez perversas y maravillosas; es un recorrido entre el miedo y el deseo. 
  
http://culturacolectiva.com/wp-content/uploads/2014/09/bondage.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/fetiche.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/fetichismo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/berquetW.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/berquetGilles.jpg
http://culturacolectiva.com/wp-content/uploads/2014/09/Berquet.jpg