Polaroid y la provocación en la fotografía

 Con la llegada de la era digital, el papel parecía perdido. Las herramientas tecnológicas suponían un nuevo avance para la humanidad, una nueva manera de mirar, ver, leer y sobre todo de consumir. El consumo, si bien ya era masivo, se intensificó. Tener y poseer algo de manera virtual, hubiera parecido imposible o cercano a la ciencia ficción años atrás. Sin embargo, la llegada de la Internet nos demostró que las posibilidades eran múltiples y que la acumulación infinita. Muchos le dijeron adiós a las páginas de las revistas, se despidieron de los discos y de los cuadernos. Caímos en un mundo donde nuestros dedos dejaron de sentir el paso de las páginas y se acostumbraron a teclear. Un mundo en el que los aromas se perdieron junto con la tridimensionalidad y en muchos casos la calidad. Así, la fotografía dejó de ser análoga para ser reemplazada por la digital. Contar con una memoria que puede almacenar cientos o miles de fotografías facilitó el consumo de la imagen. Decidir con precisión cuándo o en qué momento se debía apretar el obturador dejó de tener relevancia. Ahora se puede accionar un centenar de veces y no importa, pues no hay rollo fotográfico que cuidar. Entonces, decidimos amar la imagen más que nunca. Nos saturamos de ella y por lo mismo dejamos de ver y percibir como antes lo hacíamos. Las famosas instantáneas de la marca Polaroid quedaron como un recuerdo, como el testimonio de que algo había sucedido y no podía borrarse con tan sólo un click. La imagen impresa era mucho más fuerte que la fría y distante que se observa a través de la pantalla. Una de la que no es posible apropiarse verdaderamente, pues no hay aroma que la distinga o letra que la marque de por vida.  En los últimos años, la nostalgia de una vida pasada marcada por el recuerdo de las fotografías nos hizo valorar su importancia. Lo que motivó a que nuevamente se sacaran al mercado algunas cámaras de fotografía instantánea, aunque éstas son más un accesorio que lo que solía ser las verdadera fotografía Polaroid. Estos son algunos de los artistas y fotógrafos que incursionaron en el mundo de la Polaroid, que dejaron un legado provocador con las fotografías instantáneas.  
Robert Mapplethorpe Mapplethorpe fue un fotógrafo nacido en 1963, conocido sobre todo por sus imágenes de desnudo  que causaron controversia. Experimentó con diversos materiales y estuvo influenciado por los artistas Joseph Cornell y Marcel Duchamp. En 1970, obtuvo su primera cámara Polaroid y un año más tarde ya tenía varias fotografías instantáneas. En 1973, la Light Gallery en Nueva York montó la exhibición “Polaroids”. A pesar de que el trabajo con Polaroid fue bastante satisfactorio para el fotógrafo, no se conoce mucho sobre estas fotografías.  http://culturacolectiva.com/wp-content/uploads/2015/08/robert-.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/ropert-mapplethorpe.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/mapplethorpe.jpg 
Andy Warhol Nacido en 1928, Andy Warhol fue un personaje clave en el arte pop. El personaje excéntrico fue un ilustrador importante que participó en el performance, instalaciones, escritura y fotografía. Tomó muchas Polaroids durante sus viajes, sobre todo a Europa, fotografías que utilizó como referentes para luego hacer retratos. http://culturacolectiva.com/wp-content/uploads/2015/08/polaroid.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/basquiat1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/andy.jpg 
Nobuyoshi Araki El trabajo del fotógrafo y artista japonés ha sido criticado por diversas personas, quienes lo han acusado de misógino por el contenido de sus fotografías. En su trabajo con Polaroid se observan imágenes que muestran el bondage. Sus fotografías resultan muy provocadoras, algo en lo que Araki es considerado un experto.

http://culturacolectiva.com/wp-content/uploads/2015/08/tokio.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/Suicide-in-Tokyo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/polaroid-nob.jpg

 
Helmut Newton El famoso fotógrafo de desnudo, cuyas imágenes resultan seductoras, fue uno de los más relevantes del siglo XX. Newton utilizó la cámara Polaroid para realizar las pruebas previas a la sesión fotográfica. Fue cuando falleció en 2004, que muchas de estas fotos fueron recopiladas en el libro para Taschen.

http://culturacolectiva.com/wp-content/uploads/2015/08/helmut-polaroid.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/helmut-newton.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/Newton-polaroid.jpg

 
Edo Bertoglio El fotógrafo y cineasta Bertoglio trabajó para Vogue, además de involucrase en la escena musical a finales de los 70 e inicios de los 80. Sus Polaroids muestran la juventud en Nueva York. En éstas se puede ver a figuras como su ex esposa Maripol, Grace Jones y Deborah Harry. New York Polaroids 1976-1989 es el libro que muestra las fotografías en Polaroid del fotógrafo suizo.  http://culturacolectiva.com/wp-content/uploads/2015/08/edo-polaroid.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/polaroids.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/edo.jpg 
Maripol La artista, estilista y diseñadora fue esposa de Bertogliio, con quien fotografió a una buena cantidad de artistas. Fue reconocida durante la década de los 80 por ser una de las mejores estilistas; trabajó para celebridades como Madonna, Grace Jones y Deborah Harry, a quien sin duda ayudó para influenciar en los estilos que las caracterizaron. También es conocida por su trabajo en Polaroid.

http://culturacolectiva.com/wp-content/uploads/2015/08/maripol.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/polaroids-maripol.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/maripola-polaroid.jpg

 
Dash Snow La obra del artista estadounidense giró en torno al ambiente neoyorquino, mismo que retrató y quedó para la posteridad con sus Polaroids. Las fotografías muestran las noches, las drogas y todo el mundo que para muchos existe sólo en la oscuridad.

http://culturacolectiva.com/wp-content/uploads/2015/08/dash1.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/dash.jpg
http://culturacolectiva.com/wp-content/uploads/2015/08/snow.jpg