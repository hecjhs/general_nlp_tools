El antes y el después de las conejitas de Playboy más iconicas de la historia

 El mundo de la fantasía, así como se llamara también un filme protagonizado por la siempre maravillosa Marilyn Monroe , está plagado de belleza y un esplendor que todo lo cubre. Y en las fantasías que incluyen mujeres de magna divinidad, que han sabido dar ambrosía a nuestra libido, estas musas del bajo instinto siempre permanecen así. Con rostro angelical, seductor, y disposición para la aventura. 
  ¿Pero qué sucede cuando la realidad es otra? Cuando aquellas columnas de lo hermoso se han cuarteado con el paso de los años. Cuando el tiempo acechante encuentra el punto más débil de la seda. Muchos hombres y mujeres a lo largo del tiempo hemos hallado reconfortante hermosura en ídolos históricos como Brigitte Bardot, Twiggy, Catherine Deneuve, Anita Ekberg, etcétera. Sin embargo, en sus últimos días dentro de la vida real, esta monumentalidad que mostraban no se mantuvo, o mantiene, inmortal. 
  Es normal y completamente comprensible el avance de la edad; pero justo eso, la normalidad no debería caber en esos seres supremos que motivan y conmueven al ojo humano. Ejemplo de esto también es el contraste de apariencias, de antaño y actual, en las mujeres que posaron como conejitas del Playboy . Esas chicas que hicieron temblar a las viejas costumbres y abrieron todas las posibilidades para el desnudo contemporáneo en las revistas para caballeros. 
 A inicios de los años 60, Hugh Hefner prácticamente creador de la desinhibición, generó un concepto de mujeres vestidas en corset, puños y cuello de camisa, orejas puntiagudas y cola de algodón: las conejitas de Playboy. Quienes atendían en los bares de la empresa y asistían a importantes fiestas. 
 Hoy lucen así: 
   Cheryl Hill-Gallucci. Hoy es decoradora de interiores. 
  
 Barbara Drumgoole. Ahora es guía turística en un museo. 
  
 Micko Nakamura. Se dedicó a la distribución de equipo para artes marciales. 
  
 Rita Plank. Ahora trabaja en una sociedad de beneficencia y promueve la adopción de perros. 
  
 Debbie Cleffie. Hoy tiene un centro de spa 24 horas.   Sandy Speier. Después de ser conejita hasta el día de hoy, se ha dedicado a la docencia. 
  
 Barbi Holstein. Fue despedida como conejita a la edad de 33, pero cuenta que el mismo Hugh le ofreció pagarle su educación y titulación para que tuviera un futuro más allá de Playboy. 
  
 Marsha Callender. Agradece las maravillosas experiencias en Playboy, principalmente el ganar confianza y audacia.   Pam Jabos. Actualmente es agente de viajes. 
 
 Fotografías de Sara Naomi Lewkowicz.*Referencia:Flavorwire 
 *** 
 Te puede interesar: 
 Playboy se despide de los desnudos “El mundo erótico de Salvador Dalí” para Playboy