La fría y desnuda juventud de la última dictadura europea en fotografías

 
 En el fondo del alma se guarda un tesoro de vivencias y emociones que fueron creadas durante la más tierna existencia. La juventud es la época del gozo, del arrebato de los sentidos y la libertad. Pero en ocasiones, alguien evita que la luz siga su curso natural hasta los corazones adolescentes. Ellos que tanto necesitan del calor para sonreír y soñar, viven en una prisión hostil y melancólica, donde el único paisaje que observan son calles desoladas y distantes que conducen a la desesperanza. 
  
 El mayor problema del ser humano es justo que depende otros y si se tiene mala suerte, en un acto violento e inhumano, el espíritu de la juventud es encadenado con oxidadas cadenas forjadas de plomo y pólvora por luchas sociales o políticas. De este dolor se alimentaron las inocentes almas que habitan Bielorrusia, el última país de Europa que sufrió los estragos de una despiadada dictadura. 
 Desde su independencia en 1991, el tiempo se ha detenido dentro su territorio. La desolación causada por vivir en constantes conflictos y asesinatos injustificados hace que sus habitantes, y sobre todo los jóvenes, vivan suspendidos de la realidad. Con esta imposibilidad de movimiento y teniendo su piel como la única prenda, sus cuerpos son colmados por el frío. 
  
  
  
 La fotógrafa Masha Svyatogor, siendo una de las afectadas por los problemas del Estado, sabe que la juventud es la más perjudicada. Su obra ofrece una rara visión de su vida cotidiana, de su lucha por entender su lugar en esta extrema existencia . 
 Para ella, el proyecto “My Poor Little Room of Imagination” es la forma perfecta para comprender y ponerse de acuerdo con su entorno circundante. “Solía ​​estar interesado sólo en mi mundo interior, en mi melancolía. Pero ahora estoy abriendo los ojos al mundo que me rodea, es decir, Bielorrusia con su estética absurda”. 
  
  
  
 Sus fotografías retratan un territorio íntimo donde jóvenes pobres y vulnerable se encuentran desorientados, preocupados y confundidos de una realidad que no eligieron y aún no logran entender . 
  
  
  
 “Podría ser muy difícil vivir aquí. Muchas veces es insoportable, confuso, incomprensible, divertido y devastador. Cuanto más se sufre y se queja, las energías se van agotando, pero se tiene que hacer lo que sea necesario para vivir de la manera que se quiere”. 
  
  
 Haber nacido en este inhabitable para el espíritu de la juventud, provoca frustraciones que serán difícilmente tratadas. A los jóvenes que decidan quedarse en Bielorrusia sólo les queda permaneces en una noche perpetua a causa de una dictadura absurda, sin poder decir palabra alguna que sea escuchada fuera de esos fríos y alejados muros. 
 Al igual que muchos jóvenes bielorrusos, Masha emigró a otros países en busca de tranquilidad. Su distanciamiento le permitió a reflexionar sobre su país y comprender mejor los fenómenos sociales que golpean al espíritu adolescente de Bielorrusia. 
  
 – Masha Svyatogor comenta que todas sus fotos son tomadas en la región de Minsk, dentro de las casas de los modelos que conoció tras compartir la las bases del proyecto en las redes sociales. Para su sorpresa, recibió cientos de llamados de personas que encajaban perfectamente con el alma del proyecto y sintió aún más intenso el frío que dejó en los jóvenes la dictadura bielorrusa, la gelidez de su alma, pero ¿sabes cómo se ve la incertidumbre en una fotografía? 
 *** Te puede interesar: 
 El fotógrafo que te hará sentir esquizofrenia a través de su arte Inseguridad y violencia puestas al descubierto a través de la fotografía