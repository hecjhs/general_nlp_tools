El verdadero food porn

 Encontrar una persona que salive al pensar en un brócoli, es como buscar una aguja en un pajar. Desde pequeños, por alguna razón odiamos los vegetales. Comer un platillo que los contuviera, representaba una batalla segura contra nuestra madre; ella nos decía que debíamos comerlos porque eran muy saludables, pero se veían tan verdes, tan crudos y tan “sin chiste” que era imposible que nos gustaran, no era nuestra culpa.  Tal vez, por ese desprecio hacia los vegetales que nació cuando éramos pequeños, repudiamos todo aquello que se venda bajo el nombre de “saludable” y al contrario, tenemos un loco y descontrolado amor por todo lo que esté repleto de queso, chocolate, grasa y dulce. Sin embargo, conforme crecemos nos damos cuenta que en efecto, comer vegetales es imprescindible para tener una buena salud, y es así como poco a poco le encontramos el gusto. De pronto nuestras comidas se vuelven verdes y balanceadas y nuestra calidad de vida mejora, pero también notamos que nada de eso igualará el placer de comer un buen trozo de chocolate.  Si nuestra salud no se viera tan afectada al consumir estos deliciosos placeres de la vida, seguramente nuestros alimentos diarios serían una extraña combinación entre “comida saludable” y “comida chatarra”.   “Jelly bean Sandwich”    Lizzie Darden es una estudiante de diseño de Jacksonville, Florida que ama el queso y el juego de palabras. Lizzie juega con la famosa frase que nos decía nuestra mamá cuando éramos pequeños: “Come tus vegetales” y le otorga un nuevo significado. Sustituye las verduras y los alimentos que algunos odian y consideran “aburridos” por dulces, helado, chispas de colores, queso y crema para batir.  “Jelly bean burrito”     Sin duda, Lizzie trajo a la vida la perfecta combinación de comida que antes sólo había existido en nuestros sueños e imaginación. Comer tus vegetales no tiene porque ser una molestia. Aquí una selección de su trabajo. 

 “Candy Corn”  “Cheese cake” “Spaghetti & gumballs”  


 

 

 

 

 

 

 

 









Si te gustó el trabajo de Lizzie Darden puedes seguirla en su Behance e Instagram.


***  

 Te puede interesar: 20 recetas hechas con los 20 alimentos más saludables