Las perversiones y contradicciones de una mujer al desnudo

 
 Siénteme cerca, muy de cerca , porque sólo así se develan las pasiones, las perversiones y aquello que te hace conocer verdaderamente a alguien. 
  
 De ese modo aprendí a disfrutar mi cuerpo y mi soledad. Me di cuenta de que no necesitaba a alguien que me provocara, un compañero que me excitara o el deseo de verme en los ojos de otra persona. Simplemente una buena botella de vodka y una cámara que siempre me acompañaran… Lo sé, suena estúpido, pero cuando lo hice me percaté de que es uno de los ejercicios más difíciles de todos. 
  
 Ahora no necesito otra cosa, ni siquiera la ropa que cubra mis imperfecciones, porque en esos momentos, sólo puedo sentirme y vivirme tal y como soy, sin ninguna otra complicación más que la aventura de descubrir la peca que nunca había visto. Esos pezones rígidos que se excitan ante la menor turbulencia como con el frío del mármol duro. El roce de una alfombra aterciopelada que sólo hace encender el deseo y la imagen caótica que muestro sin saber cuando me doy cuenta de que todo está perdido: que mis anhelos y pasiones han sobrepasado la razón. 
  
  
 No hay víctimas, de hecho, no hay nadie más en esa habitación entre sombras, pero eso está bien, porque a veces, la mayoría de los días, las delicias de la soltería me demuestran que es lo mejor, nos hacen volvernos locas, sin pudor, sin rendirle cuentas a nadie ni solapar el deseo ajeno. Sólo soy yo. La que disfruto, la que gozo con mi cuerpo con verme tan alejada de todo y al mismo tiempo, ser la más sensual del mundo. 
  
  
 La libertad es perfecta y la cámara sólo está a la expectativa. Un ojo diferente me mira, pero las imágenes que capta las califico yo, esa que está recostada sobre el helado mosaico que me provoca cosquilleos como ningún otro sitio, la que hace el rostro más sensual y sutil para nadie más que el ojo ajeno. 
  
  
 Miro la cámara como si se tratara de ese amante que me provocó un gran orgasmo. Abro mis piernas y la dejo entrar. Pero no todo se trata de placer, en esos momentos de intimidad también me convierto en una persona fría y solitaria. El desierto que yace en esa alcoba me invade, me acecha, me carcome y quiero dejarlo de una vez por todas. 
  
  
 Se convierte en un golpe bajo, me dobla y sólo puedo sentir una profunda agonía. Quiero mandarlo todo al carajo, dejar del lado mis responsabilidades para desaparecer del mundo por unas horas, pero me es imposible, me rompe ser tan humanamente frágil y caótica, tan poco firme, sin los pantalones suficientes como para demostrarle al mundo exterior que puedo superar la ausencia y la pérdida.  
  
 Sí, una contradicción total con las delicias de estar sola y descubrir el placer en una botella de vodka y una reflex, pero así es la condición femenina. Somos el ejemplo perfecto entre lo que queremos, tenemos y anhelamos. No sabemos el punto exacto en el que todo se transforma, pero un día podemos sentir placer desmedido por nuestra condición de soledad y al otro simplemente doblarnos en posición fetal para lamentarnos por ella. Cada tumbo me pega con más estruendo porque soy mi peor crítico, la más cruel, tirana y desastrosa. 
  
 De algún modo, el autorretrato invadió el arte. Probablemente porque a través de él, aquel que lo hace puede hacer un análisis introspectivo verdaderamente profundo. Implica conocerse, pero sobre todo, ser capaz de representarse tal y como ese que lo hace desea ser visto a los ojos del otro. 
  
 Pensar en narcisimo caería en la superficialidad latente en la que pensamos cuando, por ejemplo, mencionamos las selfies o esas fotos sin contenido estético, reflexivo o interpretativo. Pero el autorretrato es mucho más que sólo eso: implica escrutarse, conocerse y saber hablar a través de una imagen. 
  
 Sentirse a través de una imagen, vibrar al ver el resultado, reflejar ese momento único en el que solamente tú sabes por lo que estás pasando. No se trata de egocentrismo, sino un trabajo que implica sacar el alma para mostrarla de manera impresa. 
  
 Con sus fotografías, Jasmin Meyer ha logrado cambiar el estatus de simpleza para mostrarnos un paso más profundo. Uno que involucre caricias, nostalgia y hasta hablar sobre las drogas que invaden su cuerpo. En sus fotos sólo se trata de ella, de la línea que nos quiere mostrar para que veamos su sutil fragilidad, en los ojos penetrantes que desatan el más profundo deseo o ese acercamiento que nos muestra un doble sentido a lo que vemos. 
  
 Esta fotógrafa alemana mantiene activas sus fotografías cada vez menos recurrentes: “No he sido muy activa en los últimos años, pero continúo con el placer de descubrirme a través de la fotografía; sin embargo, he estado muy ocupada. No dejo de fotografiarme, pero cada vez soy más vieja y tengo que encontrar más de mí, aprender a amarme y apreciarme”.  
                    La intimidad de una fotografía puede ser el pretexto ideal para captar aquello que más amamos y, de alguna manera, presumirlo al resto del mundo… tal y como lo hizo el proyecto fotográfico “The Ones We Love”. 
 *** 
 Te puede interesar: 
 Las sutiles y descaradas fotografías de una mujer desnudaEl fotógrafo erótico que captura la sensualidad de las mujeres rusas