Retratos del hombre vestido de mujer del que me enamoré

 
 Yo me enamoré . A pesar de los prejuicios, del qué dirán, de lo que todos los que estaban alrededor pensaban que estábamos  haciendo mal, sentía un golpeteo en mi corazón que parecía desembocar en el deseo de algo que no podía entender. Un día, mientras viajábamos en el subterráneo, se propuso contarme sus hazañas de la infancia. Él se vestía como mujer todo el tiempo. Sí, se vestía como mujer y lo disfrutaba mucho más que cuando tenía que usar el distintivo patrón masculino de pantalón y camisa, con zapatos de agujetas y calcetines de rombo. 
  
 En ese momento, simplemente fue una confesión entre amigos, el secreto de su pasado que le daba miedo mostrar a los demás, aquello que probablemente nos uniría para el resto de nuestra vida. Me dijo que en la universidad siempre vestía con ropa de mujer, pero después de la graduación comenzó a experimentar al mundo, a vivir las consecuencias de no ser igual que el resto, a llenarse de prejuicios que cubrían a los demás y trataban de ocultar su sexualidad. 
  
  
 No supe qué decirle, impávida, sabía que debía hacer algo para remediar el terrible aislamiento que él sentía del mundo. Quise mostrarle algo más que no fuera simplemente la idea de no poder hacer nada por él. Tomé mi cámara y llenándome de valor, ocultando los prejuicios que también cubrían mi mente, le pedí que posara para mí tal y como deseara. 
  
  
 Él se mostraba reacio. Supuse que nunca antes se había desnudado, vestido de mujer al lado de alguien más. Me mantuve a la expectativa, siempre contemplándolo desde atrás, de ese punto ciego que él no alcanzaba a observar y que me llenaba de satisfacción cuando veía que por fin no se reprimía con tanto recelo. 
  
  
 Con la primera toma mi piel se erizó. Nunca antes me había percatado de lo hermoso que era, del valor que debía tener y lo afortunada que era por tenerlo delante de mí. Conecté con su vulnerabilidad y de algún modo nos convertimos en dos seres peleando contra el mundo. No quería que se sintiera comprometido con las tomas, con los ángulos que le tomaba pero, al parecer, él sabía exactamente lo que quería. 
  
 Una noche peleamos como nunca. Pensé que no regresaría, que este proyecto no tendría fin. Nos gritamos, yo aventé un florero que absurdamente era de plástico y sólo me ridiculizó aún más, pero fue ese momento en el que todo cambió. Yo reí, él rió y los dos acordamos qué debíamos hacer para solucionar el malentendido. Era la una de la mañana pero ambos decidimos seguir con la sesión. 
  
 Los dos supimos cómo movernos de un lado a otro. Nos sentimos libres, como nunca antes, por primera vez. Yo sabía cómo hacerlo sentir cómodo; él, hacerme reír. Cada toma fue primero un juego de miradas y sin quererlo, de seducción. Los dos sentíamos deseos de vernos, de explorarnos y mi identidad, sin quererlo, comenzó a emerger. 
  
 El tiempo pasaba y de pronto, comencé a derretirme por él. No sabía qué ocurría en mi interior y cómo de la manera más absurda, experimentaba algo que parecía fuera de cualquier plan, de esas normas convencionales que no marcaban algo similar como posible. Sin conocer lo que ocurría por su mente, él me besó. Yo desvanecí en sueños y aspectos melancólicos que sólo una persona enamorada podría entender. 
  
 Todo lo idealizaba, los dos nos convertimos en el secreto a voces del otro. Comencé a hacer fotografías sin pena ni vergüenza. Me sentía con el poder de ordenarle, de capturar sus partes más íntimas, de hacer lo que fuera con tal de que esa toma me sedujera cuando la recordara. 
  
 Nuestra relación se hizo cada vez más real, nos convertimos en una pareja como ninguna otra, una pareja sin límites de ningún tipo. Ambos tuvimos la certeza de que, por extraño que pareciera, nos habíamos convertido en la persona ideal para el otro. 
  
 A través del lenguaje visual hicimos profundas narrativas sobre género, deseo, libertad y los tabúes culturales que embargaban a nuestra sociedad. Él se transformaba en cada toma para actuar en cada fotografía y de algún modo, fusionamos la identidad creativa con la imagen. Cada toma, cada imagen, nos hizo experimentar nuestro mundo de maneras diferentes y complejas para hacerlas públicas. 
  
 Mi nombre es Lissa Rivera y trabajo en Nueva York. Mis trabajos yacen en importantes publicaciones como la exhibición del National Museum of Woman in Arts, algunas más son parte de Humble Arts Foundation’s Collector’s Guide to Emerging Art Photography. Fui la primera en recibir reconocimiento por documentar los interiores de las instituciones educativas preparatorias con sus fraternidades y hermandades. 
  
 Me fascinan las historias que existen detrás de las fotografías. Él se convirtió en mi musa para el proyecto “Beautiful boy” y así lo honro con cada una de sus fotografías.            
 La comunidad trans es una de las más discriminadas, los artistas comunes no se preocupan por este tema, pero aquellos que viven de cerca la violencia y problemas de identidad que afectan a la comunidad, exploran los problemas que enfrentan, sobre todo en el mundo del arte. A diferencia del arte queer, en el que se profesa una vida sin etiquetas ni restricciones de género, en el arte trans se busca hacer una crítica de la falsa normalidad que sufren aquellos miembros de la comunidad transexual o andrógina. Puedes ver el trabajo de 15 artistas aquí. 
 *** Te puede interesar: 
 El porno que no es nuestro. Imágenes del erotismo que no imaginasArte queer: transgresión de normas a través del cuerpo  
 
 *Esta es una ficción que se basa en las declaraciones de Lissa Rivera y su relación con BJ, el hombre trasvesti del que se enamoró y a quien fotografió con anhelo.