Alimentos y bebidas que inspiraron a los grandes escritores

 En un Macondo “olvidado por los pájaros, donde el polvo y el calor se habían hecho tan tenaces que costaba trabajo respirar”, la única pareja feliz era la quinta generación Buendía: Aureliano Babilonia y Amaranta Úrsula. 
 “Se entregaron a la idolatría de sus cuerpos, al descubrir que los tedios del amor tenían posibilidades inexpoloradas, mucho más ricas que las del deseo. Mientras él amasaba con claras de huevo los senos eréctiles de Amaranta Úrsula, o suavizaba con manteca de coco sus muslos elásticos y su vientre aduraznado, ella jugaba a las muñecas con portentosa criatura de Aureliano, y le pintaba ojos de payaso con carmín de labios y bigotes de turco con carboncillo de las cejas, y le ponía corbatinez de organza y sombreritos de papel plateado. Una noche se embadurnaron de pies a cabeza con melocotones de almíbar, se lamieron como perros y se amaron como locos en el piso del corredor, y fueron despertados por un torrente de hormigas carniceras que se disponían a devorarlos vivos”. 
 En este jugueteo de comida, deseo y amor en Cien años de soledad,  Gabriel García Márquez plasma su devoción a la comida. Su vasta obra cuenta con múltiples referencias a los alimentos, mismos que en ocasiones se vuelven coprotagonistas de sus grandes historias. Las berenjenas, la calabaza, los tomates, los pimientos, el chocolate y el café reciben sus minutos de fama al centro de las tramas latinoamericanas de Gabo. 
  
 Además de la berenjena, la guayaba formó parte importante de la obra y vida del colombiano. Tras exiliarse de Colombia en 1981, Gabo escribió en su columna en El País, “ Así las cosas, con el dolor de mi alma, me he visto precisados a seguir apacentando, quién sabe por cuánto tiempo más, mi persistente y dolorosa nostalgia del olor de la guayaba”. Esa última línea inspiraría el título para un libro de Gabo, entrevista con Plinio Apuleyo Mendoza. 
 Así como Gabo rindió homenaje a la comida, a la que consideró aún mejor que el amor pues alimenta, la gastronomía y la bebida siempre han sido parte esencial del mundo de la literatura. Diversos alimentos han sido centrales en la vida de múltiples escritores, llegando incluso a ser parte de peculiares rituales para alcanzar la inspiración, relajarse o simplemente mantener el ritmo en el acomodo de palabras. Te compartimos algunos de los alimentos y bebidas que determinaron la vida de algunas de las mejores plumas de la literatura universal: 
 Agatha Christie – una verdadera amante de las manzanas, pues gustaba de comerlas mientras tomaba largos baños en tina. 
 Franz Kafka – además de mantener una dieta vegetariana, Kafka tomaba un vaso de leche mientras escribía. 
 Lord Byron – la figura del movimiento romántico gustaba de engañar a su estómago, haciéndolo creer que ya había comido, solía ingerir cucharadas de vinagre. 
 Walt Whitman – el enlace entre el trascendentalismo al realismo, tenía una particular debilidad por comer una generosa cantidad de carne y ostiones durante el desayuno. 
  
 John Steinbeck – el ganador del Premio Nobel en 1926, tomaba una taza de café frío al final del día. 
 Charles Dickens – emblema de la época victoriana, Dickens tenía una debilidad por las manzanas cocidas, a las cuales incluso les adjudicaba múltiples beneficios para la salud. 
 Jean-Paul Sartre – el filósofo francés tenía un poderoso gusto por la haiva, un dulce basado en pasta de sémola, y popular en la India y Pakistán. 
 H.P. Lovecraft – el genio del terror se declaró fanático del spaghetti italiano, especialmente aquel acompañado con salsa de tomate y carne, cubierto con una capa gratinada de queso parmesano. 
  
 Molière – durante una de sus enfermedades en 1667, se sabe que tuvo que apegarse a una dieta exclusiva de leche durante dos meses. 
 Honoré de Balzac – quizás pienses dos veces antes de decir que te gusta el café, pues el autor de La comedia humana consumía 50 tazas de café al día, y en ocasiones comía granos de café. Amante de la puntualidad, siempre cenaba a las 6 de la tarde en punto. 
 Jack Kerouac – uno de los tres íconos de la Generación Beat no se molestó en ocultar su gusto por la comida china, especialmente por aquellos platillos con cerdo.  
  
 Victor Hugo – cuando vivía exiliado en las Islas del Canal, el genio detrás de Los Miserables se despertaba en la madrugada para comer dos huevos crudos, una taza de café frío antes de comenzar a escribir. 
 Truman Capote – en aras de encontrar la inspiración, el estadounidense siempre escribía mientras fumaba y tomaba café negro. Por la tarde, sustituía el café con alguna bebida alcohólica. 
 Kurt Vonnegut – el humorista estadounidense y veterano de guerra, bebía a diario, y en punto de las 5:30 de la tarde se servía un vaso de whiskey con agua. 
 *** 
 Referencias: Animal Gourmet , The Awl