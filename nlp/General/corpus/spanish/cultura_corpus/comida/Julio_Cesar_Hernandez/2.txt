Las papas a la francesa y otras mentiras gastronómicas

 Alrededor del mundo existen varios platillos que parecen ser de lo más tradicional, y que algunas veces hasta se vanaglorian de ser únicos e irrepetibles. La gastronomía es donde la tradición y los productos de la región se conjuntan para así crear conceptos sobre lo que es el país en cuestión. Suele ser lugar común que al viajero le digan “a donde fueres haz lo que vieres”, y si el comer es hacer, probar los distintos platillos y preparaciones que un lugar ofrece es sin duda parte importante de la experiencia. 
 Debemos recordar que toda tradición es un constante movimiento en perpetuo; si bien el mole mexicano es un platillo típico de nuestro país, eso no lo exime de haber sufrido modificaciones a lo largo del tiempo por la comunicación de boca en boca – o en su caso en recetarios -, con lo cual el concepto se ha modificado. La receta del primer mole no será exactamente igual a uno que encuentras en una fonda de la ciudad, y mucho menos a las distintas versiones que han venido desarrollándose en todo el territorio. Partiendo de este ejemplo, te presentamos una serie de platillos cuyo origen es distinto al que marca la “tradición”: 
 ¿Las papas a la francesa, son una tradición nacida en Francia? 
  
 Quien crea que las papas a la francesa provienen de Francia, está totalmente equivocado. En las anécdotas de la modernidad, se cuenta que el nombre proviene de un error. Los soldados que aterrizaron en el occidente de Flandes, Bélgica durante la Segunda Guerra Mundial las conocieron allí por primera vez, y las llamaron así porque en esa parte del país se habla francés. De hecho dicen que las papas a la francesa en Bélgica saben mucho mejor. 
 – 
 ¿La pasta proviene de Italia? 
  
 La pasta es uno de los alimentos más antiguos de los que se tiene registro, debemos recordar que gracias al asentamiento de los pueblos nómadas se desarrollaron los cultivos como el del trigo. Exactamente no se sabe bien de dónde proviene ya que se tienen datos que apuntan a que en China y en el Mundo Árabe se pudo haber desarrollado la técnica de hacer pasta de forma simultánea. Asimismo existe la versión sobre la llegada de la pasta a Europa gracias a los viajes de Marco Polo. 
 – 
 ¿Cuál es el origen del Chili  con carne? 
  
 Al igual que otros platillos “mexicanos” famosos en el mundo, el origen se remonta a siglos atrás. Según Claudia Prieto, investigadora de la Universidad del Claustro de Sor Juana, no es tan erróneo pensar que el origen se remita a civilizaciones precolombinas ya que el concepto básico del platillo es la carne preparada con chile. Incluso llega a citar Historia de las Indias de Fray Bartolomé de las Casas en el cual afirma “era popular entre los mexicas y que incluso llegaron a prepararlo con carne de conquistadores”. 
 Avanzando en la Historia se sabe que el platillo se comenzó a popularizar gracias a las Chili Queens a finales del siglo XIX en el sur de los Estados Unidos, y que era considerada una comida para gente pobre. Las Chili Queens se dedicaban a vender carne con chile en la calles de San Antonio Texas con el fin de sobrevivir. Por lo anterior se catalogaba como una comida marginal. A este intercambio cultural es lo que finalmente se le denominó: cultura Tex-Mex. 
 En la actualidad, este platillo tiene muchas variaciones y es bastante popular, es un ejemplo claro que los orígenes y nacionalidades de la comida son más complejos que sólo decir “Esto es de gringos…” 
 – 
 ¿El Burrito es gringo? 
 El Burrito es un platillo que también suele confundir a las personas sobre su origen, pero al contrario de lo que muchos creen, el burrito con tortilla de harina sí se considera comida mexicana , ya que la historia cuenta que fue Ciudad Juárez, Chihuahua la cuna de este plato. Por la cercanía con la frontera se popularizó en Estados Unidos. 
  
 – 
 ¿De dónde proviene el Chop Suey? 
 Para finalizar, nos remitiremos a una preparación que se ha hecho icónica: El Chop Suey. Hecho a base de noodles, y que se traduce como “trozos mezclados”;   no se considera en definitiva un platillo chino. Los orígenes se remiten directamente a Estados Unidos donde a mediados del siglo XIX y la construcción del Ferrocarril Transcontinental.  Muchos chinos fueron la mano de obra en la construcción del ferrocarril y se cree que al no tener acceso a comidas preparadas, comenzaron a mezclar sobras de carne con vegetales imprimiéndole el sello tradicional. Al igual que el Chili con carne, es uno de los platillos más solicitados en E.U.A. 
  
 Resulta casi imposible que un platillo pueda considerarse “autóctono” o “nativo” de un país en específico pues el tiempo, el intercambio cultural y factores geográficos han generado mezclas valiosas para la humanidad. 
 *** 
 Te puede interesar: 10 tipos de tacos mexicano que sólo un conocedor ha probado.