10 cosas que debes saber sobre la fotografía de comida antes de criticarla

 
 ¿Cuál es el sentido de fotografía comida? ¿Qué pasa por la mente de aquellas personas que antes de agarrar la cuchara y aspirar un condimentado aroma de clavo y tomillo toman su celular y le sacan una foto? Lo peor es que la imagen carece de una estética verdadera, la única intervención que se tiene es el de un precario filtro que distorsionan el color de los ingredientes. Los alimentos se comen, no se fotografían. ¿Nos damos cuenta de que con este acto se deshonra una historia culinaria que se nutre de grandes descubrimientos, errores y muchas muertes? 
 La historia de las especias, aquellos ingredientes que transforman el platillo para bien o para mal, se remonta hasta los primeros años de las Tierras Santas. Los musulmanes y los asiáticos la utilizaban desde la antigüedad; debido a sus variedades de colores y olores podían utilizarse para teñir prendas y hacer perfumes . El occidente sólo conocía un número reducido de estas plantas aromáticas, así que que en la Edad Media se convirtió en un objeto tan preciado como la plata. 
   
 En ese entonces la única forma de transportar grandes cargamentos de especias era a través de barcos. Los musulmanes recibían un pago por dejar transitar embarcaciones extranjeras por sus tierras. los compradores que llegaban de España, Francia o Inglaterra después de un tiempo comenzaron a sentirse estafados por el alto impuesto y desataron una guerra en medio del Mar Mediterráneo. Hubo miles de muertes y cargamentos perdidos. En definitiva esa no era la opción. 
 Por eso empezaron a buscar rutas alternativas por Oriente. Durante la exploración de nuevos caminos, un joven navegante confundió la brújula y se perdió en mar adentro. Mucho tiempo después sus barcos por fin vieron tierra y desembocaron para cargarse de especias. Su pequeño extravío terminó en el descubrimiento de América. Él era Cristobal Colón, un cartógrafo que tenía la tarea de encontrar nuevas maneras de llegar a la India. Por eso cuando llegó a nuestras tierras a los habitantes les decía “indios”, erróneamente. 
  
 Lo que comenzó como un viaje por especias se convirtió en una revolución culinaria. No encontraron la India pero descubrieron un enorme listado de nuevos ingredientes. Tomar la comida a la ligera es transgredir con su historia y su importancia en la vida del hombre. Quien sabe muy bien esto es el fotógrafo Andrew Scrivani. Él retrata el alma de cada jitomate, cebolla o pedazo de carne. Como actualmente existe una moda por retratar cada uno de los platillos que se nos pone enfrente, lo mejor es conocer lo que piensa un experto al respecto. 
 – Fotografiar alimentos no es fácil 
  
 Fotografiar alimentos es más difícil que retratar personas. La comida no tiene vida, pero en sus fotografías es esencial hacer que parezcan tan sonrientes como un niño. Convencer al otro que la verdura que se tiene enfrente tiene un candor humano no es tarea fácil. No sólo es dar un click, hay que tomar el papel de un dios y reanimar al objeto que se tiene enfrente. 
 – Toda la comida puede ser fotografiada si se tiene respeto 
  
 Tenerle respeto a un alimento es conocer su alma. Conocer con qué otros ingredientes puede mezclarse para reanimar su imagen; sentir si le hace falta un poco de fuego sobre su piel o una gota de agua para emular la frescura. El fotógrafo de comida sabe cuál es el mejor ángulo de la verdura, fruta o pedazo de carne. 
 – 
 Tomar una fotografía de tu comida no te hace fotógrafo 
  
 Tener una cámara en las manos es una responsabilidad con el arte. Cualquier objeto puede ser fotografiado, pero no todos pueden ser considerados una gran fotografía. Al momento de disparar hay que tener en cuenta los colores, formas y tamaño. Usar un filtro es el peor acto que se puede hacer. 
 – Tomar fotografías con un celular es sólo una moda 
  
 Si creías que las 100 fotos que tienes en tu Instagram son una obra de arte, estás en un grave error. Por naturaleza la cámara de un celular no puede compararse con la calidad de una cámara profesional. El retratar tu platillo dentro de un lujoso restaurante sólo te hace partícipe de la moda. 
 – La fotografía de alimentos es como la arquitectura 
  
 Cuando se tienen varios objetos en un mismo plano hay que respetar la estructura de sus formas. No son figuras planas, tiene una tridimensionalidad que debe verse en la imagen. Los verdaderos fotógrafos de alimentos lo saben y lo respetan. 
 – No se debe desperdiciar la comida 
  
 Ningún trabajo fotográfico debe terminar en la basura. Para que el platillo sea reluciente tiene que prepararse con esmero. Desechar la perfección sería algo absurdo. Por eso tampoco se tiene que usar decoración extra. 
 – No se toman fotos cuando se tiene hambre 
  
 Es tan importante el profesionalismo en este trabajo que si el fotógrafo tiene un poco de apetito la sesión fotográfica no se puede continuar. Cuando se tiene el apetito abierto la objetividad se pierde y el ojo crítico y creativo se confunde o se bloquea. Además, nunca se le debe perder el respeto a los alimentos y comer mientras se labora. 
 – Todo se basa en la composición 
  
 Al igual que con otros objetos y escenarios, la fotografía de alimentos es realizada bajo los principios de la composición de imagen. Todo lo que sale a cuadro tiene un fin, nada es fortuito o improvisado. Para lograrlo tiene que haber un equilibrio entre la teoría del color y la proxémica. 
 – La luz es más importante que la comida 
  
 Caravaggio fue el padre del tenebrismo, él demostró que la luz y las sombras sirven para acentuar alguna característica del objeto o imprimirle un toque dramático a la escena. Al igual que el pintor barroco, los nuevos fotógrafos de comida tienen que valerse de los claroscuros para realzar la figura de la comida. 
 – La comida a veces no es lo que parece 
  Si quieres conocer las mejores recetas para cocinar carne debes dar click aquí . 
 Por naturaleza algunos ingredientes pueden tener un color opaco que empeora la belleza de las fotografías. En ese caso, hay que intervenir de alguna manera con los alimentos. Con un simple retoque natural o artificial el error se corrige. Lo mejor es tratar de dejar intacto los platillo, pero a veces es inevitable. 
 – Andrew Scrivani es un fotógrafo profesional que vive en Nueva York. Se especializa en temas como comida y lifestyle . A lo largo de su trayectoria ha trabajado para The New York Times, Conde Nast, Disney y muchas otras compañías de gran prestigio. Él sabe todo lo necesario sobre los alimentos y éstas fueron algunas de sus palabras con respecto a la moda de subir a Instagram fotografías de alimentos con filtro. 
 Después de ver un trabajo profesional donde cada alimento parece bajado del mismo paraíso, es probable que tengas ganas de comer. En ese caso, aquí hay 9  recetas francesas para preparar en 20 minutos . Si quieres algo para el postre puedes ir al siguiente enlace y conocer los alimentos que saben mucho mejor cubiertos de chocolate. Por último, para tener el dato curioso del día, descubre cuáles son las definiciones sexuales más buscadas en Google por los mexicanos dando click aquí .