Cómo tomar vino barato sin quedar mal con tu pareja

 
 Existe una antigua sentencia latina que sirvió como pilar de toda una nación: In vino veritas, in aqua sanitas,  que significa “En el vino está la verdad, en el agua la salud”. Algunos historiadores indagaron el porqué de esta frase y encontraron que algunos pueblos tenían la costumbre de beber durante las reuniones de los consejos, pues consideraban que en estado de ebriedad, nadie podía mentir. Así, la política y las decisiones importantes eran realizadas bajo el efecto de esta bebida.   
 Así de importante era el vino en el pasado, y por supuesto, en el presente. Esta bebida es símbolo de elegancia, de categoría y conocimiento. Quien sabe sobre el significado de la vida entiende la importancia de beber de una copa. El único inconveniente, porque no todo puede ser perfecto, es el precio de una buena botella. Para ejemplificar, se puede tomar del vino Romanée-Conit, un vino modesto con fecha del 2005, el cual tiene un valor aproximado de 3,283 euros. 
 Con ese dinero podrías comprarte un auto usado que esté en condiciones óptimas para — en promedio — usarlo 10 años. ¿Gastarías un año de una década en una sola botella? La respuesta es obvia, pero así de caros pueden llegar a estar los vinos. Entonces, la respuesta es comprar una bebida que no supere los 150 pesos y atenerte a un sabor insípido, parco y ácido. En definitiva no. 
  
 Lo mejor es encontrar un equilibrio entre calidad y precio para que en los momentos especiales de descorche te veas como todo un conocedor . Es aquí donde entra un sencillo método que realzará, en medida de lo posible, las cualidades de cualquier vino. Este acto se llama hipercantado y fue descubierto por Nathan Myhrvold. Este conocedor de la cocina sugiere poner el vino en una licuadora a la potencia máxima durante 45 segundo para que el vino se oxigene y mejore su sabor. 
  
 Puede parecer una locura, pero este procedimiento obligará al vino a que “respire”. Puedes aplicar este método cuando quieras impresionar a tu pareja o invitados con un sabor exquisito y de buen gusto. No con ello tienes que olvidarte de querer ir por más y dejes de lado ahorrar dinero para darle un gusto incomparable a tu paladar. Así que ponte a ahorrar y mientras tanto, hiperdecanta tu vino. 
   
 Sólo un detalle más sobre este excéntrico procedimiento. Decantar no es lo mismo que oxigenar. Decantar consiste en separar el vino de sus sedimentos. Si tienes un vino con bastantes sedimentos por el paso de los años, debes vertirlo cuidadosamente en el decantador, procurando que la mayor parte de la “borra” quede en la botella. Las formas de la botella y el decantador serán los aliados para evitar que estos posos (así se los puede llamar también) nos amarguen el vino. En conclusión, este acto en sí no es “hiperdecantado”, sino “hiperoxigenado”. 
  
 – Si eres un amante del vino, esta pequeña guía debe ser complementada con otros grandes secretos que sólo un gran conocedor sabe . 
 *** Te puede interesar: 
 20 errores garrafales al tomar vino 
 10 beneficios del vino tinto