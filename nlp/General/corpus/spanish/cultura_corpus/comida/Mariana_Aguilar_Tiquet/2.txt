10 platillos mexicanos que debes probar en septiembre

 Pensar que la historia de un país se encuentra sólo en los monumentos, vestigios y edificaciones es un error. Una nación o región se puede conocer a través de los sentidos. La imagen, la textura, el ruido, los olores y los sabores hablan de un país; de su presente y por lo tanto de su pasado. 
 México es un país de tradiciones que se destaca por hermosas playas que atraen cada año a miles de turistas; pero cuando se trata de sabores y de mantener contento a nuestro paladar, la cocina mexicana se convierte en una de las más relevantes del mundo. Además cuenta con una gran variedad de platillos, muchos de ellos originados antes de la Conquista. 
 Gran parte de la riqueza de México radica en lo que se sirve sobre la mesa. Los colores, texturas y olores se vuelven fundamentales al momento de cocinar o degustar un platillo típico. 
 Chiles en nogada 
  
 Este es uno de los platillos más representativos de la gastronomía mexicana. Proviene de Puebla y se dice que fue creado por las monjas agustinas para honrar a Agustín de Iturbide. Además, los colores simbolizan la bandera de México. El chile poblano se rellena de un guisado de frutas y picadillo, luego se cubre con crema de nuez, perejil y granada. Es considerado uno de los platillos más finos de la cocina mexicana tanto por la presentación, proceso de elaboración e ingredientes. Como se trata de un platillo de temporada, además del complicado proceso de elaboración, es en los meses de agosto y septiembre que hay que aprovechar para probarlo; ideal en el mes patrio. 
 Pozole 
 Este platillo, típico para cenar en el día que se celebra la Independencia de México, proviene de épocas prehispánicas. En aquella época se utilizaba carne de tepezcuintle, una especie de roedor. Actualmente el tepezcuintle ha sido sustituido por carne de pollo o cerdo. El pozole está hecho a base del grano de maíz conocido como cacahuazintle. Dependiendo la región del territorio mexicano, existen el pozole tanto blanco como rojo. 
 Enchiladas  
 Es uno de los platillos favoritos de la cocina mexicana. La tortilla se rellena de pollo, res o queso y luego se baña de una salsa que puede ser verde, roja o de mole; así como las cubiertas de frijol, mejor conocidas como enfrijoladas. 
 Pambazo 
  El nombre de este platillo viene del nombre del pan que era utilizado para su elaboración: “pan basso” o pan bajo virreinal. Si pruebas este plato tradicional en el bajío, notarás que el pan es más liso. Éste se rellena de papa o algún otro ingrediente y se baña en salsa de chile guajillo. En el Estado de México el pan blanco se baña con una salsa roja hecha de chiles ancho, chipotle, puya y guajillo.TamalesLa historia de los tamales se remonta, según algunos arqueólogos como Karl Taube al año 100 a.C. El maíz fue la base de la alimentación de pueblos indígenas, por lo que los tamales fueron una parte importante de la gastronomía de aquella época. No se sabe exactamente dónde se originaron, pero estuvieron presentes en la región mesoamericana. Actualmente hay una gran variedad de tamales en México, pues dependiendo la región puedes encontrar algunos cubiertos con hoja de plátano y otros con la del maíz. Tostadas 
  
 Consideradas sobre todo como un típico antojito mexicano, las tostadas son fáciles de preparar. La tortilla sirve como base para colocar los ingredientes deseados y disfrutarla con el crujido de la tostada en cada bocado. 
 Esquites  
 Este antojito es ideal para disfrutar cuando se visitan los pueblos coloniales de México. Mientras caminas por la calle empedrada seguro verás algún puesto sobre la calle que venda elotes o esquites. Servidos en un vaso, puesdes caminar sin problemas al tiempo que llevas a la boca los granos de elote acompañados de caldo, crema, queso, chile, limón y/o mayonesa. 
 
 Buñuelos 
  
 Hechos con masa de harina freída en aceite, el buñuelo no puede faltar en la mesa o ferias durante diversas celebraciones mexicanas. Originalmente era consumido por culturas mesoamericanas como los Aztecas. Los buñuelos pueden ser tanto dulces como salados, aunque probablemente los primeros sean más conocidos. 
 Birria 
  
 Este platillo se hace a base de carne de borrego o chivo dependiendo la región del país. Se hace un hoyo en la tierra y sobre piedras calientes se coloca una charola con chivo y adobo de chiles. Todo se calienta entre 4 y 5 horas. 
 Tacos de guisado 
 Si quieres otra opción para cenar la noche mexicana del 15 de septiembre, los tacos de guisado son una gran opción. Son los tacos probablemente el antojito o platillo más conocido de México. Rellena la tortilla con chicharrón, papa con chorizo, tinga, mole, rajas con queso o más. 
 *** 
 Te puede interesar: Razones por las que la comida mexicana es la peor del mundo