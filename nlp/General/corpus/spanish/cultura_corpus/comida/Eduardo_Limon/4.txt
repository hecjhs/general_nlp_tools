9 cosas que no sabías que podías freír, pero querrás intentarlo

 
 ¿Qué importan las sugerencias de loa expertos del buen comer cuando lo único que sabes es que el estómago demanda antojos? La gula, el apetito insaciable, la lujuria del paladar y el hambre de estudiante (o de esclavo oficinista) son unos de los más grandes pecados en el mundo; lo sabemos. Son unos pequeños demonios que nos acechan día y noche con un único propósito: el de arrojarnos a las crueles llamas de la inventiva culinaria con tal de procurar enormes cantidades de grasa, calorías y carbohidratos. Incluso ahora hay páginas destinadas a promover ese tipo de cocina y lo único que pueden hacer nuestras almas es pervertirse en una seducción casi diabólica de la gordura. 
  Cuando estamos entonces en manos de esas apuestas sobrenaturales de la gastronomía, hoy llamadas “estrellas” del food porn , no nos queda más opción que involucrarnos de lleno en ese culto y comenzar a aportar ideas; ya propusimos mil y un formas de hacer molletes, cientos de aderezos para un hot dog y decenas de ingredientes para crear la pizza más monstruosa del universo. Pero ¿qué horizontes faltan por ser descubiertos? Quizá podamos romper con los esquemas de lo permitido, llevar nuestro entendimiento al extremo y sumarle a las alternativas que demos una freidora, una sartén y bastante mantequilla. 
  ¿Qué pasaría si tomáramos alimentos comunes y los sumergiéramos en aceite? Si rompiéramos todas esas reglas que conocemos en nombre de la bella, voluptuosa y sensual glotonería, lo único, que podría resultar de ello, obviamente, es perfección pura. Ni más ni menos. 
 
 Ravioles 
 Por ejemplo, freír unos ravioles no es una opción cualquiera. Es la mejor para cuando estás a solas en casa y te espera una noche de entrega total entre tú y Netflix. 
  
 – Queso de cabra El queso de cabra, sabiéndolo empanizar bien y tratándolo con respeto, es una buena alternativa para un fin de semana entre amigos. 
  
 – Pepinillos Los pepinillos pueden ser capeados, empanizados o dejados al natural para después ser arrojados en una alberca de ternura y mantequilla. Perfectos para una noche de viernes. 
  
 – Aceitunas Las aceitunas también pueden ser una mezcla de pan, especias, amor y una freidora en una fría mañana de diciembre. 
  
 – Galletas La masa de las galletas es uno de los alimentos más nobles que la naturaleza (bueno, o el hombre) ha podido darnos; se puede comer cruda, horneada e incluso frita. Gracias, madre Tierra. 
  
 – Masa para pastel El mismo caso ocurre con la masa para hornear un pastel; se puede tomar entre las manos, moldear cariñosamente y dejar jugar en un espejo de mantequilla para ser degustada después. 
  
 – Coliflor La coliflor es un alimento tristemente sano que suele tener un olor gracioso; sin embargo, nada que la harina para rebozar y una sartén no puedan solucionar. 
  El queso panela puede hallar dos formas de perfección: 
 1) Asado a la plancha. 
 2) Empanizado y frito. Como un gran dedo de felicidad. 
 – Elotes 
  Los elotes pueden ser capeados como una banderilla para unirse al aceite en un juego de amor puro, pero también pueden ser arrojados así, al natural, para sorprendernos con el resultado. 
 – Mac&Cheese El Mac&Cheese de por sí es increíble; ahora hay que imaginarlo en una especie de croqueta recién salida de la sartén para saber lo que es el paraíso en la tierra. 
  
 Si eres un amante de la comida y siempre estás buscando nuevas formas de darle pasión a tus gustos, puedes seguir experimentando con otros platillos muy bien conocidos; por ejemplo, con estas 10 recetas para amar los chilaquiles sobre todas las cosas  y estas 9 recetas para preparar una cena elegante en 20 minutos te darán las ideas suficientes en tu cometido.