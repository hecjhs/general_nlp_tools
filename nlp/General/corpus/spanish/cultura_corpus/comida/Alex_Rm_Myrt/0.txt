Frases con las que todo amante del mezcal se identificará

 El mezcal, la bebida de los dioses, fue concebida por un rayo que cayó sobre un agave que dio origen a la primera tatema, iniciando así la tradición del elixir oaxaqueño por excelencia que hoy cobija la cultura culinaria de los mexicanos y con ello: su identidad. 
 El Mezcal tuvo su origen y nombre a partir del principio de combinación, la palabra Mezcal proviene de la estructura náhuatl de la palabra Mezcal: ‘Metl’ que significa Maguey, en yuxtaposición con la palabra ‘Ixcalli’ que quiere decir cocido, la traducción formal debería ser ‘Maguey Cocido’, sin embargo había otro término, una palabra oníricamente  más parecida a la pronunciación actual: “M excalmetl” palabra de origen Náhuatl con la que también se refería a la planta de Maguey.   El término “mezcal” tiene que ver directamente el proceso de preparación de la bebida, una cuestión netamente cultural, es por eso que no se trata de cualquier licor sino de uno que encierra la esencia toda una tradición en su preparación y en su consumo, pues más allá de la fiesta, es utilizado como un brebaje especial para cierto tipo de ceremonias en Mitla por ejemplo donde es apodado como ‘El trago’, también se utiliza para rendir ofrendas o tributos que tienen que ver con cosmovisiones hiladas a las culturas milenarias en México. A pesar de que alrededor del mundo el tequila es quizá una de nuestras referencias culturales, el mezcal posee más que ninguna otra bebida la esencia de nuestra cultura pues antes de que el tequila se posicionara como referente cultural primero existieron los ‘Vinos de Mezcal’ productos hechos durante la conquista a partir de la extracción del jugo de agave, la fermentación y la destilación. 
 Los productores son verderos amantes y cuidadores de la tierra, quizá parte de lo que compone a su sabor y su poder es la dedicación con la que se hace, algunas productoras como Mezcal Amores han demostrado ser sin duda una de las empresas socialmente responsables al cuidar tanto sus productos como a sus maestros mezcaleros, además promover el consumo de productos de artesanos mantiene una conexión entre la industria y la tradicional forma de producir. 
  La razón de Mezcal Amores es crear relaciones ganar-ganar para todas las partes involucradas en el proceso del Mezcal, implementado un modelo creativo que se recrea y adapta a la necesidades que se presentan. Están comprometidos en apoyar a las comunidades locales a través de los maestros mezcaleros, campesinos y la economía local en todas las zonas donde producen: Oaxaca, Durango, Guanajuato y Guerrero. El objetivo principal de esta filosofía es ser lo mas sostenibles posibles. Por ello dedican el 15% de la utilidad bruta de cada botella vendida en los siguientes puntos: 1. Programa de replantación 
 2. Viveros 
 3. Proceso completamente natural y orgánico 
  A lo largo y extenso de nuestra república, la reputación del Mezcal es innegable y presencial, podemos encontrar territorios mezcaleros productores en diversos puntos de la república y consecuentemente las llamadas mezcalerías donde se concentra todo el folclor de la producción y venta de este producto de origen 100% mexicano, cabe mencionar que existen distintas preparaciones e incluso un grado máximo como productor y conocedor, los llamados ‘Maestros Mezcaleros’. 
 Además de catadores, productores tradicionales y rituales que invocan su presencia, su popularidad se extiende a jóvenes y adultos ampliando totalmente la cantidad de productos, sabores, colores y tipos de Mezcales, así pues los estigmas se han hecho a un lado y se ha dado paso a una verdadera experiencia mexicana. 
 Hay formas de tomarlo, mitos que argumentan su existencia, sabores y preparaciones, la picardía del uso del lenguaje del mexicano nos ha dejado conocer algunas expresiones cuya creación se le atribuye al deleite mágico de esta bebida tan popular entre jóvenes y adultos, es por eso que si eres un degustador amante del mezcal te dejamos estas frases con las que seguramente has tirado la farra, ahogado un mal de amores o simplemente beber ‘El trago’ en tu propia ceremonia. 
 He aquí algunas de las mejores frases dichas por mezcaleros de corazón: 
  
 “El hubiera sí existe pero está para torturarte”. 
 “En este mundo terrenal es oro líquido el mezcal”. 
 “Tú tan agua simple y yo tan mezcal” 
 “El mezcal espadín huele a tierra húmeda, es muy romántico” 
 “Si me vas a dejar, que sea con mas ganas”. 
  
 “El Mezcal no te embriaga, te pone mágico”. “Amor sincero el de un mezcalero”. “Santo mezcal, líbranos de todo mal”. 
  
 “Si no hay remedio, litro y medio”. 
 “Lo malo de la soledad es que no la compartimos”. 
 “Nosotros, al tomar mezcal, estamos tomando la energía solar”. 
 “Saturas todos mis sentidos”. 
 “El mezcal cura, después de unos traguitos ya no pienso en ti”. 
   
 Además de ser uno de los mejores compañeros en las celebraciones, recuerda que tomar mezcal es cultural , es casi un rito compartirlo en la mesa para que sea el testigo de los logros, fracasos y celebraciones que marcan cada paso en nuestra vida. 
  
 Sigue a Mezcal Amores en su página de Facebook