Lugares en el mundo que todo amante del misterio debe visitar

 
 Respirar la humedad de las paredes abandonadas, escuchar el sigiloso silbido de una puerta oxidada, sentir el frío que recorre los pasillos del lugar, sospechar de cada rincón obscuro con el que te topes y pensar en lo que te persigue mientras te adentras más a un misterioso lugar son algunas de las sensaciones que a muchas personas han perturbado al visitar casonas, edificaciones o espacios tenebrosos rodeados de leyendas paranormales. 
 Un tanto de imaginación y otro poco de realismo escalofriante es lo que convierte a estos lugares en nidos de historias que la gente asegura verdaderas sobre entidades que divagan o sucesos que ocurren aun después de la muerte. Ya sea como parte de una construcción basada en el cine, la literatura o hasta el arte, estas leyendas han trascendido generación tras generación sugestionando nuestro escepticismo hasta torcerlo de miedo. 
 Pues por más científico que sea tu pensamiento, se vuelve completamente natural experimentar el terror que experiencias como la de adentrarse a un lugar que aún vibra de terribles historias, más que reales, nos pueden provocar. Cárceles, instituciones mentales, mazmorras o escenas del crimen son algunas de las funciones que estos misteriosos y perturbantes construcciones ejercieron en un pasado. 
  
 Si eres amante de esas experiencias que te inquietan hasta calarte los huesos, debes visitar alguno de los lugares más misteriosos y supuestamente paranormales del mundo. 
 *Disfraces de Halloween que sí daban miedo 
 – 
 “La Casa de Monte Cristo” – Nueva Gales del Sur, Australia 
 Considerado el lugar con más fantasmas de toda Australia, esta propiedad ha presenciado una cantidad bastante grande de muertes trágicas y violentas. Estos accidentes o asesinatos súbitos han colocado al inmueble como uno de los puntos más atractivos del país de Oceanía, el cual hoy se conoce como “La casa más embrujada de Australia”. 
  
 Para que te invadas del miedo que algunos valientes se han atrevido a experimentar al visitar esta casa totalmente a oscuras, puedes comenzar viendo este video antes de planear tu viaje. 
  
 – 
 “Castillo de Buena Esperanza” – Ciudad del Cabo, Sudáfrica 
 Ya son varias las apariciones reportadas sobre el castillo en donde muchos aseguran que un hombre camina por las murallas del lugar, al igual que el fantasma de una mujer que corre y llora alrededor y dentro de esas paredes. Lo impresionante fue que durante unas excavaciones muy cerca de la propiedad, descubrieron el cuerpo de un mujer enterrada y a partir de ese momento dejó de presenciarse la figura blanquecina con la que se identificaba y escuchaba a aquel espíritu en pena. 
  
 Antes de comprar tu boleto a Ciudad del Cabo, conoce al equipo de cazafantasmas que se adentró en el castillo para capturar la evidencia de estos espíritus. 
  
 – 
 “La Torre de Londres” – Londres, Inglaterra 
 Lo que en 1078 era una fortaleza, se convirtió en el palacio real que ahora alberga, según la mayoría de los visitantes, una actividad paranormal impresionante. Pues desde fantasmas sin cabeza hasta el espíritu de príncipes, como Ricardo III, es lo que las torres de este lugar esconden. 
  
 Este es un episodio completo dedicado a retratar cada uno de los fantasmas que cuentan que han perturbado a la propiedad inglesa durante años. 
  
 – 
 “Ancient Ram Inn” – Gloucestershire, Inglaterra 
 Solamente con escuchar que dentro de ese pensión se practicaron los sacrificios satánicos más malévolos se nos enchina la piel. En esta posada del siglo XXI casi nadie pone un pie, pero el propietario del inmueble compartió su testimonio contando que la primera noche que pasó en la casa fue “secuestrado” por una clase de almas que después lo arrastraron, con una fuerza inexplicable, por toda la habitación. 
  
 En cinco minutos y medio podrás conocer la historia completa que rodea el misterio del Ancient Ram Inn. 
  
 – 
 “Hotel Banff Springs” – Alberta, Canadá 
 Este hotel es histórico para Canadá, pero también es reconocido por las apariciones que continuamente comentan los huéspedes del lugar; sobre todo una pareja de recién casados que dicen que se aparece sobre el salón de baile del hotel, danzando en medio de la pista. 
  
 La entrevista que esta presentadora de televisión le hace a una de las trabajadoras del hotel te confirmará muchas de las experiencia que los huéspedes del lugar han compartido en distintos medios. 
  
 – 
 “Castillo de Brissac” – Maine-et-Loire, Francia 
 El hermoso castillo fue sede de un horrible asesinato que terminó por convertirse en “El Castillo de la Dama de Verde”; la leyenda cuenta que esta mujer se aparece con un vestido verde y un rostro calavérico en descomposición, gimiendo por los pasillos y ventanas de la imponente propiedad. 
  
 No podrás ver a la “Dama de Verde” pero si conocerás los interiores amueblados del castillo que aseguran ahora es la residencia de este espíritu de vestido verde. 
  
 – 
 “Isla de las Muñecas” – Xochimilco, México 
 La historia sobre la que se siembra el miedo que muchos le tienen a esta isla comienza con un hombre que en los años 50 se refugió sobre este lugar para alejarse de todo, sin saber que ahí mismo seguía flotando sobre las aguas de Xochimilco, y muy cerca de la isla, el espíritu de una niña que se ahogó décadas antes en el lago. El hombre le confesó a uno de sus familiares el pavor que sentía cada vez que veía aparecerse al espíritu de la menor que, de alguna forma, le comunicaba que no dejaría de estar disgustada hasta que él se convirtiera en un fantasma como ella. 
  
 Según la leyenda, el día que él decidió contarle esta historia a un familiar, fue el mismo que lo encontraron ahogado justo donde aquella niña había muerto 10 años antes; pero además de la historia, lo escalofriante del lugar nos perturba al ver las muñecas desfiguradas que cuelgan alrededor de la isla. 
  
 *El cortometraje de H.P. Lovecraft que te hará gritar de miedo 
 – 
 “La Casa Sallie” – Atchison, Kansas 
 Con la reputación de uno de los lugares más embrujados de Estados Unidos, “La Casa Sallie” lleva ese nombre por el fantasma más popular y temido de todas las historias que hacen del lugar un sitio emblemático. Sallie era un niña de seis años que sufría de apendicitis y a quien trasladaron al tercer piso de la casa para ser atendida por un doctor, que tenía su consultorio dentro de la casona. 
  
 Éste decidió operar a Sallie, a quien anestesió por medio de una dosis de éter, la cual no fue la suficiente para que la pequeña no despertará antes de terminar el procedimiento quirúrgico. Pues a mitad de la primera incisión, Sallie despertó agonizando de dolor y entró en un estado de shock debido a la abertura en su estómago; además de eso, se cuenta que justo antes de morir sobre la camilla, la niña miró con odio al médico, quien aseguró que desde ese día ella no dejó de aparecerse un solo día por los pasillos de la mansión. 
  
 – 
 “Salon y Hotel McMenamins White Eagle” – Oregon, Estados Unidos 
 Más que un lugar al cual temerle por los fantasmas que residen en él, este parece un espacio al cual nunca deberías entrar, pues en de las historias de algunos huéspedes del restaurante y hotel, ellos aseguran haber sido empujados por las escaleras por una fuerza invisible que sintieron como dos manos sobre su espalda. 
  
 La evidencia que este grupo de cazafantasmas capturó en video demuestra, según ellos, cómo un miembro del equipo fue perseguido por un fantasma durante el recorrido que hicieron dentro del edificio. 
  
 – 
 Castillo Akershus – Oslo, Noruega 
 Más bien como una fortaleza de lo sobrenatural, el castillo de Akershus ha atemorizado a sus visitantes debido a una serie de específicas y claras apariciones, como un mujer en túnica y un perro de ojos rojos que vagan por el lugar desde que fueron traumáticamente ejecutados en lo que hace años fue una de las prisiones nazis dentro de la que se organizaban todo tipo de ejecuciones. 
  
 Aunque no te espantarás al conocer por medio del video la fachada e interior del castillo nazi, sí te convencerás de empezar a ahorrar para conocer la imponente edificación. 
  
 El miedo proviene de cualquier cosa que nos parezca desconocida, inexplicable, extraña o perturbadora, y aunque para muchos es una sensación insoportable, para otros resulta una experiencia excitante la de arriesgarse a sentirlo hasta superarlo, o bien, hasta salir corriendo de aquello que les produce tanto terror. 
 Afortunadamente para estos apasionados del misterio, no sólo los viajes pueden aportarles este tipo de experiencias, pues existen algunos lugares aquí en la CDMX en los que también podrás pasar una noche tétrica, como los bares de la Ciudad de México para pasar una noche oscura y de la literatura también se pueden obtener historias más que descriptivas que seguramente te provocarán pesadillas, simplemente elige alguno de los 66 libros de terror que debes leer si deseas sentir miedo por el resto de tu vida .