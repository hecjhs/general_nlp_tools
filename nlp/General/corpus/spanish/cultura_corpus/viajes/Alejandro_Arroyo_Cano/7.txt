Los barrios hipsters de Latinoamérica que mueres por conocer

  Históricamente, los hipsters eran los jóvenes amantes del jazz, de la ginebra y de bailar swing. No es extraño que cuando alguien de este grupo enlista sus libros favoritos menciones las novelas de F. Scott Fitzgerald, Jack Kerouac y William Burroughs, escritores que elogiaban estos vicios dentro de sus libros. Dos de estos autores llevaron al extremo sus pasiones que tuvieron que salirse de su país para poder drogarse tranquilamente. 
 William Burroughs, el máximo representante de la Generación Beat, encontró en México un pasivo lugar para la experimentación, el Surrealismo, el uso de drogas y la libertad sexual. El barrio donde se instaló él, y posteriormente Kerouac, fue la Roma. Desde la década de los 50 este popular barrio de la Ciudad de México se convirtió en rl lugar donde los artistas se reunían, ingerían alucinógenos y experimentaban con la literatura. 
  Si te interesa saber más conoce los tres libros básicos para entender la Generación Beat . 
 Los dos miembros de la Generación Beat quedaron encantados por las calles que conservaban el estilo colonial, con colores vívidos, un cielo despejado y limpio, así como por la gente que era más natural a comparación de los habitantes de las grandes ciudades. La estadía de Keroauc y Burroughs en México duro sólo un par de años, porque sus fuertes adicciones los fueron orillando a la destrucción. Los barrios que a continuación se enlistan aún conservan ese paisaje que fascinó a los escritores. No son ciudades cosmopolitas, pues en ese ambiente la creatividad se esfuma entre grandes rascacielos. En cambio, en estas pequeñas localidades lo vintage , el arte y la espiritualidad fluye por sus calles para inspirar 
 – Barrio Lastarria, Chile 
  
 Kerouac y Burrougs encontraron en México un lugar donde fluía la cultura, por eso se quedaron varios años. El barrio de Lastarria es su símil en Chile. Aquí los artistas se reúnen para compartir experiencias y, a su vez, recibir inspiración. Cada año se realizan cientos de eventos culturales que colorean las calles con música, pintura y teatro. Los hipsters, o mejor dicho, los nuevos artistas, encontrarán un nuevo hogar en esta localidad. 
  
 – Barranco, Perú 
   
 Por el día el turismo se concentra en conocer los mejores restaurantes de comida peruana. Todo parece un ambiente familiar y tranquilo, pero una vez que cae la noche, el lugar se transforma para darle gusto a la vida bohemia con sus cientos de pubs y bares en los que un escritor puede encontrar una gran inspiración para contar sus ácidas historias. 
  
 – Valparaíso, Chile 
   
 Esta ciudad tiene una vigorosa actividad cultural, en especial en el arte urbano. Su calles han sido intervenidas por cientos de artistas visuales y ahora es considerada como un enorme y colorido cuadro que asombra a cualquier visitante.  El ambiente es perfecto para que cualquier artista pueda crear su obra cumbre. Por las noches hay varios lugares para tomar alcohol mientras se escucha una buena pieza de jazz o blues.  
 – Antigua, Guatemala 
   
 Muy cerca de la frontera con México, se encuentra un pequeño barrio donde el tiempo dejó de correr. Su arquitectura aún tiene un estilo colonial, con calles empedradas y cielos despejados.  Las tradiciones aún se conservan, así como el buen humor de sus habitantes. Si eres pintor y buscar escenarios con mucho color y profundidad, este es el lugar indicado. 
  
 – La Candelaria, Colombia El barrio de la Candelaria, en Colombia, cuenta con más de 500 lugares dedicados al arte; desde galerías, museos y teatros, hasta bibliotecas y universidades. para cualquier lugar al que se voltee se encontrará una expresión artística. Si Burroughs hubiera vivido más tiempo, seguramente hubiera viajado a este lugar para experimentar con sus letras e imaginación.  – La Boca, Argentina 
   
 En la zona de Caminito se encuentra uno de los barrios más coloridos de toda Argentina. Estas casas fueron las viviendas típicas de los inmigrantes italianos que llegaron en el pasado. Hoy quedó un gusto europeo en sus muros. La variedad de colores del paisaje atrae a los artistas de todo el país y el mundo. Si quieres combinar tus prendas vintage , lo mejor es visitar este lugar al menos una vez en tu vida. 
  
 – William Burroughs y Jack Kerouac, los ancestros de los hipsters, buscaban ciudades tranquilas donde no existieran cadenas para experimentar en su arte. Por eso huían de las grandes ciudad y por eso estos barrios son los indicados para un viaje como los de ellos. Si aún no tienes la oportunidad de salir a explorar Latinoamérica, conoce los 10 lugares en México perfectos para viajar de mochilero o los 8 impresionantes escenarios naturales que te harán sentir en otro planeta.