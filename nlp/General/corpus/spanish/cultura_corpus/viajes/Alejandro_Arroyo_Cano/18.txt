El roadtrip por México diseñado para surfers de todo el mundo

 
 Más que un deporte, el surf es un arte. Tener la delicadeza de acariciar una ola mientras se mantiene el equilibrio en una tabla que se desliza, casi como por nubes, a altas velocidades, es un acto que requiere más que sólo una agilidad y desarrollo físico. Surfear requiere la suavidad de un trazo y la fuerza para esculpir el agua que deja a los espectadores con la boca abierta. Así le pasó al británico James Cook , quien durante un viaje  las islas Hawaianas en 1778, se impresionó de como los habitantes surcaban el mar con tablas prácticamente de la selva. 
 Antes de sumergirte totalmente en el agua, tal vez necesitas una pausa y revisar algunas fotografías que contrastan la expectativa y la realidad de viajar por el mundo, si te gustaría conocerlas, da un click aquí . 
 Cook llevó este arte a Europa y a principios del siglo XX, la popularidad de este deporte llegaría hasta Estados Unidos y Australia. En la década de los años 60, las playas de Tijuana y Ensenada de Baja California comenzaron a ser visitadas por surfistas norteamericanos que buscaban lugares solitarios para domar las aguas. A partir de ahí, algunos puertos de Sinaloa, Nayarit, así como las costas de Michoacán y Oaxaca comenzaron a ser el punto de reunión de deportistas de todo el mundo. 
 – Todos Santos, Baja California Sur   Hogar de artistas, surfistas y extranjeros, se localiza a 80 kilómetros de  La Paz. Esta playa es para quienes practican el surf como deporte extremo debido a la gran altura y fuerza que cobran sus olas al llegar a la costa. Los conocedores dicen que esta playa es el más grande reto, inclusive para los expertos. 
  
 – Rosarito, Baja California 
  
 Esta playa está ubicada a 26 km al sur de Tijuana y puedes llegar a ella por la carretera federal 1D. El rompimiento de las olas ese da durante todo el año, así que puedes visitarla cuando quieras. Toma en cuenta que el lado de aquiebra es por ambos sentidos y su fondo es de barra de arena. 
  
 – Escollera, Mazatlán 
  
 Mazatlán es un paraíso y un recinto ideal para los surfistas. Cuando llegues a este hermosos lugar para surfear, considera que el fondo de la playa es de piedra, para que tengas cuidado cuando caigas de la tabla. Otro dato que debes saber es que el lado de aquiebra es hacia la derecha y que la época de rompimiento es durante todo el año. 
  
 – Stoners, Nayarit 
  
 Esta playa es muy concurrida por surfistas australianos, así que además de poder surcar por las olas, conocerás a gente muy interesante.  El fondo de la playa es de piedra, el lado de aquiebra es hacia la derecha, así que toma este factor a tu favor. Por fortuna, durante todo el año las olas son ideales para surfear, así que prepara muy bien tu viaje para que sea una experiencia inolvidable. 
  
 – Barra de Navidad, Jalisco 
  
 En las costas de Jalisco se encuentra un excelente y hermoso lugar para visitar en vacaciones. Si eres surfista, no esperes hasta la temporada de descanso y viaja para domar las olas. La playa tiene un fondo de piedra y arena, así que puedes relajarte un poco si cometes un error. El lado de aquiebra es hacia ambos lados durante todo el año. 
  
 – Boca de Pascuales, Colima 
  
 El estado de Colima tiene una gran fama por su grandes áreas de naturaleza casi intactas. En la costa tiene un espacio ideal para que los surfistas practiquen su amado deporte de una manera libre y segura, pues el fondo de la playa es de barra de arena, haciendo un poco más suaves las caídas. Eso sí, tienen que tener un poco de experiencia para poder cazar las olas que quiebran para ambos lados. 
  
 – Bonfil, Guerrero 
  
 Guerrero siempre ha sido un punto de reunión turístico para todo el mundo, y más allá de las los lugares comunes del estado, se encuentra un lugar especial para todos los surfistas. Bonfil tiene un fondo de arena y las olas quiebran en ambos lados, así que es una playa de dificultad mediana. Si estás empezando en este deporte, puede ser un buen lugar para practicar. 
  – Baja Malibú, Baja California 
  
 Regresando al norte del país, Baja Malibú es un hermoso lugar donde surfistas extranjeros se reúnen para dar a conocer sus mejores trucos. El fondo de la playa es de barra de arena y el lado de aquiebra es hacia ambos lados, entonces se puede decir que es una playa salvaje que tienes que respetar. Si eres un deportista experimentado, gozarás del reto que te ponga. 
  
 – Las Gatas, Zihuatanejo 
  
 Este hermoso lugar es conocido porque parece el mismo paraíso. Y para hacer todo mucho mejor, las olas de la playa Las Gatas son ideales para los surfistas. El lado de aquiebra es hacia ambos lados, así que deberas tener un poco de experiencia para no tener problemas al domar sus aguas. Puedes despreocuparte un poco, pues su fondo es de arena y suavizará las pequeñas caídas por cualquier descuido. 
  
 – Puerto Escondido, Oaxaca 
  
 Oaxaca lo tiene todo; una hermosa ciudad, áreas naturales por explorar y una costa ideal para llegar y surfear todo el día y todo el año. La playa Puerto Escondido tiene un fondo de arena y el lado de aquiebra es hacia la derecha, así que es un buen lugar para practicar sin tener mucho miedo a sufrir un accidente. Date la oportunidad de conocer una de las mejores playas de México. 
  
 Después de ver estos escenarios magníficos y hermosas deportistas, debes conocer otros lugares donde también puedes surfear o simplemente disfrutar de las delicias del mundo . Tal vez quieras conocer las playas de Europa pero no tienes el dinero necesario, si es el caso, puedes conocer un artículo que te dirá cómo hacer tu sueño realidad  sin gastar mucho . También puede que estés cansado de soñar y necesitas algo más de realidad, entonces debes conocer algunos datos históricos que no podrás creer que sucedieron en verdad.