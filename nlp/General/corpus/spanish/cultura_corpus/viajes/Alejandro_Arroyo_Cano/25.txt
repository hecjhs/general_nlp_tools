Más que contaminación: lugares imprescindibles para conocer en China

 
 Si entraste a este articulo es porque tienes un interés por viajar a ese país desde tiempo atrás y por supuesto, ya conoces lo suficiente de su cultura, así que sería repetitivo describirte lo que ya sabes. En cambio, se ocupará este espacio para decirte cuáles son los lugares que debes visitar cuando llegues a ese enorme lugar, y para complementar tu viaje, sabrás algunos pequeños detalles que harán  tu viaje más placentero. 
 – Cosas que debes tomar en cuenta antes de tu viaje. 
  
 Antes de que te enamores de sus increíbles recintos, conocerás información importante que debes considerar antes de viajar hasta el otro lado del mundo. 
 Moneda: La moneda oficial de la República Popular de China es el Renminbi (Rmb), que se traduce como “moneda del pueblo”. Para que lo tengas en cuenta, 100 pesos mexicanos equivalen a 35.66 Renminbi. 
 Idioma: La Constitución china establece que el Estado promueve el uso nacional del Putonhua, mejor conocido como chino mandarín. Tampoco es necesario que te alarmes, porque hasta esa parte de la Tierra llegó el inglés, en especial en Beijing y Pekín. 
 Religión: Las religiones tradicionales de China son el taoismo, practicada por 20 millones de personas, mientras que el budismo rige la vida de más de 100 millones. Aunque no lo creas, también se practica el confucianismo, aunque más que una creencia religiosa es un un sistema de conducta con enorme influencia en la historia del país. 
 Documentación: De acuerdo con los últimos acuerdos diplomáticos, cualquier mexicano tiene derecho a viajar a China con una visa. Para obtenerla necesitas acudir a la Oficina Consular China con tu pasaporte con validez mínima de seis meses y con dos hojas en blanco consecutivas, junto con tu boleto de avión de viaje redondo y una carta de invitación emitida por la entidad o persona que cumpla los requisitos en China. El precio puede variar, a los mexicanos les otorgan el documento con un costo aproximado de 100 dólares. 
 – Cosas que no debes hacer cuando llegues a China 
  
 Viajar durante fiestas nacionales chinas: La primera semana de octubre y la semana anterior y posterior al Año Nuevo Chino (varía de fecha cada año, pero por lo regular es entre enero y febrero) son las peores. Lo cierto es que últimamente da igual en qué época del año viajes, pues el turismo va en aumento y es casi imposible transitar por sus calles a cualquier hora del día. 
 Querer visitar sitios gratis: En China se paga por todo. El turismo se ha convertido en un negocio donde todos se quieren hacer ricos, y encontrarás que si visitas, por ejemplo, unos  campos de arroz  tendrás que abonar un ticket de entrada en varias zonas, y que estos tickets cuestan lo mismo que visitar la Ciudad Prohibida de Pekín. Todo tiene un precio, así que prepárate. Esperar a que los coches te dejen pasar: No te confíes ni aunque el semáforo esté en verde para ti, muchos hacen caso omiso.  Los pasos de peatones sin semáforo no sirven absolutamente para nada y aunque te lances a cruzar por ellos, más te vale correr si ves que un coche se te acerca. El tráfico es salvaje y los coches tienen preferencia sobre las personas. 
 Después de conocer los requerimientos mínimos para viajar a este místico país, ahora conocerás los lugares que podrás visitar y que nunca se borrarán de tu memoria. 
 – La Montaña Amarilla 
  
 La imagen de un árbol torcido que crece de una roca curiosamente curvada viene a la mente de sus habitantes cuando se habla sobre la Montaña Amarilla. Estas místicas montañas cubiertas por una mágico velo de neblina, son parte del paisaje más bello y famoso de China. Su atractivo inicia durante el amanecer, cuando los primeros rayos del sol colorean el paisaje de colores indescriptible  y sus “cuatro maravillas naturales” (pinos, rocas de formas irregulares, mares de nubes, y aguas termales) se llenan de vida. 
  
 – Jiuzhaigou 
  
 Ubicado en el remoto oeste de China, este paisaje ha inspirado los sueños de los viajeros para pensar en lugar habitado hadas u otras criaturas mágicas. Jiuzhaigou cuenta con lagos multicolores rodeados de extensas  áreas boscosas y montañas. Sus aguas cambian de color durante todo el día y el año. 
  
 – Grutas de Yungang 
  
 Este lugar sagrado fue construido hace más de 1,500 años como uno de los más importantes templo budistas en China. Fue levantado en en el interior de unas grutas que albergan 252 cuevas y más de 51 mil estatuas de Buda, la mayoría talladas entre el quinto y el sexto siglo durante la dinastía Wei del Norte. 
  
 – Las Tres Pagodas del Templo Chongsheng 
  
 Estas tres torres budistas son el principal punto de referencia de Dali, una antigua ciudad en el sudoeste de China, en la provincia de Yunnan. La torre principal fue construida a mediados del siglo IX con la esperanza de aliviar las inundaciones periódicas. Su altura es de 69 metros, considerada como un rascacielos para la dinastía Tang. Las otras dos torres son idénticas, aunque se construyeron casi un siglo más tarde y miden 42 metros de altura. 
  
 – La Muralla China 
  
 Uno de los símbolos icónicos de China, la Muralla es el muro más largo del todo el mundo. Es tan grande que se alcanza a ver desde el espacio. Esta construcción, impresionante muestra arquitectónica defensiva, tiene una extensión de cinco mil kilómetros, por lo que sería imposible mantenerla toda en buen estado. Las hermosas fotos que se observan en todos lados son tomadas en el área que está cerca de Pekín. 
  
 – El Ejército de Terracota 
  
 Estas estatuas habían permanecido bajo tierra por más de dos mil años. Fue en 1974 cuando un grupo de agricultores, queriendo construir un pozo, descubrieron uno de los sitos arqueológicos más impresionantes de todo el mundo. Las esculturas representan al ejército que venció a los pequeños pueblos rebeldes en el Periodo de los Estado Combatiantes de 475 a 221 a. C., gracias a ellos China se consolidó como un único país. 
  – Lago Hangzhou 
  
 Conocido también como “el paraíso en la tierra” o “Lago del Oeste”, es un hermoso y pasivo paisaje chino que mantiene vivas la arquitectura tradicional que se conoce en libros y películas. Para admirar su belleza, los visitantes pueden tomar un viaje en bicicleta o caminar alrededor del lago, el cual es también Patrimonio de la Humanidad. 
  
 – La provincia de Fenghuang 
  
 Fenghuang significa “fé nix” en chino, que representa la longevidad y el buen augurio. La mayor característica de este bello lugar es el conjunto de casas Diaojiao , que son viviendas de madera en saledizos. Están construidas a orillas del río Tuo Jinag y en las pendientes de las montañas. Las calles del pueblo conservan el mismo trazado de hace años. Puedes pasear por sus más de 30 calles del mismo ambiente antiguo y tradicional. Los turistas también pueden tomar el bote que recorre el río y que cruza el pueblo. 
  
 – La ciudad de Beijing 
  
 Esta ciudad alberga algunos de los más importantes restos del pasado imperial de China, como lo son la Ciudad Prohibida, el Templo del Cielo, el Palacio de Verano y la Plaza de Tian’anmen, donde la República Popular de China fue inaugurada por el Presidente Mao, cuyo cuerpo aún reside allí dentro de su ataúd. La tradición se mezcla con los espectaculares recintos que fueron construidos recientemente como la súper moderna Villa Olímpica, el estadio Nido de Pájaro y Cubo de Agua. 
  
 – Esta fue una pequeña guía para tu próximo viaje a China, si quieres complementarla, debes conocer unos cuantos tips para no morir en el intento. 
 *** Te puede interesar: 
  10 preguntas que debes hacerte durante un viaje para saber que lo estás haciendo bien  13 cosas que puedes hacer para ganar dinero y hacer el viaje de tus sueños