Viajes que debes hacer según tu música favorita

 
 Prácticamente todos los actos del ser humano se podrían resumir en una única y primordial necesidad: la de conocer. Cualquier acción humana, ya sea científica, humanista, personal, colectiva, artística o de cualquier otra naturaleza , se basa en una urgencia por transformar y desentrañar lo vivido. Por hacer más placentera, transparente e interesante nuestra experiencia de estar aquí, en el mundo que nos conforma, pero al cual también configuramos. De esta forma es como se torna posible hacer cruces entre disciplinas o prácticas que nos ayudan a descubrir, cada día un poco más, ese vasto universo en el cual fuimos arrojados. Una de las combinaciones más atractivas en la actualidad, entonces, es aquella que nos permite desplazarnos física y emocionalmente a lo largo y ancho del mundo : el turismo musical. 
  Esta manera de viajar, con un propósito específico y siempre múltiple, se ha convertido en una tendencia desde hace pocos años, aunque constantemente se ha sabido que en el planeta Tierra tenemos muy diversas manifestaciones culturales, especialmente cuando hablamos de música. Lo que hace distinto a este modo de excursión, es que sus paradas consisten en la participación de un espectáculo musical o el descubrimiento de ritmos característicos en ciertas regiones. Es así como se ha promovido recientemente una búsqueda por aquellos viejos sitios donde se originaron ciertos géneros o artistas imprescindibles, y por esos destinos que ahora promueven la vanguardia, la renovación y la gran proyección dentro del campo sonoro. 
 A continuación, te presentamos 8 destinos que no te puedes perder si amas alguno de los géneros musicales más característicos. Considéralos para tus próximas vacaciones y empaca sólo lo necesario, porque sus músicos y festivales se encargarán de darte lo extraordinario. – Jazz 
  
 Nueva Orleans, Louisiana 
  Obviamente, la parada obligada es y seguirá siendo Nueva Orleans. En Bourbon Street, plagada de una increíble arquitectura francesa, se vive el jazz como en ningún otro sitio; los clubes de jazz y blues organizan las mejores noches y se recuerda con gozo a Keppard, Armstrong y Oliver. Imperdonable no visitar el Congo Square, mítico lugar donde todos los grandes músicos del jazz solían reunirse a tocar. 
 República Checa 
  Ya que si lo tuyo es una mirada un tanto más europea en torno al jazz, puedes visitar este país que bien ha adoptado al género y ahora se encarga de organizar un evento anual llamado “Bohemia Jazz Fest”; un destino básico si se pretende conocer dicho ritmo completamente a fondo. – Rock 
  
 Austin, Texas 
  Si alguien quiere saber de verdad lo que está ocurriendo en la escena del rock contemporáneo, debe dejar ir ciertas creencias absurdas y dirigirse cuanto antes a Austin. Sellos discográficos, radiodifusoras, plataformas de emergencia, productores y una sociedad en extremo involucrada a darle un nuevo rostro a la música se reúnen en este estado norteamericano. 
 Inglaterra 
  Con una amplia historia musical, este país se centra como una de las cunas para el turismo de esta especie; desde The Beatles hasta los estudios que vieron nacer el britpop, a cada esquina se puede hallar un punto distinto que narre la evolución del rock en el mundo. – Electrónica 
  
 Amberes, Bélgica 
  Y obviamente nos referimos al festival Tomorrowland: ese evento que conjuga la tradición europea, el cuento fantástico de sus regiones y la innovación constante en los terrenos de la música electrónica. Todos sabemos que se puede ver en línea sin ningún problema, pero nada como estar allí y vivir la experiencia en carne propia. 
 Lyon, Francia 
  En este lugar se ha realizado desde 2003 un festival llamado “Nuits Sonores”, el cual da lugar a propuestas de música electrónica e independiente al mismo tiempo que promueve la discusión alrededor de otras artes. El evento adquiere cada vez más notoriedad, aunque podríamos decir que no es un suceso para cualquiera. – Metal 
  
 Wacken, Alemania 
  Alemania siempre se ha distinguido por ser la cuna de varios géneros “pesados” en la historia de la música; pasando por el gótico, el industrial y llegando hasta el heavy metal, siempre hemos ubicado a la nación germana como un destino para ese tipo de innovaciones estilísticas. Wacken Open Air es un festival sumamente relevante para todos los amantes de dicho sonido. – Pop 
  
 Pilton, Somerset 
  En Inglaterra tiene lugar uno de los festivales más importantes del mundo en cuanto a música pop se refiere; podríamos pensar que una serie de ritmos y géneros se reúnen en un solo lugar a finales de junio cada año, considerando que se entremezclan en el lineup sonidos de rock, indie, folk, hip hop, etcétera, pero la realidad es que el fuerte de este evento es la música pop o el rostro más amable de cualquier otro estilo musical. – ¿A cuál de estos te enlistarías primero? Es una difícil decisión y aunque se traten de mantener los precios bajos en el sector del turismo musical , siempre se requiere una inversión significativa para no morir en el intento de explorar el mundo mediante sus sonidos . 
 *** Te puede interesar: 
 Un viaje en el tiempo a través de fotografías por los mejores festivales de música 15 himnos de festivales de música