8 bares que los amantes del terror morirán por conocer

 
 El arte tiene la capacidad única de transportarnos a sitios irreales y crear a través de un relato, una canción o una obra pictórica, una construcción mental en la que llenamos aquellos espacios que el autor deja libres a la interpretación con nuestros propios conceptos, creando un reflejo único de su obra dentro de nuestra imaginación, una de las sensaciones más gratificantes y creativas que podemos experimentar. 
 En ocasiones nuestro agrado con una construcción mental es tal que fantaseamos con su materialización. Estos bares temáticos llevaron esas fantasías al plano terrenal y son la mejor opción si eres un fanático de  la literatura y el cine de terror : 
 – 1. Frankenstein – Edimburgo, Escocia 
  
 Un sitio definitivamente inspirado en la novela de Mary Shelley. El interior está decorado con miles de objetos alusivos a la obra y a las películas que ha inspirado, y cuenta con un calendario particular de eventos, desde las noches de cine mudo los martes o el clásico sábado de karaoke hasta la atracción principal: un Frankenstein animado que todas las noches cobra vida en medio de un juego de luz y sonido. 
  
 – 2. Cambiare – Tokio, Japón 
  
 La atmósfera de una de las películas más estéticas de terror jamás realizadas, “Suspiria”, de Dario Argento, es recreada en este bar de estilo italiano ubicado en el distrito de Shinjuku. El nivel de detalle es impresionante, los muros con diseños floreados, el piso conserva los tonos rojos y negros de la película, los mosaicos geométricos y el ambiente te pondrán en medio del misterioso departamento de Suzy Bannion. 
 – 3. The Lovecraft Bar – Portland, Estados Unidos 
  
 Abierto en 2011 con la consigna de rendir tributo a la prolífica obra de H. P. Lovecraft, este bar se caracteriza por la diversidad de bandas que alberga en vivo, desde punk, rock alternativo, electro y hasta música de los 80 o 90, que lo han convertido en uno de los mejores sitios de Oregon para bailar. Todo en un ambiente oscuro que transporta irremediablemente al mundo de fantasía del autor de “El caso de Charles Dexter Ward”.   
 – 4.   Donny Dirk’s Zombie Den – Minnesota, Estados Unidos 
  
 Este bar tiene toda una teoría detrás que demuestra casi científicamente porqué será un gran refugio cuando llegue el apocalipsis zombie, relacionada con el frío intenso de la región y los grandes cerebros que ahí se congregan. El ambiente es cálido sin dejar de ser tétrico (las cabezas de zombie empotradas en la pared lo reafirman todo el tiempo) y las especialidades de la casa son las cervezas artesanales y las infusiones creadas por ellos mismos.   
 – 5. Giger Bar – Chur, Suiza 
  
 Cada uno de los elementos dentro del Giger Bar está inspirado por el artista suizo con el estilo que desarrolló a lo largo de su obra. Las sillas, mesas y columnas incluyen estructuras óseas con formas de criaturas y máquinas, parecidas visualmente a la serie Alien, que trabajó junto con Ridley Scott. Es simultáneamente un museo que alberga distintas piezas de la colección personal del autor. 
  
 – 6. The Slaughtered Lamb – Nueva York, Estados Unidos 
  
 Inspirado en el largometraje “An American Werewolf in London”, este bar ubicado en el vecindario de Greenwich tiene tres grandes salas: el lobby principal donde se puede tomar un trago sobre la barra, el lounge para acompañar una charla frente a la chimenea con alguna cerveza del amplio menú o bien, las mazmorras, donde el ambiente es propicio para grupos más grandes e incluye juegos de dardos. 
  
 – 7. Vampire Café – Tokio, Japón 
  
 Esta bar ubicado en el mismo distrito que el Cambiare, se distingue por tener una decoración en su totalidad roja y muy elegante, un espacio dispuesto para vampiros sedientos de sangre. Al cruzar la puerta, una mesera disfrazada de ama de llaves te lleva a tu mesa entre muros y candelabros que dan la impresión de estar en un lujoso castillo. 
  
 – 8. The Jekyll & Hyde Club – Nueva York, Estados Unidos Autoproclamado como “el único restaurante y bar embrujado de la ciudad”, Jekyll & Hyde Club promete cenas en un ambiente fuera de lo ordinario. La decoración está llena de artefactos tenebrosos de todo el mundo y las criaturas que ahí habitan (un hombre lobo, una gárgola, un vampiro y una momia) cobran vida cada 10 minutos, reafirmando la frase que tienen como insignia. En octubre, el bar habilita un espacio como casa embrujada para todos los comensales. 
  
 – Si pretendes cambiar totalmente la experiencia de cenar o tomar un trago, los restaurantes y bares temáticos son la opción. El concepto en su totalidad crea una atmósfera que te hace escapar de la realidad por un par de horas dentro de un ambiente bohemio y tenebroso, ideal para sorprender a una cita y visita obligada en tu próximo viaje. ¿Conoces algún sitio similar? 
 *** Te puede interesar: 
 10 películas de terror que nunca deberías de ver solo  10 películas de terror tan intensas que te podrían provocar un paro * Fuente: 
 Bloody Disgusting