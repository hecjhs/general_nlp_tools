5 lugares más profundos de la Tierra que puedes visitar si no te asusta la oscuridad

 
 La Tierra esconde secretos inimaginables. La insaciable curiosidad del hombre por descubrir el mundo lo ha llevado a recorrer casi todos los confines del globo. Del Ártico a la Tierra del Fuego , los humanos aprendieron a adaptarse a las condiciones climáticas más adversas y por medio de riesgosas expediciones, poblaron sitios impensados. Con la invención del GPS, la cartografía quedó resuelta y con ella se concretó el conocimiento de cada coordenada terrestre, pero ¿qué hay de lo que se oculta debajo de la superficie terrestre y oceánica, que contiene los más grandes misterios de la vida? 
 La ambición por recorrer las entrañas del planeta está latente en los espíritus más aventureros. Si no tienes problema por estar en espacios reducidos, sitios oscuros y húmedos donde el nivel de oxígeno en el ambiente escasea, atrévete a descubrir los destinos más profundos de la Tierra, escenarios reales que ni siquiera Julio Verne pudo imaginar: 
 – Cueva de Voronia, Georgia 
   
 En el sistema montañoso del Cáucaso Occidental se encuentra la cueva conocida más profunda del planeta: se trata de un agrietado espacio dentro del macizo de Arábika que se abre y desemboca en un abismo de más de 2,200 metros, según las últimas expediciones. Descubierta en 1960, equipos científicos realizan expediciones anualmente al lugar para analizar sus cualidades minerales y la vida que se desarrolla en ella. En 2010 se encontraron 500 especies nuevas de plantas y animales a más de 1,500 metros de profundidad. 
  
 – Fosa de las Marianas, Océano Pacífico 
   
 Es conocido que el origen de la vida se encuentra en el mar. Uno de los sitios más importantes para la investigación relacionada con este gran misterio es la Fosa de las Marianas, una depresión en el Pacífico con más de 10 mil metros de profundidad, ubicada al sur de Japón y las Islas Marianas. El abismo de Challenger es el punto más bajo que se conoce de la corteza terrestre y la presión es tal que ningún submarino es capaz de llegar a la zona abismal, donde existen organismos unicelulares y fotolumínicos desconocidos. 
  
 – Volcán Thrihnukagigur, Islandia En la mágica tierra de Islandia se encuentra el volcán Thrihnukagigur, un gigante dormido desde hace más de 4 mil años, pero cuya cámara magmática se mantiene activa. Si decides aventurarte en un tour a las entrañas del volcán, abiertas al público desde 2012, deberás recorrer dos kilómetros de un paisaje inhóspito antes de iniciar el descenso. Entre desfiladeros e increíbles paredes de color bronce, azul cobalto y dorado formadas hace miles de años, los más arriesgados llegan al final del recorrido, desde donde se contempla el fondo de magma ardiente.  
 – Sótano de las Golondrinas, México 
   
 La Huasteca Potosina guarda uno de los abismos naturales más impresionantes del mundo, santuario de la naturaleza y refugio de miles de aves y especies endémicas. Descubierto en 1966, tiene una abertura de 60 metros de diámetro cuya profundidad ronda los 512 metros, 376 de ellos de caída libre. Las visitas guiadas parten un poco antes del amanecer para presenciar el espectáculo que hacen miles de aves al salir por la mañana, provocando un ruido ensordecedor y un abanico de colores sin igual. 
  
 – Majlis al Jinn, Omán 
   
 En el noreste de Omán, el paisaje desértico se colapsa en una enorme fosa de más de 140 metros de profundidad y un kilómetro de longitud. El sitio es visitado por aventureros y montañistas de todo el mundo, especialmente quienes practican la escalada en roca. La única forma de bajar por sus paredes verticales es a través del descenso de rappel, considerado como un sitio para los más experimentados por su alto riesgo. Una vez abajo, el ascenso es la parte más exigente, pues la erosión del terreno dificulta encontrar los puntos de apoyo necesarios para volver a la superficie. 
  
 Si estás cansado de hacer el mismo turismo de siempre, no te pierdas de las ciudades ocultas que creías no podías visitar y apúntalas en tu lista de destinos para las próximas vacaciones. ¿Amas viajar en avión? No te pierdas de los mejores paisajes que necesitas ver por lo menos una vez en la vida desde las alturas .