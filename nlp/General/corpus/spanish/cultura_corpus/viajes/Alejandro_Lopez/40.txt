10 destinos de México que debes conocer antes de ir a Europa

 
 Sabemos que el cliché de conocer Europa es un deseo latente en el imaginario colectivo latinoamericano, que además de alimentarse constantemente por la sociedad occidental a través de la idealización e idolatría desmedida por todo lo proveniente de ultramar, suele poner por encima de los pueblos americanos a las capitales europeas en todos los aspectos, aún cuando se trata de sitios con historia y procesos completamente distintos. 
 Estos son 10 destinos de México que guardan una riqueza cultural, histórica, artística y natural sin comparación que deberías conocer antes de ir a Europa, para los cuales no necesitas hacer una inversión tan grande ni obtener un pasaporte. 
 – Oaxaca de Juárez, Oaxaca 
  
 La Sierra Madre del Sur resguarda una de las ciudades más coloridas de México: la capital de Oaxaca es un punto de encuentro de distintas culturas concentradas a lo largo y ancho de sus ocho regiones, donde el folclor de la artesanía tradicional se mezcla con las representaciones plásticas del arte moderno y crea un estilo único en el mundo. Internarte en sus decenas de mercados es la mejor forma de conocer la esencia de Oaxaca, que además de ofrecer artesanías, alebrijes, textiles y piezas autóctonas, es sede de la gastronomía más rica del país. 
  
 – Zacatecas, Zacatecas 
  
 Una ciudad que rebosa de historia: desde la época colonial, cuando se construyeron obras muy importantes del barroco y el tequitqui, hasta la famosa Batalla de Zacatecas que encumbró a la División del Norte de Pancho Villa. Tiene un teleférico que comunica al Cerro del Grillo con el Cerro de la Bufa y brinda una vista excepcional de toda la ciudad. El centro histórico es Patrimonio Cultural de la Humanidad y se puede recorrer a pie. La Catedral de Zacatecas, el Teatro Calderón y los callejones por la noche hacen de Zacatecas un destino ideal para una escapada romántica. 
  
 – Mérida, Yucatán 
  
 Además de ser la puerta de entrada al mundo maya, la capital de Yucatán tiene su propio encanto: ya sea que decidas dar un paseo a pie por el centro histórico y conocer el Palacio Municipal, la Casa Montejo y la Plaza Principal, o bien a través de una calesa (carroza típica jalada por caballos) con la guía del conductor, Mérida tiene un clima subtropical y exuberante vegetación que lo hacen un sitio inmejorable para conocer México. 
  
 
 – Guanajuato, Guanajuato  
 Guanajuato se distingue por la magia de sus callejones , que dan testimonio de los siglos de historia que se respira en cada rincón. Algunos solamente pueden recorrerse a pie y cada uno tiene algo distinto que contar, como el Callejón del Beso o el de la Constancia. Misterio aparte se esconde bajo las calles subterráneas. Es indispensable visitar la Universidad, el Teatro Juárez y el Monumento al Pípila, que tiene una vista inmejorable de la ciudad. 
  
 – San Miguel de Allende, Guanajuato 
  
 Enclavada en el Bajío, San Miguel de Allende posee un encanto colonial que la ha hecho merecedora del reconocimiento como la mejor ciudad turística del mundo en distintas ocasiones, debido a su gran oferta cultural, la riqueza arquitectónica que posee y la belleza colonial que guarda entre sus calles, donde abundan los pequeños talleres y galerías de arte junto con cafeterías y bares que hacen de San Miguel un destino obligado dentro del territorio nacional. 
  
 – Santiago de Querétaro, Querétaro 
   
 Querétaro es una ciudad cosmopolita que ha presentado un veloz crecimiento en servicios e infraestructura reciente, pero conserva la calma de una ciudad de provincia. El centro histórico comprende un vasto espacio de calles, mercados y plazas que albergan desde el inicio de la lucha de independencia de la nación. Los arcos que atraviesan la ciudad son un referente del estado, además del Cerro de las Campanas, lugar donde se puso fin al segundo Imperio Mexicano, uno de los episodios más álgidos en la historia de México. 
  
 
 – Puerto Vallarta, Jalisco 
  
 Rodeada por la Bahía de Banderas, esta joya del Pacífico combina el turismo de playa, sol y arena con un centro histórico que mantiene su esencia, bordeado por el malecón que durante el día reúne a turistas a ver como rompen las olas con el ocaso de fondo y por las noches se convierte en el centro neurálgico de diversión y fiesta en Vallarta. El paseo en barco y buceo por las islas Marietas, el avistamiento de ballenas grises y las aventuras ecoturísticas en la selva son actividades que no pueden faltar en tu visita al puerto. 
   
 – San Cristobal de las Casas, Chiapas  
 Una ciudad colonial en el sureste mexicano con una intensa oferta cultural producto de la diversidad étnica que alberga. Al igual que en Oaxaca, la mejor forma de empaparse de sus colores y sabores es a través de sus mercados y calles, especialmente el Mercado Artesanal, el de dulces y artesanías y el andador turístico que recorre las principales atracciones arquitectónicas de la ciudad, como el Templo y exconvento de Santo Domingo y el Museo Na-Bolom. 
  
 – Barrancas del Cobre, Chihuahua 
  
 Este accidente geográfico dos veces más profundo que el Gran Cañón de Colorado se ubica en la Sierra Tarahumara y ofrece una amplia gama de actividades desde deportes de montaña como rappel, ciclismo o rafting, hasta recorridos por las misiones y las bellas cascadas que guarda entre sus precipicios y laderas. El tren Chihuahua-Pacífico atraviesa las barrancas a través de túneles, puentes y desfiladeros en una experiencia inolvidable a través de su caprichosa geografía.   
 
 – Playa del Carmen, Quintana Roo 
  
 El destino turístico de México por excelencia es visitado por una enorme cantidad de turistas europeos y norteamericanos en todas las épocas del año. Las paradisíacas y cristalinas playas del Mar Caribe, catalogadas como las mejores del mundo, bañan toda la costa de la Riviera Maya y a unos cuantos kilómetros se ubican importantes ciudades mayas como Tulúm y Cobá, así como distintos parques ecoturísticos como Xel-Há o Xcaret. 
  
 – Sin duda alguna, la oferta turística que nos ofrece México en términos culturales, artísticos y naturales no tiene comparación con algún otro sitio en el mundo; es resultado de un proceso histórico único que conllevó la conquista y resistencia de los pueblos prehispánicos por la conservación de sus tradiciones, de una ubicación geográfica privilegiada que dota al país de diversos climas y altitudes, que genera la megadiversidad biológica con la que cuenta y una condición contemporánea que hace de este país un sitio que es imprescindible conocer. 
 *** Te puede interesar: 
 10 viajes baratos que puedes hacer con tus amigos en 2016 
 Viajes que cambiarán tu vida en un fin de semana