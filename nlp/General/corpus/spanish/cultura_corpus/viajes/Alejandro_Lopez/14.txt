60 cosas que tienes que hacer mientras viajas

 
 Viajar es mucho más que tomar tu equipaje y partir hacia tu destino. Si conoces la diferencia entre un turista y un viajero , sabrás que explorar el mundo es una lección de vida. Conocerás culturas totalmente distintas a la tuya y dependiendo de tu actitud hacia lo nuevo y lo desconocido, el viaje será una increíble aventura para recordar o bien, un cúmulo de momentos incómodos donde te sentiste fuera de tu lugar en el mundo. 
 La clave para viajar como nunca antes está en tomar una actitud real, humilde, positiva y honesta. Nunca sabrás lo que un viaje te depara, por más que intentes conocer sobre tu destino y los sitios que visitarás. Puede que cada uno de tus complejos, esos que todos arrastramos según la sociedad en que crecimos, se derrumben cuando conozcas personas, actitudes, formas de pensar y actividades que ni siquiera imaginabas que existían, eso significa un crecimiento personal y una mejor comprensión del mundo en que habitas. Además, requieres de una cara siempre positiva ante cualquier eventualidad que se pueda presentar. Piensa que cada opción, cada plan que se escapa de tus manos, es una aventura más que puede resolverse de la manera más impensada: muchas veces las mejores historias para contar son aquellas que sólo ocurren y que no se tenían en los planes. Si estás planificando tu escapada de verano y pretendes que sean las mejores vacaciones de tu vida, trata de cumplir estas 60 cosas que tienes que hacer mientras viajas: 
 1. Coleccionar algún objeto relacionado a tus viajes. 
   
 – 2. Escribir a tus seres queridos. 
 – 3. Estar a punto de perder un avión o tren y tomarlo de último momento. 
 – 4. Perder un tren o un avión. 
 – 5.Recordar con precisión la dirección del hotel o departamento donde te alojaste en cada viaje. 
 – 6. Guardar los boletos de entradas a espectáculos, medios de transporte y atracciones turísticas. 
  
 – 7. Probar la comida típica por más extraña que sea. 
 – 8. Tener un viaje en el que todo sale mal. 
 – 9. Perder alguna maleta y tener que vestir igual o comprar ropa en el destino. 
 – 10. Pasar la noche en una terminal de transporte o viajando. 
 – 11. Conocer amigos y contactarse frecuentemente. 
 – 12. Conocer a gente de otros países y platicar con ellos. 
  
 – 13. Querer comunicarte en el idioma local aunque no entiendas nada. 
 – 14. Llegar a tu habitación y preparar con mapa, folletos e Internet lo que harás los próximos días. 
 – 15. Dormir en un parque, restaurante o pasar la noche viajando porque el presupuesto no es suficiente. 
 – 16. Viajar en primera clase o tener un beneficio extra en el hotel por error o porque le agradaste al responsable. 
 – 17. Encontrar a alguien conocido a miles de kilómetros de casa y pensar en lo pequeño que es el mundo. 
 – 18. Caminar sin rumbo fijo y asombrarte por lo distinto que es a tu lugar de origen. 
  
 – 19. Sentarte en una banca a ver cómo es la vida realmente en el destino al que llegaste. 
 – 20. Conversar con otros turistas sobre las aventuras y consejos del lugar. 
 – 21. Llevar parte del desayuno que daban en el lugar donde te alojas para aguantar el hambre por la tarde. 
 – 22. Mirar un mapa y ver qué tan lejos estás de tu país y pensar en lo que acontece en éste en ese momento. 
 – 23. Encontrarte con alguna celebridad en una atracción turística, aeropuerto o terminal. 
 – 24. Coleccionar pasaportes con sellos de todos los países donde has estado. 
  
 – 25. Mandarle una postal a tu familia y que tú llegues antes que ella. 
 – 26. Unirte a una visita guiada que está en tu idioma sin pagar por ella. 
 – 27. Alquilar un auto y recibir uno mejor por el mismo precio. 
 – 28. Tirar ropa vieja al paso porque lo que traes ya no cabe en tu maleta. 
 – 29. Crear un vínculo especial entre personas locales y no querer volver a la realidad. 
 – 30. Unirte a una fiesta local y conocer nuevas tradiciones. 
  
 – 31. Sentir las ansias de llegar mientras vas en el transporte. 
 – 32. Imaginar desde mucho antes cómo va a ser el viaje y que al final sea algo completamente distinto pero genial. 
 – 33. Descubrir que sacaste las peores fotos y en realidad tu mejor recuerdo está en tu mente. 
 – 34. Comprar recuerdos y pensar que son perfectos. 
 – 35. Tener una situación graciosa con alguna persona local. 
 – 36. Pensar que tu próximo viaje será mejor que el anterior y cumplirlo siempre. 
  
 – 37. Regresar a casa con el acento o la jerga del sitio donde estuviste. 
 – 38. Ver los videos que tomaste en casa y pensar que perdiste tiempo valioso grabando en vez de disfrutar del momento. 
 – 39. Luchar contra el sueño por un par de días por el jet lag. 
 – 40. Olvidarse de las redes sociales y el Internet por días. 
 – 41. Aprender que hay tantas formas de pensar como personas y todas deben ser respetadas. 
 – 42. Conectarte con la naturaleza y darte cuenta de que mantiene el equilibrio perfecto en el universo. 
  
 – 43. Descubrir que tu país tiene la mayoría de frutas y verduras que los demás no casi todo el año. 
 – 44. Preguntar a alguien en el idioma local y descubrir que habla perfectamente el español. 
 – 45. Estar en un sitio donde nadie te conoce y pensar que podrías iniciar una nueva vida ahí. 
 – 46. Llorar de la emoción al conocer un sitio nuevo. 
 – 47. Darte cuenta de que tu mejor recuerdo no es material, sino las sensaciones que guardas en tu mente. 
 – 48. Mezclarte entre los locales y diferenciarte de los turistas. 
  
 – 49. Adoptar hábitos alimenticios o sociales del lugar que visitas. 
 – 50. Cambiar tu pensamiento cerrado y prejuicioso sobre otras nacionalidades y descubrir personas tan reales como tú. 
 – 51. Comprender que ningún sitio es perfecto y que en todas las sociedades existen los problemas. 
 – 52. Jurar que lo que pasa en un sitio se quedará ahí por siempre. 
 – 53. Admirarte por la belleza natural de un rincón del planeta. 
 – 54. Escribir en un diario cada una de tus vivencias para no olvidarlas. 
  
 – 55. Conocer las grandes ciudades que protagonizan películas e historias. 
 – 56. Descubrir que hay pequeños pueblos mágicos alejados del turismo y el bullicio. 
 – 57. Interesarte por la cultura del país donde te encuentras. 
 – 58. Tomarte fotografías con todas las nuevas personas que conociste en el viaje. 
 – 59. Leer una historia que se desarrolle en el sitio en el que te encuentras. 
 – 60. No querer irte y convencerte de que no hay nada como viajar por el mundo. 
  
 – Si estás mentalmente preparado pero requieres de algo más para disfrutar tu aventura al máximo, lee los mejores consejos de grandes viajeros que necesitas conocer  para tener un verano inolvidable. ¿Aún no tienes clara la forma de pasar de turista a viajero? No te pierdas de las cosas que tienes que hacer para no viajar como turista y cambia radicalmente la actitud que tienes mientras descubres nuevos lugares.