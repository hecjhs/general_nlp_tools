Cosas que tienes que hacer en el D.F. cuando eres joven

 La ciudad te espera. En medio de todo su caos, los secretos de esta maravillosa metrópoli aguardan a ser descubiertos por quienes superen la densa capa de tráfico y contaminación. A pesar de los múltiples estilos de vida y modos de entender la ciudad, existen ciertos elementos que unen a todo un pueblo. Se trata de una serie de actividades que todo defeño debe de haber hecho al menos una vez en su vida, sin importar la zona en la que viva, el tipo de transporte use y qué tipo de tacos coma. Además, por el carácter de estas aventuras se prestan a vivirlas durante tu juventud. ¿Cuántas de ellas has hecho ya? ¿Cuándo harás la siguiente? 
 Ir a un concierto en el Zócalo 
 Sea Juan Gabriel, Chayanne, Café Tacvba o Litte Jesus, debes de ser parte de esa masa de gente que salta y corea al ritmo de la música. Qué mejor oportunidad de disfrutar de la ciudad de los palacios que en el epicentro de su caos. 
  – 
 Meditar en el espacio escultórico de CU 
 Disfruta de la quietud y calma del espacio escultórico construido por algunos de los más grandes arquitectos de la época contemporánea. Recorre el camino de la serpiente y tómate fotografías en cada gran escultura. Aprovecha el silencio, medita y repiensa tu vida mientras disfrutas de una burbuja de paz en el caos de la ciudad, y cuídate de las serpientes. 
  – 
 Pasear en las lanchas de Chapultepec 
 Sea con tus mejores amigos o con tu pareja, no puedes evitar pasear en las lanchas del lago construido por Porfirio Díaz. Remos o pedales, será la única decisión difícil que deberás hacer, pues una vez a bordo, recorrerás el lago mientras disfrutas de la vista del bosque y del castillo de chapultepec en todo su esplendor. No olvides llevar un sombrero y unos lentes pues es una experiencia agotadora. 
  – 
 Pasar horas en la Cineteca 
 La Cineteca es uno de esos lugares que debes descubrir en tu juventud, y en el cual podrás pasar horas disfrutando de su cartelera. Puedes irte de pinta y aprovechar los miércoles de estudiante, pasar de sala en sala mientras aprecias tal o cual ciclo de películas. Aprecia los grandes clásicos, las nuevas propuestas y los cineastas emergentes. Y si te cansas de la oscuridad, las butacas y tus signos de claustrofobia se alteran, siempre puedes aprovechar la sala al aire libre. 
  – 
 Navegar Xochimilco en trajinera 
 Este es un plan perfecto para disfrutar de la compañía de tus amigos y familiares, pues durante un par de horas podrás navegar por los canales de la joya natural del sur de la ciudad. Mientras tanto, disfruta de la bebidas, el mariachi y las botanas típicas. Procura agarrarte bien de la trajinera pues no querrás terminar en el canal. No olvides pedirle al lanchero que te lleve a conocer la misteriosa casa de las muñecas. Las quesadillas y los antojitos a la salida son un excelente cierre. 
  – 
 Visitar la Feria de Chapultepec 
 Sí, gracias a su accesible precio de entrada, la feria te dará múltiples razones para divertirte un rato en el corazón de la capital. Recorre cada atracción de la feria, aunque llueva, truene o relampagué, pues siempre resultará una experiencia muy divertida subirte a la montaña rusa y casi tocar los coches que circulan por Periférico. Vive la adrenalina mientras tus pulmones se llenan de aire proveniente del hermoso Bosque de Chapultepec. 
  – 
 Ir a un clásico capitalino de futbol 
 Tú eliges entre las tres posibilidades de clásicos que la capital ofrece, aunque siempre resultará increíble disfrutar del clásico en el gran monumento del fútbol mexicano: el Estadio Azteca. Empápate de la emoción futbolera, disfruta de la cerveza, las botanas y prepárate para gritar un gol que retumbará en cada grada del coloso de Santa Úrsula. ¿No le vas a ningún equipo de la capital? Es lo de menos, el fútbol es sólo un pretexto para vivir la pasión que enciende millones de corazones cada fin de semana. 
  – 
 Observar la ciudad desde un mirador 
 Sea la Torre Latino o el Monumento a la Revolución, esta ciudad se presta para admirarse desde las alturas. Propónte disfrutar de la vista de palacios y edificios modernos en un día despejado para que veas cómo se pierde la ciudad en el horizonte. Esta opción es ideal para una cita, y resultará una mejor opción si conoces los edificios más distintivos de la ciudad. 
  – 
 Maravíllate en La Lagunilla 
 Tienes que llegar en Metro o caminando desde el Zócalo, recorrer el mercado, los puestos, el ambulantaje y notarás cuán difícil es no comprar nada en sus pasillos. Más allá de cuestionar la presencia de piratería y la fayuca, este lugar tiene una gran importancia histórica en la ciudad, además que ofrece un gran caleidoscopio a la sociedad mexicana: tradiciones, gastronomía, lenguaje, intereses y estilos de vida. 
  – 
 Recorrer la ciudad en bicicleta 
  
 A pesar del peligro que la ciudad tiene para los ciclistas, ello no debe detenerte para recorrer las principales calles de la ciudad en bicicleta. Aprovecha los días que la ciudad cierra las calles para el paso de los ciclistas, la oferta de Ecobici, y hasta la oportunidad de viajar en metro con tus dos ruedas y aprecia la ciudad desde una nueva perspectiva. Tú eliges si Reforma, Insurgentes, la Roma, Condesa, Polanco, la del Valle y hasta Coyoacán. Prepara una gran kit para llevar en tu mochila y sobretodo piensa que sin importar cuán lejos logres llegar, eventualmente deberás volver sobre tus pedaleos. 
 – Te puede interesar: 10 lugares que tienes que visitar en México mientras eres joven