5 destinos de ecoturismo cerca del D.F.

 “Mantén tu amor hacia la naturaleza, porque es la verdadera forma de entender el arte más y más”. Vincent Van Gogh  En un mundo de cotidianidad, caos y emociones a flor de piel, la ciudad se vuelve agobiante. Entre las calles de la vorágine citadina, enfrascado en pleitos interminables y responsabilidades, el cuerpo pierde su energía. La única esperanza reside en la oportunidad de reencontrarse con nuestros principios interiores, con aquello que nos hace humanos y sensibles: la naturaleza. Los sonidos de la vida salvaje, olores que nos recuerden a nuestra tierra y colores llenos de vida, crean una atmósfera perfecta para recargar el cuerpo de energía y limpiar la mente.México, una de las joyas del mundo, ofrece un sinfín de rincones que esperan ser descubiertos en ambientes de aventura, adrenalina y mucha naturaleza. En su amplia diversidad, nuestro país cuenta con destinos que llaman a los espíritus que necesitan escapar de la ciudad y volverse uno con la vida silvestre. Para aquellas almas que requieren experiencias extremas, momentos de relajación y ganas de recorrer este maravilloso país, compartimos 5 destinos de ecoturismo cerca del D.F.Agua Blanca, Michoacán
A tan sólo dos horas del D.F., y con la posibilidad de llegar en autobús, este resort ofrece sus 30 hectáreas para reconectarte con la naturaleza. Con una ubicación idónea, pues en ella conviven tres zonas climáticas distintas, es una gran oportunidad para olvidarte de la ciudad. A menos de 50 pasos de las rústicas habitaciones, podrás acceder a las aguas termominerales que salen a placenteros 32ºC e internarte en un temazcal. Explora la hermosa cascada ‘Velo de Novia’, las pequeñas grutas naturales, descubre las huertas que alimentan el restaurante e intérnate en el río Tuxpan. Si durante tu estancia deseas explorar la región, cuentas con la zona arqueológica de San Felipe los Alzati, el pueblo de Jungapeo y los recintos sagrados de la mariposa monarca. Conoce más de Agua Blanca y de las experiencias que ofrece en su página oficial o en Facebook y Twitter.Si al reservar mencionas el código “CulturaColectiva1″ recibirás un tratamiento de SPA de cortesía.Cualquier duda, comentario, sugerencia o reservación puedes hacerlo al siguiente correo y teléfono:[email protected]/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */01 715 15 70056

http://culturacolectiva.com/wp-content/uploads/2015/09/aguas-termales-michoacan.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/hotel-agua-blanca-michoacan.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/rappel-cascada-agua-blanca.jpg


Jalcomulco, Veracruz
Esta es una gran oportunidad para disfrutar un fin de semana distinto a todos los demás. A cinco horas de distancia, podrás hospedarte en cómodas cabañas desde donde parten excursiones al río Actopan. Ahí, en la cuna del rafting en México, vivirás la adrenalina a bordo de una balsa mientras desciendes de los mejores rápidos en el país. En los momentos en que no luches por mantenerte a flote o evitar que el agua te nuble la visión, podrás admirar la belleza de la naturaleza que cuelga de los acantilados y de las aves que acompañan a los turistas. El lugar también se presta a vivir el rappel, la tirolesa y el senderismo.

http://culturacolectiva.com/wp-content/uploads/2015/09/jalcomulco-veracruz.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/jalcomulco-veracruz-rafting.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/deportes-extremos-mexico.jpg


Huasteca Potosina, San Luis Potosí
Paisajes, cuevas, naturaleza, sitios arqueológicos y un hermoso río. En medio de la majestuosa vegetación, se esconden siete cascadas que forman impresionantes estanques, ideales para la práctica del kayak, senderismo, buceo, salto de cascada y rafting. Por si fuera poco, el destino te permite conocer el Sótano de las Golondrinas, con profundidad de 512 metros y hogar de cientos de miles de aves. ¿Te atreves?

http://culturacolectiva.com/wp-content/uploads/2015/09/huasteca-potosina.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/sotano-de-las-golondrinas.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/huasteca-potosina-ecoturismo.jpg


Parque Nacional Izta-Popo
Con las medidas de seguridad establecidas por los senderistas en este lugar, el Parque Izta-Popo, recupera el gusto de los capitalinos que buscan una experiencia para llevar su cuerpo al límite. Con la posibilidad de tener momentos de reflexión e introspección en la inmensidad de la montaña, deberás poner a prueba tu resistencia física y mental para superar los obstáculos naturales. Puedes quedarte en Puebla o Cholula y subir desde temprana hora a la montaña, y decidir si quedarte en el refugio y llegar a la cima al día siguiente, o descender el mismo día y disfrutar de una deliciosas quesadillas en el Paso de Cortés.

http://culturacolectiva.com/wp-content/uploads/2015/09/izta-popo-parque.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/izta-popo.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/paisaje-popo-izta.jpg


Peña de Bernal
México debe estar orgulloso de tener el tercer monolito más alto del mundo a escasas horas de la Ciudad de México. El pueblo mágico de Peña será la base para definir tu estrategia a la hora de escalar; aquí podrás obtener todo el equipo necesario para escalar y el guía que hará el ascenso posible. La escalada, desafiante y emocionante, te permite disfrutar de bellos paisajes del estado de Querétaro, y cuando estés listo para el descenso, podrás hacerlo en rappel. El ciclo perfecto de adrenalina.  http://culturacolectiva.com/wp-content/uploads/2015/09/rappel-mexico.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/pena-de-bernal.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/bernal-ecoturismo-queretaro.jpg Te puede interesar: 10 lugares que tienes que visitar en México mientras eres joven