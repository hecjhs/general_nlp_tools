Museos de los que siempre has oído hablar pero no sabes dónde están

 
 Hemos escuchado en repetidas ocasiones sobre la importancia de los museos : que son fundamentales para la difusión de la cultura, que resguardan piezas antiguas de un valor incalculable y que ayudan a la conservación del pasado. A pesar de que somos conscientes de todos estos puntos, pocas veces pensamos sobre el papel central que juega un museo en la edificación de una nacionalidad. 
 Hasta los inicios del siglo XIX, la mayoría de los gobernantes coloniales del sudeste de Asia desestimaron los monumentos antiguos de las civilizaciones que ellos habían sometido, este mismo hecho sucedió en la Nueva España cuando se pretendió borrar el pasado mexica. Con el paso del tiempo, estas tendencias cambiaron, comenzaron a surgir voces que buscaron una identidad propia y llamaron a la conservación de los restos arqueológicos. Los pueblos antiguos se convirtieron en una muestra de la gloria que precedió a las fuerzas colonialistas, es por ello que surgieron autores como Francisco Javier Clavijero que escribieron sobre el pasado prehispánico. 
 Los museos, en este esfuerzo por encontrar una identidad, surgen como sitios sagrados que resguardan los más preciados tesoros de una creciente nación. Los objetos que antes no tenían ningún valor se convierten en pilares que sostienen a la nacionalidad; es en este punto cuando una piedra que antes representaba la herejía y el paganismo se convierte en un símbolo digno de estar en cada moneda de nuestros bolsillos. 
 Es por ello que nunca debe de verse la colección de un museo sin conocer su contexto. Cada pieza y obra es una muestra de los intereses que esa sociedad en particular le da al arte y a su nacionalidad.  
 – Belvedere, Viena 
  
 Entre 1714 y 1723 el príncipe Eugenio de Saboya mandó a construir el Palacio Belvedere para celebrar la victoria austriaca sobre los turcos. El recinto en primera instancia fue pensado para celebrar las fiestas de la alta sociedad de la época. Desde el 2007 este recinto alberga una de las galerías que no le debe nada a las grandes colecciones europeas. Su colección presenta toda la historia del arte austriaco, desde cuadros medievales hasta “El beso”, de Gustav Klimt. 
  
 – Inhotim, Brasil 
  
 El Centro de Arte Contemporáneo Inhotim es un museo único en el mundo. El magnate Bernardo Paz edificó un paraíso que se extiende por 140 hectáreas con más de 80 instalaciones artísticas, 22 galerías y más de 500 piezas. Inaugurado en el 2006, el Inhotim es un proyecto que rompe con la tradicional oscura y encerrada sala de museo para ofrecer un espacio inclusivo que invita a formar parte del arte. 
  
 – Lenbachhaus, Múnich 
  
 Franz von Lenbach fue un pintor alemán famoso por sus retratos de estilo realista. En 1924, su viuda le ofreció venderle a la Ciudad de Múnich la casa del artista junto con todas sus obras. Es en esta propiedad donde se encuentran una de las mejores colecciones alemanas, sus muros resguardan decenas de obras de Wassily Kandinsky y por si fuera poco, da una visión muy profunda al arte contemporáneo con artistas que van desde Franz Ackermann hasta Andy Warhol. 
  
 – Museo Picasso, Barcelona 
  
 Este museo nació gracias a la iniciativa de Jaime Sabartés, amigo y secretario de Pablo Picasso desde 1935, cuando donó su colección personal, con la que inició el proyecto. Un lugar clave para toda persona interesada en el artista español, el Museo Picasso resguarda una colección de 4,251 trabajos que son fundamentales para conocer los años de formación del pintor malagueño. 
  
 – Naoshima, Japón 
  
 Naoshima es una isla localizada en el mar interior de Seto, Japón y es hogar de más que quince recintos dedicados al arte contemporáneo. El proyecto nació gracias a Tetsuhiko Fukutake y Chikatsugu Miyake, que buscaron crear un área cultural y educacional en donde personas de todo el mundo pudieran reunirse. Uno de los muchos recintos que conforman a este conjunto es el museo Chichu Art, que alberga instalaciones de James Turrell y Walter De Maria, así como pinturas de Claude Monet.  
  
 – Museo Peabody de Arqueología y Etnología, Massachusetts 
  
 Fundado en 1866 por George Peabody, este recinto es uno de los museos dedicados a la antropología más antiguos del mundo. Su colección es producto de las multitudinarias expediciones arqueológicas financiadas por la Universidad de Harvard. Es por esta razón que alberga miles de piezas de culturas mesoamericanas e incas, entre las que se encuentran los tesoros rescatados del fondo del cenote de Chichén Itzá. 
  
 – Museo de l’Orangerie, París 
    L’Orangerie recibe su nombre  debido a que el edificio fue construido como un cobertizo para proteger a los naranjos durante el invierno. Actualmente se encuentra en la Plaza de la Concordia y funge como una galería de arte que alberga pinturas impresionistas y de arte moderno. La colección incluye la serie Nymphéas de Claude Monet, que fueron pintadas en el jardín del artista en Giverny y fueron donadas por el Estado francés. 
  
 En el mundo siempre habrá lugares llenos de misterio y de belleza , estos museos van más allá de estos adjetivos al mostrarnos las identidades y gustos que determinan a cada nación en la que se encuentran. 
 *** Te puede interesar: 
 Los 10 mejores museos de arte contemporáneo en el mundo 
 Museos más extraños del mundo