7 lugares en la Ciudad de México donde puedes perforarte con estilo

  Alterar el cuerpo como una forma de decoración es bastante común y se ha convertido en una tradición en todas las culturas del mundo. Las causas han cambiado pero el amor a iluminar nuestro cuerpo con otros pigmentos y adornarlo con otros materiales sigue presente. Una impresión en el otro, protegerse de las inclemencias del tiempo o rituales, fueron los motivos perfectos para alterar el cuerpo.  
Ahora somos nosotros los que adornamos nuestro cuerpo con tatuajes y perforaciones que, según creemos, nos hacen vernos mejor que antes. Los labios, las cejas, las orejas, pezones y ombligo sufren modificaciones para hacernos sentir más bellos. Duele pero nos gusta, nos encanta pero en ocasiones nos da miedo…. como el amor.

Un estudio de tatuaje adecuado para realizar estos procedimientos, debería tener medidas de higiene bien claras, para que después, la pus no aparezca en nuestras perforaciones y utilicemos cientos de remedios caseros para quitarnos el dolor que nosotros mismos nos causamos. Recuerda, más vale invertir un poco más y asegurarte que no te contagiarás de alguna enfermedad.    Wakantanka



 Wakantanka es el primer estudio de Body Piercing en México. En su currículum tienen decenas de ponencias y seminarios sobre modificaciones corporales y en su página de Internet puedes esclarecer todas tus dudas e inquietudes sobre perforarte o tatuarte. Los encargados de las perforaciones son Danny Yerna de Bélgica; Memo quien ha colaborado por más de 9 años en esta tienda y participa constantemente en cursos y seminarios sobre perforaciones y cuidados básicos y es considerado uno de los mejores perforadores mexicanos; y Sioux de 23 años  Nuevo León 284, Col. Condesa. Teléfono:  5264 1110 / (55) 1998 1989     
Rock Shop



En Rock Shop, Victor es el genio detrás de todas las perforaciones. Algunos diseños clásicos, otros más arriesgados y también expansiones, es posible encontrarlas en este lugar. Si antes quieres ver su catálogo, puedes hacerlo en su página de Internet y así, puedes ir seguro de lo que deseas.  Insurgentes Sur 363, Condesa. Teléfono: 1054 6243





Kaustika



Kaustika es el lugar más popular para todos los residentes de Coapa. Chuky, Israel y Oskar se encargan de hacer las perforaciones y siempre encontrarás todo limpio y desinfectado para que nada malo ocurra.  
Av. Canal de Miramontes 118. Teléfono: 5679 1852

  

Infierno Tatuajes



Aunque su especialidad sin duda son los tatuajes, los piercings ahora son una realidad en Infierno Tatuajes. Habían dejado de hacerlo por algún tiempo, pero ahora, de martes a sábado de 12:00 a 20:oo horas, podrás perforar la parte de tu cuerpo que decidas.    


Av. Pedro Henríquez Ureña 27. Teléfono:



 5554 0858







    Estudio 184
    El la calle Colima y el número 184, puedes encontrar un edificio de ventanales que en su interior tiene a grandes artistas de la modificación corporal. Se ha convertido en uno de los mejores estudios de tatuajes pero inició la aventura con sólo tres personas que se dedicaban a hacer perforaciones, por lo que son su especialidad. Tienes que hacer una cita previa para poder asistir.   Colima 184, Roma Norte. 



  
Ganesh    Ganesh es uno de los sitios más relevantes en la Ciudad de México. Con 14 años de tradición tiene artistas de la tinta y las perforaciones sumamente reconocidos en su estudio. Se ha caracterizado por ser un lugar sumamente higiénico, con calidad y sus tatuajes y perforaciones, son bastante bellas. Hay diferentes áreas que se especializan a todo lo referente en cierta actividad, como perforaciones, tatuajes o de desinfectado y esterilización.   Florencia #70 Dep.1 Col. Juárez. Teléfono:  5533-2046


   
Coyoacán estudio

 Es un sitio relativamente nuevo y con gran éxito en la zona. Puedes encontrar precios bastante accesibles y, tanto sus tatuajes como perforaciones, tienen gran higiene.   Av. Centenario #27- A, Coyoacán. Teléfono: 6550-5649     Si ya conoces todo sobre perforaciones, tal vez puedas recorrer los mejores estudios de tatuaje de la ciudad o conocer a  los tatuadores más importantes.  ***  Te puede interesar:  Las 10 mejores tatuadoras del mundo
 10 perforaciones que te pueden inspirar para tener un look más sexy