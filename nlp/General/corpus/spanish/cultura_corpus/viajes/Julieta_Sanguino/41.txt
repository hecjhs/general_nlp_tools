Lugares que todo fotógrafo debe visitar

 El sueño de cualquier fotógrafo es conocer los lugares más fabulosos del mundo. En la actualidad, cualquiera puede publicar las maravillosas fotos que toma a través de las redes sociales: las fotos de una borrachera increíble, las de su mascota o con sus amigos. Todas éstas pueden subirse a redes como lo es Instagram. Sin embargo, la vida cotidiana no tiene comparación con un fabuloso viaje alrededor del mundo. 
 Todos deberíamos realizar un viaje que cambie nuestra existencia. Un ejemplo podría ser el de la película La vida secreta de Walter Mitty,  quien con la idea de encontrar a su fotógrafo y amigo O’ Connell, se sumerge en una aventura por todo el mundo. O’Connell recorre los mejores paisajes alrededor del mundo para tomar fotografías increíbles, porque sólo conociendo podemos aprender a hacer mejores fotos y mejores cosas. 
 El fotógrafo debe capturar el momento ideal, aquel que muestre la grandeza de un lugar, esto con el fin de permanecer en la memoria del espectador ante cualquier circunstancia, y buscar la inmortalidad. No solamente se trata de recibir la luz adecuada sino de lograr captar la atención a través de una imagen impactante. Aquí te mostramos los mejores lugares que ayudarán a todo fotógrafo a inspirarse y retratar momentos sorprendentes. 
 Pirámides de Giza, Egipto 
    Campos de tulipanes, Holanda 
 Mount Valley, Estados Unidos 
   Marsazlokk, República de Malta 
  
 Iglesia del Salvador sobre la sangre derramada, Rusia  
 Cesky Krumlov, República Checa 
  
 Blagaj, Bosnia y Herzegovina 
  
 Procida, Italia 
   Weißgerbergasse, Alemania   El cuarto de los pavorreales en Castello di Samezzano, Italia  
 Leptis Magna, Libia 
  
 Cappadocia, Turquía 
  
 Lago Retba, Senegal 
   
 Islas Marietas, México 
   
 Monte Roraima, Venezuela 
   
 Monte Grinelll, Estados Unidos 
   
 Chan Baori, India 
    
 Socotra, Yemen 
   
 Parque Nacional Yellostone, Estados Unidos 
   Cueva de cristal, Islandia 
  
 Parque geológico de Zhangye, China   
 Machu Pichu, Perú 
  
 Taj Mahal, India  
 Alaska 
    Viti Levu, Islas Fiyi   
 Creta, Grecia 
    Cabo San Lucas, México   
 Cartagena, Colombia 
    Ushuaia, Argentina   
 Salvador, Brasil 
   
 Myanmar, Bagan