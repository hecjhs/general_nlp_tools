50 cosas que debes saber antes de viajar a Estados Unidos

 
 El siglo XX vio surgir a una potencia mundial que a diferencia de los antiguos imperios, no conquistó imponiendo su bandera en otros territorios. Estados Unidos es un país que ha llegado a posicionarse en el imaginario de miles de millones de personas gracias a su cultura. A través de series de televisión, películas, canciones, arte y mucho más, el país norteamericano se ha impuesto en la vida de muchas naciones. Esa es la razón por la que muchos desean visitar la tierra de Mickey Mouse, Woody Allen y Foo Fighters. Estados Unidos es uno de los destinos más visitados del mundo, pero eso no implica que llegar ahí sea un lecho de rosas. Estas son 50 cosas que debes saber antes de viajar a la nación de las barras y las estrellas. 
 1. Son pocos países — y en su mayoría son europeos — los que no necesitan visa para entrar a Estados Unidos. Si deseas hacer un viaje como turista o estudiante, aquí puedes saber que necesitas. 
 2. Con más de 316 millones de habitantes, es el tercer país con mayor población después de China e India. 
 3. Es un país multicultural gracias a que creció a partir de la llegada de extranjeros de todo el mundo. 
 4. Sin embargo, los nativos americanos son los grupos originarios que hoy continúan luchando por un mayor reconocimiento. 
 5. El español es hablado por cerca del 12% de la población. 
  
 6. Los Ángeles es una de las ciudades con más hispanos o latinos en el mundo. 
 7. Otra gran población en el país es la asiática, sobre todo en Nueva York. 
 8. Estados Unidos no es solamente su política, es mucho más que Donald Trump y eso lo demuestra la sociedad civil. 
 9. La comida es mundialmente famosa. De nuevo, gracias a la multiculturalidad, Estados Unidos produce mezclas internacionales que después son tendencia en todo el mundo. 
 10. Una de las especialidades es la pizza. En un día se sirven 100 acres de pizza en todo el país. 
  
 11. Es considerado el país más poderoso y por eso muchos lo consideran el más rico, pero si dentro del territorio estadounidense tienes diez dólares en tus bolsillos y no tienes deudas, te puedes considerar más rico que el 25% de su población. 
 12. Uno de cada ocho estadounidenses ha sido empleado por McDonald’s. 
 13. Los acentos son bastante marcados, por lo que no debes sorprenderte si viajas a Los Ángeles, Nueva York o Mississippi y tienes problemas para entender tres distintas entonaciones del mismo idioma. 
 14. Creadores del vertiginoso estilo de vida que se está adoptando en cada vez más países, es posible que si buscas correctamente encuentres establecimientos abiertos a cualquier hora. Bares, pizzerías, cafés, librerías y más extravagancias pueden ser visitadas a cualquier hora. 
 15. No todo está en las grandes ciudades, no te olvides de visitar sus espectaculares parques nacionales. Son 84 millones de acres en total y algunos de los más espectaculares están en Alaska, Nuevo México y claro, el parque Yellowstone que tiene mayor presencia en Wyoming. 
  
 16. Conoces Coachella, Lollapalooza y SXWX, pero Estados Unidos es uno de los países con más festivales en el mundo. Desde uno dedicado a la escultura con motosierras, pasando por la premiación del mejor contenido en Internet, hasta uno en el que toda la comida se fríe (twinkies, hamburguesas y más). 
 17. Si bien no son tan serios como los ingleses, tampoco son tan afectuosos como los latinos; no invadas su espacio personal. 
 18. En Estados Unidos las propinas son voluntarias, pero la realidad es que siempre se deja el 15% del total y cuando el servicio es extremadamente bueno, se puede llegar incluso hasta el 25%. 
 19. Prepárate para conocer grandes platillos imposibles de ser consumidos por una persona, así como la más grande variedad de dulces y caramelos en el mundo.  
 20. Nueva York, Los Ángeles y Chicago son las ciudades más visitadas de la nación. 
  
 21. Por eso no debes perderte otros grandes destinos como Nueva Orleans, Filadelfia o Santa Mónica. 
 22. Debes planear tu viaje muy bien. A finales de enero (Super Bowl) y principios de julio (día de la independencia de Estados Unidos) los vuelos pueden ser bastante costosos. 
 23. A diferencia de otros países, las profesiones de policía y bombero son muy respetadas; los últimos están constituidos por un 69% de voluntarios. 
 24. El 25% de los estadounidenses creen en la reencarnación y un porcentaje menor cree que la sociedad está al servicio de los reptilianos . 
 25. La mítica Ruta 66 hoy ya no es tan transitada, pero sin duda es parte esencial de los roadtrips que puedes hacer en Estados Unidos. 
  
 26. Lamentablemente, el país mantiene muchos problemas de racismo y xenofobia, por lo que siempre ten a la mano tus papeles o copias de ellos. 
 27. Prepárate para sentir frustración, pues Estados Unidos, a comparación de la mayoría de los países, se niega a adoptar el sistema métrico al que estamos acostumbrados, por lo que todo se mide en millas, pies y yardas. 
 28. El Super Bowl supone uno de los eventos deportivos más importantes a nivel mundial, pero el beisbol representa al país al ser el deporte oficial. 
 29. Aunque para muchos el Sueño Americano ni siquiera existió y a pesar de la crisis financiera de 2008, muchos estadounidenses aún creen en la primicia de que si te esfuerzas, lograrás todo lo que quieras. 
 30. Uno de cada tres estadounidenses padece de obesidad. 
  
 31. Fumar es la causa de muerte entre uno de cada cinco estadounidenses. 
 32. Los hábitos de comida son rigurosos, las cenas comienzan temprano y si es un restaurante que requiere reservación, no supongas que podrás llegar sin previo aviso y conseguir mesa. 
 33. Los carteristas son comunes en las grandes ciudades, ten siempre seguro tu dinero e intenta no cargar con mucho efectivo. 
 34. México y otros países de Latinoamérica son expertos en la impuntualidad, no cometas ese error en un país anglosajón. 
 35. Si vas a Nueva York no olvides zapatos cómodos. A diferencia de la mayoría de las ciudades, en ésta caminar es esencial. 
 36. En la Gran Manzana siempre lleva una cámara lista, las curiosidades y lo inesperado puede suceder de un momento a otro. 
 37. Visita los lugares populares entre semana. Puede que estén un poco llenos, pero no se compararán con el ajetreo del fin de semana. 
 38. Los desayunos suelen comenzar a las 7 de la mañana y la gente come rápido. 
 39. El alojamiento puede ser costoso, intenta buscar hostales o quedarte con los locales antes de buscar un hotel. 
 40. El clima es diverso. Desde las calurosas Miami o Texas hasta la helada Alaska. 
  
 41. Después de los ataques del 11 de septiembre la seguridad aumentó considerablemente, por lo que los aeropuertos pueden ser una tortura. 
 42. En el centro y el sur del país encontrarás las mejores BBQs. 
 43. El Denali es la montaña más alta de Estados Unidos y América del Norte con 6168 msnm. 
 44. Estadísticamente, en Estados Unidos es más probable que cometas suicidio a que te asesine alguien más. 
 45. Hay más gente soltera que casada. 
  
 45. En ese país existen interesantes ligas y torneos oficiales muy peculiares, tales como la Liga oficial de “Piedra, papel o tijera”. 
 46. Estados Unidos no cuenta con un idioma oficial a nivel federal. 
 47. Aunque solamente el 28% de los estadounidenses leen 11 libros al año, su territorio cuenta con más bibliotecas que McDonald’s. 
 48. 60 mil bolsas de plástico se usan cada cinco segundos en el territorio estadounidense. 
 49. El plátano es la fruta más popular. 
 50. A pesar de todos los clichés, los estadounidenses pueden ser las personas más caritativas y honestas que existen, siempre dispuestas a ayudar. 
  
 Viajar a un país tan grande es una experiencia inigualable. Seguramente es imposible recorrer todos los lugares impresionantes de ese país, pero si buscas destinos que puedas visitar en menos tiempos, tal vez París u otro destino de Europa sea perfecto para ti. 
 *** Te puede interesar: 
 50 cosas que debes saber antes de visitar México 
 Las ciudades más importantes para la música en Estados Unidos 
 * Fuente: 
 Rd 
 Factslides  
 Rough guides  
 Travelers United