La guía definitiva para ser un buen turista

 
 Cada año miles de personas se reúnen en el desierto de Nevada en Estados Unidos para la celebración del Burning Man, uno de los mejores festivales del mundo. Durante días el arte y la cultura son el motor en la vida de esa festividad en la que el dinero está prohibido, todo se hace por medio de trueques y regalos. Con una política de “No dejar rastro”, se anima a los miles de asistentes que recojan toda la basura, reciclen en la medida de lo posible y más, para que al dejar el sitio parezca que nunca sucedió un evento masivo. 
  
 ¿No sería increíble que siempre pensáramos de esa forma? Tal vez estamos malacostumbrados en nuestra vida rutinaria, pero al viajar podría ser un gran lema bajo el cual regirse. El turista o viajero forma parte de la cultura que visita aunque sea temporalmente. Se transforma con las ciudades, bosques y montañas de las que disfruta; recoge algo de la esencia de cada sitio y con suerte cambia la vida de algunas personas. Si tu sueño es viajar por el mundo de la mejor manera, sigue esta guía para hacerlo con estilo y sacar lo mejor de cada lugar que visites. 
 Investiga con anticipación 
  
 La película “Yes Man” trata de un hombre que comienza a decir que sí a todo, por lo que su vida cambia de manera positiva. En una de sus aventuras va al aeropuerto y compra un boleto de avión para el vuelo más próximo. Eso es algo que muchos deberían hacer, pero sin duda si tienes planeado un viaje, investiga lo más que puedas sobre tu siguiente destino: la historia, costumbres y tradiciones. 
 – 
 Busca lugares alternativos 
  
 Existen paradas obligatorias en muchos sitios, pero también están los destinos engañosos que se venden como algo esencial y en realidad no es importante. Busca esos sitios alejados de los demás turista, aléjate de la manada y recorre con libertad las calles empedradas, busca lugares con historia que te puedan llenar de pasión y te hagan recordar el viaje por el resto de tu vida. 
 – 
 Habla con los lugareños 
  
 Un guía puede enseñarte mucho, pero si quieres captar el verdadero rostro de esa ciudad, conversa con las personas que la habitan y transforman diario. Los puestos de comida callejera siempre tienen gente que gusta hablar de todo y de nada, encuentra en su filosofía esa chispa que te hace querer conocer siempre más. 
 – 
 Respeta el lugar 
  
 Existe un código de conducta en cada lugar , pero también existen reglas universales. Cuida el ambiente. El clima no es el mismo que hace décadas debido al cambio climático y aunque se están tomando medidas avaladas por SEMARNAT como el hoy no circula, los programas de playas limpias o de transporte limpio; las acciones individuales siempre serán necesarias para salvar nuestros lugares favoritos. 
 – 
 Mira la escena local 
  
 Claro que los grandes eventos y atracciones deben estar en tu lista, pero no olvides que existe un mundo alternativo que no es mencionado en los folletos turísticos. Conciertos con bandas emergentes, exposiciones de aspirantes a grandes artistas, obras de teatro que se rigen por la pasión de los actores que buscan trascender en el mundo de la actuación y mucho más. Cada ciudad tiene un circuito de este tipo y no debes perdértelo por nada. 
 – 
 Compra local 
  
 No dejes pasar la oportunidad de consumir productos locales y artesanales. Fuera de los recuerdos sencillos que todos llevan, tal vez una playera que solamente puedes conseguir en ese lugar es justo lo que necesitas para hacerle saber a esa persona que realmente pensaste en ella durante el viaje. 
 – 
 Observa todo a tu alrededor 
  
 No caigas en una rutina, los viajes merecen ser vividos durante el mayor tiempo posible. No pases 12 horas al día dormido, descansa sólo lo suficiente para despertar con energía y proponte salir a conocer todo lo que puedas. No viajes con la mirada pegada a tu celular o al piso; que tu mirada conozca todos los rincones posibles, enamórate muchas veces al día y haz todo lo que no harías en tu lugar de origen. 
 – 
 Seguir sencillos pasos es la fórmula para no ser un huésped indeseado en un nuevo lugar. Viaja siempre consciente de las otras personas, respeta su hogar y su medio ambiente, pues al final todos vivimos en el mismo planeta y no importa dónde estemos, nuestras acciones repercuten a corto, mediano y largo plazo en todo el mundo. 
  
 *** 
 Te puede interesar: 
 Razones por las que las estaciones del año dejarán de existir 10 países que están generando acciones contra el cambio climático