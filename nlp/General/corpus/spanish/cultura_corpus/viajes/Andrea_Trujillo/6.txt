La página que te dice cuál será tu próxima aventura en Canadá

 
 Imagínate conocer uno de los lugares más hermosos de la tierra, ¿cómo luciría? Probablemente sería un lago cristalino, rodeado de árboles y montañas nevadas; un paisaje inmerso en bosques boreales, ondulantes pastizales y ríos caudalosos; aguas termales, castillos andinos, esponjosa nieve y vida salvaje. Así es la  provincia canadiense de Alberta, calificada, literalmente, como uno de los destinos más bellos del mundo. 
 Alberta mereció ese título por variadas y justas razones; no sólo está rodeado de bellezas naturales, también es un lugar con una gran diversidad étnica y con muchísimas opciones para visitar. Una provincia que mezcla lo rural y lo cosmopolita, donde puedes visitar reservas naturales, así como museos, monumentos históricos y la ruta escénica de las Rocosas Canadienses. Para dar a conocer su gran oferta, el diario “The Telegraph” y el sitio oficial de la provincia, Travel Alberta , lanzaron un quiz interactivo online, en el cual a partir de siete preguntas, pueden conocer tu perfil como viajero y te sugieren destinos en Alberta que se acomoden con tus gustos y necesidades. 
 Puedes empezar el quiz AQUÍ. 
 Así luce la página de inicio. Pincha en ‘Discover Yours’ para empezar tu aventura. 
  
 Si eres un espíritu viajero, es momento de tirar los dados y aventurarte por los rincones de Alberta, Canadá. No te preocupes por el dinero o el hospedaje, pues existen muchas opciones de intercambio en la provincia , en los que puedes prestar tus servicios a cambio de hospedaje y comida; en tus manos sólo queda el boleto de avión. ¿Qué tal suena ayudar a cuidar caballos en un rancho de la zona rural de Alberta, cocinar o construir cabañas? Nada mal, ¿eh? Si te interesa conocer este bello lugar, ingresa a este ENLACE para conocer las diferentes opciones. 
 No esperes más y deja que el teleférico te guíe hasta tu próxima aventura. Te explicamos un poco cómo funciona y puedes tomar el quiz AQUÍ . 
 
 ¿Cómo sería tu viaje ideal? Arrastra la burbuja de las opciones A, B, o C a la cabina del teleférico. 
   
 ¿Con qué cosas no puedes sobrevivir durante un viaje? 
   
 ¿Cuál sería tu medio de transporte ideal? 
   
 Después de haber pasado el día explorando, ¿cómo te gustaría pasar la noche? 
   
 ¿Qué animales te gustaría conocer durante el viaje? 
   
 ¿Cuál sería la mejor manera para relajarte? 
   
 ¿En dónde te gustaría hospedarte? 
   
 Esta es la página de resultado, que te dirá qué tipo de vacación y actividades podrías realizar de acuerdo a tus respuestas del quiz. 
  
 Mi aventura ideal en Alberta mezcla un poco de lujo con las bellezas naturales y la aventura de la provincia. Para saber más de Alberta y conocer los lugares que te sugieren según tus intereses, pincha en ‘Explore More’. 
 ¿Cuál será tu próxima aventura ? Compártenos tus resultados en los comentarios y cuéntanos cómo sería tu viaje ideal . 
 *** 
 Te puede interesar: 
 6 formas para viajar gratis por el mundo 
 Sé voluntario y viaja por el mundo 
 10 programas de voluntariado en el mundo para 2016