12 fobias causadas por el cine

 
 Hay ocasiones donde no se sabe qué fue primero “el huevo o la gallina”. Cuando se habla de situaciones psicológicas dentro del cine, ocurre lo mismo, ya que a veces es imposible saber si lo que ocurre en la ficción se desprende de la realidad, o sólo es una maquinación del director o escritor de la historia. Todo apunta a que nada que pueda ser imaginado por la mente de un sujeto, es creado por generación espontánea; es decir, cuando imaginamos una quimera, partimos del concepto, tenemos una idea de lo que es, mas no cómo es; podemos agregarle patas de araña, cabeza de vaca, cuerpo de perro, etc. 
 Al hablar de fobias, siempre podremos remitirnos a películas que o bien llevan el nombre de alguna, o películas donde personajes sufren de alguna en específico. El pensar que de la coladera salga un payaso, se lo debemos a Eso. 1.- Coulrofobia  eEs el término que se le da al miedo a los payasos . 
  
 2.- Aracnofobia El miedo desmedido que provoca el tener arañas cerca. La película con la cual muchos se dieron cuenta de que sufrían de ese temor: Aracnofobia . 
  
 3.- Selacofobia La aleta superior de un tiburón acercándose a los bañistas. La fobia a los tiburones se hizo consciente a partir de la película de Steven Spielberg de 1975: Tiburón. 4.-Pediofobia El miedo a los muñecos. Chucky  es la viva imagen de este trauma sobre los inofensivos muñecos. 
  
 5.- Somnifobia Es lo que se representa en Pesadilla  en la calle del infierno; el miedo a quedarse dormido. En la película, el asesino llega a través de los sueños a matar a la víctima. 
  6.- Claustrofobia  Alien  es la película que remite a este temor de quedarse encerrado en algún lugar o de estar en espacios reducidos. 
  
 7.- Pedofobia El miedo a los niños puede tener un origen cinematográfico en: Cementerio Maldito  o en el clásico Los niños del maíz. 
 8.- Cinofobia Stephen King aparece de nuevo con Cujo,  el perro que fue mordido por un murciélago y ataca. Tener miedo a los perros se le denomina: cinofobia. 
  
 
 9.- Ofidiofobia Las serpientes no son tus amigas si padeces de esta fobia como lo hacía el célebre: Indiana Jones. 
 10.- Tripanofobia Miedo a las agujas. La escena de la trampa llena de agujas en Saw II, te traerá malos momentos. 
  
 11.- Acrofobia Totalmente relacionada con la película Vértigo de Hitchcock. El miedo a las alturas parece ser de los más comunes. 
  
 12.- Musofobia  Willard de 1971, podría ser la génesis de la fobia hacia las ratas. 
  
 Como se pudieron dar cuenta, muchas de las fobias de las que se han hablado en este artículo se las debemos al maestro del terror: Stephen King. 
 Referencia: 13 fobias causadas por películas.  
 *** Te puede interesar 10 películas de terror que nunca verás en televisión.