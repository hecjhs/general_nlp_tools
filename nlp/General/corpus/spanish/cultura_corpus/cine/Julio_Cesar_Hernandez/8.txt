Las mejores adaptaciones de videojuegos en el cine y la literatura

 Los  videojuegos se han convertido en una plataforma ideal para contar historias de manera interactiva. El inmiscuirte en la vida de un personaje y tomar su rol dentro del juego, caminar por mundos desconocidos siguiendo pistas, resolviéndolas y, por qué no, disparando a diestra y siniestra resulta emocionante. 
 Muchos títulos de videojuegos han sido llevados a la pantalla grande , unos con adaptaciones buenas como el caso de Tomb Raider  y otras medianamente buenas como Silent Hill.  Es relevante cómo la industria del cine, ve en los juegos de video historias dignas de contarse en narrativas de dos horas y media. Lo complejo de dicho trabajo es que muchos juegos son largos y llenos de discontinuidades que difícilmente pueden ser trasladadas al cine. 
 De igual forma, hay casos extraños en que los videojuegos han inspirado a autores para escribir novelas como el caso de Splinter Cell que fue llevado a la novela dos años después de la aparición del juego. Asimismo hay títulos literarios que han sido llevados a la plataforma virtual de los juegos de video 
 Aquí una lista de las adaptaciones más relevantes y los coqueteos que las tres industrias, la cinematográfica, la literaria y la de los videojuegos han tenido. 
 Tomb Raider (2001), Simon West 
  
 La película toma el concepto del juego de video que nace en 1996 para Playstation, y que ha tenido múltiples entregas desde su creación en los ya lejanos años 90. Para muchos este título ha sido la mejor adaptación de un videojuego a la pantalla grande. La acción, los escenarios y aventuras, son dignos de las entregas del juego. Asimismo, el año en el que salió la película, Lara Croft gozaba de gran fama. Actualmente está en el mercado la última entrega de la franquicia: Rise of the Tomb Raider. 
  
 Prince of Persia: The sands of time (2010), Mike Newell 
  
 El primer juego salió en 1989, y se podía jugar en MS-DOS. Al igual que el título anterior, ha habido diversas secuelas a lo largo de los años. La película va más en la tónica del concepto, donde el mítico héroe debe salvar a la princesa. Los que lo han jugado, deben recordar que es el fin último de este juego. Muy buenos efectos especiales y mucha parafernalia como peleas de espadas, acción y mucho misterio. Es una buena película para pasar el rato. 
  
 Resident Evil  (2002), Paul W. S. Anderson 
  
 Como ocurre con las películas que están basadas en series completas de juegos de video, es que normalmente toman un poco de la historia y luego hacen un spin off  del concepto principal. En la primer película de 2002 se narra la historia de cómo la corporación Umbrella desató el caos y el origen del virus. Posteriormente, y al igual que el juego, la franquicia de la película fue creciendo hasta tener al día de hoy seis películas. En la primer entrega pudimos ver a Milla Jovovich como el personaje central. 
  
 Street Fighter  (1994), Steven E. de Souza 
  
 En realidad la película en sí no es muy buena, pero entretiene bastante y más si eres de esa generación que vio sangrar sus dedos intentando sacar los combos de Ryu . En la película podemos ver actuar a Jean-Claude Van Damme, un icono de las películas de peleas en los años 90. Le hace un homenaje a medias al juego, pero si eres fan de la serie de juegos, puedes pasar un rato agradable viéndola.  
 Silent Hill (2006), Christophe Gans 
  
 El concepto del juego está bien trabajado en la película, con la gran falla de que la tensión que este juego genera nunca se logra. El juego es una obra maestra del suspenso y del misterio, así como del horror que genera el enfrentarse con dos balas a una criatura maligna, pocos videojuegos son tan estresantes como la serie de Silent Hill . En la película lo que vale la pena es el arte que hay detrás y los saltos en la historia, ya que ocurre en un poblado que cambia totalmente de aspecto al sonar una alarma. Quizá si eres muy fanático del juego la película te haya parecido mala, pero para verla un domingo no es tan mala. 
  
 Max Payne  (2008), John Moore 
  
 El típico antihéroe, el personaje se desenvuelve más allá del bien y mal. El juego es un clásico casi de culto que vio la luz en el año 2001. El género de acción le debe mucho a este juego. La adaptación de la película es medianamente buena, lo que ocurre es que la acción siempre va sobre toda la historia. A pesar de eso, es una película entretenida con explosiones y balaceras. 
  
 Libros que se convirtieron en videojuegos 
 Tom Clancy 
  
 Tom Clancy es un reconocido autor de novelas sobre misterio y espionaje. Entre sus novelas más leídas se encuentran Rainbow Six  y La caza del Octubre Rojo . Existen varios títulos de juegos de video inspirados en la narrativa de Tom Clancy. Entre los más destacados se encuentra la serie completa de Splinter Cell,  así como la serie de Raibow Six.  Lo relevante de Splinter Cell es que no era un libro, hasta que en 2004, dos años después de la primer entrega del videojuego, el autor decidió crear la novela con el personaje del videojuego Sam Fisher. Así como los juegos, las novelas tienen una serie de siete libros. 
  
 Dune II 
  
 El caso de este juego es extraño, ya que estuvo basado primero en la película Dune de David Lynch de 1984, pero a su vez esta película está fundamentada en el libro que lleva el mismo nombre, escrito por Frank Hebert. La importancia del juego de video radica en que fue el precursor del modo de juego que pudimos ver en Age of Empires y en Warcraft, donde la estrategia lo es todo para conquistar al rival. En el terreno de la literatura, la novela de Hebert es uno de los clásicos de ciencia ficción. The Witcher Este título esta basado en una serie de libros homónimos, publicados por el autor polaco Andrzej Sapkowski, y que hablan de la historia de un hechizero. La temática perfecta para crear un juego de mundo abierto y fantasía épica, donde la imaginación de los desarrolladores estuvo a la altura de las criaturas imaginadas por Sapkowski. El primer juego de la serie se lanzó en 2007, el segundo en 2011 y la tercera entrega es en 2015. 
  Metro 2033 Este es de los casos donde el videojuego supera completamente a la novela. El juego es de gran emoción por el tema que toca, un escenario post-apocalíptico donde el desastre nuclear divide a los hombres que aún quedan con vida. El libro es lento y si lo lees después de jugar, te decepcionará mucho. Este juego es del estilo de Borderlands , donde hay que sobrevivir en un mundo alterno. 
  
 *** 
 Te puede interesar: Los videojuegos más complicados de la historia