Las mejores 999 películas de todos los tiempos según IMDb ¿Cuántas has visto?

 Una de las mentes cinematográficas más brillantes de la historia y que definió el movimiento cinematográfico de la post-guerra fue François Truffaut. Uno de los abanderados de la “Nouvelle Vague” fue un ferviente creyente de que el cine tenía la capacidad de retratar la realidad de una manera única, y a través de las secuencias y escenas de una producción, trastocar la mente de la audiencia. Múltiples frases de sus escritos, discursos y entrevistas sobreviven como referentes para quienes buscan adentrarse en la industria y ser fieles a sus ideales: 
 “No hay malas ni buenas películas, sólo buenos y malos directores” 
 “Algún día haré una película que le guste a los críticos. Cuando tenga dinero que pueda desperdiciar”. 
  
 “Un actor no es tan bueno hasta que te recuerda a un animal; caer como un gato, echarse como un perro y moverse como un zorro”. 
 “Los amantes del cine son personas muy enfermas”. 
 Y estamos locos porque somos voyeuristas, testigos y cómplices de miles de historias proyectadas a través de la magia del séptimo arte. Uno de los máximos logros como especie se ha definido por trabajos como los de Truffaut y cientos más de hombres y mujeres que entendieron cómo hablar del ser humano a través de una cámara, un proyector y una pantalla. Pero ¿cuáles son las mejores películas de todos los tiempos? 
  
 Porque sabemos que 250 películas no son suficientes, los conocedores y amantes de películas necesitan de una cantidad más grande para probar su sentido cinematográfico. Por eso, superando el anterior listado, te compartimos uno nuevo que incluye “The Shawshank Redemption”, catalogada como la mejor película de IMDb, y 999 películas más. Se trata de un listado hecho en “List Challenges” a partir del ranking definitivo de IMDb; una plataforma en el que los amantes del cine pueden saciar su curiosidad sobre sus películas favoritas, encontrar recomendaciones y críticas de lo mejor y peor del séptimo arte. 
 Puedes hacer el test en esta dirección 
  
 Un listado apabullante por el número pero que pone a prueba nuestra adicción por las películas. La lista incluye títulos como “El Padrino”, “Pulp Fiction”, El Señor de los Anillos”, “Harry Potter”, “OldBoy”, “Amores Perros”, “El último emperador”, “Irreversible”, “Star Wars”, “Goodfellas” y más. Hay películas extranjeras , animadas , un par de cintas mexicanas , algunas producciones históricas y muchas de las mejores cintas de los directores icónicos de la historia. Una oda a directores como Kubrick , Truffaut, Hitchcock , Scorsese , Tarantino , Nolan, Coppola, Kurosawa y Fellini. 
  
 No olvides compartir tus resultados en tus redes. 
 Aquí la dirección para hacer el test.  *** Te puede interesar:  
 Las mejores películas de la historia según 358 directores 
 Las mejores películas de la historia según los críticos de cine 
 Las mejores 50 películas de culto de todos los tiempos