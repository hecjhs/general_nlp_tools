Películas que debes ver si quieres ser escritor

 No seas como tantos escritores, no seas como tantos miles de personas que se llaman a sí mismos escritores, no seas soso y aburrido y pretencioso, no te consumas en tu amor propio. Las bibliotecas del mundo bostezan hasta dormirse con esa gente. No seas uno de ellos. No lo hagas. A no ser que salga de tu alma como un cohete, a no ser que quedarte quieto pudiera llevarte a la locura, al suicidio o al asesinato, no lo hagas. A no ser que el sol dentro de ti esté quemando tus tripas, no lo hagas. Cuando sea verdaderamente el momento, y si has sido elegido, sucederá por sí solo y seguirá sucediendo hasta que mueras o hasta que muera en ti. No hay otro camino. Y nunca lo hubo. 
 Bukowski lo dijo en su célebre poema, las bibliotecas del mundo están llenas de libros con historias vacías, personajes sin emociones y tramas que rayan en lo soso. El poeta de culto entendió que la escritura se refiere al fuego que invade la sangre del escritor y que provoca que las palabras sean escupidas. El arte de las palabras es un juego de emociones y un reto contra tus propios demonios. Además de leer a los grandes, a los que murieron en defensa de su estilo y carcomidos por el peso de sus palabras, las películas también ayudan a construir la voluntad de ser escritor; una profesión que poco paga pero que libera. 
 Te compartimos algunas de las mejores películas que no puedes dejar de ver si sueñas con escribir tu nombre en el libro de la historia. 
 Medianoche en París 
  
 Ganadora del Oscar por mejor guión original, esta comedia de Woody Allen se ha consagrado como una de las mejores películas del cineasta estadounidense. La trama acompaña a Gil Pender en un viaje en el tiempo al París de los años 20. De la mano de los escritores y artistas que se consagraron en la ciudad de la luz, el protagonista deberá encontrarse a sí mismo en el París nocturno, mientras de día su prometida amenaza todo rastro de su creatividad. 
 La ventana secreta 
  
 Basada en el thriller ,  Secret Window, Secret Garden  de Stephen King, Mort Rainey es un afamado escritor que se enfrenta al bloqueo creativo, mientras atraviesa un divorcio que lo relega a una cabaña junto a un lago. Un extraño hombre se aparece ante su puerta para acusarlo de plagio y pide a cambio una compensación. Ante la amenaza de un psicótico de asesinarlo a sangre fía, Mort deberá enfrentar jugar al gato y al ratón, actuando con astucia y determinación ante un enemigo que parece conocerlo mejor de lo que cree. 
 Diarios de la calle 
  
 Inspirada en una historia real, la profesora de literatura Erin Gruwell se enfrenta al reto de dar clase a un grupo de adolescentes de Long Beach, California, tras los disturbios de 1992 en la ciudad de Los Ángeles. En conjunto con la maestra, los jóvenes deberán comprender el trasfondo del racismo y conocer que éste sólo conduce al odio y a la violencia.  Conmovidos por el Diario de Ana Frank, los estudiantes tendrán la oportunidad de escribir su propio diario, en el que tanto la maestra como los adolescentes se conocerán a través de las ideas que los convierten en verdaderos defensores de la libertad. 
 Casi famosos 
  
 William Miller quiere transformar el mundo del periodismo musical y escribir sobre él desde dentro. Convencido de que una experiencia como periodista gonzo le permitirá obtener el mejor reportaje jamás publicado, recibe el encargo de la revista Rolling Stone para seguir la gira de la banda Stillwater. ¿Qué aventuras le esperan al reportero que decide vivir la historia misma? 
 The Pillow Book 
  
 Kyoto, década de los 70. Un anciano calígrafo escribe en el rostro de su hija la felicitación por su cumpleaños, un hecho que marcará a Nagiko. Años después, la protagonista debe vivir bajo la sombra de un matrimonio arreglado, y ante la homosexualidad de su pareja decidirá huir de Kyoto para iniciar una nueva vida. Sin embargo, el recuerdo de su padre escribiendo en su casa persiste, y se transforma en la necesidad de encontrar un amante que utilice su cuerpo como una hoja en blanco. 
 La sociedad de los poetas muertos 
  
 En 1959, el profesor de literatura John Keating llega a la Welton Academy para compartir su visión sobre este arte. Cuestionando los paradigmas sobre la educación, el profesor les enseñará a sus alumnos la belleza de la vida, sobre los versos escritos desde los corazones en llamas de los poetas, y la necesidad de aprovechar cada momento de la vida como si fuera el último. Un grupo de cuatro estudiantes decide revivir un viejo grupo de poesía que jurara rebelión contra los pilares conservadores de la academia. 
 Antes que anochezca 
  
 Javier Bardem interpreta al escritor y poeta cubano Reinaldo Arenas en esta película cuya trama cuenta la historia de vida del escritor desde su infancia rural y su participación en la Revolución Cubana, hasta su persecución por ser escritor y homosexual. Fiel a sus principios y libertad política, artística y sexual, Arenas deberá decidir entre morir a costa de sus ideales o exiliarse en Estados Unidos, dejando su vida atrás. 
 La gran belleza 
  
 Durante el verano, Jep Gambardella es el centro de toda la vida social en Roma, donde artistas, políticos, periodistas, actores y criminales de altos vuelas generan una rama de relaciones inconsistentes que se desarrollan en los fastuosos palacios y villas de la capital latina. Con una novela premiada en su juventud, Jep enfrenta nostalgia por la escritura, las memorias de un joven amor al que sigue anclado y la necesidad de abandonar un estilo que sólo le provoca repulsión hacia sí mismo y hacia los demás. 
 En el camino 
  
 Basada en la novela homónima de Jack Kerouac, la trama de la película se centra en los años que el escritor dedicó a viajar por América durante la década de los 40 con sus amigos: Neal Cassady, William S. Burroughs y Allen Ginsberg. Los jóvenes, ávidos de libertad, son el retrato de la generación beat en busca del encuentro con el mundo y con ellos mismos. 
 Capote 
   
 El escritor Truman Capote leyó, en 1959, una crónica del New York Times que relataba el asesinato de los cuatro miembros de la familia Clutter en su granja de Kansas. A pesar de que historias como éstas inundaban la prensa todos los días, el escritor probará la teoría de que en manos de un escritor adecuado, la realidad puede ser tan fascinante como la ficción. Capote es enviado a Kansas para cubrir el caso, y aunque en un principio despierta la sospecha de los vecinos, pronto se ganará la confianza del encargado de la investigación.