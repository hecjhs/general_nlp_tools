Nueva película de ‘El Principito’ producida por DiCaprio

 El día de ayer se dio a conocer el trailer de la película animada El Principito,  producida por el actor Leonardo DiCaprio. El avance de casi un minuto y medio, está en idioma francés y permite darnos una idea de lo que será esta cinta en 3D sobre el popular libro de Antoine de Saint-Exupéry que a tantas infancias ha marcado. 
  
 La película se estrenará el 7 de octubre de 2015 y en su versión inglesa contará con las voces de Riley Osborne, James Franco, Benicio del Toro, Rachael McAdams, Marion Cotillard y Ricky Gervais. Esta película estará dirigida por Mark Osborne, director de Kunf Fu Panda y tendrá el guión de Irena Brignull. 
   
 *** 
 Con información de El Universal.