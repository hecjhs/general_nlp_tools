Películas de culto que puedes ver en Netflix

 El término de película de culto se remonta a la década de los 70, en que el concepto se empleó para describir la cultura alrededor de una película underground  o de Cine B. Posteriormente, se utilizó para describir a esas producciones cinematográficas que eran mantenidas con vida gracias a sus devotos fans. Lejos de las grandes salas, los fuertes presupuestos y la distribución masiva, muchas películas sobrevivieron gracias a los pocos fans que quedaron marcados por dichas producciones. Sin embargo, el concepto actualmente también se emplea para las películas que innovaron en el séptimo arte, que causaron controversia o que tienen un gran cúmulo de seguidores, lo cual coincide con el cine de autor. 
 Hoy, muchas películas que marcaron décadas pasadas tienen un gran número de seguidores, y se han convertido en referentes del séptimo arte. Algunas de esas películas han llegado hasta Netflix, en donde podrás disfrutarlas. Aquí una breve lista de películas de culto que puedes ver en la plataforma digital. 
 Taxi Driver (1976) Martin Scorsese 
  
 Un hombre mentalmente inestable conduce un taxi para calmar su insomnio crónico, producto de su experiencia en la Guerra de Vietnam. A través de sus ocupantes, el taxi representa un mosaico de las realidades sociales y políticas que el país de las barras y las estrellas enfrenta a final de los años 70. El espectador generará una extraña empatía hacia el antihéroe, Travis Bickle, quien deberá enfrentar sus instintos violentos. El filme de Scorsese, causó controversia por mostrar de manera tan vil, la realidad de un país que buscaba definir su identidad tras Vietnam. 
  
 – Reservoir Dogs (1992) Quentin Tarantino 
  
 En la que sería su primer película y que le llevaría a la fama, Tarantino describe el conflicto que surge entre un grupo de criminales después de un fallido robo. Con identidades desconocidas entre sí, y el nombre de colores para identificarse entre ellos, los ladrones, contratados para robar un almacén de diamante, desconfían de sus compañeros y dudan de la lealtad de los implicados, asumiendo que quizás alguno de ellos es miembro de la policía. Sangre y violencia en el filme de un Tarantino quien estaba en camino de descubrir su esencia cinematográfica. 
  
 – Scarface (1983) Brian de Palma 
  
 Tony Montana, un inmigrante cubano en los Estados Unidos, escalará posiciones en un cartel que trafica cocaína en el país norteamericano. A través de las disputas tradicionales de los carteles de la droga, Montana deberá hacer frente a sus enemigos que se empeñarán en destruir su recién adquirido imperio. Sin embargo, el protagonista también deberá luchar contra su propia paranoia, que amenaza con destruirlo antes que sus rivales. 
  
 – Shawshank redemption (1994) Frank Darabont 
  
 Tras ser declrado culpable por el asesinato de su esposa y el amante, Andrew Dufresne se enfrentará a la realidad de la cárcel, la corrupción, la crueldad y los abusos. Aunque el futuro es poco prometedor, Andrew encontrará en el gran capo de contrabando, una poderosa amistad que le dará sentido a esos años. Considerada la mejor película de todos los tiempos según IMDb, la cinta tiene una gran historia, convirtiéndose en un referente moderno del cine. 
  
 – León: El Profesional (1994) Luc Besson 
  
 La vida de un asesino a sueldo, León, se cruza con la de la pequeña Mathilda, interpretada por Natalie Portman en su primer papel cinematográfico. La familia de la niña ha sido asesinada a manos de agentes corruptos del Departamento de Policía de Nueva York, y Mathilda, con sed de venganza, le pedirá a León que le enseñe cómo hacer el trabajo sucio. 
  
 – American History X (1998) Tony Kaye 
  
 Después de pasar varios años en la cárcel por asesinar a un hombre negro, un joven neo-nazi intentará reinsertarse en la sociedad. Dejando atrás una ideología que sólo le condujo a la violencia y al crimen, Derek también deberá evitar que su hermano menor siga sus equivocados pasos. La película de Taye refleja el extremismo ideológico en Estados Unidos, mismo que conduce a comportamientos violentos fundamentados en ideas de segregación y odio. 
  
 – Se7en (1995) David Fincher 
  
 Ante su inminente retiro, un detective deberá enfrentarse a un último caso que involucra a un asesino serial. De la mano de su sustituto en la fiscalía, ambos investigarán las pistas de múltiples asesinatos con pistas relacionados con los siete pecados capitales. Un asesino, cuyo sentido de vida consiste en la adrenalina de ser atrapado, jugará con la mente de los detectives que se ven incapaces de detener al hombre que siembra terror por toda la ciudad. 
  
 – Raging Bull (1980) Martin Scorsese 
  
 La adaptación de Scorsese del libro escrito por el boxeador Jake La Motta, resulta en una joya cinematográfica. La historia sigue a La Motta, quien añora convertirse en el mejor boxeador de los pesos medios. Sin embargo, su ascenso rumbo al éxito se verá complicado por problemas maritales y el involucramiento de la mafia en su carrera. La cinta de Scorsese fue muy bien recibida por la crítica y la revista Premiere la colocó en el décimo lugar de las películas con las mejores actuaciones de todos los tiempos. 
  
 – 12 monkeys (1995) Terry Gilliam 
  
 Ante la amenaza de un extraño virus que está acabando con la población humana en el año 2035, una comunidad científica decide enviar a un prisionero al pasado para descubrir el origen del problema. Sin embargo, el viajero en el tiempo será recluido en una institución mental al argumentar que viene del futuro y tiene la encomienda de encontrar el futuro de un virus que terminará con la raza humana. Con las actuaciones de Pitt y Willis, este título se ha convertido en uno de los favoritos de los amantes de las películas de los 90. 
  
 – Pulp Fiction (1994) Quentin Tarantino 
  
 Dos asesinos a sueldo buscan recuperar un maletín que le fue robado a su jefe, quien además ha pedido que uno de ellos lleve a su esposa de paseo por la ciudad. Con saltos en el tiempo y regresiones en el mismo, el título de Tarantino, y todo un clásico de finales del siglo pasado, presentan múltiples historias que convergen en una serie de extraños incidentes. 
  
 – Te puede interesar: Películas que no sabías estaban en Netflix