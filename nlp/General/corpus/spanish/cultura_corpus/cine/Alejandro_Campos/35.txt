Las mejores 25 series de anime según IMDb

 “En Titán perdí dos cosas. Una de ellas fue mi memoria, ni siquiera podía recordar mi propio nombre. Es como si yo no estuviera aún con vida, con todo un camino para empezar”. 
 Vincent Volaju en Bebob Cowboy El mundo del anime, criticado por muchos y vanagloriado por otros, es una de las ofertas culturales más interesantes de Japón. No sólo por la oportunidad de adentrarse en una historia a través de un lenguaje visual rico en elementos gráficos, sino también por la facilidad que tienen para atrapar al espectador y hacerlo cómplice de los personajes. Es sumamente probable que cualquiera de nosotros se haya enganchado en la infancia con una buena historia de anime: Pokemon, Dragon Ball Z  o Los Caballeros del Zodiaco.  Sin embargo, más allá de las clásicas historias que han sido explotadas comercialmente por “Occidente”, la disciplina que nace del dibujo también contiene grandes historias que esperan ser descubiertas. 
 Aunque es probable que los verdaderos amantes del género sean amplios conocedores de estas historias, el resultado del conteo propuesto por IMDB ofrece una gran oportunidad para conocer tramas que han sido celebradas alrededor del mundo, y que tanto por su riqueza visual como por la relevancia de los temas expuestos, merecen una oportunidad. Es un gran camino para empezar. 
 ¿Estás de acuerdo con este ranking? 
 10.- Leyend of the Galactic Heroes (1988) 
  
 En un futuro lejano y sin civilizaciones alienígenas, dos poderosas fuerzas espaciales luchan intermitentemente una contra la otra: el Imperio Galáctico y la Alianza de Planetas Libres. La historia acompaña a los líderes de cada una de las potencias, y explora las dificultades personales y políticas a las que se enfrentan en aras de asegurar el dominio total de la galaxia. ¿Quién se impondrá? 
 – 9.- Now and then, Here and There (1999) 
  La trama acompaña a un joven chico llamado “Shuzo” Matsutani, quien en aras de salvar a una chica desconocida, es transportado a otra dimensión. En un nuevo mundo desolado, militarizado y con escasez de agua, Shuzo será convertido en un niño soldado. Guerra, esclavitud, explotación infantil, desolación y una oscura distopia sirven como escenario para presentar una visión pesimista del futuro de la humanidad. 
 – 8.- Berserk (1997) 
  
 Originalmente planteado como un manga, fue adaptado al cine bajo un estilo épico fantástico. La historia nos remonta a una época medieval que aborda al personaje de Guts; un mercenario huérfano que acompaña a un elfo caza demonios. Las aventuras de estos dos particulares personajes sientan la base para explorar lo mejor y peor de la naturaleza humana, eso sí, con fuertes tintes de violencia. 
 – 7.- La rosa de Versalles (1979) 
  
 Basada en mayor o menor medida en personajes reales de la Francia del siglo XVIII, la autora de este anime aprovecha un personaje ficticio para presentar el caso de una mujer criada como varón, quien más tarde se convertirá en la protectora de los reyes franceses. Desafiando todo tipo de normas de género, Lady Oscar será testigo y participante de los convulsos cambios políticos y sociales que la Revolución Francesa traerá consigo. 
 – 6.- Mushi-shi (2005) 
  
 Guinko, el personaje principal de este anime, se topa con unos extraños seres conocidos como “mushi”, que no son ni plantas ni animales, y que suelen ser desconocidos para la mayoría excepto por un número limitado de humanos que poseen la habilidad de verlos. Aquellos que tienen la capacidad de interactuar con ellos, se convierten en maestros capaces de investigar sobre los Mushi y resolver eventos supernaturales relacionados con esta especie. 
 – 5.- Baccano! (2007) 
  
 Con una historia que transcurre en distintos lugares, líneas temporales y puntos de vista de personajes distintos, la sinopsis de este anime podría sintetizarse en la disputa entre mafiosos por el control de la ciudad de Nueva York y por encontrar el secreto que permite la inmortalidad. Al parecer, un grupo de personas tienen acceso al elixir de la vida, que no sólo les asegura la inmortalidad sino la capacidad de elegir el momento de su eventual muerte en caso de desearlo. 
 – 4.- Maison Ikkoku (1986) 
  
 Inspirado en el manga homónimo, la historia sigue a un estudiante universitario y sus intentos por conquistar a su casera viuda. Además, la trama aborda a los peculiares personajes de la Maison Ikkoku, que representan de manera adecuada y graciosa a la variedad de personalidad que la humanidad ofrece. 
 – 3.- Kenshin, el Guerrero Samurai: Recuerdos (1999) 
  
 Inspirado en el manga homónimo, la trama recupera los primeros años de la era Meiji del Imperio del Japón; época que sirve como referencia para detallar cuestiones como la paz, el amor y la redención. ¿Hasta dónde llegará el guerrero por defender su honor? 
 – 2.- Steins; Gate (2011) 
  
 Con un gran planteamiento, este anime acompaña a un grupo de amigos que logró convertir su microondas en un dispositivo capaz de enviar mensajes de texto al pasado. Sin embargo, el experimento de los jóvenes no pasará desapercibido por una organización secreta que también experimenta con viajes al pasado. ¿El futuro se puede cambiar con la alteración de algún hecho pasado? El grupo de amigos intentará averiguarlo mientras huyen de ser capturados por la organización. 
 – 1.- Cowboy Bebop (1998) 
  
 Con 26 episodios ambientados en el 2071, la trama sigue a un cazarrecompensas que viaja a través de la galaxia en busca de trabajos especiales. Conforme la tripulación recorre múltiples rincones del universo, nuevos personajes se unen a la trama, mismos que permitirán múltiples temas filosóficos: soledad, muerte, trascendencia, vacío y existencialismo.  – Si disfrutas el anime y deseas conocer el resto de las mejores historias, te compartimos el listado completo, ordenado de forma descendente. 
 25.- Kôdo giasu: Hangyaku no rurûshu 24.- Ghost in the Shell: Stand Alone Complex 23.- Cazador 22.- Neon Genesis Evangelion 21.- Hunter x Hunter 
  
 20.- Elfen Lied 19.- Monster 18.- Death Note 17.- Lupín 16.- Musekinin kanchô Tairâ 15.- GTO: Great Teacher Onizuka 
  
 14.- Planetas 13.- Seirei no moribito 12.- El guerrero samurai 11.- Usagi doroppu 
 – Fuente IMDb 
 – Te puede interesar: 10 series japonesas de anime que no puedes dejar de ver