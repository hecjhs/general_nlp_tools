5,000 películas para descarga gratuita legal

 Actualmente nos enfrentamos a un debate entre aquellos que buscan difundir contenido multimedia en aras de que llegue a más personas y otros, que acusan dicha actividad de ilegal por atentar contra los derechos de otra persona sobre un producto multimedia. Tal es el caso por ejemplo de lo sucedido con los fundadores de The Pirate Bay quienes han sido acusados de difundir contenido que infringe leyes de copyright en beneficio propio. Sin embargo, no todo está perdido respecto al libre acceso al patrimonio cultural de la humanidad. 
  
 Como gran opción para poder compartir, disfrutar y ver películas de dominio público (cuando no existe derecho sobre la propiedad de un producto o su plazo ya expiró) existe archive.org. La página es en realidad una plataforma para compartir contenido que no posee restricciones de derechos de autor en distintos ámbitos, desde textos y software hasta audios y películas. Al respecto de éstas últimas, pueden verse e incluso descargarse, 5,000 títulos que van desde las comedias hasta las “películas de culto”. 
 Si bien la página no es muy amigable a la vista y no existe un listado completo de las películas disponibles, basta con usar el buscador y con el nombre en inglés de la película para verificar su existencia en el archivo. 
 Existen por ejemplo películas como: 
 Dr. Strangelove- Stanley Kubrick 
 M – Fritz Lang 
 The 400 blows – François Truffaut 
 Metropolis – Fritz Lang 
 The Phantom of the Opera – Rupert Julian 
 y más… 
 Puedes consultar la página y las películas disponibles en esta dirección .