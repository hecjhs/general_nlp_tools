Las mejores 50 películas de la historia según los críticos de cine

 El British Film Institute nació en 1933, con miras a “alentar el desarrollo de las artes del cine, la televisión y la imagen en movimiento en todo el Reino Unido”, y desde entonces promueve festivales al interior del Reino Unido, ofrece múltiples actividades educativas y mantiene el más largo archivo de cine en el mundo, el cual contiene más de 60 mil películas de ficción, 120 mil de no ficción y alrededor de 750 mil programas de TV. 
 Como una de las instituciones cinematográficas más importantes del mundo, cada 10 años convoca a críticos, académicos y distribuidores para realizar un ranking de las mejores 50 películas de la historia. El último, hecho en 2012, convocó a 846 personalidades del cine para evaluar lo mejor del mundo en aspectos cinematográficos. Lo interesante de este ranking, es que contrario a otros como los de la Revista Empire o de IMDb , tiene posiciones y títulos distintos entre los mejores lugares pues fue establecido a partir de estándares técnicos y narrativos mucho más estrictos. Lejos quedaron ya los tiempos en que la obra maestra de Orson Welles dominaba los listado, pues ahora el padre del suspenso estadounidense mantiene la corona. 
 ¿Qué películas de los últimos años incluirías en el próximo ranking en 2020? 
 1.- Vertigo (1958) Alfred Hitchcock 
  
 Es considerada por la crítica no sólo la mejor película de la historia sino la joya cinematográfica de Hitchcock, por su técnica y narrativa. La trama gira en torno a Ferguson, un policía que debe renunciar a su trabajo a causa de los constantes ataques de vértigo, pero ante el retiro, encuentra a un nuevo trabajo. Éste consistirá en vigilar a la esposa de un amigo, lo que desemboca en una persecución inesperada con movimientos ágiles de la cámara y giros que provocarán nauseas entre el público, aludiendo al título de la película. 
  
 – 2.- Citizen Kane (1941) Orson Welles 
  
 Charles Foster Kane es un magnate de los medios, quien en su último suspiro pronuncia la misteriosa palabra “Rosebud”. La trama se desenvuelve a partir de entrevistas del periodista Jerry Thompson, lo que nos permite conocer la vida privada de Kane desde su infancia en la pobreza hasta la soledad en su mansión. El personaje interpretado por el propio Welles está inspirado en William Randolph Kane, criticando el sensacionalismo, la manipulación y lo tendencioso que puede llegar a ser el periodismo. 
  
 – 3.- Tokyo Story (1953) Ozu Yasujiro 
  
 La obra de Ozu trata sobre dos padres ancianos, de origen rural, que viajan a la gran capital japonesa para visitar a sus hijos. Sin embargo, el bello encuentro familiar en realidad se convierte en un fuerte encuentro de distintas realidades, en que los padres son rechazados por sus propios hijos, por lo que deciden hospedarse en un hotel. En un marco de postguerra, Ozu refleja la brecha generacional producto del desastre, la guerra y la barbarie de la Segunda Guerra Mundial. 
  
 – 4.- La regla del juego (1939) Jean Renoir 
  
 Con un pésimo estreno en taquilla por el inicio de la Segunda Guerra Mundial y fuertemente censurada por los gobiernos francés y alemán, la obra de Renoir recibió años después el crédito que merecía. A través del argumento en que múltiples personas de la alta burguesía y sus sirvientes se reúnen un fin de semana en un lujoso castillo, dando cuenta del mosaico social francés previo a la guerra. Ante la destrucción inminente, la sociedad sólo mantiene “insensibilidad moral”. 
  
 – 5.- Sunrise: A song of two humans (1927) FW Murnau 
  
 Un granjero vive felizmente con su esposa en el campo, pero la llegada de una mujer interpretada por Margaret Livingston, proveniente de la ciudad, destruye el amor perfecto. El granjero se enamorará de los detalles citadinos de la mujer, y su esposa, por quien ante profesa un amor desbordado y pasional, se convertirá en sólo un estorbo para su felicidad. 
  
 – 6.- 2001: Odisea en el espacio (1968) Stanley Kubrick 
  
 Silencios, misterio, una gran banda sonora y el planteamiento de una trama de ciencia ficción que sigue cautivando a los cinéfilos. Tras una breve explicación de cómo la raza humana evolucionó gracias a la intervención de un ente alienígena, un cuerpo de astronautas persigue las señalas de un extraño monolito encontrado en la luna. Referente de las películas del espacio, esta película es una de las cintas básicas de Stanley Kubrick . 
  
 – 7.- The Searchers (1956) John Ford 
  
 Un western ocupa la séptima posición en este listado de la BFI y está inspirado en la novela de Alan Le May. La trama oversa sobre Amos y Martin, dos vaqueros que buscan a dos hermanas secuestradas por los indios. La consagración de Ford como director que impulsó al western como un género importante en la industria cinematográfica. El esfuerzo por los vaqueros, con las vicisitudes del viaje conlleva un desarrollo de la personalidad de los mismos, enmarcados por una maravillosa fotografía del desierto. 
  
 – 8.- El hombre de la cámara (1929) Dziga Vertov 
  
 Con un profundo discurso ideológico, Vertov describe su fascinación por el constructivismo y futurismo a partir de la vida cotidiana en la Unión Soviética. Considerada como “un retrato puntillista en el que sólo la totalidad de los breves retazos permiten percibir la ciudad en su totalidad” según Filmaffinity, esta producción es una de las más ambiciosas de la época tanto por su técnica como su narrativa. 
  
 – 9.- La pasión de Juana de Arco (1927) Carl Dreyer 
  
 Basada en la figura histórica de Juana de Arco, la trama se aleja del sentido épico de otras producciones modernas, pues ahonda en el juicio de la Inquisición por el que pasó la dama de Orleans al ser acusada de herejía. Con primeros planos que permiten recalcar la capacidad dramática de los actores, esta película de 1929 sentó las bases de otras películas que representan “el triunfo de la imagen sobre la palabra”. 
  
 – 10.- 8 1/2 (1963) Federico Fellini 
  
 El célebre director de cine Guido Anselmi atraviesa una profunda crisis creativa producto de su última producción cinematográfica. Lejos del ambiente, la tranquilidad y la inspiración que le permita seguir creando. Considerada una película autobiográfica, la trama nos permite ahondar en el proceso creativo de un director de cine, los problemas de rodaje y la presión de los productores al trabajo de un autor. Al final, Guido entenderá que ahí donde todo parece estorbarle y nublarle la mente, se encuentra la clave para regresar al cine. 
  
 – Te compartimos el resto del ranking: 
 11. Acorazado Potemkin (1925) Sergei Eisenstein 
 – 12.- L’Atalante (1934) Jean Vigo 
  
 – 13.- Breathless (1960) Jean-Luc Godard 
 – 14.- Apocalypse Now (1979) Francis Ford Coppola 
 – 15.- Late Spring (1949) Ozu Yasujiro 
 – 16.- Au hasard Balthazar (1966) Robert Bresson – 17.-  Seven Samurai (1954)Kurosawa Akira–17.- Persona (1966)Ingmar Bergman–19.- Mirror (1974)Andrei Tarkovsky–20.- Singin’ in the Rain (1951)Stanley Donen & Gene Kelly–21.- L’avventura (1960)Michelangelo Antonioni–21.- Le Mépris (1963)Jean-Luc Godard–21.- El Padrino (1972)Francis Ford Coppola–24.- Ordet (1955)Carl Dreyer–24.- In the mood for love (2000)Wong Kar-wai–26.- Rashomon (1950)Kurosawa Akira–26.- Andrei Rublev (1966)Andrei Tarkovsky–28.- Mulholland Dr. (2001)David Lynch–29.- Stalker (1979)Andrei Tarkovsky–29.- Shoah (1985)Claude Lanzmann–31.- El Padrino II (1974)Francis Ford Coppola–31.- Taxi Driver (1976)Martin Scorsese–33.- Ladrón de bicicleta (1948)Vittoria De Sica–34.- El General (1926)Buster Keaton & Clyde Bruckman–35.- Metropolis (1927)Fritz Lang–35.- Psycho (1960)Alfred Hitchcock–35.- Jeanne Dielman, 23 quai du Commerce 108o Bruxelles (1975)Chantal Akerman–35.- Sátántangó (1994)Béla Tarr–39.- Los 400 golpes (1959)François Truffaut–39.- La dolce vita (1960)Federico Fellini–41.- Journey to Italy (1954)Roberto Rossellini–42.- Pather Panchali (1955)Satyajit Ray– 42.- Some like it hot (1959)Billy Wilder–42.- Gertrud (1964)Carle Dreyer–42.- Pierrot le fou (1965)Jean-Luc Godard–42.- Play Time (1967)Jacques Tati–42.- Close-Up (1990)Abbas Kiarostami–48.- The Battle of Algiers (1966)Gillo Pontecorvo–48.- Histoire(s) du cinéma (1998)Jean-Luc Godard–50.- City lights (1931)Charlie Chaplin–50.- Ugetsu monogatari (1953)Mizoguchi Kenji–50.- La Jetée (1962)Chris Marker–Referencia: BFI– Te puede interesar: Las mejores películas de la historia según 358 directores