Películas que te harán amar a tu mejor amiga

 La amistad  es ese poderoso e inquebrantable lazo que une el alma de dos personas durante un periodo específico de tiempo. Más allá del cliché de argumentar que una verdadera amistad es ‘para siempre’, o de características que pretendan definir la maravilla de la amistad, existen ciertos rasgos, emociones, actitudes y acciones que despiertan ese pequeño fuego en nuestro interior y se trata de un concepto ampliamente definido por los grandes escritores . 
 El mundo del cine tiene en su largo repertorio de filmes, múltiples producciones que ahondan en el complejo y maravilloso mundo de la amistad. Así como existen listados de películas para ver con tu mejor amigo , en esta ocasión nos dimos a la tarea de hallar esas películas que logran retratar la magia de una amistad entre mujeres. Aunque sea por un breve periodo de tiempo, ocurra en circunstancias extremas o termine en una turbulenta relación amorosa, la amistad entre mujeres es radicalmente diferente a la que puedan tener dos hombres: emociones profundas, la complicidad y gustos comunes muy marcados. En honor a esas grandes amistades que han marcado tu vida -y que te llevan a pensar en una persona en particular- te compartimos un listado de películas que detallan cómo es una amistad entre dos mujeres, y claro, también filmes que detallan cómo una hermana en ocasiones puede ser tu mejor amiga. 
 Las vírgenes suicidas (1999) Sofia Coppola 
  
 Ambientada en los años 70, la trama gira en torno a las cinco hermosas y deseadas  hijas de la familia Lisbon. La hija menor, Cecilia, se suicida a pesar de los intentos de sus padres por integrar a sus hijas con el resto del mundo. Tras el baile de graduación, otra de las hijas tendrá sexo con uno de los chicos más populares de la escuela, por lo que las cuatro hijas serán castigadas y encerradas en casa. Aisladas de todo contacto e interacción con el exterior, las hermanas forjaran un fuerte lazo entre sí y harán todo por escapar de su triste realidad. 
  
 – 
 Criaturas celestiales (1994) Peter Jackson 
  
 Basada en un caso policial ocurrido en 1954, en Nueva Zelanda, la trama cuenta la historia de dos adolescentes de distintas clases sociales que forjan una gran amistad en el colegio. A pesar de las diferencias de sus mundos, las dos chicas crean un mundo de fantasía propio, en el que la magia hace cualquier cosa posible. Con sueños de publicar sus aventuras en Estados Unidos, las amigas llevan su amistad un paso más allá, en el cual los sentimientos y el deseo se hacen más fuertes, suponiendo una grave transgresión para las tradiciones de la época. 
  
 – 
 Whip it (2009) Drew Barrymore 
  
 Bliss es una chica incomprendida de 17 años, aburrida por su vida controlada por sus conservadores padres, quienes insisten que sea modelo. Sin embargo, tras una visita al centro comercial, descubrirá un mundo alrededor del roller derby, pero sobretodo un grupo de amigas que le harán sentir que pertenece. Mujeres que rompen con los estereotipos femeninos y que se dedican a disfrutar de su autenticidad. ¿Podrá la amistad vencer cualquier obstáculo? 
  
 – 
 Inocencia interrumpida (1999) James Mangold 
  
 Basada en las memorias de la escritora Susanna Kaysen durante su estadía en un hospital psiquiátrico, la película detalla no sólo la vida al interior de la institución mental, sino también el fuerte lazo de amistad que se creó entre Susanna y Lisa, otra de las internas, quien se había escapado durante 15 días del hospital. Lisa alienta a Susanna a no tomar sus medicamentos, a resistirse a la terapia e incluso a escaparse de la habitación, reforzando la idea de que sin importar el cómo ni el dónde, la amistad es más que un concepto. 
  
 – 
 La vida de Adèle (2013) Abdellatif Kechiche 
  
 Con tan sólo quince años, Adèle enfrenta dudas sobre su sexualidad, misma que termina por definir al conocer a Emma, una chica de pelo azul. A través de la amistad, el amor y el deseo, ambas mujeres encontrarán el camino hacia su propia madurez, aunque deben toparse con los prejuicios de una sociedad conservadora. 
  
 – 
 Sucker punch (2011) Zack Snyder 
  
 Babydoll, con 20 años de edad, es internada en el Hospital Lennox para enfermos mentales después de haber matado accidentalmente a su hermana menor. Con el peligro de ser sometida a una lobotomía a petición de su padrastro, Babydoll imagina todo un mundo fantástico a su alrededor, por lo que la película mantiene tres niveles: la realidad, una sub-realidad en el que BabyDoll forma parte de un extraño burdel, y un último nivel en el que la chica recibirá la oportunidad de escaparse del manicomio. De la mano de sus amigas, Baby luchará por escapar de la realidad. 
  
 – 
 Hannah y sus hermanas (1986) Woody Allen 
  
 La historia sigue la vida de tres hermanas, hijas de un matrimonio de actores, que mantiene una estrecha relación. A lo largo de dos años, empezando y terminando con una cena del Día de Acción de Gracias, el director narra las vicisitudes en la relación de las hermanas, quienes enfrentan problemas individuales y algunas complicaciones familiares que amenazan la relación. ¿Quién dijo que las hermanas no pueden ser mejores amigas? 
  
 – 
 Magnolias de acero (1989) Herbert Ross 
  
 Durante poco menos de dos horas, Ross presenta la historia de seis mujeres que viven en un pequeño pueblo de Luisiana, al sur de Estados Unidos. La trama relata las inquietudes, miedos, sueños y deseos de las mujeres, en los cuales la amistad se forja como un gran valor que les permite superar la adversidad y encaminarse hacia sus más profundos sueños. 
  
 – 
 Thelma & Louise (1991) Ridley Scott 
  
 Esta road movie gira en torno a dos amigas que deciden abandonar sus monóticas y violentadas vidas para emprender un viaje hacia la montaña durante un fin de semana. Sin embargo, un crimen en defensa de una de las dos amigas, hará que el fin de semana de esparcimiento se convierta en una desesperada huída por alcanzar México. En el dolor, el crimen y la adrenalina, ambas mujeres encontrarán en su amistad un punto de resistencia ante una sociedad que las oprime. 
  
 – 
 Historias cruzadas (2011) Tate Taylor 
  
 Dos clases sociales radicalmente opuestas se unen en la década de los 60, al sur de Estados Unidos, para construir una inusual amistad en torno a un secreto proyecto de escritura. El ambicioso proyecto romperá las normas sociales y pondrá la seguridad de las mujeres en peligro, sin embargo, la determinación por quebrantar los límites de su época hará que se forme entre ellas una poderosa hermandad. 
  
 – Te puede interesar: Tatuajes para hacerte con tu mejor amiga