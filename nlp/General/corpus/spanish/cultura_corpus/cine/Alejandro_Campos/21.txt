Las 10 mejores películas de 2015 según IMDb

 Tres años antes de la primer exhibición de cine a cargo de los Hermanos Lumière, Émile Reynaud realizó la primer película de dibujos animados. A través del teatro óptico, un invento del propio Reynaud, se proyectaba una película sobre una pequeña pantalla translúcida, después de que ésta se hacía pasar por luz con múltiples espejos. En 1892, Reynaud contó la historia de tres personajes extraídos de la “comedia del arte italiana”, con la trama en la cual Arlequin y Pierrot peleaban por el amor de Colombina, quien debía decidirse por alguno de los dos. 
  
 Ahora, más de cien años después, una película animada se ha llevado el mayor éxito del año según IMDb , puesto que la audiencia ha votado a su favor, celebrando su comicidad, estilo, técnica y narrativa. 
 La plataforma digital sobre cine compartió su ranking de las mejores películas de habla inglesa de 2015 . A partir de una tabulación de ratings de los usuarios para películas estrenada en 2015 y luego compaginada con el top 250 de la plataforma, estos son los ganadores. ¿Estás de acuerdo? 
 10.- Avengers: Age of Ultron Joss Whedon 
  
 La secuela de los Avengers fue anunciada desde 2012, convirtiéndose en una producción largamente esperada por los fanáticos del universo Marvel. Con la llegada de la Bruja Escarlata, Visión y Quicksilver, los superhéroes agrupados bajo S.H.I.E.L.D. enfrentarán a un nuevo y poderoso enemigo: Ultrón. Aquellos fanáticos de la saga quedaron complacidos, y otros más se subieron al tren, esperando las próximas entregas de Avengers. 
  
 – 9.- Ant-Man Peyton Reed 
  
 Una historia más del universo de Marvel, en que un famoso estafador deberá encontrar en su interior, todo aquello que lo pueda convertir en un héroe. Scott Lang, será portador de la capacidad de reducir su tamaño hasta las dimensiones de una hormiga, y que le permitirá proteger al mundo de nuevas amenazas. Una película que entusiasmó al público, sobretodo por su brillante estrategia de publicidad con anuncios a tamaño hormiga. Literalmente. 
  
 – 8.- Ex-Machina Alex Garland 
  
 Para todos los geeks, esta adaptación resultó en uno de los mejores estrenos del año. La trama acompaña a un joven programador, seleccionado por el líder de su empresa, para ser enviado a un lugar lejano para participar en una prueba. La peculiaridad de la película consiste en que una robot-mujer cuestionará los límites de la vida real y la inteligencia artificial. 
  
 – 7.- Kingsman: El servicio secreto Matthew Vaughn 
  
 La adaptación del cómic de Millar y Gibbons resultó en una de las producción más divertidas del año. En ésta, un agente veterano intentará entrenar a un novato en la profesión del espía secreto. Ante la incompetencia de su colega, el agente confiará en un “ultra-programa de entrenamiento”, aunque una amenaza global pone en peligro a todo el mundo. 
  
 – 6.- Yo, él y Raquel Alfonso Gomez-Rejon  Un chico, en su último año de preparatoria, está determinado a concluir la escuela pasando desapercibido. Con un peculiar gusto por rodar películas de bajo presupuesto con su único amigo, verá su vida transformarse cuando tiene que convivir y conocer a una chica con leucemia. Tuvo un gran recibimiento en Estados Unidos. 
  
 – 5.- Sicario Denis Villeneuve 
  
 Profundamente inspirada en la realidad que se vive en la frontera entre Estados Unidos y Méxic0, la producción que hace alusión a los asesinos a sueldo, narra la historia de un agente del FBI que es reclutado por oficial de la fuerza especial de élite del gobierno. Determinado y con fuerte respeto a sus ideales, el agente deberá participar en la guerra contra las drogas, en la que conocerá que la realidad es mucho más cruda de lo que pensaba. 
  
 – 4.- Letras explícitas F. Gary Gray 
  
 La producción basada en historia real mejor valuada en IMDb fue la historia del grupo NWA, quienes a través de las letras, el rap y el hip hop, hallaron una forma de mover las conciencias de una sociedad dividida en la década de los 80. Con temas como racismo y segregación social, la relevancia del discurso se mantiene en un país que aún se enfrenta a estos problemas. 
  
 – 3.- The Martian Ridley Scott 
  
 Radicalmente distinta a Interstellar (2014), los amantes de las películas del universo criticaron cómo Ridley Scott trató una gran trama. Durante una misión tripulada a Marte, el astronauta Watney queda incomunicado con la Tierra, por lo que se asume ha muerto durante una tormenta. Sin embargo, en el planeta rojo, un solitario astronauta deberá encontrar la manera de sobrevivir, mientras se aferra a la posibilidad de enviar una señal a casa, y por ende, ser rescatado. 
  
 – 2.- Mad max: Fury Road George Miller 
  
 La secuela de la producción de 1979, fue una de las mejor recibidas por la audiencia, quienes sin momentos para descansar de la acción, quedaron extasiados al término de la película. El argumento aborda a una mujer que se rebela contra el líder totalitario de una Australia post-apocalítpica y reunirá a un gran número de prisioneras para destruir un sistema represor. 
  
 – 1.- Intensamente Pete Docter, Ronnie Del Carmen 
  
 Esta película fue la producción mejor valorada de este año. Además, la historia de las emociones al interior de una niña pre-adolescente la catapultó a empatarse con Toy Story 3 y Wall-E como la mejor película de Pixar en el conteo de IMDb. Considerada una película animada para adultos, puesto que muchas referencias y chistes eran para un público mayor, los personajes también cautivaron a los niños. 
  
 – Referencia: IMDb 
 – Te puede interesar: Las mejores películas que no viste en 2015