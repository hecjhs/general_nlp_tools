El payaso ‘Eso’ vuelve al cine, comenzará a grabarse en verano

 Tras meses de muchos rumores, el día viernes 5 de diciembre por fin se hizo oficial. La productora Warner Bros. dio a conocer que el rodaje de la película Eso (IT en inglés) inspirada en el libro homónimo de Stephen King comenzará el próximo verano (2015). 
  
 Según el sitio Vulture , Dan Li confirmó que el será el productor de la nueva cinta y que Cary Fukunaga será el director. 
 “La preparación previa de la película comienza en marzo. Cary anticipa mucho su proyecto y ya hemos esperado tres o cuatro años por éste. El libro es épico, por lo que no podemos explicar todo en una película. La primera parte será “Eso” atormentando a los niños, mientras que la segunda mostrará la continuación de la pelea del grupo cuando adultos. Por el momento, Fukunaga sólo dirigirá la primera pero hay un acuerdo para que co-escriba la segunda”. 
 Esta nueva película sobre el clásico de 1986 y adaptada a la pantalla grande en 1990 ha contado con la ‘bendición’ de Stephen King pues incluso leyó el guión, lo aceptó y afirmó “Vayan con Dios, ésta es la versión que el estudio debe filmar”. 
 Sin embargo, hasta el momento no se ha revelado más información respecto al filme de uno de los personajes más terroríficos de la literatura y de la pantalla grande. Ahora, volverá para causar pesadillas a una nueva generación. 
 Con información de Morbido Fest.