¡Ya viene! Lo mejor de Ambulante 2015

 Ambulante, como desde hace 10 años, se dedica a recorrer los rincones de México para mostrar al público lo mejor de la producción documental dentro y fuera de las fronteras. La edición correspondiente a 2015 arranca el jueves 29 de enero en el D.F. donde permanecerá dos semanas para después recorrer 12 estados de la República. 
  Siendo un festival de cine ampliamente reconocido por los amantes del décimo arte, Ambulante ha preparado una cartelera muy especial que se exhibirá del 29 de enero al 12 de febrero en 41 salas de cine capitalinas. Este año, el homenaje-retrospectiva será para Agnès Varda, cineasta de la llamada Nueva Ola francesa y que demás es una de las pioneras del cine feminista. Sus películas, documentales e incluso video-instalaciones, guardan características realistas y con profundos mensajes sociales que además rompen los paradigmas clásicos del cine francés. 
 Este año, la sección Sonidero incluye documentales de músicos como Björk, Pulp, The Beatles, Serrat y Sabina, Daniel Johnston y Nick Cave. La sección Enfoque trata sobre cine etnográfico, Ambulantito está destinado para el público más pequeño y en los “Imperdibles” se encuentran documentales que no son precisamente nuevos, pero que han sido adoptados por el público como clásicos: Searching for Sugar Man, Los dos Escobar, Exit through the Gift Shop: una película de Bansky.  “Injerto” es una sección predispuesta para la experimentación visual mientras que “Dictator’s Cut” presenta una serie de documentales con enfoque hacia los derechos humanos y la libertad de expresión alrededor del mundo. 
 Como todos los años, Ambulante también ofrecerá charlas, mesas redondas, clases magistrales y proyecciones especiales. 
 Aquí la lista completa de documentales . 
 Aquí el calendario de proyecciones    
 Algunos documentales que se presentarán en Ambulante: 
 A Hard Day’s Night 
  Un mes después de que The Beatles conquistaran el mercado estadounidense, comenzaron a trabajar en un proyecto cinematográfico en el que demostrarían una vez más su talento revolucionario. A Hard Day’s Night captura el momento extraordinario en que los integrantes de esta banda inglesa cambiaron la música para siempre y se volvieron oficialmente los ídolos únicos e irreverentes de su generación. 
   Tráiler-Sonidero, A Hard Day’s Night from AMBULANTE on Vimeo.  
 Cléo de 5 a 7 
  Cléo, cantante y paradigma de belleza, espera los resultados de un análisis médico. Pasando de la superstición al miedo, de la calle Rivoli al Café du Dôme, de la coquetería a la angustia, de su casa al parque Montsouris, de la apariencia a la desnudez, Cléo descubre cosas que le abrirán los ojos al mundo y a otra vida posible. 
   
  Apunte y dispare 
  En 2006, Matt VanDyke, un tímido joven de 26 años con trastorno obsesivo-compulsivo dejó su hogar en Baltimore, compró una motocicleta, una cámara de video y comenzó un viaje a través del Norte de África y Medio Oriente. Al detonarse la revolución en Libia, Matt se unió a la resistencia contra Muammar al-Gaddafi, filmando y luchando en la guerra. Esta es la historia de un joven y su búsqueda por una revolución política y su propia transformación personal. 
   Tráiler-Reflector, Apunte y dispare from AMBULANTE on Vimeo.  
 Los reyes del pueblo que no existe 
  San Marcos, un pueblo en el Noroeste de México, queda parcialmente inundado por la construcción de una presa durante parte del año. A pesar de todo, tres familias aún viven ahí: Pani y Paula se rehusan a cerrar su tortillería y dedican su tiempo para rescatar al pueblo de las ruinas; Miro y sus padres sueñan con marcharse pero no pueden; Yoya y Jaimito, aunque sienten miedo, tienen todo lo que necesitan. 
   Tráiler-Pulsos, Los reyes del pueblo que no existe. from AMBULANTE on Vimeo.  
 El cuarto de los huesos 
  Desde el Instituto de Medicina Legal, El cuarto de los Huesos, acompaña a varias madres salvadoreñas en su búsqueda por los restos de sus hijos desaparecidos a causa de la violencia que se vive en su país. Una mirada hacia la veintena de cuerpos que se reciben en la morgue mensualmente y que nadie reclama; la historia del ADN sin nombre, sin familiares identificables, de cuerpos que se volvieron cadáveres tras haber sido abatidos por pertenecer a la pandilla contraria. 
 
 Cuando sea dictador 
  ¿Y si, al otro lado del universo, cada segundo nacen nuevos mundos con distintas posibilidades para nuestras vidas? ¿Cómo serían? ¿Seríamos un aventurero, un psicópata, una madre ejemplar, un hombre invisible? Este documental de ciencia ficción parte de esta premisa para desarrollar la historia de George y lo que podría ser en otro universo. Una película que trenza ficción y documental, producida a partir de cientos de rollos de película amateur de 8mm. 
   Tráiler-Observatorio, Cuando sea dictador. from AMBULANTE on Vimeo.  
 Citizenfour 
  En 2013, la cineasta Laura Poitras comenzó a recibir correos electrónicos de parte de un tal “citizen four”, quien estaba a punto de denunciar programas de espionaje secreto coordinados por agencias de inteligencia y seguridad. Poitras voló a Hong Kong para encontrarse con quien resultaría ser Edward Snowden. Esta película es el resultado de ese encuentro: un thriller de la vida real, un retrato de Snowden y una experiencia profundamente perturbadora. 
 * 
 Las sinopsis son cortesía de la página de Ambulante.