Frases de “Breaking Bad” para llevar tu vida al límite

 “ My name is Ozymandias, king of kings: Look on my works, ye Mighty, and despair”. -Percy Bysshe Shelley Sal de la zona de confort, o mejor dicho, haz que te expulse. Enfréntate al verdadero mundo real, no al que creías conocer en la comodidad de tu hogar y de tu familia. Ve la cara del peligro, consigue lo que quieres sin pedir perdón ni permiso; corrómpete, transfórmate y transforma, inventa, ingenia, roba, haz trampa, juega limpio… mata. 
 Muchas personas son obligadas a entrar al mundo criminal sin buscarlo, otras lo hacen por necesidad y otras por el placer de hacer lo que quieren. Después de ver cinco temporadas de Breaking Bad  es difícil catalogar la verdadera razón por la que Walter White lo hace.  Breaking Bad Lo que es fácil saber es que la serie es considerada una de las mejores de la historia. Esto debido a las actuaciones, al nivel de producción y la acción, pero más que nada al intenso diálogo entre los personajes. Esta serie es un ejemplo de todas aquellas en las que los actores improvisan muy poco, pues los diálogos son realmente buenos, casi perfectos para cada personaje. Estar limitado en la improvisación, evita muchas veces que los actores saquen lo mejor de ellos, sin embargo en Breaking Bad se observan algunas de las mejores actuaciones recientes, tanto que han sido elogiadas por Anthony Hopkins .  Breaking Bad Te dejamos a continuación el excelente trabajo de los guionistas de esta gran serie; frases célebres que es probable asociar inmediatamente con una escena, y si eres fan de la serie, con el sentimiento que tuviste la primera vez que la escuchaste salir de la boca de tus personajes favoritos. Así que ponte tu traje amarillo, esconde el dinero en el barril, mata a esa mosca y demuestra que tú eres el peligro. “Me he dado cuenta… que es el miedo… lo peor de todo. Ese es el verdadero enemigo. Levántate. Y Patea a ese bastardo lo más fuerte que puedas justo en los dientes”. 
 “Estoy en el negocio del imperio”. 
 “Nunca cometas dos veces el mismo error”. 
 “Hay oro en las calles esperando que alguien venga y lo levante”. 
 “Si eso es cierto, si no sabes quién soy, entonces quizá tu mejor opción sería andar con cuidado”. 
  Breaking Bad “Nunca le mientas a un mentiroso”. “Di mi nombre”. 
 “Los hombres están para abastecer a sus familias a cualquier precio”. 
 “Esta familia es todo para mí. Sin ella no tengo nada que perder”. “Alguien tiene que proteger esta familia del hombre que protege esta familia”. 
 “Eres una bomba de tiempo y no tengo ninguna intención de estar cerca cuando explotes”. 
  Breaking Bad “Sólo porque le disparaste a Jesse James eso no te convierte en Jesse James”. “Si crees que hay un infierno, nosotros vamos casi seguro hacia allá. Pero no me voy a recostar hasta que llegue”. 
 “Que su muerte los deje satisfechos”. “¡Don Eladio está muerto, sus jefes están muertos, no tienen a nadie más por quién luchar, llenen sus bolsillos y váyanse en paz o vengan a luchar y mueran!”. 
 “Mataré a tu esposa, mataré a tu hijo, mataré a tu hija infante”. 
  Breaking Bad “¿Cuál es el punto de ser un forajido si tienes responsabilidades?” “No puedes confiar en un drogadicto”. “No estoy en peligro Skyler, yo soy el peligro. Si llaman a la puerta de un hombre y le disparan, ¿piensas que ese seré yo? ¡No! Yo soy el que llama”. 
 “Cierra la maldita boca y déjame morir en paz”. “Bitch”. 
  
 ** 
 Te puede interesar: 
 El verdadero significado de Breaking Bad 
 Las mejores series de 2015 según The Guardian