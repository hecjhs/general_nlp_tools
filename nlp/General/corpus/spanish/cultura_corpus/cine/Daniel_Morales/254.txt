Los mejores trailers de cine en la historia

 El libro 100 ideas que cambiaron al cine,  de David Parkinson, coloca en el número 70 al avance cinematográfico o trailer, s in duda una de las herramientas indispensables que productores, distribuidores y directores usan para garantizar los asientos ocupados en las salas de cine. 
  El término trailer surgió en Estados Unidos aproximadamente hace 100 años cuando al final de la proyección estelar promocionaban el avance de otra película, pero la gente solía abandonar la sala al terminar la misma, así que decidieron mostrar estos avances antes de que comenzara la función. Se intentó cambiar el nombre a preview,  pero ya era demasiado tarde y el nombre se había quedado en la mente de los espectadores. 
  La genialidad de todo buen trailer radica en mostrar algo que intrigue al espectador, darle alguna pista de hacia dónde se dirige la trama de la película sin mostrar parte fundamental de la historia. 
 Esta es una lista de los mejores trailers de la historia, según el sitio  ifc.com: 15. Pulp Fiction (1994) 
  
 La mejor de las joyas de Tarantino, la que lo catapultó como cineasta estrella. Aunque muchos le adjudican ser el primero en jugar con los tiempos de las películas, el mismo Tarantino dijo que se inspiró en la película The killing  (1956), de Stanley Kubrick. Armas, violencia y música, sello característico de Tarantino quien en este trailer introduce a famosos personajes como Mia Wallace o Vincent Vega. 
 14. Garden State (2004) 
  La opera prima de Zach Braff, quien dejó su papel en Scrubs  para escribir, dirigir y protagonizar esta película. Nostalgia, comedia y música de The Shins la convirtieron en una película de culto . 
   
 13. Mr. Sardonicus (1961) 
  
 La mejor persona para contarte qué sucederá en la película es quien la hizo, o eso pensaba William Castle, quien aparece en el avance de esta cinta invitando al espectador a decidir cómo es que terminará la película. 
 12. Independence day (1996) 
 El patriotismo americano a su máxima potencia, el dos de julio ellos llegan, el tres de julio ellos atacan y el cuatro de julio, nosotros (¿nosotros?) nos defendemos. Este trailer mostró justo lo necesario para llamar la atención del público sin dejar a la vista lo más importante de la película. 
 11. The Blair Witch Project (1999) 
  
 Una de las películas que fueron amadas y odiadas. El primer avance, de menos de 40 segundos, sólo muestra cinco segundos de imagen de la película. A los actores no los dejaron participar en la promoción de la misma, ni pudieron asistir a Cannes, con tal de asegurar que lo que la gente veía en pantalla era algo verídico. 
 10. The Shining (1980) 
  
 La obra maestra de terror de Stephen King, adaptada por Stanley Kubrick está llena de misticismo en el mundo del cine; hay teorías que abordan qué es lo que el filme en verdad significa y se dice que el autor de la novela y el autor de la película se odiaban. Con un avance simple y aterrador este trailer es uno de los mejores de la historia del cine. 
 9. Mission: Impossible (1996) 
  
 La primer parte de la saga protagonizada por Tom Cruise genero un éxito en taquilla. La música icónica y el cerillo encendiendo la mecha dejan ver una película llena de espionaje y acción como no se habían visto nunca. 
 8. The Texas chainsaw massacre (1974) 
   Esta película apostó por una fórmula diferente, mostró escenas clave de la trama y las cortó en el momento más importante, dejando al público cuestionándose qué es lo que pasaría después. 
 7. Dr. Strangelove or: How I learned to stop worrying and love the bomb (1964) 
  
 Otra de Stanley Kubrick. Considerado uno de los mejores cineastas de todos los tiempos, dirigió películas épicas de guerra, drama, terror, y hablando de esta obra, comedia. Los rápidos cortes, la presentación de personajes y situaciones hicieron de este un clásico de los ya famosos trailers de Kubrick. 
 6. Citizen Kane (1941) 
  
 Considerada por algunos como la mejor película de la historia, es difícil pensar que su avance no muestra a su protagonista, en cambio, el director Orson Wells presenta al resto del elenco e intriga más y más al no contar mucho sobre quién es Kane. 
 5. Comedian (2002) 
  
 Este trailer no muestra personajes de la película, no tiene ninguna implicación con ella hasta los últimos segundos en que deja ver que  es una cinta sobre comedia con Jerry Seinfield. 
 4. Miracle on 34th street (1947) 
  
 El director necesita un trailer para su última película, así que acude a estrellas de cine para encontrar cuál es el “ángulo” que necesita. 3. Cloverfield  (2006) 
  La película que decepcionó a miles, el trailer mostraba la destrucción que “algo” está dejando por toda Nueva York. El manejo de cámara molestó al público en general quien se quejó de que se mareaban y que la historia los decepcionó, pero sin duda este es uno de los mejores avances que se han hecho. 2. Psycho (1960) 
  
 El maestro del horror es, también, maestro de los avances cinematográficos, basta con ver esta presentación de la locación de Psicosis o la forma en la que aborda la gran simpatía de los pájaros para su película Birds. 1. Alien (1979) 
  
 El clásico de Ridley Scott, una obra maestra de ciencia ficción, de suspenso, de acción. “En el espacio nadie puede escuchar tus gritos” es lo que advierte el avance, mientras se ven escenas de astronautas enfrentando algo definitivamente aterrador.