Películas que debes ver si amas Game of Thrones

 
 Nunca la televisión había visto un fenómeno como el de “The Game of Thrones”. La popular épica tiene una producción impresionante, capaz de avergonzar a cualquier largometraje que quiera competir con alguno de sus episodios. 10 capítulos por temporada son suficientes para proveer por un año a los fanáticos, muchos de los cuales no eran asiduos de las series que tuvieran un alto grado de fantasía, magia, maldiciones y muchas otras cosas que componen a una ficción muy alejada de la realidad. La diferencia radica en que la serie no es un simple cuento de hadas, es una representación fantástica del comportamiento humano, de la política, ética, filosofía y muchas otras cosas que atañen la esencia del hombre. 
  
 No encontrarás una serie parecida a Game of Thrones, por lo menos no en mucho tiempo, pero sí puedes encontrar algunas películas o series que tengan algo en común con ella. Tal vez puede ser la cantidad de acción o erotismo, elementos comunes gracias a que la serie es parte de HBO; también es posible que encuentres algunas cintas o programas que toquen temas de política o algunos que te muestren a los personajes más desalmados. De algo podemos estar seguros: si te gusta la que es considerada una de las mejores series de televisión de la historia, seguramente te van a encantar las siguientes propuestas. 
 – “The Lord of the Rings” – Peter Jackson 
  
 La más obvia de todas. Es obvio que George R. R. Martin tiene mucha influencia de Tolkien, quien creó un mundo fantástico en el que orcos, elfos y más luchaban contra un enemigo común. Casi 50 años después, la serie de “A Song of Fire and Ice” llevó una trama parecida a la posmodernidad. Aquí no hay un enemigo en común, o por lo menos todos los que luchan entre sí aún no lo saben, tampoco hay bueno ni malos; cada uno tiene luz y oscuridad en su interior. La fantástica trilogía de Jackson demostró que no hay imposibles en la industria, así como ahora lo demuestra la serie. 
  
 – “House of Cards” 
  
 Si la política de King’s Landing o Winterfell te llama la atención, tal vez lo que sucede en la Casa Blanca también lo haga. La serie más poderosa de Netflix demuestra que la política no es aburrida, que los tintes trágicos dignos de Shakespeare se encuentran en todos lados y que la ambición muchas veces es más poderosa que el mejor de los deseos. Muchos odian a Joffrey o a Cersei, pero Frank Underwood es tan real que el miedo es que alguien como él en realidad llegue a gobernarnos. 
  
 – “300” – Zack Snyder 
  
 La acción es esencial en una serie con una trama tan complicada. Está bien que los diálogos sean inteligentes y la importancia de la trama radique en las decisiones de los personajes, pero si no hubiera mucha acción, el programa no se sostendría. “300” es la antítesis de lo que “Game of Thrones” representa. Una cinta llena de intriga, hecha en su totalidad en pantalla verde, pero si en algo concuerdan es en las excelentes escenas de acción. En esta cinta la sangre fluye junto con la cámara lenta, que permite apreciar cada espada que se blande a la perfección y si bien la serie no usa esos recursos, sí es bastante gráfica al momento de mostrar un asesinato. 
  
 – “The Wire” 
  
 ¿Puedes nombrar a todos los personajes principales y secundarios de “Game of Thrones”? Son pocos los aficionados que realmente pasan esa prueba. Si eres uno de ellos, puedes acercarte a la otra producción de HBO que también es considerada una de las mejores series de la historia. La trama es bastante complicada, los buenos y los malos siempre cruzan las lineas en las que supuestamente se definen, y las contradicciones morales y éticas son el pan de cada día en el programa… ¿aún no te recuerda a tu serie favorita? Siguiendo a un equipo de policías y narcotraficantes en los barrios de Maryland, “The Wire” supondrá uno de los mayores retos televisivos que puedes disfrutar en tu vida. 
  
 – “Wall Street” – Oliver Stone 
  
 La economía no es el tema que más apasiona a la mayoría de los espectadores del séptimo arte, pero hay algunas propuestas que llegan a trascender. Esta cinta se parece a la serie al retratar la ambición de muchos de los personajes. El dinero nunca duerme, ni en Westeros ni en Nueva York. Cómo olvidar cuando Joffrey se convirtió en rey y eso dejó en serios problemas económicos a todo el reino, tal vez comparable con la traición de Gordon Gekko a muchos de sus inversionistas en esta clásica película. 
  
 – “Rome” 
  
 La predecesora oficial de “Game of Thrones”. Si esta serie no hubiera existido, con su enorme presupuesto y el set más grande de la historia construido hasta ese momento para una serie de televisión, tal vez nunca habríamos visto la serie de fantasía nacer. Gracias a que la serie perdió espectadores y la inversión había sido tan grande, el set se transformó y muchos de los productores encontraron la forma de darle nueva vida a través de los libros de Martin. Si eres fan de la enorme producción de “Game of Thrones” (un episodio llegó a costar 8 millones de dólares), seguramente esta épica romana en la que aparecen Julio César, Cleopatra, Marco Antonio y más será de tu agrado. 
  
 – Si uno de tus amigos vuelve a decirte que no le gusta “Game of Thrones” porque le parece aburrida, puedes recomendarle alguna de estas grandes producciones. Tal vez no sea la serie más amable, pero no se trata de darte todo fácil, sino de retar al espectador y llevarlo al limite… “Valar Morghulis”. 
 *** Te puede interesar:  
 Consejos para trabajar en Game of Thrones 11 terribles hechos históricos que inspiraron Game of Thrones