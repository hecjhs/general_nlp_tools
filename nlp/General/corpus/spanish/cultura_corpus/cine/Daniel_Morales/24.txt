Las heroínas más guapas y sensuales de todos los tiempos de cine y televisión

 
 La belleza puede cautivar a muchos, pero la valentía, la iniciativa, la inteligencia y la autosuficiencia son mucho más importantes para admirar e incluso amar a alguien. Por esa razón, las heroínas en el cine y la televisión siempre han sido símbolo de adoración para muchos. Su personalidad suele ser distinta a la de cualquiera. Usualmente marcadas por el pasado, así se convierten en misteriosas figuras que resaltan por sus habilidades físicas y mentales. Claro que Hollywood siempre ha hecho que la gente se identifique con ellas y por eso ha reclutado a las más bellas mujeres del mundo para ser quienes las interpreten. 
 Es una villana, pero en la nueva cinta “Suicide Squad”, Harley Quinn jugará el papel de héroe y villano. Desde valentía moderada hasta habilidades extraordinarias, las heroínas enamoran a miles cada vez que salen en la pantalla. Las siguientes son algunas mujeres que se han negado a ser las víctimas y que en lugar de eso han optado por demostrar que a pesar de ser hermosas, pueden ser realmente las que salvan el día. 
 Ladgerda – “Vikings”  
 La esposa y escudera de Ragnar en la infravalorada serie acerca de un grupo de vikingos es un ejemplo de una mujer que demuestra que a pesar de vivir con un hombre fuerte, no necesita de él para sobrevivir. Pese a los problemas que suceden a lo largo de la serie, ella es por la que siempre apostamos para que no le suceda algo malo. 
 – Jessica Jones – “Jessica Jones” Una de las heroínas más famosas del momento. Protagonizada por Krysten Ritter, quien anteriormente había enamorado a muchos en “Apartment 23” y “Breaking Bad”, ahora llega a la serie de Netflix para mostrar un lado mucho más oscuro y humano de los superhéroes. 
 – Elizabeth Swann – “Pirates of the Caribbean”  
 Deadpool dice al final de su película que Keira Knightley tiene la capacidad para interpretar a Cable en la secuela de su película, eso nadie lo duda. La hermosa actriz inglesa ha llegado muy lejos con su gran trayectoria, pero ninguna fue tan notoria como el de la bella damisela convertida en pirata. 
 – Natasha Romanoff (Black Widow) – “Avengers”  
 Tal vez la más sensual heroína de la historia del cine. Scarlett Johansson ha interpretado a Black Widow desde 2010 y poco a poco ha ganado protagonismo, dejando de ser una cara bonita en el elenco para convertirse en uno de los personajes principales en el universo cinematográfico de Marvel. 
 – Sue Storm – “The Fantastic 4”  
 Todas las entregas de esta franquicia han sido un desastre en taquilla y ante la crítica, pero Sue Storm siempre ha sido una belleza. En los cómics es la superheroína con la que todos desean estar. Eso no está lejos de la realidad si pensamos que fue Jessica Alba quien le dio vida en dos olvidables películas. 
 – Wonder Woman – “Batman v Superman: Dawn of Justice”  
 La más reciente heroína en una de las más polémicas películas de superhéroes. Ella no es la protagonista, pero acaba de entrar en un universo mucho más grande de lo que pensamos, por lo que todos esperamos su película en solitario. 
 – Alice Abernathy – “Resident Evil”  
 En una saga que ya se alejó bastante de sus orígenes, parece que lo único por lo que vale la pena ver las nuevas cintas es por Milla Jovovich. Por esa razón elegimos a su personaje, Alice, en la tercera cinta de la saga. Con el cabello corto y en un mundo postapocaliptico, “Resident Evil: Extinction” siempre será su mejor momento. 
 – Rey – “Star Wars: Episode VII”   
 Algunos la llaman el mejor modelo que las niñas pueden seguir en la actualidad y otros dicen que se trata del cliché más grande jamás creado. A pesar de eso, Rey ha dado mucho de qué hablar gracias a su descomunal belleza, pero sobre todo a su identidad independiente y fuerte carácter. 
 – Imperator Furiosa – “Mad Max: Fury Road”  
 Mucho se puede decir de Rey, pero la verdad es que el personaje de Charlize Theron es el que realmente muestra el empoderamiento femenino en el cine. Una mujer que sin un brazo y sin cabello continúa representando la belleza pero también el peligro y el poder. 
 – Selene – “Underworld”  
 Kate Beckinsale estrenará este año la quinta (y esperemos última) cinta de la serie de vampiros y hombres lobo que realmente pelean. Pueden ser muchas las balas y la acción, pero es Selene quien se roba la escena cada vez que aparece con sus hipnóticos ojos azules y traje de cuero. 
 – Jean Grey – “X-Men”  
 Como una de las sagas que le dio vida a una nueva generación de cintas de superhéroes. Jean Grey se posicionó como la femme fatale para Wolverine y desde el estreno de la primera cinta de los mutantes en el año 2000, pocas la han superado. 
 – Catwoman – “The Dark Knight Rises”  Catwoman es una de las más sensuales heroínas y villanas en el cine. Podría ser la malvada y sensual Michelle Pfeiffer en 1992, la desastrosa Halle Berry en 2005 o la olvidada Lee Meriwether en 1966. La ganadora es la última, Anne Hathaway, quien en realidad es heroína y termina seduciendo para siempre al caballero de la noche. 
 – Hermione – “Harry Potter”  
 Claro que nos referimos a las últimas entregas en las que Emma Watson ya se había convertido en un ícono de la belleza y la moda. A pesar de no tener superpoderes y no ser la protagonista, Hermione siempre fue (en las películas) una hermosa e inteligente bruja capaz de enamorar a cualquiera. 
 – Buffy – “Buffy the Vampire Slayer”  
 Sarah Michelle Gellar fue el sueño de miles de adolescentes a finales de los años noventa. Su angelical belleza contrastaba con el duro carácter de la protagonista y el que fuera una cazadora de vampiros la hacía aún más sexy. 
 – Sydney Bristow – “Alias”  
 Con un gran desarrollo del guión, “Alias” mantuvo a sus espectadores intrigados durante cinco temporadas. Cada vez que algo se resolvía, surgía un nuevo misterio, pero tal vez fueron el carisma y la belleza de Jennifer Garner las que le dieron a la serie tanto tiempo de vida. 
 – Hay muchas heroínas que han quedado fuera y otras más que aún no llegan. El cine de superhéroes es uno de los que está creciendo de forma exponencial, por lo que seguramente en poco tiempo la lista cambiará, mientras tanto te invitamos a que nos digas cuáles son tus heroínas favoritas del cine  y televisión. 
 *** Te puede interesar:  
 10 películas que demuestran que las mujeres no son el sexo débil  
 El complicado mundo femenino y cómo sobrevivir a él a través de 10 películas