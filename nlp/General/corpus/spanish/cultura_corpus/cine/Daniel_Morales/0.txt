Todas las referencias escondidas de Stranger Things

 
 No ha pasado mucho tiempo desde su estreno y todos están hablando de la última y exitosa apuesta de Netflix. “Stranger Things” se ha convertido en ese unicornio televisivo que llega cada cierto tiempo. Si bien “Breaking Bad” o “Game of Thrones” son consideradas como unas de las mejores series de la historia, tardaron más de una temporada en convertirse en un fenómeno masivo. La serie original de Netflix, por otro lado, llamó la atención desde el primer día, pues miles de personas terminaron la primera temporada en tiempo récord gracias a lo cautivante que resulta. 
  
 “Los libros de King son otra parte de la influencia. ‘The Body’, ‘It’, ‘Carrie’ y más son referencias claras acerca de la historia”. Críticas positivas, fans por todo el mundo y el más grande homenaje que se le ha hecho a los años ochenta son parte de lo que ha logrado esta serie. Si no has visto la serie, te recomiendo dar click aquí para que leas todas las razones por las que vale la pena dejar cualquier serie a un lado y sumergirte en el extraño pueblo de Hawkins y los misterios que esconde, pero si ya devoraste los ocho capítulos y sientes que hay más referencias — además de las obvias — , te invitamos a seguir leyendo la siguiente lista que busca mostrar la mayor cantidad de referencias y guiños a los años ochenta y a la historia del cine, pues los hermanos Duffer (creadores de la serie) no buscaron solamente hacer un pastiche de referencias a Spielberg y King; hay mucho más aparte de ellos. 
 SPOILERS ABAJO 
 Desde los créditos podemos ver las referencias a Stephen King. La fuente usada para el título de la serie es casi idéntica a la de sus libros publicados en los 80, sobre todo a “Needful Things”, pero eso no es todo. Otro guiño al cine son las letras en una especie de luz neón sobre un fondo negro. Además, la música es parecida al tema de la cinta original de Halloween. 
  
  
  
 –  “X-Men”  Durante los primeros minutos de la serie, Will y Dustin apuestan el cómic #134 de X-Men. Parece algo menor, pero es en ese número en el que aparece Dark Phoenix, el álter ego de Jean Grey, quien demuestra su verdadero poder. Esto puede relacionarse con Eleven y sus poderes mentales, los cuales se intentan llevar al máximo por otra persona y no por ella. 
  – 
 “El señor de los anillos” 
 Mirkwood es como llaman a la calle en la que creen que Will desaparece, y Radagast es la contraseña para entrar al fuerte de Will. En la serie de J. R. R. Tolkien, Mirkwood es el bosque negro y Radagast es el tercero de los magos que habitan la Tierra Media. 
   
 – 
 “Alien” 
 La franquicia de uno de los mejores extraterrestres en la historia del cine y un referente de los 80 también aparece. El otro mundo al que se sumergen en la segunda mitad de la temporada muestra un cascarón abierto, si bien los huevos en “Aliens” nacen de forma distinta, el tamaño y color son similares, así como el momento en que por fin encuentran a Will entubado por el extraño ser. Alien usa a los humanos a través de las criaturas que se adhieren a sus caras y bocas para controlar a las personas. 
   – 
 “Altered States” y “Minority Report” 
 Hay dos cámaras que Eleven usa para llegar a ese extraño mundo alterno. El primero es un tanque de agua en el que entra y respira a través de un casco y el segundo es una alberca improvisada después de la lección que el profesor de Ciencias (tal vez el mejor maestro que ha aparecido en una serie) les dice cómo construir. La primera se remite a la película de 1980, “Altered States”, mientras que la segunda recuerda a esas piscinas de la cinta de Spielberg, “Minority Report”. 
   – “The Evil Dead” 
 Tal vez un poco obvia, uno de los posters en el cuarto de Jonathan, ese que es considerado inapropiado, aparece brevemente. Ambientada en 1983, la cinta llevaba dos años circulando en cines y VHS. 
  – 
 “Under the Skin” 
 La cinta en la que Scarlett Johannson demuestra que es mucho más que “Avengers” tiene una extraña dimensión en la que su personaje se alimenta de los humanos que seduce. La serie hace uso de ese espacio como la conexión entre Eleven y el monstruo, así como el lugar en el que Eleven lograba encontrar y escuchar a otras personas. 
  – 
 “Birdman” 
 Un poco sutil, pero el juego de luces (que también es un homenaje a otros directores) se parece a una de esas grandes escenas de la obra de arte de Alejandro González Iñárritu. 
  –  “The Goonies” 
 La emblematica cinta de uno niños en busca de un tesoro tiene la misma referencia de “E.T.” y “Stand by Me”. Un grupo de amigos que son víctimas y en general buenas personas. Pero esta cinta además tiene a un grupo de amantes de Dungeon and Dragons y esa escena en la que uno de los que molesta al grupo obliga a Truffle a “hacer esa cosa” sólo para burlarse de él.   
 – 
 Las películas de Quentin Tarantino 
 Después de algunos capítulos las referencias comienzan a ser claras, y así es como se puede encontrar este pequeño homenaje a la clásica toma desde el interior de la cajuela que Tarantino siempre usa. 
  –Directores homenajeados constantemente Steven Spielberg 
 Uno de los más comunes. Desde que los niños aparecen se nota la esencia del director estadounidense. Las dos cintas que más se proyectan son “E.T.” y “Close Encounters of the Third Kind”. Algunos de las referencias son las siguientes: 
 “E.T.” 
   
  
 – 
 “Close Encounters of the Third Kind” 
 Esta película tiene muchas referencias por la trama y el desarrollo de los personajes. El hombre que se niega a creer, pero que después se convierte en el más ferviente creyente, la historia que incluye a todo un pueblo, un padre que busca desesperadamente a sus hijos, y claro, la forma de comunicarse a través de luces y sonidos. 
   
  
 – 
 “Poltergeist” 
  
 – 
 David Cronenberg 
 “Videodrome” se relaciona debido a la criatura que sale de la pared; en la película de Cronenberg, esto pasa desde el televisor e incluso un cuerpo humano. “Scanners” lo hace a partir de la forma en la que Eleven puede asesinar a sus enemigos destruyendo su cerebro, claro, de una forma menos violenta que en la película. 
 – 
 John Carpenter 
 El director de cintas de terror está presente en obvias escenas como el momento en el que “The Thing” aparece en la televisión, pero el monstruo parece ser un gran homenaje a él y a Guillermo del toro. 
  
 – 
 Stephen King 
 Los libros de King son otra parte de la influencia. “The Body”, “It”, “Carrie” y más son referencias claras acerca de la historia. Amigos que viajan demasiado en bicicleta como en la adaptación cinematográfica de “The Body”, la pelea contra un ser superior usando una simple resortera y una niña con poderes mentales que pueden salirse de control. Además, todas las referencias a extrañas dimensiones y un mundo “oscuro” similar al nuestro es completo homenaje a Stephen King. 
  
 – 
 ¿Conoces más referencias? “Stranger Things” es una de las series que debes ver repetidas veces para captar todos esos easter eggs  que están escondidos. Tal vez después de terminar puedes ver algunas de las mejores películas de terror para prepararte para la segunda temporada, o si tienes una curiosidad grande como la de los niños de la serie,  estas teorías acerca del universo y nuestra realidad pueden servirte. 
 * Fuentes: 
 Vulture Zimbio