32 cosas que no sabías de los Premios Oscar

 
Una de las ceremonias más esperadas del año. El momento en el que se decide que película tuvo el mayor impacto y que pasará a la historia entre miles de otras que fueron producidas en ese año. Directores, actores, productores y más se reúnen cada febrero para atender a la premiación de los Oscar, y la gente conoce tan bien los premios que no puede evitar sentir felicidad o verdadero enojo cuando su actor o película favorita no gana.    Mucho se sabe de la entrega: que comenzó siendo transmitida por radio en 1929, que el origen del nombre Oscar es desconocido pero que lo más probable es que se llame así por el tío de una de las mujeres que estaba en la junta en la que se le dio nombre a la estatuilla, que durante la Segunda Guerra Mundial las estatuillas fueron hechas de arcilla entre otras cosas. Pero con casi noventa años de trayectoria, los premios esconden muchas anécdotas curiosas en su historia, estas son algunas de ellas.       La estatuilla mide 34 centímetros de alto y pesa 3.5 kilos. 
   Representa a un caballero parado en una cinta de cine sosteniendo una espada. Los cinco cuadros de la cinta de cine representan a los actores, directores, productores, técnicos y escritores.     Desde 1950 es ilegal vender la estatuilla si la ganas. Primero debes ofrecérsela a la Academia por el módico precio de un dólar.      A pesar de eso se dice que en 1999 Michael Jackson compró un Oscar por un millón de dólares.         Un estudio reveló que las mujeres que eran nominadas a mejor actriz tenían mayor probabilidad de divorciarse en un futuro próximo. Eso le sucedió a Kate Winslet, Reese Witherspoon, Sandra Bullock entre otras. El impulso profesional que la nominación puede traer hace que sea más difícil lidiar con las relaciones de pareja.


La alfombra roja que vemos horas antes de la ceremonia mide 152.4 metros.

   La ceremonia más larga ocurrió en 2002. La presentó Whoopi Goldberg y duró 4 horas y 23 minutos. La ceremonia más corta sucedió en 1959 y solamente duró una hora y 40 minutos.     Tatum O’Neal es el actor más joven en ganar un Oscar (fue en la categoría de mejor actor de reparto) por su papel en “Paper Moon” en 1973. Tenía 10 años.      El actor más viejo en ganar ese premio fue Christopher Plummer en 2010 por la película “Beginners”.     Por su parte Adrien Brody es el actor más joven en ganar en la categoría de mejor actor. A los 29 años se llevó el premio por su actuación en “The Pianist” (2002).     Henry Fonda fue el ganador más viejo en esa categoría cuando en 1981 ganó por la película “On Golden Pond”. Contaba con 76 años.      Liza Minnelli ganó el Oscar a mejor actriz en 1973 por su papel en “Cabaret”. Ese momento es histórico porque se convirtió en la única persona ganadora del Oscar cuyos padres también lo ganaron. Su madre, Judy Garland lo recibió en 1939 de forma honoraria y su padre, Vincent Minnelli, lo ganó en 1958 cuando dirigió la película “Gigi”.     Cate Blanchet actuó como Katherine Hepburn en la cinta “The Aviator” por lo que ganó el Oscar al actuar como otra ganadora del Oscar.     
Jack Nicholson es el actor con mayor número de nominaciones, 12 en total.
  
Woody Allen ha sido nominado 24 veces en múltiples categorías.


“Midnight Cowboy” es la única película para adultos que ha ganado el premio a mejor película.     
Kathryn Bigelow, esposa de James Cameron, es la única mujer que ha ganado el Oscar a mejor director. Lo hizo en 2010 con su película bélica “The Hurt Lucker”.     Meryl Streep es una de las figuras más emblemáticas de Hollywood. Con 19 nominaciones, ha ganado dos premios a mejor actriz: “Sophie’s Choice” (1983) y “The Iron Lady” (2012), así como uno a mejor actriz de reparto en “Kramer vs Kramer” (1980). 

   Walt Disney ganó 22 premios de 59 nominaciones. Además ha ganado cuatro premios honorarios.      La persona viva con más nominaciones es el compositor John Williams. Ha sido nominado 49 veces y ha ganado cinco estatuillas.     Kevin O’Connel ha sido nominado 20 veces en la categoría de mejor diseño de audio y no ha ganado, convirtiéndose en el mayor perdedor en la historia de los premios Oscar. Eso no es importante para él, pues considera que ser nominado ya es un gran honor.

     
La mujer con más estatuillas por mejor actriz se ha llevado cuatro. Katharine Hepburn ganó gracias a las películas “Morning Glory” (1933), “Guess Who’s Comming to Dinner” (1967), “The Lion in Winter” (1968) y “Golden Pond” (1981).     “Gone with the Wind” es la cinta de mayor duración que ha ganado el Oscar a mejor película. Con 234 minutos de historia, se llevó el premio en 1939.     Italia es el país que más cintas ha posicionado como mejor película extranjera con un total de 10.     
Greer Garson ganó el Oscar a mejor actriz en 1942 y dio el discurso más largo de la historia de la premiación, fueron casi seis minutos los que habló. Después de eso entro en vigor la regla de dar discursos de un máximo de 45 segundos. El discurso más corto fue el de Patty Duke en 1962, quien después de ganar el premio a mejor actriz de reparto simplemente subió al podio, tomó su premio y dijo “gracias”, para después alejarse.   

“The Godfather II” es la única secuela que ha ganado el Oscar a mejor película, lo hizo en 1975.     En la misma franquicia, Marlon Brando y Robert De Niro son los únicos actores que han ganado el Oscar al representar al mismo personaje, el gran Don Vito Corleone.

     Sidney Poitier fue el primer actor negro en ganar el Oscar. Lo hizo en 1964 por la película “Lilies of the Field”.      Por su parte Halle Berry fue la primer mujer afroamericana en ganar el premio. Lo ganó en 2002 por la cinta “Monster Ball”.      
Heath Ledger (“The Dark Knight”) y Peter Finch (“Network”) son lo únicos actores que han ganado el Oscar póstumamente.      Los presentadores de premios tienen prohibido decir “Y el ganador es…” en lugar de eso están forzados a decir “Y el Oscar va para…”     Así es como se hace una estatuilla dorada para los premios.



Seguramente este año será un gran año para la Academia. La competencia es reñida, la controversia racial da mucho de qué hablar y así como cada año parece que el Nobel puede ser para Murakami, puede que en unos días por fin le entreguen la merecida estatuilla a Leonardo DiCaprio. ¿Cuáles son tus películas favoritas para la edición 88 de la entrega de los premios Oscar?

***
Te puede interesar: 

Las peores ganadoras del Oscar a mejor película

Quiénes podrían y deberían ganar el Oscar 2016

***
Fuentes: MSN, The FW