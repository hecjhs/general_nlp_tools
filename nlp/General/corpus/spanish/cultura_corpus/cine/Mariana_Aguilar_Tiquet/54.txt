Ganadores Premios Goya 2015

 El pasado 7 de febrero de 2015, se llevó a cabo la 29 edición de los Premios Anuales de la Academia, también conocidos como Premios Goya. Durante la ceremonia, la Academia de las Artes y Ciencias Cinematográficas reconoció lo mejor del cine español. Entre los filmes nominados, destaca La Isla Mínima, la cual recibió 10 galardones de las 17 nominaciones que tenía.A continuación Cultura Colectiva te presenta la lista de los ganadores de los Premios Goya 2015:Mejor PelículaLa Isla Mínima Producción: Antena 3 Films (Mikel Lejarza, Mercedes Gamero), Atípica Films, S.L. (José Antonio Félez), Sacromonte Films, S.L. (Gervasio Iglesias) 
 Mejor dirección  
 Alberto Rodríguez por La Isla Mínima 
 Mejor Guión Original Rafael Cobos, Alberto Rodríguez por La Isla Mínima 
 Mejor Dirección de Fotografía 
 Álex Catalán por La Isla Mínima 
 Mejor Película Animada Mortadelo y Filemón contra Jimmy el Cachondo  Producción: Zeta Cinema (Francisco Ramos, Luis MAnso) Mejor Guión Adaptado  Javier Fesser, Cristóbal Ruiz, Claro García por Mortadelo y Filemón contra Jimmy el Cachondo 
 Mejor Interpretación Masculina Protagonista 
 Javier Gutiérrez por La Isla Mínima 
 
 Mejor Interpretación Femenina Protagonista 
 Bárbara Lennie por Magical Girl 
 
 Mejor Interpretación Masculina de Reparto 
 Karra Olejalde por Ocho Apellidos Vascos 
 
 Mejor Interpretación Femenina de Reparto 
 Carmen Machi por Ocho Apellidos Vascos 
 Mejor Actor Revelación 
 Dani Rovira 
 
 Mejor Actriz Revelación 
 Nera Barros por La Isla Mínima 
 
 Mejor Dirección de Producción 
 Edmon Roch y Toni Novella por El Niño 
 
 Mejor Película Documental 
 Paco de Lucía: La búsqueda Ziggurat Films (Anxo Rodríguez, Lucía Sánchez Varela) 
 Mejor Película Iberoamericana 
 Relatos Salvajes Damián Szifron 
 
 Mejor Película Europea 
 Ida Pawel Pawlikowsky 
 
 Mejor Cortometraje de Ficción Español 
 Café para llevar Patricia Font 
 
 Mejor Cortometraje de Animación Español 
 Juan y la nube Giovanni Maccelli 
 Mejor Documental Español 
 Walls (Si estas paredes hablasen) Miguel López Berraza 
 
 Mejor Sonido 
 Sergio Bürmann, Marc Orts, Oriol Tarragó por El Niño 
 
 Mejor Montaje 
 José M. G. Moyano por La Isla Mínima 
 
 Mejor Música Original Roque Baños por El Niño Mejor Canción Original 
 Niño sin Miedo por El Niño 
 
 Mejor Dirección Artística 
 Pepe Domínguez por La Isla Mínima 
 
 Mejor Diseño de Vestuario 
 Fernando García por La Isla Mínima 
 
 Mejor Maquillaje y Peluquería 
 Pedro Rodríguez “Pedrati”, José Quetglas, Carmen Veinat por Musarañas 
 
 Mejores Efectos Especiales 
 Raúl Romanillos, Guillermo Orbe por El Niño 
 
 Mejor Dirección Novel 
 Carlos Marques-Marcet por 10.000 km