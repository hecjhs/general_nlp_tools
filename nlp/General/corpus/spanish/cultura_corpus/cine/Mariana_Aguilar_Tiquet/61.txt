Cirque du Soleil abre parque temático en México

 Se planea que para el 2018, el primer parque de atracciones temático de Cirque du Soleil abra sus puertas al público en Nuevo Vallarta, Nayarit. El proyecto que se encuentra en planeación, podría incluir un parque acuático y uno ecoturístico en el que se montarán shows al aire libre para recibir entre 3 mil y 5 mil espectadores.  La compañía de entretenimiento y producción de espectáculos canadiense, que inició operaciones en 1984, ha estado trabajando en conjunto con Grupo Vidanta, una empresa de desarrollo e infraestructura turística de lujo. Las dos compañías han desarrollado una producción teatral y culinaria titulada Joyà, la cual se estará presentando desde su estreno reciente, el 8 de noviembre, en el Teatro Vidanta, ubicado en la Riviera Maya.  La noticia del nuevo parque, se dio a conocer días cercanos al estreno de Joyà, el primer espectáculo permanente de Cirque du Soleil fuera de Las Vegas y Orlando. Incluso antes de su estreno ya tenía una gran demanda, lo que ha demostrado el interés por parte del público en este tipo de espacios.

Guy Laliberté, fundador y presidente de Cirque du Soleil, declaró que “México emerge como uno de los destinos principales en el mundo para vivir experiencias de entretenimiento sin precedentes”. El presidente confía en que el parque dará muy buenos resultados y que sin duda será exitoso.   Por su parte Daniel Chávez Morán, fundador de Grupo Vidanta, aseguró que los futuros proyectos con la empresa circense serán exitosos: “Cuando los asistentes sean testigos de lo que fuimos capaces de hacer en Nuevo Vallarta, sabrán que ahora tenemos la capacidad de construir nuestros sueños”.   Se prevé que con la creación del parque de atracciones el turismo aumente en el estado de Nayarit, además de que generará miles de empleos. 
  
http://culturacolectiva.com/wp-content/uploads/2014/11/Cirque-Du-Soleil-1.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/cirquedusoleil4.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/065684-dralion-cirque-du-soleil.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/cirquedusoleil6.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/cirquedusoleil2.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/cirquedusoliel3.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/cirquedusoleil.jpg
  
 *** 
 Con información de Forbes y El Universal