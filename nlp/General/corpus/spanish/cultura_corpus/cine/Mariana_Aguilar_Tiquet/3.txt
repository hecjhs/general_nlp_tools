Fotos detrás de cámaras de tus series favoritas de los 90

 La década de los 90 fue en la que muchos cumplimos los primeros años de vida o en la que llegamos al mundo. Fue la década en la que abrieron los ojos los primeros millennials y en la que Internet se volvió parte de nuestra realidad. Los 90 fueron años que muchos recordamos con nostalgia; años que han quedado grabados en la memoria por la moda, por utilizar pantalones aguados, por llevar la chamarra en la cintura y por colocar una dona en la muñeca de la mano. Los recordamos también por videojuegos como Super Mario Kart, Super Mario 64 y Zelda 3. Por boybands como Backstreet Boys  y series de televisión como Clarissa lo explica todo. 
 Nacimos en un mundo dividido, disputado en la llamada Guerra Fría y vimos cómo ello terminó; aunque otros llegaron sólo para escuchar sobre ello, sólo para darse cuenta de la fugacidad de la vida, de la velocidad de las conexiones y de la importancia tecnológica. Crecimos rodeados de cables y nos amarramos a ellos; permitimos que la evolución de ambos dependiera del otro. Nos comunicamos como nunca antes se había hecho, aunque en aquellos años aún no supiéramos lo que estaba por venir. Comenzaron los realities shows y la televisión se transformó. Fue esta caja la que nos acompañó durante la última década antes del nuevo milenio. Una década decisiva para el futuro de la humanidad. Una que muchos recuerdan con ojos nostálgicos y que anhelan, otros que la ven como años de risas y de infancia. Una década marcada por innumerables sucesos culturales e históricos, pero que fácilmente recordamos y a la que nos es fácil regresar a través de la caja que muchos llaman idiota. Una caja que nos trajo también grandes series, algunos dirán que como esas que ya no se hacen. Series televisivas que nos hacían sentir, soñar y olvidar. 
 Para recordar la década que para muchos fue nuestra favorita, te dejamos una selección de fotografías del detrás de cámaras de las series que marcaron nuestra infancia. La última década en la que fuimos inocentes. 
  Seinfeld 
  Friends 
  John Stamos y las gemelas Olsen, Full House 
  Dawson’s Creek 
  Clarissa lo explica todo 
  Los Expedientes Secretos X 
  El Príncipe del Rap 
  Anthony Stewart Head, Alyson Hannigan, David Boreanaz, Buffy la cazavampiros 
  Seinfeld 
  Full House 
  Friends 
  Dawson’s Creek 
  Los Expedientes Secretos X Britney Spears y Melissa Hart en  Sabrina la bruja adolescente 
  Seinfeld 
  Paso a Paso 
  Aprendiendo a vivir 
 
 *** 
 Te puede interesar:  Fotos íntimas de Heath Ledger detrás de cámaras