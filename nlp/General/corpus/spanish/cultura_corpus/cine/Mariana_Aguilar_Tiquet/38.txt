Documentales que puedes ver en Netflix

 Contamos con muchas herramientas para conocer el mundo; para saber qué sucedió en el pasado y así comprender nuestro presente. Pero, también hay incontables situaciones que ocurren en estos momentos, quizá delante de nosotros o al otro lado del mundo que ignoramos. Ya sea por nuestra situación geográfica o la falta de difusión por parte de los medios masivos de comunicación, hay algunas historias que nunca hemos escuchado. Historias que no han logrado penetrar nuestra pequeña burbuja. Historias que merecen la pena ser contadas y escuchadas. 
 El documental, expresión audiovisual que tiene como finalidad comunicar para dar evidencia de un hecho del pasado o presente, hace uso de testimonios para dar a conocer situaciones de cualquier tipo. En muchas ocasiones ha sido utilizado como una herramienta visual que testifica, y que da voz y rostro a aquellos que han permanecido en las sombras. Con un enfoque social, ha sido un medio ideal para denunciar y gritar lo que muchos quieren que permanezca silenciado; aunque también ha servido para dar a conocer las sombras de aquellos que siempre han estado ante los reflectores. Esas anécdotas que pocos conocen o lo que hay detrás de una estrella, ha podido conocerse por investigaciones realizadas que se dan a conocer con el documental. 
 Con la finalidad de informar y mostrar la realidad, el documental ha logrado crecer y atrapar al público. Periodistas, cineastas, escritores y demás unen fuerzas para indagar y presentarnos historias visuales que vale la pena conocer. Aquí te dejamos algunos documentales que puedes ver en Netflix. 
  Smash his camera (2010) 
Dirigido por Leon Gast  
 Hemos visto incontables fotografías de celebridades en las redes y revistas, sobre todo de aquellas especializadas en chismes. Sabemos que son imágenes que fueron tomadas por fotógrafos paparazzi, pero la realidad es que están muy lejos de las fotografías que solían tomarse en la década de los 60 – 70. Este documental se centra en la vida y trabajo de Ron Galella, figura clave en el desarrollo de los paparazzi. A lo largo de 90 minutos se conocerá el trabajo polémico del pionero de los paparazzi y debatirá la obsesión que se tiene con ciertas figuras públicas. 
    
 David Bowie: Five Years (2013) 
Dirigido por Francis Whately Uno de los músicos más emblemáticos de la década de los 70, con una experiencia de más de cinco décadas en la industria musical, merecía un documental. David Bowie: Five Years muestra los cinco años más importantes en la vida del famoso músico británico responsable de sencillos como “Space Oddity” y “Fame”. 
    
 Hot Girls Wanted (2015) 
Dirigido por Jill Bauer y Ronna Gradus  
 Con un tema profundo y delicado, Hot Girls Wanted da a conocer la vida de jóvenes de entre 18 y 19 años que trabajan como actrices porno. El documental estrenado en el Festival de Cine de Sundance 2015, muestra lo sencillo que es para las nuevas generaciones dedicarse al porno. 
   
  
 Good Ol’ Freda (2013)  Dirigido por Ryan White  Ringo Starr con Freda Kelly y George Harrison, 1967. Difícilmente se puede decir de alguien que nunca haya escuchado una canción de The Beatles, y menos que no sepa quiénes son los cuatro músicos originarios de Liverpool. Lo que sí es probable es que el nombre Freda Kelly no sea tan sonado. Kelly fue la secretaria del cuarteto, misma que fue testigo del éxito de la banda y que conoció detalles que nadie más supo. Este documental da a conocer la vida tanto de Kelly como de la emblemática banda de rock.    
 The Garden (2008) 
Dirigido por Scott Hamilton Kennedy 
 Este documental muestra la vida de un grupo de familias de bajos ingresos que luchan por conservar y proteger un huerto localizado en Los Ángeles, California. Este documental social fue nominando en 2008 a un premio de la Academia, y fue galardonado con el Premio del Jurado en Silverdocs Documentary Festival. 
    
 Blackfish (2013) 
Dirigido por Gabriela Cowperthwaite 
 Quizás algunos hayamos presenciado la majestuosidad e inteligencia de animales marinos en parques como Sea World, tal como es el caso de las orcas, pero son animales que nacieron para ser libres y no puestos en cautiverio. El documental, explora y analiza el caso de la ballena Tilikum, luego de que ésta matara a su entrenadora en 2010. Cowperthwaite muestra la historia de la ballena asesina desde su cautiverio en 1983, al tiempo que cuestiona los parques que mantienen a estos animales en piscinas. 
   
 
  Jesus Camp (2006) 
Dirigido por Rachel Grady y Heidi Ewing 
 Este documental muestra la historia de tres niños que asisten a un campamento de verano para transformar sus vidas y convertirse en activistas políticos cristianos. Durante el campamento se les dice que ellos pueden ser los que regresan a Cristo a Estados Unidos, por lo que tienen una gran responsabilidad en sus manos. El polémico documental fue nominado a un Oscar. 
   
  Gonzo: Vida y hazañas de del Dr. Hunter S. Thompson (2008) 
Dirigido por Alex Gibney 
 El documental gira en torno al fundador del periodismo gonzo, en el que el periodista se convierte en parte fundamental de la historia y un protagonista más. Hunter S. Thompson se caracterizó por su estilo narrativo único y extravagante. A lo largo del documental se puede conocer más sobre el coraje y el estilo de vida del escritor vanguardista. 
   
 
  Auschwitz: Los nazis y la solución final (2005) 
 Se trata de una serie documental que cuenta uno de los genocidios más desgarradores de la historia: el Holocausto. Contado en seis episodios producidos por la BBC, los hechos se centran en el campo de concentración de Auschwitz; uno de los más atroces durante la Segunda Guerra Mundial. La serie documental utiliza tanto testimonios como grabaciones para crear la historia que cuenta el terrible genocidio que marcó al mundo para siempre. 
   
  Narco Cultura (2013) 
Dirigido por Shaul Shwarz 
 El documental gira alrededor de la problemática de las drogas y el narcotráfico en Ciudad Juárez, México. A lo largo de poco más de 100 minutos, Narco Cultura muestra cómo el tráfico de drogas ha representado para algunos un “nuevo sueño americano”. Convertirse en narcotraficante es la manera de dejar a un lado la pobreza, pero sobre todo de ser alabados y respetados. El documental dirigido por el fotógrafo de guerra Shaul Shwarz, muestra cómo el dinero y poder ha desatado una guerra en México y la frontera con Estados Unidos.