Películas que nos demuestran que el salvajismo del hombre está matando al planeta

  “La Tierra proporciona lo suficiente para satisfacer las necesidades del hombre, no la codicia de cada hombre”. -Gandhi   El 22 de abril de 2010 se detectó en el Golfo de México  un grave derrame de petróleo. El desafortunado accidente ocurrió el mismo día en el que se celebraba, en el mundo entero, el Día de la Tierra. Este desastre ecológico representó -y representa- una nueva amenaza para el medio ambiente que afectará durante años, pues a pesar de haber transcurrido poco más de seis, aún se encuentran restos de fauna marina afectada por esta situación. 

 La cinematografía, esa otra realidad que retrata y relata las pasiones, deseos y fines más bajos de la humanidad, nos ha sorprendido con ficciones en las que escenarios catastróficos ponen en peligro la existencia de nuestra raza en la Tierra: virus, criaturas mutantes y guerras nucleares son las historias preferidas. A todos nos gustan y solemos sentirnos afortunados por no estar en ese entorno apocalíptico; sin embargo, olvidamos que vivimos en un lugar donde la realidad supera la ficción. Basta con ver las noticias, leer el periódico o investigar un poco para saber que matamos al planeta progresivamente desde hace mucho tiempo con deforestaciones, extinción de especies, guerras, consumismo y contaminación que acelera cada vez más el cambio climático, ese que en las últimas décadas se ha vuelto un problema debido a su crecimiento acelerado a causa del modo de vida del hombre. 

 La industrialización exponencial, el uso de combustibles fósiles, las deforestaciones y la poca cultura e información que tiene la sociedad al respecto, convierten al cambio climático en una de las principales preocupaciones de los distintos gobiernos alrededor del mundo.  El maltrato que hemos dado a nuestro planeta se torna en situaciones que nos ponen en un escenario cada vez más peligroso, debemos reflexionar en el futuro que nos espera si continuamos con estos “ataques” hacia el único lugar donde sabemos que, hasta hoy,  podemos conservar la vida.  Y ya que nuestra realidad supera los escenarios de ciencia ficción, te invitamos a ver los siguientes filmes que reflejan las consecuencias de los múltiples daños que el ser humano le ha provocado a la Tierra desde hace varias décadas: 
–

“Acción Civil” (1998)


 Otra historia basada en hechos reales que aborda el problema de la contaminación del río Aberjona, en Nueva Inglaterra, debido a los desechos de dos emporios industriales. El filme analiza el proceso legal que enfrenta  Schlichtmann (John Travolta), un ambicioso abogado que fingía ayudar a las personas afectadas para después estafarlas. 

–

“Contaminación” (1970)


 La contaminación en la Tierra llegó a niveles extremos y ha generado la aparición de un virus letal que está matando a la humanidad. Los países del mundo se enfrentan a una guerra para erradicar el foco de infección, mientras la gente huye en busca de un mejor lugar para vivir. –
 
“Tierra Prometida” (2012)
  La trama de esta cinta se desarrolla en un pequeño pueblo de agricultores amenazados por una empresa de gas que pretende adueñarse de la tierra para realizar perforaciones. Debido a la necesidad económica, los pobladores se ven tentados a ceder; sin embargo, un ecologista y un vecino anciano intentan impedir la situación.


   
“No Hay Mañana” (2012)


 Este cortometraje animado habla sobre el inminente colapso de la Tierra en medio del agotamiento de los recursos naturales y la inestabilidad económica mundial. Remarca la dependencia que tenemos hacia el petróleo y otros combustibles fósiles, así como las consecuencias que esto conlleva. Expone, además, una serie de posibles soluciones.


Los Simpson: la película Matt Groening, además de festejar las inagotables temporadas de los simpson,  con esta secuencia cinematográfica nos exhorta a la reflexión ambiental. Bajo este matiz dramático, todo Springfield tendrá que enfrentar las consecuencias de su descuido hacia la Tierra, así que su ciudad se sella en una bola de cristal gigante para evitar expandir su desastre.  La trilogía qatsi (Qatsi trilogy): Koyaanisqatsi, Powaqqatsi y Naqoyqatsi Toda una trilogía que gira en torno al papel del hombre hacia dos concepciones vitales para su verdadera realización: el respeto por la Tierra como hogar de origen y la compleja dinámica de cohabitar en las ciudades. Estas tres historias componen la Qatsi Trilogy, las cintas fueron dirigidas por Godfrey Reggio, producidas, entre otros, por Francis Ford Coppola y Steven Soderbergh. Wall-E La devastación llevad a un punto de exageración caótica que para el día de hoy no parece ninguna fantasía sino una triste realidad futura; Wall-e, un robot diseñado para apilar la basura de un planeta devastado y abandonado debido al deterioro ambiental, este simpático personaje será testigo de un suceso que traerá a los humanos gordos y despreocupados a su antiguo hogar. Los hijos del Hombre Un filme dirigido por Alfonso Cuarón y estrenada en 2006, una trama desarrollada en un mundo arrasado por guerras, terrorismo nuclear, contaminación sin solución posible, suicidios en masa, donde los hombres han perdido la capacidad de procrear y se ignora por qué razón las mujeres del planeta se han vuelto estériles. 
   Erin Brockovich Una película basada en una historia real en la que Julia Roberts representa a Erin Brockovich, una activista que se enfrenta a la negligencia empresarial debido a la contaminación del agua a cargo de una empresa de prestigio, Erin deberá implementar todos los medios a su alcance para denunciar tal atrocidad.   
 After Earth  Un aterrizaje de emergencia deja al joven Kitai Raige (Jaden Smith) y a su legendario padre Cypher (Will Smith) atrapados en el planeta Tierra, 1.000 años después de que una serie de catastróficos eventos obligaran a la humanidad a escapar de ella.  
El objetivo de estos filmes es crear consciencia para comenzar a generar un cambio y preservar nuestros ecosistemas.  En México,  SEMARNAT  ha realizado distintos proyectos de restauración, protección y conservación de sitios naturales que en muchas ocasiones se han visto amenazados por la inconsciencia del hombre. Con tu ayuda podemos preservar el medio ambiente. Nuestra misión es estar informados y hacer labores propias y colectivas que a largo plazo nos aseguren un mejor lugar de vida.