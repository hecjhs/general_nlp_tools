Películas que te harán amar el futbol americano

     Los segundos en el reloj se agotan, el balón llega a tus manos, das pequeños pasos laterales sobre las puntas de tus pies mientras tus ojos analizan las diversas posibilidades. La derecha está imposible; a tu izquierda, una barrera evita que quedes a merced de una tacleada… en el centro ves a tu corredor esquivando bloqueos.

Tu brazo se convierte en una catapulta que se activa sólo segundos antes de ser tacleado por el gigantesco número 68 del equipo rival, el oviode se aleja, vuela diez, quince, veinte yardas, tu corredor, ya en zona de anotación, hace un salto casi increíble, el brazo se estira y la yema de los dedos alcanza a rozar el balón y quitarle fuerza; se dirige hacia el suelo y el balón junto a él, las rodillas están por tocar el suelo y a pocos centímetros ocurre lo impensable: una mano aprisiona el ovoide y no lo suelta nunca, esta vez la suerte jugó de su lado.

    El futbol americano es uno de los deportes más apasionantes que existen, cada segundo de partido es una posibilidad de ver jugadas extraordinarias y vivir emociones extremas.

El séptimo arte ha aprovechado estas sensaciones para plasmarlas en celuloide, dando como resultado cintas como las que presentaremos a continuación; películas que te harán amarlo más si eres fanático del deporte, y te enamorarán si nunca has estado en contacto con él.  
The Blind Side (2009)
John Lee Hancock


  Conocida en español como Un Sueño Posible, esta cinta, basada en hechos reales, cuenta la historia de Michael Oher, tacleador estrella de las Panteras de Carolina. Nacido en Memphis, Tennesse, Oher se refugió en el futbol americano tras vivir una adolescencia complicada, encontrando valiosas personas y descubriendo su potencial.  Gracias a su guión inteligentemente escrito, The Blind Side fue nominada al Oscar a mejor película en 2009.

     Radio (2003)
Mike Tollin    No sólo hay personajes memorables de futbol americano sobre el terreno de juego, también los hay fuera de él, y prueba de ello es James Robert “Radio” Kennedy. Basada en hechos reales, esta cinta cuenta la historia de “Radio” Kennedy, un enfermo mental que sueña con jugar fútbol de manera profesional y que con la ayuda del coach Harold Jones, logra ser parte del staff del equipo de la preparatoria T.L. Hanna.  Radio hará que te des cuenta que el futbol americano tiene el potencial de hacerte feliz.  

  Undefeated (2011)
Daniel Lindsay y T.J. Martin


  El equipo preparatoriano Manassas Tigres de Memphis nunca ha tenido una temporada ganadora y están dispuestos a revertir eso. En este documental de 2011, Daniel Lindsay y T.J. Martin retratan tanto la tensión en los vestidores como el esfuerzo y los sacrificios que se deben hacer para convertirse en el mejor equipo de la temporada.  Inspirador y emotivo, Undefeated ganó el Oscar a Mejor Documental en 2011.

  Any Given Sunday (1999)
Oliver Stone  

El entrenador de los Miami Sharks, Tony D’Amato, debe lidiar con un matrimonio que se está derrumbando y unos hijos que comienzan a distanciarse, al mismo tiempo de levantar al equipo en la crisis en la que está inmerso. Al Pacino brinda una de sus mejores interpretaciones en esta cinta, una de las primeras que muestra el lado más doloroso del futbol americano, razón por la que la NFL no le brindó su apoyo.  Any Given Sunday recibió críticas polarizadas en su lanzamiento, pero con el tiempo se ha convertido en una importante cinta de este deporte.

  The Replacements (2000)
Howard Deutch  

Inspirada en la huelga de jugadores de la NFL en 1987, The Replacement cuenta la historia del peculiar equipo de suplentes de los Washington Sentinels, quienes están obligados a ganar tres de sus cuatro últimos partidos para llegar a los playoffs y luchar por el título. Jimmy McGinti reúne a un extraño grupo de personajes, liderados por el quarterback Shane Falco,  para ser el equipo suplente y lograr la hazaña.  La cinta nos enseña que no es necesario ser el jugador más técnico, el más fuerte o el más inteligente, lo único necesario para ser un ganador de las canchas es tener corazón.

  Remember The Titans (2000)
Boaz Yakin


  A principios de la década de los 70, la preparatoria TC Williams High School contrató al entrenador afroamericano Herman Boone para dirigir al equipo de la escuela, compuesto principalmente por blancos, en un intento por frenar la segregación racial. Esto causó que el equipo se dividiera y los problemas raciales se incrementaran; sin embargo, Boone, gracias a mucha motivación y extensos entrenamientos, logró la unión y los convirtió en el equipo a vencer de la liga.  Esta cinta también está basada en hechos reales.    The Longest Yard (2005)
Peter Seagal


  Después de que el exmariscal de campo de los Pittsburgh Steelers, Paul Crewe, llega a prisión, es obligado a entrenar a un grupo de reos y formar un equipo para jugar contra los guardias de la prisión.  Esta comedia con pequeños momentos dramáticos, muestra un lado más relajado del futbol americano y nos enseña los lazos de amistad que se pueden crear con el deporte y cómo, también, puede convertirte en una mejor persona.    Little Giants (1994)
Duwayne Dunham  

Los hermanos O’Shea dirigen equipos infantiles de futbol en su natal Urbania, Ohio, Danny con niños distraídos y poco hábiles en el deporte, y Kevin un equipo más profesional que entrena duramente y es bastante bueno. Al ser los únicos equipos del pueblo, deciden jugar un partido en el que el ganador se quedará como el único equipo y así representar Urbania.  En esta cinta se aprecian dos maneras de entender el futbol americano, una divertida y apasionada y la otra más fría y calculadora, estilos distintos de disfrutar el juego del ovoide.  
 
  We Are Marshall (2006)
McG  

El 14 de noviembre de 1970 ocurrió una de las mayores tragedias en la historia del deporte estadounidense: el vuelo 932 de Southern Airways se desplomó y acabó con la vida de todo el equipo de futbol de la Universidad Marshall. Se tiene que empezar de cero y formar un nuevo equipo, y para ello se elige a Jack Lengyel, un  joven entrenador con experiencia casi nula, quien tiene la difícil misión de formar un equipo y hacer olvidar la tragedia de semanas atrás.

  Jerry Maguire (1996)
Cameron Crowe  

Tras descubrir la deshonestidad que se encuentra en algunos agentes deportivos, Jerry Maguire decide abandonar la agencia en la que trabajaba para comenzar a manejarse de manera independiente. Los deportistas que representaba lo abandonan y sólo queda con Rod, una joven estrella del futbol americano, como cliente. Entre ambos se crea una interesante amistad en la que ambos se benefician, Rod escalando en el mundo del americano y Jerry mejorando su calidad como persona.  Con esta cinta, el director Cameron Crowe trató de demostrar que en el deporte las amistades trascienden más que los intereses.

  Drama, tensión, comedia, estas cintas proyectan las mismas emociones que vemos en cada juego los fines de semana, y nos recuerdan porqué el futbol americano es uno de los deportes que toca las fibras más sensibles de nuestro cuerpo.

    Oscar Mayer quiere que tú también vivas de cerca esta explosión de emociones, y junto con la NFL te invitan al Super Bowl 50, a realizarse el 7 de febrero de 2016 en San Francisco, California.  Sólo debes comprar productos Oscar Mayer y registrar tus tickets en www.oscarmayerpromonfl.mx para concursar por uno de los viajes dobles y asistir al evento deportivo más importante del año.

Además, al ingresar tus tickets podrás acumular puntos para ganar boletos gratis de cine y premios oficiales de la NFL (gorras, balones, cascos, tazas, tarros, jerseys, etc).

¡Participa y vive esta increíble experiencia!