Lo que tu película favorita dice de tu personalidad

 “Apenas sé, señora, lo que soy en este momento… Sí sé quién era al levantarme esta mañana, pero creo que he cambiado varias veces desde entonces”. – Alicia en el país de las maravillas 
 Lo que tus ojos ven va más allá de una simple imagen capturada en el tiempo y en el espacio para ser almacenada en tu cerebro o ser decodificada por sólo algún momento antes de ser un desecho; si tal fuera el caso, no podríamos exaltar al corazón con el puro pensamiento de nuestro ser amado , no habríamos aprendido nada de nuestros padres o no estaríamos tan apegados a un específico tipo de arte, etcétera. Quizá eso de que los globos oculares están conectados al cerebro por un nervio que convierte la información en un impulso eléctrico sea mentira, no puede ser verdad… es demasiada clínica para un fenómeno tan fantástico. Debe haber algo mágico detrás de todo ello que todavía no logramos entender, una especie de hilos invisibles que enlacen lo visto, entendiéndole como queramos, con el alma o con una configuración alejada de la ciencia que explique por qué las impresiones del mundo sensible tienen tal efecto en quienes somos. Como si todo lo absorbido tejiera una compleja red de amores, odios, miedos, angustias o pasiones que nos envuelven desde dentro en una persona que nos asombra a nosotros mismos en cada oportunidad de ser vista. 
  Algo así podríamos ejemplificarlo en el ingenuo acto de ver una película; cuando esas historias, esa fotografía y esos diálogos impactan de una manera más notoria que lo que pensamos. Nuestra película favorita puede hablar de nuestro interior (o transformarlo) con mayor claridad que incluso la propia voz y ser un elemento determinante en cómo narramos la historia de una vida o actuamos en ella. 
  En el siguiente listado se encuentran algunas películas que bien pueden entrar en ese juego de tratar de descubrir el quién y el cómo de las personas, a partir de piezas constituyentes en su forma y en su contenido; puede que no acertemos del todo, pero es un ejercicio interesante en la clasificación del cinéfilo en conjugación con el yo humano. “El ángel exterminador” (1962), Luis Buñuel 
  Lo inesperado es parte de tu ser, no soportas la idea de algo fijo ni de caer en el lugar común donde los demás encuentran la paz y el sosiego. Te gustan esos detalles sorpresa y los múltiples significados que pueden adquirir tus decisiones. Nadie se puede aburrir junto a ti; mucho menos durante una charla con café. 
  
 “The dreamers” (2003), Bernardo Bertolucci 
  Tu pasión por todo lo que te rodea es algo que siempre logras transmitir en los demás; no hay regla para ti que no hayas escrito por tu cuenta y no sabes lo que significa tener que rendirle cuentas a otro. Aprecias los momentos románticos, pero también encuentras cierto encanto en el conflicto. 
  
 “Cloud Atlas” (2012), Tykwer y hermanos Wachowski 
  Eres una persona altamente introspectiva que gusta de medir bien sus pasos al mismo tiempo que se embarca en aventuras poco ortodoxas. Tu memoria es inigualable y tu corazón lo es aún más, nadie puede dudar de ti en tiempos complicados pues siempre eres una mano amiga que sabe cómo actuar. 
  
 “El último tango en París” (1972), Bernardo Bertolucci 
  Cuando se piensa en alguien arrebatado o incluso vanidoso el primer rostro que se presenta es el tuyo. Las personas saben que eres alguien interesante, pero en ocasiones temen a tus arranques de furia o al tener que confrontarse contigo, sobre todo en situaciones de trabajo. 
  
 “Annie Hall” (1977), Woody Allen 
  La ruptura con lo establecido es un asunto prácticamente tuyo, aunque también aprecias esos momentos de estar sobre terreno seguro. Seguramente muchos de tus amigos o pareja saben que eres la neurosis andando, pero no pueden dejar de divertirse y aprender contigo. Sabes crear verdaderos lazos humanos con los otros a pesar de tu cierto rechazo a la sociedad. 
  
 “El resplandor” (1980), Stanley Kubrik 
  Hay un carácter multifacético al que no puedes renunciar fácilmente, tu extravagancia suele espantar a algunos, pero enganchar a otros; sobre todo cuando se trata de análisis duros en cuanto a la gente y sus actitudes. Reconoces bastantes de tus errores, pero prefieres no decirlos y seguir adelante. A veces no estaría mal calmar tus ataques de ira o ser menos hermético ante el mundo. 
  
 “Harry Potter (saga)” (2001 – 2011), Varios directores 
  Radica en tu interior esa ferviente idea de que las cosas no pueden ir a peor siempre y cuando tomes cartas en el asunto y te sientas apoyado por alguien; y esto no en un sentido egoísta, pues sabes que también debes estar para los demás cuando requieran de ti. La responsabilidad y la preocupación por los tuyos son tu sello distintivo. 
  
 “Scott Pilgrim vs The World” (2010), Edgar Wright 
  Eres alguien que goza al no verse envuelto en la manera cotidiana de pensar; preferirías estar en la quietud de tu hogar si no pudieras hacer frente a la vida desde tu particular y relajado punto de vista; no hay manera de pensarte como un ser infantil, pero esa manera supuestamente “madura” de ver la vida no es para ti. 
  
 Como siempre, sabemos que esto no es un recetario o un manual  irrefutable, cada quien es dueño de lo que está sucediendo dentro y fuera de sí mismo, o por lo menos eso es lo que pensamos; pero hay cosas que sobrepasan al entendimiento humano y dejan una huella imborrable en todo lo que sale de nuestra boca o se efectúa con el cuerpo ¿Qué otras películas podrían definir la personalidad de alguien o ser muestra de ella y estamos olvidando? 
 *** Te puede interesar: Lo que tu banda favorita dice de tu personalidad El trago que pide en la primera cita te dice más sobre su personalidad