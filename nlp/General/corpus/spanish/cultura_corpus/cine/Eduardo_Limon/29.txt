7 documentales que nos muestran la miseria, el horror y la violencia de vivir en México

 La muerte es un negocio en nuestro país y todos lo sabemos. Ya sea como el acto de quitar la vida o documentar las consecuencias de esto. A lo largo de nuestra historia, la “nota roja” ha sido una acción cotidiana y un objeto de culto entre los mexicanos, acostumbrados a la imagen de la violencia como a cualquier otra. La manera en la que nos relacionamos con la pérdida del pulso va más allá de un juicio superfluo que asegure que en esta región nos burlamos de esa temida hora. Esto ya no tiene nada que ver con el seguimiento de una cultura ancestral. 
  Por el contrario, la asimilación actual obedece a razones políticas o económicas que involucran al gobierno, a la delincuencia y al tráfico de lo ilegal. Incluso podríamos hablar de la violencia y la muerte como elementos hiperestetizados en nuestra sociedad como expresiones del tortuoso día a día. Vivir en México conlleva un miedo constante ante las prácticas distópicas del funcionamiento de una nación y esto se ha tomado como objeto de estudio en producciones artísticas que critican su presencia, aunque, lamentablemente, no consigan en ocasiones obviar su denuncia, sino darle continuidad a su aparición en una suerte de violencia sistematizada. A pesar de las promesas por el gobierno mexicano de haber superado estos momentos de terror y expectativa terrible, dentro de esas prácticas del arte se encuentran documentales que lo desmienten; que dan esa visión, sí, valiente, pero también habituada a hablar de estos sucesos con la confianza de no ser (catastróficamente) enjuiciados. 
 – “El hombre que vio demasiado” (2015) – Trisha Ziffs 
  Un ejemplo es este documental que trata la vida de Enrique Metidines como ese mítico sujeto obsesionado con la fotografía del crimen, quien nos enseñó poco a poco la cultura del “mirón” que no puede dejar escapar un accidente o un asesinato sin la necesidad de empapar nuestros ojos con la fatídica escena. 
  
 – “El Paso” (2015) – Everardo González 
  Producción documental que aborda la situación de esos periodistas mexicanos refugiados en Texas y que buscan el asilo político tras haberse envuelto en conflictos gubernamentales y delictivos más de la cuenta. Una mirada íntima a las familias y afectados por una degradación social que parece no detenerse. 
  
 – “Reportero” (2012) – Bernardo Ruíz 
  A manera de una denuncia fílmica, Ruíz expone en sus escenas la consistencia que han adoptado las malas decisiones de una presidencia calderonista, el fracaso de creer que México es un país con libertad de expresión y el sacrificio que implica no guardar silencio ante circunstancias de corrupción y narcotráfico. 
  
 – “¿Es el Chapo?” (2014) – Charlie Minn 
  A partir de una singular mirada, el artista neoyorkino expone en su filmación un evento del que muchos dudamos e incluso en el extranjero pareció una burla: la supuesta aprehensión de Joaquín “El Chapo” Guzmán en 2014. Una captura demasiado conveniente para el momento político que atravesaba México. 
  
 – “Drug Lord: The Legend of Shorty” (2014) – Guillermo Galdós y Angus McQueen 
  Esta película toma a Joaquín Guzmán como protagonista y desdobla su evolución como la máxima autoridad en el narcotráfico a partir de entrevistas a familiares, amigos, aliados, trabajadores y el mismo delincuente. Él juega un papel bastante peculiar en nuestro país y en el mundo al ser considerado como héroe en ciertos puntos de la nación y como una pieza clave para los gobiernos americanos. 
  
 – “Ni vivos ni muertos” (2014) – Federico Mastrogiovanni 
  Mastrogiovanni es un periodista que desarrolló la investigación suficiente como para publicar un libro y posteriormente este documental bajo el presente nombre. Él rastreó la historia de la desaparición forzada en México, su trabajo dio comienzo en los años 70 y termina en la actualidad para demostrar la poca importancia ciudadana que se le da a este problema. 
  
 – “Narcocultura” (2013) – Shaul Schwarz 
  ¿Qué tanta admiración se tiene hacia el narcotráfico en México? Con esa pregunta se guía el trabajo documental de Schwarz para, en cada segundo, hacernos sentir culpables y avergonzados por sostener una cultura que poco a poco ha vuelto su mirada a un capitalismo gore, fascinada por supuestos ídolos que ahora cuentan con su propia música e indumentaria. 
  
  El horror de ser mexicano es una condición que deberíamos analizar con más detalle, con el suficiente ojo crítico como para saber si denunciamos o vanagloriamos esas prácticas que perjudican a nuestros cuerpos , mentes y modos de actuar. 
 *** Te puede interesar: 
 10 obras de arte alrededor del mundo para entender la violencia 
 Más allá del Chapo; narcotraficantes que marcaron nuestra historia 
 * Referencia: 
 Remezcla La Opinión