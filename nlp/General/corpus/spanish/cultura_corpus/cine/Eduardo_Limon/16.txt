10 películas inspiradas en la filosofía de Heidegger

 Muchos aseguran que el filósofo que cambió el rumbo del pensamiento del mundo fue Kant , allá por el siglo XIX; y podríamos compartir tal visión, de no ser porque Heidegger apareció en el XX para estremecer a la filosofía en sí. A partir de su libro “Ser y tiempo”, la tarea por el discernimiento y la reflexión ha tomado su obra como eje principal, ya sea para seguirle o refutarle. ¿A qué se debe esto? De manera resumida, él pensaba que toda idea es un error a menos que se base en una correcta noción del hombre y su existencia . Sólo eso; algo tan simple pero complicado a la vez, pues él pensaba que –para solucionar esto– debemos regresar a los presocráticos y reexaminar nuestro ser. 
  
  Su proyecto fue tan confuso y específico que incluso logró serlo aún más al darse cuenta de que requería de un lenguaje propio y de hecho, lo creó. Al respecto, y arriesgándonos a omitir demasiadas cosas importantes, podemos resumir que (según este autor alemán) el hombre existe, pero lleva a cuestas la muerte o la inexistencia, el hombre es un ser en el mundo en cuanto está consciente de su existencia y se hace un ser para la muerte tan pronto es consciente de su potencial inexistencia. En esos términos, es entonces que el hombre ve mostrarse al ser, creándole angustia y miedo al advertir que el mundo no es su hogar y pronto tendrá que abandonarlo. Así, la sociedad y la tecnología pueden distraer al hombre de vivir esa muestra y prevenirle de ser para la muerte; además de que el ser debe tomar la forma del tiempo, ya que la existencia o la inexistencia no significan nada sin el tiempo con su horizonte. 
   Como hemos dicho, a grandes rasgos y sólo como una muestra de la complejidad que el pensamiento heideggeriano comprende. Sistema que se ha retomado y criticado por muy diversas vías; por ejemplo, el cine. Un arte que puede crear distintos canales de discusión retomando una multiplicidad de opiniones o lecturas en torno a algo. En este caso, el “Ser y tiempo” del filósofo más leído, comentado y enjuiciado del siglo pasado, es un libro que intencional o involuntariamente ha dado mucho material fílmico; a continuación, se muestran 10 películas que aportan un seguimiento a su contenido y podrían ayudarnos en su lectura.  –  “Melancholia” (2011) – Lars von Trier    Una película que retrata a la perfección distintos modos de conciencia ante la existencia, la muerte y la estancia sobre el mundo; no sabemos si von Trier lo pensó exactamente en clave heideggeriana, pero justo ese momento de calma ante las certezas de la inexistencia hacen pensar que “Melancholia” está más ligada a la filosofía alemana de lo que pensamos. 
  
  – “Ikiru” (1952) – Akira Kurosawa 
   En una búsqueda interna por la felicidad, el protagonista de este filme atraviesa el hecho de sufrir un cáncer terminal y sus funciones burocráticas para hallar que su enfrentamiento con la inexistencia le permite ver las cosas de un modo distinto. 
  
  – “Primer” (2004) – Shane Carruth 
   Esta película llena de mensajes, situaciones y símbolos sobre la existencia muestra a un protagonista que se desdobla en el tiempo y cualquiera de sus manipulaciones brinda alteraciones que advierten sobre su propia existencia y la de los demás. 
  
  – “The Diving Bell and the Butterfly” (2007) – Julian Schnabel 
   Ese sentimiento extraño de estar atrapado en tu propio cuerpo es algo que queda de manifiesto en esta cinta; las crisis existenciales toman un carácter principal en conjunción con el peligro de muerte y éstas adquieren representaciones alucinantes a lo largo del filme. Hacia el final de éste, los problemas en torno a la voluntad y la permanencia en el mundo acaparan el discurso total. 
  
  – “Metrópolis” (1927) – Fritz Lang 
   A lo largo de su trabajo, Heidegger advirtió sobre los peligros de la tecnología, señalando que sin una amplia conciencia de nuestra existencia, no hay forma de entender cómo es que existimos en relación con la otra. La preocupación heideggeriana recae en el hecho de que los avances tecnológicos nos pueden hacer olvidar la muerte y la propia existencia. Algo así como sucede en este filme. 
  
  – “The Seventh Seal” (1957) – Ingmar Bergman 
   Bergman juega magistralmente con una de las enseñanzas básicas de Heidegger; teniendo enfrente al ser para la muerte, nadie puede vivir, reconociendo esto todo lo demás es en realidad nada. 
  
   – “Synecdoche, New York” (2008) – Charlie Kaufman 
   Seymour Hoffman, protagonista de la cinta, es el típico papel psicológico que lo tiene todo para aportar ansias al espectador; él sabe que el fin se acerca, que tiene que esforzarse por completo para terminar sus planes en esta vida y que las líneas divisorias entre realidad y ficción son borrables. 
  
  – “The Fountain” (2006) – Darren Aranofsky 
   En líneas permeables del tiempo, esta historia aborda temas como el amor, el cuidado, la permanencia, la conciencia, la mortalidad y la perdurabilidad. Aranofsky trata estos tópicos con sutileza metafísica y al final, arroja una sentencia que poco esperábamos en su narración ficticia. Vale completamente la pena verla con calma y atención. 
  
  – “The Thin Red Line” (1998) – Terrence Malick 
   En definitiva, ésta no es la típica película de guerra. Malick estudió cuidadosamente a Heidegger y eso es notorio en su filme; a partir de problemáticas propias de un contexto bélico, el director presenta preguntas fundamentales para continuar con la filosofía heideggeriana: ¿Cuál es nuestra relación con la naturaleza? ¿Con los otros y uno mismo? ¿En qué consiste nuestra existencia y cómo debemos afrontarla? 
  
  – “The Sacrifice” (1986) – Andrei Tarkovsky 
   Un filme plagado de filosofía y poesía. Los intereses del director por el pensamiento de Nietzsche, Kierkegaard, Hemingway y Heidegger, hacen de esta producción un torrente de ideas en extremo intenso alrededor de temas como el origen, la divinidad, el mundo, el paso ante la vida y la ciencia. 
    – Heidegger fue un filósofo de compleja e incomprendida producción filosófica; para saber un poco más sobre ello, puedes leer acerca de su vida en una alejada cabaña o de qué lecturas se posibilitan entre él y el Nihilismo . 
 *** Te puede interesar: 
 El reflejo verdadero: Psicoanálisis de personajes en el cine 
 10 películas basadas en la filosofía de Michel Foucault ** Referencia: 
 Taste of Cinema