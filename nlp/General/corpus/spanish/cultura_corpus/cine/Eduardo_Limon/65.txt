Cosas que no sabías de “That 70’s Show”

 “That 70’s Show” es una serie de finales de los 90 y mediados de los 2000 que muchos recordamos, y aunque nunca se pudo situar en los grandes lugares del rating televisivo frente a monstruos como “Friends” o “Frasier”, este sitcom cautivaba con elementos que lo hacían únicos al tratar temas de una década tan añorada. 
  Con una tendencia siempre innovadora, “That 70’s Show” contaba con detalles característicos de su estilo; por ejemplo, los diálogos entre los personajes eran a pantalla dividida o un círculo que viraba al hablante, las transiciones entre escenas conjugaban un baile groovey en un fondo psicodélico , etcétera. De personajes y secuencias que se guardan en nuestra memoria, esta serie cómica tiene bastantes datos que no se conocen abiertamente. A continuación te presentamos algunos de ellos. 
 El papá de Eric, Red, siempre en la mente como esa autoridad clásica que se contraponía a los estándares de la Juventud revolucionaria, estuvo a punto de ser interpretado por Chuck Norris. 
  Antes de esta serie, Ashton Kutcher jamás había actuado. Se dedicaba al modelaje. 
  Mila Kunis tuvo sus orígenes en este legendario show. Interpretó a Jackie, una chica banal y malcriada, durante toda la serie. Cuando fue a audicionar mintió sobre su edad, dijo que estaba por cumplir 18 cuando en realidad tenía 14. 
  Kunis tuvo su primer beso durante una filmación con Kutcher; y Valderrama (Fez) le enseñó a manejar. 
  El programa se iba a llamar “Teenage Wasteland”pero por cuestiones de derechos e ideología, Pete Townshend, guitarrista y compositor de The Who, se negó a prestar sus creaciones. 
  
 El movimiento circular de la cámara del que hablábamos más arriba, y que permitía declaraciones del personaje en cuestión, era un recurso que facilitaba la manera políticamente correcta de mostrar a un grupo de amigos fumando marihuana sin mostrarlo de verdad. 
  
 El drogadicto jefe de Hyde, Leo, de verdad fue arrestado por posesión y venta de drogas. 
  
 Desde la temporada cinco hasta la final, los episodios se nombraban o relacionaban en honor de alguna banda. Led Zepellin, The Who, The Rolling Stones y Queen fueron los elegidos. 
  
 Cuando la hermosa Donna de cabello rojo casi naranja se vuelve rubia, nos rompió el corazón a muchos. La razón para este cambio fue su papel en el filme “Karla”. 
  
 Topher Grace (Eric) fue descubierto por los co-creadores de “That 70’s Show”, Bonnie y Terry Turner, en una obra escolar de la preparatoria donde participaba el hijo de ellos. 
  
 Kurtwood Smith se inspiró y tomó aspectos reales de su padrastro para interpretar a Red Foreman. 
  
 Se intentó lanzar una versión británica del concepto bajo el nombre de “Days like these” pero sólo duró diez episodios. 
  
 Al inicio de cada episodio, una de las mejores intros jamás hecha, se muestra al elenco cantando su propia versión de “In the Street”, canción de Big Star. 
  
 En esa misma intro se puede ubicar históricamente en qué momento se sitúa la narración. En la placa de la camioneta donde van todos se alcanza a leer siempre en qué número de la década están viviendo. 
  
 *** 
 Te puede interesar: 
 Datos que no sabías de “Friends”, la serie que marcó generaciones enteras ¿Joey o Barney, Robin o Rachel? ¿”Friends” o “How I Met Your Mother”?