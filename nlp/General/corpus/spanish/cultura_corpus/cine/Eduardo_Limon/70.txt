10 películas que te gustarán si amas Amélie

 “ Amélie ” es una dulce mezcla de colores pastel, de postales parisinas, de extravagante misterio, de realismo mágico y seductora fotografía. Tuvo, y tiene, todo lo necesario para que su llegada a las pantallas se presentara como un éxito. Es un filme que ha servido como referente desde su primer momento de lo que es el cine francés contemporáneo y de las emociones más juveniles en el mundo. Una producción cinematográfica posicionada como favorita de las nuevas generaciones, musicalizada de manera magistral por Yann Tiersen, es la profunda y simple historia de una chica francesa frente a la realidad. Una realidad en que logra transformar cualquier mínimo aspecto en un bocado de fantasía para desentrañar las razones o las respuestas más conmovedoras que se pueden vivir. 
  Los impetus y valores que mueven a la protagonista de esta joya son raros de encontrar en otras películas, sobre todo si se persigue esto en los trabajos posteriores de Jeunet y Tautou , y sonreír tanto como lo hacemos viendo a la joven que ha decidido arreglar vidas ajenas. Rastreando diversos filmes que pudieran generar algo similar, algún tipo de calma cuando los problemas nos aquejan, es que se pueden mencionar los siguientes títulos esperando que las fibras internas de la emoción se cimbren. Tal vez es demasiado compararles con el largometraje más famoso de Jeunet (en ambas direcciones), pero sin duda todas las alternativas expuestas son capaces de guardar y compartir magia. “My sassy girl” (2001), Kwak Jae-Yong 
  Fenómeno fílmico en Asia que cuenta una historia de amor honesta y universal que presenta personajes reales, lo más humano posibles, con los que el público de cualquier parte del mundo se puede encariñar e identificar. Se puede encontrar bajo el nombre de “Mi chica descarada”. 
  “Millenium actress” (2001), Satoshi Kon 
  Ahora, una pieza clásica de la animación japonesa. Un completo carrusel de emociones que nos recuerda la gran humanidad que es posible hallar en el interior de nuestras personas y que, aún no teniendo el concepto más original por ofrecer, la manera en que dirige la narración le hace única en su clase. 
  “In the mood for love” (2000), Wong Kar-Wai 
  Historia situada en los años 60 de Asia, cuenta la historia de un hombre y una mujer que se conocen durante las largas ausencias de sus respectivas parejas, generan una fuerte amistad y posteriormente descubren secretos de estos. Se necesitan pañuelos desechables para poder aguantar ciertas partes de la película. 
  “La doble vida de Verónica” (1991), Krzysztof Kieslowski 
  Una historia que se desdobla en dos perspectivas sobre la vida de una mujer y se traduce en texturas fantásticas, nos comprueba esa sensación, o necesidad, de no ser ajenos al mundo, a las personas. 
  
 “Luces de la ciudad” (1931), Charles Chaplin 
  Propuesta del cine que se filmó en una época en que ya era posible grabar con sonido pero su director, y protagonista, se rehusó a caer en las trampas de la tecnología. Chaplin interpreta a un vagabundo que, a su vez, encarna el magnífico sentimiento de preocuparse por otro y mover cielo, mal y tierra, para traerle la felicidad. 
  “Frances Ha” (2013), Noah Baumbach 
  Retrato de lo que significa ir tras tus sueños cueste lo que cueste; es una odisea sobre los sueños y la vida, un viaje para el hallar la superación, la amistad y el optimismo por encima de todo. Verla puede ser ese trago revitalizante que a veces es necesario. 
  “Medianoche en París” (2011), Woody Allen 
  A pesar de que muchos fans se han quejado de esta entrega, “Medianoche en París”, completando una trilogía europea del director, muestra la dificultad de aceptar una realidad que no es suficiente para las inquietudes del alma y del corazón. Seguramente más de una vez hemos sentido esa urgencia por ir a un lugar que efectivamente sentimos nuestro. 
  “Submarine” (2010), Richard Ayoade 
  Debut fílmico del director, narra una brillante mezcla de humor y malicia en su adaptación del libro de Dunthorne. Juventud y dolor se ven codificados en estos cuadros de imagen para que podamos vernos reflejados de la manera más personal posible en cada uno. 
  “The brothers Bloom” (2008), Rian Johnson 
  De detalles surrealistas, este filme puede perdernos en su entramado de verdades y mentiras. La calidad y humor con que es tratado el tema principal: el cubrimiento y descubrimiento de lo verdadero, es magnífica. Uno de esos filmes que te mantienen atento a cada segundo que corre. 
  “Big Fish” (2003), Tim Burton 
  Una producción que será siempre recordada por el nivel de argumentación y de estética que presenta. Prácticamente es una pintura hecha realidad que nos muestra una serie de eventos mágicos en la vida de un hombre, que bien podemos ser todos, y da detalle de la importancia por vivir con cada poro abierto de nuestra piel. 
  
 
 *** 
 Te puede interesar: 
 Una charla con los mejores directores de cine contemporáneo Películas inspiradas en obras de arte