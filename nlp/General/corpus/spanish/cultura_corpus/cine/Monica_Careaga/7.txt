“Box”, un mapping de antecedentes rotoreliefsistas

 Un diseñador y un equipo de ingenieros unieron esfuerzos para realizar un clip que oscila entre un mapping y una coreografía de monitores y cuerpos para generar una experiencia puramente dadaísta. 
  Como en las imágenes de Anémic Cinémica , de Marcel Duchamp y Man Ray, “Box” explora la homologación del espacio real con las bondades digitales a través del mapeo sobre las superficies. 
  La declaración artística de Bot y Dolly es una muestra de su habilidad con una sola cámara, un argumento técnico que, junto a diversas tecnologías como robótica, cartografía de proyección y software especializado, respaldan las configuraciones de su propia imaginación. 
   Son ilusiones ópticas como poemas visuales de antecedentes rotoreliefsistas. Una nueva experiencia en la comunicación digital.