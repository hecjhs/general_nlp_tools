10 películas que te enseñarán la cara más triste del amor

 
 De la misma forma que el amor tiene un potencial creativo inimaginable, la cara opuesta, el desamor , es una de las experiencias más amargas de las que se tenga memoria. Esos momentos en que la complicidad se rompe, cuando despiertas y te das cuenta que su relación nunca volverá a ser la misma, son tan dolorosos como catastróficos que se deben aprender a digerir poco a poco para comenzar de nuevo. 
 ¿Cuáles son esas emociones por las que todos pasamos cuando se acaba nuestra felicidad? Cada sentir se plasma de una forma distinta de persona a persona y puede ir desde las lágrimas, una carta desgarradora, una obra de arte o cualquier otro objeto que deja de ser una simple cosa y pasa a ser una representación pura del dolor de la ruptura, un monumento genuino a lo que terminó. Es el caso del Museo de las Relaciones Rotas , una exposición itinerante que describe la tristeza a través de objetos que pueden parecer comunes, pero cada uno tiene una historia propia que puede ser la más significativa y desgarradora para esa persona. 
 Las historias llevadas a la pantalla grande son una fuente de inspiración y te harán sentir reconfortado al ver que todos, sin excepción, alguna vez hemos pasado por una terrible decepción amorosa. Conocemos los más grandes relatos de desamor en la literatura, en la pintura y son comunes en la poesía, pero ¿cuál es la cara más triste del amor en la cinematografía? Estas son diez respuestas contundentes: 
 – “The Bridge of Madison County” (1995) – Clint Eastwood 
  
 Drama romántico sobre la vida de Francesca, una mujer madura que hace a un lado sus sueños por establecerse en un pequeño condado y hacerse cargo de su familia. Esta vida gris tiene un vuelco inesperado cuando un fotógrafo de National Geographic llega al pueblo que es famoso por sus puentes. Francesa se enfrenta a la disyuntiva de dejarlo todo por amor o bien mantener el  status quo  por algo que no le apasiona. 
 – “The English Patient” (1996) – Anthony Minghella 
  
 Un hombre con graves quemaduras viaja a través de Italia al término de la Segunda Guerra Mundial, pero le es imposible continuar el viaje debido a su estado de salud. El hombre es recibido en un monasterio abandonado por Hanah, una enfermera que cuida de él. Reacio a contar su historia y lo que ocurrió en el accidente, el hombre se mantiene callado mientras pequeñas visiones muestran el drama alrededor del amor que sufrió en su vida. 
 – “Casablanca” (1942) – Michael Curtiz 
  
 Una de las cintas más vanagloriadas del cine estadounidense que cuenta la historia de amor suscitada entre Rick e Ilsa. El primero tiene en sus manos el poder de cambiar la suerte de Ilsa y su esposo, pero esa decisión significaría perderla para siempre, una disyuntiva entre el deber moral y la renuncia a todo por amor. 
 – “Edward Scissorhands” (1990) – Tim Burton 
  
 El clásico de Tim Burton cuenta la historia de Edward, un hombre creado artificialmente cuya característica es poseer tijeras en vez de manos. A través de un relato dramático, con tintes oscuros y por momentos cómicos, Depp encarna un personaje entrañable, que descubre el verdadero amor en su versión más triste. 
 – “In The Mood for Love” (2000) – Wong Kar-Wai 
  
 Chow, jefe de redacción de un periódico y Li-Zhen, una secretaria, se mudan simultáneamente al mismo edificio con sus respectivos esposos. Poco a poco se van conociendo hasta el momento en que descubren que están profundamente enamorados el uno del otro. A partir de entonces, deberán armarse de valor para perseguir el amor real o resignarse y olvidar el frenesí que intenta salir de su pecho. 
 – “Doctor Zhivago” (1965) – David Lean 
  
 Un drama histórico ubicado temporalmente en los años turbulentos después de la Revolución rusa, cuenta la historia de Zhivago, un cirujano felizmente casado que en medio de los recuerdos de la guerra, pronto descubre que la pasión no encuentra límite ni el sitio que le corresponde dentro de su matrimonio, poniendo a prueba sus principios y la vida de todos a su alrededor. 
 – “ Eternal Sunshine of the Spotless Mind” (2004) – Michel Gondry 
  
 Clementine y Joel terminan su relación y ella decide someterse a un tratamiento experimental que borrará sus recuerdos. Cuando Joel se entera, decide hacer lo mismo y mientras su memoria desaparece, se da cuenta de que aquellos son los momentos más valiosos de su vida. 
 – “Annie Hall” (1977) – Woody Allen 
  
 Alvy Singer es un comediante de medio pelo que vive en Nueva York, donde un día conoce a Annie Hall y se enamoran perdidamente. Después de algunos años en su relación, Hall y Singer descubren que realmente no son el uno para el otro y poco a poco comprenden las enormes diferencias entre dos mundos distintos. La cinta aborda la dura realidad de aceptar que a veces el amor no es suficiente en las relaciones. 
 – “The Great Gatsby” (1974) – Jack Clayton 
  
 Basada en la obra maestra de Fitzgerald, cuenta la historia de Nick, un joven estudiante de Yale que llega a vivir a Nueva York. Ahí descubre que su vecino, el misterioso y millonario señor Gatsby, a pesar de estar rodeado de opulencia, lleva una carga que lo llena tristeza. Se trata de un amor del pasado, del cual Nick intenta averiguar más mientras aprende a relacionarse con el círculo social del gran Gatsby. 
 – “The Barber of Siberia” (1998) – Nikita Mikhalkov 
  
 Jane es una joven estadounidense que decide viajar a Rusia para reunirse con su padre. Durante su estancia, conoce a Andrei Tolstoi, un soldado con quien poco a poco va desarrollando una relación mucho más profunda que una amistad, incluso comparten los días y ambos caen perdidamente enamorados el uno del otro. El joven es enviado a servir al ejército en Siberia y Jane hará hasta lo imposible por reencontrarse años después con su gran amor. 
 Si crees que el amor no existe, debes ver las películas que nos enseñan que el amor es simplemente una fantasía . Si te interesa el erotismo y estás en busca de nuevos títulos, toma nota de las 8 películas eróticas extranjeras que puedes ver este fin de semana .