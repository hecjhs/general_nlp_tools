10 películas de Netflix para ver en pareja y aprender a darlo todo en el amor

 
 “Dar todo en el amor” es una frase que puede caer en el simplismo de la entrega incondicional de un amor quijotesco u obsesivo. No significa perder la razón a través de un laberinto de desenfreno cuyo derrotero es, en la mayoría de las veces, un final trágico. Un amor valiente y entregado no es aquel que pierde su esencia y enajena a la pareja, creando una necesidad enferma de estar juntos, sino el que se entrega realmente sabiendo que la otra persona puede tener su corazón en la mano , sobre su puño y puede hacer lo que quiera con él: estrujarlo, ignorarlo, tirarlo a la basura, da igual. Pero cuando esa persona lo conserva, lo valora y guarda con cariño, entonces el amor se hace incondicional. 
 La capacidad de amar está íntimamente ligada a la concepción de que cada uno tiene en la mente lo que significa y la forma en que se demuestra. Las cartas, los detalles, los regalos y la coquetería no sirven de nada si no existe complicidad en pareja, esa que se forja en la solidaridad, el cariño desinteresado y la preocupación por el otro. Estas son diez películas disponibles en Netflix que enseñan formas distintas de amar, ideales para ver con tu pareja y comprender más sobre la complejidad y belleza de las relaciones humanas: 
 – “Love & Other Drugs” (2010) – Edward Zwick 
  
 Jamie Randall es un empleado de la compañía farmacéutica que comercializa el Viagra. Su gran éxito en todas las facetas de su vida (especialmente con las mujeres), se debe a su encanto físico y simpatía. Cuando conoce a Maggie Murdock se da cuenta de que ambos están influenciados por una droga más potente que las que vende: el amor. Finalmente, ambos deberán mostrar la entereza que se necesita frente a las adversidades para estar juntos. 
 – “La vita è bella” (1997) – Roberto Benigni 
  
 Un hombre perdidamente enamorado hace hasta lo imposible por agradar a la mujer de sus sueños en la Italia antes de la Segunda Guerra Mundial. Cuando las cosas empiezan a marchar a la perfección, la vida de la familia se ve truncada por la ocupación del Tercer Reich. Entonces Guido deberá armarse con la fortaleza necesaria para demostrar a sus eres más queridos que pese a todos los males, la vida es bella. 
 – “Amélie” (2001) – Jean-Pierre Jeunet 
  
 La historia de una chica con un corazón puro y sincero que toma como razón de ser la búsqueda de la felicidad de todos quienes la rodean. Cuando menos se lo espera, el amor toca a la puerta de Amélie, que no sabe si está preparada para ocuparse en su propia felicidad. 
 – “Elsa y Fred” (2005) – Marcos Carnevale 
  
 Dos adultos mayores con historias completamtente distintas se cruzan en el crepúsculo de sus vidas por casualidad, demostrando que el amor sincero desconoce edad, condición social o creencias religiosas. Una oda a la vida, un segundo aire que demuestra que el amor verdadero es capaz de vencer cualquier obstáculo. 
 – “Love, Rosie” (2014) – Christian Ditter 
  
 Dos jóvenes irlandeses comparten un vínculo muy fuerte que los une y trasciende de las parejas normales: son amigos desde que tienen memoria. Rosie y Alex viven toda clase de aventuras juntos, pasando por todas las etapas de la vida hasta que se convierten en adultos. Entonces una decisión trascendente amenaza con terminar su amistad, por lo que deberán decidir qué significan realmente el uno para el otro. 
 – “8 apellidos vascos” (2014) – Emilio Martínez Lázaro 
  
 Comedia romántica española con un giro que le da originalidad y diversión. Un sevillano de cepa se enamora súbitamente de una chica donostiarra. Guiado por su corazón gitano, decide emprender un viaje hasta el país vasco en su búsqueda; sin embargo, lo que encuentra es totalmente distinto a lo que tenía en mente. Un gran engaño basado en sus barreras culturales habrá de definir la vida de ambos en el momento más álgido. 
 – “Como agua para chocolate” (1992) – Alfonso Arau 
  
 La obra que lanzó a Laura Esquivel a la fama es llevada a la pantalla grande por Alfonso Arau. A finales de la revolución, Tita es una joven que vive enamorada de Pedro, el novio de su infancia; sin embargo, las tradiciones familiares impiden que ambos estén juntos. La madre de Tita es fiel a los arraigados conservadurismos de su pueblo y los jóvenes enamorados tendrán que librar distintas batallas contra el tiempo, la fidelidad y las tradiciones para saber si en verdad son el uno para el otro. 
 – “6 Years” (2015) – Hannah Fidell 
  
 La relación de pareja perfecta atraviesa por una serie de dificultades que pondrán a prueba el amor y el respeto mutuo. Los celos, enojos y arranques viscerales amenazan con acabar lo que alguna vez fue la sensación más bella. Los sueños se tornan pesadillas y su vida da un vuelco inesperado que los llevará por un camino en el que deben decidir si vale la pena seguir con la relación o terminar en el fracaso. 
 – “Cuestión de tiempo” (2013) – Richard Curtis 
  
 Una novela romántica que tiene un dejo de frescura en la capacidad de Tim Lake, quien descubre que puede volver al pasado cuantas veces quiera para tratar de enamorar a la chica de sus sueños; sin embargo, viajar en el tiempo también involucra graves riesgos que tendrá que correr una y otra vez para lograr conquistar a una tímida e insegura chica. 
 – “You Will Meet a Tall Dark Stranger” (2010) – Woody Allen 
  
 Una de las tramas más enredadas de Allen que muestra las distintas facetas de todo lo que se esconde detrás de la palabra “amor”. Interés, sexo, ambición, ganas de sentirse amado, soledad, fortuna: todos esos temas se conjugan en una ágil e inteligente película que muestra la fragilidad de la condición humana ante el sentimiento más grande que existe. Los personajes deberán decidir entre el amor desinteresado o bien, seguir viviendo en el engaño. 
 – El amor no es algo ideal. Amar no es la meta al final del tortuoso camino que hace creer la Iglesia y la tradición occidental. Amar no es sufrir , ni se compara con la redención. Ningún gran amor nace de la espontaneidad, las historias de ese género se reservan para los cuentos de caballeros y princesas. El amor se demuestra en la vida diaria y se construye en la terrenalidad de lo humano, de lo pasajero. El amor no es impoluto, es real y tiene imperfecciones todo el tiempo, que lejos de ocultarse, le dan un carácter terriblemente humano. 
 *** Te puede interesar: 
 Películas que demuestran que el amor se puede convertir en tu peor obsesión 
 Las mejores 20 películas que puedes ver en una cita