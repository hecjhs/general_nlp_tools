Las 10 películas favoritas de Slavoj Zizek

 
 Slavoj Žižek  es la postura refrescante de la filosofía en pleno siglo XXI. No es el filósofo academicista que se jacta de entender a Kant ni pretende serlo. Tampoco el existencialista atormentado por la incapacidad de cambiar al mundo con tendencias suicidas. Ni siquiera el idealista que construye a partir de la repetición de términos ininteligibles teorías que nadie entiende, excepto Heidegger y él mismo. El esloveno es un filósofo práctico que muestra el poderío de su pensamiento en la terrenalidad a través de una lectura  crítica del psicoanálisis lacaniano y el poderoso instrumental teórico marxista, utilizando los elementos más presentes en la sociedad que no son ajenos a nadie dentro de la cultura pop: las películas de Hollywood . 
 Žižek no sólo es un lector empedernido, también es un cinéfilo obsesionado con buscar en las películas aspectos que retraten características de la sociedad moderna. En el largometraje  “The Pervert’s Guide to Ideology” , escrito por él y dirigido por Sophie Fiennes, el esloveno habla sobre temáticas tan variadas como el fascismo en “Jaws”, el significado de “Taxi Driver” y su asombroso parecido con una película “The Searchers” y hasta la crítica de la última entrega de “Batman”, de Christopher Nolan. En una visita realizada al asombroso acervo de Criterion Collection , el filósofo escogió diez de sus películas favoritas y ante la cámara explicó brevemente la razón de sus elecciones: 
  
 – “Trouble in Paradise” (1932) – Ernst Lubitsch 
  
 Una ácida comedia sobre el romance entre un ladrón, Gaston Monescu y una hábil carterista, Lily, quienes se conocen en Venecia durante una gala, cada uno aparentando ser algo que no son (barón y condesa, respectivamente). Ambos deciden aliarse y robar juntos, pero sin perder el estilo. El filósofo esloveno manifiesta su gusto por ella denunciándola como “la mejor crítica al capitalismo”. 
 – “Sweet Smell of Success (1957)” – Alexander Mackendrick 
  
 Uno de los hombres más influyentes en la prensa neoyorquina utiliza sus influencias para construir la fama de sus socios y derrumbar a sus rivales a través de su rotativo. Cuando su hermana menor se enamora de un músico de jazz, pone en marcha su maquinaria para acabar con la carrera del jazzista. “Una gran representación de la corrupción en la prensa estadounidense”, afirma Žižek. 
 – “Picnic at Hanging Rock” (1975) – Peter Weir 
  
 Un grupo de jóvenes de un colegio + hace un día de campo en Hanging Rock, una formación rocosa lejana de la civilización. Una serie de sucesos extraños termina por convertir el paseo en una pesadilla: una maestra y tres alumnas desaparecen y otros más pierden el conocimiento. Žižek reconoce que le gusta el trabajo temprano de Peter Weir desde que era joven. – “Murmur of The Heart” (1971) – Louis Malle 
  
 Un drama sobre la infancia libertina de un adolescente y una enfermedad repentina que crea lazos extraños con su madre y un manifiesto complejo de Edipo. El esloveno la cataloga como “una de esas gentiles películas francesas donde hay un incesto, retratado como un secreto entre una madre y su hijo. Sin duda me gusta”. 
 – “The Joke” (1969) – Jaromil Jire š 
  
 Un expulsado del partido comunista por una broma encuentra al culpable de su expulsión y destierro. 15 años después, vuelve para buscar venganza. El filósofo fue crítico con la cinta basada en el libro homónimo: “La primera novela de Milan Kundera y en mi opinión, la única buena. Después de ella, todo se vino abajo”. 
 – “The Ice Storm” (1997) – Ang Lee 
  
 “Tengo un cariño especial por este filme. Cuando James Schaumus estaba escribiendo el guion, me dijo que leía uno de mis libros y … fue inspiración. Es una razón personal, pero también amo la película”. 
 – “Great Expectations” (1946) – David Lean 
  
 Un joven pobre se convierte de la noche a la mañana en un caballero gracias a una misteriosa donación. Basada en la obra homónima de Charles Dickens, se considera una de las mejores películas inglesas de la historia: “Soy un enorme fan de Dickens”. 
 – “City Lights” (1931) – Charlie Chaplin 
  
 Una de las cumbres de la cinematografía de la mano de Chaplin que encarna a un vagabundo enamorado de una chica ciega. El comentario del esloveno es contundente: “¿Qué más puedo decir? Una de las mejores de todos los tiempos”. 
 – “Y tu mamá también” (2002) – Alfonso Cuarón 
  
 La cinta que lanzó a la fama a Alfonso Cuarón es mucho más que una entretenida road movie. Temas como la amistad, el sexo, las desigualdades sociales y el escenario político en México son tocados en uno de los filmes favoritos de Žižek:  “Esta (me gusta) por obvias razones, yo hice los comentarios (para la película en DVD). De todas formas, debo decir que mi película favorita de Cuarón es ‘Children of Men'”. 
 – “Antichrist” (2009)” – Lars von Trier 
  
 Con la crudeza que caracteriza a von Trier, este filme aborda a una pareja de padres que quedan afectados psicológicamente por la pérdida de su hijo. El padre psicólogo trata de ayudar a su esposa a superar el duelo y deciden internarse en el bosque, sitio donde pasaron sus últimas vacaciones juntos. 
 – Žižek  construye  una filosofía práctica y para ello critica desde su núcleo a la sociedad capitalista, basándose en sus mayores referentes culturales que desnudan la decadencia de la posmodernidad y el mercado. El filósofo  centra su obra en temas políticos trascendentales y los extrapola al presente de un modo ameno, entendible para la mayoría del público y sin las pretensiones ni la pedantería que se maneja en los círculos académicos más altos. 
 *** Te puede interesar: 
 10 películas que te dejarán alterado si te gustó Se7en: los siete pecados capitales 
 Las películas con los finales más confusos ahora tienen explicación * Fuente: 
 Open Culture