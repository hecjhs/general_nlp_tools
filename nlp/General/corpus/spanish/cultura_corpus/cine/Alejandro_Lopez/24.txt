La receta secreta para comer caca como en las películas

 
 ¿Recuerdas la icónica escena de Los 120 días de Sodoma cuando sentados a la mesa ingieren excrementos? ¿Sabías que un actor ingirió caca real (y no fue humano) en una película ? 
 Los coprófagos son aquellas personas que se deleitan al ingerir heces fecales, lo cual en su versión más sencilla resulta bastante fácil y aburrido. El platillo servido sin más, no necesita de una preparación y únicamente requiere del esfuerzo desinteresado de un voluntario haciendo sus necesidades fisiológicas directo al plato o bien, de uno mismo. Sin embargo, estas películas demuestran que como cualquier otro plato culinario, comer caca también es parte de la alta cocina que requiere de una preparación minuciosa. Descúbrelo a través de las mejores recetas del cine coprófago: 
 – “Pink Flamingos” (1972) – John Waters 
  
 El largometraje de Waters fue uno de los primeros en incluir una desagradable escena de coprófagos. Lo peor de todo: es real. El plano transcurre desde que el perro detiene su marcha, se pone en posición para evacuar y deja su excremento en el piso. Acto seguido, Divine, según un diario la persona más inmunda del mundo, se acerca y recoge la caca, tragándola tras masticarla un par de veces. 
 – “Sweet Movie” (1974) – Dusan Makavejev 
  
 La polémica cinta de Makavejev rompe con la estructura tradicional de la narrativa fílmica y cuenta dos historias distintas. Por un lado, relata la decadente historia de una modelo que gana un concurso de belleza y como premio contrae matrimonio con un magnate, mientras que la segunda cuenta la travesía de una mujer que capitanea un barco comunista repleto de golosinas. La cinta fue prohibida en muchos países por las fuertes escenas de coprofagia infantil. El recurso usado por Makavejev fue la mezcla de hojuelas de maíz con chocolate, dando la impresión de una sustancia desagradable. 
 – “Saló o Los 120 días de Sodoma” (1975) – Pier Paolo Pasolini 
  
 Basada en el libro homónimo del Marqués de Sade, esta cinta es completamente gráfica y cruda. A pesar de que no abunda la sangre, muestra sin reservas las violaciones sexuales, golpizas, torturas y castigos a los que son sometidos los jóvenes secuestrados. Una crítica dura de Pasolini a la burguesía decadente de la posguerra. La escena donde comen excremento fue lograda con ayuda de chocolate con mermelada. La caca no logra un efecto tan realista como en otras películas, pero es igualmente bien lograda. 
 – “Trainspotting” (1996) – Danny Boyle 
  
 La historia de un grupo de amigos adictos a la heroína, rechazados por la sociedad y sin interés de ser parte activa de ella. Todos parecen estar condenados a la decadencia, excepto Mark Renton, quien intenta salir del casi trazado camino hacia la perdición en el mundo de las drogas. Al inicio de la cinta, Renton se sumerge por la adicción en el que llama “el peor baño de Escocia”. En realidad, los desechos humanos fueron adaptados con una mezcla de distintos chocolates donde el protagonista bucea con asco. 
 – “The Help” (2011) – Tate Taylor 
  
 Esta profunda historia que abunda en los sentimientos humanos llega a su clímax cuando Minny Jackson, la cocinera de la señora Hilly Holbrook se venga de ella y su rampante racismo y clasismo entregándole un supuesto pastel que cocinó para ella. La señora Holbrook lo prueba en dos ocasiones, exclamando lo delicioso que sabe, mientras Jackson espera el momento de culminar su venganza. Entonces se produce la famosa escena donde la cocinera se niega a invitar a la madre de Holbrook y todo explota con el icónico “Eat my shit”. 
 – Actualmente, los psicólogos y psiquiatras debaten acerca de si esta práctica debe formar parte del “Manual de Diagnóstico y Estadística de los Trastornos Mentales” publicado por la APA (Asociación Americana de Psiquiatría), la biblia de los trastornos que registra todos los “hallazgos médicos” de conductas humanas deplorables. ¿Cuál es la regla y cual el parámetro de la desviación que una persona debe sufrir para dejar de ser parte de la normalidad y pasar a la lista de trastornados ? Tal vez el análisis crítico de la psiquiatría pueda dar luz sobre esa cuestión en nombres tan resonados como Michel Foucault . ¿Consideras a la coprofagia como un trastorno mental o simplemente te parece un gusto exótico? 
 *** Te puede interesar: 
 28 películas que puedes ver en Netflix en menos de 120 minutos 
 El curso en línea gratuito que te convertirá en todo un crítico de cine 
 * Fuente: 
 HojaSanta