10 películas para cuestionar el significado de la existencia humana

 
 ¿Cuál es el fin de la vida humana? ¿Acaso los hombres venimos al mundo a perecer, rodeados de una gris niebla que distorsiona la conciencia y nubla los sentidos? ¿O estamos hechos para cumplir una misión especial? Los pensadores de todas las épocas han tratado de responder una de las más grandes interrogantes de la existencia humana : ¿Cuál es la razón de existir? A partir de esta incógnita, pueblos enteros han dedicado su vida a seres supremos, con el objetivo de trascender a otra mejor vida, sin ninguna garantía de qué es lo que esté del otro lado de la vida; religiones han forjado su historia, prometiendo la trascendencia del ser humano a un reino ideal, donde todos los sufrimientos de la vida quedan superados y sólo está la plenitud, el gozo eterno y la redención. 
 La conciencia humana se ha esforzado por otorgarle una razón a cada una de las actividades que el hombre desarrolla en sociedad, en la realidad o en su mente. Pero ¿qué pasaría si descubrimos que ni el mundo ni la existencia de la humanidad tienen razón alguna, que no existe algún plan maestro, ser supremo, ni siquiera nuestro planeta es único entre miles de millones de formas de vida en el Universo? 
 Estas diez películas buscan ahondar en la búsqueda del significado de la vida humana, preguntándose sobre la existencia y su irremediable encuentro con la muerte, creando un diálogo permanente que penetra en la mente del espectador en busca de respuestas que quizás nunca encuentren certeza alguna: 
 – 10. “Donnie Darko” (2001) – Richard Kelly 
  
 ¿Qué peso carga consigo un chico que recibe la noticia de que el mundo se terminará en menos de un mes? La historia de Donnie, un chico que sufre problemas mentales, cambia una madrugada de octubre, cuando una extraña presencia le devela el cruel porvenir de la Tierra y la raza humana. Se trata de un thriller inteligente y creativo que aborda al lector con preguntas sobre la trascendencia de la raza humana sobre la Tierra. 
 – 9. “Se7en” (1995) – David Fincher 
  
 Dos agentes de policía persiguen a un asesino en serie cuyo móvil parece ser el castigo de los siete pecados capitales. Poco a poco, ellos comienzan a descifrar el errático comportamiento del psicópata, hasta que se dan cuenta que en realidad parece tratarse de un enorme plan que también los involucra, junto con sus seres más queridos. 
 – 8.  “Mindwalk” (1990) – Bernt Amadeus Capra 
  
 Un político estadounidense y un escritor bohemio se reúnen en Francia para charlar sobre el pasado, mientras encuentran a una científica que participó en un importante proyecto cuántico que posteriormente fue utilizado por el gobierno estadounidense. A partir de su atención a un enorme reloj antiguo, los tres inician una conversación sobre el racionalismo y Descartes, que termina por prolongarse y explorar cada uno de los problemas políticos y sociales de la actualidad, así como los paradigmas de la física que rigen a la conciencia humana. 
 – 7. “Melancholia” (2011) – Lars von Trier 
  
 La historia de dos hermanas da cuenta de cómo el llamado a ser “día más feliz de la vida” de una de ellas termina en un auténtico laberinto sin salida, cuya única constante es la depresión. Mientras tanto, un extraño fenómeno astronómico está a punto de hacer colisionar a la Tierra y borrar todo rastro de civilización humana y vida en el planeta. 
 – 6. “The Last Temptation of Christ” (1988) – Martin Scorsese 
  
 El evangelio y la pasión de Cristo desde la visión de la novela homónima de Nikos Kazantzakis y el peculiar estilo de Scorsese dan como resultado una historia bíblica con tintes de realidad y humanidad. Jesucristo sigue el determinismo al que está condenado y justo en el momento en que será crucificado y volverá al cielo para salvar a los hombres, un supuesto ángel le hace una oferta difícil de rechazar, que pondrá en predicamento las bases del cristianismo y la supremacía de Dios sobre un ser terrenal que sólo desea vivir. 
 – 5.  “My Dinner With Andre” (1981) – Louis Malle 
  
 Una conversación casual entre dos amigos que se encuentran en un lujoso restaurante neoyorquino para ponerse al tanto de sus vidas, se convierte en una poderosa metáfora de la intrascendencia de la vida y cómo paso a paso el hombre pretende existir y crear en su entorno algo que supere al olvido, sobrepasando al impecable tiempo, la muerte y el irremediable olvido que trae consigo. 
 – 4. “The Turin Horse” (2011) – Bela Tarr 
  
 La leyenda sobre el día en que Nietzsche salió de su casa en Turín y en su camino, encontró a un cochero que golpeaba furiosamente a un caballo exhausto que no podía seguir avanzando. Aterrorizado, el filósofo defendió al equino y se lanzó sobre su lomo a llorar. Algunos marcan el episodio como el momento en que Nietzsche perdió la cordura; sin embargo, Tarr da un giro a la historia y se enfoca en la vida del caballo, una obra negra que muestra la decadencia en todo su espectro con una sencillez que deja al espectador abandonado, hundido y víctima de la pasividad. 
 – 3. “Wings of Desire” (1987) – Jim Wenders 
  
 El mundo está dividido y la política de guerra parece ser el motor que alimenta a la humanidad. Dos seres mitológicos del cristianismo descubren las profundas contradicciones existentes entre la sociedad berlinesa, separada por un muro y dos ideologías. Tratando de internarse en lo más profundo del sufrimiento humano pero incapaces de hacerlo en su condición, uno de ellos decide acercarse al cuerpo mortal de una chica para sentir en carne propia el cruel designio de vivir una vida destinada al sufrimiento. 
 – 2. “Through a Glass Darkly” (1961) – Ingmar Bergman 
  
 Un viejo escritor viudo se reúne con sus hijos, un adolescente que está camino a independizarse y una joven que sufre de crisis esquizofrénicas casada con un médico. Después de una serie de hechos desafortunados, el padre descubre que todo el tiempo que ha dedicado a sus hijos desde que murió su esposa es insuficiente y que posiblemente, el sufrimiento y la inconformidad serán los sentimientos que guíen el resto de sus días. 
 – 1. “Stalker” (1979) – Andrei Tarkosvksy 
  
 En una zona natural olvidada de la antigua Unión Soviética, se cuenta el mito de que existe una casa abandonada en donde se materializan todos los deseos e ilusiones de quien logre llegar a ella. Un grupo de guías ayudan a las personas que buscan desesperadamente la felicidad a dirigirse al lugar; sin embargo, el paisaje cambia intempestivamente y un extraño acontecimiento terminará por poner en duda el objeto de fe de cientos de peregrinos. 
 – A pesar de que no se trata de películas mundialmente conocidas ni con un poderoso imán en taquilla, todas crean disonancia en el espectador y despiertan esa parte de la conciencia humana, que está en permanente búsqueda con los más grandes misterios de la existencia. Si te gusta el cine y las historias que te hacen pensar, agrega a tu lista estas 45 películas que todo estudiante de filosofía debería ver . Foucault es, probablemente, el pensador más influyente del siglo XX. Descubre su trabajo filosófico a través de estas películas basadas en el pensamiento de Michel Foucault . 
 * Fuente: Taste of Cinema