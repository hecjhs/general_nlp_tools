8 películas de cine gore que pocos se atreven a ver

 
 El drama, la ciencia ficción y el terror son géneros que de la pluma de maestros como Isaac Asimov, Ray Bradbury, H.P. Lovecraft o Stephen King fueron cultivados en la literatura y posteriormente adaptados a la pantalla grande con la invención del cine. A pesar de las referencias literarias, el caso del gore es distinto: este género nació como una de las respuestas al Código de Producción de Películas que se apoderó de la gran industria del cine en 1934, después de que durante sus primeros años experimentara (como todo movimiento artístico) inmersiones en temas que no eran socialmente aceptados, como el sexo explícito, las violaciones, asesinatos y violencia gráfica. 
 El gore tomó fuerza de obras como la transgresora “The Moon is Blue” (1953) de Otto Preminger y Psycho (1960) de Hitchcock, mostrando rebeldía creativa en los planos y con respecto a las imposiciones del Código. A partir de entonces el género se expandió por Estados Unidos y los países europeos con pequeñas producciones serie B, que con el paso de los años fueron obteniendo mayores recursos. La crudeza del género no es apta para cualquier conciencia, muestra desmembramientos, torturas, violaciones y laceraciones físicas que incluyen sangre, intestinos, cerebros expuestos y el sufrimiento humano como telón de fondo. Estas son 8 cintas del cine gore que muy pocos se atreven a ver: 
 – “Cannibal Holocaust” (1980) – Ruggero Deodato 
  
 Un grupo de jóvenes se interna en la zona más indómita del Amazonas para documentar la vida de las tribus originarias que se cree que aún practican el canibalismo. Su desaparición alerta a un grupo de antropólogos que se internan en su búsqueda, encontrando sus grabaciones y con ellas un testimonio de horror y una crueldad impresentable. 
 – “Salò o le 120 giornate di Sodoma” (1975) – Pier Paolo Pasolini 
  
 Basada en el libro homónimo del Marqués de Sade, esta cinta es completamente gráfica y cruda. A pesar de que no abunda la sangre, muestra sin reservas las violaciones sexuales, golpizas, torturas y castigos, como comer excremento, a los que son sometidos los jóvenes. 
 – “Blood Feast” (1963) – Herschell Gordon Lewis 
  
 Comúnmente reconocida como la primera cinta gore, cuenta la sangrienta historia de una serie de asesinatos a mujeres caracterizados por el desmembramiento de las víctimas. El autor del crimen es un egipcio que trata de revivir a una deidad de la antigüedad con las partes del cuerpo de sus víctimas mientras sus crímenes siguen impunes. 
 – “Grotesque” (1963) –   Kôji Shiraishi Un doctor desequilibrado mentalmente secuestra a una pareja de jóvenes en su primera cita. Ellos quedan inconscientes y despiertan en un sitio subterráneo. El doctor les provoca toda clase de sufrimientos, torturas y vejaciones producto de su imaginación, mientras uno y otro observan cómo su pareja está padeciendo. 
 – “The Human Centipede” (2009) – Tom Six 
  
 Terrible historia de una pareja de turistas estadounidenses que viajan por Alemania cuando su auto se descompone y quedan varados en medio de una autopista desolada. A lo lejos encuentran una casa y deciden pedir refugio a un hombre que parece ser amable; sin embargo, se trata de un científico que las ata en un laboratorio clandestino y practica con ellas y otro turista secuestrado los más horrendos procedimientos quirúrgicos. 
 – “Trouble Everyday” (2001) – Claire Denis 
  
 Una pareja viaja hacia París para festejar su luna de miel en busca de una cura para el padecimiento extraño que sufren: un instinto animal posee a Shane con cada pulsión sexual y se convierte en un sádico caníbal, la misma enfermedad que presenta la esposa del doctor Leo, Coré. 
 – “Dawn of The Dead” (1978) – George A. Romero 
  
 El escape desesperado de los sobrevivientes después de una invasión zombie en los Estados Unidos (secuela de “Night of The Living Dead”) los lleva a atrincherarse en un centro comercial, donde en un primer momento verán realizadas sus utopías consumistas, pero el choque con la realidad de nuevo habrá de ser duro y el tiempo corre en su contra en la lucha por sobrevivir. 
 – “The Burning Moon” (1992) – Olaf Ittenbach 
  
 Clásico del cine B alemán, la trama gira sobre un par de relatos que un drogadicto cuenta a su hermana menor antes de dormir: un cura que ataca a familias con la premisa de las salvación y un asesino en serie demente que aterroriza a una familia entera. Estuvo prohibida en países con tradición católica por el escándalo que levantó la historia del sacerdote. 
 – ¿Qué es lo que busca desentrañar el gore?  ¿La exposición cruda, sin censura de la fragilidad humana , el terror psicológico que se materializa en un cuerpo o bien, una crítica sobre la sociedad actual? ¿Acaso encuentra un rastro de placer en el sufrimiento y la desesperación, en el dominio de un cuerpo sobre otro ejercido mediante el castigo, el abuso sexual, la tortura, o tal vez es un reflejo de los deseos reprimidos y la naturaleza del género humano? 
 *** Te puede interesar: 
 Los mejores directores de películas gore 
 Las 12 películas más sangrientas y violentas que sólo un ser insensible, frío y desentendido puede ver de principio a fin