Las mejores películas inspiradas por el genio de Stanley Kubrick

 
 ‘Echoes’ es una de las mejores muestras del increíble talento de Pink Floyd. Amantes de esta canción con el tiempo se dieron cuenta de que sincroniza perfectamente con una escena de “2001: A Space Odyssey”; ante esta situación, los integrantes de la banda inglesa se han cansado de negar la existencia de un vínculo y han señalado que es sólo una coincidencia, aunque afirman que hubieran amado trabajar en la cinta. 
  
 Este rumor, de ser verdad, no habría sido novedad. En numerosas ocasiones el arte de Stanley Kubrick ha influenciado directamente a músicos de todos los géneros. David Bowie ha asegurado que escribió ‘Space Oddity’ justamente después de ver la película de Kubrick; Blur realizó una clara referencia a “A Clockwork Orange” en el video de ‘Universal’, mientras que Guns N’ Roses’ hizo lo suyo en ‘Welcome To The Jungle’. 
 Estas influencias no son nada comparadas con la revolución que provocó en el cine. Las obras de Kubrick no sólo fueron aclamadas por la crítica, también introdujeron numerosas innovaciones cinematográficas que fueron utilizadas posteriormente. Es por ello que directores de la talla de Martin Scorsese, Steven Spielberg, Quentin Tarantino, Woody Allen y George Lucas lo han citado como una fuente de inspiración. Sin más, aquí están los elementos ideados por Kubrick que influyeron a verdaderas obras maestras. 
 Filosofía  Kubrick en la mayoría de sus películas intentó que su ideología no se hiciera presente en la trama, es por ello que el significado y las visiones de sus películas están abiertas a la interpretación. Para justificar esta afirmación, aseguró que la reacción subconsciente de las audiencias era mucho más relevante que lo que él creía. 
 – “Picnic at Hanging Rock” (1975) Peter Weir 
  
 Una historia idílica que basada en la novela de Joan Lindsay, de la misma manera que “2001: A Space Odyssey” al  final de la película no se llega a una resolución clara. 
 – “Donnie Darko” (2001) Richard Kelly 
  
 Richard Kelly en “Donnie Darko”, al igual que Kubrick, tuvo la intención de confundir a la audiencia. ¿Donnie puede viajar en el tiempo? ¿Es todo un sueño? 
 – “Inception”  (2010) Christopher Nolan 
  
 Los conceptos que presenta Christopher Nolan son increíblemente complejos y, al igual que “2001: A Space Odyssey”, en el  final no es claro qué es realidad y qué es ficción. 
 
 Temas 
  La gran variedad de temáticas abordada por las 16 películas de Kubrick han trascendido su época y han sido revisitadas en innumerables veces. 
 – “Solaris”  (1972) Andrei Tarkovsky 
  
 La respuesta rusa a “2001: A Space Odyssey”; esta película, dirigida por Andrei Tarkovsky, es una reinterpretación de la novela que inspiró a Kubrick cuatro años antes. 
 – “Eraserhead” (1977) David Lynch 
  
 Una obra creada para confundir y alterar a las audiencias, al igual que “A Clockwork Orange” y “Eyes Wide Shut”. 
 – “Fight Club” (1999) David Fincher 
  
 Una fascinante reflexión sobre la violencia que recuerda a “A Clockwork Orange”.  
 Cinematografía  Desde la perspectiva de un punto hasta el uso del steadicam y el video assist , el cine contemporáneo no sería el mismo sin estas innovaciones. –“The Prestige” (2006) Christopher Nolan 
  
 Una historia que narra la rivalidad entre dos magos con una cinematografía que retoma las tomas de los largos corredores, características de Kubrick. –“There Will Be Blood” (2007) Paul Thomas Anderson  Esta película narra una historia de un barón petrolero con una trama y ángulos de cámara que inmediatamente evocan a “2001: A Space Odyssey” . 
 – “Shame” (2011) Steve McQueen 
  
 Una pesadilla distópica que utiliza una fotografía tan perturbadora como estable. McQueen abunda en los temores psicológicos detrás de la intimidad de una manera muy similar al papel de Tom Cruise en “Eyes Wide Shut”. Efectos Especiales  “2001: A Space Odyssey” impactó al mundo por el uso de efectos especiales. La gran parte del cine actual no sería lo mismo sin las increíbles escenas del universo ideadas por Kubrick.  
 – “Blade Runner” (1982) Ridley Scott 
  
 Además de que temáticamente es muy similar a lo producido por Kubrick, Douglas Trumbull hizo los efectos tanto de esta película como los de “2001: A Space Odyssey” . 
 – “Pan’s Labyrinth” (2006) Guillermo del Toro 
  
 Es como “The Shining” si Danny fuera el héroe, el giro oscuro a la imaginación de una niña y los monstruos de esta fantasía son aspectos de los cuales Kubrick estaría orgulloso. 
 – “Interstellar” (2014) Christopher Nolan 
  
 Una de las mayores obras de arte de los últimos años que claramente no podría haber existido sin “2001: A Space Odyssey” . 
 En los primeros años de su vida, Stanley Kubrick no tenía un gran interés por el ámbito cinematográfico . Este número de influencias nos hace pensar sobre lo diferente que hubiera sido la historia del cine sin este director neoyorquino. 
 *** Te puede interesar:  
 Lo que no sabías de Stanley Kubrick  
 Las 93 películas favoritas de Stanley Kubrick 
 *** Referencia: 
 Pop Optic