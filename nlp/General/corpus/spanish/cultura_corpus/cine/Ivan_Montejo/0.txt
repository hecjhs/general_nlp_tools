10 finales alternativos que hubieran echado a perder las mejores películas de la historia del cine

 
 Como cada día, las plegarias de la mezquita lo despertaron, anunciaban que era la hora del rezo. La mayoría de los religiosos asistían al recinto religioso para escuchar al imán, pero él realizaba su oración en su hogar. Se arrodillaba en dirección hacia la Meca, lugar que algún día visitaría para cumplir uno de los más importantes pilares de su religión. 
 Se alistó para asistir a la universidad, todos los días pasaba por las orillas del Sena y observaba la mezquita Abderramán. En momentos anteriores este edificio hubiera pasado desapercibido, pero ese día era diferente, ya que enseñaría la batalla que definió el futuro de Europa. El templo hacía referencia al hombre que venció a Carlos Martel en la batalla de Poitiers en el 732. Cada vez que recordaba este conflicto armado se preguntaba qué hubiera pasado si el galo hubiera vencido y asesinado a Abderramán. ¿Su ciudad seguiría llamándose París? ¿El cristianismo se hubiera convertido en la religión predominante en lugar del islam? ¿Existiría una realeza europea en lugar de una bereber? 
   
 Para muchos historiadores, este ejercicio de historia contrafactual es un acto literario que no aporta mucho al recuento del pasado. Sin embargo, preguntarse sobre lo que podría haber pasado es una buena forma de entender la relevancia de algunos hechos históricos. Probablemente Abderramán y sus bereberes hubieran derrotado a Carlos Martel en la batalla de Poitiers, habrían conquistado fácilmente el resto de Europa. En ese tiempo el califato árabe se encontraba en su mayor esplendor gracias a su unión y avances científicos; en contraste, el Viejo continente estaba conformado por una gran cantidad de feudos que constantemente rivalizaban entre sí. 
 En la historia este ejercicio es una gran forma para mostrar la importancia de algunos eventos. En cambio, en la ficción las cosas son diferentes, las narraciones forman parte de un proceso imaginativo en el cual prácticamente todo es válido. Generalmente pensamos que las historia que se nos presentan en el cine siempre tuvieron la misma estructura y únicamente sufrieron ligeros cambios en su adaptación a la pantalla. No obstante, estos ejemplos son la clara muestra que, incluso en las obras maestras, hay extraños finales que son capaces de arruinar hasta la más perfecta película. 
 – “Pretty in Pink” (1986) – Howard Deutch 
  
 Esta película presenta una complicada relación amorosa protagonizada por Andie, una chica pobre que está becada en la escuela, Blane, un joven rico que es sumamente inseguro. La historia de esta pareja inspiró a muchas personas, pero en el final original, Andie y Duckie, un amigo de la infancia, terminan juntos. En las exhibiciones de prueba el público inmediatamente rechazó este desenlace y los creadores decidieron filmar el final con el que fue estrenada la película. 
  
 – “Rocky” (1976) – John G. Avildsen 
  
 Por todos es conocida la inspiradora historia de un boxeador amateur que realiza el entrenamiento de su vida para enfrentar a Apollo Creed, el campeón del mundo. La meta de Rocky no es derrotar al gran favorito, simplemente desea durar los quince rounds sin ser noqueado y en el proceso integrar su vida y metas que al final logra cumplir. Sin embargo, en el final original Rocky decide perder la pelea a propósito simplemente para conseguir dinero y dejar el mundo del boxeo atrás. Al  final, Rocky acabaría en el retiro y con el dinero le compraría una tienda de mascotas a Adrian. 
  
 – “Dawn of the Dead” (1978) – George A. Romero 
  
 La segunda parte del clásico “Night of the Living Dead” hubiera causado un mayor impacto en las audiencias de haber integrado este final. En la versión original los protagonistas que logran mantenerse con vida optan por suicidarse antes de ser devorados por zombies hambrientos de su ser. El director decidió decantarse por un final esperanzador antes de mostrar una decapitación con las aspas de un helicóptero y un tiro en la cabeza. 
  
 – “Alien” (1979) – Ridley Scott 
  
 Originalmente, “Alien” finalizaba con la destrucción de Nostromo y con el escape de Ripley. Ridley Scott pensó en un cuarto acto en el cual el alien le devoraría la cabeza a la protagonista de la película, para después comunicarse con la tierra con la voz de Ridley. Por fortuna, los productores vetaron esta idea ya que creían que el monstruo debía morir al final de la cinta. 
  
 – “The Shining” (1980) – Stanley Kubrick 
  
 Después de una semana del estreno de esta película, Kubrick decidió modificar su final. En la primera versión se incluía una escena que mostraba una conversación en un hospital entre el señor Ullman, el administrador del hotel, y una enfermera que se encuentra con el pequeño Dany. Con esta secuencia la película no finalizaría con las dos imágenes icónicas de la película: Jack congelado en la nieve y la foto que muestra al encargado del bar del hotel con la fecha del 4 de julio de 1921. Los rollos originales fueron recolectados y destruidos por la productora para evitar futuras proyecciones. 
  
 – “First Blood” (1982) – Ted Kotcheff 
  
 Esta película vio nacer al verdadero héroe estadounidense. Si Estados Unidos sufrió una terrible derrota en Vietnam, las sagas de Rambo se encargaron de vengar al espíritu norteamericano. No obstante, en la novela en la que está inspirada esta primera cinta, el protagonista termina suicidándose. Originalmente “First Blood” fue leal a su fuente de inspiración, pero cambiaron de decisión cuando en las exhibiciones de prueba se rechazó el final al considerarlo oscuro y que desperdiciaba a uno de los mejores héroes de su tiempo. 
  
 – “Return of the Jedi” (1982) – Richard Marquand 
  
 El final de la saga de ciencia ficción más importante es conocido por todos: se logra destruir la Estrella de la Muerte y Darth Vader por fin le hace frente al Emperador Palpatine. De acuerdo con el productor Gary Kurtz, la primera versión de la conclusión es completamente diferente ya que Han Solo muere en su intento por destruir el generador del arma del Imperio. Aunado a esto, Luke queda sumamente alterado por su encuentro con Darth Vader y decide abandonar a los rebeldes, dejando a Leia con el resto de la Alianza. 
  
 – “Back to the Future” (1985) – Robert Zemeckis 
  
 La increíble historia de Marty McFly y el doctor Emmett Brown con el tiempo se ha convertido en una obra de culto que generalmente es colocada entre las mejores películas de ciencia ficción. Uno se puede cuestionar si hubiera alcanzado este éxito de haberse filmado la versión original, la màquina del tiempo se parecería más a un refrigerador que a un DeLorean, Marty regresaría a través de una prueba nuclear en Nevada y descubriría que gracias a su injerencia su padre se convirtió en un boxeador. 
  
 – “Titanic” (1997) – James Cameron 
  En la versión original, Rose, después de narrar su historia, encuentra el “Diamante Hope” en su bolsillo y arroja la joya al mar sin que nadie se dé cuenta. En el final alternativo, todos intentan evitar que la anciana cumpla su meta e inclusive en un momento parece que los buscadores de tesoros obtendrán su recompensa. Rose simplemente los tienta con darles el diamante sólo para tirarla al océano. 
  
 – “The Butterfly Effect” (2004) – Eric Bress y J. Mackye Gruber 
  Evan es un hombre que descubre que tiene la habilidad de viajar al pasado y tomar el lugar de sí mismo, lo que le permite tomar decisiones que alterarán su futuro. Lo que en principio parece una bendición se convierte en pesadilla cuando todas sus elecciones, a pesar de tener buenas intenciones, tienen consecuencias. En el final alternativo, la solución que encuentra el protagonista a todo su sufrimiento es prevenir su existencia, por lo que se transporta al interior de su madre cuando estaba embarazada y se ahorca con el cordón umbilical. 
  
 Sin duda alguna de estos finales alternativos hubieran cambiado la historia del cine. Otras películas  también tuvieron la capacidad de modificar al séptimo arte pero desgraciadamente jamás fueron hechas. 
 *** Te puede interesar: 
 Las películas con las finales más confusos ahora tienen explicación 
 8 finales alternativos que nunca has visto de las mejores películas