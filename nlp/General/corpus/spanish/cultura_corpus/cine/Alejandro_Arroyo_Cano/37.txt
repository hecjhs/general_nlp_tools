Películas que demuestran que el amor se puede convertir en tu peor obsesión

 
 La mente es un laberinto con muchos pasajes ocultos y callejones sin salida. Lo natural es que nuestro aparato psíquico vaya desechando poco a poco todo las frustraciones y angustias que se obtienen en el día a día, pero en ocasiones, un suceso impacta tanto en la conciencia del ser que altera el orden natural de las cosas y hace más oscuro el recorrido por la cabeza del humano. 
 Cuando se vive un acontecimiento traumático es casi imposible que la psique desahogue por sí sola la energía negativa que dejó dicha frustración y para mantener el equilibrio del sujeto, la mente reprime las emociones y recuerdos que puedan destruirlo. Si no existe un tratamiento indicado, lo que se guardó en el baúl del inconsciente saldrá a la luz algún día de manera más intensa. 
  
 Puede ser que las personas que presentan algún trastorno obsesivo nunca hayan desahogado aquel hecho traumático que marcó su vida. Por ello el inconsciente los obliga a actuar de maneras compulsivas, una manera de buscar que el sujeto libere el mal que lleva cargando. La persona obsesiva, al igual que un psicópata, se transforma en un ser inadaptado que no entiende las estructuras sociales y por ende, carece de los límites que regulan la interacción con otro individuos. No es su culpa la manera en que se conducen, su discapacidad psicológica los descoloca de la realidad, pero es indudable que acaban haciendo un daño al mundo que los rodea. 
 La ruptura amorosa es una de las experiencias más traumáticas en la vida. Lo que parece ser el sentimiento que más placer brinda, también es el que más destruye. Sumado a eso, si nunca se trató esta frustración puede que el ser reprima ese dolor en el inconsciente, pero en algún momento de su futuro, hará que el amor sea sinónimo de una obsesión. Las películas que siguen ilustra a la perfección la manera en que este sentimiento se transforma. 
 – “Stockholm” (2013) – Rodrigo Sorogoyen 
  
 La película inicia en una inocente fiesta, donde un chico le coquetea a una linda joven. Tras varios episodios de rechazo, ella acepta ir al departamento para pasar la noche juntos. A partir de este momento, la cinta da un giro altamente dramático. La chica, que al inicio se presentó como alguien sumisa e introvertida, empieza a revelar su lado histérico y agresivo cundo él le pide que deje su departamento. Lo que inició como una noche de amor, culminó en un horrendo cuadro obsesivo. 
  
 – “Coco Chanel & Igor Stravinsky” (2009) – Jan Kounen 
  
 Esta película es mitad ficción y mitad realidad. Chanel y Stravinsky sí se conocieron en vida, pero nunca se relacionaron de una manera tan apasionada y obsesiva como se muestra en la película. Aun así, la premisa resulta interesante cuando las personalidades reales de los personajes son colocadas en un escenario que las obliga a interactuar tan intensamente. Sin duda, la película supera a la verdad y presenta un fuerte cuadro de dependencia mutua que inevitablemente lleva a la destrucción. 
  
 – “Un homme idéal” (2015) – Yann Gozlan 
  
 Es la historia de un joven escritor fracasado que nunca obtiene reconocimiento por sus producción literaria, hasta que un día encuentra un manuscrito muy antiguo que vende a una editorial con su nombre. La motivación de realizar esta trampa es porque se enamora de una bella mujer de alta sociedad que sólo se relacionaba con escritores reconocidos. Cuando consigue el éxito por la novela robada, también obtiene el corazón de su amada, pero mantener en pie el engaño hará que con el tiempo lleve a cabo actos demenciales. 
  
 – “¡Átame!” (1990) – Pedro Almodóvar 
  
  Un joven impulsivo, inmaduro y agresivo logra salir del orfanato donde había vivido toda su vida. Lo primero que hace al ser libre es buscar a una actriz con la que esta obsesionado. Una vez que logra su encuentro con la mujer, y por obvias razones es rechazado, decide raptarla y encerrarla en su departamento. La cinta demuestra cómo la mente humana puede alterarse de tal manera por una obsesión, que la persona que la sufre puede convertirse en un total psicópata.  
 – “La pianiste” (2001) – Michael Haneke 
  
 Esta película es una adaptación de la novela homónima escrita por Elfriede Jelinek. Erika, profesora de piano de edad madura, tiene que lidiar con la relación enfermiza que lleva con su madre, quien la ha reprimido sexualmente toda su vida, creándole un problema de histeria. La única manera que tiene la pianista para desahogar su energía sexual es por medio del voyeurismo y el masoquismo. Un día un joven músico llamado Walter cae enamorado por su gran destreza en el piano y decide seducirla. A pesar de que al inicio la relación entre Erika y Walter parece ser normal, las fuertes frustraciones reprimidas de ella salen a flote y distorsionan todo lo bueno que restaba. El trágico desenlace sólo se puede explicar como un acto obsesivo.  
 – “Love” (2015) – Gaspar Noé 
  
 La película se centra en Murphy, un joven que vive harto y decepcionado de su presente por una razón, que al menos al inicio de la película, se desconoce. Poco a poco, Gaspar Noé muestra que en el pasado vivía en un mundo perfecto con su expareja, Elektra. Ahora él está sujeto a una nueva relación y por un descuido, ella termina embarazada. Un día recibe una inesperada llamada de Elektra y todas las emociones que había reprimido estallan. Así, entre flashbacks llenos de sexo y tomas geométricas rodadas con cinemascope, se reconstruye una historia de amor obsesivo entre su actual pareja y su exnovia.   
 – “Mommy” (2014) – Xavier Dolan   Steve tiene un trastorno de déficit de atención e hiperactividad que lo convierte en un joven salvaje e impulsivo. Un día es expulsado de su escuela donde estaba internado y su madre, Diane, es obligada a hacerse cargo de él. Ella es una mujer desesperada, soltera y depresiva, mientras que él tiene una relación edípica obsesiva que lo llevará hasta el extremo en su relación madre-hijo. Aunque los dos alimentan esta obsesión, llegará sin previo aviso una tercera persona que intentará ordenar todo. 
  
 – “Vértigo” (1958) – Alfred Hitchcock 
  
 Esta clásica historia de misterio creada por Hitchcock se centra en Scottie, un detective privado que sufre acrofobia debido a un reciente accidente. Cuando él decide retirarse del oficio aparece un amigo del pasado para pedirle que siga a su mujer, la cual parece ser víctima de un encantamiento que la obliga a hacer cosas extranormales. Scottie sigue a Madeleine por diversos lugares hasta quedar fascinado con ella. Después de salvarla de un intento de suicidio ambos se enamoran, sin embargo, Madeleine termina por arrojarse de un campanario, dejando a Scottie en una depresión melancólica. En última instancia, él conoce a Judy, una mujer que se parece a su amada difunta. En esta relación entre Madeleine y Judy es cuando la obsesión hace que la historia dé un giro inesperado.  
  
 – “El secreto de sus ojos” (2009) – Juan José Campanella   Benjamín Espósito, un juez de la corte argentina, durante 25 años estuvo investigó el misterioso asesinato de una mujer. Durante todo este tiempo intenta resolver el problema, el cual aparenta no tener respuesta. Al  final, su obsesión lo conduce hasta la casa del viudo que aparentemente vive alejado y retirado por el trauma de perder a su esposa. Es aquí cuando las obsesiones del exesposo y de Benjamín se descubren y se superponen mostrando un secreto que cambiará el paisaje del pasado. 
  
 – “Belleza americana” (1999) – Sam Mendes 
  
 Lester Burnham es un hombre en crisis. La relación con su esposa murió hace años y su hija lo ve como un perdedor decrépito. Él vive en un mundo de decepciones y resignaciones, hasta que un día conoce a la bella amiga de su hija, obsesionándose inmediatamente de ella. A partir de este hecho, Lester reivindica el sentido de su vida, volviendo a sentir la vitalidad que deseaba. El drama surge cuando la hija entabla una relación con su vecino, quien en un punto de la historia se enlazará de manera peligrosa con la familia de Lester.  
 – Si te gustaron estas películas que se basan en las obsesiones del hombre, puedes conocer otras que tratan con trastornos  más severos. 
 *** Te puede interesar: 
 Explicación filosófica de películas que significan más de lo que crees 
 Los 8 mejores besos en la historia del cine que le darán pasión a la vida