10 películas que fueron prohibidas en el mundo sin argumentos válidos

 Toda censura es peligrosa porque detiene el desarrollo cultural de un pueblo.Mercedes Sosa Por un lado, la labor del cine es retratar algún fragmento de la realidad con una gran estética visual, una discurso fluido y una narrativa que juega con el orden habitual de las cosas. Por otro, en el afán de crear un mundo alterno y sorprendente, los directores deforman los objetos e imponen escenarios subjetivos, nuevos umbrales a dimensiones desconocidas. En cambio, existen otros formatos que buscan capturar la vida de manera cruda y fidedigna con la intención de denunciar algún hábito, pensamiento o recuerdo del humano. 
 Las mejores cintas son las que logran cruzar la objetividad con lo fantástico, revelando artísticamente una verdad oculta que incomoda a la sociedad o a cierto grupo de personas. Cuando esto sucede, las personas encargadas de velar por la salud cultural de su país aplican el temible acto de la censura. La prohibición de una película puede ser por cualquier cosa, desde un hecho que compromete al gobierno hasta un capricho de un líder o mandatario. Por ejemplo, en 1984, el alcalde de New Jersey censuró la cinta protagonizada por Carmen Dauset Moreno debido a que la actriz, al final de un baile, levanta las telas que caían hasta sus pies y enseña el fondo del vestido.  Su palabras fueron: “Degrada a la mujer. No creo que una mujer, excepto para su marido, se deba exhibir de esta forma”. Esa fue la primer película o video que fue prohibido en la historia cinematográfica. Como se puede leer, el motivos por el que fue censurada es absurdo, pero aún así se retiró de las primitivas salas de cine. Las películas prohibidas que siguen a continuación fueron durante un tiempo por lo que parece ser un capricho de algún alto mando. 
 – 10. “Hail Mary” (1985), Jean-Luc Godard 
  
 La película fue censurada en Francia casi desde su estreno debido que había escenas con actos religiosos, como oraciones o ceremonias cristianas, y en las escenas siguientes se mostraba un desnudo completo por parte de la protagonista. Esta combinación era considerada una injuria de las más bajas.  En Argentina y Brasil, países donde la población es muy religiosa, fue prohibida. En palabras de Godard, la cinta trata de una jovencita llamada Mary que, en un momento determinado de su vida, participa en un acontecimiento excepcional que ella nunca hubiera deseado por sí misma. 
  
 – 9. “Nymphomaniac” (2013), Lars von Trier 
  
 La película de Lars von Trier fue prohibida en Turquía debido a sus desnudos y escenas sexuales. La sorpresa fue que el grupo que estuvo a favor de retirar la cinta de las salas de cine estuvo encabezado por el directo Nuri Bilge Ceylan, quien un día ganó el mayor reconocimiento del Festival de Cannes. Este cineasta frustrado la calificó como “pornográfica, antinacional y amoral”. Otros países de Europa Central siguieron los mismos pasos, excepto un grupo de artistas rumanos que protestaron contra la censura y el gobierno tuvo que ceder. 
  
 – 8. “Saló” (1975), Pier Paolo Pasolini 
  
 Nadie niega la brutalidad de imágenes que presenta Pasolini. “Saló” es una barbarie sexual y decadente del lado insensible del humano; aún así, grandes personalidades como Martin Scorsese y Alec Baldwin la defendieron sin titubeos. La película ha sido censurada en diversas partes del mundo y en diferentes épocas. Algunos de los gobiernos, como los de Australia y Nueva Zelanda, dicen que contiene una “crueldad ofensiva de alto impacto, violencia sexual y fetiches repugnantes”. La cinta tiene muchas escenas violentas de violación y asesinatos, aún así es considerada como una obra maestra. Quizá te puede interesar por qué dicen que sólo un pervertido puede verla hasta el final . 
  
 – 
 7. “Brokeback Mountain”  (2005), Ang Lee 
  
 La cinta es sobre un apasionado y trágico romance de dos vaqueros americanos. Una bella muestra sobre las relaciones homosexuales que se viven en los Estados Unidos. Por desgracia, algunos países de los Emiratos Árabes Unidos no lo tomaron de la misma forma y prohibieron la película debido a que en sus textos religiosos, las relaciones sexuales entre dos personas del mismo sexo son un pecado mortal. En su país de origen también hubo grupos religiosos que difamaron la cinta diciendo que van contra de los valores tradicionales. 
  
 – 
 6. “Last Tango in Paris” ( 1972), Bernardo Bertolucci 
  
 A pesar de que hoy los críticos la califican como cine de culto, en su estreno el gobierno italiano la consideró altamente transgresora y tomó todas las copias que existían en el país y las quemó. La Suprema Corte italiana la prohibió durante más de 10 años en su territorio. La controversia no se quedó en la censura, pues después de su estreno los actores Marlon Brando y Maria Schneider declararon que fueron abusados sentimentalmente y manipulados para realizar ciertas escenas. Unos cuantos años más tarde, la protagonista de “Last Tango in Paris”  confesaría que la escena sexual que aparece al final del filme no estaba en el guión y que todo ocurrió sin su total consentimiento. Conoce la historia de l a violación que traumó a una joven promesa del cine para siempre. 
  
 – 
 5. “Irreversible” (2002), Gaspar Noé 
  
 “Irreversible” es la obra más agresiva de Gaspar Noé, al retratar escenas de sexo violento, violaciones y otras brutalidades. Quizá esa sea la razón primordial para que Nueva Zelanda la prohibiera. La cinta siguió recorriendo el mundo con altibajos, algunos países aceptaban proyectarla con severas clasificaciones y anuncios de precaución antes de su inicio. Otros obligaron a censurar los 9 minutos de cinta que abarcaban la violación de Monica Bertolucci. En otros lugares se evitaron problemas y la prohibieron, cuidando la mente de sus espectadores. Si ya viste el filme y te pareció encantador, hay otras  10 películas que tienen el mismo.  
  
 – 
 4. “A Real Young Girl” (1999), Catherine Breillat 
  
 Como su nombre lo indica, la cinta de Catherine Breillat retrata a una niña que pasa a la pubertad, época donde su deseo sexual despierta y comienza a experimentar con su cuerpo. Si el tema es delicado por la corta edad de la protagonista, cuando su vagina se ve a cuadro, el público soltó un enorme grito de susto –o perversión–. Los tabúes de antaño se volvieron a hacer presentes en las mentes de las personas y los gobernantes la prohibieron en diferentes partes del mundo. Incluso algunos críticos de cine la calificaron como pornográfica, diciendo que la única intención de Catherine es excitar al público. 
  
 – 
 3. “Persépolis” (2007), Marjane Satrapi 
  
 La película autobiográfica de Marjane Satrapi fue prohibida en Irán y Líbano porque era “ofensiva para el Islam”. La prohibición se retiró más tarde cuando círculos de intelectuales y artistas se manifestaron en contra de este acto degenerativo para la cultura. La cinta se proyectó un par de años después con la condición de que se retiraran sus escenas sexuales. Lo contradictorio es que no hay ninguna escena pornográfica o violenta, sino una clara descripción del autoritarismo religioso y político del lugar. 
  
 – 
 2. “The Last Temptation of Christ” (1988), Martin Scorsese 
  
 En algunos países como Grecia, Turquía, Chile, México y Argentina, esta cinta fue prohibida o censurada durante muchos años. Hasta el 2010 seguía prohibida en Filipinas y Singapur. En la introducción de la cinta se recalca explícitamente que “la obra no se basa en los Evangelios, sino en una exploración ficticia de lo que pudo haber sucedido”. Grupos cristianos de todo el mundo se alzaron en su contra cuando en la última escena se presenta a Jesucristo clavado en la cruz mientras que Satanás lo tienta con visiones de una vida como cualquier mortal de la mano de María Magdalena. La protesta influyó para que miles de cines se negaran a proyectar la cinta. 
  
 – 
 1. “Love” (2015),  Gaspar Noé 
  
 En pleno 2015, el Ministerio de Cultura ruso le negó la licencia de exposición debido a que “la película contiene numerosas escenas pornográficas”. Este es el más absurdo argumento pues la pornografía es la representación de la materia sexual con el único propósito de la excitación. “Love” no es una película pornográfica en lo absoluto, porque el sexo explícito parte de un argumento cinematográfico. Hay todo un tratamiento de la historia, los personajes y las locaciones detrás de las escenas de sexo. A pesar de esta clara distinción, la película fue prohibida a mitad del Moscow International Film Festival, dejando a una gran número de seguidores de Noé con el corazón roto. 
  
 – Con la prohibición de “Love” queda claro que muchas de las personas que deciden la censura de estos productos artísticos desconocen por completo de qué trata la pornografía, satanizando cualquier representación sexual. Otras de estas cintas no fueron proyectadas para proteger intereses religiosos o ideológicos. Más ejemplos de este acto sin sentido se encuentran en las 15 películas más polémicas de la historia y en los libros prohibidos y el atentado contra la libertad.