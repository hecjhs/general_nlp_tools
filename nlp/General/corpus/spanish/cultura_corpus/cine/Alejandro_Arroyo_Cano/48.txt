27 películas para reír, llorar, gritar de miedo y aprender del amor que debes ver en Netflix antes de que las quiten

 
 Con un enorme catálogo de películas, Netflix se ha ganado el corazón de todos nosotros. Puedes encontrar la cinta adecuada para cualquier momento de tu vida. El único problema en esta inmensidad cinematográfica es encontrar la película indicada. ¿Cuántas veces has puesto una película que parece prometer que te enganchará de principio a fin y en los primeros 15 minutos descubres por desgracia que no tiene nada que ofrecer? 
 A estas alturas de tu vida ya lograste consolidar tu gusto, sabes realmente lo que quieres ver y estás en la búsqueda de nuevas películas que estén al nivel de tu increíble juicio, el cual se forjó a costa de ver al menos tres películas al día. A continuación te presentaremos las mejores cintas que te gustarán si buscas reír, llorar, gritar de miedo o aprender sobre el amor, pero eso sí, todas sobresalen en su género. Tal vez algunas ya las hayas visto, pero nunca está de más recordarlas y tenerlas frescas para tu siguiente debate sobre el séptimo arte. 
 – Películas para aprender del amor 
  
 El tema del amor se ha abordado de maneras muy diferentes en el cine y siempre va acompañado de otros recursos. Hay películas que sólo tienen lugar en un escenario idílico y perfecto, pero también encontramos historias románticas que se apoyan de la risa para llegar a nuestros corazones o se cubren de misterio y nos intrigan, pero al final el amor siempre sale victorioso. Cualquiera que tú prefieras, al apreciarlas encontrarás una enseñanza que es resultado de un proceso de reflexión de un breve momento trágico , porque siempre existirán adversidades en el amor y justo eso es lo que le brinda su dulce sabor en la meta. 
 “Cuando Harry encontró a Sally” (1989) – Rob Reiner 
  
 “Cuestión de tiempo” (2013) – Richard Curtis 
 “Amor ciego” (2001) – Peter Farrelly 
 “Una sola noche” (2010) – Massy Tadjedin 
  
 “Solicitud de admisión” (2013) – Paul Weitz  
 “Te amaré por siempre” (2009) – Robert Schwentke   
 – Películas para reír Dicen que la risa es la mejor medicina para el ser humano. Sea cierto o no, todos disfrutamos de una buena película que nos haga reír y quedamos con deseos de más. Generalmente el éxito de las comedias radica es el ingenio y perspicacia de los personajes, lo que hace innecesarias las situaciones espectaculares y superfluas. A continuación te presentamos algunos clásicos y otras películas recientes que ya se han consolidado como favoritas. 
 “Una noche en el fin del mundo” (2013) – Edgar Wright–“Cheech & Chong’s Up in Smoke” (1978) – Lou Adler  
 – “Amigos” (2011) – Olivier Nakache 
 – “Date Night ” (2010) – Shawn Levy 
  
 – “Supercool” (2007) – Greg Mottola 
 –“A toda madre” (2012) – Anne Fletcher 
 – Películas para llorar El llanto es una reacción física al dolor. Ya sea que el mundo te haya hecho una herida en el cuerpo o alma, a veces una buena película te ayuda a calmarte y liberarte de lo que te duele. Es muy probable que te autoprovoques tristeza al ver estas películas que van desde un drama ordinario hasta cine de auto, pero ten la certeza de que te puedes identificar con alguna de ellas y aprenderás más que en el mundo habitual. 
 Por desgracia, en esta categoría existen un sinfín de películas que te darían la estocada final, pero tenemos que limitarnos, por el momento, al catálogo de Netflix. Estas películas varían en la profundidad dramática de sus historias, pueden presentar una sucesión de conflictos sencillos, pero se apoyan de un desenlace trágico. Otras en cambio, desde el inicio nos sumergen en un panorama de infortunios y te mantendrán desde el inicio con un amargo sabor de boca. 
 “El listón blanco” (2009)  – Michael Haneke 
 “El llanto de la mariposa” (2007) – Julian Schnabel“Perdidos en la noche” (1969) –  John Schlesinger 
 “Atrapado sin salida” (1975) – Milos Forman 
  
 “¡Tan lejos, tan cerca!” (1993) – Wim Wenders 
 “Desde mi cielo” (2009) – Peter Jackson 
 “Perfect Sense”  (2011) – David Mackenzie“Marley y yo” (2008)  – David Frankel  
 – Películas para gritar de miedo  
  
 El miedo puede provocar adrenalina en nuestro cuerpo. Esta sustancia i ncrementa tu frecuencia cardíaca, contrae los vasos sanguíneos, dilata los conductos de aire y un sinfín de reacciones en tu cuerpo. Diciéndolo de manera sencilla, aumenta la potencia de tu metabolismo en un 100%. Este golpe de euforia se asemeja a subirte a una montaña rusa sin salir de tu habitación. Quizá estás buscando conseguir dicha emoción para salir de la paz del día a día, ese mundo donde todo es tranquilidad y belleza. Esta categoría reúne a los géneros de terror, misterio, thriller y todo lo que tenga que ver con violencia. Prepárate para conocer seres siniestros, viajar a escenarios lúgubres y vivir situaciones violentas. “Stigmata” (1999) – Gabriel Byrne“La maldición de Amityville” (1979) – Tom Berry“The Babadook” (2014) – Jennifer Kent“Fenómeno siniestro” (2010) – The Vicious Brother “Ringu” (1998) – Hideo Nakata“Psicósis” (1960) – Alfred Hitchcok“El despertar del diablo” (1981) – Sam RaimiCon estas 27 películas ya tienes suficiente material para ver Netflix por un largo rato. No olvides que el cine se disfruta más si estás acompañado.***Te puede interesar:Frases de películas que debes usar para que quien te gusta ya te diga que sí.47 finales de películas que sólo un conocedor ha visto*Nota: Las recomendaciones están basadas en el catálogo de Netflix México. Pueden variar en cada país y con el paso del tiempo, así que apresúrate a verlas.