Las 12 películas más sangrientas y violentas que sólo un ser insensible y frío ha soportado

 
 Aún se puede discutir si el gusto por el sufrimiento ajeno es parte de la naturaleza humana o es un síntoma adquirido de algún desequilibrio mental más profundo. También existe el caso de que las personas se acerquen a este género porque en esa línea entre los sangriento y la extrema violencia, sientan un golpe de adrenalina que provoca un placer extraño. 
 Se dice que el género del horror proyecta en el cine los miedos  y las ansiedades colectivas. Así, en un inicio se creaban monstruos que permitían al espectador destruir al mal, porque los rasgos de la perversidad y crueldad no pertenecían a los humanos.  Hoy, en cambio, la gran pantalla se concentra principalmente en psicópatas y personas con otros desórdenes mentales, mostrando y aceptando que la depravación, inmoralidad y sentido maligno está en uno mismo. 
  
 Es una posibilidad que la producción de este género aumentó porque los trastornos mentales , que a la larga crearán a los más temibles y despiadados agresores de la sociedad, están a la vuelta de la esquina debido a la reciente reivindicación de los valores humanos causados por la llegada de la posmodernidad. Quizá hacer películas sangrientas sea una forma de ir exorcizando los demonios que la gente guarda antes de que salgan a la luz y desaten el caos en el mundo. 
 – “Taxidermia” (2006) – György Pálfi 
  
 Un joven taxidermista está obsesionado con conseguir la inmortalidad aplicándose a él mismo los conocimientos de su oficio. Los trastornos que lo acompañan surgen desde su abuelo, quien fue un general durante la Segunda Guerra Mundial e hizo actos macabros. También se presenta la historia de su padre, un repugnante comedor de velocidad. Esta película lleva al extremo lo grotesco y lo combina con grandes efectos visuales que la vuelven un poema surrealista de lo asqueroso, que sólo una persona con estómago de acero aguantará. 
  
 – “Das Experiment” (2001) – Oliver Hirschbiegel 
  
 Basada en un experimento real de 1971, la película muestra la forma en que engañan a un grupo de personas para  llevarlas a una prisión alemana y recrear el papel de prisioneros o guardias con el fin de poder estudiar los límites de la personalidad humana. Lo que empezó como una forma rápida de ganar dinero, se convirtió en un juego macabro donde el principal objetivo es sobrevivir. Poco a poco los roles llevado al extremo, hasta crear una película sangrienta sin escrúpulos. 
  
 – “The Loved Ones” (2009) – Sean Byrne 
  
 Brent, un joven de diecisiete años, rechaza la invitación de salir con Lola, una de las chicas más tranquilas e inocentes del instituto. Lo que nunca se imaginó es que ella y su padre lo secuestrarían para torturarlo mortalmente con recursos tan salvajes como una lobotomía con un taladro de mano. Brent tendrá que luchar para sobrevivir a la macabra celebración que le tienen preparado por rechazar ir al baile de celebración. Esta película usa el recurso del humor negro para aligerar las escenas de violencia, aunque con el gran sufrimiento que se presenta, es difícil que aparezca una sonrisa en tu rostro.  
 – “High Tension” (2003) – Alexandre Aja 
  
 La joven Marie es invitada a pasar unos días en la casa de los padres de su mejor amiga, una granja aislada y rodeada de campos de maíz. Lo que inicia como un prometedor fin de semana de tranquilidad y alegría, de pronto se torna de los más violento. De noche llegará a la granja un visceral asesino que mata a todos los miembros de la familia.  Marie es la única que logra evadirlo en primera instancia, pero tendrá que enfrentarlo más tarde en un duelo sangriento a muerte.  
 – “Irreversible” (2002) – Gaspar Noé 
  
 Gaspar Noé presenta uno de los relatos más extremos, crudos y agresivos que se ha visto en la pantalla grande. Contado en modo inverso, se presenta al inicio la violación de una mujer al final de la noche. Situado en estado nauseabundo por los gritos ahogados de la mujer que ruega por su integridad, poco a poco se conocerán las situaciones que desencadenaron este brutal acto. Con un golpe tan duro al principio del filme, lo que sigue después es una carrera de aguante, que nadie ha soportado con placer o tranquilidad. 
  
 – “A Serbian Film” (2010) – Srdjan Spasojevic 
  
 Milo es una estrella del porno retirada que vive alejado de los excesos junto con su mujer e hijo. En una temporada de apuros económicos, visita a una antigua compañía de rodaje a pedir trabajo. De inmediato le encomiendan participar en una nueva película de porno experimental que aceptará de inmediato. Lo que desconocía Milo es que sería arrastrado a una tormenta de depravación, violencia y drogas que lo dejará en un abismo de locura. 
  
 – “Red White & Blue” (2010) – Simon Rumley 
  
 El tema principal de la película es la venganza, pero a diferencia de los demás thrillers que se centran en este recurso, “Red, White and Blue” da un paso más allá de lo común para presentar el carácter tanto del vengador como de la víctima. Frankie es un joven que cuida de su madre enferma y lleva largo tiempo separado de su novia, por lo que necesita una noche de diversión. Una día de borrachera, comparte con sus amigos a la joven Erica para descubrir más tarde que es VIH positivo. Para los ojos de Frankie, sólo hay una persona con quien desquitar su furia. 
  
 – “Eden Lake” (2008) – James Watkins 
  
 Steve organiza una salida de fin de semana con su novia Jenny para gozar de su tierno amor y tener la oportunidad perfecta para pedirle matrimonio. Sin embargo, en el tranquilo “Lago Edén” se encuentran con un grupo de adolescentes problemáticos que convierten lo que pudo ser un momento romántico en su peor pesadilla. De golpe, el escenario se transforma en un una experiencia sangrienta de horror e impotencia que más de uno dejará de ver por la crueldad con que la pareja es cazada y torturada. 
  
 – “Calvaire” (2004) – Fabrice Du Welz 
  
 El auto de Marc Stevens se avería en medio de un bosque alejado de toda civilización. De la nada surge un misterioso hombre que le ofrece ayuda y hospedaje en un tétrico pueblo. La agresión se desata cuando el anfitrión confunde a Marc con un enemigo del pasado y comienza a torturarlo al extremo que la película se transforma en un cuadro sangriento que parece no tener límites. El filme roza en el cine gore, así que será necesario contar con el temple necesario para no querer salir corriendo en el momento cumbre de la obra. 
  
 – “The Woman” (2011) – Lucky Mckee 
  
 Si estás buscando una película de terror salvaje y tortura psicológica fuera de lo convencional, “The Woman” saciará tus oscuras pulsiones con la historia de una mujer que es maltratada y torturada de las maneras más inhumanas por su esposo, con el fin de “civilizarla”. Al final, el asunto se sale de control, convirtiendo lo que parecía un drama familiar —con ciertas dosis de humor negro— en un espectáculo violento y con mucho gore. 
  
 – “Martyrs” (2008) – Pascal Laugier 
  A comienzos de 1970,  Lucie, una niña que estuvo desaparecida durante un año, es vista mientras camina por una carretera totalmente desorientada y en un estado catatónico, incapaz de contar lo que sufrió. Más adelante, la joven regresará al antiguo matadero donde estuvo secuestrada para buscar la más sangrienta y extrema venganza que la industria cinematográfica ha visto. Esta película es para las personas que buscan un desafío visual y emocional, que dicen ser insensibles a las representaciones de la realidad. 
  
 – “Funny Games” (1997) – Michael Haneke 
  
 Una familia llega a su casa de fin de semana para pasar unas tranquilas vacaciones lejos del ajetreo cotidiano. En las primeras horas de su llegada, dos jóvenes tocan la puerta con el pretexto de buscar un poco de comida. Inmediatamente se instalan en la casa y comienzan a golpear al padre y la madre. En esta película, Haneke juega con las emociones de los espectadores con la tortura que, sin justificación alguna, destruye a la inocente familia. No hay razón para este acto, sólo hay espacio para una cruel e inhumana agresión por diversión. A comparación de las pasadas películas, “Funny Games” no escurre de sangre, pero el terror psicológico que crea el director supera a los demás filmes en una gran medida. 
  
 – Si careces del tiempo para buscar las películas, aquí tienes otra lista que podrás ver en un instante . 
 *** Te puede interesar: 
 11 películas de horror extremo de las que nunca habías escuchado hablar 
 10 películas de terror tan intensas que podrían provocarte un infarto