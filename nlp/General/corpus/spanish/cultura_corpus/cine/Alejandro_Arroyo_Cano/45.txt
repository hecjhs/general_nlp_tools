Películas para reconfortar un corazón roto que puedes ver en Netflix

 
 Un día me enamoré de una mujer y perdí. Aún guardo en mi memoria enamorada la figura de su sombra y su olor a tierra mojada, que me conduce y seduce de manera inevitable al último día que me fulminó su mirada. Todavía siento que en la lejanía, su recuerdo me acecha y me persigue en cada brecha, porque ella era lo que yo más deseaba en esta tierra. 
 Quería recorrer el mundo con ella, conocer los rincones secretos del norte, los santuarios perdidos del sur y regresar a casa transformado por todo lo que mi ojos habían presenciado, pero con la tranquilidad que el hogar que juntos hubiéramos alzado, siguieran nuestros corazones intactos . 
  
 No lo logré. Pude tener su atención, pero no su corazón. Sólo viví unos meses acompañado y al final del año, ganó su amor del pasado. Me abandonó después de embriagarme con su presencia y como un adicto sin su vicio, caí en la demencia . 
 Entonces me fui llorando a casa y me encerré una semana mirando una pantalla. De este trago amargo, comparto las películas que me ayudaron en ese estado, esperando que al igual que yo, te atrevas a mirar a otro lado . 
 Observé una película por día y éstas son mis enseñanzas. 
 – “Adventurland: Un verano memorable” (2012) – Gregg Mottola 
  
 Lo primero que entendí fue que el amor, como una feria de verano, tiene una duración, un lugar y una intensidad propia. Forzarlo más allá es romper las reglas. Esta película se desarrolla justo durante las vacaciones de unos adolescentes que durante un breve tiempo se conocen a fondo y entablan grandes lazos, pero cuando llega el tiempo de regresar al ritmo de vida habitual, es inevitable decir adiós, pero no por ello hay que sufrir. 
 – “Amor sin escalas” (2009) – Jason Reitman 
  
 Tardé en comprender que no puedes invadir el corazón que ya está ocupado por otra persona. Yo, como el personaje principal, creí que me era suficiente mi soledad, pero cuando se descubre lo mágico que puede ser compartir el tiempo con alguien, es inevitable no aferrarse a esa persona. “Amor sin escalas” es la historia de un hombre que se pasaba la vida trabajando y sin prevenirlo, se enamora de una persona que comparte muchas afinidades. El problema es que el corazón de esa otra persona ya fue conquistado. 
 – “Una historia diferente” (2010) – Anna Boden  
 Si por algo no triunfó mi amor fue porque no surgió de la amistad, el paso más importante antes de formalizar una relación. Este drama romántico demuestra que hasta en las situaciones más extrañas, como en una clínica psiquiátrica, por ejemplo, puedes encontrar a un grupo de amigos que te apoyarán en los momentos más difíciles de tu vida y quizá, como por arte de magia, surja el verdadero amor. 
 – “El mejor lugar del mundo” (2009) – Sam Mendes  
 Yo nunca lo supe, puesto que nunca se hizo realidad mi sueño, pero esta cinta me enseñó que hasta la pareja más unida puede tambalearse en una cuerda floja. Su escasez de dinero y la noticia de un embarazo hace que pierdan la dirección de sus vidas y se den cuenta de que encontrar o formar un hogar es mucho más difícil que sólo vivir juntos. 
 – “Date Night” (2010) – Shawn Levy 
  
 Para mi quinto día me sentía con más ánimos, así que decidí ver una comedia romántica. Ésta es la historia de un matrimonio que transformó su vida en una rutina. Por querer hacer un cambio, comienzan un juego de identidades inocente hasta que son confundidos por verdaderos criminales. En un estado así, lo mejor que puedes hacer es voltear a ver a tu pareja y reír. 
 – “Solicitud de admisión” (2013) – Paul Weitz 
  
 El sábado fue un día de reflexión, pues observé en la película el orden natural del amor. Un hombre correcto y con grandes valores ayuda a un joven a entrar a una universidad de prestigio. En su camino se encuentra con una mujer del comité directivo que enamora sin planearlo. Lo que sigue es un dulce encadenado de hechos que culmina con la unión de dos seres. 
 – “Empezar otra vez” (2014) – John Carney 
  
 El último día aprendí la importancia de dejar ir las cosas que te hacen daño. Es la historia de dos personas que el destinó unió con el único fin de que se ayudaran a sanar mutuamente. Cada uno tiene sus desdichas y su solución, pero a veces un mal amor te vicia en un círculo. Es entonces cuando una ayuda externa te hace recuperar tu camino. 
 – 
 Yo recuperé mi camino y ahora estoy aquí, gozando de mi destino. Mi corazón roto sanó y ahora vivo en el pecado. 
 *** Te puede interesar: 
 Películas de terror dirigidas por mujeres que te volverán paranoicoLas mejores películas de culto de los 90