Matt Doust, un espejo de su época

 El arte es el reflejo de su tiempo, hay momentos que parecen repetirse pero cada uno es único. El contexto sociocultural marca a un artista, no importa si desea representar al mundo en el que vive  o que pretenda romper con el conocimiento establecido, es gracias a su entorno social que actúa de determinada forma. Como un intento de mostrar el tiempo que vivió, el pintor Matt Doust reflejó su época y su visión del mundo a través de pinturas hiperrealistas. 
  Matt nació en Estados Unidos, pero vivió gran parte de su vida en Perth, Australia, donde aprendió a pintar de forma autodidacta, regresó a Los Angeles y logró colocarse en el mercado del arte, en el que tuvo gran éxito entre coleccionistas y celebridades como Mickey Rourke o Jim Carrey. 
  Los retratos marcan la visión del artista, su filosofía, sus gustos. Su trabajo resulta una conversación íntima e incluso voyerista entre él y el protagonista. Las líneas marcan los cuerpos que conocía, que conocemos; los cuerpos del nuevo milenio que son tan normales para nosotros como extraños para quien vive en otro contexto. 
   Sus pinturas hiperrealistas dejan atrás las representaciones figurativas, Doust pinta lo que ve como lo ve, siendo éste su toque personal; sus modelos son a quienes plasma en lienzos pero es la esencia del artista la que se encuentra en estos. Expresión, insensatez, juego y más se puede encontrar en aquellas personas que confrontan, desde el lienzo, al espectador. 
  Matt Doust, pintor hiperrealista, murió el 29 de agosto de 2013 a causa de un ataque epiléptico, enfermedad que padeció toda su vida; tenía programada una exhibición en Los Angeles, muestra que se llevará a cabo como un tributo el próximo 7 de septiembre. 
http://culturacolectiva.com/wp-content/uploads/2013/08/boca-hiperrealista-.jpg
http://culturacolectiva.com/wp-content/uploads/2013/08/cuerpo-hiperrealismo-Doust.jpg
http://culturacolectiva.com/wp-content/uploads/2013/08/cuerpos-Doust1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/08/ojos-hiperrealismo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/08/pecas-hiperrealista1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/08/pintura-doust-hiperrealismo.jpg