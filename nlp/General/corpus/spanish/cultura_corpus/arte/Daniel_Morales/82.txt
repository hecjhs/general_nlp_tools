Alex Seton, el mármol como algo mundano

 Alex Seton es un artista australiano quien trabaja con mármol para lograr lo contrario de lo que muchos artistas intentaban hacer con ese material: hacer de sus obras algo mundano. 
  Las grandes esculturas de dioses mitológicos, escenas bíblicas e historias antiguas fueron talladas en mármol; pero Alex Seton, al estar consiente de la “belleza artística” que con el mármol se logra, basó su arte en mostrar esa consagración a través de objetos o piezas cotidianas talladas en este material. 
   Un choque cultural al explotar uno de los materiales más preciados en el mundo artístico con figuras comunes de nuestro tiempo, no más ángeles y demonios, dioses y titanes, Seton muestra ropa deportiva, cámaras de seguridad, banderas, objetos que lejos de quitar valor a esta roca metamórfica, demuestran que en ese material las cosas pueden ser aún más hermosas. 
  
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-sudadera-marmol.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-sudadera-marmol-1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-marmol-mangas-cultura.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-gancho-marmol-cultura.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-cuerpo-cultura.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-camara-cultura.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/Seton-bandera.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/seton-bandera-no-surrender-cultura.jpg