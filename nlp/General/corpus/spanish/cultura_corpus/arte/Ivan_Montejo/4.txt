Las historias de las mujeres asesinadas por no ser dignas

  Desde chica Mutlu había fascinado a las personas que la rodeaban con su voz, en todas las reuniones familiares su padre constantemente le insistía que cantara para sorprender a las visitas. Su interés se fue cultivando con el tiempo y cuando vio el comercial supo que era su oportunidad. El reality la podría dar a conocer y le abriría las puertas a una carrera prometedora. Después de largas discusiones con su padre, éste por fin aceptó. No importaba que su familia la rechazara, lo único que quería era la bendición de su papá y después de obtenerla se embarcó en la aventura.   El día de su debut fue todo un éxito, logró una puntuación de 83 por ciento y sabía que lo podía hacer mejor, la competencia era dura, pero confiaba que podía con ella. Menos de una semana después de la transmisión llegaron las amenazas, las voces eran familiares, pero decidió no prestarles atención. Era la madrugada del 18 de abril,  estaba durmiendo en su cama cuando irrumpieron en su hogar, una bala alcanzó su cráneo. 
 


Este ataque presumiblemente fue perpetuado por los familiares de su padre que interpretaron que su decisión era un insulto para todos sus parientes. Mutlu tuvo suerte de sobrevivir este ataque; sin embargo, año con año miles de mujeres y hombres sufren de este tipo de asesinatos. 
 


El crimen de honor consiste en el asesinato de una persona por parte de un miembro de la familia. Estos homicidios se comenten porque uno o varios familiares consideran que la acción de una persona ha manchado a su nombre; los pecados pueden ir de incesto a violación, rechazar un matrimonio arreglado, huir de casa, cometer adulterio, embarazarse o perder la fe.  Generalmente se piensa que estos casos únicamente afectan a las mujeres que siguen viviendo con sus padres, pero los intentos de control van mucho más allá del hogar. Hace algún tiempo Rama Kunwar se casó con un hombre que era considerado de casta superior, a pesar de las protestas de su familia. De eso habían pasado ocho años, su relación era estable y tenía un hijo de tres años, por lo que decidió regresar  a su pueblo natal con la esperanza de ser perdonada. Al día siguiente de su llegada, una treintena de hombres la arrastraron por la calle, la cubrieron de queroseno y le prendieron fuego. Sus gritos de dolor fueron un alivio para sus familiares, habían recuperado el honor que les había robado. 


Erróneamente se asocia  a este fenómeno exclusivamente con el Islam; si bien muchos países tienen reglas islámicas que lo permiten, hay otros lugares, como Irán, que no castigan esta práctica con la severidad necesaria del Código penal del Imperio napoleónico. Estos casos tampoco son exclusivos de Medio Oriente, ya que han sucedido este tipo de crímenes en todas las regiones del globo. 
 


En Kuwait estos crímenes son castigados por el Código penal con tres años de prisión o una multa de 3 mil rupias. Consciente de la ineficacia de esta reglamentación, Sheikha Lulu M. Al-Sabah, a través de su organización JAMM, presenta la exhibición “Abolish 153” que busca cambiar la ley del país árabe. 
 



Sheikha asegura que Kuwait es una tierra de contrastes, por un lado las mujeres por fin lograron alcanzar todos sus derechos políticos en el 2005, pero todavía existen este tipo de prácticas que limitan la libertad de las mujeres. Ante esta situación, asegura que su esfuerzo tiene un gran potencial de modificar la situación: 




“Creo que el arte es un medio eficaz para promover el cambio social, especialmente en entornos en los que temas tabú son barridos bajo la alfombra”.  




“Mediante la celebración de exposiciones con obras de arte que abordan el tema de los crímenes de honor, creamos una razón válida para mantener este problema vigente en los medios de comunicación”.




“El arte es una manera de darle sentido al mundo que nos rodea. Es un medio para expresar lo bello, lo feo y lo profundo. Nos ofrece diferentes perspectivas y tiene la capacidad de conectar a las personas de todas las culturas. El arte a menudo permanece mientras que los edificios e ideologías políticas caen”.




La exhibición reúne el trabajo de 11 artistas en 40 trabajos. Las obras tocan los temas de visibilidad y representación, justicia, libertad reproductiva y derechos humanos. 

 


 El arte de Maha Al-Asaker centra sus imágenes en flores y partes del cuerpo desnudas que se pueden observar tras un material transparente. Su arte se enfoca en la ruptura de los tabúes que todavía existen hacia el cuerpo humano.




 Farah Salem participa con su serie “Cornered”, que consiste en mujeres refugiadas en cajas en diversos escenarios. Estas imágenes representan las frustraciones que implican ser mujer en una sociedad que las pretende controlar: “Terminamos tan encerradas en la caja que olvidamos cómo salir”. 
        Es común que los artistas aparentan adelantarse a su época, crean obras que rompen las estructuras que antes eran la norma; por esta razón en varias ocasiones han sido incomprendidos y perseguidos . Sheikha Lulu M. Al-Sabah se une a estos revolucionarios al luchar por un mundo mucho más inclusivo y justo para la mujer.    *** Te puede interesar:   El artista con VIH que creó armas mortales con su sangre

 Mujeres que han revolucionado el arte usando su cuerpo como performance  * Referencia:   ArtSlant