El artista con VIH que creó armas mortales con su sangre

 En la década de los ochenta, una extraña enfermedad atacó a la comunidad gay de Estados Unidos. Durante los primeros años no fue tomada en cuenta, parecía que sólo perjudicaba a los homosexuales y buena parte de la sociedad estadounidense rechazaba esta orientación sexual. Sólo fue hasta que los números alcanzaron tendencias epidemiológicas cuando las autoridades le prestaron atención. 
 Un estudio publicado en el American Journal of Medicine en 1984, adjudicó el surgimiento de la epidemia a un aeromozo que aparentemente había contraído la enfermedad en África occidental y fue llamado “paciente cero”. Su nombre era Gaëtan Dugas y pronto surgió una leyenda negra alrededor de su nombre. Varias personas aseguraban que frecuentaba bares gay por todo el mundo y seducía a los hombres para tener relaciones sexuales, después de esta actividad le confesaba a sus parejas que él tenía “cáncer homosexual”, por lo que ellos estaban contagiados y morirían en poco tiempo. Ya no se llama cáncer homosexual, sino SIDA y es causado por el Virus de Inmunodeficiencia Humana y que Dugas no fue el paciente cero, aunque lo más probable es que sí contagió a una enorme cantidad de personas por estas prácticas. 
  
 Fue precisamente en esta época cuando un artista estadounidense contrajo la enfermedad. Barton Lidice Benes vivió y trabajó en su departamento de Greenwich Village en Nueva York, al que se refería como sus “catacumbas”. Este recinto guardaba una colección de extraños objetos procedentes de todas partes del mundo: tótems de vudú, animales disecados, heces fosilizadas, la taza de té de Hitler , la orina de Sylvester Stallone, un pedazo de zapato de Elizabeth Taylor y una migaja del pastel  de bodas del Príncipe de Gales. 
  
 Entre estos objetos se encontraba un reloj de arena que funcionaba con las cenizas de dos de sus amigos. Muchas personas creían que él las había asesinado, pero en realidad estos dos compañeros habían muerto a causa del VIH. Cuando comenzó a darse cuenta de que muchas de las personas que le rodeaban estaban muriendo, decidió hacerse una prueba para conocer si tenía la enfermedad. El resultado fue positivo. 
  
 Esta noticia lo persiguió por el resto de su vida, era algo con lo que no podía lidiar. Un día, mientras preparaba la cena, se cortó por accidente, la sangre comenzó a brotar y por un instante temió por su vida; no por morir desangrado, sino por el riesgo de contagiarse del terrible virus. Al poco tiempo tomó consciencia de lo ridículo que era su miedo, pero por un momento olvidó que tenía VIH y el terror de contraer la enfermedad lo inspiró para convertirse en un “terrorista”. 
  
 Cabe recordar que en los primeros años en los que el VIH comenzó a manifestarse, nadie sabía exactamente cómo actuaba, cuando un paciente llegaba y se le diagnosticaba con la enfermedad, era aislado del resto de las personas porque no se conocía la forma en la que el virus actuaba. La única información disponible era que las personas estaban muriendo a causa de algo que tenían en la sangre. 
  
 Al tomar consciencia sobre el poder de su sangre, comenzó a crear una serie de “armas letales” y llenó toda serie de objetos con ella: botellas de perfume, pistolas de agua, atomizadores, dardos y jeringas. 
  
 Todas estas “armas” fueron exhibidas en cajas de cristal que no permitían ser utilizadas, pero el mensaje era claro. El miedo a lo desconocido se apoderaba de estos objetos que en principio eran insignificantes. La sangre era el arma mortal. 
  
 Esta particular serie fue exhibida por Laurel Reuter en su galería en Dakota del Norte, la popularidad de esta colección la llevó a Suecia, donde sucedió algo sumamente interesante. 
  
 A los dos días de haber sido inaugurada la exhibición, las autoridades suecas le ordenaron cerrar la presentación. Le mencionaron a Laurel que por ley debían velar por la seguridad de los visitantes y evitar a toda costa el riesgo de un contagio. La situación se agravó cuando diversos panfletos y periódicos comenzaron a correr el rumor de que en la galería de arte se estaba vendiendo sangre contaminada por litro. 
  
 A partir de ese instante el trabajo de Barton Lidice se convirtió en un riesgo para la seguridad pública. Tuvo que colocar sus piezas en hornos que eliminarían todo agente que fuera capaz de contagiar a una persona, las “armas letales” tuvieron que llevar un certificado que garantizara la seguridad del objeto para que el artista estadounidense no pudiera ser catalogado de terrorista. 
  
 Al mismo tiempo que su arte se alimentó del miedo de la época, también se convirtió en una sátira hacia lo irracional que puede ser el terror.  Actualmente, sabemos que todos estos miedos no tenían fundamentos y podrían ser tachados de ridículos, pero la naturaleza de estas armas sigue teniendo  impacto. La sangre estará envuelta de una capacidad de alterar nuestros sentidos y al mismo tiempo, fascinarnos. 
 Recientemente la publicación alemana Vangardist imprimió una revista que tenía tinta de impresión mezclada con sangre de tres donantes cero positivo. Estos esfuerzos artísticos son de suma importancia para derribar mitos sobre el VIH y conscientizar a la sociedad. 
 *** Te puede interesar:  
 El proyecto artístico que te enseña cómo masrtubar y hacer feliz a una mujer en la cama  
 Ilustraciones para disfrutar del sexo, la pasión y ser más perverso cada día 
 * Referencia: 
 Radiolab