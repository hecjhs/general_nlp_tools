25 preguntas que todo conocedor de arte debería poder responder

 
 Claude Lévi Strauss asegura que en el oeste de Canadá, sobre la costa del Océano Pacífico, los pintores y escultores formaban parte de una estirpe diferente. Su nombre implicaba un aura de misterio, de hecho la persona que los sorprendiera usurpando su labor era castigado con la muerte. Para poder formar parte de este grupo, los interesados debían de pasar por largos y severos ritos de iniciación. Todo este misterio se debía a que las máscaras representaban a entidades temibles que poseían a cualquier individuo que no estuviera preparado. 
 Así, entre estos pueblos el arte iba más allá de la emoción estética. La acción de crear una obra de arte estaba sumamente ligada a un origen sobrenatural que los convertía en un grupo único. Nadie más podía gozar de este poder, ni los nobles, la gente común o los esclavos; era una fortaleza que se encontraba en el fondo del mar o en el corazón de los bosques que sólo podía ser admirada y temida. 
 Si reflexionamos en torno a esta idea nos daremos cuenta de que estamos más cerca de esta postura de lo que nos gustaría aceptar. Cuando estamos ante una obra de arte no podemos evitar sentirnos cautivados. Nos acercamos para conocer esos lienzos que nos transportan al tiempo en los que fueron imaginados por el pintor, por un momento nos unimos con los antiguos pueblos canadienses al ser testigos de un poder que sólo los más grandes tienen. 
 – ¿Qué es la cueva de Rouffignac? 
  Se encuentra en Francia y es uno de los conjuntos pictóricos más importantes del Paleolítico gracias a sus 250 grabados que tienen unos 13 mil años de antigüedad. 
 – ¿Cuál es la obra artística mexica más grande? 
  Es el monolito de Tlaltecuhtli, hallada el 2 de octubre de 2006 y representa a una deidad de la Tierra. La piedra fue rota desde tiempos prehispánicos muy probablemente para “matar” a la pieza y que perdiera el poder espiritual que algún día tuvo. 
 – ¿El arte ha provocado conflictos internacionales? 
  Sí, en 1054 sucedió el Cisma de Oriente y Occidente, que produjo la mutua separación y excomunión entre el máximo jerarca de la Iglesia católica y los líderes eclesiásticos de la Iglesia Ortodoxa. Uno de las cuestiones que motivaron la separación fue la disputa iconoclasta, que señaló a toda obra que representara a un santo o a Dios como una señal de idolatría. 
 – ¿Los pintores crearon todas las obras que incluyen su firma? 
  No,  en muchas ocasiones –principalmente en la pintura renacentista– los pintores más reconocidos eran dueños de talleres en los cuales habían trabajadores y aprendices que producían cientos de cuadros; en muchas ocasiones, estas obras eran pintadas en su totalidad por los trabajadores, al final el pintor simplemente colocaba su firma, como si se tratara de una marca. 
 – ¿Qué le sucedió a los pies de Jesús en “La Última Cena” de Da Vinci? 
  La obra originalmente tenía los pies del santo, pero en 1652, durante la instalación de una puerta en el refectorio donde la pintura está, los constructores le cortaron la parte inferior central del mural. 
 – ¿Qué es “La batalla de Anghiari”? 
  Fue una pintura al fresco de Leonardo da Vinci que desapareció en 1563. Fue pintada en un muro del Salón de los Quinientos del Palazzo Vecchio de Florencia. Por fortuna se conocen los dibujos del pintor italiano, a partir de estos Peter Paul Rubens, en 1603, creó la réplica del cuadro que ha llegado hasta nosotros. 
 – ¿Cuántas pinturas se conocen de Da Vinci? 
  A pesar de su gran influencia en la historia del arte, sólo han llegado a nosotros 15 pinturas. Esto en parte a la obsesión de Leonardo por experimentar con nuevas técnicas y su interés en otras ramas del arte y la ciencia. 
 – ¿Cuáles fueron los modelos de Miguel Angel? 
  Miguel Ángel para todas sus obras utilizó modelos del sexo masculino, por eso en algunas ocasiones las mujeres parecen tener musculatura de hombre. 
 – ¿Qué hay detrás de “La Madonna” de Rafael y los seis dedos del Papa? 
  En este cuadro el pintor italiano plasmó al papa Sixto IV con seis dedos debido a que la tradición asociaba a esta anomalía con un sexto sentido y una capacidad para interpretar sueños proféticos. 
 – ¿Quién pintó con cadáveres humanos? 
  De hecho, en el Renacimiento, pintores como Leonardo da Vinci o Miguel Ángel buscaban plasmar con la mayor exactitud posible los cuerpos humanos, pero fue Theodore Gericault, un pintor francés que se considera prototipo de artista romántico, quien en su obra “Anatomical Pieces” utilizó restos humanos para su expresar su arte. 
 – ¿Qué pintor  fue fundamental para el descubrimiento de nuevas especies? 
  Entre 1784 y 1802, John James Audubon pintó 435 especies de aves a lo largo de su vida. Nació en Santo Domingo y se mudó a Estados Unidos en 1802, ahí se enamoró de las aves de la región y dedicó su vida a observar y pintar a estas criaturas. También Alberto Durero se interesó en representar especies que eran desconocidas en su época, como ballenas o rinocerontes. 
 – ¿Quién es Hokusai? 
  Katsushika Hokusai fue un pintor y grabador japonés que formó parte de la escuela conocida como “pinturas del mundo flotante”. Su cuadro “La gran ola de Kanagawa” es una de las obras orientales más famosas en Occidente. 
 – ¿De dónde viene el término “Fauvismo”? 
   El 18 de octubre de 1905, Matisse y otros pintores como Road, Auburn y Bertrand se presentaron en el Salón de exposiciones de otoño en París: pinceladas audaces y colores intensos. El crítico Louis Vauxcelles entró en la sala de exposiciones y exclamó: “Parece feroz y terrible”. Al centro del salón se encontraba un busto de mujer al estilo clásico de Donatello y dijo:”Mira, Donatello está rodeado por un grupo de animales salvajes”. Así se nombró a este grupo de pintores, descalificados por hacer una revolución en el arte: fauvismo=bestia. 
 – ¿Quiénes fueron los padres de Paul Cezanne? 
  Según los registros policiacos,  Auguste Cézanne y una sirvienta tuvieron una fugaz relación que dio vida al pintor francés el octubre 2 de 1906. A pesar de esta cuestión, Cézanne es uno de los pintores más importantes del Impresionismo; su arte es una exploración que utilizó planos de color para transmitir un mundo de reflexión, arte y diversión. E l día de su muerte se encontraba rodeado de pinturas sin vender y con facturas pendientes de pago. 
 – ¿Qué ciudad está plasmada en “La noche estrellada” de van Gogh ? 
  Es Saint-Rémy-de-Provence, y su autor pintó el cuadro desde su ventana en el hospital psiquiátrico de Saint-Rémy. Actualmente este recinto tiene un ala que se llama como el pintor neerlandés. 
 – ¿Qué opinaba el padre de Monet de su arte? 
  En una ocasión su padre le aseguró que estaba decepcionado de su arte ya que él quería que fuera un tendero en una tienda de abarrotes. 
 – ¿Cuáles son las características de las “Latas de sopa Campbell” de Andy Warhol? 
  Esta obra consiste en 32 cuadros que representan todas las variedades de sopa que Campbell ofrecía en esa época. El artista no dejó un orden con el cual acomodarlos, por lo que el museo decidió colocarlos conforme a la fecha en que las sopas salieron al mercado. 
 – ¿Curadores se han equivocado al colocar alguna pintura? 
  En 1962, la pintura “Le Bateau” de Henri Matisse fue colgada al revés en el Museo de Arte Moderno de Nueva York, pasaron 46 días para que alguien se diera cuenta. 
 – ¿Qué artista hizo un holograma de Alice Cooper? 
  En 1973, Salvador Dalí y Cooper se reunieron por primera vez, el pintor español le dio un yeso de su cerebro coronado por una corona de chocolate, a continuación le pidió al cantante que posara para un modelo. El resultado de esta reunión fue un holograma cubierto de diamantes. 
 – ¿Qué importantes obras han sufrido de vandalismo? 
  Son numerosas las obras que en algún momento fueron atacadas, entre las más importantes están: el “David” y “La Piedad” de Miguel Ángel,  la “Venus del espejo” de Diego de Velázquez, el “Iván el Terrible y su hijo” de Iliá Repin, y “La ronda de la noche” de Rembrandt, entre muchas otras más. 
 – ¿Cómo murió Gaudí? 
  Uno de los mayores arquitectos de nuestra era en varias ocasiones fue confundido por un mendigo por su falta de interés al vestirse. Este fue un factor determinante para que en 1926 muriera atropellado por un tranvía. 
 – ¿Qué le sucedió a “El hombre controlador del universo” de Diego Rivera? 
  El original fue pintado en 1933 en el Centro Rockefeller de Nueva York, cuando la sociedad se enteró que la obra incluía a Lenin, comenzaron a surgir voces que rechazaron la obra. Para evitar más problemas, la familia Rockefeller decidió destruirlo. El año siguiente Rivera realizó una réplica de su mural en el Palacio de Bellas Artes de la Ciudad de México. 
 – ¿Qué pintor intentó matar a Trotsky? 
  El 24 de mayo de 1940, David Alfaro Siqueiros, comandado por su cuñado Leopoldo Arenal Bastar, ingresó a la casa de Trotsky para asesinarlo. Se realizaron más de cien disparos (algunos todavía son visibles), pero los guardaespaldas del antiguo líder soviético repelieron el ataque. Debido a este hecho, Siqueiros tuvo que exiliarse en Chile. 
 – ¿Quién fue Marcel Duchamp? 
  Fue un artista contemporáneo que creó arte con objetos de la vida diaria. Su obra más conocida y controversial se llama “Fuente”, que solamente es un urinal expuesto. Cuando llegó el momento de presentar su creación en una muestra de arte, la junta de la exposición tuvo un amplio debate. Finalmente decidieron ocultar la pieza de la vista, presumiblemente en el baño. 
 – ¿Qué le sucedió a los Budas de Bamiyán? 
  En el 2001, el gobierno islamista talibán aseguró que estas enormes estructuras era ídolos y las devastó con dinamita y disparos de tanques. Una buena noticia surgió de esta desgracia, ya que gracias a esta acción arqueólogos encontraron una serie de pinturas distribuidas  en una serie de cuevas, presumiblemente tienen mil años de antigüedad. 
 El arte ha sido un elemento central en la historia de la humanidad, generalmente es un reflejo de su tiempo aunque en ocasiones va más allá de su realidad y e s sujeto a una serie de persecuciones . 
 *** Te puede interesar: 
 Plagio u homenaje. Pintores que se inspiraron en otros para su obra de arte 
 8 pinturas que tienen grandes historias detrás