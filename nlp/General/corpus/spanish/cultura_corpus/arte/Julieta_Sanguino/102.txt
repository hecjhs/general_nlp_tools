El safari parisino de Julien Nonnon

 En la actualidad, cada vez más piezas artísticas se vuelven momentáneas, efímeras e instántaneas. Con más regularidad ellas desaparecen y dejan en el espectador tan sólo un recuerdo. Aquello que el día de mañana no volverá a existir, a menos que el artista así lo quiera. Walter Benjamin decía que el arte lo era porque conformaba un aura llena de significados, aquellas que sólo se daban a través de un tiempo y espacio determinado. Una aparición irrepetible que en su época se perdía con la reproducibilidad de las obras de arte. Si su aura se perdía, la obra se transformaba en un producto cultural banal. Sus afirmaciones ya eran un hecho antes de la Segunda Guerra Mundial y si viera a lo que nos enfrentamos hoy, se iría para atrás, pues el mundo se ha transformado en un lugar etéreo y gaseoso, sobre todo en el mundo del arte. 
  Sin embargo, la evolución artística ha permitido expresiones ilimitadas con carácter natural y cotidiano que nos parecen muchas veces que no encajan en el concepto de arte, sin considerar que éste avanza y evoluciona a la par de sus transformaciones. Uno de los artes más criticados es el Street art , pues sus realizadores son los más anónimos del mundo del arte; simplemente impregnan sus piezas en las calles y dejan que un público infinito y etéreo lo consuma, a veces por placer y a veces simplemente porque se encuentran en ese lugar. 
  El artista Julien Nonnon, fundador del estudio L3, lleva su arte a los límites del reconocimiento y el desconocimiento, de lo efímero y perdurable, entre lo tangible y lo etéreo con su proyecto Safari Parisino . Nonnon realiza instalaciones con mappings en las calles nocturnas de París. Su trabajo busca mezclar los elementos arquitectónicos y urbanos que ya están en su entorno y sus instalaciones de mapeo. Aquello que proyecta son animales de la fauna salvaje  pero con elegantes trajes, tales como un guepardo con chamarra o un águila con traje y corbata. Cada uno lleva como nombre aquella calle en la que se proyecta y solamente pueden ser vistos de noche. 
  El artista instala su equipo: una  tablet , su cámara fotográfica y un proyector. Decide qué punto de París invadirá esa noche y retrata hermosas fotografías que pueden ser vistas en su página de Internet.   Gracias a su deseo de ver su obra plasmada en las calles de París, ahora alrededor de todo el mundo podemos ver sus piezas artísticas. Con su obra, lo absurdo y salvaje se vuelve parte de la vida del ser humano y al mismo tiempo parece totalmente racional y urbano. 
  
http://culturacolectiva.com/wp-content/uploads/2015/09/lemur-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/bufalo-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/hiena-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/elephant-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Babouin-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/Coq-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/el-perezoso-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/el-cocodrilo-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/pastor-aleman-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/guepardo-julien-nonnon.jpg
http://culturacolectiva.com/wp-content/uploads/2015/09/koala-julien-nonnon.jpg
 *** 
 Te puede interesar: Grandes artistas de street art en la ciudad de México