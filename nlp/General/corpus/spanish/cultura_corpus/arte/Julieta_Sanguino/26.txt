Las 47 obras más importantes en la historia del arte

  Diferentes periodos han transcurrido en el mundo. En cada uno de ellos algo cambió. Nos hicimos más sabios, menos rigurosos, queríamos progresar a toda costa e intentábamos cientos de cosas para hacerlo. Las ciencias llegaron a revolucionar el pensamiento, la guerra destruyó el mundo y el ánimo del ser humano por seguir adelante intentó reconstruir el mundo tan rápidamente como podían.       Las pinturas de las cuevas de Chauvet    –   “Laocoonte y sus hijos”
 Agesandro, Polidoro y Atenodoro de Rodas    –   “El Retrato de Giovanni Arnolfini y su esposa” (1434)  Jan Van Eyck      






En cada punto de referencia algo destacaba. Un sentimiento indescriptible por querer expresar, permanecer y significar marcaba a las personas. Dice Arthur Danto que el punto filosófico básico es que el arte es siempre más que las pocas condiciones necesarias requeridas para el arte. Una idea nos parece desconocida, pero alguien se atreve hace lo que nadie creía posible y es ahí cuando nace una obra.





“La Virgen de las Rocas” (1486)



  



 Leonardo da Vinci







*Leonardo da Vinci, uno de los pintores e inventores más grandes de todos los tiempos, tiene obras que podrían hablar de todo su trabajo artístico en pocas pinceladas. Ve aquí las mejores obras de Leonardo da Vinci.


  –  “Alegoría de la primavera” (1478) Sandro Botticelli     –  “La pietà” (1499) Miguel Ángel Buonarrotti    –   



“El jardín de las delicias” (1503–1515)
El Bosco 


–







“El caballero, la muerte y el diablo” (1513)
Alberto Durero


–














“Venus de Urbino” (1538) 
Tiziano


–






“El entierro del conde de Orgaz” (1586–1588)
El Greco















       Cada una depende del contexto, pero nadie puede definir el arte. Ver algo como arte requiere una calidad invisible al ojo: un poco de historia, un poco de teoría según el crítico de arte ya mencionado y sólo aquellos que sepan esto podrán dejar de juzgar como absurdas las obras que otros dicen que cambiaron al mundo. Ya no sentenciarán a cada oportunidad “eso lo pude haber hecho yo”, renunciarán al prejuicio y al arte básico que todos conocen para adentrarse en corrientes, pintores y épocas que marcaron nuestra historia.       

“La decapitación de San Juan Bautista” (1608)

Michelangelo da Caravaggio








Caravaggio, el magnífico pintor de las sombras, tiene una técnica tan realista, tan pesimista y tan lúgubre que te enamorarás de él. Murió de manera trágica antes de obtener el perdón igual que otros pintores, ve aquí sus historias.





–

“La ronda de noche” (1642)
Rembrandt van Rijn


–

“Las Meninas” (1656)
Diego Velázquez


–





“La muerte de Marat” (1793)
Jacques-Louis David

















–

“Aníbal cruzando los Alpes” (Hacia 1810-1812)
Joseph Mallord William Turner


–





“El caminante sobre el mar de nubes” (1818)
Caspar David Friedrich











–







“Saturno devorando a su hijo” (1823) 
Francisco de Goya










–






“El origen del mundo” (1866)









Gustave Courbet









Calificar las obras de arte más importantes es complicado: claroscuro, ruptura, antigüedad, técnica, innovación, estética, paradigma… tal vez cada una deba juzgarse en su época, pero ya que eso nos es imposible, este pequeño compendio intenta ser lo más parcial posible para que seamos capaces de ver la evolución de la técnica y la importancia de acabar con el arte. 




“Impresión, sol naciente” (1872)
Claude Monet














–
“Le Dejeuner des Canotiers” (1881)
Pierre Auguste Renoir



–






“Los comedores de patatas” (1885)
 Vincent van Gogh

















–

“Tarde de domingo en la isla de la Grande Jatte” (1886)
Georges Seurat














–

“El grito” (1893)


 Edvard Munch


–













“La edad madura” (1902)
Camille Claudel


–

“El friso de Beethoven” (1902)
Gustave Klimt


–

“La montaña Sainte-Victoire” (1905)
Paul  Cézanne


–

“Las señoritas de Avignon” (1907)
Pablo Picasso



–





“The Dessert: Harmony in Red” (1908)
 Henri Matisse


–







“Formas únicas de continuidad en el espacio” (1913)
Umberto Boccioni



–

“Cuadrado negro sobre fondo blanco” (1913)
Kazimir Malévich 
















–
“Composición VII” (1913)
Wassily Kandinsky 






–







“Hombre con guitarra” (1914)
Georges Braque




Abstracción, Renacimiento, barroco, claroscuro, Surrealismo, Dadaísmo, pop, arte griego… todo cambia, se transforma y la evolución del arte es sin duda un vaivén muy difícil de calificar. Algunas de estas piezas trazaron un nuevo rumbo, como las técnicas innovadoras casi fotográficas de Caravaggio antes de que se inventara la cámara o el paso a la abstracción por Kandinsky, aquel que muchos aseguran fue el primer pintor moderno en hacer una obra no figurativa en su totalidad.




“La fuente” (1917) 
Marcel Duchamp


–

“Corte con cuchillo de cocina a través de la barriga cervecera de la República de Weimar” (1919)
Hannah Höch


–

“La cercana pubertad o las Pléyades” (1921)
Max Ernst


–






“La traición de las imágenes” (1929)
René Magritte











–











“La persistencia de la memoria” (1931)
 Salvador Dalí



–

“Nighthawks” (1942)
Edward Hopper 



–
















“Autumn Rhythm (Number 30)” (1950)
Jackson Pollock



–

“Estudio de Inocencio X, Estudio según el retrato del Papa Inocencio X de Velázquez” (1953)
Francis Bacon 



–

“Pero ¿qué es lo que hace a los hogares de hoy día tan diferentes, tan atractivos?” (1956)
Richard Hamilton



–

“Brillo Box” (1964) 
Andy Warhol



–







“La imposibilidad física de la muerte en la mente de algo vivo” (1991)
Damien Hirst



–












“Au naturel” (1994)
Sarah Lucas



–

“Petite maman” (1995)
Louise Bourgeois



–











“Balloon dog” (1998)
Jeff Koons



–

“The Rose II” (2008)
Cy Twombly




Nunca conoces lo suficiente del arte, si crees que lo sabes todo, intenta con estas 25 preguntas que todo conocedor de arte debería poder responder… si lo lograste, es hora de que conozcas estas 51 pinturas magníficas que no son tan básicas como los snobs creen.