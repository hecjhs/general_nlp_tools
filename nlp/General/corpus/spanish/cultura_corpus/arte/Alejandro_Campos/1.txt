Únete al equipo de Cultura Colectiva – Vacantes de articulistas y prácticas profesionales

 Como plataforma que apoya al talento artístico emergente estamos buscando personas que se integren a nuestro equipo. Entre las cosas que hacemos están: la creación de contenidos para la página web, la comunicación constante con artistas, la creación de estrategias de difusión digital, desarrollo de proyectos artísticos y la producción de eventos, entre otras cosas. 
  
 Actualmente tenemos vacantes disponibles en tres áreas, tiempo completo y en la Ciudad de México. 
     
 También tenemos espacios para liberar prácticas profesionales y servicio social en la siguiente área: