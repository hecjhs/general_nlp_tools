CENTRO en Zona MACO

 Zona MACO es una de las ferias de arte contemporáneo más importantes a nivel Latinoamérica, fundada en 2002 por Zélika García. Hoy, 13 años después, se ha consolidado como una de las plataformas más notables para la venta, difusión y exposición de arte contemporáneo en toda la región. Este año, Zona MACO se celebrará del 4 al 8 de febrero en el CENTRO BANAMEX. 
 Como parte de los invitados a Zona MACO, CENTRO tendrá un stand donde se llevará a cabo la exposición y venta de piezas de diseño realizadas por alumnos y egresados. 
 El Stand podrás encontrarlo bajo el código ZMD-206, todo ello dentro del Pabellón de diseño de Zona MACO, Centro Banamex, Sala D. En los siguientes horarios: 
 Miércoles 04: 16 a 21 horas Jueves 05, viernes 06 y sábado 07: 12 a 21 horas Domingo 08: 12 a 20 horas 
 Recuerda además, que CENTRO ofrece un precio preferencial de entrada a todos aquellos que tengan credencial CENTRO 
 ¡No olvides visitar tu stand!