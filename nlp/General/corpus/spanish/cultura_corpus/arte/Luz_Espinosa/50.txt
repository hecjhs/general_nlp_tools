Baptistde Debombourg y su ola de consumismo

 En la demonología medieval, un Leviatán es un demonio acuático que intenta poseer a las personas, siendo éstas difíciles de exorcizar. Algunos eruditos bíblicos consideran que Leviatán representa las fuerzas preexistentes del caos. Cuando las creaciones del hombre se superan cada vez en menos tiempo, Baptistde Debombourg presentó Flow , una pieza que critica el consumo global, el que plantea como un juego de conceptos en el que el consumismo ha tomado forma en un monstruo apocalíptico y amenaza con devorar a la humanidad. 
  Cientos de parabrisas rotos forman una ola gigante que amenaza con aplastar el futuro de una humanidad, la que ante la falta de sueños intenta llenar vacíos con objetos materiales, lo que resulta en la idea que llevó al artista francés a explicar que el demonio ha despertado, nuevamente, el caos, y cuando el desarrollo de las especies mate a las especies, el mundo sufrirá un inundación de locura colectiva. De ahí el caos que provoca los vidrios rotos. 
  Baptistde Debombourg toma la ola como el caos de consumo global. Una ola que crece ante las nuevas “necesidades de supervivencia”; ésta se vuelve tan incontrolable que amenaza a cada momento con volcar y ahogar a una humanidad rodeada de miedo y cosas materiales. 
  “Nadamos en un mar de parabrisas, remamos entre el vidrio, sofocándonos como si debajo de las olas nos devoraran nuestras propias creaciones”.

http://culturacolectiva.com/wp-content/uploads/2013/06/Flow031.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/Flow05.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/Flow04.jpg