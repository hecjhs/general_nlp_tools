Iris Scott: un río de cambiantes tonos

 El color del mundo es mayor que el sentimiento del hombre. En pequeños trazos multicolores queda atrapada la esencia de la imagen, la luz, el perfume, la línea, la forma y la expresión. El color es la fuente eterna de poesía porque siempre hará sentir como siente todo pintor. 
   Con una licenciatura en Bellas Artes por la Universidad del Estado de Washington, Iris Scott exploró el mundo de la pintura hasta encontrar las herramientas que ayudarían a plasmar en color las palabras que no halla en frases y fue en la pintura con los dedos donde la encontró. Guantes quirúrgicos y la colocación de la pintura al óleo directamente sobre los dedos dan vida a cuadros de estilo post-impresionista y vibrante que se hace eco de los maestros del siglo XX. Pinceladas que  expresan, a partir del color, el deseo de no querer que la realidad sea siempre así.  Su composición y estilo ha atraído a coleccionistas de todo el mundo pues despierta emociones que generan las relaciones de color en su obra. 
  Iris encuentra en la pintura fuerza personal y vinculación con el espacio y seres los que lo integran. Escenas que se esconden en todas partes y que Iris encuentra mientras camina por la acera o descansa en una sala de estar, es en esos espacios donde se dan las relaciones entre el color y las formas interesantes. Así como para André Derain los colores son cartuchos de dinamita, para Iris h acer un cuadro es como ir a la guerra, pero la batalla es emocionante.  Por suerte, esta batalla es cada vez más fácil de ganar, su munición es cada vez más avanzada y su resistencia mejora con la práctica. Al contrario de la guerra, el color prolonga la vida hacia un río de cambiantes tonos.

http://culturacolectiva.com/wp-content/uploads/2013/09/iris-scott-1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/omagenes-impresionistas.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/bailarinas.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/iris-scott.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/iris-scott-finger-painting-sea.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/Iris-scott.jpeg
http://culturacolectiva.com/wp-content/uploads/2013/09/images.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/iris-scott1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/caballos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/arrabal.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/vias.jpg