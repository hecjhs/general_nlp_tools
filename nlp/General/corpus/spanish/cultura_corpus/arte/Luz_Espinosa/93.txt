Los retratos bordados de Daniel Kornrumpf

 Se dice que algunas personas van por la vida mostrando distintas caras, éstas pueden ser el rostro de la conveniencia, la mentira, la hipocresía o el desencanto. Sin embargo, todos estos rostros siguen manteniendo la anatomía de la persona que los adopta: de piel humana que disimula las malas actitudes. Si tuviéramos la posibilidad de cambiar de cara en distintas situaciones de la vida, muy pocas veces dudaríamos en no hacerlo, pero no todos tenemos la oportunidad de crear diferentes rostros, Daniel Kornrumpf, sí.            
Nacido en Filadelfia, este profesor de pintura realiza impresionantes retratos bordados a mano utilizando solamente hilo y aguja. Kornrumpf trabaja especialmente la pintura impresionista como medio de expresión artística, sin embargo, Kornrumpf decidió experimentar el arte del bordado para formar rostros que parecen hechos a pinceladas.
 

 
Actualmente Daniel Kornrumpf radica en Boston donde continúa perfeccionando esta técnica puramente artesanal, que consiste en bordar en hilo de lino sobre tela logrando cuadros “pintados” con hebras de este material. 
 

 
Los retratos de Kornrumpf se caracterizan por el grado de realismo que alcanzan. En cada costura se aprecian los detalles de la persona: el movimiento natural del cabello, la expresión de los ojos, la forma de los labios o las particularidades de los accesorios.
 

 
Quizá la característica más impresionante del trabajo del artista es el tamaño de sus obras, pareciera que las piezas son retratos de grandes dimensiones, pero en realidad son pequeñas reproducciones sobre un gran trozo de tela lisa, lo que hace aún más extraordinaria la labor de Kornrumpf.
 

 
La obra de Kornrumpf ha sido expuesta principalmente en Estados Unidos, como en la Asociación de Arte de Cambridge y en la Pennsylvania Academy of the Fine Arts. 
 

 
“El hecho mismo de la alusión al retrato de un ser humano individual, que en realidad existe fuera de los retratos, define la función de la obra de arte en el mundo y constituye la causa de su entrada en vigor. Esta relación esencial entre el retrato y su objeto de representación se refleja directamente en la dimensión social de la vida humana como un campo de acción entre las personas, con su propio repertorio de señales y mensajes” -Richard Brilliant
        
http://culturacolectiva.com/wp-content/uploads/2012/10/los111.png http://culturacolectiva.com/wp-content/uploads/2012/10/los10.png http://culturacolectiva.com/wp-content/uploads/2012/10/los9.png http://culturacolectiva.com/wp-content/uploads/2012/10/los8.jpeg http://culturacolectiva.com/wp-content/uploads/2012/10/los7.jpeg