Lo casto y lo lascivo del arte en su máxima intimidad

 
 Nicholas Tomalchev,  con tan solo veintiún años, trae para nosotros una peculiar colección de acuarelas en las que combina lo inocente y asexual con la homosexualidad en su máxima intimidad. 
  Nació en Kiev, Ucrania, en 1993 y posteriormente se mudó a París (donde radica actualmente), para estudiar en el École Nationale Supérieure des Beaux-Arts. Se dio a conocer prácticamente a través de su página de Benhance y de la aplicación Tumblr, donde publicó gran parte de su trabajo. 
  Su trabajo como acuarelista ha desconcertado a millones, pues mezcla elementos naturales, puros y castos con escenas de la vida íntima de una pareja homosexual, como un hombre haciendo una felación al pico de un cisne. Sin embargo, Tomalchev nunca pierde la elegancia y muestra imágenes que podrían parecer perturbadoras de la manera más sutil posible: en colores pasteles, fondos simples y con una magnífica técnica que hace que sus personajes se vean reales. 
  
 Puedes visitar su obra completa en Benhance .