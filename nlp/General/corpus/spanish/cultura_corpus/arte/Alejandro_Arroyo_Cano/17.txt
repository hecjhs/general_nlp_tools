Pinturas con las que te identificarás si eres una mujer tímida e introvertida

 Las mujeres deben ser sumisas, educadas, delicadas, calladas , y sobre todo, hermosas. Si no se cumple con alguna de estas características, se dice, no pueden tener el título de “mujer de verdad”. Este pensamiento se creó con el surgimiento de la cultura occidental, y aunque se piense que es cosa del pasado, aún quedan en el aire ecos que impactan en la mentes femeninas. 
 Justamente del fondo de estas características, nace un problema que impide a las mujeres vivir de manera plena y sin prejuicios su vida. Porque para su espíritu femenino, que por dentro es un paisaje de tulipanes, rosas y girasoles, se le ha atacado de una manera absurda, haciéndole creer que lo importante está en el exterior. 
  
  
 No importa el fresco manantial que encierren debajo de sus pechos, lo que cuenta, a la vista de todos los demás, son las ilusiones, las imágenes efímeras que perciba la vista, sentido tan manipulado por un mundo de espejismos sinsentido. Entonces, la armadura más importante para el ser humano, es disminuida casi hasta hacerla desaparecer . 
 La confianza de la mujer ha sido golpeada tantas veces que ahora la vergüenza reina sus encantos.  Ya no tienen seguridad ante su aspecto físico y es común que quieran esconderse detrás de alguna máscara que oculte una falsa idea de imperfección. 
 Las pinturas de la artista Clare Elsaesser, reflexionan sobre querer esconder el rostro detrás de objetos para protegerse a sí mismas de acusaciones o señalamientos sobre su estética. 
  
  
 La artista, conociendo a la perfección el alma femenina, juega con los colores pasteles y objetos delicados para crear una armonía entre cuerpo y sentimiento. Las flores, figura de delicadeza y belleza por excelencia, son un recurso recurrente para cubrir los rostros de las pequeñas damas. 
 Lo único que se puede pensar es que debajo de una bella flor, se encuentra otra delicada forma que se oculta para no ser dañada por los fuertes vientos que llegan desde el pasado con un fétido olor a ignorancia y desdicha. 
   
  
 A pesar que cuenten con cuerpos perfectos, curvas salvajes que arrancan suspiros y pieles de duraznos, están obligadas a no dar la cara a todos los espectadores que más que elogiar la belleza, hieren con unos ojos como cuchillos y nunca se cansan de hacer daño. 
  
  
  
 Sus pinturas son de delicada estética, debido a que retrata cuerpos femeninos casi perfectos,  y aún así, tienen la necesidad de ocultar sus rostros. Ese contraste, entre lo que es perfecto y lo que se ve ensuciado por un sentimiento implantado, hace que su obra se cargue con un sentido crítico que 
 Existe gran segregación en la actualidad que va en paralelo con los problemas de racismo que se vive. Ahora no es suficiente atentar en contra de el color de la piel, sino contra el alma entera, que es prisionera por las habladurías del mundo. 
  
   
 El alma  es prisionera de una máscara de vergüenza busca liberarse por sentido natural. Por eso sale en momentos de oscuridad y se presenta en todo su esplendor. Son en esos pequeños momentos donde la mujer respira la libertad y toma aire para enfrentar lo que venga. Pero en algún punto de su vida, vuelve a ser víctima de un comentario sanguinario que la vuelve a retraer, que la obliga de nuevo a ocultarse detrás de una forma artificial. 
  
  
   
 Se pregunta Clare Elsaesser: “¿Qué es lo más suave y delicado en el mundo?” Su respuesta es: “la mujer”. Pero ella misma no se atreve a presentar los rostros de sus similares y busca la metáfora para denunciar lo que significa vivir en esta sociedad. 
   
  
 – Si te identificaste con estas tímidas mujeres, no te preocupes, sólo tienes que seguir unos sencillo hábitos para fortalecer tu autoestima . 
 *** 
 Te puede interesar: 
 La historia de las mujeres asesinadas por no ser dignas. La historia de un amor prohibido que quedó retratada en pintura.