Los aromas del café en papel

 Me tranquiliza saber que al despertar estará a mi lado esperándome.  Me encanta percibir su aroma todas las mañanas y que mi nariz despierte antes que mi vista. Amo su fuerte y tostado olor que me invita a abrir los ojos y comenzar el día. Acerco los labios para disfrutarlo más de cerca y captar cada uno de los aromas que de él emanan. Aromas que resultan notas melodiosas y que cual música recorren cada rincón de la habitación. Notas que en cuanto tocan mis labios se vuelven amargos, pero sólo hasta que logro saborearlos y notar que también hay algo de dulce en ellas. Amargura y dulzura conviven entre sí; se fusionan para darle vida a una taza vacía.  *  La artista Maria A. Aristidou ha utilizado el café como su herramienta principal para realizar sus ilustraciones. Inspirada por la cultura popular, ha recreado a sus personajes favoritos utilizando únicamente café.  Para llevar a cabo estos originales diseños, muele los granos de café y los mezcla con agua. Como si se tratara de acuarelas, utiliza el pincel para darle aroma a una hoja sin vida. 
  Napoleon, el primer gato que realizó con café. 
  Cuando vemos las imágenes nos damos cuenta que en realidad pareciera que se tratara de acuarelas.  Para darle tonalidades distintas utiliza diferentes granos y marcas de café, esto permite dotar a sus obras de sombras y colores más claros.   Lo que inició como un accidente, se ha convertido para Aristidou en parte de su día a día. Hoy la artista ha hecho una gran cantidad de pinturas con sólo café, algunas que han sido por encargo y otras para ella misma. R2D2 La famosa portada de la revista National Geographic de la niña afgana, fotografía tomada por Steve McCurry. 
  Davy Jones. 
  Bob Marley. 
  Minions. 
  “The Queen”. 
  Yoda. 
  Mario Bros. 
  Tyrion Lannister en Game of Thrones. 
  
  
  *** Si quieres conocer más de su trabajo no dejes de visitar su página en Facebook y seguirla en Instagram.