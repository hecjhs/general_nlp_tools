Eloy Morales, transición entre las técnicas

 De la fotografía a la pintura hay sólo un paso, o al menos, así lo deja ver el artista Eloy Morales. El español acerca a los detalles del rostro con una maestría fotográfica y hace una transición de técnicas entre pintura y grandes formatos. Son pinturas hiperrealistas de justificación fotográfica protagonizadas por su figura en la que destacan la fidelidad del rostro con lo brusco de los tonos en la pintura. 
  La realidad queda expresada en cada incrustación violenta de los tonos; su trabajo sirve de manifiesto acerca de la evidente relación entre la realidad, materializada en la fidelidad de una imagen con la fotografía, y la pintura. A Eloy, señala, le interesa indagar la realidad a través de la pintura con su propio código configurado entre la imagen fija y la técnica pictórica. 
  “Me estimula el tremendo poder de la imagen y sus inagotables posibilidades”. 
  Eloy desarrolla principalmente autorretratos en los que parece que la pintura va creciendo de poco a poco en las dimensiones que alcanzan sus lienzos. La pintura se desdobla en rasgos y gestos que se apegan a su propia representación mental, aunque, también, utiliza a amigos y familiares como modelos para sus obras. Eloy Morales vive y trabaja en Madrid. 
   Eloy Morales from Didi Menendez on Vimeo . eloymorales.jimdo.com 
 http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-135.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-243.png
http://culturacolectiva.com/wp-content/uploads/2013/09/eloy-morales-6.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Morales_004.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/Morales_005.jpg