Some pigeons are more equal than: Arte al vuelo

 Y cuando bajé la mirada, mis pasos se encontraron con las aves. Extendieron sus alas escalopadas, abiertas como abanicos de colores, se movían cuidadosas, íntimas, con el garbo que produce una nueva piel. Revoloteaban en mis tobillos y emprendían un vuelo bajo, eran puntos de colores en el aire, como bolas encendidas que tienen alas y son lanzadas de un cuento a una realidad urbana. Cuando bajé la mirada, mis pasos se encontraron con las aves, son palomas que ya no celan el azul del cielo.    Con motivo de la Bienal de Venecia 2012, el artista Julius Von Bismarck en colaboración con el fotógrafo suizo Charriere Julian, llevaron a cabo una peculiar muestra en la que las aves y el color fueron los protagonistas en las calles de Venecia.    El proyecto que se titula Some pigeons are more equal than (Algunas palomas son más iguales que otros)oscila entre el arte urbano y la instalación, cuyo resultado son unas impresionantes fotografías de palomas que cambiaron el gris de la ciudad por vivos colores resultado de un tratamiento al que fueron sometidas.  http://culturacolectiva.com/wp-content/uploads/2014/11/mon-13.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-14.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-16.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon21.jpg  Some pigeons are more equal than consistió en teñir 35 palomas locales de una plaza en Venecia, donde se consideran a las aves como una plaga. El objetivo del proyecto fue cambiar la percepción que los habitantes poseen frente a las que consideran “las ratas del cielo”. Para ello, Von Bismarck y Julian decidieron “colorear” a algunas aves mediante un dispositivo creado por ellos mismos, y al que ya habían puesto a prueba por primera vez en una azotea en Copenhague. El dispositivo es una especie de jaula en la que una vez dentro la paloma, éste despide de manera automática pintura en colores vibrantes que tiñe al ave sin causarle alguna intoxicación. 
http://culturacolectiva.com/wp-content/uploads/2014/11/mon31.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon11.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-11.jpg
    Una vez que las palomas fueron teñidas “con aerógrafo”, son liberadas para que convivan con el resto de su especie, lo que concluye en un espectáculo visual por el contraste entre el gris original de las aves y los fascinantes colores que hacen parecer al resto de las palomas como pelotas aladas brillantes. Según declaraciones de los artistas, lo que buscaban era dar a cada paloma una propia identidad para hacerlas más aceptables.  
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-10.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-8.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-9.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-12.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-15.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon-17.jpg
http://culturacolectiva.com/wp-content/uploads/2014/11/mon41.jpg
http://culturacolectiva.com/wp-content/uploads/2012/11/mon-7.png
http://culturacolectiva.com/wp-content/uploads/2012/11/mon5.png
http://culturacolectiva.com/wp-content/uploads/2012/11/mon6.png