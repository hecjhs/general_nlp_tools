Zaria Forman, la eternidad del cielo y el mar

 En una de sus breves y rebeldes líneas poéticas, el mexicano Mario Santiago Papasquiaro hacía su propia definición de la eternidad: “La eternidad es el cielo cogiendo con el mar”. El infrarrealista convocó en un poema la inmensidad del espacio celeste con el horizonte infinito del cuerpo de agua salada para soportar en un verso el no-tiempo. 
  Esta eternidad de los “gestos de la naturaleza” tiene que ver directamente con la inmensidad de los cuerpos que abrazan lo que podría ser el infinito. Esta sensación de los límites difusos del cielo y el mar debido a su grandeza es la que envolvió la mente de la artista Zaria Forman cuando aún era pequeña. La entonces niña hacía viajes con su familia alrededor de los paisajes más remotos del mundo, el registro de estas imágenes quedó en la mente de Zaria y, con el tiempo, maduró una técnica hiperrealista que le ha otorgado reconocimiento internacional. 
  Zaria desarrolló un sentido de apreciación de la inmensidad del cielo y el mar siempre cambiantes. Ya fuera en un estado de tranquilidad o alterado por el viento o la lluvia, el paisaje dejaba ver su ánimo frente a los ojos de la niña, quien recuperó la fidelidad de las imágenes fotográficas tomadas por su madre, en dibujos de gran detalle. 
  Como si se trataran de fotografías, los dibujos pastel de Zaria atestiguan con gran precisión los momentos y paisajes que presenció cuando niña. Una tormenta en las llanuras desérticas, las lluvias monzónicas del sur de la India o la luz fría del Ártico iluminando las aguas de Groelandia son algunos de los fenómenos presentes en sus dibujos que retan la valoración del espectador sobre si es una foto o una pintura. 
  Zaria tiene un excelente manejo de la luz y la composición, además de un gran conocimiento de los comportamientos naturales de la tierra y el paisaje que se refleja en la elaboración de sus dibujos en grandes dimensiones. Con su obra, la originaria de South Natick dice explorar los momentos de transición, turbulencia y tranquilidad presentes en un paisaje para ponerlos a la vista del espectador y confrontarlo, desde la pintura, con la poderosa fuerza de la naturaleza. La realización de su trabajo la lleva a una reflexión sobre lo pequeño que es el hombre frente a la inmensidad del cielo y el mar. 
  
http://culturacolectiva.com/wp-content/uploads/2014/01/agua.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/el-mar.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/hielo-agua.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/hielo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/hiperrealismo.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/lago.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/nieve.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/paisaje-agua.jpg
http://culturacolectiva.com/wp-content/uploads/2014/01/paisaje.jpg
  El trabajo de Zaria ha sido exhibido en gran parte de Norteamérica, principalmente en Nueva York, e incluye los paisajes de Las Maldivias, Groelandia, Israel y el Lago Thompson, por mencionar algunos. 
 www.zariaforman.com