Park Sunga, acuarelas de la cotidianidad

 Cuando se realiza un viaje, las fotografías sirven como un testimonio físico de ciudades, calles, ambientes y personas. La imagen fija es prueba y recuerdo de lo que fue, y se almacenan para la posteridad como instantáneas en un cajón de memorias. Esto no ocurre con Park Sunga, una artista coreana quien registra sus viajes en escenas de acuarela. 
  Sacre-Coeur, Montmartre, París Park Sunga es diseñadora gráfica e ilustradora y ve en la acuarela el recurso para recrear los momentos de sus viajes: construye con tinta y pincel edificios y pasajes de distintas ciudades alrededor del mundo, principalmente de Europa, el sudeste asiático y la India. 
  Oxford, Reino Unido Pero no sólo recrea las formas de la ciudad; la acuarela es el medio para contar historias protagonizadas por los habitantes de sus viajes. Su trabajo son episodios de la gran ciudad: su gente, sus colores, sus formas, sus edificios, sus instantes en la cotidianidad. 
  París, Francia Las acuarelas de Park parecen desvanecerse, desdibujarse en su propia tinta como si sólo ofrecieran una parte de la imagen y con ello permitir a los otros completar la escena con su propia visita. Park Sunga actualmente vive y trabaja en Busan, Corea del Sur. 
 http://culturacolectiva.com/wp-content/uploads/2013/09/park-4.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/park-2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/park-5.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/park-6.jpg
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-119.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-25.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-31.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-41.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-51.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-61.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-71.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-8.png
http://culturacolectiva.com/wp-content/uploads/2013/09/Imagen-9.png