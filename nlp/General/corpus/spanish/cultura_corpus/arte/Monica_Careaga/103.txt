Agnes Cecile, acuarelas del acontecer mundano

 Pintar como manifiesto, como discurso de aceptación artística y de legitimización de las emociones. Las acuarelas son el catalizador de un pensamiento creativo de sensaciones líquidas que adopta formas en rostros inconclusos, completados por asociación y contemplados por obligación. Así es el arte, nos hace mirar el objeto como un deber casi religioso. 
  La italiana Agnes Cecile utiliza la acuarela como una evidencia plástica de su acontecer mundano. A través de la técnica del dibujo en acuarela y recursos como el carbón, la tinta y el aceite, Silvia Pelissero ofrece una visión renovada del dibujo y sus formas intrínsecas para documentar una expresión del arte contemporáneo entre tonos vivos y trazos con soltura. 
  De formación autodidacta, Agnes se formó en el acontecer que podía registrarse en palabras, versos, notas y frases cortas que abren los caminos en su cabeza y materializa en dibujos de la expresión interna, alimentados por la imaginación, la situación y un impulso que se nutre de la necesidad de dejarse llevar por el pincel. 
  La técnica en la elaboración de las acuarelas de Agnes ve su justificación en la escuela de Pollock y su innovador drip painting , presente en la estética de las obras de la artista: pintura vertida sobre un lienzo, sin aparente intención, pero que otorga a la pieza una fuerza visual que contrasta con la delicadeza del dibujo y el protagonismo, principalmente, de figuras femeninas.    http://culturacolectiva.com/wp-content/uploads/2013/06/a2.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a4.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a5.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a6.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a8.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a9.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a11.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a12.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a13.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a15.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a16.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a17.jpg
http://culturacolectiva.com/wp-content/uploads/2013/06/a18.jpg  El vértigo en la acuarela se atestigua bajo la técnica del speed painting , con la que Agnes enfrenta destreza y calidad a la hora de pintar; pero ésta última no se ve sacrificada, es una entrega inmediata de una emoción que aguarda su salida en salpicaduras de acuarela.  agnes-cecile.cleanfolio.com