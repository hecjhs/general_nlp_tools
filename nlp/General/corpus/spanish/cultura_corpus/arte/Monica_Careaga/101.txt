Gabriel Moreno y la visión gráfica del amor

 Encontrarte a una mujer en la vida siempre modifica el destino: eres hijo, te conviertes en padre, se piensa en una familia y, sobretodo, se conoce el amor por definición. A Gabriel Moreno le sucedió lo mismo, conoció a una mujer y conceptualizó el amor; hizo una expresión gráfica del sentimiento. 
  Llegó a Madrid movido por el amor de una mujer, y una vez allí, reconoció su vocación en la ilustración y las curvas femeninas. Egresado de la licenciatura en Bellas Artes de la Universidad de Sevilla, la obra de Gabriel Moreno es un estudio de las formas mujeriles evocadas en sus pensamientos, los que libera en trazos de cadencia fina. 
   Su mujeres, protagonistas y musas, son de las que se nutre su trabajo, un catálogo de la feminidad dibujada sobre una estética vaporosa que se identifica por aparentes trazos volátiles que definen sus ilustraciones. 
  Con técnicas como el rotulador, la tinta y la acuarela, Moreno se mueve entre la ilustración, el grabado y la pintura, y su trabajo explora los universos femeninos con una disposición práctica de materializarlos. Reconoce que su inspiración viene de la observación hacia la figura femenina, pricipalmente del cuello y la boca, partes del cuerpo que puede ilustrar sin dificultad. 
  Su experiencia abarca distintos estudios de diseño y agencias de publicidad andaluzas, sin contar que su nombre se lee y se reconoce en prácticamente todas las grandes agencias madrileñas, lo que lo ha proyectado de manera internacional. En 2007 fue seleccionado entre los 20 nuevos talentos de la ilustración por la revista londinense   Computer Arts y desde entonces su carrera no ha dejado de ser una metamorfosis de la sensualidad femenina.   
http://culturacolectiva.com/wp-content/uploads/2013/06/Maria1.jpeg
http://culturacolectiva.com/wp-content/uploads/2013/06/g4.jpeg
http://culturacolectiva.com/wp-content/uploads/2013/06/Imagen-169.png
http://culturacolectiva.com/wp-content/uploads/2013/06/Imagen-216.png
http://culturacolectiva.com/wp-content/uploads/2013/06/Imagen-35.png
http://culturacolectiva.com/wp-content/uploads/2013/06/Imagen-41.png
http://culturacolectiva.com/wp-content/uploads/2013/06/Imagen-51.png
http://culturacolectiva.com/wp-content/uploads/2013/06/lushecarma800.jpeg Moreno ha colaborado con numerosas publicaciones nacionales e internacionales, como la editorial norteamericana Los Angeles Times Magazine , para la que elaboró la portada de alguna de sus ediciones. gabrielmoreno.com gabrielmorenogallery.com