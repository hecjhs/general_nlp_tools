Peihang Huang y su Cuento de Hadas Carnoso

 Peihang Huang es una joven artista taiwanesa que a través de la pintura deja ver la fuerte influencia que posee de la mezcla cultural chino-japonesa-norteamericana que existe en Taiwán.          Las pinturas de Peihang están inspiradas en la historia de la isla, pero al mismo tiempo reflejan su interés por la cultura occidental, en específico, por la llamada cultura pop de la que retoma el estilo para producir su arte.  

http://culturacolectiva.com/wp-content/uploads/2013/10/1303.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/2182.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/3140.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/4116.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/5115.jpg
     El trabajo de Peihang está basado en imágenes de cuerpos de personas semidesnudas, así como en la figura de la muñeca más famosa del mundo: Barbie. De igual forma involucra marionetas en sus obras, entre las que se perciben de manera distorsionada a personajes populares como Hello Kitty, Doraemon o Snoopy.          Mediante sus pinturas, Peihang retrata tópicos como el amor, la belleza, el cuerpo, la vida y la muerte. Incorpora diversas tonalidades en su obra, pero parte siempre de colores fríos que van desde el azul celeste hasta el magenta.          Uno de los trabajos más asombrosos de esta joven artista, es su serie titulada Cuento de Hadas Carnoso, donde transforma los exteriores de plástico liso de Barbie en pinturas que dejan ver la carne viva de la muñeca. “Peihang utiliza lienzos que destacan la naturaleza grotescamente hermosa y delicada de la carne humana a través de una combinación de paletas de colores únicos y trazos de pincel”.   
http://culturacolectiva.com/wp-content/uploads/2013/10/692.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/774.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/864.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/960.jpg