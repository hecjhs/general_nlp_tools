Heikki Leis: Reflexiones diarias en dibujos hiperrealistas

 Utilizamos los lápices y las plumas para apuntar un teléfono, hacer una anotación rápida, para escribir en la escuela, el trabajo o simplemente para respaldar lo que nos sucede día a día. Heikki Leis les da un uso más artístico: dibuja retratos hiperrealistas, donde el nivel de detalle que logra convierte a sus ilustraciones en fotografías.  Nacido en Estonia, Heikki Leis es un artista que pareciera hacer lo que todos alguna vez hemos hecho, hacer un dibujo con un lápiz o una pluma en la parte de atrás de un cuaderno, pero Heikki hace retratos de personas; de historias y experiencias que detalla con graffito y con tinta.  Los dibujos de Heikki sólo están hechos a lápiz y bolígrafo con los que logra imprimir un realismo a su obra que supera cualquier garabato hecho por una mano sin experiencia artística.  Desde el año 2000, Heikki trabaja como artista independiente, pero no sólo dibuja, también es fotógrafo y escultor. Con su obra se ha presentado en diversas exposiciones, donde presenta su trabajo en la fotografía, aunque su actividad principal siguen siendo retratos a mano.  Entre su vasta colección de trabajos, destaca la serie titulada Reflexiones diarias , un conjunto de dibujos elaborados a lápiz que representan escenas de la vida cotidiana “reflejadas a diario en nuestros espejos”  cada mañana. Es tan preciso su trabajo que se puede apreciar cada rasgo de la persona dibujada; sus imperfecciones y características que le devuelven el sentido humano a la obra. 
http://culturacolectiva.com/wp-content/uploads/2012/10/1258.jpeg http://culturacolectiva.com/wp-content/uploads/2012/10/2149.jpeg http://culturacolectiva.com/wp-content/uploads/2012/10/3108.jpeg http://culturacolectiva.com/wp-content/uploads/2012/10/492.jpeg http://culturacolectiva.com/wp-content/uploads/2012/10/595.jpeg “El tema principal de mis dibujos son personas. Los trato de representar tan real como son, con todas sus arrugas, el pelo y las formas, sin ningún tipo de adornos o glorificación. Es por eso que me gusta dibujar a las personas mayores, ya que sus rostros se han caracterizado por toda una vida de historias”. 
http://culturacolectiva.com/wp-content/uploads/2013/10/674.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/759.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/849.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/950.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1046.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/11104.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1259.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1334.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1430.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1523.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1622.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1720.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1818.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/1916.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/2011.jpg