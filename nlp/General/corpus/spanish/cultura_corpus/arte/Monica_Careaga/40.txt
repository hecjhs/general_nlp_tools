Anne Lindberg, luz en una habitación

 Como si rayos de luz quedaran atrapados en una habitación, la propuesta es una manifestación que circunda la instalación y la arquitectura, aunque la vista cree es un rayo en color rosa o verde que traspasa un muro para continuar su trayectoria. 
  Draw Pink y Andante Green son los hilos con los que Anne Lindberg crea fragmentos de color suspendidos en un espacio. La artista está interesada en los fenómenos ópticos y la exploración del espacio como las posibilidades para construir instalaciones en las que integra la luz, el color y el dinamismo que genera la tensión de los hilos en un conjunto. 
  El antecedente en su práctica de instalación es su trabajo gráfico, se puede intuir que Lindberg trasladó sus dibujos a un plano físico para convertirlos en una experiencia sensorial que involucra la percepción del espacio, la iluminación, el empleo del color (a los que reconoce como símbolos) y lo que la composición produce al ojo ajeno. La artista, señala, influye en el resurgimiento del dibujo en el arte contemporáneo. 
  Para Anne cada espacio guarda un sentido de color y una sensibilidad especial que acepta o se opone a la inclusión de los tonos que elige para montar sus piezas. 
   
 Los materiales que ocupa para su propuesta son hilo de algodón egipcio y grapas para sostener la obra sobre los muros. En una conceptualización de su obra, la artista la nombra repetidamente como arquitectura por la configuración y resignificación del lugar en la que se establece: “las piezas se consideran arquitectónicas en la medida en que son integrales para contextualizar el espacio”. 
   Lindberg manipula cientos de hilos de color y toma en cuenta los matices de la luz, la imagen de la trayectoria horizontal de la misma, la audiencia, el espacio y la materialidad para llevar a cabo su propuesta. La intención de su obra es generar respuestas viscerales a partir de la percepción que cada uno construye al presenciar la instalación. 
   Anne Lindberg tiene una licenciatura en la Universidad de Miami (1985) y un MFA de la Cranbrook Academy of Art (1988). Vive y trabaja entre Kansas City y Nueva York. 
 annelindberg.com  http://culturacolectiva.com/wp-content/uploads/2013/11/hilo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/andante-green.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/arte-con-hilo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/green-hilo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/hilos-de-color.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/instalacion-con-hilo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/lindberg-hilo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/11/lindberg-rosa.jpg