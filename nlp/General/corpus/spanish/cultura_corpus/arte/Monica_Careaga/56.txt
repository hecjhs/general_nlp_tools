Mateus Gapski, miradas urbanas de Polonia

 Chicas de cabellos azules se estampan en las paredes de Polonia. Los muros de Europa del Este son los espacios vacíos para que Mateus Gapski construya escenas de su país, sus mujeres y el folklor del lugar. 
  Mateus Gapski es un artista polaco con una fijación por la figura femenina, la que convierte en murales que asaltan su país entre acrílicos y pinturas de aerosol. Sus trazos exploran temas propios de esa región de Europa pero contados desde la elevación femenina: chicas que se hacen una con la naturaleza, saltan en medio del paisaje, casi siempre fantástico y fosforescente, e inundan los muros de las calles polacas. 
  
http://culturacolectiva.com/wp-content/uploads/2013/10/Mateus-Gapski.png
http://culturacolectiva.com/wp-content/uploads/2013/10/artista-urbano-Mateus-Gapski.png
http://culturacolectiva.com/wp-content/uploads/2013/10/arte-urbano.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Gapski.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/murales.png
http://culturacolectiva.com/wp-content/uploads/2013/10/murales-Mateus-Gapski.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/murales-Polonia.png
  De estética contemporánea, el trabajo de Gapski es un vistazo a las expresiones urbanas del viejo continente. Sus elevadas composiciones desvelan a personajes que conforman los habitantes de Polonia, o no; hombres y mujeres, animales, flores y formas se vuelven parte del horizonte urbano transformado por la mano de Gapski. 
 http://culturacolectiva.com/wp-content/uploads/2013/10/arte-urbano-Mateus-Gapski.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Mateus-Gapski-murales.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Mateus-Gapski.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/murales-Mateus-Gapski1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/muros-Mateus-Gapski.jpg