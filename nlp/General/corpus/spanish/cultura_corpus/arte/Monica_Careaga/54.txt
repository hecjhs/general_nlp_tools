Melvina Orozco, arrebatos de deseos colectivos

 En una observación de pestañeos constantes, la primera impresión sobre la obra de la potosina Melvina Orozco (San Luis Potosí, 1979) es la agitación de sus trazos, arrebatos pictóricos que evidencian la premura por comunicar retablos de su propia historia. 
   Poco se conoce de la obra de artistas oriundos de San Luis Potosí; Melvina es la referencia visual de las manifestaciones emergentes de esa región del país. Su trabajo se vuelve una oposición del rito artístico bajo el que se explora la sexualidad, la ausencia y la ansiedad, y los símbolos circundantes de cada uno, pues lleva de la mano y contagia la violencia de su intención, entendida desde la intensidad e, incluso, la profanación de esquemas reconocidos en el quehacer artístico. 
  Tres series atañen a estas líneas sobre la obra de Melvina: Ansia, Bloom y Cartas para Miguel . Cada una cuenta un discurso distinto pero se reconocen como terrenos limítrofes por la exposición del cuerpo humano en sus estados primitivos, a partir de su expresión y su desnudez. En Ansia , la potosina hace una oda al término desde la ambivalencia. Deja ver en los gestos de las figuras la inquietud y agitación de un momento pero, también, el deseo por lo que viene y “puede ser”. Visualmente, sus trazos intensos caen en el exceso de la emotividad, y arriesga con los colores y la seguridad expresionista de su técnica. 
    Bloom es una confrontación con el placer. La artista da sentido a la sexualidad en la expresión gráfica del erotismo y el deseo carnal; en una paráfrasis a Melvina “pone al descubierto su gestualidad más íntima y compulsiva”. 
  En una práctica, y plástica, insurrecta, la potosina afirma la polaridad de la sexualidad explícita, aunque no la abrevia o clausura: la indaga a través de composiciones eróticas y escenas que exploran el autoplacer. Son hombres y mujeres despojados de la moral que viste a una sociedad. 
  
http://culturacolectiva.com/wp-content/uploads/2013/10/Melvina-Orozco1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/artistas-potosinos.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/erotica.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/erotismo.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/pinturas-de-sexo1.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/sexo-explícito.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/sexo.jpg
  En Cartas para Miguel , Melvina continúa en la pintura la vida de su hermano fallecido. Es la voz con la que nombra a Miguel desde el lienzo y con la que se niega a enterrarlo en las profundidades del olvido. Rescata sus recuerdos, y los de su familia, y los vacía en pinturas para el consuelo. Recupera el tratamiento expresionista y libertario de su práctica, ahora con ánimos de evocar la presencia de un ser ausente. 
  La obra de Melvina Orozco está sujeta a la práctica artística académica, realizó estudios profesionales en la Escuela Estatal de Artes Plásticas de San Luis Potosí. Su obra bien puede reconocerse como contestataria frente al canon y la propia práctica, en ocasiones, inundada de un romanticismo para rodear lo difícil, temido o vergonzoso de una temática. Melvina demuestra que la libertad en el trazo puede contener un arrebato colectivo que despida los deseos contenidos. 
  
http://culturacolectiva.com/wp-content/uploads/2013/10/arte-de-SLP.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/cartas-para-Miguel.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Melvina-Orozco-artista.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Melvina-Orozco-SLP.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/Melvina-Orozco-SLP.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/obra-Melvina-Orozco.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/pintura-Melvina-Orozco.jpg
http://culturacolectiva.com/wp-content/uploads/2013/10/pinturas-Melvina-Orozco.jpg
  Ha participado en exposiciones colectivas como Dos océanos, Un mar, en Atenas, Grecia (2010); MACAY Anfitrión de la Plástica Nacional en Yucatán (2009), Colectiva regional de pintura del Centro-Occidente (2009), 65 Ostentan-Artoficio en el CENART, México, D.F, entre otras. De manera individual ha expuesto en la capital potosina los proyectos Cartas para Miguel, Galería Germán Gedovius; Más blanco, Teatro del IMSS; Carne, Museo Francisco Cossío; El gran sí, Centro Cultural Mariano Jiménez; Ansia, Galería Antonio Rocha Cordero, Bloom Centro de las Artes de San Luis Potosí, entre otras. Ha recibido distintos reconocimientos como el primer lugar en el Premio Ana Sokolow, SLP, y el Premio Estatal de la Juventud. Ha sido becaria del Fondo Estatal para la Cultura y las Artes, FECA.