Artista ciego crea sorprendentes gifs

 Algo que ha evolucionado en las redes sociales es la posibilidad de utilizar gifs , ya sea para ilustrar algún comentario, una situación, o simplemente para compartir con tus contactos. Muchos de esos gifs están creados anónimamente, por sujetos que invierten un poco de su tiempo para animar extractos de películas, series de televisión o hasta pinturas clásicas, renacentistas y medievales. Esto ocurre porque el ver una imagen en movimiento nos genera placer y más cuando son momentos específicos, que repetidos muchas veces en loop pueden sacarnos carcajadas frente a la pantalla. 
 Existen algunos artistas que nos han demostrado que a demás de ser divertidos, los gifs también pueden ser una propuesta estética.  
 En muchas ocasiones, cuando nos ocurre algo que nos incapacita para hacer alguna actividad, como lastimarnos, o por alguna enfermedad ya no poder realizar actividades, creemos que el mundo nos ha cerrado una puerta frente a nuestras narices. Para muchas personas, estas puertas cerradas dan la oportunidad de voltear hacia arriba y encontrar una ventana. 
 Esto es lo que ha venido haciendo George Redhawk, quien es débil visual y ha creado una serie de gifs, con los cuales quiere demostrar al mundo cómo es que él ve a través de la vista dañada. Redhawk no es completamente ciego, pero tiene un rango de visión muy limitado, lo que le permite, con dificultad, operar el programa que utiliza para modificar las fotos y crear los gifs. 
  
 El artista dice haberse dedicado a aprender el programa de modificación de fotos desde hace muchos años; notó que si jugaba en el software con la misma foto, podía generar una especie de secuencias que permitieran movilidad en algunas zonas de la captura. 
  
 Lo que se puede observar en su cuenta de Google+, es que tiene mucho contacto con fotógrafos del mundo , los cuales les otorgan el permiso de modificar las fotos que han tomado. Esto es un ejemplo de colaboración en el mundo del arte digital, pues Redhawk recibe fotos de otros artistas, y éste las anima, creando efectos impresionantes jamás antes vistos. 
  
 La calidad de las imágenes es muy alta, y la combinación de los encuadres de los fotógrafos le dan un toque distinto a la cultura del gif, ya que estamos acostumbrados a ver gifs de calidades muy bajas porque son creados a partir de videos sacados de la red. Por otro lado, George Redhawk, al crearlas con fotos profesionales, genera gifs de tamaños grandes y bien definidos, esto para poder apreciar la calidad del trabajo. 
  
 Algo que es de recalcarse en estas imágenes en movimiento, es que se distinguen perfectamente de un video, es decir, que explota perfectamente las posibilidades que da tener una imagen fija y darle movimiento, textura y realce; no sólo son gifs que implican una acción determinada, estos gifs respetan la esencia estética de una foto, con un agregado visual que tiene movimiento. 
  
  
  
  
  
    Este artista digital sube todas sus creaciones a su perfil de  Google+, así que si te interesa seguir su trabajo y publicaciones puedes acceder a este perfil. Te puede interesar: Gifs del fotógrafo mexicano detrás de esta juventud melancólica 
 *** 
 Referencia: Graphicarts-news