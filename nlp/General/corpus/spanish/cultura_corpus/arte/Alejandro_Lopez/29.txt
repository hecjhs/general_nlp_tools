10 películas inspiradas en el psicoanálisis de Sigmund Freud

 
  
 Aceptar que el deseo sexual está presente en toda relación humana es una máxima del psicoanálisis que hoy sigue causando polémica. El aporte de Freud en el campo de la psicología posiblemente sea más importante para el arte que para la ciencia, pues alrededor de su pensamiento han enraizado distintas formas de manifestación artística basadas en el automatismo psíquico, los viajes oníricos, la pulsión sexual y la interpretación de los sueños. Freud fue tentado un par de veces a escribir un guion de película, incluso tuvo una oferta multimillonaria por un estudio de Hollywood, pero siempre creyó que era imposible captar la esencia del psicoanálisis con una cámara de video. 
 La ambición por llevar las ideas de Freud a la pantalla grande fue llevada a cabo por nada menos que Jean-Paul Sartre en 1962, con el guion de “The Secret Passion”, dirigida por John Huston y en la que Montgomery Clift interpretó a Freud, pero las complicaciones no se hicieron esperar por las diferencias creativas entre Huston y Sartre, diferencias que en realidad se debían a la percepción que uno y otro tenían del psicoanálisis. Al final, Sartre culpó a Huston de utilizar la película como una excusa para un análisis propio y quitó su nombre de los créditos. Estas son 10 películas inspiradas en el psicoanálisis desarrollado por Freud: 
 – “Spellbound” (1945) -Alfred Hitchcock 
  
 Obra cumbre sobre los sueños y su significado, contó con la ayuda de Dalí para el montaje en que se describen los sueños del amnésico Gregory Peck. Después de la frase de Shakespeare, la leyenda advierte: “Nuestra historia trata del psicoanálisis, el método por medio del cual la ciencia moderna trata los problemas emocionales de la mente”. 
 – “A Clockwork Orange” (1971) – Stanley Kubrick 
  
 La obra de culto de Kubrick da para un sinfín de análisis psicológicos, desde el conductismo y la carencia de valores mostrados por Alex DeLarge y su banda, el tratamiento experimental al que es sometido DeLarge, hasta la influencia definitiva de Freud en distintas escenas, como la escultura de forma fálica con la que Alex golpea a la mujer de los gatos. 
 – “Black Swan” (2006) – Darren Aronofsky 
  
 Nina es una bailarina profesional de ballet con una técnica única y sueña con ser la estelar de la siguiente presentación, El Lago de los Cisnes, pero no es capaz de llenar el papel del cisne negro por su pasividad y aparente falta de pasión. La represión de los deseos de Nina se observa constantemente en la trama, en parte por su madre sobreprotectora y por el carácter que le ha forjado, incluso experimenta un sueño sexual con Lily, compañera de la compañía y tiene fuertes discusiones con su madre, que refleja el origen de su lado más conservador. 
 – “The Science of Sleep” (2006) – Michael Gondry 
  
 Película que es, en toda su trama, un viaje a través de la creatividad y los deseos de Stéphan, quien es engañado por su madre para volver a Francia donde le ha conseguido un trabajo que él detesta. Inmerso en la desgracia, Stéphan abandona su presente material y lleva a cabo sueños oníricos, descubriendo lo que hay en su subconsciente, que incluyen a Stephanie, vecina de la que está enamorado pero en quien solamente encuentra el rechazo. La trama se confunde entre la triste realidad y sus sueños, en donde Stéphan puede sentirse pleno aunque sólo sea en la imaginación. 
 – “Eternal Sunshine of the Spotless Mind” (2004) – Michael Gondry 
  
 La pérdida selectiva de los recuerdos que alberga la memoria siempre ha sido un tema aspiracional de la psicología conductista. Para el psicoanálisis, los sueños revelan deseos profundos que son reprimidos o bien, irrealizables. El drama de Gondry relata las profundas conexiones mentales entre las pulsiones y la realidad, alternando entre presente, pasado y el mundo de los recuerdos que parece tomar forma en la realidad por evitar que Joel concluya el tratamiento que borrará sus más preciados recuerdos. 
 – “Un chien andalou” – (1928) Luis Buñuel 
  
 Este cortometraje de apenas 17 minuto se interna en el mundo de los sueños y el surrealismo desde la primera escena: un ojo es diseccionado con una navaja, impidiéndole percibir la realidad, obligándolo a llevar a cabo el automatismo psíquico puro del cual deviene el verdadero pensamiento surreal. La obra de Freud en el trabajo de Buñuel y Dalí ha tenido tal impacto que ha sido reconocida ampliamente como una influencia del nivel de David Lynch, Hitchcock e incluso David Bowie. 
 – “Annie Hall” (1977) – Woody Allen 
  
 El psicoanálisis es un tema recurrente en la cinematografía de Woody Allen, tanto para crear una justificación en el carácter de sus personajes, como para analizar su propia obra. El comediante Alvy Singer encuentra distintos motivos en su pasado que influencian la forma en que actúa en el tiempo presente en que se desarrolla la trama. El mejor ejemplo está en el icónico inicio de la película, cuando se dirige a la cámara para contar sobre su infancia, su personalidad tímida y los traumas que desarrolló durante esa etapa. 
 – “La stanza del figlio” (2001) – Nanni Moretti 
  
 Giovanni, un psicoanalista obsesivo, sufre la pérdida de su hijo, quien muere ahogado en un accidente de buceo. Desde ese momento, su vida y la de su familia dan un giro inesperado y debe ser por fuerza Giovanni quien saque a todos de su trance a través del trabajo psicológico de esa afrenta. 
 – “Whirlpool” (1941) – Otto Preminger 
  
 Largometraje de terror psicológico y crimen, cuenta la historia de Richard Conte, un famoso psicoanalista que debe ayudar a su esposa, Ann Sutton, cuando ella comienza a creer que ha cometido una serie de crímenes sin darse cuenta al estar bajo los efectos hipnóticos de un asesino. Conte deberá resolver la situación a través del psicoanálisis y salvar a su esposa. 
 
 La mente humana siempre representará un misterio indescifrable, más allá de la relevancia del psicoanálisis como teoría científica y tratamiento alternativo, el papel que ha jugado en el cine, especialmente a mediados del siglo XX, es innegable. La sustitución de detectives y policías por psicoanalistas que trataban de resolver todos los misterios terrenales a partir de la mente humana, es una clara muestra de ello. De la misma forma, muchos artistas adheridos a movimientos surrealistas no tuvieron problema en aceptar la fuerte influencia de las teorías de Freud en su trabajo. Y tú, ¿cuál crees que es la mejor película inspirada en el psicoanálisis? 
 *** Te puede interesar: 
 Tristes lecciones de amor por Sartre y Freud Las etapas de la sexualidad según Freud