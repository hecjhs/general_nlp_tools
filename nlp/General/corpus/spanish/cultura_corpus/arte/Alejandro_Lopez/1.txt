Los nombres completos de los artistas más famosos de la historia

 
 En el mundo del arte no sólo es necesario conocer la obra y la vida de los más  grandes maestros históricos y contemporáneos. A veces no basta con comprender a cabalidad el sentimiento que influenció a un pintor para llevar al lienzo una paleta determinada, un trazo forzado o suave, o una visión pesimista o completamente rosa del mundo. Conocer el nombre completo de los artistas más famosos de la historia no sólo te sacará de dudas, también evitará tropiezos graves mientras charlas sobre arte. 
 Para que no resultes sorprendido cuando te encuentres con una obra de Freud donde plasma desnuda a Kate Moss y te preguntes cómo pudo el psicoanalista trascender al tiempo, la técnica y el espacio para pintar a la supermodelo de finales del siglo XX, o evites confundir el trabajo de Michelangelo Buonarroti con Michelangelo Merisi da Caravaggio, estos son veinte nombres  completos de grandes artistas que te hace falta conocer para descubrir un poco más de las curiosidades del mundo del arte: 
  
 1. En realidad, Diego Rivera es una abreviatura muy conveniente para evitar decir Diego María de la Concepción Juan Nepomuceno Estanislao de la Rivera y Barrientos Acosta y Rodríguez. 
 – 
  
 2. El monstruo malagueño se llamó Pablo Diego Francisco de Paula Nepomuceno Cipriano de la Santisíma Trinidad Ruiz y Picasso. Mejor llamarle simplemente Picasso. 
 – 
  
 3. Genio del surrealismo y excéntrico en todo, incluso en su nombre: Salvador Domingo Felipe Jacinto Dalí i Domènech. 
 – 
  
 4. Humanista, matemático, pintor, científico, arquitecto e ingeniero. Leonardo di ser Piero da Vinci lo fue todo. 
 – 
  
 5. El primer artista que trabajó en la abstracción, Vasili Vasílievich Kandinski. 
 – 
  
 6. El nombre artístico del representante del Pop Art, Andrew Warhola, era simplemente Andy Warhol. 
 – 
  
 7. Doménikos Theotokópoulos, mejor conocido como El Greco. 
 – 
  
 8. Hilaire-Germain-Edgar de Gas, autor de “Las bailarinas en azul” (1895). 
 – 
  
 9. El Bosco, en realidad era Hieronymus Bosch Jeroen Anthonizoon Van Aeker. 
 – 
  
 10. Si te intentas referir seriamente a William Turner, lo mejor es llamarle Joseph Mallord William Turner. 
 – 
 11. El pintor del Nacimiento de Venus (1484), Sandro Botticelli, en realidad se llamó Alessandro di Mariano di Vanni Filipepi. – 
  
 12. Uno de los grandes exponentes del Quattrocento italiano, mejor conocido por su último apellido, fue Tommaso di ser Giovanni di Mone Cassai Masaccio. – 
  
 13. El maestro del barroco, Rembrandt Harmenszoon van Rijn. 
 – 
  
 14. El nombre real del creador del chiaroscuro , es Michelangelo Merisi da Caravaggio. 
 – 
  
 15. Henri Émile Benoît Matisse, mejor conocido por su último apellido, fue un pintor adscrito al Fauvismo. – 
  
 16. Ferdinand-Victor-Eugène Delacroix. De la Cruz para los conocidos. – 
  
 17. La conocidísima Magdalena Carmen Frieda Kahlo Calderón también tuvo un nombre largo. – 
  
 18. Jacopo Comin es el nombre real del pintor conocido como Tintoretto. 
 – 
  
 19. El vanguardista ruso Lázar Márkovich Lisitski, abreviado como El Lissitski.–20. El pintor de “La Gran Odalisca” (1814), Jean-Auguste-Dominique Ingres.–Si quieres conocer más datos curiosos de la historia, debes conocer todo lo que tienes que saber sobre arte en 19 puntos para iniciar de una manera básica tu conocimiento sobre la pintura. ¿Te consideras un experto en la historia del arte? Intenta reconocer las 47 obras más importantes en este link.