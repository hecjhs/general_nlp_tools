10 tatuajes inspirados en Game of Thrones

 En 1991 se encontró una momia del siglo II d.C. con 57 tatuajes en la espalda  dentro de un glaciar de los Alpes austro-italianos. A la momia se le conoce como el Hombre de Hielo u Ötzi, y es el cadáver humano con piel más antiguo que se ha encontrado como prueba de que desde tiempos remotos el arte en la piel ha existido como símbolo de identidad y pertenencia –ya sea a un determinado grupo social o por pertenecer a sí mismo-. 
 Poco a poco los tatuajes han de dejado de ser estigmatizados como símbolos denigrantes, por lo tanto, quienes se dedican a este tipo de arte comienzan a ver su forma de vida como un negocio y sobre todo como una industria. Nuestra generación, en algunos años o décadas, se podrá reconocer por algo, porque llevamos uno o varios tatuajes en el cuerpo dejando atrás los tabúes sobre ellos para quienes nos precedan. 
 La tendencia por tatuarse referencias al cine y la televisión como el Señor de los Anillos o Dr. Who, o el fanatismo por Game of Thrones va en aumento. La obra creada por George R.R. Martin, además de joyería, ropa y hasta una cerveza, también tiene seguidores alrededor del mundo quienes plasman de forma permanente su amor por la popular serie de HBO. 
 Estos son algunos de los tatuajes más recurrentes e impresionantes inspirados en Game of Thrones que se han compartido gracias a las redes sociales: 
  
  
http://culturacolectiva.com/wp-content/uploads/2014/08/U5JsR.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/valar22.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/tree11.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/6f1b4512f84a5ca51bf2a6852bebcc2e.jpg
     http://culturacolectiva.com/wp-content/uploads/2014/08/arm_tattoo___game_of_thrones_by_piicareta-d77an6b.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/game-of-thrones-tattoos10.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/blogger-image-1044710198.jpg
http://culturacolectiva.com/wp-content/uploads/2014/08/980ed2d6616d1bd8f7eaa2cc66b3ca03.jpg