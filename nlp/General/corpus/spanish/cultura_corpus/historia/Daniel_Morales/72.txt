Biografías colectivas: Las generaciones

 Cuando la Modernidad terminó, en los años ochenta, el mundo estaba en crisis. La caída del muro de Berlín anunciaba el fin del bloque soviético. La globalización dio paso a nuevas formas de comunicación, de interacción y de mirar al otro, el otro quien pronto se convirtió en el reflejo de uno mismo. Una época en la que para poder seguir creciendo algunos miraron a quienes habían estado antes e intentaron entender cómo habían llegado hasta ahí. 
  William Strauss, escritor e ilustrador,  y Neil Howe, economista, fueron quienes iniciaron un estudio sobre las “biografías colectivas” de las generaciones estadounidenses. Gracias a 20 años de estudio lograron un método analítico que les permite examinar a esta sociedad, aunque gracias a su origen puede ser herramienta para la cultura occidental. 
 Una generación debe compartir  creencias y comportamientos que los distingan de la gente que nació en otro momento de la historia. Toda generación ha tenido grandes personajes, algunas generaciones llegan a brillar más que otras gracias a sus inventos o a las circunstancias que vivieron. Estas son algunas de las grandes generaciones y los personajes que trascendieron en su época: 
 1541 – 1565 Isabelinos 
 Los ingleses combatieron la guerra contra España y vivieron la edad de oro de la reina Isabel I. 
  William Shakespeare, Francis Bacon,  Miguel de Cervantes, Galileo Galilei y Doménikos Theotokópoulos “El Greco”, pertenecieron a este grupo generacional. 
 1742 – 1766 Republicanos 
 Ellos ayudaron a la expansión y llegaron a una época de crecimiento y desarrollo para los estadounidenses, su industria se debe gracias a ellos. Vivieron las revoluciones e independencias de su época. 
  A esta generación pertenecieron Thomas Jefferson, Luis XVIII, Francisco de Goya, Wolfgang Amadeus Mozart y José María Morelos y Pavón. 
  1843 – 1859 Progresista 
 Vieron grandes cambios generados a partir del psicoanálisis freudiano y el sentimentalismo victoriano. 
  Oscar Wilde, Sigmund Freud, Paul Gauguin, Vincent van Gogh, Salvador Díaz Morón y Venustiano Carranza vivieron esta generación. 
 1883 – 1900 La generación perdida 
 Bautizada así por Hemingway y Gertrude Stein, fue una generación de “chicos malos”. Su actitud deriva del desencantamiento de su juventud al ser arrastrados a la Primera Guerra Mundial. Novelistas, gangsters y las primeras estrellas de cine aparecen en esta generación. 
  Junto a ellos también aparecieron F. Scott Fitzgerald, Groucho Marx, Al Capone, Ernest Hemingway, Franz Kafka, René Magrite, Lazaro Cardenas y David Alfaro Siqueiros. 
 1961 -1981 X 
  La famosa generación X. Su vida sexual comenzó paralelamente con la pandemia del SIDA, fueron mucho más cautelosos que la generación pasada a la hora de escoger parejas sexuales, crecieron y se adaptaron a un mundo antes y después de Internet. 
 Barack Obama, Quentin Tarantino, Lance Armstrong, Björk y Charlie Sheen son algunos de sus contemporáneos.  1982 -1997 del nuevo milenio 
  Conocida como la “Generación Y”, el abuso y la educación infantil fueron de gran importancia cuando nacieron, por lo que fueron educados con métodos diferentes a los de la “vieja escuela”. Los ídolos dejan de ser para siempre y siempre surge uno para reemplazar a otro cuando éste pasa de moda. La “Generación Y” sigue en  desarrollo y aún no se mide el impacto que tendrá en la sociedad. 
 Scarlett Johansson, Lady Gaga, Amy Winehouse y  el príncipe William.