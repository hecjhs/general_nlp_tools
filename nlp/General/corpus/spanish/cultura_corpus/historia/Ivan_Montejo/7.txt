Imágenes inéditas que demuestran la cumbre y la derrota del movimiento del 68

 
 La cucaracha, la cucaracha ya no puede caminar, pues la golpearon los granaderos mandados por Díaz Ordaz. 
 Con lujo de fuerza bruta atacaron las escuelas y después con la bazooka le derribaron la puerta. 
 Era el 22 de julio de 1968, ellos solamente iban a un partido de fútbol americano, los equipos que protagonizaban el juego eran las Vocacionales 2 y 5 y la preparatoria incorporada a la UNAM, Isaac Ochoterena. Los eventos en la cancha pasaron a un segundo plano cuando comenzó la gresca, los rumores decían que los del IPN estaban capitaneados por las pandillas “Las Arañas” y “Los Ciudadelos”, pero nada estaba claro y todo era confusión. Cuando llegaron los granaderos, cambió todo, ya que parecía que les habían ordenado arrestar a todo joven, sin importar si habían cometido un delito o simplemente estaban en la hora y el lugar incorrecto. 
  
 Por esta razón decidieron emprender la retirada a la Vocacional 5, pensaron que los policías no se atreverían a violar las instalaciones educativas y la gresca finalizaría ahí. Pero inmediatamente se dieron cuenta de que las cosas no iban a ser así, las macanas y los gases lacrimógenos atravesaron las puertas de la escuela para reprimir a cualquier alumno o profesor que se atravesara, el delito que perseguían los granaderos ya no tenía que ver con la pelea, ahora el crimen era ser joven. 
  
 La oposición de los estudiantes no se hizo esperar, la furia y cólera que estaba contenida se liberó. Entre el 26 y el 29 de julio las calles del Centro Histórico fueron testigos de una batalla campal, los jóvenes ya no se conformaban con huir de la autoridad y la encararon con lo que tuvieran a la mano, sabían que el mundo estaba pasando por un gran cambio y ellos iban a ser los protagonistas de la nueva revolución. 
  
 Formaban parte de una generación única; alrededor del planeta, el campesinado redujo sus números a la mitad y las ciudades crecieron indiscriminadamente. Gracias a este hecho, el acceso a la educación media y superior, que antes era un privilegio de la clase alta, se volvió accesible a buena parte de la juventud. Ahora los jóvenes podían estudiar sobre los problemas de su nación y tratar de modificarlos. El mayo Francés y la Primavera de Praga mostraban un deseo de cambio que quería ser erradicado por el status quo , sin embargo, en México parecía haber lugar para una alteración al no existir tanques soviéticos que acabaran con el socialismo de rostro humano. 
  
  
 La intensidad de las protestas rebasó a las autoridades y el ejército tuvo que intervenir, esta institución demostró su incapacidad de interactuar con la sociedad civil cuando la noche del 29 de julio entró a la Preparatoria Número Uno en San Ildefonso a punta de bazuca. El saldo fue de mil detenidos y 400 heridos. 
  
 Fue a partir de este hecho cuando las autoridades decidieron intervenir en el conflicto. El rector Javier Barros Sierra declaró que era un día de duelo para todos los profesores y estudiantes e izó la bandera a media asta. Para sustentar sus palabras, el 1 de agosto encabezó una de muchas manifestaciones en repudio de la intolerable respuesta del ejército y cuerpo de granaderos. Por su parte,  Guillermo Massieu Helguera, director de IPN, hizo lo propio en una reunión con secretarios generales de las sociedades de alumnos y otros dirigentes sindicales. Iniciaba una nueva etapa del movimiento estudiantil, ahora con una alianza con las autoridades universitarias. 
  
  
  
 El movimiento que nació de una riña estudiantil estaba cobrando fuerza y demostró su poder durante la marcha del 27 de agosto. La multitud arrió la bandera del Zócalo para izar la bandera rojinegra, por primera ocasión se insultó públicamente al presidente y Sócrates Campos Lemus instó a quedarse en la Plaza de la Constitución hasta que obtuvieran una respuesta. La réplica llegó a través de bayonetas y tanquetas que acabaron con toda la resistencia. 
  
  
  
  
  
 Las autoridades universitarias respondieron a esta escalada de violencia el 13 de septiembre por medio de la Marcha del Silencio. Ante esta situación, el gobierno decidió encabezar la toma de las dos principales instituciones educativas del país: el 18 de septiembre el ejército ocupó Ciudad Universitaria y seis días después haría lo mismo en el Casco de Santo Tomás. Las tropas federales resguardarían las instituciones educativas hasta el 1° de octubre. 
  
  
  
 La noche del 2 de octubre llegó, el plan solamente era conocido por las altas cúpulas del gobierno y los miembros del Batallón Olimpia; ni siquiera los soldados que vigilarían el mitin estaban enterados de lo que iba a suceder. 
  
  
  
 A las seis de la tarde, un helicóptero disparó dos bengalas para iniciar el ataque. Los francotiradores del Batallón Olimpia apostados en los edificios de la Plaza de las Tres Culturas comenzaron a disparar hacia la multitud. Los militares, confundidos por la situación, repelieron la agresión disparando hacia los inmuebles habitacionales y hacia la multitud de manifestantes. 
  
  
  
  
 Diez días después, en el Estadio Olímpico Universitario, se inaugurarían los XIX Juegos Olímpicos, bautizados como “la olimpiada de la paz”. 
  
 A casi cincuenta años de estos eventos, todavía no podemos decir que hemos avanzado mucho como nación, actualmente  incluso los fotoperiodistas son víctimas de la violencia. Las imágenes que nos quedan del movimiento del 68 son de suma importancia para revisitar el pasado y criticar el presente. 
 *** Te puede interesar:  
 Películas y libros para entender el 2 de octubre de 1968 
 2 de octubre: Fotos ocultas del Batallón Olimpia