Frases para entender la forma de pensar de Vladimir Putin

 
 Valdimir Putin es una de las figuras más controvertidas a nivel mundial, nombrado el personaje del año en 2014, según el periódico británico “The Times”. Líder de la Rusia postsoviética, criticado por sus políticas bélicas y represivas, admirado por otros por su afrenta a los norteamericanos, lo queramos o no, no podemos ignorar lo que este hombre representa en el escenario mundial. Sabemos que Putin, que ahora se encuentra en su tercer mandato presidencial, tiene detrás una historia severa. Vladimir Putin creció con la Unión Soviética, misma que lo forjó e hizo de él un hombre preparado para la  guerra  y para la paz.  Su formación dentro de los servicios de inteligencia soviética, hizo de él un hombre diestro y hábil para el desafío de hacer una carrera política. Cuando en 1999 Yeltsin le dijo a Putin: “Cuide de Rusia”, le estaba entregando junto con el ’maletín atómico’ que contenía estrategias misilísticas, una misión que ha conducido hasta hoy a la guerra en Oriente. Como líder mundial, se ha destacado por su papel en el conflicto con Siria, el asilo temporal a Edward Snowden y  sus políticas que han situado a Rusia como contrapeso en el mundo frente al avance de Estados Unidos y la Unión Europea en Oriente. Sin embargo, d etrás de Putin también hay una historia que pone su mandato en tela de juicio. Mientras Cristina Kitchner habla de él como un líder ejemplar en la lucha contra el terrorismo, su participación en Oriente tampoco justifica las muertes civiles que los misiles soviéticos han dejado a su paso, así como el encarcelamiento del grupo de feministas punk “Pussy Riot”; actos que dejan mucho que pensar sobre su noción de democracia y la criminalización de quienes dentro de su país se oponen a sus políticas de guerra. 
   
 No podemos dejar de lado a este personaje que hoy está en el centro de la política mundial. Sabemos que hay un riesgo al centrarnos en el personaje y ejercer cierto culto a la personalidad; si nos descuidamos un poco, la historia pareciera girar en torno al carácter de una persona. No es así, pero conocer el pensamiento de un personaje puede ayudarnos a entender el rol político que ha jugado en su país y en el mundo. 
 Vladimir Putin hombre de principios sólidos, ha pasado a la historia nos guste o no, estas frases que presentamos muestran rasgos de su personalidad y carácter que nos revelan la base de su pensamiento como “hombre de Estado”. 
   “Tengo la sensación de que tengo todo lo que quiero” 
 “Un auténtico hombre debe intentarlo siempre, y una auténtica mujer debe resistirse siempre” “Soy el hombre más saludable no en Europa, sino en el mundo entero. Colecciono emociones” 
  
 “No leo libros de gente que haya traicionado nuestra madre patria” 
 “Un dirigente estatal debe tener, como mínimo, cabeza” “Tengo una vida privada en la cual no permito interferencias. Debo ser respetado en ella” 
  
 “Quien no extrañe a la Unión Soviética no tiene corazón. Quien quiera que pretenda volver a lo que era la unión soviética no tiene cerebro” 
 “Dejen que los perros ladren, la caravana sigue su camino” “Los ladrones no están en prisión” 
  
 “No estamos ya en la guerra fría, el espionaje es inaceptable” 
 “Que se cambien ellos los sesos y no nuestra Constitución” 
 “Todo puede probablemente no estar bien nunca. Pero tenemos que intentarlo nosotros” 
  
 “Creo que no es una sorpresa si os digo que está claro que me gustan todas las mujeres rusas, las mujeres rusas son las más bellas del mundo” 
 “Es nuestro propio país. ¿Cómo podemos quedarnos con los brazos cruzados? ¿Abrir la boca y cazar moscas?” 
 “Si quiere convertirse en fundamentalista islámico y someterse a una circuncisión, le invito a Moscú. Le aseguro que se lo harán de tal forma que no le volverá a crecer nada más”, Todos tenemos la opción  libertad y decisión de formar un juicio propio, una postura a favor o en contra de lo que Putin representa, desde nuestro punto de vista este personaje simplemente no puede pasar desapercibido y además, si somos críticos recordemos que, como decía Maquiavelo, para vencer al enemigo, hay que conocer también como piensa. 
  
 * Referencias Frases de Vladimir Putin Te puede interesar: 12 cosas que no sabías de Vladimir Putin La juventud de los actuales líderes del mundo