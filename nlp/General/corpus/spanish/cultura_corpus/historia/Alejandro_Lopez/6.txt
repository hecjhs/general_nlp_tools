Términos que puedes usar para aprender a hablar vikingo

 
 En la mitología nórdica, Odín ocupa el sitio más alto entre todos los dioses escandinavos. Se trata de una poderosa deidad que juega distintos papeles como dios de la guerra, la muerte y la sabiduría. En las grandes batallas, un presagio de Odín significaba una victoria segura. El creador del mundo y de los hombres enseñó a la humanidad de arte porque también era un gran poeta. Durante mucho tiempo, la poesía fue considerada en los territorios vikingos como un indicativo de poderes sobrenaturales, relacionados con el arte adivinatorio y las profecías. 
 Desde entonces se cree que la divinidad guerrera recorre el mundo con un disfraz, la mayoría de las ocasiones modifica su apariencia como la de un viejo con una enorme barba y una túnica sencilla, que lleva un bastón para poder caminar y pide posada en los pueblos más alejados de la civilización en el norte de Europa. Por esa razón, durante mucho tiempo los escandinavos europeos acostumbraron abrir sus puertas a cada vagabundo y viajero que se acercara a pedir posada, especialmente a los viejos cuyas características físicas tuvieran algo en común con Odín. 
 Si deseas comprender más sobre la fascinante cosmovisión vikinga, un territorio que por sus maravillas naturales fue considerado mágico, lleno de hadas, elfos, valquirias y poderosos dioses, aprende estas palabras básicas del idioma vikingo: 
  
 Ausa Vatni 
 Ritual vikingo por el que se le da nombre a un recién nacido y se invoca para él la protección del dios Thor.  
 Blót 
 Sacrificio ritual que ofrecían los vikingos a sus dioses nórdicos. 
  Drakkar (o LangSkip) Barco de guerra vikingo. Conocido por este nombre debido a la forma de dragón de sus mascarones de proa. 
 Drápa 
 Poesía de alabanza cantada por los escaldos o Skaldir a sus anfitriones.  
 Danegeld 
 Impuesto pagado por diferentes reinos para evitar la guerra con los vikingos. 
  Doegr 
 Nombre con el que se conoce a cada una de las dos partes en las que los vikingos dividían un día. 
  
 Fimbulvetr 
 En la mitología nórdica es el invierno de los inviernos, en el que perecerán todos los mortales y que precederá a la llegada del Ragnarök. 
  Frelsisol 
 Fiesta en la que un esclavo celebraba su libertad. 
  Fyrd 
 Milicia local. Tropas no profesionales que se encargaban de la defensa de los condados frentes a los asaltos vikingos. 
  Futhark 
 Alfabeto rúnico usado por los vikingos. 
  Glima 
 Especie de lucha libre que practicaban los vikingos. 
  Hamingja 
 Ángel guardián que acompañaba a una persona y decidía su suerte y su felicidad. También puede significar felicidad o protección de los dioses en general. 
   Hávamál 
 Libro de la sabiduría atribuido al dios Odín. 
  Heitstrenging 
 Ritual vikingo de juramento solemne que implicaba un castigo si no se cumplían los votos jurados. 
  Hird 
 Comitiva personal, compañeros de armas o séquito que acompañaba a la reina y a otros miembros poderosos de los clanes vikingos. 
  Hnefatafl (o hneftafi) Juego de mesa vikingo similar al ajedrez. 
  Húsfreya 
 Señora de la casa que administraba los bienes y las tierras cuando su marido estaba en una expedición comercial o de saqueo. 
  Jarl 
 Dentro de la sociedad vikinga, los jarls era hombres libres, ricos y propietarios de sus tierras. 
   
 Jólaöl 
 Cerveza con especias elaborada especialmente para la celebración del Jól Blót. 
  Jól Blót 
 Fiesta del solsticio de invierno que marcaba la llegada del año nuevo. 
  Jolegeiti 
 Jóvenes disfrazados de cabras que realizaban travesuras durante la celebración del Jól Blót 
  Knar 
 Barco mercante vikingo. 
  Knattleikr 
 Juego con palos y una pelota de cuero. 
  Laeknir 
 Curandera. 
  
 Langskip (o Drakkar) Tipo de embarcación de guerra veloz y de poco calado que utilizaban los vikingos durante sus saqueos e incursiones. 
  Leysingi 
 Esclavo que había comprado su libertad convirtiéndose de nuevo en un hombre libre. 
  Miklagard 
 Nombre con el que los vikingos se referían a la ciudad de Constantinopla. 
  Mjöd 
 Hidromiel o bebida de miel fermentada. 
  Muerto sobre paja 
 Forma en la que los vikingos se referían a todos aquellos guerreros que morían de viejos en lugar de en el campo de batalla. 
  Naustr 
 Astillero donde los artesanos construían, equipaban o reparaban las embarcaciones vikingas. 
  Odalsbondi 
 Propietario de tierras con carácter hereditario. 
   
 Ragnarök 
 En la mitología nórdica, el Ragnarök es la batalla del fin del mundo que enfrentará a los Aesir, liderados por el dios Odín, contra los Jotuns, liderados por el dios Loki. 
  Seid 
 Hechizo, conjuro o palabra usada para nombrar cierto tipo de hechizos realizados por las Seidkona o por las Völvas. 
  Seidkona 
 Hechicera o bruja. 
  Sirgblot 
 Fiesta para celebrar la llegada de la primavera. 
  Skjaldborg 
 Formación defensiva conocida como muro de escudos que consistía en formar una barrera defensiva con varios escudos superpuestos. 
  Skjaldmö 
 Mujeres guerreras. 
  
 Skali 
 Casa alargada de una sola dependencia considerada el edificio principal de las granjas donde se desarrollaba la mayor parte de la vida cotidiana.  
 Skreid 
 Tira de carne o pescado seco. 
  Smidir 
 Artesano encargado de la reparación de armaduras, barcos, casas y otros menesteres relacionados con su oficio. 
  Snekkar 
 Barco de guerra vikingo. 
  Thing 
 Asamblea local de los hombres libres y tribunal de justicia. 
   
 Thralls 
 Esclavos, normalmente capturados durante las expediciones de saqueo, dedicados a las tareas más pesadas de la granja. 
  Thulir 
 Recitadores anónimos y errantes de poemas y hechos heroicos vikingos que con el paso del tiempo fueron sustituidos por los Skaldir (o escaldos) . 
  Valhalla 
 Según la mitología nórdica, el Valhalla era el palacio de Odín en Asgard, donde todos los guerreros caídos en combate eran llevados por las valkirias. 
 Vetrarblot 
 Celebración del solsticio de verano dedicada al dios Balder. 
  Ymir 
 Según la mitología nórdica, Ymir fue el primer gigante y con su cuerpo Odín creo el mundo. 
  
 – La influencia de los vikingos en la cultura moderna es decisiva, desde sus avances en ingeniería naval con los distintos tipos de embarcaciones, destinadas para la carga, la guerra o el transporte. Su gran sentido de la orientación y conocimientos en navegación les permitieron trazar los mapas más cercanos a la realidad que ha visto la humanidad sin ningún tipo de aparato de geolocalización y mantener ataques relámpago con una precisión nunca antes vista. Si quieres conocer más sobre estos personajes legendarios, mira estas series de Netflix para aprender de historia . ¿Sabías que los vikingos eran asiduos bebedores y creían que en su muerte los esperaba una cabra de cuyas ubres brotaba cerveza? Conoce esta historia a profundidad leyendo 20 datos curiosos que no sabías sobre la cerveza . 
 * Fuente: 
 Tierra Quebrada