La aplicación que legitimará la elección de la Asamblea Constituyente de la CDMX

 En aras de transparentar el proceso del conteo de los votos, Tu Conteo surge de la iniciativa de jóvenes mexicanos para que la ciudadanía sea la primera en legitimar un proceso electoral. A través de su aplicación, permite que los ciudadanos formen una base de datos que contiene evidencia fotográfica que permite que cualquier usuario realice un conteo de los votos de la elección a partir de la sabana con los resultados de la votación. En esta ocasión, sólo estará disponible para las elecciones de la Asamblea Constituyente de la CDMX. 
  ¿Cómo funciona Tu Conteo? 1.- Descarga la App sin costo desde Google Play o Itunes Store 
 2.- Regístrate en Tuconteo.com con tu cuenta de Facebook 
 3.- Toma una foto de la sábana electoral de tu casilla 
 ¿Qué es la sabana electoral? Es una manta que el presidente de casilla llena en función de los votos emitidos en la casilla, misma que se exhibe de manera pública al cierre de la casilla y todos los representantes de casilla firman de conformidad. 
 4.- Llena el formulario requerido en el que deberás confirmar la información de tu fotografía con la base de datos de Tu Conteo 
 5.- Vigila la transparencia en el conteo 
  
 ¿Se ha probado antes? Sí, de hecho la aplicación surge después de la anulación de la elección del alcalde de la capital Tabasco y se probó en la elección extraordinaria. Aquí puedes ver el reporte. – Más información: 
 tuconteo.com