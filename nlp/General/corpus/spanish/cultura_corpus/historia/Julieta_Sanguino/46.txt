20 datos que no conocías sobre Frida Kahlo

 Frida Kahlo es la pintora más importante de México; su popularidad pone en alto a las mujeres mexicanas, nuestras tradiciones, costumbres y el arte de nuestro país. Fue una gran luchadora social y activista; no le importaba lo que los demás pensaban y vivía con libertad y sin tapujos. Le encantaba pasar tiempo con sus amigos, su fuerza y vigor eran enormes, mellando las dificultades que tenía por el dolor que provocó un terrible accidente. 
 Hoy, su figura se ha convertido en un ícono popular de México. Su nombre es aclamado y tiene miles de admiradores. Es amada u odiada pero nunca ignorada. Su figura es parte de la identidad nacional; sus casas en San Ángel y Coyoacán son patrimonio de nuestro país; su historia, un relato que parece ficción. 
 ¿A quién no le hubiera gustado convivir con esa mujer ríspida, con un sentido del humor  ágil y sarcástico? Frida Kahlo fue una mujer muy interesante que vale la pena conocer. Por eso te presentamos 15 datos que lo comprueban. 
 El nombre completo de Frida fue Magdalena Carmen Frieda Kahlo y Calderón; lo cambió en 1935 por Frida, eliminando la letra e. 
 Guillermo Kahlo, su padre, fue un fotógrafo alemán; él fue quien le enseñó el mundo del arte. 
 Cuando tenía seis años sufrió poliomielitis, por lo que sus crueles compañeros de la escuela la apodaron “Frida pata de palo”.    
 El accidente que sufrió a los 18 años rompió su columna vertebral, su clavícula, costillas, pelvis, su pierna derecha, pie derecho y dislocó su hombro. Una pieza de hierro hirió su abdomen y útero, lo que le impidió ser madre. 
 Frida fue una de las pocas mujeres que estudió en la Preparatoria Nacional, tenía un grupo de amigos que se hacían llamar “Los Cachuchas”, conocidos por su rebeldía y participación política estudiantil. Su primer amor, Alejandro Gómez Arias era parte de ellos. 
 El Marco fue la primera pintura de un artista mexicano del siglo XX en ser comprada por el Museo del Louvre. 
  Su canción favorita era “La Adelita”. 
 Su obra Raíces tiene el récord del cuadro latinoamericano más costoso con un precio de 5,6 millones de dólares en 2006. 
 La fotógrafa italiana Tina Modotti fue quien presentó a Diego Rivera y Frida, la fotógrafa sostuvo una relación con el pintor antes de presentarlos. 
 En 1927, cuando Frida tenía 22 años y Diego 43,  se casaron . Su padre le advirtió a su yerno que su hija era “un demonio” y su madre comentó que era como si se casara un elefante con una paloma. 
  
 A Frida le encantaba la cerveza y su favorita era la Bohemia, marca que hizo una edición limitada de la pintora. 
 La letra de la canción “Scar Tissue” del grupo Red Hot Chili Peppers está dedicada a Frida Kahlo. 
  
 El matrimonio de Diego y Frida estuvo lleno de infidelidades y deslices, uno de los peores fue el engaño de Diego con la hermana menor de Frida, Cristina Kahlo. Cuando Frida se enteró, la pareja se separó. 
 Frida nunca negó su bisexualidad, tuvo diez amores comprobados además de Diego entre los que destacan el político Leon Trotsky, el fotógrafo Nickolas Muray y Chavela Vargas. 
 Frida trabó amistad con algunos miembros del movimiento surrealista como André Breton. Frida aseguró sobre su obra que “No sabía que era surrealista hasta que llegó André Breton a México y me lo dijo”. Aunque ella no estuvo del todo de acuerdo pues pintaba su vida y no sueños. 
  
 Produjo cerca de 200 pinturas, dibujos y esbozos; 143 fueron pinturas, de las cuales realizó 55 autorretratos.”Me retrato a mí misma porque paso mucho tiempo sola y porque soy el motivo que mejor conozco”. 
 El nombre del disco Viva la vida , de Coldplay, está inspirado en uno de los cuadros de Frida. Chris Martin, vocalista del grupo, dijo que lo atrajo la vivacidad del cuadro totalmente opuesta al sufrimiento que experimentaba la pintora. 
  Sus últimas palabras fueron“Espero alegre la salida y espero no volver jamás”. 
 Las cenizas de Frida yacen en su casa de Coyoacán y aunque Diego pidió colocar los suyos al lado de los de Frida, los llevaron a la Rotonda de los Hombres Ilustres en el Panteón Civil de Dolores en la Ciudad de México. 
 La popularidad de Frida Kahlo en el extranjero comenzó hasta la década de los 80, cuando se convirtió en referente de la cultura mexicana. 
  
 *** 
 Te puede interesar: Los amores y amantes de Frida Kahlo