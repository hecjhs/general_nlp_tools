from django.apps import AppConfig


class TokenizersConfig(AppConfig):
    name = 'tokenizers'
